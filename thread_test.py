import time
import datetime
from threading import Thread


def foo(bar, *args, **kwargs):
    print("Hello %s" % bar)
    return datetime.datetime.now()


class ThreadWithReturnValue(Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, Verbose=None):
        Thread.__init__(self, group, target, name, args, kwargs)
        if kwargs is None:
            kwargs = dict()
        self._return = None

    def run(self):
        print(type(self._target))
        if self._target is not None:
            time.sleep(10)
            self._return = datetime.datetime.now()

    def join(self, *args):
        Thread.join(self, *args)
        return self._return


if __name__ == '__main__':
    twrv = ThreadWithReturnValue(target=foo, args='world', kwargs=dict())
    twrv.start()
    twrv1 = ThreadWithReturnValue(target=foo, args='world', kwargs=dict())
    twrv1.start()
    print(twrv.join())
    print(twrv1.join())

