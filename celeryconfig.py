from celery.schedules import crontab
from appconfig import REDIS_HOST, REDIS_PORT

CELERY_IMPORTS = ('core.task.task')
CELERY_TASK_RESULT_EXPIRES = 30
CELERY_TIMEZONE = 'UTC'
CELERY_RESULT_BACKEND = 'redis://' + REDIS_HOST + ':' + str(REDIS_PORT)
CELERY_ACCEPT_CONTENT = ['json', 'msgpack', 'yaml']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

CELERYBEAT_SCHEDULE = {
    'mail-packet-error': {
        'task': 'core.task.task.mail_pckt_err',
        # Every minute
        'schedule': crontab(hour=2, minute=00),
    },
    '15-mins-get-traffic-international': {
        'task': 'core.task.task.get_traffic_uplink_international',
        # Every minute
        'schedule': crontab(minute="*/15"),
    },
    'half-daily-scan-ring-info-vipa': {
        'task': 'core.task.task.scan_ring_info_vipa',
        # Every day
        'schedule': crontab(hour=17, minute=1),
    },
    'daily-get-kpi-kv1': {
        'task': 'core.task.task.get_kpi_wait_and_scan_kv1',
        # Every minute
        'schedule': crontab(hour=0, minute=10),
    },
    'daily-get-wanphy-ckv': {
        'task': 'core.task.task.get_interface_wanphy_wrong',
        # Every minute
        'schedule': crontab(hour=10, minute=0),
    },
    'daily-get-kpi-kv2': {
        'task': 'core.task.task.get_kpi_wait_and_scan_kv2',
        # Every minute
        'schedule': crontab(hour=0, minute=25),
    },
    'daily-get-kpi-kv3': {
        'task': 'core.task.task.get_kpi_wait_and_scan_kv3',
        # Every minute
        'schedule': crontab(hour=0, minute=30),
    },
    'daily-get-kpi-cam': {
        'task': 'core.task.task.get_kpi_wait_and_scan_cam',
        # Every minute
        'schedule': crontab(hour=0, minute=45),
    },
    'daily-get-kpi-tim': {
        'task': 'core.task.task.get_kpi_wait_and_scan_tim',
        # Every minute
        'schedule': crontab(hour=1, minute=30),
    },
    'daily-get-kpi-hai': {
        'task': 'core.task.task.get_kpi_wait_and_scan_hai',
        # Every minute
        'schedule': crontab(hour=5, minute=30),
    },
    'daily-get-kpi-moz': {
        'task': 'core.task.task.get_kpi_wait_and_scan_moz',
        # Every minute
        'schedule': crontab(hour=1, minute=30),
    },
    'daily-get-top-nocpro': {
        'task': 'core.task.task.mail_top_alm_vn',
        # Every minute
        'schedule': crontab(hour=2, minute=5),
    },
    'daily-get-kpi-per': {
        'task': 'core.task.task.get_kpi_wait_and_scan_per',
        # Every minute
        'schedule': crontab(hour=5, minute=10),
    },
    'daily-mail-alarm-nocpro': {
        'task': 'core.task.task.mail_top_syslog_dev_vn',
        # Every minute
        'schedule': crontab(hour=2, minute=30),
    },
    'daily-scan-interface-not-in-group-kv1': {
        'task': 'core.task.task.insert_interface_not_in_group_kv1',
        # Every minute
        'schedule': crontab(hour=18, minute=30),
    },
    'daily-scan-interface-not-in-group-kv2': {
        'task': 'core.task.task.insert_interface_not_in_group_kv2',
        # Every minute
        'schedule': crontab(hour=19, minute=30),
    },
    'daily-scan-interface-not-in-group-kv3': {
        'task': 'core.task.task.insert_interface_not_in_group_kv3',
        # Every minute
        'schedule': crontab(hour=20, minute=30),
    }

    #,
    #'half-daily-dev-intf-list': {
    #    'task': 'core.task.task.scan_dev_intf_lst',
    #    # Every minute
    #    'schedule': crontab(hour=13, minute=1),
    #}
}
