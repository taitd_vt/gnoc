__author__ = 'vtn-dhml-dhip10'
# import sys
# print(sys.path)
import cx_Oracle
from core.threading.bot_thread.scan_bot_cbs_thread import ScanBotCbsThread
from core.threading.bot_thread.scan_bot_soc_thread import ScanBotSocThread
from core.threading.bot_thread.scan_bot_nocpro_error_device_thread import ScanBotNocproErrorDeviceThread
from core.threading.bot_thread.scan_bot_aom_thread import ScanBotAomThread
from core.threading.bot_thread.scan_bot_bss_thread import ScanBotBssThread
from core.threading.bot_thread.bss.scan_bot_bss_important_1h_thread import ScanBotBssImportant1hThread
from core.threading.bot_thread.bss.scan_bot_bss_important_10m_thread import ScanBotBssImportant10mThread
from core.threading.bot_thread.bss.scan_bot_bss_important_12h_thread import ScanBotBssImportant12hThread
from core.threading.bot_thread.bss.scan_bot_bss_important_30m_thread import ScanBotBssImportant30mThread
from core.threading.bot_thread.bss.scan_bot_bss_important_10m_series_thread import ScanBotBssImportant10mSeriesThread
from core.threading.bot_thread.bss.scan_bot_bss_rnc_period_15m_thread import ScanBotBssRncPeriod15mThread
from core.threading.ipms.scan_kpi_rnc_cbs_thread import ScanKpiRncCbsThread
from core.threading.server_minhnd_thread.scan_web_connect_thread import ScanWebConnectThread
from core.threading.server_minhnd_thread.scan_web_nslookup_thread import ScanWebNslookupThread
import os
import telebot
import time
from config import Development

text_messages = {
    'welcome':
        u'Please welcome {name}!\n\n'
        u'This chat is intended for questions about and discussion of the pyTelegramBotAPI.\n'
        u'To enable group members to answer your questions fast and accurately, please make sure to study the '
        u'project\'s documentation (https://github.com/eternnoir/pyTelegramBotAPI/blob/master/README.md) and the '
        u'examples (https://github.com/eternnoir/pyTelegramBotAPI/tree/master/examples) first.\n\n'
        u'I hope you enjoy your stay here!',

    'info':
        u'My name is TeleBot,\n'
        u'I am a bot that assists these wonderful bot-creating people of this bot library group chat.\n'
        u'Also, I am still under development. Please improve my functionality by making a pull request! '
        u'Suggestions are also welcome, just drop them in this group chat!',

    'wrong_chat':
        u'Hi there!\nThanks for trying me out. However, this bot can only be used in the pyTelegramAPI group chat.\n'
        u'Join us!\n\n'
        u'https://telegram.me/joinchat/067e22c60035523fda8f6025ee87e30b'
}

config = Development()
BOT_ID = config.__getattribute__('BOT_ID')
TELEBOT_BOT_TOKEN = BOT_ID
bot = telebot.AsyncTeleBot(BOT_ID)

SERVER_NOCPRO = config.__getattribute__('SERVER_NOCPRO')
SERVER_NOCPRO_PORT = config.__getattribute__('SERVER_NOCPRO_PORT')
SERVER_NOCPRO_SERVICE_NAME = config.__getattribute__('SERVER_NOCPRO_SERVICE_NAME')
SERVER_NOCPRO_USERNAME = config.__getattribute__('SERVER_NOCPRO_USERNAME')
SERVER_NOCPRO_PASSWORD = config.__getattribute__('SERVER_NOCPRO_PASSWORD')
SERVER_AOM = config.__getattribute__('SERVER_AOM')
SERVER_AOM_PORT = config.__getattribute__('SERVER_AOM_PORT')
SERVER_AOM_SERVICE_NAME = config.__getattribute__('SERVER_AOM_SERVICE_NAME')
SERVER_AOM_USERNAME = config.__getattribute__('SERVER_AOM_USERNAME')
SERVER_AOM_PASSWORD = config.__getattribute__('SERVER_AOM_PASSWORD')
SERVER_CBS = config.__getattribute__('SERVER_CBS')
SERVER_CBS_PORT = config.__getattribute__('SERVER_CBS_PORT')
SERVER_CBS_SERVICE_NAME = config.__getattribute__('SERVER_CBS_SERVICE_NAME')
SERVER_CBS_USERNAME = config.__getattribute__('SERVER_CBS_USERNAME')
SERVER_CBS_PASSWORD = config.__getattribute__('SERVER_CBS_PASSWORD')

def listener(messages):
    for m in messages:
        print(str(m))


if __name__ == '__main__':
    check_running = False
    firt_run = 0
    period_time = 60 * 5
    per_15m = 60 * 15
    period_hourly = 60 * 60
    period_half_hour = 30 * 60
    period_daily = 60 * 60 * 24
    period_fw = 2 * 60
    area_lst = ['KV1', 'KV2', 'KV3']

    _scan_web_nslookup_new = ScanWebNslookupThread(False, period_time, True, bot)
    _scan_web_nslookup_new.start()

    #_scan_web = ScanWebConnectThread(False, period_daily)
    #_scan_web.start()

