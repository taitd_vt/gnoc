__author__ = 'vtn-dhml-dhip10'
import threading
import time
from core.database.impl.bot.bot_telethon_impl import BotTelethonImpl
from core.log.logger.logger import Logging
from core.threading.device_thread.dev_scan_backhaul_upstream_lst import DeviceScanBackhaulUpstreamListThread
from core.helpers.list_helpers import chunkIt, get_pop_lst
from app.api.vmsa.parsing.vmsa_parsing import VmsaParsing
import urllib.request
import uuid
import os
import os
import uuid
import telebot
import time
import json
import cx_Oracle
from core.threading.device_thread.dev_show_interface_crc_one_thread import DeviceShowInterfaceCrc
from config import Config, Development
from core.database.impl.gnoc.tbl_syslog_gnoc_impl import TblSyslogGnocImpl
from core.helpers.date_helpers import get_time_format_now, get_date_minus, get_date_minus_format_elk, \
    convert_date_str_to_date_obj_spring, get_date_now, convert_date_to_epoch, get_date_yesterday_format_ipms, \
    get_only_date_now_format_ipms
from app.api.elasticsearch.api_kibana import ApiKibana
from core.database.impl.server_minhnd.tbl_deny_web_impl import TblDenyWeb
from core.database.impl.npms.tbl_bss_npms_impl import TblBssNpmsImpl
from core.database.impl.nocpro.tbl_ip_syslog_core import TblNocproImpl
from core.database.impl.pctt.tbl_pctt_app_impl import TblPcttAppImpl
from pygooglechart import XYLineChart
from pygooglechart import Chart
from pygooglechart import SimpleLineChart
from pygooglechart import XYLineChart
from pygooglechart import SparkLineChart
from pygooglechart import PieChart2D
from pygooglechart import Axis
from core.helpers.api_helpers import api_post_retry, api_get_retry
from core.helpers.stringhelpers import get_province_bras_name, get_province_type, get_mess_filter
from core.database.impl.server_minhnd.tbl_useronline_bras_impl import TblUseronlineBrasImpl
from core.helpers.date_helpers import get_time_format_now, get_date_minus, get_date_minus_format_elk, \
    convert_date_str_to_date_obj_spring, get_date_now, convert_date_to_epoch, convert_date_to_ipms
from core.helpers.int_helpers import convert_string_to_int
from core.database.impl.tbl_aaa_impl import TblAaaImpl
from core.helpers.stringhelpers import convert_api_vmsa_create_dt_log_for_bsc_xml_to_str, split_mess_with_uctt
from core.helpers.tenant_helpers import get_tenant_id_from_area, get_tenant_id_syslog_from_area
from core.helpers.fix_helper import get_area, get_area_from_acc
from core.database.impl.nocpro.tbl_gpon_olt_sub_impl import TblGponOltSubImpl
from core.database.impl.tbl_online_aaa_impl import TblOnlineAaaImpl
from core.database.impl.nocpro.tbl_bss_core_impl import TblBssCoreImpl
from core.database.impl.ipms.host_ipms_impl import HostIpmsImpl
from core.database.impl.ipms.syslog_nocpro_impl import SyslogNocproIpmsImpl
from core.database.impl.server_minhnd.tbl_interface_drop_impl import TblInterfaceDropImpl
from core.database.impl.server_minhnd.tbl_ring_info_order_impl import TblRingInfoOrderImpl
from core.database.impl.tbl_ring_info_impl import TblRingInfoImpl
from core.helpers.fix_helper import get_node_type_mobile
from core.helpers.stringhelpers import convert_api_television_check_user_to_dct
from core.helpers.list_helpers import get_filter_in_list
from core.database.impl.nocpro.tbl_bss_cbs_core_impl import TblBssCbsCoreImpl
from core.database.impl.server_minhnd.tbl_cable_backbone import TblCableBackboneImpl
from app.api.vmsa.parsing.vmsa_parsing import VmsaParsing
from app.api.television.qos_api_server import QosApiServer
from core.helpers.encode.decode_base64 import DecodeBase64
from core.database.impl.elasticsearch.elastic_impl import ElasticSearchHost
from config import Config, Development


class ScanBackhaulUpstreamThread(threading.Thread):
    def __init__(self, is_stop, period_time):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.period_time = period_time
        self.thread_name = 'ScanBackhaulUpstream'

    def run(self):
        interval = self.period_time
        check_late = False
        time_scan = None
        time_now = get_time_format_now()
        time_next_interval = plus_time_sec(time_now, interval)
        while not self.is_stop:
            log_scan = Logging('Scan Backhaul', 'Thread get data of Backhaul and Upstream')

            try:
                time_now = get_time_format_now()
                if check_late and time_scan:
                    time_now = time_scan
                    check_late = False

                time_next_interval = plus_time_sec(time_now, interval)
                hour_now = str(time_now.hour)
                minute_now = str(time_now.minute)
                second_now = str(time_now.second)
                year_now = str(time_now.year)
                month_now = str(time_now.month)
                day_now = str(time_now.day)
                date_format = '{0}_{1}_{2}_{3}_{4}_{5}'.format(year_now, month_now, day_now, hour_now, minute_now,
                                                               second_now)
                name_log = 'log_scan_backhaul_' + date_format + '.txt'
                log_scan = Logging(name_log, 'Thread get data of Backhaul and Upstream')
                log_scan.create_log('info', 'Start getting data Backhaul and Upstream')

                #hst_mnl_lst = ['HHT9602CRT24.IGW.ASR22.04', 'HCM_IW2_ASR9K_HHTN4', 'HKH9103CRT13.DGW.ASR12.01']
                hst_mnl_lst = ['SGP_POP4_PEERING_ASR']
                dev_mnl = DeviceScanBackhaulUpstreamListThread(False, time_now, 'manual', hst_mnl_lst,'cisco')
                #dev_mnl.start()

                # scan auto
                pop_lst = get_pop_lst()

                if pop_lst:
                    pop_lst_div = chunkIt(pop_lst, 4)
                    dev_auto = DeviceScanBackhaulUpstreamListThread(False, time_now, 'auto', pop_lst_div[3], 'cisco')
                    dev_auto.start()
                    #for y in pop_lst_div:
                     #   dev_auto = DeviceScanBackhaulUpstreamListThread(False, time_now, 'auto', y, 'cisco')
                        #dev_auto.start()

                time_later = get_time_format_now()
                time_diff = time_later - time_now
                time_diff_second = convert_timedelta_to_second(time_diff)

                print('Diff time scan:' + str(time_diff_second))
                if interval >= time_diff_second:
                    time_sleep = interval - time_diff_second
                    print('Sleeping for next ' + str(time_sleep) + " s")
                    time.sleep(time_sleep)
                else:
                    check_late = True
                    time_scan = time_next_interval

            except Exception as err:
                print(err)
                time_later = get_time_format_now()
                time_diff = time_later - time_now
                time_diff_second = convert_timedelta_to_second(time_diff)

                print('Diff time:' + str(time_diff_second))
                if interval >= time_diff_second:
                    time_sleep = interval - time_diff_second
                    print('Sleeping for ' + str(time_sleep) + " s")
                    time.sleep(time_sleep)
                else:
                    check_late = True
                    time_scan = time_next_interval

                log_scan.create_log('critical', err)

    def stop(self):
        self.is_stop = True


def get_time_format_now():
    import datetime
    return datetime.datetime.now()


def plus_time_sec(tme, num):
    import datetime
    return tme + datetime.timedelta(seconds=num)


def convert_timedelta_to_second(time_delta):

    return time_delta.total_seconds()


if __name__ == '__main__':
    #test = ScanBackhaulUpstreamThread(False, 60 * 7)
    #test.run()
    #pass
    '''
    from core.database.impl.nocpro.tbl_ip_syslog_core import TblIpSyslogCoreElk
    _obj = TblIpSyslogCoreElk(None, None)
    res = _obj.find_olt_lst("TTH")
    print(res)
    
    from netaddr import IPNetwork, cidr_merge, cidr_exclude


    class IPSplitter(object):
        def __init__(self, base_range):
            self.avail_ranges = set((IPNetwork(base_range),))

        def get_subnet(self, prefix, count=None):
            for ip_network in self.get_available_ranges():
                subnets = list(ip_network.subnet(prefix, count=None))
                if not subnets:
                    continue
                self.remove_avail_range(ip_network)
                self.avail_ranges = self.avail_ranges.union(set(cidr_exclude(ip_network, cidr_merge(subnets)[0])))
                return subnets

        def get_available_ranges(self):
            return sorted(self.avail_ranges, key=lambda x: x.prefixlen, reverse=True)

        def remove_avail_range(self, ip_network):
            self.avail_ranges.remove(ip_network)


    s = IPSplitter('27.66.128.0/20')
    res_lst = s.get_subnet(24, 100)
    print(s.get_available_ranges())

    print(res_lst)
   
    msg_and_splt = []
    msg_or_splt = []
    date_time = '2021-02-02'
    mes_fnd = "FOIP&over threshold@Packet Error@under threshold"
    msg_and_splt_tmp = mes_fnd.split("&")
    for msg_and in msg_and_splt_tmp:
        if msg_and.find('@') >= 0:
            msg_or_splt_tmp = msg_and.split("@")
            for msg_or in msg_or_splt_tmp:
                if msg_or not in msg_and_splt:
                    msg_or_splt.append(msg_or)
        else:
            msg_and_splt.append(msg_and)
    sql_find = "Select * from log_nocpro where datetime > " + str(date_time) + " and "
    # ghep them and va or
    num_and = 0
    num_or = 0
    for msg_and in msg_and_splt:
        if num_and == 0:
            sql_find += " msg like %" + str(msg_and) + "% "
        else:
            sql_find += " and msg like %" + str(msg_and) + "% "
        num_and += 1
    for msg_or in msg_or_splt:
        if num_or == 0:
            sql_find += " and (msg like %" + str(msg_or) + "% "
        else:
            sql_find += " or msg like %" + str(msg_or) + "% "
        num_or += 1
    # them phan dong ngoac vao cho full
    sql_find += ")"
    print(sql_find)
    '''
    from core.threading.bot_thread.scan_bot_bss_thread import ScanBotBssThread
    import cx_Oracle
    import telebot
    import time
    from config import Development
    config = Development()
    BOT_ID = config.__getattribute__('BOT_ID')
    TELEBOT_BOT_TOKEN = BOT_ID
    bot = telebot.AsyncTeleBot(BOT_ID)

    SERVER_NOCPRO = config.__getattribute__('SERVER_NOCPRO')
    SERVER_NOCPRO_PORT = config.__getattribute__('SERVER_NOCPRO_PORT')
    SERVER_NOCPRO_SERVICE_NAME = config.__getattribute__('SERVER_NOCPRO_SERVICE_NAME')
    SERVER_NOCPRO_USERNAME = config.__getattribute__('SERVER_NOCPRO_USERNAME')
    SERVER_NOCPRO_PASSWORD = config.__getattribute__('SERVER_NOCPRO_PASSWORD')
    SERVER_AOM = config.__getattribute__('SERVER_AOM')
    SERVER_AOM_PORT = config.__getattribute__('SERVER_AOM_PORT')
    SERVER_AOM_SERVICE_NAME = config.__getattribute__('SERVER_AOM_SERVICE_NAME')
    SERVER_AOM_USERNAME = config.__getattribute__('SERVER_AOM_USERNAME')
    SERVER_AOM_PASSWORD = config.__getattribute__('SERVER_AOM_PASSWORD')
    SERVER_CBS = config.__getattribute__('SERVER_CBS')
    SERVER_CBS_PORT = config.__getattribute__('SERVER_CBS_PORT')
    SERVER_CBS_SERVICE_NAME = config.__getattribute__('SERVER_CBS_SERVICE_NAME')
    SERVER_CBS_USERNAME = config.__getattribute__('SERVER_CBS_USERNAME')
    SERVER_CBS_PASSWORD = config.__getattribute__('SERVER_CBS_PASSWORD')
    SERVER_HOST = config.__getattribute__('SERVER_HOST')
    SERVER_HOST_USER = config.__getattribute__('SERVER_HOST_USER')
    SERVER_HOST_PASS = config.__getattribute__('SERVER_HOST_PASS')
    dsn_str = cx_Oracle.makedsn(SERVER_NOCPRO, SERVER_NOCPRO_PORT, service_name=SERVER_NOCPRO_SERVICE_NAME)
    '''
    pool_nocpro = cx_Oracle.SessionPool(min=1,
                                        max=1, increment=1, threaded=True, dsn=dsn_str,
                                        user=SERVER_NOCPRO_USERNAME, password=SERVER_NOCPRO_PASSWORD,
                                        encoding='UTF-8', nencoding='UTF-8')


    dsn_cbs_str = cx_Oracle.makedsn(SERVER_CBS, SERVER_CBS_PORT, service_name=SERVER_CBS_SERVICE_NAME)
    pool_cbs = cx_Oracle.SessionPool(min=1,
                                     max=1, increment=1, threaded=True, dsn=dsn_cbs_str,
                                     user=SERVER_CBS_USERNAME, password=SERVER_CBS_PASSWORD,
                                     encoding='UTF-8', nencoding='UTF-8')
    '''
    message = "checkbsc BCHT01"

    key_txt = ''
    alrm_lvl = ''
    words = message.split()
    res_info = ''

    n = len(words)
    if n >= 0:
        key_txt = words[1].upper()
        alrm_lvl = ''
        para_lst = []
        para_dct = dict(para_key="OP_STATE", para_val="BL")
        para_lst.append(para_dct)

        _test = VmsaParsing(29020, "xxx-check-rnc-and-bsc", para_lst, key_txt, "BSC", 1)
        _ring_info = _test.request()

        _res_dct = convert_api_vmsa_create_dt_log_for_bsc_xml_to_str(_ring_info)
        if 'logFileContent' in _res_dct:
            _encode = DecodeBase64(_res_dct['logFileContent'], 'milanox')
            key_lst = ['BUSY FULL RATE', 'BUSY HALF RATE', 'BUSY SDCCH', 'IDLE FULL RATE', 'IDLE HALF RATE',
                       'IDLE SDCCH', 'BLOCKED RADIO TIME SLOTS', 'GPRS TIME SLOTS']
            _res_bsc_lst = _encode.read_file_get_val(key_lst)
            if _res_bsc_lst:
                res_info = "**Thông số lưu lượng của BSC:**\n"
                if key_fnd != '':
                    _res_bsc_lst = get_filter_in_list(_res_bsc_lst, key_filter=key_fnd)
                for _res_bsc in _res_bsc_lst:
                    if isinstance(_res_bsc, dict):
                        if 'key' in _res_bsc and 'val' in _res_bsc:
                            res_info += _res_bsc['key'] + ": " + str(_res_bsc['val']) + "\n"


            else:
                res_info = "Không có cảnh báo tương ứng"

        else:
            res_info = "Không có node tương ứng"
    else:
        res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
    print(res_info)
