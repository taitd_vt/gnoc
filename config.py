import os

basedir = os.path.abspath(os.path.dirname(__file__))
from os import environ


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'strongkey_secret'

    @staticmethod
    def init_app(app):
        pass


class Development(Config):
    DEBUG = True
    MONGO_DATABASE_SERVER = os.environ.get('DEV_DATABASE_SERVER') or '192.168.251.15'
    MONGO_DATABASE_PORT = os.environ.get('DEV_DATABASE_PORT') or 8686
    API_DATABASE_PORT = os.environ.get('DEV_API_PORT') or 8686

    #ELASTIC_SERVER = os.environ.get('DEV_DATABASE_SERVER') or '192.168.251.15'
    ELASTIC_SERVER = os.environ.get('DEV_DATABASE_SERVER') or '10.240.203.20'
    ELASTIC_SERVER_OLD = '192.168.251.15'
    ELASTIC_USERNAME = 'ironman'
    ELASTIC_PASSWORD = 'AdminViettel@123'
    ELASTIC_PORT = 9200

    MONGO_DATABASE_NAME = os.environ.get('DEV_DATABASE_NAME') or 'iron_crawl'
    API_BITTREX_COIN_PAIR = "https://bittrex.com/Api/v2.0/pub/market/GetTicks?marketName="

    SERVER_PARSING = "10.60.97.100"
    SERVER_PARSING_USER = "dt_ipcore"
    SERVER_PARSING_PASS = "VIPA@8569vipa"

    SERVER_AAA_LST = list()
    SERVER_AAA_KV1 = dict(ip="10.60.60.77", area="KV1")
    SERVER_AAA_KV2 = dict(ip="10.42.50.23", area="KV2")
    SERVER_AAA_KV3 = dict(ip="10.73.66.48", area="KV3")

    SERVER_AAA_LST.append(SERVER_AAA_KV1)
    SERVER_AAA_LST.append(SERVER_AAA_KV2)
    SERVER_AAA_LST.append(SERVER_AAA_KV3)

    SERVER_AAA_USERNAME = "account_tshoot"
    SERVER_AAA_PASSWORD = "Trouble@Shoot135"

    SERVER_AAA_MAIN_LST = [dict(ip="10.60.60.71", area="KV1"),
                           dict(ip="10.60.60.70", area="KV1"),
                           dict(ip="10.42.50.16", area="KV2"),
                           dict(ip="10.42.50.17", area="KV2"),
                           dict(ip="10.73.66.41", area="KV3"),
                           dict(ip="10.73.66.42", area="KV3")]

    SERVER_AAA_MAIN_USERNAME = "bot_account"
    SERVER_AAA_MAIN_PASSWORD = "Bot135@$^"

    API_BLANK = "http://" + MONGO_DATABASE_SERVER + ":" + str(API_DATABASE_PORT)
    API_IRON = "http://" + MONGO_DATABASE_SERVER + ":" + str(API_DATABASE_PORT)
    API_IRON_TOKEN = "http://" + MONGO_DATABASE_SERVER + ":" + str(API_DATABASE_PORT) + "/token"
    API_IRON_NUMBER_TOKEN = "http://" + MONGO_DATABASE_SERVER + ":" + str(API_DATABASE_PORT) + "/number_token"
    API_ERC20 = "&tag=latest&apikey=YourApiKeyToken"
    API_COIN = "http://" + MONGO_DATABASE_SERVER + ":" + str(API_DATABASE_PORT) + "/coin"
    API_COIN_DAILY = "http://" + MONGO_DATABASE_SERVER + ":" + str(API_DATABASE_PORT) + "/coin_price_d1"
    API_COIN_M30 = "http://" + MONGO_DATABASE_SERVER + ":" + str(API_DATABASE_PORT) + "/coin_price_m30"
    API_IRON_COIN_PUMP_HISTORY = "http://" + MONGO_DATABASE_SERVER + ":" + str(API_DATABASE_PORT) + "/coin_pump_history"
    API_COIN_SUPPORT = "http://" + MONGO_DATABASE_SERVER + ":" + str(API_DATABASE_PORT) + "/coin_price_support"
    API_COIN_THRESHOLD = "http://" + MONGO_DATABASE_SERVER + ":" + str(API_DATABASE_PORT) + "/coin_price_threshold"
    API_COIN_RSI = "http://" + MONGO_DATABASE_SERVER + ":" + str(API_DATABASE_PORT) + "/coin_price_rsi"
    API_COIN_BB = "http://" + MONGO_DATABASE_SERVER + ":" + str(API_DATABASE_PORT) + "/coin_price_bb"
    API_COIN_TRADE_HISTORY = "http://" + MONGO_DATABASE_SERVER + ":" + str(API_DATABASE_PORT) + "/coin_trade_history"

    API_VIPA_RING = "http://10.60.97.112:8888/IPChangeProcess/rest/DbTaskService/query2"
    API_VIPA_PARSING = "http://10.60.97.100:8686/IPChangeProcess/WSForOtherSystem"
    API_VIPA_PARSING_2 = "http://10.60.97.112:8686/IPChangeProcess/WSForOtherSystem"
    API_VIPA_RING_USERNAME = "vipa"
    API_VIPA_RING_PASSWORD = "Vipa@123"

    #API_VMSA_PARSING = "http://10.60.129.74:8668/MSChangeProcess/WSVMSA"
    API_VMSA_PARSING = "http://10.60.129.244:8800/MSChangeProcess/WSVMSA?wsdl"
    API_VMSA_PARSING_MOP_ALL = "http://10.60.129.73:8666/MSChangeProcess/WSVMSAImpl?wsdl"
    API_VMSA_USERNAME = "vmsa"
    API_VMSA_PASSWORD = "Viettel@123#@!"

    API_TELEVISION = "http://10.60.66.228:8080/QOS_API_Server.php"
    API_TELEVISION_USERNAME = "monitor"
    API_TELEVISION_PASSWORD = "Viettel@123"

    API_SOC = "http://10.60.129.76:8099/SOC/ServiceForOtherSystem?wsdl"
    API_SOC_LST = ["http://10.60.129.78:8093/SOC/ServiceForOtherSystem?wsdl",
                   "http://10.60.129.78:8095/SOC/ServiceForOtherSystem?wsdl",
                   "http://10.60.129.78:8096/SOC/ServiceForOtherSystem?wsdl",
                   "http://10.60.129.78:8097/SOC/ServiceForOtherSystem?wsdl",
                   "http://10.60.129.78:8098/SOC/ServiceForOtherSystem?wsdl"]

    API_VMSA_LIST = "http://10.60.129.74:8888/MSChangeProcess/rest/DbTaskService/query2"
    API_VMSA_LIST_USERNAME = "vipa"
    API_VMSA_LIST_PASSWORD = "Vipa@123"
    API_SOC_CUSTOM_VIP_1 = "http://10.60.129.245:8093/SOC/ServiceForOtherSystem"
    #API_SOC_CUSTOM_VIP_1 = "http://10.60.129.75:8093/SOC/ServiceForOtherSystem?wsdl"
    API_SOC_CUSTOM_VIP_2 = "http://10.60.129.77:8093/SOC/ServiceForOtherSystem?wsdl"
    API_SOC_CUSTOM_VIP_3 = "http://10.60.129.78:8093/SOC/ServiceForOtherSystem?wsdl"
    API_SOC_CUSTOM_VIP_4 = "http://10.60.129.75:8094/SOC/ServiceForOtherSystem?wsdl"
    API_SOC_CUSTOM_VIP_5 = "http://10.60.129.75:8095/SOC/ServiceForOtherSystem?wsdl"
    API_SOC_CUSTOM_VIP_6 = "http://10.60.129.77:8094/SOC/ServiceForOtherSystem?wsdl"
    API_SOC_CUSTOM_VIP_7 = "http://10.60.129.77:8095/SOC/ServiceForOtherSystem?wsdl"
    API_SOC_CUSTOM_VIP_8 = "http://10.60.129.78:8094/SOC/ServiceForOtherSystem?wsdl"
    API_SOC_CUSTOM_VIP_9 = "http://10.60.129.78:8095/SOC/ServiceForOtherSystem?wsdl"

    # SERVER_NOCPRO = "10.60.94.151"
    # SERVER_NOCPRO_USERNAME = "chientx1"
    # SERVER_NOCPRO_PASSWORD = "chientx1"
    SERVER_NOCPRO = "10.60.94.13"
    SERVER_NOCPRO_PORT = "1521"
    SERVER_NOCPRO_SERVICE_NAME = "gatepro"
    SERVER_NOCPRO_USERNAME = "itbusiness_mss"
    SERVER_NOCPRO_PASSWORD = "Nocpro123"

    SERVER_NOCPRO_USERNAME2 = "tableau"
    SERVER_NOCPRO_PASSWORD2 = "Viettel#12358"

    SERVER_AOM = "10.60.127.57"
    SERVER_AOM_PORT = "1521"
    SERVER_AOM_SERVICE_NAME = "aom"
    SERVER_AOM_USERNAME = "gscntt"
    SERVER_AOM_PASSWORD = "A12345678Aa#321"

    SERVER_GNOC = "10.60.94.105"
    SERVER_GNOC_PORT = "1521"
    SERVER_GNOC_SERVICE_NAME = "GNOC"
    SERVER_GNOC_USERNAME = "tableau"
    SERVER_GNOC_PASSWORD = "Viettel#1235"

    SERVER_NPMS = "10.255.58.4"
    SERVER_NPMS_PORT = "3306"
    SERVER_NPMS_USERNAME = "npmsv2_tableau"
    SERVER_NPMS_PASSWORD = "E6@c$a57#b"



    SERVER_CBS = "10.30.8.18"
    SERVER_CBS_PORT = "1521"
    SERVER_CBS_SERVICE_NAME = "BIKT"
    SERVER_CBS_USERNAME = "tableau"
    SERVER_CBS_PASSWORD = "Viettel#1235"

    SERVER_PCTT = "10.255.65.7"
    SERVER_PCTT_PORT = "4006"
    SERVER_PCTT_SERVICE_NAME = "pctt_app"
    SERVER_PCTT_USERNAME = "pctt"
    SERVER_PCTT_PASSWORD = "changeme@123"

    API_SPRING_IPMS_SERVER = "192.168.251.15"
    API_SPRING_IPMS_USER = "minhnd"
    API_SPRING_IPMS_PASS = "minhnd@123"
    #API_SPRING_IPMS_PORT = 8686
    API_SPRING_IPMS_PORT = 8688
    #API_SPRING_IPMS_SERVER_TENANT = "kv1015ll"
    API_SPRING_IPMS_SERVER_TENANT = "kv1015"
    API_FLASK_PORT = 9110
    SERVER_HOST = "192.168.251.15"
    SERVER_HOST_USER = "linhlk1"
    SERVER_HOST_PASS = "linhlk135"
    SERVER_LOG_IP_LST = [dict(area="KV1", ip="192.168.251.30"),
                         dict(area="KV2", ip="192.168.98.232"),
                         dict(area="KV3", ip="10.73.224.71")]
    SERVER_LOG_USER = "linhlk1"
    SERVER_LOG_PASS = "linhlk1@135"

    TENANT_ID_LST = list()
    TENANT_ID_KV1026 = dict(ip="192.168.251.26", id="kv1026", type="traffic")
    TENANT_ID_KV1022 = dict(ip="192.168.251.22", id="kv1022", type="traffic")
    TENANT_ID_KV1025 = dict(ip="192.168.251.25", id="kv1025", type="traffic")
    TENANT_ID_KV1027 = dict(ip="192.168.251.27", id="kv1027", type="traffic")
    TENANT_ID_KV1029 = dict(ip="192.168.251.29", id="kv1029", type="traffic")
    TENANT_ID_KV1030 = dict(ip="192.168.251.30", id="kv1030", type="syslog")
    TENANT_ID_KV2239 = dict(ip="192.168.98.239", id="kv2239", type="traffic")
    TENANT_ID_KV2238 = dict(ip="192.168.98.238", id="kv2238", type="traffic")
    TENANT_ID_KV2232 = dict(ip="192.168.98.232", id="kv2232", type="syslog")
    TENANT_ID_KV3070 = dict(ip="10.73.224.70", id="kv3070", type="traffic")
    TENANT_ID_KV3073 = dict(ip="10.73.224.73", id="kv3073", type="traffic")
    TENANT_ID_KV3068 = dict(ip="10.73.224.68", id="kv3068", type="traffic")
    TENANT_ID_KV3065 = dict(ip="10.73.224.65", id="kv3065", type="syslog")
    TENANT_ID_KV3069 = dict(ip="10.73.224.69", id="kv3069", type="traffic")
    TENANT_ID_KV3072 = dict(ip="10.73.224.72", id="kv3072", type="traffic")
    TENANT_ID_KV1079 = dict(ip="10.60.60.79", id="kv1aaa079", type="traffic")
    TENANT_ID_KV2025 = dict(ip="10.42.50.25", id="kv2aaa025", type="traffic")
    TENANT_ID_KV3050 = dict(ip="10.73.66.50", id="kv3aaa050", type="traffic")
    TENANT_ID_TIM038 = dict(ip="10.226.44.38", id="tim038", type="traffic")
    TENANT_ID_TIM039 = dict(ip="10.226.44.39", id="tim039", type="traffic")
    TENANT_ID_CAM134 = dict(ip="10.79.37.134", id="cam134", type="traffic")
    TENANT_ID_CAM135 = dict(ip="10.79.37.135", id="cam135", type="traffic")
    TENANT_ID_CAM136 = dict(ip="10.79.37.136", id="cam136", type="traffic")
    TENANT_ID_CAM137 = dict(ip="10.79.37.137", id="cam137", type="traffic")
    TENANT_ID_MOZ022 = dict(ip="10.229.17.22", id="moz022", type="traffic")
    TENANT_ID_MOZ023 = dict(ip="10.229.17.23", id="moz023", type="traffic")
    TENANT_ID_MOZ024 = dict(ip="10.229.17.24", id="moz024", type="traffic")
    TENANT_ID_MOZ025 = dict(ip="10.229.17.25", id="moz025", type="traffic")
    TENANT_ID_HAI018 = dict(ip="10.228.120.18", id="hai018", type="traffic")
    TENANT_ID_PER070 = dict(ip="10.121.62.70", id="per070", type="traffic")
    TENANT_ID_PER071 = dict(ip="10.121.62.71", id="per071", type="traffic")
    TENANT_ID_BUR040 = dict(ip="10.225.8.40", id="bur040", type="traffic")
    TENANT_ID_BUR039 = dict(ip="10.225.8.39", id="bur039", type="traffic")
    TENANT_ID_BUR041 = dict(ip="10.225.8.41", id="bur041", type="traffic")
    TENANT_ID_LAO050 = dict(ip="10.78.18.50", id="lao050", type="traffic")
    TENANT_ID_LAO202 = dict(ip="10.120.8.202", id="lao202", type="traffic")

    AREA_IP_MAIN_LST = list()
    area_kv1 = dict(area="KV1", ip="192.168.251.26", username="linhlk1", password="linhlk1@135", db='ipms')
    area_kv2 = dict(area="KV2", ip="192.168.98.239", username="linhlk1", password="linhlk1@135", db='ipms')
    area_kv3 = dict(area="KV3", ip="10.73.224.68", username="linhlk1", password="linhlk1@135", db='ipms')
    area_tim = dict(area="TIM", ip="10.226.44.38", username="monitor", password="Mon1t0r@", db='ipms')
    area_cam = dict(area="CAM", ip="10.79.37.134", username="minhnd6", password="Metfone@123", db='ipms')
    area_moz = dict(area="MOZ", ip="10.229.17.22", username="minhnd6", password="Viettel@#2016", db='ipms')
    area_hai = dict(area="HAI", ip="10.228.120.18", username="linhlk1", password="QazQwe@123", db='ipms')
    area_per = dict(area="PER", ip="10.121.62.70", username="minhnd6", password="minhnd6", db='ipms')
    area_bur = dict(area="BUR", ip="10.225.8.40", username="linhlk1", password="linhlk1@135", db='ipms')
    area_lao = dict(area="LAO", ip="10.78.18.50", username="minhnd6", password="IpN0c19@", db='ipms')

    AREA_IP_SYSLOG_MAIN_LST = [dict(area="KV1", ip="192.168.251.30", username="linhlk1",
                                    password="linhlk1@135", db='openlog'),
                               dict(area="KV2", ip="192.168.98.232", username="linhlk1",
                                    password="linhlk1@135", db='openlog'),
                               dict(area="KV3", ip="10.73.224.65", username="linhlk1",
                                    password="linhlk1@135", db='openlog'),
                               ]

    AREA_IP_MAIN_LST.append(area_kv1)
    AREA_IP_MAIN_LST.append(area_kv2)
    AREA_IP_MAIN_LST.append(area_kv3)
    AREA_IP_MAIN_LST.append(area_tim)
    AREA_IP_MAIN_LST.append(area_cam)
    AREA_IP_MAIN_LST.append(area_moz)
    AREA_IP_MAIN_LST.append(area_hai)
    AREA_IP_MAIN_LST.append(area_per)
    AREA_IP_MAIN_LST.append(area_bur)

    TENANT_ID_LST.append(TENANT_ID_KV1026)
    TENANT_ID_LST.append(TENANT_ID_KV1022)
    TENANT_ID_LST.append(TENANT_ID_KV1025)
    TENANT_ID_LST.append(TENANT_ID_KV1027)
    TENANT_ID_LST.append(TENANT_ID_KV1029)
    TENANT_ID_LST.append(TENANT_ID_KV2239)
    TENANT_ID_LST.append(TENANT_ID_KV2238)
    TENANT_ID_LST.append(TENANT_ID_KV3070)
    TENANT_ID_LST.append(TENANT_ID_KV3073)
    TENANT_ID_LST.append(TENANT_ID_KV3068)
    TENANT_ID_LST.append(TENANT_ID_KV3069)
    TENANT_ID_LST.append(TENANT_ID_KV3072)
    TENANT_ID_LST.append(TENANT_ID_KV1079)
    TENANT_ID_LST.append(TENANT_ID_KV2025)
    TENANT_ID_LST.append(TENANT_ID_KV3050)
    TENANT_ID_LST.append(TENANT_ID_TIM038)
    TENANT_ID_LST.append(TENANT_ID_TIM039)
    TENANT_ID_LST.append(TENANT_ID_CAM134)
    TENANT_ID_LST.append(TENANT_ID_CAM135)
    TENANT_ID_LST.append(TENANT_ID_CAM136)
    TENANT_ID_LST.append(TENANT_ID_CAM137)
    TENANT_ID_LST.append(TENANT_ID_MOZ022)
    TENANT_ID_LST.append(TENANT_ID_MOZ023)
    TENANT_ID_LST.append(TENANT_ID_MOZ024)
    TENANT_ID_LST.append(TENANT_ID_MOZ025)
    TENANT_ID_LST.append(TENANT_ID_HAI018)
    TENANT_ID_LST.append(TENANT_ID_PER070)
    TENANT_ID_LST.append(TENANT_ID_PER071)
    TENANT_ID_LST.append(TENANT_ID_BUR040)
    TENANT_ID_LST.append(TENANT_ID_BUR039)
    TENANT_ID_LST.append(TENANT_ID_BUR041)
    TENANT_ID_LST.append(TENANT_ID_LAO050)
    TENANT_ID_LST.append(TENANT_ID_LAO202)
    TENANT_ID_LST.append(TENANT_ID_KV2232)
    TENANT_ID_LST.append(TENANT_ID_KV3065)
    TENANT_ID_LST.append(TENANT_ID_KV1030)

    FW_USERNAME = 'noc_ip'
    FW_PASSWORD = 'N0c_ip@321'
    FW_FTG_LST = [{'name': 'T5_VAS_FW_01', 'ip': '10.60.91.140', 'version': 5.2, 'vdoom': 'root', 'port': 'port33'},
                  {'name': 'HLC9106.IT.FG1500.FWPUB.01', 'ip': '10.240.180.212', 'version': 6.2, 'vdoom': 'IPBN', 'port': 'PUB_IPBN'},
                  {'name': 'GVM103.IT.FG1500.FWPUB.01', 'ip': '10.240.179.169', 'version': 6.2, 'vdoom': 'IPBN', 'port': 'PUB_IPBN'},
                  {'name': 'HLC9102_MAIL_FW01', 'ip': '10.240.128.145', 'version': 5.2, 'vdoom': 'root', 'port': 'port17'},
                  {'name': 'HLC9103_OnBox_01', 'ip': '10.30.150.193', 'version': 5.2, 'vdoom': 'root', 'port': 'wan2'},
                  {'name': 'HLC9106.IT.F1200.PUB.FW01', 'ip': '10.60.141.156', 'version': 5.2, 'vdoom': 'root', 'port': 'UPLINK_IPBN'}]

    BOT_ID = "520479193:AAFSOXbRo62EVO7iZjqOavhVRTxJDgAbh24"
    BOT_3_ID = "1289527114:AAGIwA0V8v_KvdHfdmGxk9RoVmQgZn0n1CQ" # hotline IP
    BOT_2_ID = "1302416019:AAGsv-OaRafsKCOCEGvrH0Erv5urHwfdP_o" #thunv5
    BOT_4_ID = "1378549900:AAG4YCt5sTk8XKpm3CK8QxJhlsWWtJz2Xco" # hotline SD
    BOT_5_ID = "1322303176:AAFaxCsHiRJ3p6Lhv2ZHdP2YoQgmhPcMlG8" # may 1986 ID
    # muon check id cua supergroup, add RawDataBot vao trong group. No se phun ra group id cua super group
    BOT_GROUP_ID = -250248326
    BOT_GROUP_CHECK_THREAD_ID = -446773630
    BOT_GROUP_UCTT_ID = -269566029
    BOT_GROUP_OVER_THRESHOLD_ID = -377409831
    BOT_GROUP_CRC_ID = -345980469
    BOT_GROUP_INTERNET_VN = -222169021
    BOT_DENY_WEB = -366864415
    BOT_GROUP_MONITOR_ID = -218379033
    BOT_GROUP_VIP_CDBR_ID = -342861279
    BOT_GROUP_IP_CDBR_KV1 = -1001477941204
    BOT_GROUP_IP_CDBR_KV2 = -1001380317024
    BOT_GROUP_IP_CDBR_KV3 = -1001451842632
    BOT_GROUP_IP_CDBR = -1001461530509
    BOT_GROUP_CNTT_ID = -327859229
    BOT_GROUP_BSS_ID = -314029679
    # Bot tram nhay cell nhay
    BOT_GROUP_BSS_ACCESS_ID = -384213289
    BOT_GROUP_CHECK = -185131084
    BOT_GROUP_CSKH_ID = -474842241
    BOT_GROUP_SUPER_VIP = -442660649
    BOT_GROUP_IP_OVERTHRESHOLD_INTERNATIONAL = -452216446
    BOT_GROUP_ACCESS_ALARM_STATION_ID = -461697506
    BOT_GROUP_CHECK_WEB_DENY = -442783674
    #BOT_GROUP_VIP_KHDN_ID = -449216417
    BOT_GROUP_VIP_KHDN_ID = -1001190668403
    BOT_GROUP_TEST_IP_TEAM_ID = -1001433336299
    BOT_GROUP_GIAM_SAT = -1001290990716

    BOT_GROUP_WHITELIST = [BOT_GROUP_ID, BOT_GROUP_OVER_THRESHOLD_ID, BOT_GROUP_CRC_ID, BOT_GROUP_MONITOR_ID,
                           BOT_GROUP_BSS_ID, BOT_GROUP_IP_OVERTHRESHOLD_INTERNATIONAL, BOT_GROUP_UCTT_ID,
                           BOT_GROUP_INTERNET_VN, BOT_GROUP_GIAM_SAT]
    BOT_GROUP_GREY_LIST = [BOT_GROUP_CHECK, BOT_GROUP_BSS_ACCESS_ID, BOT_GROUP_IP_CDBR,
                           BOT_GROUP_ACCESS_ALARM_STATION_ID, BOT_GROUP_TEST_IP_TEAM_ID]
    BOT_GROUP_CHECK_CUSTOMER_LIST = [BOT_GROUP_CSKH_ID, BOT_GROUP_SUPER_VIP, BOT_GROUP_VIP_KHDN_ID]

    BOT_GROUP_RESET_LST = [BOT_GROUP_IP_CDBR]

    BOT_GROUP_SECRET_LIST = [BOT_GROUP_CHECK_WEB_DENY]

    USER_TACCAS = 'thongplh'
    PASS_LOCAL = 'alehap@123'
    PASS_TACCAS = 'Nghi@123'

    USER_TACCAS_2 = 'minhnd6'
    PASS_TACCAS_2 = 'Qazwsx@123'

    API_BOT_ID = 1140292
    API_BOT_HASH = 'bbe8edaf7c5687dc7045ed4f6cd8335f'
    API_ACCESS_LST = [dict(ip='10.74.225.110', type='kv3_acc_srt', area='KV3'),
                      dict(ip='10.74.225.111', type='kv3_acc_olt', area='KV3'),
                      dict(ip='10.58.65.185', type='kv1_acc_olt', area='KV1'),

                      dict(ip='192.168.251.26', type='kv1_agg', area='KV1'),
                      dict(ip='192.168.98.239', type='kv2_agg', area='KV2'),
                      dict(ip='10.73.224.70', type='kv3_agg', area='KV3'),

                      dict(ip='10.41.6.78', type='kv2_acc_olt', area='KV2'),

                      dict(ip='192.168.251.26', type='kv1_core', area='KV1'),
                      dict(ip='192.168.98.239', type='kv2_core', area='KV2'),
                      dict(ip='10.73.224.68', type='kv3_core', area='KV3'),

                      dict(ip='10.225.8.40', type='bur_core', area='BUR'),
                      dict(ip='10.228.120.18', type='hai_core', area='HAI'),
                      dict(ip='10.121.62.70', type='per_core', area='PER'),
                      dict(ip='10.79.37.134', type='cam_core', area='CAM'),
                      dict(ip='10.229.17.22', type='moz_core', area='MOZ'),
                      dict(ip='10.201.23.164', type='myt_core', area='MYT'),
                      dict(ip='10.226.44.38', type='tim_core', area='TIM'),
                      dict(ip='10.78.18.50', type='lao_core', area='LAO')
                      ]

    GROUP_ID_KPI_REPORT_LST = [
        dict(area='KV1', group_id=18921, group_name='FOIP_KV1_KPI_CTPECV-CKV', type='kv1_core'),
        dict(area='KV1', group_id=18926, group_name='FOIP_KV1_IPBN_MPBN_CORE_CPU', type='kv1_core'),
        dict(area='KV1', group_id=18931, group_name='FOIP_KV1_IPBN_MPBN_CORE_MEM', type='kv1_core'),
        dict(area='KV1', group_id=18531, group_name='FOIP_KV1_KPI_PE_P1_Minhnd6', type='kv1_core'),
        dict(area='KV1', group_id=19146, group_name='FOIP_KV1_FW_VASCP_SESSION', type='kv1_core'),
        dict(area='KV1', group_id=19151, group_name='FOIP_KV1_FW_VASCP_CPU', type='kv1_core'),
        dict(area='KV1', group_id=19156, group_name='FOIP_KV1_FW_VASCP_MEM', type='kv1_core'),
        dict(area='KV1', group_id=19141, group_name='FOIP_KV1_IPBN_BB_NOIKV', type='kv1_core'),

        dict(area='KV2', group_id=6338, group_name='FOIP_KV2_KPI_CTPECV-CKV', type='kv2_core'),
        dict(area='KV2', group_id=6343, group_name='FOIP_KV2_IPBN_MPBN_CORE_CPU', type='kv2_core'),
        dict(area='KV2', group_id=6348, group_name='FOIP_KV2_IPBN_MPBN_CORE_MEM', type='kv2_core'),
        dict(area='KV2', group_id=6298, group_name='FOIP_KV2_KPI_PE_P1_Minhnd6', type='kv2_core'),

        dict(area='KV3', group_id=24903, group_name='FOIP_KV3_KPI_DOMESTIC_MINHND', type='kv3_core'),
        dict(area='KV3', group_id=24975, group_name='FOIP_KV3_IPBN_MPBN_CORE_CPU', type='kv3_core'),
        dict(area='KV3', group_id=24981, group_name='FOIP_KV3_IPBN_MPBN_CORE_MEM', type='kv3_core'),
        dict(area='KV3', group_id=24969, group_name='FOIP_KV3_KPI_CTPECV-CKV', type='kv3_core'),
        dict(area='KV3', group_id=21543, group_name='Backbone B-N', type='kv3_core'),
        dict(area='KV3', group_id=21555, group_name='Backbone B-T', type='kv3_core'),
        dict(area='KV3', group_id=21549, group_name='Backbone T-N', type='kv3_core'),
        dict(area='KV3', group_id=24627, group_name='FOIP_KV3_KPI_PE_P1_Minhnd6', type='kv3_core')

    ]


class Production(Config):
    DEBUG = False
    # MONGO_DATABASE_SERVER = os.environ.get('PROD_DATABASE_SERVER') or 'localhost'
    # MONGO_DATABASE_PORT = os.environ.get('PROD_DATABASE_PORT') or 27017
    # MONGO_DATABASE_NAME = os.environ.get('PROD_DATABASE_NAME') or 'PROD_NET4CODE_SECURITY'


config = {
    'development': Development,
    'production': Production,
    'default': Development
}