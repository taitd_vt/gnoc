# -*- coding: utf-8 -*-
# This bot was made specifically for the pyTelegramAPI Telegram chat,
# and goes by the name 'TeleBot (@pyTeleBot)'. Join our group to talk to him!
# WARNING: Tested with Python 2.7

import os
import logging
import uuid
import telebot
import time
import cx_Oracle
from config import Config, Development
from core.threading.device_thread.dev_show_interface_crc_one_thread import DeviceShowInterfaceCrc
from core.threading.device_thread.dev_show_interface_one_drop import DeviceShowInterfaceOneDrop
from core.helpers.date_helpers import get_time_format_now, get_date_minus, get_date_minus_format_elk, \
    convert_date_str_to_date_obj_spring, get_date_now, convert_date_to_epoch, get_date_yesterday_format_ipms, \
    get_only_date_now_format_ipms, convert_date_obj_to_date_str_ipms
from core.helpers.stringhelpers import clean_html_str
from app.api.elasticsearch.api_kibana import ApiKibana
from pygooglechart import XYLineChart
from pygooglechart import Chart
from pygooglechart import SimpleLineChart
from pygooglechart import XYLineChart
from pygooglechart import SparkLineChart
from pygooglechart import PieChart2D
from pygooglechart import Axis
from config import Config, Development
from core.database.impl.nocpro.tbl_gpon_olt_sub_impl import TblGponOltSubImpl
from core.database.impl.server_minhnd.tbl_interface_drop_impl import TblInterfaceDropImpl
from core.database.impl.server_minhnd.server_mysql_connect import ServerMysqlConnect
import time
from core.helpers.stringhelpers import get_province_bras_name, get_province_type, divide_str, \
    get_mess_filter_between_char, get_mess_filter
from core.database.impl.server_minhnd.tbl_useronline_bras_impl import TblUseronlineBrasImpl
from core.helpers.date_helpers import get_time_format_now, get_date_minus, get_date_minus_format_elk, \
    convert_date_str_to_date_obj_spring, get_date_now, convert_date_to_epoch, convert_date_to_ipms
from core.helpers.int_helpers import convert_string_to_int
from core.helpers.list_helpers import get_filter_in_list
from core.helpers.list_helpers import chunk_node_str_to_lst
from core.helpers.tenant_helpers import get_tenant_id_from_area, get_tenant_id_syslog_from_area
from core.helpers.fix_helper import get_area, get_area_from_acc
from core.database.impl.nocpro.tbl_gpon_olt_sub_impl import TblGponOltSubImpl
from core.database.impl.tbl_online_aaa_impl import TblOnlineAaaImpl
from core.database.impl.nocpro.tbl_bss_core_impl import TblBssCoreImpl
from core.database.impl.ipms.host_ipms_impl import HostIpmsImpl
from core.database.impl.ipms.syslog_nocpro_impl import SyslogNocproIpmsImpl
from core.database.impl.bot.bot_telethon_impl import BotTelethonImpl
from core.database.impl.gnoc.tbl_syslog_gnoc_impl import TblSyslogGnocImpl
from core.database.impl.bot.bot_impl import BotImpl
from core.database.impl.server_minhnd.tbl_cable_backbone import TblCableBackboneImpl

# from telegram import ParseMode chac version minh bi loi
text_messages = {
    'welcome':
        u'Please welcome {name}!\n\n'
        u'This chat is intended for questions about and discussion of the pyTelegramBotAPI.\n'
        u'To enable group members to answer your questions fast and accurately, please make sure to study the '
        u'project\'s documentation (https://github.com/eternnoir/pyTelegramBotAPI/blob/master/README.md) and the '
        u'examples (https://github.com/eternnoir/pyTelegramBotAPI/tree/master/examples) first.\n\n'
        u'I hope you enjoy your stay here!',

    'info':
        u'My name is TeleBot,\n'
        u'I am a bot that assists these wonderful bot-creating people of this bot library group chat.\n'
        u'Also, I am still under development. Please improve my functionality by making a pull request! '
        u'Suggestions are also welcome, just drop them in this group chat!',

    'wrong_chat':
        u'Hi there!\nThanks for trying me out. However, this bot can only be used in the pyTelegramAPI group chat.\n'
        u'Join us!\n\n'
        u'https://telegram.me/joinchat/067e22c60035523fda8f6025ee87e30b'
}
# 123
config = Development()
BOT_ID = config.__getattribute__('BOT_3_ID')
BOT_GROUP_ID = config.__getattribute__('BOT_GROUP_ID')
TELEBOT_BOT_TOKEN = BOT_ID
GROUP_CHAT_ID = int(BOT_GROUP_ID)
BOT_GROUP_WHITELIST = config.__getattribute__('BOT_GROUP_WHITELIST')
BOT_GROUP_GREY_LIST = config.__getattribute__('BOT_GROUP_GREY_LIST')
BOT_GROUP_RESET_LST = config.__getattribute__('BOT_GROUP_RESET_LST')
BOT_GROUP_ID = -218379033
BOT_GROUP_CNTT_ID = config.__getattribute__('BOT_GROUP_CNTT_ID')
SERVER_NOCPRO = config.__getattribute__('SERVER_NOCPRO')
SERVER_NOCPRO_PORT = config.__getattribute__('SERVER_NOCPRO_PORT')
SERVER_NOCPRO_SERVICE_NAME = config.__getattribute__('SERVER_NOCPRO_SERVICE_NAME')
SERVER_NOCPRO_USERNAME = config.__getattribute__('SERVER_NOCPRO_USERNAME')
SERVER_NOCPRO_PASSWORD = config.__getattribute__('SERVER_NOCPRO_PASSWORD')
SERVER_GNOC = config.__getattribute__('SERVER_GNOC')
SERVER_GNOC_PORT = config.__getattribute__('SERVER_GNOC_PORT')
SERVER_GNOC_SERVICE_NAME = config.__getattribute__('SERVER_GNOC_SERVICE_NAME')
SERVER_GNOC_USERNAME = config.__getattribute__('SERVER_GNOC_USERNAME')
SERVER_GNOC_PASSWORD = config.__getattribute__('SERVER_GNOC_PASSWORD')

SERVER_NOCPRO_USERNAME2 = config.__getattribute__('SERVER_NOCPRO_USERNAME2')
SERVER_NOCPRO_PASSWORD2 = config.__getattribute__('SERVER_NOCPRO_PASSWORD2')
SERVER_AOM = config.__getattribute__('SERVER_AOM')
SERVER_AOM_PORT = config.__getattribute__('SERVER_AOM_PORT')
SERVER_AOM_SERVICE_NAME = config.__getattribute__('SERVER_AOM_SERVICE_NAME')
SERVER_AOM_USERNAME = config.__getattribute__('SERVER_AOM_USERNAME')
SERVER_AOM_PASSWORD = config.__getattribute__('SERVER_AOM_PASSWORD')
BOT_GROUP_VIP_CDBR_ID = config.__getattribute__('BOT_GROUP_VIP_CDBR_ID')
BOT_GROUP_IP_CDBR_KV1 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV1')
BOT_GROUP_IP_CDBR_KV2 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV2')
BOT_GROUP_IP_CDBR_KV3 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV3')
BOT_GROUP_CHECK_CUSTOMER_LIST = config.__getattribute__('BOT_GROUP_CHECK_CUSTOMER_LIST')
BOT_GROUP_SECRET_LIST = config.__getattribute__('BOT_GROUP_SECRET_LIST')
BOT_GROUP_BSS_ID = config.__getattribute__('BOT_GROUP_BSS_ID')
SERVER_CBS = config.__getattribute__('SERVER_CBS')
SERVER_CBS_PORT = config.__getattribute__('SERVER_CBS_PORT')
SERVER_CBS_SERVICE_NAME = config.__getattribute__('SERVER_CBS_SERVICE_NAME')
SERVER_CBS_USERNAME = config.__getattribute__('SERVER_CBS_USERNAME')
SERVER_CBS_PASSWORD = config.__getattribute__('SERVER_CBS_PASSWORD')
BOT_GROUP_CRC_ID = config.__getattribute__('BOT_GROUP_CRC_ID')

dsn_str = cx_Oracle.makedsn(SERVER_NOCPRO, SERVER_NOCPRO_PORT, service_name=SERVER_NOCPRO_SERVICE_NAME)
dsn_cbs_str = cx_Oracle.makedsn(SERVER_CBS, SERVER_CBS_PORT, service_name=SERVER_CBS_SERVICE_NAME)
dsn_gnoc_str = cx_Oracle.makedsn(SERVER_GNOC, SERVER_GNOC_PORT, service_name=SERVER_GNOC_SERVICE_NAME)

pool_nocpro = cx_Oracle.SessionPool(min=1,
                                    max=10, increment=1, threaded=True, dsn=dsn_str,
                                    user=SERVER_NOCPRO_USERNAME, password=SERVER_NOCPRO_PASSWORD,
                                    encoding='UTF-8', nencoding='UTF-8')

pool_nocpro_tableau = cx_Oracle.SessionPool(min=1,
                                            max=10, increment=1, threaded=True, dsn=dsn_str,
                                            user=SERVER_NOCPRO_USERNAME2, password=SERVER_NOCPRO_PASSWORD2,
                                            encoding='UTF-8', nencoding='UTF-8')

pool_cbs = cx_Oracle.SessionPool(min=1,
                                 max=10, increment=1, threaded=True, dsn=dsn_cbs_str,
                                 user=SERVER_CBS_USERNAME, password=SERVER_CBS_PASSWORD,
                                 encoding='UTF-8', nencoding='UTF-8')

pool_gnoc = cx_Oracle.SessionPool(min=1,
                                  max=4, increment=1, threaded=True, dsn=dsn_gnoc_str,
                                  user=SERVER_GNOC_USERNAME, password=SERVER_GNOC_PASSWORD,
                                  encoding='UTF-8', nencoding='UTF-8')

# pool_cbs = None

dbconfig = {
    "host": "192.168.251.15",
    "port": 3306,
    "user": "linhlk1",
    "password": "linhlk135",
    "database": "luuluong",
}
mysql_pool = ServerMysqlConnect(**dbconfig)
logging.basicConfig(level=logging.INFO, datefmt='%H:%M:%S',
                    format='%(asctime)s [%(name)s]'
                           ' %(levelname)s: %(message)s')

logging.basicConfig(level=logging.ERROR, datefmt='%H:%M:%S',
                    format='%(asctime)s [%(name)s]'
                           ' %(levelname)s: %(message)s'
                    )


def is_secretlist(chat_id):
    if chat_id in BOT_GROUP_SECRET_LIST:
        return True
    else:
        return False


def is_whitelist(chat_id):
    if chat_id in BOT_GROUP_WHITELIST:
        return True
    else:
        return False


def is_resetlist(chat_id):
    if chat_id in BOT_GROUP_RESET_LST:
        return True
    else:
        return False


def is_greylist(chat_id):
    if chat_id in BOT_GROUP_GREY_LIST:
        return True
    else:
        return False


def is_custom_lst(chat_id):
    if chat_id in BOT_GROUP_CHECK_CUSTOMER_LIST:
        return True
    else:
        return False


def is_api_group(chat_id):
    return chat_id == GROUP_CHAT_ID


# bot = telebot.TeleBot(BOT_ID, True)
bot = telebot.TeleBot(BOT_ID)


@bot.message_handler(func=lambda m: True, content_types=['new_chat_participant'])
def on_user_joins(message):
    if not is_api_group(message.chat.id):
        return

    name = message.new_chat_participant.first_name
    if hasattr(message.new_chat_participant, 'last_name') and message.new_chat_participant.last_name is not None:
        name += u" {}".format(message.new_chat_participant.last_name)

    if hasattr(message.new_chat_participant, 'username') and message.new_chat_participant.username is not None:
        name += u" (@{})".format(message.new_chat_participant.username)

    bot.reply_to(message, text_messages['welcome'].format(name=name))


@bot.message_handler(commands=['info', 'help'])
def on_info(message):
    if not is_api_group(message.chat.id) and not is_whitelist(message.chat.id) \
            and not is_greylist(message.chat.id):
        bot.reply_to(message, text_messages['wrong_chat'])
        return
    res_info = "Danh sách lệnh: \n"
    res_info += "Kiểm tra CRC của 1 interface của 1 node /checkcrc Node_IP Interface Time_Between_2_time_check\n"
    res_info += "Kiểm tra interface của 1 node có bị drop không /checkdrop Node_IP Interface \n"
    res_info += "Kiểm tra Lưu lượng backhaul và thực dùng 24h /checkbackhaul\n"
    res_info += "Kiểm tra Lưu lượng backhaul và thực dùng bây giờ /checkbackhaul now\n"
    res_info += "Kiểm tra Lưu lượng backbone và thực dùng bây giờ /checkbackbone now\n"
    res_info += "Kiểm tra Lưu lượng backbone và thực dùng bây giờ khi down cáp /checkbackbonedown 1C,2B now\n"
    res_info += "Kiểm tra 1 node có bao nhiêu khách hàng OLT /checknode Node_Name\n"
    res_info += "Kiểm tra KPI của 1 node core di động  /kpi Node_Name \n"
    res_info += "Kiểm tra KPI của 1 node 2G  /kpi2G Node_Name \n"
    res_info += "Kiểm tra KPI của 1 node 3G  /kpi3G Node_Name \n"
    res_info += "Kiểm tra KPI của 1 node 4G  /kpi4G Node_Name \n"
    res_info += "Kiểm tra MCA của 1 node 2G  /checkmca 2G Node_Name \n"
    res_info += "Kiểm tra MCA của 1 node 3G  /checkmca 3G Node_Name \n"
    res_info += "Kiểm tra MCA của 1 node 4G  /checkmca 4G Node_Name \n"
    res_info += "Kiểm tra lưu lượng của 1 BSC  /bsc_traffic Node_Name \n"
    res_info += "Kiểm tra cảnh báo của 1 node core di động  /m_core Node_Name mức_độ (VD /m_core GSHT05 Critical \n"
    res_info += "Kiểm tra  CPU, MEM của 1 node core di động  /m_per Node_Name (VD /m_per GSHT05 \n"
    res_info += "Kiểm tra cảnh báo của 1 cell , 1 cabinet di động  /cabinet, /cell, /station Node_Name (VD /cabinet 3HD115 hoac /cell 3KG4414\n"
    res_info += "Kiểm tra cảnh báo của 1 cell lien ket, 1 cabinet lien ket di động  /cabinet_relate, /cell_relate Node_Name (VD /cabinet_relate 3HD115 hoac /cell_relate 3KG4414\n"
    res_info += "Kiểm tra WO cảnh báo của 1 cell, 1 cabinet /cabinet_info, /cell_info Node_Name (VD /cabinet_info 3HD115 hoac /cell_info 3KG4414\n"
    res_info += "Kiểm tra mạng di động có bao nhiêu cảnh báo /checktotalmobile \n"
    res_info += "Tìm trên IPMS danh sách node mạng theo tên /checknodename Node_name_need_check(PDL&CKV)\n"
    res_info += "Tìm trên IPMS danh sách CBS của 1 node /checknodealarm Node_name_need_check(PDL&CKV)\n"
    res_info += "Tìm cảnh báo thứ cấp của 1 node di động /checknodealarm1st Node_name \n"
    res_info += "Vẽ biểu đồ số useronline của 1 tỉnh hoặc 1 thiết bị /checkonline Province(or node_bras)\n"
    res_info += "Kiểm tra user có online hay không /checkuser username\n"
    res_info += "Kiểm tra thu phát của user /checkrx username\n"
    res_info += "Kiểm tra thu phát, CRC, BW của user GPON /checklistacc username\n"
    res_info += "Kiểm tra account truyền hình /checktv username\n"
    res_info += "Kiểm tra kế hoạch UCTT VD /uctt thị_trường node hiện_tượng \n"
    res_info += "Kiểm tra kế hoạch Điều hành UCTT VD /dhuctt hiện_tượng hoac /kbdh hiện_tượng \n"
    res_info += "Kiểm tra trang web VD /checkweb google.com \n"
    res_info += "Kiểm tra thông tin MSC VD /msc_info MSHL04 \n"
    res_info += "Kiểm tra thông tin BSC RNC /bscrnc_info RCHL27 \n"
    res_info += "Kiểm tra thuê bao theo tỉnh VD /sub_province HNI \n"
    res_info += "Kiểm tra thông tin vị trí cell VD /cell_location 3HN0101 \n"
    res_info += "Kiểm tra SiteRouter Tinh Huyen VD /srt TTH Phu Vang\n"
    res_info += "Kiểm tra OLT tinh Huyen down VD /olt TTH Phu Vang\n"
    res_info += "Kiểm tra SW Tinh Huyen down VD /sw TTH Phu Vang\n"
    res_info += "Kiểm tra Tram Huyen down VD /tramdown TTH Phu Vang\n"
    res_info += "Kiểm tra Tu Huyen down VD /tudown TTH Phu Vang\n"
    res_info += "Kiểm tra Trạm mất điện VD /md HNI Cầu Giấy\n"
    res_info += "Kiểm tra Trạm trục mất điện VD /mdtt HNI Cầu Giấy\n"
    res_info += "Kiểm tra Trạm nhiệt độ cao VD /ndc HNI Cầu Giấy\\n"
    res_info += "Kiểm tra Trạm trục nhiệt độ cao VD /ndctt HNI Cầu Giấy\n"
    res_info += "Kiểm tra Trạm trục mở cửa VD /mctt HNI Cầu Giấy\n"
    res_info += "Kiểm tra danh sách account của trạm VD /listinternet LSN0435 Số_lượng_account_cần_list\\n"
    res_info += "Kiểm tra danh sách node down trong vòng Ring VD /ringsrt LSN0435 \\n"
    res_info += "Reset account GPON VD /resetport h004_gftth_milano \\n"
    res_info += "Kiểm tra các vị trí trạm down VD /vitridown QNM \\n"
    res_info += "Kiểm tra tình trạng cr mapping VD /cr_nomap  or /cr_nomap_ahdv \\n"

    bot.reply_to(message, res_info)


@bot.message_handler(commands=["ping"])
def on_ping(message):
    bot.reply_to(message, "Still alive and kicking New!")


@bot.message_handler(commands=['start'])
def on_start(message):
    if not is_api_group(message.chat.id):
        bot.reply_to(message, text_messages['wrong_chat'])
        return


@bot.message_handler(commands=['checkcrc'])
def check_crc(message):
    try:

        if is_whitelist(message.chat.id):
            words = message.text.split()
            res_crc = ''
            n = len(words)
            # cu phap se la node
            date_time_now = get_time_format_now()
            time_wait = 0
            if n >= 2:
                node_ip = words[1]
                node_intf = words[2]

                chat_id = message.chat.id

                _dev_crc = DeviceShowInterfaceCrc(False, date_time_now, node_ip, node_intf,
                                                  '', 'Checking CRC')
                res_crc, crc_num = _dev_crc.run()
                print(res_crc)
                bot.reply_to(message, str(res_crc))
                if n >= 3:
                    time_wait_str = words[3]
                else:
                    time_wait_str = '300'
                time_wait_int = convert_string_to_int(time_wait_str)
                if time_wait_int > 0:
                    time.sleep(time_wait_int)
                    _dev_crc_2 = DeviceShowInterfaceCrc(False, date_time_now, node_ip, node_intf,
                                                        '', 'Checking CRC')
                    res_crc_2, crc_num_2 = _dev_crc.run()
                    crc_chg = crc_num_2 - crc_num
                    res_crc_2 += "\n Total change: " + str(crc_chg)
                    bot.reply_to(message, str(res_crc_2))

            else:
                res_crc = "Error Syntax.Please ask:/checkcrc node_ip interface time_sleep_between_next_check"
                bot.reply_to(message, str(res_crc))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['checkdrop'])
def check_drop(message):
    try:

        # chu y khi chinh sua tren visualize can check luon va chinh sua API tren api_kibana theo link
        if is_whitelist(message.chat.id):
            _now = get_date_now()
            _now_only_date = get_only_date_now_format_ipms()
            _ystd_only_date = get_date_yesterday_format_ipms()

            words = message.text.split()
            res_drop = ''
            n = len(words)
            if n >= 2:
                time_range = words[1]
                if time_range.upper() == 'NOW':
                    # lay thoi gian tu 60p - 30p truoc do elasticsearch tre 15p moi co ket qua
                    # gio tren elasticsearch la gio GMT
                    _now_end = get_date_minus(_now, -7 * 60 + 30)
                    _now_bgn = get_date_minus(_now, - 7 * 60 + 60)
                    res_info = "Total Drop Now: \n"

                else:
                    _now_end = get_date_minus(_now, -7 * 60)
                    _now_bgn = get_date_minus(_now, 17 * 60)
                    res_info = "Max Drop Last 24h: \n"
            else:
                _now_end = get_date_minus(_now, -7 * 60)
                _now_bgn = get_date_minus(_now, 17 * 60)
                res_info = "Max Drop Last 24h: \n"

            _now_end_epch = int(convert_date_to_epoch(_now_end) * 1000)
            _now_bgn_epch = int(convert_date_to_epoch(_now_bgn) * 1000)

            # tinh toan drop backhaul
            if n >= 2:
                time_range = words[1]
                if time_range.upper() == 'NOW':
                    _tbl = TblInterfaceDropImpl(_now_only_date)
                    try:
                        res = mysql_pool.execute(_tbl.get_drop_now())
                        if res:
                            time_drop = res[0]['time_frm']

                            _tbl_now = TblInterfaceDropImpl(time_drop)
                            res_lst = mysql_pool.execute(_tbl_now.get_total_drop_node())
                            time_drop = ''
                            ttl_all_drop = 0
                            for res in res_lst:
                                try:
                                    time_drop = res['timestamp']
                                    node_name = res['node_name']
                                    ttl_drop = res['total']
                                    if ttl_drop > 0:
                                        res_info += "Total Drop: " + str(ttl_drop) + " Mbps on " + str(node_name) + "\n"
                                        ttl_all_drop += ttl_drop
                                except Exception as err:
                                    print("Error %s when get total drop now" % err)

                            if time_drop:
                                res_info += "Total Drop: " + str(round(ttl_all_drop, 2)) + " Mbps " + "\n"
                                res_info += "Time: " + str(time_drop)

                    except Exception as err:
                        print("error %s when get drop now" % err)
            else:
                _tbl = TblInterfaceDropImpl(_ystd_only_date)
                # lay time co drop max truoc sau do lay time va select gio day ra
                res = mysql_pool.execute(_tbl.get_drop_max())
                if res:
                    try:
                        time_drop = res[0]['time_frm']
                        _tbl_now = TblInterfaceDropImpl(time_drop)
                        res_lst = mysql_pool.execute(_tbl_now.get_total_drop_node())
                        ttl_all_drop = 0
                        for res in res_lst:
                            time_drop_new = res['timestamp']
                            node_name = res['node_name']
                            ttl_drop = res['total']
                            if ttl_drop > 0:
                                res_info += "Total Drop: " + str(ttl_drop) + " Mbps on " + str(node_name) + "\n"
                                ttl_all_drop += ttl_drop

                        if time_drop:
                            res_info += "Total Drop: " + str(round(ttl_all_drop, 2)) + " Mbps " + "\n"
                            res_info += "Time: " + str(time_drop)
                    except Exception as err:
                        print("Error when get time of drop")
                        res_info += str(err)
            bot.reply_to(message, str(res_info))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['checkbackbone'])
def check_backbone(message):
    try:
        print("Message receive: " + str(message.text))
        # chu y khi chinh sua tren visualize can check luon va chinh sua API tren api_kibana theo link
        if is_whitelist(message.chat.id):
            _now = get_date_now()
            _now_only_date = get_only_date_now_format_ipms()
            _ystd_only_date = get_date_yesterday_format_ipms()
            _bot_telethon_impl = BotTelethonImpl(None,
                                                 pool_nocpro=pool_nocpro,
                                                 pool_nocpro_tableau=pool_nocpro_tableau,
                                                 pool_cbs=pool_cbs,
                                                 mysql_pool=mysql_pool,
                                                 gnoc_pool=pool_gnoc)
            words = message.text.split()

            n = len(words)
            if n >= 2:
                time_range = words[1]
                if time_range.upper() == 'NOW':
                    # lay thoi gian tu 60p - 30p truoc do elasticsearch tre 15p moi co ket qua
                    # gio tren elasticsearch la gio GMT
                    _now_end = get_date_minus(_now, -7 * 60 + 30)
                    _now_bgn = get_date_minus(_now, - 7 * 60 + 60)
                    res_info = _bot_telethon_impl.get_backbone_detail(message.text)

                else:
                    _now_end = get_date_minus(_now, -7 * 60)
                    _now_bgn = get_date_minus(_now, 17 * 60)
                    res_info = _bot_telethon_impl.get_backbone_detail(message.text)
            else:
                _now_end = get_date_minus(_now, -7 * 60)
                _now_bgn = get_date_minus(_now, 17 * 60)
                res_info = _bot_telethon_impl.get_backbone_detail(message.text)
            logging.info(str(res_info))
            bot.reply_to(message, str(res_info))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['checkbackbonedown'])
def check_backbone_down(message):
    try:
        print("Message receive: " + str(message.text))
        # chu y khi chinh sua tren visualize can check luon va chinh sua API tren api_kibana theo link
        if is_whitelist(message.chat.id):
            _now = get_date_now()
            _now_only_date = get_only_date_now_format_ipms()
            _ystd_only_date = get_date_yesterday_format_ipms()
            _bot_telethon_impl = BotTelethonImpl(None,
                                                 pool_nocpro=pool_nocpro,
                                                 pool_nocpro_tableau=pool_nocpro_tableau,
                                                 pool_cbs=pool_cbs,
                                                 mysql_pool=mysql_pool,
                                                 gnoc_pool=pool_gnoc)
            words = message.text.split()

            n = len(words)
            if n >= 2:
                time_range = words[1]
                if time_range.upper() == 'NOW':
                    # lay thoi gian tu 60p - 30p truoc do elasticsearch tre 15p moi co ket qua
                    # gio tren elasticsearch la gio GMT
                    _now_end = get_date_minus(_now, -7 * 60 + 30)
                    _now_bgn = get_date_minus(_now, - 7 * 60 + 60)
                    res_info = _bot_telethon_impl.get_backbone_detail_when_cable_down(message.text)

                else:
                    _now_end = get_date_minus(_now, -7 * 60)
                    _now_bgn = get_date_minus(_now, 17 * 60)
                    res_info = _bot_telethon_impl.get_backbone_detail_when_cable_down(message.text)
            else:
                _now_end = get_date_minus(_now, -7 * 60)
                _now_bgn = get_date_minus(_now, 17 * 60)
                res_info = _bot_telethon_impl.get_backbone_detail_when_cable_down(message.text)
            logging.info(str(res_info))
            bot.reply_to(message, str(res_info))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['bsc_traffic'])
def check_bsc_traffic(message):
    try:
        print("Message receive: " + str(message.text))
        # chu y khi chinh sua tren visualize can check luon va chinh sua API tren api_kibana theo link
        if is_whitelist(message.chat.id):
            _now = get_date_now()
            _now_only_date = get_only_date_now_format_ipms()
            _ystd_only_date = get_date_yesterday_format_ipms()
            _bot_telethon_impl = BotTelethonImpl(None,
                                                 pool_nocpro=pool_nocpro,
                                                 pool_nocpro_tableau=pool_nocpro_tableau,
                                                 pool_cbs=pool_cbs,
                                                 mysql_pool=mysql_pool,
                                                 gnoc_pool=pool_gnoc)
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]
            words = word.split()

            n = len(words)
            if n >= 2:
                res_info = _bot_telethon_impl.check_bsc_traffic(word.upper(), key_fnd=words_find)
                # res_info = get_mess_filter_between_char(res_info, "\n", str(words_find).upper())
                if str(res_info).strip() != '':
                    bot.reply_to(message, res_info)
                else:
                    bot.reply_to(message, "No result")
            else:
                bot.reply_to(message, "Wrong syntax Example: /bsc_traffic BCPD01")

        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of bsc traffic bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['cr_nomap', 'cr_nomap_ahdv'])
def check_cr_map(message):
    try:
        print("Message receive: " + str(message.text))
        # chu y khi chinh sua tren visualize can check luon va chinh sua API tren api_kibana theo link
        if is_whitelist(message.chat.id):
            _now = get_date_now()
            _now_only_date = get_only_date_now_format_ipms()
            _ystd_only_date = get_date_yesterday_format_ipms()
            _bot_telethon_impl = BotTelethonImpl(None,
                                                 pool_nocpro=pool_nocpro,
                                                 pool_nocpro_tableau=pool_nocpro_tableau,
                                                 pool_cbs=pool_cbs,
                                                 mysql_pool=mysql_pool,
                                                 gnoc_pool=pool_gnoc)
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]
            words = word.split()

            n = len(words)
            if n >= 1:
                res_info = _bot_telethon_impl.check_cr_map(word.upper(), key_fnd=words_find)
                # res_info = get_mess_filter_between_char(res_info, "\n", str(words_find).upper())
                my_bot_impl = BotImpl(bot)
                if len(res_info) >= 400:
                    my_bot_impl.reply_mess_by_text(message, res_info)
                else:

                    mess_lst = divide_str(res_info, 200)
                    i = 0
                    for mess in mess_lst:
                        if mess.strip() != "":
                            if i == 0:
                                bot.reply_to(message, str(mess))
                            else:
                                bot.send_message(message.chat.id, mess)
                            i += 1

            else:
                bot.reply_to(message, "Wrong syntax Example: /cr_nomap")

        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of cr no mapping ahdv" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['resetport'])
def reset_acc(message):
    try:
        print("Message receive: " + str(message.text))
        # chu y khi chinh sua tren visualize can check luon va chinh sua API tren api_kibana theo link
        if is_whitelist(message.chat.id) or is_resetlist(message.chat.id):
            _bot_telethon_impl = BotTelethonImpl(None,
                                                 pool_nocpro=pool_nocpro,
                                                 pool_nocpro_tableau=pool_nocpro_tableau,
                                                 pool_cbs=pool_cbs,
                                                 mysql_pool=mysql_pool,
                                                 gnoc_pool=pool_gnoc)
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]
            words = word.split()

            n = len(words)
            if n >= 2 and word.find('gftth') >= 0:
                res_info = _bot_telethon_impl.reset_acc(word, key_fnd=words_find)
                # res_info = get_mess_filter_between_char(res_info, "\n", str(words_find).upper())
                bot.reply_to(message, res_info)
            else:
                bot.reply_to(message, "Wrong syntax Example: /resetport h004_gftth_milano")

        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of reset account bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['olt', 'sw', 'srt', 'tramdown', 'tudown'])
def check_srt_olt_sw(message):
    try:
        print("Message receive: " + str(message.text))
        # chu y khi chinh sua tren visualize can check luon va chinh sua API tren api_kibana theo link
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id):
            _now = get_date_now()
            _now_only_date = get_only_date_now_format_ipms()
            _ystd_only_date = get_date_yesterday_format_ipms()
            _bot_telethon_impl = BotTelethonImpl(None,
                                                 pool_nocpro=pool_nocpro,
                                                 pool_nocpro_tableau=pool_nocpro_tableau,
                                                 pool_cbs=pool_cbs,
                                                 mysql_pool=mysql_pool,
                                                 gnoc_pool=pool_gnoc)
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]
            words = word.split()

            n = len(words)
            if n >= 2:
                res_info = _bot_telethon_impl.check_srt_sw_olt(word.upper(), key_fnd=words_find)
                # res_info = get_mess_filter_between_char(res_info, "\n", str(words_find).upper())
                my_bot_impl = BotImpl(bot)
                if len(res_info) >= 400:
                    my_bot_impl.reply_mess_by_text(message, res_info)
                else:

                    mess_lst = divide_str(res_info, 200)
                    i = 0
                    for mess in mess_lst:
                        if mess.strip() != "":
                            if i == 0:
                                bot.reply_to(message, str(mess))
                            else:
                                bot.send_message(message.chat.id, mess)
                            i += 1

            else:
                bot.reply_to(message, "Wrong syntax Example: /srt Ten_Tinh")

        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of bsc traffic bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['vitridown'])
def check_postion_down_cabinet(message):
    try:
        print("Message receive: " + str(message.text))
        # chu y khi chinh sua tren visualize can check luon va chinh sua API tren api_kibana theo link
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id):
            _now = get_date_now()
            _now_only_date = get_only_date_now_format_ipms()
            _ystd_only_date = get_date_yesterday_format_ipms()
            _bot_telethon_impl = BotTelethonImpl(None,
                                                 pool_nocpro=pool_nocpro,
                                                 pool_nocpro_tableau=pool_nocpro_tableau,
                                                 pool_cbs=pool_cbs,
                                                 mysql_pool=mysql_pool,
                                                 gnoc_pool=pool_gnoc)
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]
            words = word.split()

            n = len(words)
            if n >= 2:
                res_info = _bot_telethon_impl.find_pos_down_pctt(word.upper(), key_fnd=words_find)
                # res_info = get_mess_filter_between_char(res_info, "\n", str(words_find).upper())
                my_bot_impl = BotImpl(bot)
                if len(res_info) >= 400:
                    my_bot_impl.reply_mess_by_text(message, res_info)
                else:

                    mess_lst = divide_str(res_info, 200)
                    i = 0
                    for mess in mess_lst:
                        if mess.strip() != "":
                            if i == 0:
                                bot.reply_to(message, str(mess))
                            else:
                                bot.send_message(message.chat.id, mess)
                            i += 1

            else:
                bot.reply_to(message, "Wrong syntax Example: /srt Ten_Tinh")

        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of bsc traffic bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['ndc', 'md', 'mdtt', 'ndctt', 'mctt'])
def check_pwr_off_high_temp(message):
    try:
        print("Message receive: " + str(message.text))
        # chu y khi chinh sua tren visualize can check luon va chinh sua API tren api_kibana theo link
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id):
            _now = get_date_now()
            _now_only_date = get_only_date_now_format_ipms()
            _ystd_only_date = get_date_yesterday_format_ipms()
            _bot_telethon_impl = BotTelethonImpl(None,
                                                 pool_nocpro=pool_nocpro,
                                                 pool_nocpro_tableau=pool_nocpro_tableau,
                                                 pool_cbs=pool_cbs,
                                                 mysql_pool=mysql_pool,
                                                 gnoc_pool=pool_gnoc)
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]
            words = word.split()

            n = len(words)
            if n >= 2:
                res_info = _bot_telethon_impl.check_pwr_off_high_temp(word.upper(), key_fnd=words_find)
                # res_info = get_mess_filter_between_char(res_info, "\n", str(words_find).upper())
                my_bot_impl = BotImpl(bot)
                if len(res_info) >= 400:
                    my_bot_impl.reply_mess_by_text(message, res_info)
                else:
                    mess_lst = divide_str(res_info, 400)
                    i = 0
                    for mess in mess_lst:
                        if mess.strip() != "":
                            if i == 0:
                                bot.reply_to(message, str(mess))
                            else:
                                bot.send_message(message.chat.id, mess)
                            i += 1

            else:
                bot.reply_to(message, "Wrong syntax Example: /ndc HNI or /md HNI")

        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of bsc traffic bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['ringsrt'])
def check_node_down_in_ring(message):
    try:
        print("Message receive: " + str(message.text))
        # chu y khi chinh sua tren visualize can check luon va chinh sua API tren api_kibana theo link
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id):
            _now = get_date_now()
            _now_only_date = get_only_date_now_format_ipms()
            _ystd_only_date = get_date_yesterday_format_ipms()
            _bot_telethon_impl = BotTelethonImpl(None,
                                                 pool_nocpro=pool_nocpro,
                                                 pool_nocpro_tableau=pool_nocpro_tableau,
                                                 pool_cbs=pool_cbs,
                                                 mysql_pool=mysql_pool,
                                                 gnoc_pool=pool_gnoc)
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]
            words = word.split()
            my_bot_impl = BotImpl(bot)

            n = len(words)
            if n >= 2:
                res_info = _bot_telethon_impl.check_node_down(word.upper(), key_fnd=words_find)
                # res_info = _bot_telethon_impl.check_node_order_down(word.upper(), key_fnd=words_find)

                # res_info = get_mess_filter_between_char(res_info, "\n", str(words_find).upper())
                my_bot_impl = BotImpl(bot)
                if len(res_info) >= 400:
                    my_bot_impl.reply_mess_by_text(message, res_info)
                else:
                    mess_lst = divide_str(res_info, 400)
                    i = 0
                    for mess in mess_lst:
                        mess = clean_html_str(mess)
                        if mess.strip() != "":
                            if i == 0:
                                my_bot_impl.reply_to_html(message, str(mess))
                            else:
                                my_bot_impl.send_mess_html_channel(str(mess), message.chat.id)
                            i += 1

            else:
                bot.reply_to(message, "Wrong syntax Example: /ringsrt QNI0196SRT01")

        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of bsc traffic bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['checkmca'])
def check_mca(message):
    try:
        print("Message receive: " + str(message.text))
        # chu y khi chinh sua tren visualize can check luon va chinh sua API tren api_kibana theo link
        if is_whitelist(message.chat.id):
            _now = get_date_now()
            _now_only_date = get_only_date_now_format_ipms()
            _ystd_only_date = get_date_yesterday_format_ipms()
            _bot_telethon_impl = BotTelethonImpl(None,
                                                 pool_nocpro=pool_nocpro,
                                                 pool_nocpro_tableau=pool_nocpro_tableau,
                                                 pool_cbs=pool_cbs,
                                                 mysql_pool=mysql_pool,
                                                 gnoc_pool=pool_gnoc)
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]
            words = word.split()
            n = len(words)
            if n >= 3:
                res_info = _bot_telethon_impl.check_mca(word.upper(), key_fnd=words_find)
                if res_info:
                    # res_info_fltr = get_mess_filter_between_char(res_info, "\n", str(words_find).upper())
                    bot.reply_to(message, str(res_info))
                else:
                    bot.reply_to(message, str("Không có kế quả!"))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['msc_info'])
def check_msc_info(message):
    try:
        print("Message receive: " + str(message.text))
        # chu y khi chinh sua tren visualize can check luon va chinh sua API tren api_kibana theo link
        if is_whitelist(message.chat.id):
            _now = get_date_now()
            _now_only_date = get_only_date_now_format_ipms()
            _ystd_only_date = get_date_yesterday_format_ipms()
            _bot_telethon_impl = BotTelethonImpl(None,
                                                 pool_nocpro=pool_nocpro,
                                                 pool_nocpro_tableau=pool_nocpro_tableau,
                                                 pool_cbs=pool_cbs,
                                                 mysql_pool=mysql_pool,
                                                 gnoc_pool=pool_gnoc)
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]

            words = word.split()
            n = len(words)
            if n >= 2:
                res_info = _bot_telethon_impl.check_msc_info(word.upper(), key_fnd=words_find)
                if res_info:
                    # res_info_fltr = get_mess_filter_between_char(res_info, "\n", str(words_find).upper())
                    bot.reply_to(message, str(res_info))
                else:
                    bot.reply_to(message, str("Không có kế quả!"))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['bscrnc_info'])
def check_bsc_rnc_info(message):
    try:
        print("Message receive: " + str(message.text))
        # chu y khi chinh sua tren visualize can check luon va chinh sua API tren api_kibana theo link
        if is_whitelist(message.chat.id):
            _now = get_date_now()
            _now_only_date = get_only_date_now_format_ipms()
            _ystd_only_date = get_date_yesterday_format_ipms()
            _bot_telethon_impl = BotTelethonImpl(None,
                                                 pool_nocpro=pool_nocpro,
                                                 pool_nocpro_tableau=pool_nocpro_tableau,
                                                 pool_cbs=pool_cbs,
                                                 mysql_pool=mysql_pool,
                                                 gnoc_pool=pool_gnoc)
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]

            words = word.split()
            n = len(words)
            if n >= 2:
                res_info = _bot_telethon_impl.check_bsc_rnc_info(word.upper(), key_fnd=words_find)
                if res_info:
                    # res_info_fltr = get_mess_filter_between_char(res_info, "\n", str(words_find).upper())
                    bot.reply_to(message, str(res_info))
                else:
                    bot.reply_to(message, str("Không có kế quả!"))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['sub_province'])
def check_sub_province(message):
    try:
        print("Message receive: " + str(message.text))
        # chu y khi chinh sua tren visualize can check luon va chinh sua API tren api_kibana theo link
        if is_whitelist(message.chat.id):
            _now = get_date_now()
            _now_only_date = get_only_date_now_format_ipms()
            _ystd_only_date = get_date_yesterday_format_ipms()
            _bot_telethon_impl = BotTelethonImpl(None,
                                                 pool_nocpro=pool_nocpro,
                                                 pool_nocpro_tableau=pool_nocpro_tableau,
                                                 pool_cbs=pool_cbs,
                                                 mysql_pool=mysql_pool,
                                                 gnoc_pool=pool_gnoc)
            words = message.text.split()
            n = len(words)
            if n >= 2:
                res_info = _bot_telethon_impl.check_sub_province(message.text)
                if res_info:
                    bot.reply_to(message, str(res_info))
                else:
                    bot.reply_to(message, str("Không có kế quả!"))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['allsub'])
def check_all_sub(message):
    try:
        print("Message receive: " + str(message.text))
        # chu y khi chinh sua tren visualize can check luon va chinh sua API tren api_kibana theo link
        if is_whitelist(message.chat.id):
            _now = get_date_now()
            _now_only_date = get_only_date_now_format_ipms()
            _ystd_only_date = get_date_yesterday_format_ipms()
            _bot_telethon_impl = BotTelethonImpl(None,
                                                 pool_nocpro=pool_nocpro,
                                                 pool_nocpro_tableau=pool_nocpro_tableau,
                                                 pool_cbs=pool_cbs,
                                                 mysql_pool=mysql_pool,
                                                 gnoc_pool=pool_gnoc)
            words = message.text.split()

            res_info = _bot_telethon_impl.check_sub_country(message.text)
            if res_info:

                bot.reply_to(message, str(res_info))
            else:
                bot.reply_to(message, str("Không có kế quả!"))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['cell_location'])
def check_cell_location(message):
    try:
        print("Message receive: " + str(message.text))
        # chu y khi chinh sua tren visualize can check luon va chinh sua API tren api_kibana theo link
        if is_whitelist(message.chat.id):
            _now = get_date_now()
            _now_only_date = get_only_date_now_format_ipms()
            _ystd_only_date = get_date_yesterday_format_ipms()
            _bot_telethon_impl = BotTelethonImpl(None,
                                                 pool_nocpro=pool_nocpro,
                                                 pool_nocpro_tableau=pool_nocpro_tableau,
                                                 pool_cbs=pool_cbs,
                                                 mysql_pool=mysql_pool,
                                                 gnoc_pool=pool_gnoc)
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]

            words = word.split()
            n = len(words)
            if n >= 2:
                res_info = _bot_telethon_impl.check_cell_location(word.upper(), key_fnd=words_find)
                if res_info:
                    # res_info_fltr = get_mess_filter_between_char(res_info, "\n", str(words_find).upper())
                    bot.reply_to(message, str(res_info))
                else:
                    bot.reply_to(message, str("Không có kế quả!"))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['kpi2g', 'kpi3g', 'kpi4g'])
def check_kpi_access(message):
    try:
        print("Message receive: " + str(message.text))
        # chu y khi chinh sua tren visualize can check luon va chinh sua API tren api_kibana theo link
        if is_whitelist(message.chat.id):
            _now = get_date_now()
            _now_only_date = get_only_date_now_format_ipms()
            _ystd_only_date = get_date_yesterday_format_ipms()
            _bot_telethon_impl = BotTelethonImpl(None,
                                                 pool_nocpro=pool_nocpro,
                                                 pool_nocpro_tableau=pool_nocpro_tableau,
                                                 pool_cbs=pool_cbs,
                                                 mysql_pool=mysql_pool,
                                                 gnoc_pool=pool_gnoc)

            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]

            words = word.split()

            n = len(words)
            if n >= 2:
                chck_kpi = str(words[0]).upper()
                key_txt = str(words[1]).upper()
                res_info = ''
                if chck_kpi.find('KPI2G') >= 0:
                    res_info = _bot_telethon_impl.check_kpi_mobile_access(word.upper(), '2G', key_fnd=words_find)
                elif chck_kpi.find('KPI3G') >= 0:
                    res_info = _bot_telethon_impl.check_kpi_mobile_access(word.upper(), '3G', key_fnd=words_find)
                elif chck_kpi.find('KPI4G') >= 0:
                    res_info = _bot_telethon_impl.check_kpi_mobile_access(word.upper(), '4G', key_fnd=words_find)
                if res_info:
                    # res_info_fltr = get_mess_filter_between_char(res_info, "\n", str(words_find).upper())
                    bot.reply_to(message, str(res_info))
            else:
                res_info = 'Không đúng cú pháp: /KPI2G node_name'
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['checkbackhaul'])
def check_capacity_backhaul_upstream(message):
    try:
        print("Message receive: " + str(message.text))
        # chu y khi chinh sua tren visualize can check luon va chinh sua API tren api_kibana theo link
        if is_whitelist(message.chat.id):
            _now = get_date_now()
            _now_only_date = get_only_date_now_format_ipms()
            _ystd_only_date = get_date_yesterday_format_ipms()

            words = message.text.split()
            res_drop = ''
            n = len(words)
            if n >= 2:
                time_range = words[1]
                if time_range.upper() == 'NOW':
                    # lay thoi gian tu 60p - 30p truoc do elasticsearch tre 15p moi co ket qua
                    # gio tren elasticsearch la gio GMT
                    # _now_end = get_date_minus(_now, -7 * 60 + 30)
                    _now_end = get_date_minus(_now, 0)
                    # _now_bgn = get_date_minus(_now, - 7 * 60 + 60)
                    _now_bgn = get_date_minus(_now, 30)
                    res_info = "NOW: \n Capacity:"

                else:
                    _now_end = get_date_minus(_now, 0)
                    _now_bgn = get_date_minus(_now, 24 * 60)
                    res_info = "Last 24h \n Capacity:"
            else:
                # _now_end = get_date_minus(_now, -7 * 60)
                _now_end = get_date_minus(_now, 0)
                # _now_bgn = get_date_minus(_now, 17 * 60)
                _now_bgn = get_date_minus(_now, 24 * 60)
                res_info = "Last 24h \n Capacity:"

            _now_end_epch = int(convert_date_to_epoch(_now_end) * 1000)
            _now_bgn_epch = int(convert_date_to_epoch(_now_bgn) * 1000)

            _api = ApiKibana(_now_bgn_epch, _now_end_epch, "tbl_interface_status")
            chck_res_kib = False
            num_rept = 0
            max_cpct = 0
            max_util = 0
            max_cbl_lst = []

            while not chck_res_kib:
                max_cpct, max_util = _api.get_max_cpct_util()
                max_cpct_2, max_cbl_lst = _api.get_cable_cpct()
                num_rept += 1
                if max_cpct > 0 and max_util > 0:
                    chck_res_kib = True
                else:
                    time.sleep(3)
                if num_rept > 5:
                    chck_res_kib = True

            res_info = res_info + str(max_cpct) + "Gbps \n Utilizaiton:" + str(round(max_util, 2)) + " Gbps\n"
            if max_cbl_lst:
                for x in max_cbl_lst:
                    # lam tron
                    if x['capacity']:
                        x['capacity'] = int(x['capacity'])
                    res_info += "Cable name:" + x['cable_name'] + " Capacity:" + str(x['capacity']) + " Gbps\n"
            # tinh toan drop backhaul
            if n >= 2:
                time_range = words[1]
                if time_range.upper() == 'NOW':
                    _tbl = TblInterfaceDropImpl(_now_only_date)
                    try:
                        res = mysql_pool.execute(_tbl.get_max_time())
                        if res:
                            for key, val in res[0].items():
                                time_drop = val
                                time_drop_str = convert_date_obj_to_date_str_ipms(time_drop)
                                pos_dbl_dot = str(time_drop_str).find(":")
                                if pos_dbl_dot >= 0:
                                    time_drop_min = time_drop_str[:pos_dbl_dot + 3]
                                    if time_drop_min:
                                        res_drop_lst = mysql_pool.execute(_tbl.get_total_drop_on_time(time_drop_min))
                                        if res_drop_lst:
                                            for key1, val1 in res_drop_lst[0].items():
                                                if val1:
                                                    ttl_drop = round(val1 * 8 / 1000000, 2)
                                                    res_info += "Total Drop: " + str(ttl_drop) + " Mbps on " + str(
                                                        time_drop) + "\n"
                                                    break

                    except Exception as err:
                        print("error %s when get drop now" % str(err))
            else:
                _tbl = TblInterfaceDropImpl(_ystd_only_date)
                res = mysql_pool.execute(_tbl.get_drop_max())
                if res:
                    try:
                        time_drop = res[0]['time_frm']
                        ttl_drop = round(res[0]['total'] * 8 / 1000000, 2)
                        res_info += "Total Drop: " + str(ttl_drop) + " Mbps on " + str(time_drop) + "\n"
                    except Exception as err:
                        print("error %s when get drop yesterday" % str(err))

            bot.reply_to(message, str(res_info))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['checknode'])
def check_node(message):
    try:
        print("Message receive: " + str(message.text))
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id) or is_custom_lst(message.chat.id):
            _now = get_date_now()
            key_txt = ''
            area_txt = ''
            words = message.text.split()
            res_drop = ''
            n = len(words)
            if n >= 3:
                key_txt = words[1].upper()
                area_txt = words[2].upper()
            elif n == 2:
                key_txt = words[1].upper()
                area_txt = get_area(key_txt)

            _obj_gpon_impl = TblGponOltSubImpl(pool_nocpro)
            print("Pool Gpon Busy: " + str(_obj_gpon_impl.pool.busy))
            key_txt = str(key_txt).upper()
            if key_txt and area_txt:
                _obj_aaa_impl = TblOnlineAaaImpl(area_txt)
                totl_ftth = _obj_gpon_impl.get_total_sub_ftth(key_txt)
                totl_mlti = _obj_gpon_impl.get_total_sub_multiscreen(key_txt)
                totl_onl = _obj_aaa_impl.total_find_dev_online(key_txt)
                totl_off = _obj_aaa_impl.total_find_dev_stop_last_15m(key_txt)
                acc_off_lst = _obj_gpon_impl.find_total_offline_cable_olt(key_txt)
                res_info = "Kết quả kiểm tra " + str(key_txt) + ":\n" \
                                                                "Tổng account Truyền hình: " + str(totl_mlti) + "\n" \
                                                                                                                "Tổng account Internet: " + str(
                    totl_ftth) + "\n" \
                                 "Tổng account Internet online: " + str(totl_onl) + "\n" \
                                                                                    "Tổng account Internet offline 15 phút vừa rồi: " + str(
                    totl_off) + "\n"
                if totl_off:
                    res_dev_off_lst = _obj_gpon_impl.find_trouble_type_dev(key_txt)
                    for res_dev_off in res_dev_off_lst:
                        res_info = res_info + " Tổng khách hàng bị " + str(res_dev_off['TROUBLE_TYPE']) + ' ' \
                                                                                                          'là: ' + str(
                            res_dev_off['SUM_ACC']) + "\n"
                # Neu node mang la OLT thi them vao danh sach
                if key_txt.find('OLT') >= 0:
                    if acc_off_lst:
                        res_info += 'Danh sách chi tiết đứt cáp trục (nhánh):\n'
                        for x in acc_off_lst:
                            num = 0
                            for key, val in x.items():
                                if str(val) != '' and key != 'DEVICE_CODE':
                                    if num == 0:
                                        res_info += str(key) + ":" + str(val)
                                    else:
                                        res_info += "," + str(key) + ":" + str(val)
                                    num += 1
                                    ###
                            res_info += '\n'
                my_bot_impl = BotImpl(bot)
                if len(res_info) >= 400:
                    my_bot_impl.reply_mess_by_text(message, res_info)
                else:
                    mess_lst = divide_str(res_info, 200)
                    i = 0
                    for mess in mess_lst:
                        if str(mess).strip() != '':
                            if i == 0:
                                bot.reply_to(message, str(mess))
                            else:
                                bot.send_message(message.chat.id, mess)
                            i += 1

            else:
                bot.reply_to(message, str("Không có tên trạm hoặc tên khu vực tương ứng"))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['m_core'])
def check_mobile(message):
    try:
        print("Message receive: " + str(message.text))
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id):
            _now = get_date_now()
            key_txt = ''
            alrm_lvl = ''
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]

            words = word.split()
            res_info = ''
            n = len(words)
            if n >= 3:
                key_txt = words[1].upper()
                alrm_lvl = words[2].upper()
            elif n == 2:
                key_txt = words[1].upper()
                alrm_lvl = ''

            _obj_impl = TblBssCoreImpl(pool_nocpro)
            print("Pool Gpon Busy: " + str(_obj_impl.pool.busy))
            key_txt = str(key_txt).upper()
            alrm_lvl = str(alrm_lvl).upper()
            if key_txt:
                if alrm_lvl == 'CRITICAL':
                    res_alrm_node_lst = _obj_impl.get_fault_critical_node(key_txt)
                elif alrm_lvl == 'MAJOR':
                    res_alrm_node_lst = _obj_impl.get_fault_major_node(key_txt)
                else:
                    res_alrm_node_lst = _obj_impl.get_fault_node(key_txt)
                if res_alrm_node_lst:
                    if words_find != '':
                        res_alrm_node_lst = get_filter_in_list(res_alrm_node_lst, key_filter=words_find)
                    res_info = "Kết quả kiểm tra " + str(key_txt) + ":\n"
                    for res_alrm in res_alrm_node_lst:
                        res_info += "Cảnh báo: " + str(res_alrm['FAULT_NAME']) + "\n" + "" \
                                                                                        "   Mức độ: " + str(
                            res_alrm['FAULT_LEVEL_ID']) + "\n" + "" \
                                                                 "   Thời gian: " + str(res_alrm['START_TIME']) + "\n"
                    # res_info_fltr = get_mess_filter_between_char(res_info, "\n", words_find)
                    my_bot_impl = BotImpl(bot)
                    if len(res_info) >= 400:
                        my_bot_impl.reply_mess_by_text(message, res_info)
                    else:
                        mess_lst = divide_str(res_info, 2000)
                        i = 0
                        for mess in mess_lst:
                            if i == 0:
                                bot.reply_to(message, str(mess))
                            else:
                                bot.send_message(message.chat.id, mess)
                            i += 1

                else:
                    res_info = "Không có cảnh báo tương ứng"
                    bot.reply_to(message, str(res_info))
            else:
                bot.reply_to(message, str("Không có tên trạm hoặc tên khu vực tương ứng"))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['m_per'])
def check_mobile_performance(message):
    try:
        print("Message receive: " + str(message.text))
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id):
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]

            msg_txt = word.upper()
            _bot_impl = BotTelethonImpl(event=None,
                                        pool_nocpro=pool_nocpro,
                                        pool_nocpro_tableau=pool_nocpro_tableau,
                                        pool_cbs=pool_cbs,
                                        mysql_pool=mysql_pool,
                                        gnoc_pool=pool_gnoc)
            _res = _bot_impl.get_performance_mobile(message=msg_txt, key_fnd=words_find)
            # _res = get_mess_filter_between_char(_res, "\n", words_find)
            my_bot_impl = BotImpl(bot)
            if len(_res) >= 400:
                my_bot_impl.reply_mess_by_text(message, _res)
            else:
                mess_lst = divide_str(_res, 2000)
                i = 0
                for mess in mess_lst:
                    if i == 0:
                        bot.reply_to(message, str(mess))
                    else:
                        bot.send_message(message.chat.id, mess)
                    i += 1

        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['cabinet'])
def check_cabinet(message):
    try:
        print("Message receive: " + str(message.text))
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id):
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]

            msg_txt = word.upper()
            _bot_impl = BotTelethonImpl(event=None,
                                        pool_nocpro=pool_nocpro,
                                        pool_nocpro_tableau=pool_nocpro_tableau,
                                        pool_cbs=pool_cbs,
                                        mysql_pool=mysql_pool,
                                        gnoc_pool=pool_gnoc)
            _res = _bot_impl.check_cabinet(raw_text=msg_txt, key_fnd=words_find)
            # _res = get_mess_filter_between_char(_res, "\n", words_find)
            if _res:
                bot.reply_to(message, str(_res))
            else:
                res_info = "Không có cảnh báo tương ứng"
                bot.reply_to(message, str(res_info))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['station'])
def check_station(message):
    try:
        print("Message receive: " + str(message.text))
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id):
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]

            msg_txt = word.upper()
            _bot_impl = BotTelethonImpl(event=None,
                                        pool_nocpro=pool_nocpro,
                                        pool_nocpro_tableau=pool_nocpro_tableau,
                                        pool_cbs=pool_cbs,
                                        mysql_pool=mysql_pool,
                                        gnoc_pool=pool_gnoc)
            _res = _bot_impl.check_station(raw_text=msg_txt, key_fnd=words_find)
            # _res = get_mess_filter_between_char(_res, "\n", words_find)
            if _res:
                # fix loi message too long cua telegram. phai chia ra lam nhieu phan
                my_bot_impl = BotImpl(bot)
                if len(_res) >= 400:
                    my_bot_impl.reply_mess_by_text(message, _res)
                else:

                    mess_lst = divide_str(_res, 2000)
                    i = 0
                    for mess in mess_lst:
                        if i == 0:
                            bot.reply_to(message, str(mess))
                        else:
                            bot.send_message(message.chat.id, mess)
                        i += 1
            else:
                res_info = "Không có cảnh báo tương ứng"
                bot.reply_to(message, str(res_info))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['uctt'])
def check_uctt(message):
    try:
        print("Message receive: " + str(message.text))
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id):
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]

            msg_txt = word.upper()
            _bot_impl = BotTelethonImpl(event=None,
                                        pool_nocpro=pool_nocpro,
                                        pool_nocpro_tableau=pool_nocpro_tableau,
                                        pool_cbs=pool_cbs,
                                        mysql_pool=mysql_pool,
                                        gnoc_pool=pool_gnoc)
            _res = _bot_impl.check_uctt(msg_txt, key_fnd=words_find)
            # _res = get_mess_filter_between_char(_res, "\n", words_find)
            my_bot_impl = BotImpl(bot)

            if _res:
                # fix loi message too long cua telegram. phai chia ra lam nhieu phan
                mess_lst = divide_str(_res, 2000)
                i = 0
                for mess in mess_lst:
                    # do send mess html doi voi mot so truong hop co dau start tag khong support nen phai chuyen doi
                    mess = clean_html_str(mess)
                    if i == 0:
                        my_bot_impl.reply_to_html(message, str(mess))
                    else:
                        my_bot_impl.send_mess_html_channel(str(mess), message.chat.id)
                    i += 1
            else:
                res_info = "Không có cảnh báo tương ứng"
                bot.reply_to(message, str(res_info))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['dhuctt', 'kbdh'])
def check_operator_uctt(message):
    try:
        print("Message receive: " + str(message.text))
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id):
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]

            msg_txt = word.upper()
            my_bot_impl = BotImpl(bot)
            _bot_impl = BotTelethonImpl(event=None,
                                        pool_nocpro=pool_nocpro,
                                        pool_nocpro_tableau=pool_nocpro_tableau,
                                        pool_cbs=pool_cbs,
                                        mysql_pool=mysql_pool,
                                        gnoc_pool=pool_gnoc)
            _res = _bot_impl.check_operator_uctt(msg_txt, key_fnd=words_find)
            # _res = get_mess_filter_between_char(_res, "\n", words_find)
            if _res:
                # fix loi message too long cua telegram. phai chia ra lam nhieu phan
                mess_lst = divide_str(_res, 1000)
                i = 0
                for mess in mess_lst:
                    mess = clean_html_str(mess)
                    if i == 0:
                        my_bot_impl.reply_to_html(message, str(mess))

                    else:
                        my_bot_impl.send_mess_html_channel(mess, message.chat.id)
                    time.sleep(0.2)
                    i += 1
            else:
                res_info = "Không có cảnh báo tương ứng"
                bot.reply_to(message, str(res_info), parse_mode='HTML')
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['cabinet_relate'])
def check_cabinet_relate(message):
    try:
        print("Message receive: " + str(message.text))
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id):
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]
            msg_txt = word.upper()

            _bot_impl = BotTelethonImpl(event=None,
                                        pool_nocpro=pool_nocpro,
                                        pool_nocpro_tableau=pool_nocpro_tableau,
                                        pool_cbs=pool_cbs,
                                        mysql_pool=mysql_pool,
                                        gnoc_pool=pool_gnoc)
            _res = _bot_impl.check_cabinet_relate(raw_text=msg_txt, key_fnd=words_find)
            # _res = get_mess_filter_between_char(_res, "\n", words_find)
            if _res:
                bot.reply_to(message, str(_res))
            else:
                res_info = "Không có cảnh báo tương ứng"
                bot.reply_to(message, str(res_info))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['cabinet_info'])
def check_cabinet_info(message):
    try:
        print("Message receive: " + str(message.text))
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id):
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]
            msg_txt = word.upper()

            _bot_impl = BotTelethonImpl(event=None,
                                        pool_nocpro=pool_nocpro,
                                        pool_nocpro_tableau=pool_nocpro_tableau,
                                        pool_cbs=pool_cbs,
                                        mysql_pool=mysql_pool,
                                        gnoc_pool=pool_gnoc)
            _res = _bot_impl.check_cabinet_info(raw_text=msg_txt, key_fnd=words_find)
            # res_info = get_mess_filter_between_char(_res, "\n", words_find)
            if _res:
                bot.reply_to(message, str(_res))
            else:
                res_info = "Không có cảnh báo tương ứng"
                bot.reply_to(message, str(res_info))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['cell_relate'])
def check_cell_relate(message):
    try:
        print("Message receive: " + str(message.text))
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id):
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]
            msg_txt = word.upper()

            _bot_impl = BotTelethonImpl(event=None,
                                        pool_nocpro=pool_nocpro,
                                        pool_nocpro_tableau=pool_nocpro_tableau,
                                        pool_cbs=pool_cbs,
                                        mysql_pool=mysql_pool,
                                        gnoc_pool=pool_gnoc)
            _res = _bot_impl.check_cell_relate(raw_text=msg_txt, key_fnd=words_find)
            # _res = get_mess_filter_between_char(_res, "\n", words_find)
            if _res:
                bot.reply_to(message, str(_res))
            else:
                res_info = "Không có cảnh báo tương ứng"
                bot.reply_to(message, str(res_info))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['cell_info'])
def check_cell_info(message):
    try:
        print("Message receive: " + str(message.text))
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id):
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]
            msg_txt = word.upper()

            _bot_impl = BotTelethonImpl(event=None,
                                        pool_nocpro=pool_nocpro,
                                        pool_nocpro_tableau=pool_nocpro_tableau,
                                        pool_cbs=pool_cbs,
                                        mysql_pool=mysql_pool,
                                        gnoc_pool=pool_gnoc)
            _res = _bot_impl.check_cell_info(raw_text=msg_txt, key_fnd=words_find)
            # _res = get_mess_filter_between_char(_res, "\n", words_find)
            if _res:
                bot.reply_to(message, str(_res))
            else:
                res_info = "Không có cảnh báo tương ứng"
                bot.reply_to(message, str(res_info))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['cell'])
def check_cell(message):
    try:
        print("Message receive: " + str(message.text))
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id):
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]
            msg_txt = word.upper()
            _bot_impl = BotTelethonImpl(event=None,
                                        pool_nocpro=pool_nocpro,
                                        pool_nocpro_tableau=pool_nocpro_tableau,
                                        pool_cbs=pool_cbs,
                                        mysql_pool=mysql_pool,
                                        gnoc_pool=pool_gnoc)
            _res = _bot_impl.check_cell(raw_text=msg_txt, key_fnd=words_find)
            # _res = get_mess_filter_between_char(_res, "\n", words_find)
            if _res:
                bot.reply_to(message, str(_res))
            else:
                res_info = "Không có cảnh báo tương ứng"
                bot.reply_to(message, str(res_info))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['kpi'])
def kpi(message):
    from core.helpers.fix_helper import get_node_type_mobile
    from core.database.impl.nocpro.tbl_bss_cbs_core_impl import TblBssCbsCoreImpl
    print("Message receive: " + str(message.text))
    try:
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id):
            _now = get_date_now()
            key_txt = ''
            alrm_lvl = ''
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]

            words = word.split()
            res_info = ''
            n = len(words)
            if n >= 3:
                key_txt = words[1].upper()

            elif n == 2:
                key_txt = words[1].upper()

            _obj_nocpro_impl = TblBssCoreImpl(pool_nocpro)
            _obj_nocpro_tabl_impl = TblBssCoreImpl(pool_nocpro_tableau)
            _obj_cbs_impl = TblBssCbsCoreImpl(pool_cbs)

            print("Pool Gpon Busy: " + str(_obj_nocpro_impl.pool.busy))
            print("Pool CBS Busy: " + str(_obj_cbs_impl.pool.busy))
            key_txt = str(key_txt).upper()
            # can chia tach danh sach node mang VD GGHL01-10 = GGHL01, GGHL02 .. GGHL10
            # chia kieu hai bang dau , GGHL01,02,05 = GGHL01, GGHL02, GGHL05
            node_lst = chunk_node_str_to_lst(key_txt)
            if node_lst:
                for node in node_lst:
                    node_type = get_node_type_mobile(node)
                    res_lst = []
                    if node and node_type:

                        if node_type == 'MSC':
                            res_lst = _obj_cbs_impl.get_kpi_lusr_msc(node)
                            res_lst += _obj_cbs_impl.get_kpi_psr_msc(node)
                        elif node_type == 'BSC':
                            res_lst = _obj_cbs_impl.get_kpi_bsc_2g(node)
                        elif node_type == 'RNC':
                            res_lst = _obj_cbs_impl.get_kpi_rnc(node)
                        elif node_type == 'HLR':
                            res_lst = _obj_cbs_impl.get_kpi_hlr(node)
                        elif node_type == 'HSS':
                            res_lst = _obj_cbs_impl.get_kpi_hs(node)
                            pass
                        elif node_type == 'GGSN':
                            res_lst = _obj_nocpro_impl.get_kpi_ggsn(node)
                        elif node_type == 'SGSN':
                            res_lst = _obj_nocpro_impl.get_kpi_sgsn(node)
                        elif node_type == 'SMSC':
                            res_lst = _obj_nocpro_impl.get_kpi_smsc(node)
                        elif node_type == 'CRBT':
                            res_lst = _obj_nocpro_impl.get_kpi_crbt(node)
                        elif node_type == 'DSC':
                            res_lst = _obj_nocpro_tabl_impl.get_kpi_dsc(node)

                        if res_lst:
                            if words_find != '':
                                res_lst = get_filter_in_list(res_lst, key_filter=words_find)
                            res_info = "KPI của thiết bị: " + str(node) + "\n"
                            for res in res_lst:
                                for key, val in res.items():
                                    res_info += str(key) + ": " + str(val) + "\n"
                            # res_info = get_mess_filter_between_char(res_info, "\n", words_find)

                            bot.reply_to(message, str(res_info))
                        else:
                            bot.reply_to(message, str("Không có KPI tương ứng"))

            else:
                bot.reply_to(message, str("Không có tên trạm hoặc tên khu vực tương ứng"))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['checktotalmobile'])
def check_total_mobile(message):
    try:
        print("Message receive: " + str(message.text))
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id):
            _now = get_date_now()

            _obj_impl = TblBssCoreImpl(pool_nocpro)
            print("Pool Gpon Busy: " + str(_obj_impl.pool.busy))
            res_totl_alrm = _obj_impl.get_count_alarm()
            res_totl_crtc_alrm = _obj_impl.get_count_alarm_critical()

            res_info = "Kết quả kiểm tra tổng cảnh báo Mobile:" + ":\n"

            res_info += "Tổng số cảnh báo: " + str(res_totl_alrm) + "\n" + "" \
                                                                           "Tổng số cảnh báo CRITICAL: " + str(
                res_totl_crtc_alrm)

            bot.reply_to(message, str(res_info))

        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['checknodename'])
def check_node_name(message):
    try:
        print("Message receive: " + str(message.text))
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id) or is_custom_lst(message.chat.id):
            _now = get_date_now()
            key_txt = ''
            area_txt = ''
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]

            words = word.split()
            res_drop = ''
            n = len(words)
            res_info = ''
            if n >= 2:
                key_txt = words[1].upper()
                _dev_name = key_txt.upper()
                _area_fnd = ''
                _dev_name_div = ''
                _dev_name_div_lst = []
                res_lst = []

                if _dev_name.find('&') >= 0:
                    _dev_name_div_lst = _dev_name.split("&")
                    for _dev_name_div in _dev_name_div_lst:
                        _area_fnd = get_area(_dev_name_div)
                        if _area_fnd:
                            break
                else:
                    _area_fnd = get_area(_dev_name)
                    _dev_name_div = _dev_name

                if _area_fnd:
                    _host_impl = HostIpmsImpl(0, '', '', '', '', '')

                    _dev_tent_id = get_tenant_id_from_area(_area_fnd)
                    if _dev_tent_id:
                        _dev_obj_lst = _host_impl.find_host(_dev_tent_id, **dict(searchHostName=_dev_name_div))
                        if _dev_obj_lst:
                            # tim trong list dev name
                            if _dev_name_div_lst:
                                for _dev_obj in _dev_obj_lst:
                                    _dev_obj_name = _dev_obj.hostname
                                    chck_name = True
                                    for _dev_name_div in _dev_name_div_lst:
                                        if _dev_obj_name.find(_dev_name_div) < 0:
                                            chck_name = False
                                    if chck_name:
                                        res_lst.append(_dev_obj)
                            # khong nhap &
                            else:
                                res_lst = _dev_obj_lst
                if res_lst:
                    if words_find != '':
                        res_lst = get_filter_in_list(res_lst, key_filter=words_find)
                    res_info = 'Danh sách trạm: \n'
                    for res in res_lst:
                        res_info += "Tên trạm: " + res.hostname + " IP: " + res.ip + " \n"
                    # res_info = get_mess_filter_between_char(res_info, "\n", words_find)
                    res_info = res_info[:4000] + "..."
                    bot.reply_to(message, res_info)
                else:
                    bot.reply_to(message, str("Không có tên trạm hoặc tên khu vực tương ứng"))
            else:
                bot.reply_to(message, str("Không có tên trạm hoặc tên khu vực tương ứng"))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['checknodealarm'])
def check_node_name(message):
    try:
        print("Message receive: " + str(message.text))
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id):
            _now = get_date_now()
            key_txt = ''
            area_txt = ''
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]

            words = word.split()
            res_drop = ''
            n = len(words)
            res_info = ''
            if n >= 2:
                key_txt = words[1].upper()
                _dev_name = key_txt.upper()
                _area_fnd = ''
                _dev_name_div = ''
                _dev_name_div_lst = []
                res_lst = []

                if _dev_name.find('&') >= 0:
                    _dev_name_div_lst = _dev_name.split("&")
                    for _dev_name_div in _dev_name_div_lst:
                        _area_fnd = get_area(_dev_name_div)
                        if _area_fnd:
                            break
                else:
                    _area_fnd = get_area(_dev_name)
                    _dev_name_div = _dev_name

                if _area_fnd:
                    _host_impl = HostIpmsImpl(0, '', '', '', '', '')

                    _dev_tent_id = get_tenant_id_syslog_from_area(_area_fnd)
                    if _dev_tent_id:
                        _obj = SyslogNocproIpmsImpl(_area_fnd)
                        time_now = get_time_format_now()
                        time_last = get_date_minus_format_elk(time_now, 60 * 24)

                        _host_lst = _obj.find_cbs(time_last, _dev_name, True, True)
                        if _host_lst:
                            if words_find != '':
                                _host_lst = get_filter_in_list(_host_lst, key_filter=words_find)

                            _obj.send_to_group_bot(_host_lst, bot, message.chat.id)
            else:
                bot.reply_to(message, str("Không có tên trạm hoặc tên khu vực tương ứng"))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['checknodealarm1st'])
def check_node_alarm_2nd(message):
    try:
        print("Message receive: " + str(message.text))
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id):
            _now = get_date_now()
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]

            words = word.split()
            n = len(words)

            if n >= 2:
                key_txt = words[1].upper()
                _dev_name = key_txt.upper()
                _bot_impl = BotTelethonImpl(event=None,
                                            pool_nocpro=pool_nocpro,
                                            pool_nocpro_tableau=pool_nocpro_tableau,
                                            pool_cbs=pool_cbs,
                                            mysql_pool=mysql_pool,
                                            gnoc_pool=pool_gnoc)
                _res_info = _bot_impl.check_node_alarm_first(mess=message.text, key_fnd=words_find)
                # res_info = get_mess_filter_between_char(_res_info, "\n", words_find)
                bot.reply_to(message, _res_info)

            else:
                bot.reply_to(message, str("Không có tên trạm hoặc tên khu vực tương ứng"))
        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['checkuser'])
def check_onl(message):
    try:
        print("Message receive: " + str(message.text))
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id) or is_custom_lst(message.chat.id):
            _now = get_date_now()

            _bot_impl = BotTelethonImpl(event=None,
                                        pool_nocpro=pool_nocpro,
                                        pool_nocpro_tableau=pool_nocpro_tableau,
                                        pool_cbs=pool_cbs,
                                        mysql_pool=mysql_pool,
                                        gnoc_pool=pool_gnoc)
            if message.text.find('.') >= 0:
                pos = message.text.find('.')
                pos_2 = message.text.find('.', pos + 1)
                if pos_2 >= 0:
                    pos_3 = message.text.find('.', pos_2 + 1)
                    if pos_3 >= 0:
                        _res_info = _bot_impl.check_user_from_ip(message=message.text)
                    else:
                        _res_info = _bot_impl.check_user(message=message.text)
                else:
                    _res_info = _bot_impl.check_user(message=message.text)
            else:
                _res_info = _bot_impl.check_user(message=message.text)
            bot.reply_to(message, _res_info)

        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['checkuserip'])
def check_onl_ip(message):
    try:
        print("Message receive: " + str(message.text))
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id) or is_custom_lst(message.chat.id):
            _now = get_date_now()

            _bot_impl = BotTelethonImpl(event=None,
                                        pool_nocpro=pool_nocpro,
                                        pool_nocpro_tableau=pool_nocpro_tableau,
                                        pool_cbs=pool_cbs,
                                        mysql_pool=mysql_pool,
                                        gnoc_pool=pool_gnoc)
            _res_info = _bot_impl.check_user_from_ip(message=message.text)
            bot.reply_to(message, _res_info)

        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['checkweb'])
def check_web(message):
    try:
        print("Message receive: " + str(message.text))
        if is_secretlist(message.chat.id):
            _now = get_date_now()

            _bot_impl = BotTelethonImpl(event=None,
                                        pool_nocpro=pool_nocpro,
                                        pool_nocpro_tableau=pool_nocpro_tableau,
                                        pool_cbs=pool_cbs,
                                        mysql_pool=mysql_pool,
                                        gnoc_pool=pool_gnoc)
            _res_info = _bot_impl.check_web(message=message.text)
            bot.reply_to(message, _res_info)

        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['checkrx', 'checklistacc'])
def check_user_rx(message):
    try:
        print("Message receive: " + str(message.text))
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id) or is_custom_lst(message.chat.id):
            _now = get_date_now()
            mess = message.text
            _bot_impl = BotTelethonImpl(event=None,
                                        pool_nocpro=pool_nocpro,
                                        pool_nocpro_tableau=pool_nocpro_tableau,
                                        pool_cbs=pool_cbs,
                                        mysql_pool=mysql_pool,
                                        gnoc_pool=pool_gnoc)
            _res_info = ''
            if mess.upper().find('CHECKRX') >= 0:
                _res_info = _bot_impl.check_user_rx(message=message.text)
            elif mess.upper().find('GFTTH') >= 0:
                _res_info = _bot_impl.check_list_user_olt(message=message.text)
            elif mess.upper().find('_LL_') >= 0 or mess.upper().find('_OW_') >= 0 or mess.upper().find('_O_') >= 0 \
                     or mess.upper().find('_L3LL_') >= 0 or mess.upper().find('_MW_') >= 0:
                _res_info = _bot_impl.check_list_user_l3ll(message=message.text)
            else:
                _res_info = _bot_impl.check_list_user_aon(message=message.text)
            # cat ngan noi dung
            my_bot_impl = BotImpl(bot, BOT_ID)
            if len(_res_info) >= 400:
                my_bot_impl.reply_mess_by_text(message, _res_info)
            else:
                mess_lst = divide_str(_res_info, 400)
                i = 0
                for mess in mess_lst:
                    mess = clean_html_str(mess)
                    if i == 0:
                        my_bot_impl.reply_to_html(message, str(mess))
                    else:
                        my_bot_impl.send_mess_html_channel(str(mess), message.chat.id)
                    i += 1

            # my_bot_impl.reply_to_html(message, _res_info)
            # bot.reply_to(message, _res_info)

        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['checktv'])
def check_user_tv(message):
    try:
        print("Message receive: " + str(message.text))
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id) or is_custom_lst(message.chat.id):
            _now = get_date_now()

            _bot_impl = BotTelethonImpl(event=None,
                                        pool_nocpro=pool_nocpro,
                                        pool_nocpro_tableau=pool_nocpro_tableau,
                                        pool_cbs=pool_cbs,
                                        mysql_pool=mysql_pool,
                                        gnoc_pool=pool_gnoc)
            _res_info = _bot_impl.check_tv(message=message.text)
            bot.reply_to(message, _res_info)

        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of interface bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['checkonline'])
def check_user(message):
    print("Message receive: " + str(message.text))
    prvn = ''
    if is_whitelist(message.chat.id) or is_greylist(message.chat.id) or is_custom_lst(message.chat.id):
        try:
            _now = get_date_now()
            words = message.text.split()
            res_drop = ''

            n = len(words)
            if n >= 2:
                time_now = get_date_now()
                time_lst_45_mins = get_date_minus(time_now, 45)
                time_lst_24h_mins = get_date_minus(time_now, 24 * 60)
                time_lst_7d_mins = get_date_minus(time_now, 7 * 24 * 60)

                time_now_conv = convert_date_to_ipms(time_now)
                time_lst_45_mins_conv = convert_date_to_ipms(time_lst_45_mins)
                time_lst_24h_mins_conv = convert_date_to_ipms(time_lst_24h_mins)
                time_lst_7d_mins_conv = convert_date_to_ipms(time_lst_7d_mins)

                _tbl_usr_impl = TblUseronlineBrasImpl(max=9000,
                                                      current=500,
                                                      max_input=9000,
                                                      link_name='Useronline',
                                                      area='KV3',
                                                      interface_id=2004,
                                                      rtg_url='http://hehe',
                                                      time_begin=time_now_conv,
                                                      node_name='CKV09',
                                                      efficient=73
                                                      )

                prvn = words[1]
                chck_prvn_type = get_province_type(prvn)
                max_now = 0
                max_24h = 0
                avg_24h = 0
                max_7d = 0
                avg_7d = 0
                time_24h = ''
                time_7d = 0
                if chck_prvn_type == 'area':

                    _srch_tst_dct = dict(searchArea=prvn, timestampGreat=time_lst_45_mins_conv,
                                         timestampLess=time_now_conv)

                    _srch_tst_24h_dct = dict(searchArea=prvn, timestampGreat=time_lst_24h_mins_conv,
                                             timestampLess=time_now_conv)
                    _srch_tst_7d_dct = dict(searchArea=prvn, timestampGreat=time_lst_7d_mins_conv,
                                            timestampLess=time_now_conv)
                else:
                    # province
                    prvn = get_province_bras_name(prvn)
                    _srch_tst_dct = dict(searchProvince=prvn, timestampGreat=time_lst_45_mins_conv,
                                         timestampLess=time_now_conv)

                    _srch_tst_24h_dct = dict(searchProvince=prvn, timestampGreat=time_lst_24h_mins_conv,
                                             timestampLess=time_now_conv)
                    _srch_tst_7d_dct = dict(searchProvince=prvn, timestampGreat=time_lst_7d_mins_conv,
                                            timestampLess=time_now_conv)

                _res_now_lst = _tbl_usr_impl.find_like(**_srch_tst_dct)
                _res_24h_lst = _tbl_usr_impl.find_like(**_srch_tst_24h_dct)
                # ve chart 24h
                # filter chi lay user ipv4
                _res_24h_flt_lst = _tbl_usr_impl.get_user_by_fld('User_IPv4_Online', _res_24h_lst)
                if not _res_24h_flt_lst:
                    _res_24h_flt_lst = _tbl_usr_impl.get_user_by_fld('UserOnline', _res_24h_lst)

                _res_7d_lst = _tbl_usr_impl.find_like(**_srch_tst_7d_dct)
                _res_7d_fld_lst = _tbl_usr_impl.get_user_by_fld('User_IPv4_Online', _res_7d_lst)
                if not _res_7d_fld_lst:
                    _res_7d_fld_lst = _tbl_usr_impl.get_user_by_fld('UserOnline', _res_7d_lst)

                if _res_now_lst:
                    max_now = _tbl_usr_impl.get_latest_total_user(_res_now_lst)
                if _res_24h_flt_lst:
                    max_24h, avg_24h, time_24h = _tbl_usr_impl.get_max_avg_total_user(_res_24h_flt_lst)
                if _res_7d_fld_lst:
                    max_7d, avg_7d, time_7d = _tbl_usr_impl.get_max_avg_total_user(_res_7d_fld_lst)
                # Ve chart
                max_ttl_24h = max_24h
                avg_ttl_24h = avg_24h
                max_time_24h = time_24h
                print("Max 24h: %s" % str(max_ttl_24h))
                print("Max Time all: %s" % str(max_time_24h))
                if max_ttl_24h > 0:
                    chart = SimpleLineChart(700, 400, y_range=[0, max_ttl_24h])
                    _num = len(_res_24h_flt_lst)

                    chart.add_data([x['user'] for x in _res_24h_flt_lst])
                    # chart.add_data([avg_7d for x in _res_24h_flt_lst])

                    chart.set_axis_labels(Axis.LEFT, [0, max_ttl_24h / 2, max_ttl_24h])
                    chart.set_axis_labels(Axis.BOTTOM, [_res_24h_flt_lst[0]['time_begin'],
                                                        _res_24h_flt_lst[int(_num / 3)]['time_begin'],
                                                        _res_24h_flt_lst[int(_num * 2 / 3)]['time_begin'],
                                                        _res_24h_flt_lst[int(_num - 1)]['time_begin']])
                    chart.set_axis_labels(Axis.TOP, ['', 'Check User Online ' + str(prvn), ''])

                    # can phai ran dom ra ten file tranh bi nham neu nguoi hoi nhieu
                    img_name = 'chart' + prvn + time_now_conv
                    file_name_uuid = uuid.uuid3(uuid.NAMESPACE_DNS, img_name)
                    img_name_urn = file_name_uuid.urn + '.png'
                    chart.download(img_name_urn)

                    mes = "Result Check User of " + str(prvn) + ":\n Now: " + str(max_now) + \
                          "\n Avg 24h: " + str(int(avg_24h)) + \
                          "\n Last 24h \n Max: " + str(max_24h) + " Time: " + str(time_24h) + \
                          "\n Avg 7 days :" + str(int(avg_7d)) + \
                          "\n Last 7 days\n Max: " + str(max_7d) + " Time: " + str(time_7d)
                    print(mes)
                    test = bot.send_photo(chat_id=message.chat.id, photo=open(img_name_urn, 'rb'), caption=mes)
                    os.remove(img_name_urn)

                    # bot.reply_to(message, mes)
                else:
                    mes = 'No data. Please not for further check'
                    bot.reply_to(message, mes)
            else:
                mes = 'Wrong syntax: /checkonline Area(Province) .\n Example: /checkonline THA or /checkonline KV1'
                bot.reply_to(message, mes)
        except Exception as err:
            print("Error %s when check error of user now and 24h %s" % (str(err), prvn))
            bot.reply_to(message, "Error:" + str(err))
    else:
        bot.reply_to(message, "Chat Group is not in Whitelist")


@bot.message_handler(commands=['listinternet', 'listtv'])
def list_internet(message):
    try:
        print("Message receive: " + str(message.text))
        # chu y khi chinh sua tren visualize can check luon va chinh sua API tren api_kibana theo link
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id):
            _now = get_date_now()
            _now_only_date = get_only_date_now_format_ipms()
            _ystd_only_date = get_date_yesterday_format_ipms()
            _bot_impl = BotTelethonImpl(event=None,
                                        pool_nocpro=pool_nocpro,
                                        pool_nocpro_tableau=pool_nocpro_tableau,
                                        pool_cbs=pool_cbs,
                                        mysql_pool=mysql_pool,
                                        gnoc_pool=pool_gnoc)
            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]

            words = word.split()

            n = len(words)
            if n >= 2:
                if message.text.find('listinternet') >= 0:
                    res_info = _bot_impl.list_user_internet_in_device(word)
                else:
                    res_info = _bot_impl.list_user_tv_in_device(word)
                # filter res_info
                res_info = get_mess_filter_between_char(res_info, ",", words_find)
                my_bot_impl = BotImpl(bot)
                if len(res_info) >= 400:
                    my_bot_impl.reply_mess_by_text(message, res_info)
                else:

                    mess_lst = divide_str(res_info, 200)
                    i = 0
                    for mess in mess_lst:
                        if mess.strip() != "":
                            if i == 0:
                                bot.reply_to(message, str(mess))
                            else:
                                bot.send_message(message.chat.id, mess)
                            i += 1

            else:
                bot.reply_to(message, "Wrong syntax Example: /listinternet LSN0435 10 or /listtv LSN0435 10")

        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of bsc traffic bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


@bot.message_handler(commands=['test1'])
def list_internet(message):
    try:
        print("Message receive: " + str(message.text))
        # chu y khi chinh sua tren visualize can check luon va chinh sua API tren api_kibana theo link
        if is_whitelist(message.chat.id) or is_greylist(message.chat.id):
            _now = get_date_now()
            _now_only_date = get_only_date_now_format_ipms()
            _ystd_only_date = get_date_yesterday_format_ipms()
            _bot_impl = BotTelethonImpl(event=None,
                                        pool_nocpro=pool_nocpro,
                                        pool_nocpro_tableau=pool_nocpro_tableau,
                                        pool_cbs=pool_cbs,
                                        mysql_pool=mysql_pool,
                                        gnoc_pool=pool_gnoc)

            words_find = get_mess_filter(message.text, "| inc", "|inc")
            word = message.text
            if words_find != '':
                pos = word.find("|")
                if pos >= 0:
                    word = word[:pos]

            words = word.split()

            n = len(words)
            if n >= 2:
                my_bot_impl = BotImpl(bot)
                mess = " test some thing\n Test some thing 2 \n Test some thing 3"
                n = len(words)
                if n >= 2:
                    my_bot_impl.reply_mess_by_text(message, str(mess))

                else:
                    bot.reply_to(message, "Wrong syntax Example: /ringsrt QNI0196SRT01")

            else:
                bot.reply_to(message, "Wrong syntax Example: /listinternet LSN0435 10 or /listtv LSN0435 10")

        else:
            bot.reply_to(message, "Chat Group is not in Whitelist")

    except Exception as err:
        print("Error %s when check error of bsc traffic bot" % str(err))
        bot.reply_to(message, "Error:" + str(err))


def listener(messages):
    for m in messages:
        # print(str(m))
        pass


try:
    print("Start again")
    bot.set_update_listener(listener)
    bot.polling(timeout=123)
except Exception as err:
    print("Error %s of Bot Polling" % err)
    logging.critical("Error " + str(err))
    time.sleep(60)
