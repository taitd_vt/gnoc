import time
import os
import cx_Oracle
import logging
from telethon import TelegramClient, events
from core.database.impl.bot.bot_telethon_impl import BotTelethonImpl
from core.database.impl.server_minhnd.server_mysql_connect import ServerMysqlConnect
from core.database.impl.nocpro.tbl_ip_syslog_core import TblNocproImpl
from config import Config, Development

# load credentials here
api_id = 1140292
api_hash = 'bbe8edaf7c5687dc7045ed4f6cd8335f'
session_file = 'Normalboy09'
password = '42647'
phone = str('+84984973864')

config = Development()
BOT_ID = config.__getattribute__('BOT_ID')
BOT_GROUP_ID = -218379033
BOT_GROUP_CNTT_ID = config.__getattribute__('BOT_GROUP_CNTT_ID')
SERVER_NOCPRO = config.__getattribute__('SERVER_NOCPRO')
SERVER_NOCPRO_PORT = config.__getattribute__('SERVER_NOCPRO_PORT')
SERVER_NOCPRO_SERVICE_NAME = config.__getattribute__('SERVER_NOCPRO_SERVICE_NAME')
SERVER_NOCPRO_USERNAME = config.__getattribute__('SERVER_NOCPRO_USERNAME')
SERVER_NOCPRO_PASSWORD = config.__getattribute__('SERVER_NOCPRO_PASSWORD')
SERVER_NOCPRO_USERNAME2 = config.__getattribute__('SERVER_NOCPRO_USERNAME2')
SERVER_NOCPRO_PASSWORD2 = config.__getattribute__('SERVER_NOCPRO_PASSWORD2')
SERVER_AOM = config.__getattribute__('SERVER_AOM')
SERVER_AOM_PORT = config.__getattribute__('SERVER_AOM_PORT')
SERVER_AOM_SERVICE_NAME = config.__getattribute__('SERVER_AOM_SERVICE_NAME')
SERVER_AOM_USERNAME = config.__getattribute__('SERVER_AOM_USERNAME')
SERVER_AOM_PASSWORD = config.__getattribute__('SERVER_AOM_PASSWORD')
BOT_GROUP_VIP_CDBR_ID = config.__getattribute__('BOT_GROUP_VIP_CDBR_ID')
BOT_GROUP_IP_CDBR_KV1 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV1')
BOT_GROUP_IP_CDBR_KV2 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV2')
BOT_GROUP_IP_CDBR_KV3 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV3')
BOT_GROUP_BSS_ID = config.__getattribute__('BOT_GROUP_BSS_ID')
SERVER_CBS = config.__getattribute__('SERVER_CBS')
SERVER_CBS_PORT = config.__getattribute__('SERVER_CBS_PORT')
SERVER_CBS_SERVICE_NAME = config.__getattribute__('SERVER_CBS_SERVICE_NAME')
SERVER_CBS_USERNAME = config.__getattribute__('SERVER_CBS_USERNAME')
SERVER_CBS_PASSWORD = config.__getattribute__('SERVER_CBS_PASSWORD')
BOT_GROUP_WHITELIST = config.__getattribute__('BOT_GROUP_WHITELIST')
BOT_GROUP_GREY_LIST = config.__getattribute__('BOT_GROUP_GREY_LIST')


def is_whitelist(chat_id):
    if chat_id in BOT_GROUP_WHITELIST:
        return True
    else:
        return False


def is_greylist(chat_id):
    if chat_id in BOT_GROUP_GREY_LIST:
        return True
    else:
        return False


if __name__ == '__main__':
    # Create the client and connect
    # use sequential_updates=True to respond to messages one at a time
    logging.basicConfig(level=logging.ERROR,  datefmt='%H:%M:%S',
                        format='%(asctime)s [%(name)s]'
                               ' %(levelname)s: %(message)s'
                        )
    while True:
        try:
            dsn_str = cx_Oracle.makedsn(SERVER_NOCPRO, SERVER_NOCPRO_PORT, service_name=SERVER_NOCPRO_SERVICE_NAME)
            dsn_cbs_str = cx_Oracle.makedsn(SERVER_CBS, SERVER_CBS_PORT, service_name=SERVER_CBS_SERVICE_NAME)

            pool_nocpro = cx_Oracle.SessionPool(min=1,
                                                max=10, increment=1, threaded=True, dsn=dsn_str,
                                                user=SERVER_NOCPRO_USERNAME, password=SERVER_NOCPRO_PASSWORD,
                                                encoding='UTF-8', nencoding='UTF-8')

            pool_nocpro_tableau = cx_Oracle.SessionPool(min=1,
                                                        max=10, increment=1, threaded=True, dsn=dsn_str,
                                                        user=SERVER_NOCPRO_USERNAME2, password=SERVER_NOCPRO_PASSWORD2,
                                                        encoding='UTF-8', nencoding='UTF-8')

            pool_cbs = cx_Oracle.SessionPool(min=1,
                                             max=10, increment=1, threaded=True, dsn=dsn_cbs_str,
                                             user=SERVER_CBS_USERNAME, password=SERVER_CBS_PASSWORD,
                                             encoding='UTF-8', nencoding='UTF-8')

            dbconfig = {
                "host": "192.168.251.15",
                "port": 3306,
                "user": "linhlk1",
                "password": "linhlk135",
                "database": "luuluong",
            }
            mysql_pool = ServerMysqlConnect(**dbconfig)
            _obj = TblNocproImpl(pool=pool_nocpro)
            res_lst = _obj.find_high_temp_lst()

            client = TelegramClient(session_file, api_id, api_hash, sequential_updates=True)


            @client.on(events.NewMessage(incoming=True))
            async def handle_new_message(event):
                _bot_impl = BotTelethonImpl(event,
                                            pool_nocpro=pool_nocpro,
                                            pool_nocpro_tableau=pool_nocpro_tableau,
                                            pool_cbs=pool_cbs,
                                            mysql_pool=mysql_pool)
                logging.info("Receive " + str(event.raw_text))
                if event.is_group:
                    from_ = await event.client.get_entity(event.from_id)
                    # do not reply to bot
                    if not from_.bot:
                        logging.info("Respond NOT from Bot")
                        if is_whitelist(event.chat_id) or is_greylist(event.chat_id):
                            try:
                                logging.info("Respond in whitelist and greylist")
                                test_mess = _bot_impl.respond()
                                logging.info("Respond " + str(test_mess))
                                if isinstance(test_mess, dict):
                                    await event.reply(file=test_mess['file_name'], message=test_mess['mess_caption'])
                                    logging.info("Reply " + str(test_mess['mess_caption']))
                                    os.remove(test_mess['file_name'])
                                elif test_mess:
                                    await event.reply(test_mess)
                                    logging.info("Reply " + str(test_mess))
                                else:
                                    logging.info("Reply Nothing")

                            except Exception as err:
                                print("Error %s when get message respond" % err)
                                await event.reply(str(err))
                                logging.critical("Error " + str(err))


            print(time.asctime(), '-', 'Auto-replying...')
            client.start(phone, password)
            client.run_until_disconnected()
            print(time.asctime(), '-', 'Stopped!')
            logging.critical("Error " + str('STOPPED'))

        except Exception as err:
            print("Error %s of bot" % str(err))
            logging.critical("Error " + str(err))
            time.sleep(1)
