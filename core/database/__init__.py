from config import Development
from core.database.impl.elasticsearch.elastic_impl import ElasticSearchHost
config = Development()
ELASTIC_SERVER = config.__getattribute__('ELASTIC_SERVER')
ELASTIC_PORT = config.__getattribute__('ELASTIC_PORT')
