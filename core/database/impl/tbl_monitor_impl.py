__author__ = 'VTN-DHML-DHIP10'
__author__ = 'VTN-DHML-DHIP10'
import pymysql
import time
from config import Config, Development

config = Development()
SERVER_HOST = config.__getattribute__('SERVER_HOST')
SERVER_HOST_USER = config.__getattribute__('SERVER_HOST_USER')
SERVER_HOST_PASS = config.__getattribute__('SERVER_HOST_PASS')


class TblMonitorImpl:
    def __init__(self):
        pass

    def get_conn(self):

        try:
            con = pymysql.connect(host=SERVER_HOST, port=3306, user=SERVER_HOST_USER, password=SERVER_HOST_PASS,
                                  db='luuluong', cursorclass=pymysql.cursors.DictCursor)

        except:
            return None
        return con

    def find_pckt_err(self, num_err, day_str):

        conn = self.get_conn()
        # print(div_lst)
        # request
        result_acc_lst = list()
        if isinstance(num_err, int):
            if num_err > 0 and day_str:
                if conn:
                    try:
                        with conn.cursor() as cursor:
                            # check voi vendor la Juniper
                            sql = "SELECT  tbl.idInterface, tbl.nodeName,  tbl.linkName, tbl.rtg_url, tbl.time_begin, " \
                                  "tbl.tenPhanNhom, tbl.khuVuc, count(idInterface) as count_id from tbl_monitor tbl " \
                                  "where tbl.loaiCanhBao = 'Packet Error' and time_begin like '%{}%' " \
                                  "and tenPhanNhom not like '%GSKV%'  group by idInterface,khuVuc " \
                                  "having count(idInterface) > {}".format(day_str, str(num_err))

                            cursor.execute(sql)
                            result = cursor.fetchall()
                            if result:
                                result_acc_lst = result_acc_lst + result

                    except Exception as err:
                        # print(err)
                        return result_acc_lst
                    finally:
                        conn.close()
                    time.sleep(1)


        return result_acc_lst

if __name__ == '__main__':
    tbl_monitor = TblMonitorImpl()
    result_lst = tbl_monitor.find_pckt_err(6, '2019-05-21')

