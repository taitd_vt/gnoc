__author__ = 'VTN-DHML-DHIP10'
import pymysql
import time
from config import Config, Development
from core.helpers.stringhelpers import check_regex_acc
from core.helpers.date_helpers import get_date_now, get_date_minus_format_ipms
config = Development()
SERVER_AAA_LST = config.__getattribute__('SERVER_AAA_MAIN_LST')
SERVER_AAA_USERNAME = config.__getattribute__('SERVER_AAA_MAIN_USERNAME')
SERVER_AAA_PASSWORD = config.__getattribute__('SERVER_AAA_MAIN_PASSWORD')


class TblAaaImpl:
    def __init__(self, area):
        self.area = area.upper()

    def get_conn(self):
        # do co nhieu server nen ta luan phien test trong list
        ip_server_lst = []
        for x in SERVER_AAA_LST:
            if 'area' in x:
                area_x = x['area'].upper()
                if area_x == self.area:
                    if 'ip' in x:
                        ip_server_lst.append(x['ip'])

        if ip_server_lst:
            for x in ip_server_lst:
                try:

                    con = pymysql.connect(host=x, port=3306, user=SERVER_AAA_USERNAME, password=SERVER_AAA_PASSWORD,
                                            db='AAA', cursorclass=pymysql.cursors.DictCursor)
                    if con:
                        return con
                except Exception as err:
                    print("Error %s when connect AAA" % str(err))

        return None

    def reset_acc(self, key_srch):

        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = True
        if chck_sql:
            conn = self.get_conn()
            # print(div_lst)
            # request
            if conn:
                try:
                    with conn.cursor() as cursor:
                        # check voi vendor la Juniper
                        sql = "CALL AAA.bccs_Port_Reset(@a, @b, '" + str(key_srch) + "'); ".format(key_srch)
                        cursor.execute(sql)
                        conn.commit()

                except Exception as err:
                    print("Error %s when check device online " % str(err))
                    res_lst = False

                finally:
                    cursor.close()
                    conn.close()
                    return res_lst
            else:
                return False
        else:
            return False


if __name__ == '__main__':
    _tbl_obj = TblAaaImpl('KV1')
    #print("Total online")
    res_lst = _tbl_obj.reset_acc('h004_gftth_123s0')

    print("Total offline last 15 mins")

    #print('test')
