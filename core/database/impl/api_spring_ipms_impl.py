__author__ = 'vtn-dhml-dhip10'
import datetime, functools, requests, json, time
from config import Development
from core.helpers.api_helpers import api_get_retry, api_post_retry, api_put_retry, api_delete_retry
config = Development()
API_SPRING_IPMS_SERVER = config.__getattribute__('API_SPRING_IPMS_SERVER')
#API_SPRING_IPMS_SERVER = '192.168.252.6'
API_SPRING_IPMS_USER = config.__getattribute__('API_SPRING_IPMS_USER')
API_SPRING_IPMS_PASS = config.__getattribute__('API_SPRING_IPMS_PASS')
API_SPRING_IPMS_PORT = config.__getattribute__('API_SPRING_IPMS_PORT')


class ApiSpringIpmsImpl:
    def __init__(self, tbl, x_tenant):
        self.user = API_SPRING_IPMS_USER
        self.passwd = API_SPRING_IPMS_PASS
        self.x_tenant = x_tenant.lower()
        self.tbl = tbl

    def get(self, kwargs):
        str_srch = ""
        for k, v in kwargs.items():
                str_srch = str_srch + "/" + k + "=" + str(v)
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/" + self.tbl + str_srch
        headers_json = {"X-TENANT-ID": self.x_tenant, "username": self.user, "password": self.passwd,
                        "Content-Type": "application/json"}
        # r = requests.get(url_api, headers=headers_json)
        r = api_get_retry(url_api, headers_json)
        if r:
            if r.ok:
                result_json = json.loads(r.text)
                return result_json
            else:
                return list()

    def lst(self):
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/" + self.tbl + "/"
        headers_json = {"X-TENANT-ID": self.x_tenant, "username": self.user, "password": self.passwd,
                        "Content-Type": "application/json"}
        # r = requests.get(url_api, headers=headers_json)
        r = api_get_retry(url_api, headers_json)
        if r:
            if r.ok:
                result_json = json.loads(r.text)
                return result_json
            else:
                return list()

    def total(self):
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/" + self.tbl + "/total"
        headers_json = {"X-TENANT-ID": self.x_tenant, "username": self.user, "password": self.passwd,
                        "Content-Type": "application/json"}
        # r = requests.get(url_api, headers=headers_json)
        r = api_get_retry(url_api, headers_json)
        if r:
            if r.ok:
                result_json = json.loads(r.text)
                return result_json
            else:
                return 0
        return 0

    def post(self, data):
        str_srch = ""

        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/" + self.tbl + "/"
        headers_json = {"X-TENANT-ID": self.x_tenant, "username": self.user, "password": self.passwd,
                        "Content-Type": "application/json"}
        # r = requests.get(url_api, headers=headers_json)
        r = api_post_retry(url_api, headers_json, data)
        if r:
            if r.ok:
                if r.text:
                    if r.text == "OK":
                        return True
                    else:
                        return False
                return True
            else:
                return False
        return False

    def put(self, id, data):

        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/" + self.tbl + "/id=" + \
                  str(id)
        headers_json = {"X-TENANT-ID": self.x_tenant, "username": self.user, "password": self.passwd,
                        "Content-Type": "application/json"}
        # r = requests.get(url_api, headers=headers_json)
        r = api_put_retry(url_api, headers_json, data)
        if r:
            if r.ok:
                if r.text == "OK":
                    return True
                else:
                    return False
            else:
                return False
        return False

    def delete(self, id):

        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/" + self.tbl + "/id=" + \
                  str(id)
        headers_json = {"X-TENANT-ID": self.x_tenant, "username": self.user, "password": self.passwd,
                        "Content-Type": "application/json"}
        # r = requests.get(url_api, headers=headers_json)
        r = api_delete_retry(url_api, headers_json)
        if r:
            if r.ok:

                return r.ok
            else:
                return False
        return False

    def search(self, data):
        str_srch = ""
        result_json = []
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/" + self.tbl + "/search"
        headers_json = {"X-TENANT-ID": self.x_tenant, "username": self.user, "password": self.passwd,
                        "Content-Type": "application/json"}
        # r = requests.get(url_api, headers=headers_json)
        data_json = json.dumps(data)
        r = api_post_retry(url_api, headers_json, data_json)
        if r:
            if r.ok:
                if r.text:
                    result_json = json.loads(r.text)

                    return result_json

        return result_json






