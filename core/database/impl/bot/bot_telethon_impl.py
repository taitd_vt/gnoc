import urllib.request
import uuid
import os
import os
import uuid
import telebot
import time
import json
import cx_Oracle
from core.threading.device_thread.dev_show_interface_crc_one_thread import DeviceShowInterfaceCrc
from config import Config, Development
from core.database.impl.gnoc.tbl_syslog_gnoc_impl import TblSyslogGnocImpl
from core.helpers.date_helpers import get_time_format_now, get_date_minus, get_date_minus_format_elk, \
    convert_date_str_to_date_obj_spring, get_date_now, convert_date_to_epoch, get_date_yesterday_format_ipms, \
    get_only_date_now_format_ipms
from app.api.elasticsearch.api_kibana import ApiKibana
from core.database.impl.server_minhnd.tbl_deny_web_impl import TblDenyWeb
from core.database.impl.npms.tbl_bss_npms_impl import TblBssNpmsImpl
from core.database.impl.nocpro.tbl_ip_syslog_core import TblNocproImpl
from core.database.impl.pctt.tbl_pctt_app_impl import TblPcttAppImpl
from pygooglechart import XYLineChart
from pygooglechart import Chart
from pygooglechart import SimpleLineChart
from pygooglechart import XYLineChart
from pygooglechart import SparkLineChart
from pygooglechart import PieChart2D
from pygooglechart import Axis
from core.helpers.api_helpers import api_post_retry, api_get_retry
from core.helpers.stringhelpers import get_province_bras_name, get_province_type, get_mess_filter
from core.database.impl.server_minhnd.tbl_useronline_bras_impl import TblUseronlineBrasImpl
from core.helpers.date_helpers import get_time_format_now, get_date_minus, get_date_minus_format_elk, \
    convert_date_str_to_date_obj_spring, get_date_now, convert_date_to_epoch, convert_date_to_ipms
from core.helpers.int_helpers import convert_string_to_int
from core.database.impl.tbl_aaa_impl import TblAaaImpl
from core.helpers.stringhelpers import convert_api_vmsa_create_dt_log_for_bsc_xml_to_str, split_mess_with_uctt
from core.helpers.tenant_helpers import get_tenant_id_from_area, get_tenant_id_syslog_from_area
from core.helpers.fix_helper import get_area, get_area_from_acc
from core.database.impl.nocpro.tbl_gpon_olt_sub_impl import TblGponOltSubImpl
from core.database.impl.tbl_online_aaa_impl import TblOnlineAaaImpl
from core.database.impl.nocpro.tbl_bss_core_impl import TblBssCoreImpl
from core.database.impl.ipms.host_ipms_impl import HostIpmsImpl
from core.database.impl.ipms.syslog_nocpro_impl import SyslogNocproIpmsImpl
from core.database.impl.server_minhnd.tbl_interface_drop_impl import TblInterfaceDropImpl
from core.database.impl.server_minhnd.tbl_ring_info_order_impl import TblRingInfoOrderImpl
from core.database.impl.tbl_ring_info_impl import TblRingInfoImpl
from core.helpers.fix_helper import get_node_type_mobile
from core.helpers.stringhelpers import convert_api_television_check_user_to_dct
from core.helpers.list_helpers import get_filter_in_list
from core.database.impl.nocpro.tbl_bss_cbs_core_impl import TblBssCbsCoreImpl
from core.database.impl.server_minhnd.tbl_cable_backbone import TblCableBackboneImpl
from app.api.vmsa.parsing.vmsa_parsing import VmsaParsing
from app.api.television.qos_api_server import QosApiServer
from core.helpers.encode.decode_base64 import DecodeBase64
from core.database.impl.elasticsearch.elastic_impl import ElasticSearchHost
from config import Config, Development

config = Development()
API_FLASK_PORT = config.__getattribute__('API_FLASK_PORT')
# API_SPRING_IPMS_SERVER = "192.168.252.217"
API_SPRING_IPMS_SERVER = config.__getattribute__('API_SPRING_IPMS_SERVER')
ELASTIC_SERVER_OLD = config.__getattribute__('ELASTIC_SERVER_OLD')
ELASTIC_PORT = config.__getattribute__('ELASTIC_PORT')


class BotTelethonImpl():
    def __init__(self, event, pool_nocpro, pool_nocpro_tableau, pool_cbs, mysql_pool, gnoc_pool):
        self.event = event
        self.pool_nocpro = pool_nocpro
        self.pool_nocpro_tableau = pool_nocpro_tableau
        self.pool_cbs = pool_cbs
        self.mysql_pool = mysql_pool
        self.gnoc_pool = gnoc_pool

    @staticmethod
    def get_img_path(file_name):
        pth = os.path.dirname(__file__)
        path_join = os.path.join(pth, "img")
        path_join = os.path.join(path_join, file_name)
        return path_join

    def send_mess_channel(self, mess):
        return mess

    def send_photo(self, file_name, mess_caption):
        return dict(file_name=file_name, mess_caption=mess_caption)

    def save_photo(self, url_lnk):
        file_name_uuid = uuid.uuid3(uuid.NAMESPACE_DNS, url_lnk)
        file_name = file_name_uuid.urn + '.jpg'
        try:
            urllib.request.urlretrieve(url_lnk, self.get_img_path(file_name))
        except Exception as err:
            print("Error %s when save photo" % err)
            return False
        return True

    def get_help(self):
        res_info = "Danh sách lệnh: \n"
        res_info += "Kiểm tra CRC của 1 interface của 1 node /checkcrc Node_IP Interface Time_Between_2_time_check\n"
        res_info += "Kiểm tra interface của 1 node có bị drop không /checkdrop Node_IP Interface \n"
        res_info += "Kiểm tra Lưu lượng backhaul và thực dùng 24h /checkbackhaul\n"
        res_info += "Kiểm tra Lưu lượng backhaul và thực dùng bây giờ /checkbackhaul now\n"
        res_info += "Kiểm tra 1 node có bao nhiêu khách hàng OLT /checknode Node_Name\n"
        res_info += "Kiểm tra cảnh báo của 1 node core di động  /m_core Node_Name mức_độ (VD /m_core GSHT05 Critical \n"
        res_info += "Kiểm tra cảnh báo của 1 cell , 1 cabinet di động  /cabinet, /cell Node_Name (VD /cabinet 3HD115 hoac /cell 3KG4414\n"
        res_info += "Kiểm tra mạng di động có bao nhiêu cảnh báo /checktotalmobile \n"
        res_info += "Tìm trên IPMS danh sách node mạng theo tên /checknodename Node_name_need_check(PDL&CKV)\n"
        res_info += "Tìm trên IPMS danh sách CBS của 1 node /checknodealarm Node_name_need_check(PDL&CKV)\n"
        res_info += "Vẽ biểu đồ số useronline của 1 tỉnh hoặc 1 thiết bị /checkonline Province(or node_bras)\n"
        res_info += "Kiểm tra user có online hay không /checkuser username\n"
        return res_info

    def check_node(self, raw_text):
        words = raw_text.split()
        res_drop = ''
        key_txt = ''
        area_txt = ''
        n = len(words)
        if n >= 3:
            key_txt = words[1].upper()
            area_txt = words[2].upper()
        elif n == 2:
            key_txt = words[1].upper()
            area_txt = get_area(key_txt)

        _obj_gpon_impl = TblGponOltSubImpl(self.pool_nocpro)
        print("Pool Gpon Busy: " + str(_obj_gpon_impl.pool.busy))
        key_txt = str(key_txt).upper()
        if key_txt and area_txt:
            _obj_aaa_impl = TblOnlineAaaImpl(area_txt)
            totl_ftth = _obj_gpon_impl.get_total_sub_ftth(key_txt)
            totl_mlti = _obj_gpon_impl.get_total_sub_multiscreen(key_txt)
            totl_onl = _obj_aaa_impl.total_find_dev_online(key_txt)
            totl_off = _obj_aaa_impl.total_find_dev_stop_last_15m(key_txt)
            res_info = "Kết quả kiểm tra " + str(key_txt) + ":\n" \
                                                            "Tổng account Truyền hình: " + str(totl_mlti) + "\n" \
                                                                                                            "Tổng account Internet: " + str(
                totl_ftth) + "\n" \
                             "Tổng account Internet online: " + str(totl_onl) + "\n" \
                                                                                "Tổng account Internet offline 15 phút vừa rồi: " + str(
                totl_off) + "\n"
            if totl_off:
                res_dev_off_lst = _obj_gpon_impl.find_trouble_type_dev(key_txt)
                for res_dev_off in res_dev_off_lst:
                    res_info = res_info + " Tổng khách hàng bị " + str(res_dev_off['TROUBLE_TYPE']) + ' ' \
                                                                                                      'là: ' + str(
                        res_dev_off['SUM_ACC']) + "\n"

            return self.send_mess_channel(str(res_info))
        else:
            return self.send_mess_channel(str(str("Không có tên trạm hoặc tên khu vực tương ứng")))

    def check_cabinet_cell(self, raw_text):
        _now = get_date_now()
        key_txt = ''
        alrm_lvl = ''
        words = raw_text.split()
        res_info = ''
        n = len(words)
        if n >= 3:
            key_txt = words[1].upper()
            alrm_lvl = words[2].upper()
        elif n == 2:
            key_txt = words[1].upper()
            alrm_lvl = ''

        _obj_impl = TblBssCoreImpl(self.pool_nocpro)
        print("Pool Gpon Busy: " + str(_obj_impl.pool.busy))
        key_txt = str(key_txt).upper()
        # alrm_lvl = str(alrm_lvl).upper()
        if key_txt:
            res_alrm_node_lst = _obj_impl.check_cell_cabinet_alarm(key_txt)
            if res_alrm_node_lst:
                res_info = "Kết quả kiểm tra " + str(key_txt) + ":\n"
                for res_alrm in res_alrm_node_lst:
                    res_info += "   Cảnh báo: " + str(res_alrm['FAULT_NAME']) + " " \
                                                                                "" + str(res_alrm['TIME_START']) + "\n"

                return self.send_mess_channel(res_info)
            else:
                res_info = "Không có cảnh báo ở hiện tại"
                return self.send_mess_channel(res_info)

        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
            return self.send_mess_channel(res_info)

    def check_cabinet_relate(self, raw_text, key_fnd):
        _now = get_date_now()
        key_txt = ''
        alrm_lvl = ''
        words = raw_text.split()
        res_info = ''
        n = len(words)
        if n >= 3:
            key_txt = words[1].upper()

        elif n == 2:
            key_txt = words[1].upper()

        _obj_impl = TblBssCoreImpl(self.pool_nocpro)
        print("Pool Gpon Busy: " + str(_obj_impl.pool.busy))
        key_txt = str(key_txt).upper()
        res_info = ""
        if key_txt:
            # Check cabinet gom co 4 buoc.
            # B2: Lay ten tram tu cabinet
            # B3: Lay danh sach tu tu cell
            # B4: Lay danh sach cell

            # B2
            res_name_stan_lst = _obj_impl.get_station_from_cabinet_2nd(key_srch=key_txt)
            if res_name_stan_lst:
                if key_fnd != '':
                    res_name_stan_lst = get_filter_in_list(res_name_stan_lst, key_filter=key_fnd)
                res_info += "Cabinet thuộc trạm :\n"
                for res_name_stan in res_name_stan_lst:
                    for key, val in res_name_stan.items():
                        res_info += "   " + str(key) + " :" + str(val) + "\n"
            else:
                res_info += "Cabinet thuộc trạm : Không có\n"

            # B3:
            res_cabn_from_cell_lst = _obj_impl.get_cabinets_from_station_3rd(key_srch=key_txt)
            if res_cabn_from_cell_lst:
                if key_fnd != '':
                    res_cabn_from_cell_lst = get_filter_in_list(res_cabn_from_cell_lst, key_filter=key_fnd)
                res_info += "Trạm gồm các tủ:\n"
                for res_name_stan in res_cabn_from_cell_lst:
                    for key, val in res_name_stan.items():
                        res_info += "   " + str(val) + "\n"
            else:
                res_info += "Trạm gồm các tủ: Không có\n"

            # B4:
            res_cell_lst = _obj_impl.get_cell_list_from_cabinet_for_cabinet_4th(key_srch=key_txt)
            if res_cell_lst:
                if key_fnd != '':
                    res_cell_lst = get_filter_in_list(res_cell_lst, key_filter=key_fnd)
                res_info += "Trạm gồm các cell:\n"
                #  Gom dua theo danh sach CELL cho de nhin
                for res_name_stan in res_cell_lst:
                    for key, val in res_name_stan.items():
                        if str(key) == 'DANH_SACH_CELL':
                            res_info += "   " + str(val) + "\n"
            else:
                res_info += "Trạm gồm các cell: Không có\n"

        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
            return self.send_mess_channel(res_info)
        if res_info == '':
            res_info = "Không có kết quả khi tra cứu !"
        return self.send_mess_channel(res_info)

    def check_cell_info(self, raw_text, key_fnd=''):
        _now = get_date_now()
        key_txt = ''
        alrm_lvl = ''
        words = raw_text.split()
        res_info = ''
        n = len(words)
        if n >= 3:
            key_txt = words[1].upper()

        elif n == 2:
            key_txt = words[1].upper()

        _obj_impl = TblBssCoreImpl(self.pool_nocpro)
        print("Pool Gpon Busy: " + str(_obj_impl.pool.busy))
        key_txt = str(key_txt).upper()
        res_info = ""
        if key_txt:
            res_name_stan_lst = _obj_impl.get_cell_info(key_srch=key_txt)
            if res_name_stan_lst:
                if key_fnd != '':
                    res_name_stan_lst = get_filter_in_list(res_name_stan_lst, key_filter=key_fnd)
                res_info += "Thông tin Cell :\n"
                for res_name_stan in res_name_stan_lst:
                    for key, val in res_name_stan.items():
                        res_info += "   " + str(key) + " :" + str(val) + "\n"
                    res_info += "\n"
            else:
                res_info += "Thông tin Cell :Không có\n"

        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
            return self.send_mess_channel(res_info)
        if res_info == '':
            res_info = "Không có kết quả khi tra cứu !"
        return self.send_mess_channel(res_info)

    def check_cabinet_info(self, raw_text, key_fnd=''):
        _now = get_date_now()
        key_txt = ''
        alrm_lvl = ''
        words = raw_text.split()
        res_info = ''
        n = len(words)
        if n >= 3:
            key_txt = words[1].upper()

        elif n == 2:
            key_txt = words[1].upper()

        _obj_impl = TblBssCoreImpl(self.pool_nocpro)
        print("Pool Gpon Busy: " + str(_obj_impl.pool.busy))
        key_txt = str(key_txt).upper()
        res_info = ""
        if key_txt:
            res_name_stan_lst = _obj_impl.get_cabinet_info(key_srch=key_txt)
            if res_name_stan_lst:
                if key_fnd != '':
                    res_name_stan_lst = get_filter_in_list(res_name_stan_lst, key_filter=key_fnd)
                res_info += "Thông tin Cabinet :\n"
                for res_name_stan in res_name_stan_lst:
                    for key, val in res_name_stan.items():
                        res_info += "   " + str(key) + " :" + str(val) + "\n"
                    res_info += "\n"
            else:
                res_info += "Thông tin Cabinet :Không có\n"

        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
            return self.send_mess_channel(res_info)
        if res_info == '':
            res_info = "Không có kết quả khi tra cứu !"
        return self.send_mess_channel(res_info)

    def check_cabinet(self, raw_text, key_fnd=''):
        _now = get_date_now()
        key_txt = ''
        alrm_lvl = ''
        words = raw_text.split()
        res_info = ''
        n = len(words)
        if n >= 3:
            key_txt = words[1].upper()

        elif n == 2:
            key_txt = words[1].upper()

        _obj_impl = TblBssCoreImpl(self.pool_nocpro)
        print("Pool Gpon Busy: " + str(_obj_impl.pool.busy))
        key_txt = str(key_txt).upper()
        res_info = ""
        if key_txt:
            # Check cabinet gom co 4 buoc.
            # B1: lay canh bao cua cabinet name
            res_alrm_node_lst = _obj_impl.check_cabinet_alarm_frst(key_srch=key_txt)
            if res_alrm_node_lst:
                if key_fnd != '':
                    res_alrm_node_lst = get_filter_in_list(res_alrm_node_lst, key_filter=key_fnd)
                res_info += "Cảnh báo của cabinet " + str(key_txt) + " :\n"
                for res_alrm_node in res_alrm_node_lst:
                    for key, val in res_alrm_node.items():
                        res_info += "   " + str(key) + " :" + str(val) + "\n"
                    res_info += "\n"
            else:
                res_info += "Cảnh báo của cabinet : Không có\n"

        if res_info == '':
            res_info = "Không có kết quả khi tra cứu !"
        return self.send_mess_channel(res_info)

    def check_station(self, raw_text, key_fnd=''):
        _now = get_date_now()
        key_txt = ''
        alrm_lvl = ''
        words = raw_text.split()
        res_info = ''
        n = len(words)
        if n >= 3:
            key_txt = words[1].upper()
        elif n == 2:
            key_txt = words[1].upper()

        _obj_impl = TblBssCoreImpl(self.pool_nocpro)
        print("Pool Gpon Busy: " + str(_obj_impl.pool.busy))
        key_txt = str(key_txt).upper()
        res_info = ""
        if key_txt:
            # Check cabinet gom co 4 buoc.
            # B1: lay canh bao cua cabinet name
            res_alrm_node_lst = _obj_impl.get_station_alm(key_txt)
            if res_alrm_node_lst:
                if key_fnd != '':
                    res_alrm_node_lst = get_filter_in_list(res_alrm_node_lst, key_filter=key_fnd)
                res_info += "Cảnh báo của station " + str(key_txt) + " :\n"
                for res_alrm_node in res_alrm_node_lst:
                    for key, val in res_alrm_node.items():
                        res_info += "   " + str(key) + " :" + str(val) + "\n"
                    res_info += "\n"
            else:
                res_info += "Cảnh báo của station : Không có\n"

        if res_info == '':
            res_info = "Không có kết quả khi tra cứu !"

        return self.send_mess_channel(res_info)

    def check_cell_relate(self, raw_text, key_fnd=''):
        _now = get_date_now()
        key_txt = ''
        words = raw_text.split()
        res_info = ''
        n = len(words)
        if n >= 3:
            key_txt = words[1].upper()

        elif n == 2:
            key_txt = words[1].upper()

        _obj_impl = TblBssCoreImpl(self.pool_nocpro)
        print("Pool Gpon Busy: " + str(_obj_impl.pool.busy))
        key_txt = str(key_txt).upper()

        res_info = ""
        if key_txt:
            # Check cabinet gom co 4 buoc.

            # B2: Lay ten tram tu cell
            # B3: Lay danh sach tu tu cell
            # B4: Lay danh sach cell

            # B2
            res_name_stan_lst = _obj_impl.get_station_from_cell_2nd(key_srch=key_txt)
            if res_name_stan_lst:
                if key_fnd != '':
                    res_name_stan_lst = get_filter_in_list(res_name_stan_lst, key_filter=key_fnd)
                res_info += "Cell thuộc trạm :\n"
                for res_name_stan in res_name_stan_lst:
                    for key, val in res_name_stan.items():
                        res_info += "   " + str(key) + " :" + str(val) + "\n"
            else:
                res_info += "Cell thuộc trạm : Không có\n"

            # B3:
            res_cabn_from_cell_lst = _obj_impl.get_cabinet_from_cell_3rd(key_srch=key_txt)
            if res_cabn_from_cell_lst:
                if key_fnd != '':
                    res_cabn_from_cell_lst = get_filter_in_list(res_cabn_from_cell_lst, key_filter=key_fnd)
                res_info += "Trạm gồm các tủ:\n"
                for res_name_stan in res_cabn_from_cell_lst:
                    for key, val in res_name_stan.items():
                        res_info += "   " + str(key) + " :" + str(val) + "\n"
            else:
                res_info += "Trạm gồm các tủ: Không có\n"

            # B4:
            res_cell_lst = _obj_impl.get_cell_list_from_cabinet_4th(key_srch=key_txt)
            if res_cell_lst:
                if key_fnd != '':
                    res_cell_lst = get_filter_in_list(res_cell_lst, key_filter=key_fnd)
                res_info += "Trạm gồm các cell:\n"
                for res_name_stan in res_cell_lst:
                    for key, val in res_name_stan.items():
                        if key == 'DANH_SACH_CELL':
                            res_info += "   " + str(val) + "\n"
            else:
                res_info += "Trạm gồm các cell: Không có\n"

        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
            return self.send_mess_channel(res_info)
        if res_info == '':
            res_info = 'Không có kết quả khi tra cứu !'
        return self.send_mess_channel(res_info)

    def check_cell(self, raw_text, key_fnd=''):
        _now = get_date_now()
        key_txt = ''
        words = raw_text.split()
        res_info = ''
        n = len(words)
        if n >= 3:
            key_txt = words[1].upper()

        elif n == 2:
            key_txt = words[1].upper()

        _obj_impl = TblBssCoreImpl(self.pool_nocpro)
        print("Pool Gpon Busy: " + str(_obj_impl.pool.busy))
        key_txt = str(key_txt).upper()

        res_info = ""
        if key_txt:
            # Check cabinet gom co 4 buoc.
            # B1: lay canh bao cua cell name

            res_alrm_node_lst = _obj_impl.check_cell_alarm_frst(key_srch=key_txt)
            if res_alrm_node_lst:
                if key_fnd != '':
                    res_alrm_node_lst = get_filter_in_list(res_alrm_node_lst, key_filter=key_fnd)
                res_info += "Cảnh báo của cell " + str(key_txt) + " :\n"
                for res_alrm_node in res_alrm_node_lst:
                    for key, val in res_alrm_node.items():
                        res_info += "   " + str(key) + " :" + str(val) + "\n"
                    res_info += "\n"
            else:
                res_info += "Cảnh báo của cell : Không có\n"

        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
            return self.send_mess_channel(res_info)
        if res_info == '':
            res_info = 'Không có kết quả khi tra cứu !'
        return self.send_mess_channel(res_info)

    def check_kpi_mobile(self, raw_text):
        _now = get_date_now()
        key_txt = ''
        alrm_lvl = ''
        words = raw_text.split()
        res_info = ''
        n = len(words)
        if n >= 3:
            key_txt = words[1].upper()
            # alrm_lvl = words[2].upper()
        elif n == 2:
            key_txt = words[1].upper()
            # alrm_lvl = ''

        _obj_nocpro_impl = TblBssCoreImpl(self.pool_nocpro)
        _obj_nocpro_tabl_impl = TblBssCoreImpl(self.pool_nocpro_tableau)
        _obj_cbs_impl = TblBssCbsCoreImpl(self.pool_cbs)

        print("Pool Gpon Busy: " + str(_obj_nocpro_impl.pool.busy))
        print("Pool CBS Busy: " + str(_obj_cbs_impl.pool.busy))
        key_txt = str(key_txt).upper()
        node_type = get_node_type_mobile(key_txt)
        res_lst = []

        if key_txt and node_type:
            if node_type == 'MSC':
                res_lst = _obj_cbs_impl.get_kpi_lusr_msc(key_txt)
                res_lst += _obj_cbs_impl.get_kpi_psr_msc(key_txt)
            elif node_type == 'HLR':
                res_lst = _obj_cbs_impl.get_kpi_hlr(key_txt)
            elif node_type == 'HSS':
                res_lst = _obj_cbs_impl.get_kpi_hs(key_txt)
            elif node_type == 'BSC':
                res_lst = _obj_cbs_impl.get_kpi_bsc_2g(key_txt)
            elif node_type == 'RNC':
                res_lst = _obj_cbs_impl.get_kpi_rnc(key_txt)
            elif node_type == 'GGSN':
                res_lst = _obj_nocpro_impl.get_kpi_ggsn(key_txt)
            elif node_type == 'SGSN':
                res_lst = _obj_nocpro_impl.get_kpi_sgsn(key_txt)
            elif node_type == 'SMSC':
                res_lst = _obj_nocpro_impl.get_kpi_smsc(key_txt)
            elif node_type == 'CRBT':
                res_lst = _obj_nocpro_impl.get_kpi_crbt(key_txt)
            elif node_type == 'DSC':
                res_lst = _obj_nocpro_tabl_impl.get_kpi_dsc(key_txt)

            if res_lst:
                res_info = "KPI của thiết bị: " + str(key_txt) + "\n"
                for res in res_lst:
                    for key, val in res.items():
                        res_info += str(key) + ": " + str(val) + "\n"
                return self.send_mess_channel(res_info)
            else:
                res_info = "Không có KPI tương ứng"
                return self.send_mess_channel(res_info)
        else:

            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
            return self.send_mess_channel(res_info)

    def check_kpi_mobile_access(self, raw_text, type, key_fnd=''):
        _now = get_date_now()
        key_txt = ''
        alrm_lvl = ''
        words = raw_text.split()
        res_info = ''
        n = len(words)
        if n >= 3:
            key_txt = words[1].upper()
            # alrm_lvl = words[2].upper()
        elif n == 2:
            key_txt = words[1].upper()
            # alrm_lvl = ''

        _obj_cbs_impl = TblBssCbsCoreImpl(self.pool_cbs)

        key_txt = str(key_txt).upper()
        res_lst = []

        if key_txt:
            res_lst = _obj_cbs_impl.get_kpi_access_station(key_txt, type)

            if res_lst:
                if key_fnd != '':
                    res_lst = get_filter_in_list(res_lst, key_filter=key_fnd)
                res_info = "KPI của thiết bị: " + str(key_txt) + "\n"
                for res in res_lst:
                    for key, val in res.items():
                        res_info += str(key) + ": " + str(val) + "\n"
                return self.send_mess_channel(res_info)
            else:
                res_info = "Không có KPI tương ứng"
                return self.send_mess_channel(res_info)
        else:

            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
            return self.send_mess_channel(res_info)

    def check_mca(self, raw_text, key_fnd=''):
        _now = get_date_now()
        key_txt = ''
        words = raw_text.split()
        res_info = ''
        n = len(words)
        if n >= 3:
            key_txt = words[2].upper()
            mca_type = words[1].upper()

            _obj_cbs_impl = TblBssCbsCoreImpl(self.pool_cbs)

            key_txt = str(key_txt).upper()
            res_lst = []

            if key_txt and mca_type:
                if mca_type.upper().find('2G') >= 0:
                    res_lst = _obj_cbs_impl.get_mca_2g(key_txt)
                elif mca_type.upper().find('3G') >= 0:
                    res_lst = _obj_cbs_impl.get_mca_3g(key_txt)
                elif mca_type.upper().find('4G') >= 0:
                    res_lst = _obj_cbs_impl.get_mca_4g(key_txt)

                if res_lst:
                    if key_fnd != '':
                        res_lst = get_filter_in_list(res_lst, key_filter=key_fnd)
                    res_info = "KPI của thiết bị: " + str(key_txt) + "\n"
                    for res in res_lst:
                        for key, val in res.items():
                            res_info += str(key) + ": " + str(val) + "\n"
                    return self.send_mess_channel(res_info)
                else:
                    res_info = "Không có KPI tương ứng"
                    return self.send_mess_channel(res_info)
            else:

                res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
                return self.send_mess_channel(res_info)
        else:
            res_info = "Không đúng cú pháp. VD mca 2G AG39233"
            return self.send_mess_channel(res_info)

    def check_total_mobile(self):
        _now = get_date_now()

        _obj_impl = TblBssCoreImpl(self.pool_nocpro)
        print("Pool Gpon Busy: " + str(_obj_impl.pool.busy))
        res_totl_alrm = _obj_impl.get_count_alarm()
        res_totl_crtc_alrm = _obj_impl.get_count_alarm_critical()

        res_info = "Kết quả kiểm tra tổng cảnh báo Mobile:" + ":\n"

        res_info += "Tổng số cảnh báo: " + str(res_totl_alrm) + "\n" + "" \
                                                                       "Tổng số cảnh báo CRITICAL: " + str(
            res_totl_crtc_alrm)

        return self.send_mess_channel(res_info)

    def check_node_name(self, raw_text):
        _now = get_date_now()
        key_txt = ''
        area_txt = ''
        words = raw_text.split()
        res_drop = ''
        n = len(words)
        res_info = ''
        if n >= 2:
            key_txt = words[1].upper()
            _dev_name = key_txt.upper()
            _area_fnd = ''
            _dev_name_div = ''
            _dev_name_div_lst = []
            res_lst = []

            if _dev_name.find('&') >= 0:
                _dev_name_div_lst = _dev_name.split("&")
                for _dev_name_div in _dev_name_div_lst:
                    _area_fnd = get_area(_dev_name_div)
                    if _area_fnd:
                        break
            else:
                _area_fnd = get_area(_dev_name)
                _dev_name_div = _dev_name

            if _area_fnd:
                _host_impl = HostIpmsImpl(0, '', '', '', '', '')

                _dev_tent_id = get_tenant_id_from_area(_area_fnd)
                if _dev_tent_id:
                    _dev_obj_lst = _host_impl.find_host(_dev_tent_id, **dict(searchHostName=_dev_name_div))
                    if _dev_obj_lst:
                        # tim trong list dev name
                        if _dev_name_div_lst:
                            for _dev_obj in _dev_obj_lst:
                                _dev_obj_name = _dev_obj.hostname
                                chck_name = True
                                for _dev_name_div in _dev_name_div_lst:
                                    if _dev_obj_name.find(_dev_name_div) < 0:
                                        chck_name = False
                                if chck_name:
                                    res_lst.append(_dev_obj)
                        # khong nhap &
                        else:
                            res_lst = _dev_obj_lst
            if res_lst:
                res_info = 'Danh sách trạm: \n'
                for res in res_lst:
                    res_info += "Tên trạm: " + res.hostname + " IP: " + res.ip + " \n"
                res_info = res_info[:4000] + "..."
                return self.send_mess_channel(res_info)
            else:
                res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
                return self.send_mess_channel(res_info)
        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
            return self.send_mess_channel(res_info)

    def check_node_alarm_first(self, mess, key_fnd=''):
        _now = get_date_now()
        words = mess.split()
        n = len(words)
        res_info = ''
        if n >= 2:
            key_txt = words[1].upper()
            res_lst = []
            _obj_impl = TblBssCoreImpl(self.pool_nocpro)
            print("Pool Gpon Busy: " + str(_obj_impl.pool.busy))
            key_txt = str(key_txt).upper()

            if key_txt:
                res_alrm_node_lst = _obj_impl.get_alarm_secondary_node(key_txt)
                if res_alrm_node_lst:
                    if key_fnd != '':
                        res_alrm_node_lst = get_filter_in_list(res_alrm_node_lst, key_filter=key_fnd)
                    res_info = "Danh sách cảnh báo sơ cấp của " + str(key_txt) + ":\n"
                    for res_alrm in res_alrm_node_lst:
                        for key, val in res_alrm.items():
                            res_info += "   " + str(key) + " :" + str(val) + "\n"
                else:
                    res_info = "Không có cảnh báo nào"

        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
        return res_info

    def check_srt_sw_olt(self, mess, key_fnd):
        _now = get_date_now()
        words = mess.split()
        n = len(words)
        res_info = ''
        dist_txt = ''
        res_alrm_node_lst = []
        if n > 2:
            for x in words[2:]:
                dist_txt += " " + x.upper()
        else:
            dist_txt = ""
        dist_txt = dist_txt.strip()
        if n >= 2:
            key_txt = words[1].upper()
            res_lst = []
            _obj_impl = TblNocproImpl(self.pool_nocpro)
            print("Pool Gpon Busy: " + str(_obj_impl.pool.busy))
            key_txt = str(key_txt).upper()
            totl = 0
            if key_txt:
                if mess.upper().find("SRT") >= 0:
                    res_alrm_node_lst = _obj_impl.find_srt_lst(key_txt, dist_txt)
                    totl = len(res_alrm_node_lst)
                elif mess.upper().find("OLT") >= 0:
                    res_alrm_node_lst = _obj_impl.find_olt_lst(key_txt, dist_txt)
                    totl = len(res_alrm_node_lst)
                elif mess.upper().find("SW") >= 0:
                    res_alrm_node_lst = _obj_impl.find_sw_lst(key_txt, dist_txt)
                    totl = len(res_alrm_node_lst)
                elif mess.upper().find("TUDOWN") >= 0:
                    res_alrm_node_lst = _obj_impl.find_cell_lst(key_txt, dist_txt)
                elif mess.upper().find("TRAMDOWN") >= 0:
                    res_alrm_node_lst = _obj_impl.find_station_lst(key_txt, dist_txt)

                if res_alrm_node_lst:
                    # fix lai neu dua theo dist_txt
                    res_dist_lst = []
                    if dist_txt != '':
                        for x in res_alrm_node_lst:
                            if 'HUYEN' in x:
                                dist_x = x['HUYEN']
                                if dist_x.upper() == dist_txt:
                                    res_dist_lst.append(x)
                    else:
                        res_dist_lst = res_alrm_node_lst
                    # them fan filter vao
                    if key_fnd:
                        res_alrm_node_lst = get_filter_in_list(res_alrm_node_lst, key_filter=key_fnd)
                        res_dist_lst = get_filter_in_list(res_dist_lst, key_filter=key_fnd)
                    if mess.upper().find("TRAMDOWN") >= 0:
                        for x in res_dist_lst:
                            if 'SO_TU' in x:
                                totl += x['SO_TU']
                    else:
                        totl = len(res_dist_lst)

                    if mess.upper().find("TUDOWN") >= 0:
                        res_info = "Danh sách tủ down của " + str(key_txt) + "  (" + str(totl) + " tủ):\n"
                    elif mess.upper().find("TRAMDOWN") >= 0:
                        totl_sttn = len(res_dist_lst)
                        res_info = "Danh sách trạm có tủ down của " + str(key_txt) + "  (" + str(
                            totl_sttn) + " trạm, " + str(totl) + " tủ):\n"
                    else:
                        res_info = "Danh sách trạm down của " + str(key_txt) + "  (" + str(totl) + " trạm):\n"

                    for res_alrm in res_dist_lst:
                        for key, val in res_alrm.items():
                            if key != 'TINH':
                                if key != 'MA_THIETBI':
                                    res_info += "   " + str(key) + " :" + str(val) + " "
                                else:
                                    res_info += "   " + str(val) + " "
                        res_info += "\n"
                else:
                    res_info = "Không có cảnh báo nào"

        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
        return res_info

    def check_pwr_off_high_temp(self, mess, key_fnd=''):
        _now = get_date_now()
        words = mess.split()
        n = len(words)
        res_info = ''
        dist_txt = ''
        res_alrm_node_lst = []
        if n > 2:
            for x in words[2:]:
                dist_txt += " " + x.upper()
        else:
            dist_txt = ''
        dist_txt = dist_txt.strip()
        if n >= 2:
            key_txt = words[1].upper()
            res_lst = []
            _obj_impl = TblNocproImpl(self.pool_nocpro)
            print("Pool Gpon Busy: " + str(_obj_impl.pool.busy))

            totl = 0
            if mess.upper().find("NDCTT") >= 0:
                res_alrm_node_lst = _obj_impl.find_high_temp_bb_lst(key_txt, dist_txt)

            elif mess.upper().find("NDC") >= 0:
                res_alrm_node_lst = _obj_impl.find_high_temp_lst(key_txt, dist_txt)
            elif mess.upper().find("MDTT") >= 0:
                res_alrm_node_lst = _obj_impl.find_power_off_bb_lst(key_txt, dist_txt)
            elif mess.upper().find("MD") >= 0:
                res_alrm_node_lst = _obj_impl.find_pwr_off_lst(key_txt, dist_txt)
            elif mess.upper().find("MCTT") >= 0:
                res_alrm_node_lst = _obj_impl.find_open_door_bb_lst(key_txt, dist_txt)

            if res_alrm_node_lst:
                # fix lai neu dua theo dist_txt
                if key_fnd != '':
                    res_alrm_node_lst = get_filter_in_list(res_alrm_node_lst, key_filter=key_fnd)
                totl = len(res_alrm_node_lst)
                if mess.upper().find("NDCTT") >= 0:
                    res_info = "Danh sách trạm trục có nhiệt độ cao " + "  (" + str(totl) + " trạm):\n"
                elif mess.upper().find("NDC") >= 0:
                    res_info = "Danh sách trạm có nhiệt độ cao " + "  (" + str(totl) + " trạm):\n"
                elif mess.upper().find("MDTT") >= 0:
                    res_info = "Danh sách trạm trục có cảnh báo mất điện (" + str(totl) + " trạm):\n"
                elif mess.upper().find("MD") >= 0:
                    res_info = "Danh sách trạm có cảnh báo mất điện (" + str(totl) + " trạm):\n"
                elif mess.upper().find("MCTT") >= 0:
                    res_info = "Danh sách trạm trục có cảnh báo mở cửa (" + str(totl) + " trạm):\n"

                for res_alrm in res_alrm_node_lst:
                    for key, val in res_alrm.items():
                        res_info += "   " + str(key) + " :" + str(val) + " "
                    res_info += "\n"
            else:
                res_info = "Không có cảnh báo nào"

        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
        return res_info

    def check_node_down(self, mess, key_fnd=''):
        _now = get_date_now()
        words = mess.split()
        n = len(words)
        res_info = ''
        dist_txt = ''
        res_alrm_node_lst = []

        if n >= 2:
            key_txt = words[1].upper()
            res_lst = []
            _obj_impl = TblNocproImpl(self.pool_nocpro)
            _obj_ring_impl = TblRingInfoImpl(None)
            print("Pool Gpon Busy: " + str(_obj_impl.pool.busy))

            totl = 0
            # tim vong ring cua SRT truoc. Sau do dua vao tim list ring day cai nao down
            ring_lst = _obj_ring_impl.search_single(**dict(searchRingSrt=str(key_txt)))
            node_agg_lst = []
            res_alrm_node_agg_lst = []
            res_down_lst = []
            node_lst = []
            agg01 = ''
            agg02 = ''
            srt01 = ''
            srt02 = ''
            ring_srt = ''

            if ring_lst:
                # do co the ring list nhieu nen ta ghep list lai nhieu node.
                # Lay ring srt la key trong moi ring lst. Ngan cach boi dau ";"

                for x in ring_lst:
                    chck_ring = True

                    if 'agg01' in x:

                        if x['agg01'].find('PRT') >= 0:
                            chck_ring = False
                        else:
                            agg01 = x['agg01'].strip()

                    if 'agg02' in x:
                        if x['agg02'].find('PRT') >= 0:
                            chck_ring = False
                        else:
                            agg02 = x['agg02'].strip()

                    if 'srt1' in x and chck_ring:
                        srt01 = x['srt1'].strip()

                    if 'srt2' in x and chck_ring:
                        srt02 = x['srt2'].strip()

                    if 'ring_srt' in x and chck_ring:
                        ring_srt = x['ring_srt']

                    if chck_ring:
                        if agg01:
                            node_agg_lst.append(agg01)
                        if agg02:
                            node_agg_lst.append(agg02)
                        if str(ring_srt).find(";") >= 0:
                            node_lst += ring_srt.split(";")
                        else:
                            node_lst.append(ring_srt)

                res_alrm_node_lst = _obj_impl.find_node_acc_down(node_lst)
                res_alrm_node_agg_lst = _obj_impl.find_node_core_down(node_agg_lst)

            if key_fnd != '':
                res_alrm_node_lst = get_filter_in_list(res_alrm_node_agg_lst, key_filter=key_fnd)
                node_agg_lst = get_filter_in_list(node_agg_lst, key_filter=key_fnd)
            totl = len(res_alrm_node_lst)
            res_info += "Danh sách AGG đầu Ring SRT: \n"
            if totl > 0:
                for x in res_alrm_node_agg_lst:
                    for key, val in x.items():
                        if val == agg01:
                            res_info += "<b> AGG01:" + str(val) + " - DOWN" + "</b>\n"
                        elif val == agg02:
                            res_info += "<b> AGG02:" + str(val) + " - DOWN" + "</b>\n"
                        else:
                            res_info += "<b> " + str(val) + " - DOWN" + "</b>\n"
                        res_down_lst.append(val)
            if key_fnd != '':
                node_agg_lst = get_filter_in_list(node_agg_lst, key_filter=key_fnd)
            if node_agg_lst:
                for x in node_agg_lst:
                    if x not in res_down_lst:
                        if x == agg01:
                            res_info += "<b> AGG01:" + str(x) + " - UP" + "</b>\n"
                        elif x == agg02:
                            res_info += "<b> AGG02:" + str(x) + " - UP" + "</b>\n"
                        else:
                            res_info += "<b>" + str(x) + " - UP" + "</b>\n"

            if res_alrm_node_lst:
                # fix lai neu dua theo dist_txt
                if key_fnd != '':
                    res_alrm_node_lst = get_filter_in_list(res_alrm_node_lst, key_filter=key_fnd)
                    node_lst = get_filter_in_list(node_lst, key_filter=key_fnd)
                res_down_lst = [x['NAME'] for x in res_alrm_node_lst]

            res_info += "Danh sách trạm trong Ring SRT: " + "  :\n"

            for x in node_lst:
                # filter bo di OLT va SW:
                if str(x).find('ASW') <= 0 and str(x).find('OLT') <= 0 and str(x) not in node_agg_lst:
                    stat = 'UP'

                    if x in res_down_lst:
                        stat = 'DOWN'
                    if str(x) == srt01:
                        res_info += "   <b> SRT01:" + str(x) + " - " + stat + " </b>\n"
                    elif str(x) == srt02:
                        res_info += "   <b> SRT02:" + str(x) + " - " + stat + " </b>\n"
                    else:
                        res_info += "   " + str(x) + " - " + stat + "\n"

        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
        return res_info

    def check_node_order_down(self, mess, key_fnd=''):
        _now = get_date_now()
        words = mess.split()
        n = len(words)
        res_info = ''
        dist_txt = ''
        res_alrm_node_lst = []

        if n >= 2:
            key_txt = words[1].upper()
            res_lst = []
            _obj_impl = TblNocproImpl(self.pool_nocpro)
            _obj_ring_impl = TblRingInfoOrderImpl()
            print("Pool Gpon Busy: " + str(_obj_impl.pool.busy))

            totl = 0
            # tim vong ring cua SRT truoc. Sau do dua vao tim list ring day cai nao down
            ring_lst = _obj_ring_impl.find_ring_node_code(str(key_txt))
            node_agg_lst = []
            res_alrm_node_agg_lst = []
            res_down_lst = []
            node_lst = []
            agg01 = ''
            agg02 = ''
            srt01 = ''
            srt02 = ''
            ring_srt = ''
            link_type = 0
            link_order = 0
            node_code_rlt = ''
            node_code = ''

            if ring_lst:
                # do co the ring list nhieu nen ta ghep list lai nhieu node.
                # Lay ring srt la key trong moi ring lst. Ngan cach boi dau ";"

                for x in ring_lst:
                    chck_ring = True

                    if 'node_code' in x:
                        node_code = x['node_code']
                        if node_code.find('AGG') >= 0 > node_code.find('SRT'):
                            node_agg_lst.append(node_code)
                            agg01 = node_code
                            if 'node_code_relation' in x:
                                node_code_rlt = x['node_code_relation']
                                srt01 = node_code_rlt
                        elif node_code.find('SRT') >= 0:
                            node_lst.append(node_code)
                    if 'link_type' in x:
                        link_type = x['link_type']
                    if 'link_order' in x:
                        link_order = x['link_order']

                    if 'node_code_relation' in x:
                        node_code_rlt = x['node_code_relation']
                        if node_code_rlt.find('AGG') >= 0 > node_code_rlt.find('SRT'):
                            node_agg_lst.append(node_code_rlt)
                            agg02 = node_code_rlt
                            srt02 = node_code

                res_alrm_node_lst = _obj_impl.find_node_acc_down(node_lst)
                res_alrm_node_agg_lst = _obj_impl.find_node_core_down(node_agg_lst)

            if key_fnd != '':
                res_alrm_node_lst = get_filter_in_list(res_alrm_node_agg_lst, key_filter=key_fnd)
                node_agg_lst = get_filter_in_list(node_agg_lst, key_filter=key_fnd)

            totl = len(res_alrm_node_lst)
            res_info += "Danh sách AGG đầu Ring SRT: \n"
            if totl > 0:
                for x in res_alrm_node_agg_lst:
                    for key, val in x.items():
                        if val == agg01:
                            res_info += "<b> AGG01:" + str(val) + " - DOWN" + "</b>\n"
                        elif val == agg02:
                            res_info += "<b> AGG02:" + str(val) + " - DOWN" + "</b>\n"
                        else:
                            res_info += "<b> " + str(val) + " - DOWN" + "</b>\n"
                        res_down_lst.append(val)
            if key_fnd != '':
                node_agg_lst = get_filter_in_list(node_agg_lst, key_filter=key_fnd)
            if node_agg_lst:
                for x in node_agg_lst:
                    if x not in res_down_lst:
                        if x == agg01:
                            res_info += "<b> AGG01:" + str(x) + " - UP" + "</b>\n"
                        elif x == agg02:
                            res_info += "<b> AGG02:" + str(x) + " - UP" + "</b>\n"
                        else:
                            res_info += "<b>" + str(x) + " - UP" + "</b>\n"

            if res_alrm_node_lst:
                # fix lai neu dua theo dist_txt
                if key_fnd != '':
                    res_alrm_node_lst = get_filter_in_list(res_alrm_node_lst, key_filter=key_fnd)
                    node_lst = get_filter_in_list(node_lst, key_filter=key_fnd)
                res_down_lst = [x['NAME'] for x in res_alrm_node_lst]

            res_info += "Danh sách trạm trong Ring SRT: " + "  :\n"

            for x in node_lst:
                # filter bo di OLT va SW:
                if str(x).find('ASW') <= 0 and str(x).find('OLT') <= 0 and str(x) not in node_agg_lst:
                    stat = 'UP'

                    if x in res_down_lst:
                        stat = 'DOWN'
                    if str(x) == srt01:
                        res_info += "   <b> SRT01:" + str(x) + " - " + stat + " </b>\n"
                    elif str(x) == srt02:
                        res_info += "   <b> SRT02:" + str(x) + " - " + stat + " </b>\n"
                    else:
                        res_info += "   " + str(x) + " - " + stat + "\n"

        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
        return res_info

    def check_node_alarm(self, message):
        _now = get_date_now()
        key_txt = ''
        area_txt = ''
        words = message.split()
        res_drop = ''
        n = len(words)
        res_info = ''
        if n >= 2:
            key_txt = words[1].upper()
            _dev_name = key_txt.upper()
            _area_fnd = ''
            _dev_name_div = ''
            _dev_name_div_lst = []
            res_lst = []

            if _dev_name.find('&') >= 0:
                _dev_name_div_lst = _dev_name.split("&")
                for _dev_name_div in _dev_name_div_lst:
                    _area_fnd = get_area(_dev_name_div)
                    if _area_fnd:
                        break
            else:
                _area_fnd = get_area(_dev_name)
                _dev_name_div = _dev_name

            if _area_fnd:
                _host_impl = HostIpmsImpl(0, '', '', '', '', '')

                _dev_tent_id = get_tenant_id_syslog_from_area(_area_fnd)
                if _dev_tent_id:
                    _obj = SyslogNocproIpmsImpl(_area_fnd)
                    time_now = get_time_format_now()
                    time_last = get_date_minus_format_elk(time_now, 60 * 24)

                    _host_lst = _obj.find_cbs(time_last, _dev_name, True, True)
                    if _host_lst:

                        if _host_lst:
                            res_evnt_lst = _obj.send_to_group_bot_telethon(_host_lst)
                            for res_evnt in res_evnt_lst:
                                res_link = ''
                                res_mess = ''
                                if 'message' in res_evnt:
                                    res_mess = res_evnt['message']
                                if 'link' in res_evnt:
                                    res_link = res_evnt['link']
                                    if res_mess:
                                        return self.send_photo(file_name=res_link, mess_caption=res_mess)
                                    else:
                                        return self.send_mess_channel(res_mess)
                                else:
                                    if res_mess:
                                        return self.send_mess_channel(res_mess)

        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
            return self.send_mess_channel(res_info)

    def check_uctt(self, message, key_fnd=''):
        _now = get_date_now()
        key_txt = ''
        area_txt = ''
        # thay the syntax split "
        # words = message.split()
        words = split_mess_with_uctt(message)
        res_drop = ''
        n = len(words)
        res_info = ''
        if n >= 3:
            mrkt_txt = words[1].upper()
            node_txt = words[2].upper()
            if n == 3:
                symp_txt = ' '
            else:
                symp_txt = words[3].upper()

            _area_fnd = ''
            _dev_name_div = ''
            _dev_name_div_lst = []
            srch_txt = symp_txt + " " + node_txt
            # filter result list dua theo node , market va symptom
            # yeu cau dua ra la node_txt va symp_txt se nam trong ( ) va phan cach nhau bang dau " "
            res_flt_lst = []

            if srch_txt:
                _es = ElasticSearchHost(host=ELASTIC_SERVER_OLD,
                                        port=ELASTIC_PORT)
                res_lst = _es.find(index="tbl_uctt",
                                   doc_type="_doc",
                                   fld_lst=["symptom", "node_code"],
                                   val=srch_txt)
                if res_lst:

                    for x in res_lst:
                        # kiem tra ket qua. neu dung cho vao res output
                        chck_flt = True
                        if 'market' in x:
                            mrkt = x['market'].upper()
                            if mrkt_txt.find(mrkt) < 0:
                                chck_flt = False

                        else:
                            chck_flt = False

                        if 'node_code' in x:
                            node_code = x['node_code'].upper()
                            if node_code.find(node_txt) < 0:
                                chck_flt = False
                        else:
                            chck_flt = False

                        if 'symptom' in x:
                            symp = x['symptom'].upper()
                            symp_fnd = symp_txt.split(" ")
                            for x1 in symp_fnd:
                                if symp.find(x1) < 0:
                                    chck_flt = False
                        else:
                            chck_flt = False

                        if chck_flt:
                            if 'message' not in x:
                                x['message'] = ''

                            res_flt_lst.append(x)

            if res_flt_lst:
                if key_fnd != '':
                    res_flt_lst = get_filter_in_list(res_flt_lst, key_filter=key_fnd)
                num_ord = 1
                for x in res_flt_lst:
                    res_info += "Phương án UCTT số " + str(num_ord) + ":" + "\n"
                    res_info += "Thị trường:" + str(x['market']) + "\n"
                    res_info += "Mạng:" + str(x['network']) + "\n"
                    res_info += "Node:" + str(x['node_code']) + "\n"
                    res_info += "Hiện tượng:" + str(x['symptom']) + "\n"
                    res_info += "Phương án:" + str(x['message']) + "\n"
                    res_info += "Chạy thực tế:" + str(x['run_real']) + "\n"
                    res_info += "Thời gian chạy trong KPI (OK/NOK):" + str(x['kpi_ok']) + "\n"
                    res_info += "Vật tư đảm bảo:" + str(x['equip_guranteed']) + "\n"
                    res_info += "Tổng kết:" + str(x['result']) + "\n"
                    num_ord += 1
                    res_info += "\n"
            else:
                res_info = "Không có tên trạm hoặc tên khu vực hoặc hiện tượng tương ứng"
        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
            return self.send_mess_channel(res_info)
        return self.send_mess_channel(res_info)

    def check_operator_uctt(self, message, key_fnd=''):
        _now = get_date_now()
        key_txt = ''
        area_txt = ''
        # thay the syntax split "
        # words = message.split()
        # thay doi and phai chua ca network va noi dung nua
        words = split_mess_with_uctt(message)
        res_drop = ''
        n = len(words)
        res_info = ''
        if n >= 3:
            # mrkt_txt = words[1].upper()
            # node_txt = words[2].upper()
            symp_txt = words[2].upper()
            netw_txt = words[1].upper()

            _area_fnd = ''
            _dev_name_div = ''
            _dev_name_div_lst = []
            # srch_txt = mrkt_txt + " " + node_txt + " " + symp_txt
            srch_txt = netw_txt + " " + symp_txt
            # filter result list dua theo node , market va symptom
            # yeu cau dua ra la node_txt va symp_txt se nam trong ( ) va phan cach nhau bang dau " "
            res_lst = []

            if srch_txt:
                _es = ElasticSearchHost(host=ELASTIC_SERVER_OLD,
                                        port=ELASTIC_PORT)
                res_lst = _es.find(index="tbl_operator_uctt",
                                   doc_type="_doc",
                                   fld_lst=["incident"],
                                   val=srch_txt)

            if res_lst:
                if key_fnd != '':
                    res_lst = get_filter_in_list(res_lst, key_filter=key_fnd)
                num_ord = 1
                for x in res_lst:
                    try:
                        # kiem tra chinh xac co network va inciden khongkhong
                        incd = str(x['incident']).upper()
                        if incd.find(netw_txt) >= 0 and incd.find(symp_txt) >= 0:
                            res_info += "Điều hành phương án UCTT số " + str(num_ord) + ":" + "\n"
                            res_info += "Sự cố:" + str(x['incident']) + "\n"
                            res_info += "Mức độ:" + str(x['level']) + "\n"
                            res_info += "Khoanh vùng:" + str(x['staking']) + "\n"
                            res_info += "Báo cáo:" + str(x['report']) + "\n"
                            res_info += "Kịch bản:" + str(x['script']) + "\n"
                            res_info += "Phương án:" + str(x['method']) + "\n"
                            res_info += "Thời gian:" + str(x['time']) + "\n"
                            num_ord += 1
                            res_info += "\n"

                    except Exception as err:
                        res_info += "Error " + str(err) + "\n"
            else:
                res_info = "Không có tên trạm hoặc tên khu vực hoặc hiện tượng tương ứng"
        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
            return self.send_mess_channel(res_info)
        return self.send_mess_channel(res_info)

    def check_user(self, message):
        _now = get_date_now()
        key_txt = ''
        area_txt = ''
        words = message.split()
        res_drop = ''
        n = len(words)
        if n >= 3:
            key_txt = words[1]
            area_txt = words[2].upper()
        elif n == 2:
            key_txt = words[1]
            area_txt = get_area_from_acc(str(key_txt))

        _obj_gpon_impl = TblGponOltSubImpl(self.pool_nocpro)
        print("Pool Gpon Busy: " + str(_obj_gpon_impl.pool.busy))
        res_info = ''
        if key_txt and area_txt:
            _obj_aaa_impl = TblOnlineAaaImpl(area_txt)
            _acc_stat_lst = _obj_aaa_impl.find_acc_status([key_txt])
            try:
                if _acc_stat_lst:
                    _acc_stat = _acc_stat_lst[0]['status']
                    _acc_pos = _acc_stat_lst[0]['nasportid']
                    _acc_last_time = _acc_stat_lst[0]['lasttime']
                    _acc_ip_addr = _acc_stat_lst[0]['ipaddr']
                    _acc_service_class = _acc_stat_lst[0]['serviceclass']
                    _acc_policy_up = _acc_stat_lst[0]['policy_up']
                    _acc_policy_down = _acc_stat_lst[0]['policy_down']
                    _acc_mac = _acc_stat_lst[0]['mac']
                    if _acc_pos[-2:] == '35':
                        _acc_pos = _acc_pos[:-2]

                    if 'brasname' in _acc_stat_lst[0]:
                        _acc_pos_bras = _acc_stat_lst[0]['brasname']
                    else:
                        _acc_pos_bras = 'Unknown'

                    if _acc_stat == 'stop':
                        # tim nguyen nhan
                        _acc_rsn_lst = _obj_gpon_impl.find_trouble_type_acc(key_txt)
                        if _acc_rsn_lst:
                            _acc_rsn = _acc_rsn_lst[0]['TROUBLE_TYPE']
                            _acc_down_time = _acc_rsn_lst[0]['START_TIME']

                            res_info = 'Account ' + str(key_txt) + " at " + str(_acc_pos) + \
                                       "\n BRAS " + str(_acc_pos_bras) + "  MAC:" + "\n" + str(_acc_mac) + "  OFFLINE" + "\n" \
                                                                                       " Loại đăng nhập: " + str(
                                _acc_service_class) + "\n" \
                                                      " Tốc độ up: " + str(_acc_policy_up) + "  Tốc độ down: " + str(
                                _acc_policy_down) + "\n" \
                                                    'Lý do: ' + str(_acc_rsn) + "\n" \
                                                                                'Thời gian down: ' + str(_acc_down_time)
                        else:
                            res_info = 'Account ' + str(key_txt) + " at " + str(_acc_pos) + \
                                       "\n BRAS " + str(_acc_pos_bras) + "  MAC:" + str(_acc_mac)  \
                                       + " OFFLINE " + "\n " \
                                                       "Tốc độ up: " + str(_acc_policy_up) + "  Tốc độ down: " + str(
                                _acc_policy_down) + "\n" \
                                                    '\n Lý do: Không rõ nguyên nhân' + "\n" \
                                                                                       'Thời gian down: ' + str(
                                _acc_last_time)
                    else:
                        time_strt_onl = _acc_stat_lst[0]['starttime']
                        if _acc_ip_addr:
                            res_info = 'Account ' + str(key_txt) +  "  MAC:" + "\n" + str(_acc_mac) + " IP:" + _acc_ip_addr + " at " + str(_acc_pos) + \
                                       "\n BRAS " + str(_acc_pos_bras) + " ONLINE from " \
                                                                         "" + str(
                                time_strt_onl) + "\n" + " Tốc độ up: " + str(_acc_policy_up) + "  Tốc độ down: " + str(
                                _acc_policy_down) + "\n Loại đăng nhập: " + str(_acc_service_class)
                        else:
                            res_info = 'Account ' + str(key_txt) + "  MAC:" + "\n" + str(_acc_mac) +  " at " + str(_acc_pos) + \
                                       "\n BRAS " + str(_acc_pos_bras) + " ONLINE from " \
                                                                         "" + str(
                                time_strt_onl) + "\n" + " Tốc độ up: " + str(_acc_policy_up) + "  Tốc độ down: " + str(
                                _acc_policy_down) + "\n Loại đăng nhập: " + str(_acc_service_class)
            except Exception as err:
                return self.send_mess_channel(str(err))

            if res_info:
                return self.send_mess_channel(str(res_info))
            else:
                res_info = 'Account này đã offline ít nhất 1 ngày trước'
                return self.send_mess_channel(str(res_info))
        else:
            res_info = "Không có tên account hoặc tên khu vực tương ứng"
            return self.send_mess_channel(str(res_info))

    def check_user_from_ip(self, message):
        _now = get_date_now()
        key_txt = ''
        area_txt = ''
        words = message.split()
        res_drop = ''
        n = len(words)
        area_txt_lst = []
        if n >= 3:
            key_txt = words[1]
            area_txt_lst = [words[2].upper()]
        elif n == 2:
            key_txt = words[1]
            #area_txt = get_area_from_acc(str(key_txt))
            area_txt_lst = ['KV1', 'KV2', 'KV3']
        # do IP la 1 list nen lay lam 3 khu vuc

        _obj_gpon_impl = TblGponOltSubImpl(self.pool_nocpro)
        print("Pool Gpon Busy: " + str(_obj_gpon_impl.pool.busy))
        res_info = ''
        if key_txt and area_txt_lst:
            for area_txt in area_txt_lst:
                _obj_aaa_impl = TblOnlineAaaImpl(area_txt)
                _acc_stat_lst = _obj_aaa_impl.find_acc_from_ip(key_txt)
                try:
                    if _acc_stat_lst:
                        # do 1 ip co the co nhieu user nen ta day ra lam nhieu danh sach
                        for _acc_stat_x in _acc_stat_lst:
                            _acc_stat = _acc_stat_x['status']
                            _acc_username = _acc_stat_x['username']
                            _acc_pos = _acc_stat_x['nasportid']
                            _acc_last_time = _acc_stat_x['lasttime']
                            _acc_ip_addr = _acc_stat_x['ipaddr']
                            _acc_service_class = _acc_stat_x['serviceclass']
                            _acc_policy_up = _acc_stat_x['policy_up']
                            _acc_policy_down = _acc_stat_x['policy_down']
                            if _acc_pos[-2:] == '35':
                                _acc_pos = _acc_pos[:-2]

                            if 'brasname' in _acc_stat_x:
                                _acc_pos_bras = _acc_stat_x['brasname']
                            else:
                                _acc_pos_bras = 'Unknown'

                            if _acc_stat == 'stop':
                                # tim nguyen nhan
                                _acc_rsn_lst = _obj_gpon_impl.find_trouble_type_acc(key_txt)
                                if _acc_rsn_lst:
                                    _acc_rsn = _acc_rsn_lst[0]['TROUBLE_TYPE']
                                    _acc_down_time = _acc_rsn_lst[0]['START_TIME']

                                    res_info += 'IP ' + str(key_txt) + " username là" + str(
                                        _acc_username) + " at " + str(_acc_pos) + \
                                                "\n BRAS " + str(_acc_pos_bras) + "  OFFLINE" + "\n" \
                                                                                                " Loại đăng nhập: " + str(
                                        _acc_service_class) + "\n" \
                                                              " Tốc độ up: " + str(
                                        _acc_policy_up) + "  Tốc độ down: " + str(
                                        _acc_policy_down) + "\n" \
                                                            'Lý do: ' + str(_acc_rsn) + "\n" \
                                                                                        'Thời gian down: ' + str(
                                        _acc_down_time)
                                else:
                                    res_info += 'IP ' + str(key_txt) + " username là" + str(
                                        _acc_username) + " at " + str(_acc_pos) + \
                                                "\n BRAS " + str(_acc_pos_bras) \
                                                + " OFFLINE " + "\n " \
                                                                "Tốc độ up: " + str(
                                        _acc_policy_up) + "  Tốc độ down: " + str(
                                        _acc_policy_down) + "\n" \
                                                            '\n Lý do: Không rõ nguyên nhân' + "\n" \
                                                                                               'Thời gian down: ' + str(
                                        _acc_last_time)
                            else:
                                time_strt_onl = _acc_stat_x['starttime']
                                if _acc_ip_addr:
                                    res_info += 'IP ' + str(key_txt) + " username:" + str(_acc_username) + " at " + str(
                                        _acc_pos) + \
                                               "\n BRAS " + str(_acc_pos_bras) + " ONLINE from " \
                                                                                 "" + str(
                                        time_strt_onl) + "\n" + " Tốc độ up: " + str(
                                        _acc_policy_up) + "  Tốc độ down: " + str(
                                        _acc_policy_down) + "\n Loại đăng nhập: " + str(_acc_service_class)
                                else:
                                    res_info += 'IP ' + str(key_txt) + " username:" + str(_acc_username) + " at " + str(
                                        _acc_pos) + \
                                               "\n BRAS " + str(_acc_pos_bras) + " ONLINE from " \
                                                                                 "" + str(
                                        time_strt_onl) + "\n" + " Tốc độ up: " + str(
                                        _acc_policy_up) + "  Tốc độ down: " + str(
                                        _acc_policy_down) + "\n Loại đăng nhập: " + str(_acc_service_class)
                            res_info += "\n"
                except Exception as err:
                    return self.send_mess_channel(str(err))

            if res_info:
                return self.send_mess_channel(str(res_info))
            else:
                res_info = 'Account này đã offline ít nhất 1 ngày trước'
                return self.send_mess_channel(str(res_info))
        else:
            res_info = "Không có tên account hoặc tên khu vực tương ứng"
            return self.send_mess_channel(str(res_info))

    def check_tv(self, message):
        _now = get_date_now()
        key_txt = ''
        area_txt = ''
        words = message.split()
        res_drop = ''
        n = len(words)
        if n >= 2:
            key_txt = words[1]

        elif n == 2:
            key_txt = words[1]

        _obj_impl = QosApiServer(str(key_txt))
        res_info = ''
        if key_txt:
            res_xml = _obj_impl.request()

            try:
                if res_xml:
                    res_dct = convert_api_television_check_user_to_dct(res_xml)
                    if res_dct:
                        res_info += "Kết quả kiểm tra account " + str(key_txt) + " của " + str(area_txt) + ":\n"
                        for k, v in res_dct.items():
                            res_info += str(k) + ": " + str(v) + "\n"

                        return self.send_mess_channel(str(res_info))

                    else:
                        res_info = "Không có phản hồi API"
                        return res_info
                else:
                    res_info = "Không có phản hồi API"
                    return res_info
            except Exception as err:
                return self.send_mess_channel(str(err))

        else:
            res_info = "Không có tên account tương ứng"
            return self.send_mess_channel(str(res_info))

    def list_user_internet_in_device(self, message):
        _now = get_date_now()
        key_txt = ''
        area_txt = ''
        words = message.split()
        num_str = ''
        n = len(words)
        if n >= 3:
            key_txt = words[1]
            num_str = words[2]
        elif n == 2:
            key_txt = words[1]
        area_txt = get_area(str(key_txt))

        _obj = TblOnlineAaaImpl(area_txt)

        res_info = ''
        if key_txt:
            res_lst = _obj.find_dev(key_txt)

            try:
                if res_lst:
                    res_info += "Danh sách khách hàng của trạm " + str(key_txt) + " của " + str(area_txt) + ":\n"
                    num = convert_string_to_int(num_str)
                    n = len(res_lst)
                    if num > 0:
                        if n > num:
                            n = num
                    else:
                        if n > 1000:
                            n = 1000
                    num = 0
                    for res_dct in res_lst:
                        for k, v in res_dct.items():
                            if num == 0:
                                res_info += str(v)
                            else:
                                res_info += "," + str(v)
                        num += 1
                        if num > n:
                            break
                    return self.send_mess_channel(str(res_info))

                else:
                    res_info = "Không có phản hồi API"
                    return res_info
            except Exception as err:
                return self.send_mess_channel(str(err))

        else:
            res_info = "Không có tên trạm tương ứng"
            return self.send_mess_channel(str(res_info))

    def list_user_tv_in_device(self, message):
        _now = get_date_now()
        key_txt = ''
        area_txt = ''
        words = message.split()
        num_str = ''
        n = len(words)
        if n >= 3:
            key_txt = words[1]
            num_str = words[2]
        elif n == 2:
            key_txt = words[1]
            area_txt = get_area(str(key_txt))

        _obj = TblGponOltSubImpl(self.pool_nocpro)

        res_info = ''
        if key_txt:
            res_lst = _obj.get_sub_multiscreen(key_txt)

            try:
                if res_lst:
                    res_info += "Danh sách khách hàng Multiscreen của trạm " + str(key_txt) + " của " + str(
                        area_txt) + ":\n"
                    num = convert_string_to_int(num_str)
                    n = len(res_lst)
                    if num > 0:
                        if n > num:
                            n = num
                    else:
                        if n > 10:
                            n = 10
                    num = 0
                    for res_dct in res_lst:
                        for k, v in res_dct.items():
                            if num == 0:
                                res_info += str(v)
                            else:
                                res_info += "," + str(v)
                        num += 1
                        if num > n:
                            break
                    return self.send_mess_channel(str(res_info))

                else:
                    res_info = "Không có phản hồi API"
                    return res_info
            except Exception as err:
                return self.send_mess_channel(str(err))

        else:
            res_info = "Không có tên trạm tương ứng"
            return self.send_mess_channel(str(res_info))

    def check_web(self, message):
        _now = get_date_now()
        key_txt = ''
        area_txt = ''
        words = message.split()
        res_drop = ''
        n = len(words)
        if n >= 2:
            key_txt = words[1]

        elif n == 2:
            key_txt = words[1]

        _obj_impl = TblDenyWeb(web="tester",
                               usr="",
                               cv="cv",
                               usr_order="usr",
                               timestamp=None,
                               usr_acc="acc",
                               time_acc=None,
                               status=0,
                               time_check=None,
                               deny_or_not='',
                               status_run=''
                               )
        res_info = ''

        if key_txt:
            res_dct = _obj_impl.find(web_name=str(key_txt))

            try:
                if res_dct:
                    res_info += "Kết quả kiểm tra web " + str(key_txt) + ":\n"
                    for k, v in res_dct.items():
                        res_info += str(k) + ": " + str(v) + "\n"

                    return self.send_mess_channel(str(res_info))
                else:
                    res_info = "Không có phản hồi API"
                    return res_info
            except Exception as err:
                return self.send_mess_channel(str(err))

        else:
            res_info = "Không có tên account tương ứng"
            return self.send_mess_channel(str(res_info))

    def check_msc_info(self, message, key_fnd):
        _now = get_date_now()
        key_txt = ''
        words = message.split()
        n = len(words)
        if n >= 2:
            key_txt = words[1]

        elif n == 2:
            key_txt = words[1]

        _obj_impl = TblBssNpmsImpl()

        res_info = ''
        if key_txt:
            res_vend_dct_lst = _obj_impl.get_vendor_node(key_txt)
            res_sub_dct_lst = _obj_impl.get_sub_from_node(key_txt)
            res_prvn_msc_dct_lst = _obj_impl.get_prvn_from_node(key_txt)
            res_bsc_from_msc_dct_lst = _obj_impl.get_bsc_rnc_from_node(key_txt)
            if key_fnd != '':
                res_vend_dct_lst = get_filter_in_list(res_vend_dct_lst, key_filter=key_fnd)
                res_sub_dct_lst = get_filter_in_list(res_sub_dct_lst, key_filter=key_fnd)
                res_prvn_msc_dct_lst = get_filter_in_list(res_prvn_msc_dct_lst, key_filter=key_fnd)
                res_bsc_from_msc_dct_lst = get_filter_in_list(res_bsc_from_msc_dct_lst, key_filter=key_fnd)

            try:
                if res_vend_dct_lst:
                    res_info += "Kết quả kiểm tra " + str(key_txt) + ":\n"
                    for x in res_vend_dct_lst:
                        for k, v in x.items():
                            res_info += str(k) + ": " + str(v) + "\n"
                    for x in res_sub_dct_lst:
                        for k, v in x.items():
                            res_info += str(k) + ": " + str(v) + "\n"
                    frst_run = 0
                    for x in res_prvn_msc_dct_lst:
                        for k, v in x.items():
                            if frst_run == 0:
                                res_info += str(k) + ": " + str(v)
                            else:
                                res_info += "," + str(v)
                            frst_run += 1
                    res_info += "\n"

                    frst_run = 0
                    for x in res_bsc_from_msc_dct_lst:
                        for k, v in x.items():
                            if frst_run == 0:
                                res_info += str(k) + ": " + str(v)
                            else:
                                res_info += "," + str(v)
                            frst_run += 1
                    res_info += "\n"

                    return self.send_mess_channel(str(res_info))
                else:
                    res_info = "Không có phản hồi API"
                    return res_info
            except Exception as err:
                return self.send_mess_channel(str(err))

        else:
            res_info = "Không có tên account tương ứng"
            return self.send_mess_channel(str(res_info))

    def check_bsc_rnc_info(self, message, key_fnd=''):
        _now = get_date_now()
        key_txt = ''
        words = message.split()
        n = len(words)
        if n >= 2:
            key_txt = words[1]

        elif n == 2:
            key_txt = words[1]

        _obj_impl = TblBssNpmsImpl()
        _obj_bss_noc_impl = TblBssCoreImpl(self.pool_nocpro)
        res_info = ''
        if key_txt:
            res_vend = _obj_bss_noc_impl.get_vendor(key_txt)
            res_sub_dct_lst = _obj_impl.get_sub_on_bsc_rnc_from_node(key_txt)
            res_prvn_msc_dct_lst = _obj_impl.get_prvn_from_bsc_rnc_from_node(key_txt)
            res_msc_from_bsc_dct_lst = _obj_impl.get_msc_from_bsc_rnc_from_node(key_txt)
            if key_fnd != '':
                res_sub_dct_lst = get_filter_in_list(res_sub_dct_lst, key_filter=key_fnd)
                res_prvn_msc_dct_lst = get_filter_in_list(res_prvn_msc_dct_lst, key_filter=key_fnd)
                res_msc_from_bsc_dct_lst = get_filter_in_list(res_msc_from_bsc_dct_lst, key_filter=key_fnd)

            try:
                if True:
                    res_info += "Kết quả kiểm tra " + str(key_txt) + ":\n"

                    res_info += "Vendor:" + str(res_vend) + "\n"
                    for x in res_sub_dct_lst:
                        for k, v in x.items():
                            res_info += str(k) + ": " + str(v) + "\n"

                    for x in res_prvn_msc_dct_lst:
                        for k, v in x.items():
                            res_info += str(k) + ": " + str(v) + "\n"
                    frst_run = 0
                    for x in res_msc_from_bsc_dct_lst:
                        for k, v in x.items():
                            if frst_run == 0:
                                res_info += str(k) + ": " + str(v)
                            else:
                                res_info += "," + str(v)
                            frst_run += 1
                    res_info += "\n"

                    return self.send_mess_channel(str(res_info))

            except Exception as err:
                return self.send_mess_channel(str(err))

        else:
            res_info = "Không có tên account tương ứng"
            return self.send_mess_channel(str(res_info))

    def check_sub_province(self, message):
        _now = get_date_now()
        key_txt = ''
        words = message.split()
        n = len(words)
        if n >= 2:
            key_txt = words[1]

        elif n == 2:
            key_txt = words[1]

        _obj_impl = TblBssNpmsImpl()
        res_info = ''
        if key_txt:

            res_sub_dct_lst = _obj_impl.get_subs_from_prvn(key_txt)
            try:
                if res_sub_dct_lst:
                    res_info += "Kết quả kiểm tra thuê bao theo tỉnh " + str(key_txt) + ":\n"
                    for x in res_sub_dct_lst:
                        for k, v in x.items():
                            res_info += str(k) + ": " + str(v) + "\n"

                    return self.send_mess_channel(str(res_info))
                else:
                    res_info = "Không có phản hồi API"
                    return res_info
            except Exception as err:
                return self.send_mess_channel(str(err))

        else:
            res_info = "Không có tên account tương ứng"
            return self.send_mess_channel(str(res_info))

    def check_sub_country(self, message):
        _now = get_date_now()
        key_txt = '123'
        words = message.split()
        n = len(words)
        if n >= 2:
            key_txt = words[1]

        elif n == 2:
            key_txt = words[1]

        _obj_impl = TblBssNpmsImpl()
        res_info = ''
        if key_txt:

            res_sub_dct_lst = _obj_impl.get_subs_all()
            try:
                if res_sub_dct_lst:
                    res_info += "Kết quả kiểm tra thuê bao cả nước :\n"
                    for x in res_sub_dct_lst:
                        for k, v in x.items():
                            res_info += str(k) + ": " + str(v) + "\n"

                    return self.send_mess_channel(str(res_info))
                else:
                    res_info = "Không có phản hồi API"
                    return res_info
            except Exception as err:
                return self.send_mess_channel(str(err))

        else:
            res_info = "Không có tên account tương ứng"
            return self.send_mess_channel(str(res_info))

    def check_cell_location(self, message, key_fnd=''):
        _now = get_date_now()
        key_txt = ''
        words = message.split()
        n = len(words)
        if n >= 2:
            key_txt = words[1]

        elif n == 2:
            key_txt = words[1]

        _obj_impl = TblBssNpmsImpl()
        res_info = ''
        if key_txt:
            res_sub_dct_lst = _obj_impl.get_cell_loct_info(key_txt)
            try:
                if res_sub_dct_lst:
                    if key_fnd != '':
                        res_sub_dct_lst = get_filter_in_list(res_sub_dct_lst, key_filter=key_fnd)
                    res_info += "Kết quả kiểm tra thông tin vị trí cell :\n"

                    for x in res_sub_dct_lst:
                        for k, v in x.items():
                            res_info += str(k) + ": " + str(v) + "\n"

                    return self.send_mess_channel(str(res_info))
                else:
                    res_info = "Không có phản hồi API"
                    return res_info
            except Exception as err:
                return self.send_mess_channel(str(err))

        else:
            res_info = "Không có tên account tương ứng"
            return self.send_mess_channel(str(res_info))

    def find_pos_down_pctt(self, message, key_fnd=''):
        _now = get_date_now()
        key_txt = ''
        words = message.split()
        n = len(words)
        if n >= 2:
            key_txt = words[1]

        elif n == 2:
            key_txt = words[1]

        _obj_impl = TblPcttAppImpl()
        _obj_nocpro_impl = TblBssCoreImpl(self.pool_nocpro)
        res_info = ''
        res_dist_lst = []
        node_lst = []
        res_sub_down_dct_lst = []
        if key_txt:
            res_sub_dct_lst = _obj_impl.get_pos_down_lst(key_txt)
            try:
                if res_sub_dct_lst:

                    # tim them vi tri down
                    for x in res_sub_dct_lst:
                        if 'vitri_down' in x:
                            if x['vitri_down'] > 0:
                                node_lst.append(x['station_code'])

                    # lay danh sach vi tri

                    res_dist_station_lst = _obj_nocpro_impl.find_district_node_lst(node_lst)

                    for x in res_sub_dct_lst:
                        if 'vitri_down' in x:
                            if x['vitri_down'] > 0:
                                # tim ra ma huyen
                                if 'station_code' in x:
                                    stat_code = x['station_code']
                                    for res_dist in res_dist_station_lst:
                                        if res_dist['STATION_CODE_NIMS'] == stat_code:
                                            x['HUYEN'] = res_dist['MA_HUYEN']
                                res_sub_down_dct_lst.append(x)

                    if key_fnd != '':
                        res_sub_down_dct_lst = get_filter_in_list(res_sub_down_dct_lst, key_filter=key_fnd)
                    res_info += "Kết quả kiểm tra thông tin vị trí down (" + str(
                        len(res_sub_down_dct_lst)) + " trạm) :\n"
                    for x in res_sub_down_dct_lst:
                        num = 0
                        for k, v in x.items():
                            if str(k) != 'STATION_id' and str(k) != 'vitri_down':
                                if k == 'station_code':
                                    k = 'Trạm'
                                if k == 'total_cabinet':
                                    k = 'Số_Tủ'
                                if k == 'cabinet_down':
                                    k = 'Tủ_Down'
                                if num == 0:
                                    res_info += "    " + str(k) + ": " + str(v)
                                else:
                                    res_info += ", " + str(k) + ": " + str(v)
                                num += 1
                        res_info += "\n"

                    return self.send_mess_channel(str(res_info))
                else:
                    res_info = "Không có phản hồi API"
                    return res_info
            except Exception as err:
                return self.send_mess_channel(str(err))

        else:
            res_info = "Không có tên account tương ứng"
            return self.send_mess_channel(str(res_info))

    def check_user_rx(self, message):
        _now = get_date_now()
        key_txt = ''
        area_txt = ''
        words = message.split()
        res_drop = ''
        n = len(words)
        if n >= 3:
            key_txt = words[1]
            area_txt = words[2].upper()
        elif n == 2:
            key_txt = words[1]
            area_txt = get_area_from_acc(str(key_txt))
        res_info = ""
        # tao request
        _acc_stat_lst = []
        inp_dct = dict(template_id=1,
                       param_inpt_dct=dict(area_code=area_txt,
                                           nodeCode='HNI2623OLT01',
                                           accountService=key_txt))
        # Luc dau post data lay ve template id. Sau do dung template id day de lay du lieu ve
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_FLASK_PORT) + "/vmsa/template"
        headers_json = {"Content-Type": "application/json"}
        body_json = json.dumps(inp_dct)

        # r = requests.post(url_api, headers=headers_json, data=body_json)
        r = api_post_retry(url_api, headers_json, body_json)
        temp_init_id = 0
        if r:
            if r.ok:
                # lay ve template id
                temp_init_id = r.text
            # dinh ky 30s /1 lan check ket qua
            if temp_init_id:
                chck_res = False
                url_api += "/" + str(temp_init_id)
                step_count = 0
                while not chck_res:
                    step_count += 1
                    r = api_get_retry(url_api, headers_json)
                    if r.ok:
                        # lay ve template id
                        result = r.text
                        if result.upper() == "FALSE":
                            res_info = "Không có kết quả"
                            return res_info
                        elif result.upper() == 'NOT FINISH':
                            # tiep tuc cho
                            time.sleep(30)
                        else:
                            _acc_stat_lst = json.loads(r.text)
                            chck_res = True
                    else:
                        time.sleep(30)
                    if step_count > 10:
                        chck_res = True
                        break
        else:
            res_info = "Không có phản hồi API"
            return res_info

        if _acc_stat_lst:
            res_info += "Kết quả kiểm tra account " + str(key_txt) + " của " + str(area_txt) + ":\n"
            for _acc_stat in _acc_stat_lst:
                for k, v in _acc_stat.items():
                    res_info += str(k) + ": " + str(v) + "\n"

            return self.send_mess_channel(str(res_info))

        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
            return self.send_mess_channel(str(res_info))

    def check_list_user_olt(self, message):
        _now = get_date_now()
        key_txt = ''
        area_txt = ''
        words = message.split()
        res_drop = ''
        n = len(words)
        if n >= 3:
            key_txt = words[1]
            area_txt = words[2].upper()
        elif n == 2: 
            key_txt = words[1]
            area_txt = get_area_from_acc(str(key_txt))
        res_info = ""
        # tao request
        _acc_stat_lst = []
        inp_dct = dict(template_id=11,
                       param_inpt_dct=dict(area_code=area_txt,
                                           nodeCode='HNI2623OLT01',
                                           accountService=key_txt))
        # Luc dau post data lay ve template id. Sau do dung template id day de lay du lieu ve
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_FLASK_PORT) + "/vmsa/template"
        headers_json = {"Content-Type": "application/json"}
        body_json = json.dumps(inp_dct)

        # r = requests.post(url_api, headers=headers_json, data=body_json)
        r = api_post_retry(url_api, headers_json, body_json)
        temp_init_id = 0
        if r:
            if r.ok:
                # lay ve template id
                temp_init_id = r.text
            # dinh ky 30s /1 lan check ket qua
            if temp_init_id:
                chck_res = False
                url_api += "/" + str(temp_init_id)
                step_count = 0
                while not chck_res:
                    step_count += 1
                    r = api_get_retry(url_api, headers_json)
                    if r.ok:
                        # lay ve template id
                        result = r.text
                        if result.upper() == "FALSE":
                            res_info = "Không có kết quả"
                            return res_info
                        elif result.upper() == 'NOT FINISH':
                            # tiep tuc cho
                            time.sleep(30)
                        else:
                            _acc_stat_lst = json.loads(r.text)
                            for _acc_stat in _acc_stat_lst:
                                for k, v in _acc_stat.items():
                                    # them phan neu value co NOK
                                    if str(k).strip() == 'Result_All':
                                        chck_res = True
                                        break
                    else:
                        time.sleep(30)
                    if step_count > 60:
                        chck_res = True
                        break
        else:
            res_info = "Không có phản hồi API"
            return res_info

        if _acc_stat_lst:
            res_info += "Kết quả kiểm tra account " + str(key_txt) + " của " + str(area_txt) + ":\n"
            for _acc_stat in _acc_stat_lst:
                for k, v in _acc_stat.items():
                    # them phan neu value co NOK
                    if str(v).strip() != '':
                        if str(v).upper().find("NOK") >= 0:
                            tmp_html = "<b>" + str(v) + "</b>"
                            res_info += str(k) + ": " + tmp_html + "\n"
                        else:
                            res_info += str(k) + ": " + str(v) + "\n"

            return self.send_mess_channel(str(res_info))

        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
            return self.send_mess_channel(str(res_info))

    def halt_node_mobile(self, message):
        _now = get_date_now()
        key_txt = ''
        area_txt = ''
        words = message.split()
        res_drop = ''
        n = len(words)
        if n >= 3:
            node_code = words[2]
            cell_name = words[1]
        else:
            node_code = ''
            cell_name = ''

        res_info = ""
        # tao request
        _acc_stat_lst = []
        inp_dct = dict(template_id=31,
                       param_inpt_dct=dict(Cellname=cell_name,
                                           nodeCode=node_code))
        # Luc dau post data lay ve template id. Sau do dung template id day de lay du lieu ve
        # test
        #API_SPRING_IPMS_SERVER = "192.168.252.217"
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_FLASK_PORT) + "/vmsa/template"
        headers_json = {"Content-Type": "application/json"}
        body_json = json.dumps(inp_dct)

        # r = requests.post(url_api, headers=headers_json, data=body_json)
        r = api_post_retry(url_api, headers_json, body_json)
        temp_init_id = 0
        if r:
            if r.ok:
                # lay ve template id
                temp_init_id = r.text
            # dinh ky 30s /1 lan check ket qua
            if temp_init_id:
                chck_res = False
                url_api += "/" + str(temp_init_id)
                step_count = 0
                while not chck_res:
                    step_count += 1
                    r = api_get_retry(url_api, headers_json)
                    if r.ok:
                        # lay ve template id
                        result = r.text
                        if result.upper() == "FALSE":
                            res_info = "Không có kết quả"
                            return res_info
                        elif result.upper() == 'NOT FINISH':
                            # tiep tuc cho
                            time.sleep(30)
                        elif result == '[]\n':
                            time.sleep(30)
                        else:
                            _acc_stat_lst = json.loads(r.text)
                            for _acc_stat in _acc_stat_lst:
                                for k, v in _acc_stat.items():
                                    # them phan neu value co NOK
                                    if str(k).strip() == 'actionResult':
                                        chck_res = True
                                        break
                    else:
                        time.sleep(30)
                    if step_count > 60:
                        chck_res = True
                        break
        else:
            res_info = "Không có phản hồi API"
            return res_info

        if _acc_stat_lst:
            res_info += "Kết quả halt cell " + str(cell_name) + " của node " + str(node_code) + ":\n"
            for _acc_stat in _acc_stat_lst:
                for k, v in _acc_stat.items():
                    # them phan neu value co NOK
                    if str(v).strip() != '':
                        if str(v).upper().find("NOK") >= 0:
                            tmp_html = "<b>" + str(v) + "</b>"
                            res_info += str(k) + ": " + tmp_html + "\n"
                        else:
                            res_info += str(k) + ": " + str(v) + "\n"

            return self.send_mess_channel(str(res_info))

        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
            return self.send_mess_channel(str(res_info))

    def check_list_user_aon(self, message):
        _now = get_date_now()
        key_txt = ''
        area_txt = ''
        words = message.split()
        res_drop = ''
        node_code = ''
        n = len(words)
        if n >= 3:
            key_txt = words[1]
            area_txt = words[2].upper()
        elif n == 2:
            key_txt = words[1]
            area_txt = get_area_from_acc(str(key_txt))
        # do phai nhap node code tuong ung tung khu vuc vao de lam viec nen can random ra cac node tung kv
        if area_txt == 'KV1':
            node_code = 'HNI0327ASW01'
        elif area_txt == 'KV2':
            node_code = 'DNG0422ASW01'
        else:
            node_code = 'AGG0001ASW01'
        res_info = ""
        # tao request
        _acc_stat_lst = []
        inp_dct = dict(template_id=16,
                       param_inpt_dct=dict(area_code=area_txt,
                                           nodeCode=node_code,
                                           accountService=key_txt))
        # Luc dau post data lay ve template id. Sau do dung template id day de lay du lieu ve
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_FLASK_PORT) + "/vmsa/template"
        headers_json = {"Content-Type": "application/json"}
        body_json = json.dumps(inp_dct)

        # r = requests.post(url_api, headers=headers_json, data=body_json)
        r = api_post_retry(url_api, headers_json, body_json)
        temp_init_id = 0
        if r:
            if r.ok:
                # lay ve template id
                temp_init_id = r.text
            # dinh ky 30s /1 lan check ket qua
            if temp_init_id:
                chck_res = False
                url_api += "/" + str(temp_init_id)
                step_count = 0
                while not chck_res:
                    step_count += 1
                    r = api_get_retry(url_api, headers_json)
                    if r.ok:
                        # lay ve template id
                        result = r.text
                        if result.upper() == "FALSE":
                            res_info = "Không có kết quả"
                            return res_info
                        elif result.upper() == 'NOT FINISH':
                            # tiep tuc cho
                            time.sleep(30)
                        else:
                            _acc_stat_lst = json.loads(r.text)
                            # for _acc_stat in _acc_stat_lst:
                            #    for k, v in _acc_stat.items():
                            # them phan neu value co NOK
                            # if str(k).strip() == 'Result_All':d
                            #    chck_res = True
                            #    break
                            chck_res = True
                    else:
                        time.sleep(30)
                    if step_count > 60:
                        chck_res = True
                        break
        else:
            res_info = "Không có phản hồi API"
            return res_info

        if _acc_stat_lst:
            res_info += "Kết quả kiểm tra account " + str(key_txt) + " của " + str(area_txt) + ":\n"
            for _acc_stat in _acc_stat_lst:
                for k, v in _acc_stat.items():
                    # them phan neu value co NOK
                    if str(v).strip() != '':
                        if str(v).upper().find("NOK") >= 0:
                            tmp_html = "<b>" + str(v) + "</b>"
                            res_info += str(k) + ": " + tmp_html + "\n"
                        else:
                            res_info += str(k) + ": " + str(v) + "\n"

            return self.send_mess_channel(str(res_info))

        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
            return self.send_mess_channel(str(res_info))

    def check_list_user_l3ll(self, message):
        _now = get_date_now()
        key_txt = ''
        area_txt = ''
        words = message.split()
        res_drop = ''
        node_code = ''
        n = len(words)
        if n >= 3:
            key_txt = words[1]

        elif n == 2:
            key_txt = words[1]

        # do phai nhap node code tuong ung tung khu vuc vao de lam viec nen can random ra cac node tung kv

        node_code = 'HCM3642ASW02'

        res_info = ""
        # tao request
        _acc_stat_lst = []
        inp_dct = dict(template_id=26,
                       param_inpt_dct=dict(nodeCode=node_code,
                                           accountService=key_txt))
        # Luc dau post data lay ve template id. Sau do dung template id day de lay du lieu ve
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_FLASK_PORT) + "/vmsa/template"
        headers_json = {"Content-Type": "application/json"}
        body_json = json.dumps(inp_dct)

        # r = requests.post(url_api, headers=headers_json, data=body_json)
        r = api_post_retry(url_api, headers_json, body_json)
        temp_init_id = 0
        if r:
            if r.ok:
                # lay ve template id
                temp_init_id = r.text
            # dinh ky 30s /1 lan check ket qua
            if temp_init_id:
                chck_res = False
                url_api += "/" + str(temp_init_id)
                step_count = 0
                while not chck_res:
                    step_count += 1
                    r = api_get_retry(url_api, headers_json)
                    if r.ok:
                        # lay ve template id
                        result = r.text
                        if result.upper() == "FALSE":
                            res_info = "Không có kết quả"
                            return res_info
                        elif result.upper() == 'NOT FINISH':
                            # tiep tuc cho
                            time.sleep(30)
                        else:
                            _acc_stat_lst = json.loads(r.text)
                            # for _acc_stat in _acc_stat_lst:
                            #    for k, v in _acc_stat.items():
                            # them phan neu value co NOK
                            # if str(k).strip() == 'Result_All':d
                            #    chck_res = True
                            #    break
                            chck_res = True
                    else:
                        time.sleep(30)
                    if step_count > 60:
                        chck_res = True
                        break
        else:
            res_info = "Không có phản hồi API"
            return res_info

        if _acc_stat_lst:
            res_info += "Kết quả kiểm tra account " + str(key_txt) + " của " + str(area_txt) + ":\n"
            for _acc_stat in _acc_stat_lst:
                for k, v in _acc_stat.items():
                    # them phan neu value co NOK
                    if str(v).strip() != '':
                        if str(v).upper().find("NOK") >= 0:
                            tmp_html = "<b>" + str(v) + "</b>"
                            res_info += str(k) + ": " + tmp_html + "\n"
                        else:
                            res_info += str(k) + ": " + str(v) + "\n"

            return self.send_mess_channel(str(res_info))

        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
            return self.send_mess_channel(str(res_info))

    def check_online(self, message):
        _now = get_date_now()
        words = message.split()
        res_drop = ''

        n = len(words)
        if n >= 2:
            time_now = get_date_now()
            time_lst_45_mins = get_date_minus(time_now, 45)
            time_lst_24h_mins = get_date_minus(time_now, 24 * 60)
            time_lst_7d_mins = get_date_minus(time_now, 7 * 24 * 60)

            time_now_conv = convert_date_to_ipms(time_now)
            time_lst_45_mins_conv = convert_date_to_ipms(time_lst_45_mins)
            time_lst_24h_mins_conv = convert_date_to_ipms(time_lst_24h_mins)
            time_lst_7d_mins_conv = convert_date_to_ipms(time_lst_7d_mins)

            _tbl_usr_impl = TblUseronlineBrasImpl(max=9000,
                                                  current=500,
                                                  max_input=9000,
                                                  link_name='Useronline',
                                                  area='KV3',
                                                  interface_id=2004,
                                                  rtg_url='http://hehe',
                                                  time_begin=time_now_conv,
                                                  node_name='CKV09',
                                                  efficient=73
                                                  )

            prvn = words[1]
            chck_prvn_type = get_province_type(prvn)
            max_now = 0
            max_24h = 0
            avg_24h = 0
            max_7d = 0
            avg_7d = 0
            time_24h = ''
            time_7d = 0
            if chck_prvn_type == 'area':

                _srch_tst_dct = dict(searchArea=prvn, timestampGreat=time_lst_45_mins_conv,
                                     timestampLess=time_now_conv)

                _srch_tst_24h_dct = dict(searchArea=prvn, timestampGreat=time_lst_24h_mins_conv,
                                         timestampLess=time_now_conv)
                _srch_tst_7d_dct = dict(searchArea=prvn, timestampGreat=time_lst_7d_mins_conv,
                                        timestampLess=time_now_conv)
            else:
                # province
                prvn = get_province_bras_name(prvn)
                _srch_tst_dct = dict(searchProvince=prvn, timestampGreat=time_lst_45_mins_conv,
                                     timestampLess=time_now_conv)

                _srch_tst_24h_dct = dict(searchProvince=prvn, timestampGreat=time_lst_24h_mins_conv,
                                         timestampLess=time_now_conv)
                _srch_tst_7d_dct = dict(searchProvince=prvn, timestampGreat=time_lst_7d_mins_conv,
                                        timestampLess=time_now_conv)

            _res_now_lst = _tbl_usr_impl.find_like(**_srch_tst_dct)
            _res_24h_lst = _tbl_usr_impl.find_like(**_srch_tst_24h_dct)
            # ve chart 24h
            # filter chi lay user ipv4
            _res_24h_flt_lst = _tbl_usr_impl.get_user_by_fld('User_IPv4_Online', _res_24h_lst)
            if not _res_24h_flt_lst:
                _res_24h_flt_lst = _tbl_usr_impl.get_user_by_fld('UserOnline', _res_24h_lst)

            _res_7d_lst = _tbl_usr_impl.find_like(**_srch_tst_7d_dct)
            _res_7d_fld_lst = _tbl_usr_impl.get_user_by_fld('User_IPv4_Online', _res_7d_lst)
            if not _res_7d_fld_lst:
                _res_7d_fld_lst = _tbl_usr_impl.get_user_by_fld('UserOnline', _res_7d_lst)

            if _res_now_lst:
                max_now = _tbl_usr_impl.get_latest_total_user(_res_now_lst)
            if _res_24h_flt_lst:
                max_24h, avg_24h, time_24h = _tbl_usr_impl.get_max_avg_total_user(_res_24h_flt_lst)
            if _res_7d_fld_lst:
                max_7d, avg_7d, time_7d = _tbl_usr_impl.get_max_avg_total_user(_res_7d_fld_lst)
            # Ve chart
            max_ttl_24h = max_24h
            avg_ttl_24h = avg_24h
            max_time_24h = time_24h
            if max_ttl_24h > 0:
                chart = SimpleLineChart(700, 400, y_range=[0, max_ttl_24h])
                _num = len(_res_24h_flt_lst)

                chart.add_data([x['user'] for x in _res_24h_flt_lst])
                # chart.add_data([avg_7d for x in _res_24h_flt_lst])

                chart.set_axis_labels(Axis.LEFT, [0, max_ttl_24h / 2, max_ttl_24h])
                chart.set_axis_labels(Axis.BOTTOM, [_res_24h_flt_lst[0]['time_begin'],
                                                    _res_24h_flt_lst[int(_num / 3)]['time_begin'],
                                                    _res_24h_flt_lst[int(_num * 2 / 3)]['time_begin'],
                                                    _res_24h_flt_lst[int(_num - 1)]['time_begin']])
                chart.set_axis_labels(Axis.TOP, ['', 'Check User Online ' + str(prvn), ''])

                # can phai ran dom ra ten file tranh bi nham neu nguoi hoi nhieu
                img_name = 'chart' + prvn + time_now_conv
                file_name_uuid = uuid.uuid3(uuid.NAMESPACE_DNS, img_name)
                img_name_urn = file_name_uuid.urn + '.png'
                chart.download(img_name_urn)

                mes = "Result Check User of " + str(prvn) + ":\n Now: " + str(max_now) + \
                      "\n Avg 24h: " + str(int(avg_24h)) + \
                      "\n Last 24h \n Max: " + str(max_24h) + " Time: " + str(time_24h) + \
                      "\n Avg 7 days :" + str(int(avg_7d)) + \
                      "\n Last 7 days\n Max: " + str(max_7d) + " Time: " + str(time_7d)

                return self.send_photo(file_name=img_name_urn, mess_caption=mes)

                # bot.reply_to(message, mes)
            else:
                mes = 'No data. Please not for further check'
                return self.send_mess_channel(mes)
        else:
            mes = 'Wrong syntax: /checkonline Area(Province) .\n Example: /checkonline THA or /checkonline KV1'
            return self.send_mess_channel(mes)

    def check_mobile_core(self, message):
        _now = get_date_now()
        key_txt = ''
        alrm_lvl = ''
        words = message.split()
        res_info = ''
        n = len(words)
        if n >= 3:
            key_txt = words[1].upper()
            alrm_lvl = words[2].upper()
        elif n == 2:
            key_txt = words[1].upper()
            alrm_lvl = ''

        _obj_impl = TblBssCoreImpl(self.pool_nocpro)
        print("Pool Gpon Busy: " + str(_obj_impl.pool.busy))
        key_txt = str(key_txt).upper()
        alrm_lvl = str(alrm_lvl).upper()
        if key_txt:
            if alrm_lvl == 'CRITICAL':
                res_alrm_node_lst = _obj_impl.get_fault_critical_node(key_txt)
            elif alrm_lvl == 'MAJOR':
                res_alrm_node_lst = _obj_impl.get_fault_major_node(key_txt)
            else:
                res_alrm_node_lst = _obj_impl.get_fault_node(key_txt)
            if res_alrm_node_lst:
                res_info = "Kết quả kiểm tra " + str(key_txt) + ":\n"
                for res_alrm in res_alrm_node_lst:
                    res_info += "Cảnh báo: " + str(res_alrm['FAULT_NAME']) + "\n" + "" \
                                                                                    "   Mức độ: " + str(
                        res_alrm['FAULT_LEVEL_ID']) + "\n" + "" \
                                                             "   Thời gian: " + str(res_alrm['START_TIME']) + "\n"

                return self.send_mess_channel(res_info)
            else:
                res_info = "Không có cảnh báo tương ứng"
                return self.send_mess_channel(res_info)
        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
            return self.send_mess_channel(res_info)

    def check_bsc_traffic(self, message, key_fnd=''):
        _now = get_date_now()
        key_txt = ''
        alrm_lvl = ''
        words = message.split()
        res_info = ''
        n = len(words)
        if n >= 2:
            key_txt = words[1].upper()
            alrm_lvl = ''
            para_lst = []
            para_dct = dict(para_key="OP_STATE", para_val="BL")
            para_lst.append(para_dct)

            _test = VmsaParsing(29020, "xxx-check-rnc-and-bsc", para_lst, key_txt, "BSC", 1)
            _ring_info = _test.request()

            _res_dct = convert_api_vmsa_create_dt_log_for_bsc_xml_to_str(_ring_info)
            if 'logFileContent' in _res_dct:
                _encode = DecodeBase64(_res_dct['logFileContent'], 'milanox')
                key_lst = ['BUSY FULL RATE', 'BUSY HALF RATE', 'BUSY SDCCH', 'IDLE FULL RATE', 'IDLE HALF RATE',
                           'IDLE SDCCH', 'BLOCKED RADIO TIME SLOTS', 'GPRS TIME SLOTS']
                _res_bsc_lst = _encode.read_file_get_val(key_lst)
                if _res_bsc_lst:
                    res_info = "**Thông số lưu lượng của BSC:**\n"
                    if key_fnd != '':
                        _res_bsc_lst = get_filter_in_list(_res_bsc_lst, key_filter=key_fnd)
                    for _res_bsc in _res_bsc_lst:
                        if isinstance(_res_bsc, dict):
                            if 'key' in _res_bsc and 'val' in _res_bsc:
                                res_info += _res_bsc['key'] + ": " + str(_res_bsc['val']) + "\n"

                    return self.send_mess_channel(res_info)
                else:
                    res_info = "Không có cảnh báo tương ứng"
                    return res_info
            else:
                res_info = "Không có node tương ứng"
        else:
            res_info = "Không có tên trạm hoặc tên khu vực tương ứng"
            return res_info

    def check_cr_map(self, message, key_fnd=''):
        _now = get_date_now()
        key_txt = ''
        alrm_lvl = ''
        words = message.split()
        message = str(message).upper()
        res_info = ''
        res_lst = []

        n = len(words)
        if n >= 1:
            tbl_obj = TblSyslogGnocImpl(self.gnoc_pool)
            if message.find('AHDV') >= 0:
                res_lst = tbl_obj.get_cr_no_mapping_ahdv_lst()
                res_info = "Kết quả tra cứu CR chưa Map ảnh hưởng dịch vụ: " + "\n"
            else:
                res_lst = tbl_obj.get_cr_no_mapping_alm_lst()
                res_info = "Kết quả tra cứu CR chưa Map: " + "\n"

            if res_lst:
                if key_fnd != '':
                    res_lst = get_filter_in_list(res_lst, key_filter=key_fnd)
                for res in res_lst:
                    for key, val in res.items():
                        res_info += "        " + str(key) + ": " + str(val) + "\n"
                    res_info += "\n"
                return self.send_mess_channel(res_info)
            else:
                res_info = "Không có cr tương ứng"
                return self.send_mess_channel(res_info)

        else:
            res_info = "Không có tên tương ứng"
            return self.send_mess_channel(res_info)

    def reset_acc(self, message, key_fnd=''):
        _now = get_date_now()
        key_txt = ''
        alrm_lvl = ''
        words = message.split()
        res_info = ''
        n = len(words)
        if n >= 2:
            key_txt = words[1].lower()
            area_txt = get_area_from_acc(str(key_txt))
            if area_txt:
                tbl_obj = TblAaaImpl(area_txt)
                res = tbl_obj.reset_acc(key_txt)
                if res:
                    return "Reset OK"
                else:
                    return "Reset NOK"

            else:
                return "Không tìm thấy khu vực của account"

        else:
            res_info = "Không có tên account tương ứng"
            return self.send_mess_channel(res_info)

    def check_backhaul(self, message):
        _now = get_date_now()
        _now_only_date = get_only_date_now_format_ipms()
        _ystd_only_date = get_date_yesterday_format_ipms()

        words = message.split()
        res_drop = ''
        n = len(words)
        if n >= 2:
            time_range = words[1]
            if time_range.upper() == 'NOW':
                # lay thoi gian tu 60p - 30p truoc do elasticsearch tre 15p moi co ket qua
                # gio tren elasticsearch la gio GMT
                _now_end = get_date_minus(_now, -7 * 60 + 30)
                _now_bgn = get_date_minus(_now, - 7 * 60 + 60)
                res_info = "NOW: \n Capacity:"

            else:
                _now_end = get_date_minus(_now, -7 * 60)
                _now_bgn = get_date_minus(_now, 17 * 60)
                res_info = "Last 24h \n Capacity:"
        else:
            _now_end = get_date_minus(_now, -7 * 60)
            _now_bgn = get_date_minus(_now, 17 * 60)
            res_info = "Last 24h \n Capacity:"

        _now_end_epch = int(convert_date_to_epoch(_now_end) * 1000)
        _now_bgn_epch = int(convert_date_to_epoch(_now_bgn) * 1000)

        _api = ApiKibana(_now_bgn_epch, _now_end_epch, "tbl_interface_status")
        chck_res_kib = False
        num_rept = 0
        max_cpct = 0
        max_util = 0
        max_cbl_lst = []

        while not chck_res_kib:
            max_cpct, max_util = _api.get_max_cpct_util()
            max_cpct_2, max_cbl_lst = _api.get_cable_cpct()
            num_rept += 1
            if max_cpct > 0 and max_util > 0:
                chck_res_kib = True
            else:
                time.sleep(3)
            if num_rept > 5:
                chck_res_kib = True

        res_info = res_info + str(max_cpct) + "Gbps \n Utilizaiton:" + str(round(max_util, 2)) + " Gbps\n"
        if max_cbl_lst:
            for x in max_cbl_lst:
                res_info += "Cable name:" + x['cable_name'] + " Capacity:" + str(x['capacity']) + " Gbps\n"
        # tinh toan drop backhaul
        if n >= 2:
            time_range = words[1]
            if time_range.upper() == 'NOW':
                _tbl = TblInterfaceDropImpl(_now_only_date)
                try:
                    res = self.mysql_pool.execute(_tbl.get_drop_now())
                    if res:
                        time_drop = res[0]['time_frm']
                        ttl_drop = round(res[0]['total'] * 8 / 1000000, 2)
                        res_info += "Total Drop: " + str(ttl_drop) + " Mbps on " + str(time_drop) + "\n"
                except Exception as err:
                    print("error %s when get drop now")
        else:
            _tbl = TblInterfaceDropImpl(_ystd_only_date)
            res = self.mysql_pool.execute(_tbl.get_drop_max())
            if res:
                try:
                    time_drop = res[0]['time_frm']
                    ttl_drop = round(res[0]['total'] * 8 / 1000000, 2)
                    res_info += "Total Drop: " + str(ttl_drop) + " Mbps on " + str(time_drop) + "\n"
                except Exception as err:
                    print("error %s when get drop yesterday")

        return self.send_mess_channel(res_info)

    def get_backbone_detail(self, message):
        _now = get_date_now()
        _now_only_date = get_only_date_now_format_ipms()
        _ystd_only_date = get_date_yesterday_format_ipms()

        words = message.split()
        res_drop = ''
        n = len(words)
        _tbl_obj = TblCableBackboneImpl(self.mysql_pool)
        res_cabl_drct_dct = dict()
        if n >= 2:
            time_range = words[1]
            if time_range.upper() == 'NOW':

                res_info = "NOW: \n "
                _res_cbl_detl_lst, _res_cabl_drct_totl, _res_cbl_drct_lst = _tbl_obj.get_cable_direction_total(
                    _now_only_date, 'NOW')
                if _res_cbl_drct_lst:
                    res_info += "    Thời gian: " + str(_res_cbl_drct_lst[0]['timestamp']) + ' \n'
                    # Do bang tbl_cable_backbone_max khong tinh cable down. nen ta tinh lai theo tung chi tiet
                    totl_cpct = 0
                    for x in _res_cabl_drct_totl:
                        totl_cpct += convert_string_to_int(x['speed'])
                    res_info += "    Dung lượng: " + str(totl_cpct) + ' \n'
                    res_info += "    Thực dùng: " + str(_res_cbl_drct_lst[0]['sum(max_all)']) + ' \n'
                    res_info += "Chi tiết từng hướng:  \n"
                    # Them thoi gian theo tung huong

                    for x in _res_cabl_drct_totl:
                        max_traf = round(max(x['traffic_in'], x['traffic_out']), 2)
                        res_info += "    Hướng: " + str(x['direction']) + ' '
                        res_info += "    Tốc độ: " + str(x['speed']) + ' '
                        res_info += "    Lưu lượng: " + str(max_traf) + ' '
                        res_info += "\n"
                        # tinh thoi diem neu nghen
                        res_cgst = _tbl_obj.get_congestion_direction(str(x['direction']), str(x['speed']))
                        if res_cgst:
                            res_info += "    Đã nghẽn vào: " + str(res_cgst[0]['timestamp']) + ' ' \
                                                                                               'LL: ' + str(
                                res_cgst[0]['max_all']) + "\n"
                        else:
                            res_info += "    Không có nguy cơ nghẽn \n"

                    res_info += "Chi tiết từng tuyến cáp:  \n"
                    # Them thoi gian theo tung huong

                    for x in _res_cbl_detl_lst:
                        max_traf = round(max(x['traffic_in'], x['traffic_out']), 2)
                        res_info += "    Hướng: " + str(x['direction']) + ' '
                        res_info += "    Loại cáp: " + str(x['cable_type']) + ' '
                        res_info += "    Tốc độ: " + str(x['speed']) + ' '
                        res_info += "    Lưu lượng: " + str(max_traf) + ' '
                        res_info += "\n"

            else:
                res_info = "Last 24h: \n"
                _res_cbl_detl_lst, _res_cabl_drct_totl, _res_cbl_drct_lst = _tbl_obj.get_cable_direction_total(
                    _ystd_only_date, 'MAX')
                if _res_cbl_drct_lst:
                    res_info += "    Thời gian: " + str(_res_cbl_drct_lst[0]['timestamp']) + ' \n'
                    totl_speed = 0
                    for x in _res_cabl_drct_totl:
                        totl_speed += convert_string_to_int(x['speed'])
                    res_info += "    Dung lượng: " + str(totl_speed) + ' \n'
                    res_info += "    Thực dùng: " + str(_res_cbl_drct_lst[0]['sum(max_all)']) + ' \n'
                    res_info += "Chi tiết từng hướng:  \n"
                    # Them thoi gian theo tung huong

                    for x in _res_cabl_drct_totl:
                        max_traf = round(max(x['traffic_in'], x['traffic_out']), 2)
                        res_info += "    Hướng: " + str(x['direction']) + ' '
                        res_info += "    Tốc độ: " + str(x['speed']) + ' '
                        res_info += "    Lưu lượng: " + str(max_traf) + ' '
                        res_info += "\n"
                        res_cgst = _tbl_obj.get_congestion_direction(str(x['direction']), str(x['speed']))
                        if res_cgst:
                            res_info += "    Đã nghẽn vào: " + str(res_cgst[0]['timestamp']) + ' ' \
                                                                                               'LL: ' + str(
                                res_cgst[0]['max_all']) + "\n"
                        else:
                            res_info += "    Không có nguy cơ nghẽn \n"
                    res_info += "Chi tiết:  \n"
                    for x in _res_cbl_detl_lst:
                        max_traf = round(max(x['traffic_in'], x['traffic_out']), 2)
                        res_info += "    Hướng: " + str(x['direction']) + ' '
                        res_info += "    Loại cáp: " + str(x['cable_type']) + ' '
                        res_info += "    Tốc độ: " + str(x['speed']) + ' '
                        res_info += "    Lưu lượng: " + str(max_traf) + ' '
                        res_info += "\n"


        else:

            res_info = "Last 24h: \n"
            _res_cbl_detl_lst, _res_cabl_drct_totl, _res_cbl_drct_lst = _tbl_obj.get_cable_direction_total(
                _ystd_only_date, 'MAX')
            if _res_cbl_drct_lst:
                res_info += "    Thời gian: " + str(_res_cbl_drct_lst[0]['timestamp']) + ' \n'
                totl_cpct = 0
                for x in _res_cabl_drct_totl:
                    totl_cpct += convert_string_to_int(x['speed'])
                res_info += "    Dung lượng: " + str(totl_cpct) + ' \n'
                # res_info += "    Dung lượng: " + str(_res_cbl_drct_lst[0]['sum(capacity)']) + ' \n'
                res_info += "    Thực dùng: " + str(_res_cbl_drct_lst[0]['sum(max_all)']) + ' \n'
                res_info += "Chi tiết từng hướng:  \n"
                # Them thoi gian theo tung huong

                for x in _res_cabl_drct_totl:
                    max_traf = round(max(x['traffic_in'], x['traffic_out']), 2)
                    res_info += "    Hướng: " + str(x['direction']) + ' '
                    res_info += "    Tốc độ: " + str(x['speed']) + ' '
                    res_info += "    Lưu lượng: " + str(max_traf) + ' '
                    res_info += "\n"

                    res_cgst = _tbl_obj.get_congestion_direction(str(x['direction']), str(x['speed']))
                    if res_cgst:
                        res_info += "    Đã nghẽn vào: " + str(res_cgst[0]['timestamp']) + ' ' \
                                                                                           'LL: ' + str(
                            res_cgst[0]['max_all']) + "\n"
                    else:
                        res_info += "    Không có nguy cơ nghẽn \n"

                res_info += "Chi tiết:  \n"
                for x in _res_cbl_detl_lst:
                    max_traf = round(max(x['traffic_in'], x['traffic_out']), 2)
                    res_info += "    Hướng: " + str(x['direction']) + ' '
                    res_info += "    Loại cáp: " + str(x['cable_type']) + ' '
                    res_info += "    Tốc độ: " + str(x['speed']) + ' '
                    res_info += "    Lưu lượng: " + str(max_traf) + ' '
                    res_info += "\n"

        return self.send_mess_channel(res_info)

    def get_backbone_detail_when_cable_down(self, message):
        _now = get_date_now()
        _now_only_date = get_only_date_now_format_ipms()
        _ystd_only_date = get_date_yesterday_format_ipms()
        message = str(message).upper()

        words = message.split()
        res_drop = ''
        n = len(words)
        _tbl_obj = TblCableBackboneImpl(self.mysql_pool)
        res_cabl_drct_dct = dict()
        if n >= 2:
            # syntax: checkbackbonedown 1C,1D NOW
            cable_down = words[1]
            cable_down_lst = []
            cable_up_lst = []

            if cable_down.find(',') >= 0:
                cable_down_lst = cable_down.split(",")
            elif cable_down == 'NOW':
                return 'Yêu cầu nhập tên loại cáp muốn kiểm tra down'
            else:
                cable_down_lst.append(cable_down)
            if message.find('NOW') >= 0:
                res_info = "NOW: \n "
                _res_cbl_detl_lst, _res_cabl_drct_totl, _res_cbl_drct_lst = _tbl_obj.get_cable_direction_total(
                    _now_only_date, 'NOW')
                totl_cpct_down = 0.0
                totl_util_down = 0.0
                # direct _lst dung de cong them khi cable down gom co dang direct,cable_type,speed,traffic
                # y muon la khi cable down thi thuc dung se chuyen sang cac cable con lai
                direct_dct_lst = []
                if _res_cbl_drct_lst:
                    for x in _res_cbl_detl_lst:
                        max_traf = 0.0
                        sped = 0.0
                        drct = ''
                        cbl_type = ''
                        if 'direction' in x:
                            drct = x['direction']
                        if 'traffic_in' in x and 'traffic_out' in x:
                            max_traf = round(max(x['traffic_in'], x['traffic_out']), 2)
                        if 'speed' in x:
                            sped = x['speed']
                        if 'cable_type' in x:
                            cbl_type = str(x['cable_type'])
                            if cbl_type in cable_down_lst:
                                totl_cpct_down += x['speed']
                                totl_util_down += max_traf
                            elif cbl_type not in cable_up_lst:
                                cable_up_lst.append(cbl_type)

                            chck_cbl = False
                            if direct_dct_lst:
                                for cbl_drct_dct in direct_dct_lst:
                                    if 'direction' in cbl_drct_dct:
                                        drct_cbl = cbl_drct_dct['direction']
                                        if drct == drct_cbl and cbl_type in cable_down_lst:
                                            # append vao trong dictionary cua list
                                            cbl_drct_dct['traffic'] = max_traf + cbl_drct_dct['traffic']
                                            cbl_drct_dct['speed'] = sped + cbl_drct_dct['speed']
                                            chck_cbl = True
                                            break
                                if not chck_cbl and cbl_type in cable_down_lst:
                                    direct_dct_lst.append(dict(direction=drct,
                                                               traffic=max_traf,
                                                               speed=sped))

                            elif cbl_type in cable_down_lst and not chck_cbl:
                                direct_dct_lst.append(dict(direction=drct,
                                                           traffic=max_traf,
                                                           speed=sped))

                    res_info += "    Thời gian: " + str(_res_cbl_drct_lst[0]['timestamp']) + ' \n'
                    # Do bang tbl_cable_backbone_max khong tinh cable down. nen ta tinh lai theo tung chi tiet
                    totl_cpct = 0
                    for x in _res_cabl_drct_totl:
                        totl_cpct += convert_string_to_int(x['speed'])
                    totl_cpct = totl_cpct - totl_cpct_down
                    totl_util = _res_cbl_drct_lst[0]['sum(max_all)']

                    res_info += "    Dung lượng: " + str(totl_cpct) + ' \n'
                    res_info += "    Thực dùng: " + str(totl_util) + ' \n'
                    res_info += "Chi tiết từng hướng:  \n"
                    # Them thoi gian theo tung huong

                    for x in _res_cabl_drct_totl:
                        max_traf = round(max(x['traffic_in'], x['traffic_out']), 2)
                        speed_down = 0.0
                        traf_down = 0.0
                        for cbl_down in direct_dct_lst:
                            drct = cbl_down['direction']
                            if drct == x['direction']:
                                speed_down = cbl_down['speed']
                                traf_down = cbl_down['traffic']
                                break
                        res_info += "    Hướng: " + str(x['direction']) + ' '
                        res_info += "    Tốc độ: " + str(x['speed'] - speed_down) + ' '
                        res_info += "    Lưu lượng: " + str(max_traf) + ' '
                        res_info += "\n"
                        # tinh thoi diem neu nghen
                        res_cgst = _tbl_obj.get_congestion_direction(str(x['direction']), str(x['speed'] - speed_down))
                        if res_cgst:
                            res_info += "    Đã nghẽn vào: " + str(res_cgst[0]['timestamp']) + ' ' \
                                                                                               'LL: ' + str(
                                res_cgst[0]['max_all']) + "\n"
                        else:
                            res_info += "    Không có nguy cơ nghẽn \n"

                    res_info += "Chi tiết từng tuyến cáp:  \n"
                    # Them thoi gian theo tung huong

                    for x in _res_cbl_detl_lst:
                        max_traf = round(max(x['traffic_in'], x['traffic_out']), 2)
                        res_info += "    Hướng: " + str(x['direction']) + ' '
                        res_info += "    Loại cáp: " + str(x['cable_type']) + ' '

                        traf_down = 0.0
                        for cbl_down in direct_dct_lst:
                            drct = cbl_down['direction']
                            if drct == x['direction']:
                                traf_down = cbl_down['traffic']

                        if str(x['cable_type']) in cable_down_lst:
                            res_info += "    Tốc độ: 0"
                            res_info += "    Lưu lượng: 0"
                            res_info += "\n"
                        else:
                            # truong hop khong phai cable down thi cong them thuc dung / so cab con lai de share tai

                            res_info += "    Tốc độ: " + str(x['speed']) + ' '
                            if not cable_up_lst:
                                res_info += "    Lưu lượng: 0"
                            else:
                                res_info += "    Lưu lượng: " + str(
                                    max_traf + round(traf_down / len(cable_up_lst), 2)) + ' '
                            res_info += "\n"

            else:
                res_info = "Last 24h: \n"
                _res_cbl_detl_lst, _res_cabl_drct_totl, _res_cbl_drct_lst = _tbl_obj.get_cable_direction_total(
                    _ystd_only_date, 'MAX')
                if _res_cbl_drct_lst:
                    res_info += "    Thời gian: " + str(_res_cbl_drct_lst[0]['timestamp']) + ' \n'
                    totl_speed = 0
                    totl_cpct_down = 0.0
                    totl_util_down = 0.0
                    # direct _lst dung de cong them khi cable down gom co dang direct,cable_type,speed,traffic
                    # y muon la khi cable down thi thuc dung se chuyen sang cac cable con lai
                    direct_dct_lst = []
                    if _res_cbl_drct_lst:
                        for x in _res_cbl_detl_lst:
                            max_traf = 0.0
                            sped = 0.0
                            drct = ''
                            cbl_type = ''
                            if 'direction' in x:
                                drct = x['direction']
                            if 'traffic_in' in x and 'traffic_out' in x:
                                max_traf = round(max(x['traffic_in'], x['traffic_out']), 2)
                            if 'speed' in x:
                                sped = x['speed']
                            if 'cable_type' in x:
                                cbl_type = str(x['cable_type'])
                                if cbl_type in cable_down_lst:
                                    totl_cpct_down += x['speed']
                                    totl_util_down += max_traf
                                elif cbl_type not in cable_up_lst:
                                    cable_up_lst.append(cbl_type)

                                chck_cbl = False
                                if direct_dct_lst:
                                    for cbl_drct_dct in direct_dct_lst:
                                        if 'direction' in cbl_drct_dct:
                                            drct_cbl = cbl_drct_dct['direction']
                                            if drct == drct_cbl and cbl_type in cable_down_lst:
                                                # append vao trong dictionary cua list
                                                cbl_drct_dct['traffic'] = max_traf + cbl_drct_dct['traffic']
                                                cbl_drct_dct['speed'] = sped + cbl_drct_dct['speed']
                                                chck_cbl = True
                                                break
                                    if not chck_cbl and cbl_type in cable_down_lst:
                                        direct_dct_lst.append(dict(direction=drct,
                                                                   traffic=max_traf,
                                                                   speed=sped))

                                elif cbl_type in cable_down_lst and not chck_cbl:
                                    direct_dct_lst.append(dict(direction=drct,
                                                               traffic=max_traf,
                                                               speed=sped))

                    for x in _res_cabl_drct_totl:
                        totl_speed += convert_string_to_int(x['speed'])
                    totl_speed = totl_speed - totl_cpct_down

                    res_info += "    Dung lượng: " + str(totl_speed) + ' \n'
                    res_info += "    Thực dùng: " + str(_res_cbl_drct_lst[0]['sum(max_all)']) + ' \n'
                    res_info += "Chi tiết từng hướng:  \n"
                    # Them thoi gian theo tung huong

                    for x in _res_cabl_drct_totl:
                        max_traf = round(max(x['traffic_in'], x['traffic_out']), 2)
                        speed_down = 0.0
                        traf_down = 0.0
                        for cbl_down in direct_dct_lst:
                            drct = cbl_down['direction']
                            if drct == x['direction']:
                                speed_down = cbl_down['speed']
                                traf_down = cbl_down['traffic']
                                break

                        res_info += "    Hướng: " + str(x['direction']) + ' '
                        res_info += "    Tốc độ: " + str(x['speed'] - speed_down) + ' '
                        res_info += "    Lưu lượng: " + str(max_traf) + ' '
                        res_info += "\n"
                        res_cgst = _tbl_obj.get_congestion_direction(str(x['direction']), str(x['speed'] - speed_down))
                        if res_cgst:
                            res_info += "    Đã nghẽn vào: " + str(res_cgst[0]['timestamp']) + ' ' \
                                                                                               'LL: ' + str(
                                res_cgst[0]['max_all']) + "\n"
                        else:
                            res_info += "    Không có nguy cơ nghẽn \n"
                    res_info += "Chi tiết:  \n"
                    for x in _res_cbl_detl_lst:
                        max_traf = round(max(x['traffic_in'], x['traffic_out']), 2)
                        res_info += "    Hướng: " + str(x['direction']) + ' '
                        res_info += "    Loại cáp: " + str(x['cable_type']) + ' '

                        traf_down = 0.0
                        for cbl_down in direct_dct_lst:
                            drct = cbl_down['direction']
                            if drct == x['direction']:
                                traf_down = cbl_down['traffic']
                        if str(x['cable_type']) in cable_down_lst:
                            res_info += "    Tốc độ: 0"
                            res_info += "    Lưu lượng: 0"
                            res_info += "\n"
                        else:
                            # truong hop khong phai cable down thi cong them thuc dung / so cab con lai de share tai

                            res_info += "    Tốc độ: " + str(x['speed']) + ' '
                            if not cable_up_lst:
                                res_info += "    Lưu lượng: 0"
                            else:
                                res_info += "    Lưu lượng: " + str(max_traf + round(traf_down / len(cable_up_lst), 2))
                            res_info += "\n"

        else:
            res_info = "Yêu cầu nhập tên loại cáp muốn kiểm tra down \n"
        return self.send_mess_channel(res_info)

    def check_drop(self, message):
        _now = get_date_now()
        _now_only_date = get_only_date_now_format_ipms()
        _ystd_only_date = get_date_yesterday_format_ipms()

        words = message.split()
        res_drop = ''
        n = len(words)
        if n >= 2:
            time_range = words[1]
            if time_range.upper() == 'NOW':
                # lay thoi gian tu 60p - 30p truoc do elasticsearch tre 15p moi co ket qua
                # gio tren elasticsearch la gio GMT
                _now_end = get_date_minus(_now, -7 * 60 + 30)
                _now_bgn = get_date_minus(_now, - 7 * 60 + 60)
                res_info = "Total Drop Now: \n"

            else:
                _now_end = get_date_minus(_now, -7 * 60)
                _now_bgn = get_date_minus(_now, 17 * 60)
                res_info = "Max Drop Last 24h: \n"
        else:
            _now_end = get_date_minus(_now, -7 * 60)
            _now_bgn = get_date_minus(_now, 17 * 60)
            res_info = "Max Drop Last 24h: \n"

        _now_end_epch = int(convert_date_to_epoch(_now_end) * 1000)
        _now_bgn_epch = int(convert_date_to_epoch(_now_bgn) * 1000)

        # tinh toan drop backhaul
        if n >= 2:
            time_range = words[1]
            if time_range.upper() == 'NOW':
                _tbl = TblInterfaceDropImpl(_now_only_date)
                try:
                    res = self.mysql_pool.execute(_tbl.get_drop_now())
                    if res:
                        time_drop = res[0]['time_frm']

                        _tbl_now = TblInterfaceDropImpl(time_drop)
                        res_lst = self.mysql_pool.execute(_tbl_now.get_total_drop_node())
                        time_drop = ''
                        ttl_all_drop = 0
                        for res in res_lst:
                            try:
                                time_drop = res['timestamp']
                                node_name = res['node_name']
                                ttl_drop = res['total']
                                if ttl_drop > 0:
                                    res_info += "Total Drop: " + str(ttl_drop) + " Mbps on " + str(node_name) + "\n"
                                    ttl_all_drop += ttl_drop
                            except Exception as err:
                                print("Error %s when get total drop now" % err)

                        if time_drop:
                            res_info += "Total Drop: " + str(round(ttl_all_drop, 2)) + " Mbps " + "\n"
                            res_info += "Time: " + str(time_drop)

                except Exception as err:
                    print("error %s when get drop now" % err)
        else:
            _tbl = TblInterfaceDropImpl(_ystd_only_date)
            # lay time co drop max truoc sau do lay time va select gio day ra
            res = self.mysql_pool.execute(_tbl.get_drop_max())
            if res:
                try:
                    time_drop = res[0]['time_frm']
                    _tbl_now = TblInterfaceDropImpl(time_drop)
                    res_lst = self.mysql_pool.execute(_tbl_now.get_total_drop_node())
                    ttl_all_drop = 0
                    for res in res_lst:
                        time_drop_new = res['timestamp']
                        node_name = res['node_name']
                        ttl_drop = res['total']
                        if ttl_drop > 0:
                            res_info += "Total Drop: " + str(ttl_drop) + " Mbps on " + str(node_name) + "\n"
                            ttl_all_drop += ttl_drop

                    if time_drop:
                        res_info += "Total Drop: " + str(round(ttl_all_drop, 2)) + " Mbps " + "\n"
                        res_info += "Time: " + str(time_drop)
                except Exception as err:
                    print("Error when get time of drop")
                    res_info += str(err)

        return self.send_mess_channel(res_info)

    def check_crc(self, message):
        words = message.split()
        res_crc = ''
        n = len(words)
        # cu phap se la node
        date_time_now = get_time_format_now()
        time_wait = 0
        if n >= 2:
            node_ip = words[1]
            node_intf = words[2]

            chat_id = message.chat.id

            _dev_crc = DeviceShowInterfaceCrc(False, date_time_now, node_ip, node_intf,
                                              '', 'Checking CRC')
            res_crc, crc_num = _dev_crc.run()
            print(res_crc)
            # bot.reply_to(message, str(res_crc))

            if n >= 3:
                time_wait_str = words[3]
            else:
                time_wait_str = '300'
            time_wait_int = convert_string_to_int(time_wait_str)
            if time_wait_int > 0:
                time.sleep(time_wait_int)
                _dev_crc_2 = DeviceShowInterfaceCrc(False, date_time_now, node_ip, node_intf,
                                                    '', 'Checking CRC')
                res_crc_2, crc_num_2 = _dev_crc.run()
                crc_chg = crc_num_2 - crc_num
                res_crc_2 += "\n Total change: " + str(crc_chg)
                # bot.reply_to(message, str(res_crc_2))
                return self.send_mess_channel(str(res_crc_2))
            else:
                return self.send_mess_channel(str(res_crc))

        else:
            res_crc = "Error Syntax.Please ask:/checkcrc node_ip interface time_sleep_between_next_check"
            # bot.reply_to(message, str(res_crc))
            return self.send_mess_channel(str(res_crc))

    def get_performance_mobile(self, message, key_fnd=''):
        words = message.split()
        res_crc = ''
        n = len(words)
        # cu phap se la node
        date_time_now = get_time_format_now()
        time_wait = 0
        if n >= 2:
            node_name = words[1]

            node_type = get_node_type_mobile(node_name)
            res_lst = []
            _obj_impl = TblBssCoreImpl(self.pool_nocpro)
            _obj_cbs_impl = TblBssCbsCoreImpl(self.pool_cbs)
            if node_name and node_type:
                if node_type == 'MSC':
                    res_lst = _obj_impl.get_performance_msc(node_name)
                    pass
                elif node_type == 'HLR':
                    res_lst = _obj_impl.get_performance_hlr(node_name)
                    pass
                elif node_type == 'GGSN':
                    res_lst = _obj_impl.get_performance_ggsn(node_name)
                elif node_type == 'SGSN':
                    res_lst = _obj_impl.get_performance_sgsn(node_name)
                elif node_type == 'SMSC':
                    res_lst = _obj_impl.get_performance_smsc(node_name)
                elif node_type == 'BSC':
                    res_lst = _obj_impl.get_performance_bsc(node_name)
                elif node_type == 'STP':
                    res_lst = _obj_impl.get_performance_stp(node_name)
                elif node_type == 'GMSC':
                    res_lst = _obj_cbs_impl.get_performance_gmsc(node_name)
                elif node_type == 'RNC':
                    res_lst = _obj_impl.get_performance_rnc(node_name)
                elif node_type == 'SMSC':
                    res_lst = _obj_impl.get_performance_smsc(node_name)

                if res_lst:
                    if key_fnd != '':
                        res_lst = get_filter_in_list(res_lst, key_filter=key_fnd)
                    res_info = "KPI của thiết bị: " + str(node_name) + "\n"
                    for res in res_lst:
                        for key, val in res.items():
                            res_info += str(key) + ": " + str(val) + "\n"
                    return self.send_mess_channel(res_info)
                else:
                    res_info = "Không có KPI tương ứng"
                    return self.send_mess_channel(res_info)


        else:
            res_crc = "Error Syntax. /m_per GWPD01"
            # bot.reply_to(message, str(res_crc))
            return self.send_mess_channel(str(res_crc))

    def respond(self):
        # truong hop help
        raw_text = self.event.raw_text.upper()
        words = raw_text.split()

        if raw_text.find('HELP') >= 0:
            return self.get_help()
        elif raw_text.find('PING') >= 0:
            res_info = "Still alive and kicking New!"
            return self.send_mess_channel(res_info)
        elif words[0] == 'CHECKNODE' or words[0] == '/CHECKNODE':
            return self.check_node(raw_text=raw_text)
        elif raw_text.find('CHECKDROP') >= 0:
            return self.check_drop(message=raw_text)
        elif raw_text == 'CHECKCRC':
            return self.check_crc(message=raw_text)
        elif raw_text.find('M_CORE') >= 0:
            return self.check_mobile_core(message=raw_text)
        elif raw_text.find('BACKHAUL') >= 0:
            return self.check_backhaul(message=raw_text)
        elif raw_text.find('CABINET_RELATE') >= 0:
            return self.check_cabinet_relate(raw_text=raw_text, key_fnd='')
        elif raw_text.find('CABINET_INFO') >= 0:
            return self.check_cabinet_info(raw_text=raw_text)
        elif raw_text.find('CABINET') >= 0:
            return self.check_cabinet(raw_text=raw_text)
        elif raw_text.find('STATION') >= 0:
            return self.check_station(raw_text=raw_text)
        elif raw_text.find('CELL_RELATE') >= 0:
            return self.check_cell_relate(raw_text=raw_text)
        elif raw_text.find('CELL_INFO') >= 0:
            return self.check_cell_info(raw_text=raw_text)
        elif raw_text.find('CELL') >= 0:
            return self.check_cell(raw_text=raw_text)
        elif raw_text.find('KPI2G') >= 0:
            return self.check_kpi_mobile_access(raw_text=raw_text, type='2G')
        elif raw_text.find('KPI3G') >= 0:
            return self.check_kpi_mobile_access(raw_text=raw_text, type='3G')
        elif raw_text.find('KPI4G') >= 0:
            return self.check_kpi_mobile_access(raw_text=raw_text, type='4G')
        elif raw_text.find('KPI') >= 0:
            return self.check_kpi_mobile(raw_text=raw_text)
        elif raw_text.find('CHECKTOTALMOBILE') >= 0:
            return self.check_total_mobile()
        elif raw_text.find('CHECKNODENAME') >= 0:
            return self.check_node_name(raw_text=raw_text)
        elif raw_text.find('CHECKNODEALARM1ST') >= 0:
            return self.check_node_alarm_first(mess=raw_text)
        elif raw_text.find('CHECKNODEALARM') >= 0:
            return self.check_node_alarm(message=raw_text)
        elif raw_text.find('UCTT') >= 0:
            return self.check_uctt(message=raw_text)
        elif raw_text.find('CHECKUSER') >= 0:
            return self.check_user(message=raw_text)
        elif raw_text.find('CHECKONLINE') >= 0:
            return self.check_online(message=raw_text)
        elif raw_text.find('CHECKTV') >= 0:
            return self.check_tv(message=raw_text)
        elif raw_text.find('M_PER') >= 0:
            return self.get_performance_mobile(message=raw_text)
        elif raw_text.find('CHECKBACKBONE') >= 0:
            return self.get_backbone_detail(message=raw_text)
        elif raw_text.find('CHECKMCA') >= 0:
            return self.check_mca(raw_text=raw_text)
        elif raw_text.find('BSC_TRAFFIC') >= 0:
            return self.check_bsc_traffic(message=raw_text)
        else:
            return ''


if __name__ == '__main__':

    config = Development()
    BOT_ID = config.__getattribute__('BOT_ID')
    BOT_GROUP_ID = -218379033
    BOT_GROUP_CNTT_ID = config.__getattribute__('BOT_GROUP_CNTT_ID')
    SERVER_NOCPRO = config.__getattribute__('SERVER_NOCPRO')
    SERVER_NOCPRO_PORT = config.__getattribute__('SERVER_NOCPRO_PORT')
    SERVER_NOCPRO_SERVICE_NAME = config.__getattribute__('SERVER_NOCPRO_SERVICE_NAME')
    SERVER_NOCPRO_USERNAME = config.__getattribute__('SERVER_NOCPRO_USERNAME')
    SERVER_NOCPRO_PASSWORD = config.__getattribute__('SERVER_NOCPRO_PASSWORD')
    SERVER_NOCPRO_USERNAME2 = config.__getattribute__('SERVER_NOCPRO_USERNAME2')
    SERVER_NOCPRO_PASSWORD2 = config.__getattribute__('SERVER_NOCPRO_PASSWORD2')
    SERVER_AOM = config.__getattribute__('SERVER_AOM')
    SERVER_AOM_PORT = config.__getattribute__('SERVER_AOM_PORT')
    SERVER_AOM_SERVICE_NAME = config.__getattribute__('SERVER_AOM_SERVICE_NAME')
    SERVER_AOM_USERNAME = config.__getattribute__('SERVER_AOM_USERNAME')
    SERVER_AOM_PASSWORD = config.__getattribute__('SERVER_AOM_PASSWORD')
    BOT_GROUP_IP_CDBR_KV1 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV1')
    BOT_GROUP_IP_CDBR_KV2 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV2')
    BOT_GROUP_IP_CDBR_KV3 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV3')
    BOT_GROUP_BSS_ID = config.__getattribute__('BOT_GROUP_BSS_ID')
    SERVER_CBS = config.__getattribute__('SERVER_CBS')
    SERVER_CBS_PORT = config.__getattribute__('SERVER_CBS_PORT')
    SERVER_CBS_SERVICE_NAME = config.__getattribute__('SERVER_CBS_SERVICE_NAME')
    SERVER_CBS_USERNAME = config.__getattribute__('SERVER_CBS_USERNAME')
    SERVER_CBS_PASSWORD = config.__getattribute__('SERVER_CBS_PASSWORD')

    SERVER_GNOC = config.__getattribute__('SERVER_GNOC')
    SERVER_GNOC_PORT = config.__getattribute__('SERVER_GNOC_PORT')
    SERVER_GNOC_SERVICE_NAME = config.__getattribute__('SERVER_GNOC_SERVICE_NAME')
    SERVER_GNOC_USERNAME = config.__getattribute__('SERVER_GNOC_USERNAME')
    SERVER_GNOC_PASSWORD = config.__getattribute__('SERVER_GNOC_PASSWORD')

    BOT_GROUP_WHITELIST = config.__getattribute__('BOT_GROUP_WHITELIST')
    BOT_GROUP_GREY_LIST = config.__getattribute__('BOT_GROUP_GREY_LIST')

    dsn_str = cx_Oracle.makedsn(SERVER_NOCPRO, SERVER_NOCPRO_PORT, service_name=SERVER_NOCPRO_SERVICE_NAME)
    dsn_cbs_str = cx_Oracle.makedsn(SERVER_CBS, SERVER_CBS_PORT, service_name=SERVER_CBS_SERVICE_NAME)
    dsn_gnoc_str = cx_Oracle.makedsn(SERVER_GNOC, SERVER_GNOC_PORT, service_name=SERVER_GNOC_SERVICE_NAME)

    #pool_gnoc = cx_Oracle.SessionPool(min=1,
    #                                  max=10, increment=1, threaded=True, dsn=dsn_gnoc_str,
    #                                  user=SERVER_GNOC_USERNAME, password=SERVER_GNOC_PASSWORD,
    #                                  encoding='UTF-8', nencoding='UTF-8')
    '''
    pool_nocpro = cx_Oracle.SessionPool(min=1,
                                        max=10, increment=1, threaded=True, dsn=dsn_str,
                                        user=SERVER_NOCPRO_USERNAME, password=SERVER_NOCPRO_PASSWORD,
                                        encoding='UTF-8', nencoding='UTF-8')
     
    pool_nocpro_tableau = cx_Oracle.SessionPool(min=1,
                                                max=10, increment=1, threaded=True, dsn=dsn_str,
                                                user=SERVER_NOCPRO_USERNAME2, password=SERVER_NOCPRO_PASSWORD2,
                                                encoding='UTF-8', nencoding='UTF-8')

    pool_cbs = cx_Oracle.SessionPool(min=1,
                                     max=10, increment=1, threaded=True, dsn=dsn_cbs_str,
                                     user=SERVER_CBS_USERNAME, password=SERVER_CBS_PASSWORD,
                                     encoding='UTF-8', nencoding='UTF-8')
  
    dbconfig = {
        "host": "192.168.251.15",
        "port": 3306,
        "user": "linhlk1",
        "password": "linhlk135",
        "database": "luuluong",
    }
    mysql_pool = ServerMysqlConnect(**dbconfig)
    
    _bot = BotTelethonImpl(None,
                           pool_nocpro=pool_nocpro,
                           pool_nocpro_tableau=pool_nocpro_tableau,
                           pool_cbs=None,
                           mysql_pool=mysql_pool)
                           '''
    _bot = BotTelethonImpl(None,
                           pool_nocpro=None,
                           pool_nocpro_tableau=None,
                           pool_cbs=None,
                           mysql_pool=None,
                           gnoc_pool=None)
    from core.helpers.stringhelpers import get_province_bras_name, get_province_type, divide_str
    txt = '1.1.1.1'
    if txt.find('.') >= 0:
        pos = txt.find('.')
        pos_2 = txt.find('.', pos + 1)
        if pos_2 >= 0:
            pos_3 = txt.find('.', pos_2 + 1)
            if pos_3 >= 0:
                print("ok")

    res_info = _bot.halt_node_mobile('/halt LAB0062 RCHK04')
    mess_lst = divide_str(res_info, 1000)
    from core.helpers.stringhelpers import get_province_bras_name, get_province_type, divide_str

    print(res_info)
    if res_info:
        # fix loi message too long cua telegram. phai chia ra lam nhieu phan
        mess_lst = divide_str(res_info, 1000)
        i = 0
        for mess in mess_lst:
            if i == 0:
                print(1)
            else:
                print(1)
            i += 1
    else:
        res_info = "Không có cảnh báo tương ứng"
        print(1)
