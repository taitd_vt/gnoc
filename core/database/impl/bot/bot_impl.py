from config import Config, Development
import requests
import telebot
import urllib.request
import uuid
import os
from core.helpers.api_helpers import api_post_retry
from core.helpers.date_helpers import get_date_now_format_ipms
from pprint import pprint
config = Development()
BOT_ID = config.__getattribute__('BOT_ID')
#BOT_GROUP_ID = config.__getattribute__('BOT_GROUP_ID')
BOT_GROUP_ID = -218379033
BOT_GROUP_CRC_ID = config.__getattribute__('BOT_GROUP_CRC_ID')


class BotImpl():
    def __init__(self, bot, bot_id=BOT_ID):
        self.token = bot_id
        #self.bot = telebot.AsyncTeleBot(BOT_ID)
        self.bot = bot

    @staticmethod
    def get_img_path(file_name):
        pth = os.path.dirname(__file__)
        path_join = os.path.join(pth, "img")
        path_join = os.path.join(path_join, file_name)
        return path_join

    def send_mess_channel(self, mess, group_id):
        self.bot.send_message(group_id, mess)

    def send_mess_html_channel(self, mess, group_id):
        self.bot.send_message(group_id, mess, parse_mode="HTML")

    def reply_to_html(self, mess_grp, mess):
        try:
            self.bot.send_message(chat_id=mess_grp.chat.id,
                                  text=mess, parse_mode="HTML", reply_to_message_id=mess_grp.chat.id)
        except Exception as err:
            self.bot.send_message(chat_id=mess_grp.chat.id,
                                  text=str(err)
                                  )

    def send_photo(self, link, mess_caption, group_id):
        res_save = self.save_photo(link)
        if res_save:
            file_name_uuid = uuid.uuid3(uuid.NAMESPACE_DNS, link)
            file_name = self.get_img_path(file_name_uuid.urn + ".jpg")
            test = self.bot.send_photo(chat_id=group_id, photo=open(file_name, 'rb'), caption=mess_caption)
            os.remove(file_name)
            pprint(test)
            if test:
                try:
                    mess_id = test.result.message_id
                    return mess_id
                except Exception as err:
                    print("Error %s when check send photo id" % err)
                    return 0
        return 0

    def send_mess(self, mess, group_id):
        mess_new = self.bot.send_message(chat_id=group_id, text=mess)
        if mess_new:
            try:
                mess_id = mess_new.result.message_id
                return mess_id
            except Exception as err:
                print("Error %s when check send mess id" % err)
                return 0

    def reply_mess(self, mess_id, mess, group_id):
        try:
            mess_new = self.bot.send_message(chat_id=group_id, reply_to_message_id=mess_id, text=mess)
            if mess_new:
                mess_id_new = mess_new.result.message_id
                return mess_id_new
        except Exception as err:
            print("Error %s when reply message %s" % err, mess)
            return 0

    def reply_mess_by_text(self, mess_grp, mess):
        # tao mot file text co chua noi dung mess
        # send file thoi
        try:
            mess_txt = mess_grp.text
            mess_txt = mess_txt.replace("/", "")
            file_name_uuid = str(uuid.uuid3(uuid.NAMESPACE_DNS, str(mess_grp.chat.id) + mess_txt))
            if len(str(file_name_uuid)) >= 15:
                file_name = mess_txt + "_" + file_name_uuid[0:10] + ".txt"
            else:
                file_name = mess_txt + "_" + str(file_name_uuid) + ".txt"

            # tao file text
            f = open(file_name, "w+")
            f.write(str(mess))
            f.close()
            doc = open(file_name, 'rb')
            mess_new = self.bot.send_document(mess_grp.chat.id, doc)
            os.remove(file_name)
            if mess_new:
                mess_id_new = mess_new.message_id
                return mess_id_new
        except Exception as err:
            print("Error %s when reply message %s" % err, mess)
            return 0

    def send_mess_by_file(self, grp_id, mess):
        # tao mot file text co chua noi dung mess
        # send file thoi
        try:
            time_now = get_date_now_format_ipms()
            file_name_uuid = str(uuid.uuid3(uuid.NAMESPACE_DNS, str(grp_id) + str(time_now)))
            if len(str(file_name_uuid)) >= 15:
                file_name = file_name_uuid[0:10] + ".txt"
            else:
                file_name = str(file_name_uuid) + ".txt"

            # tao file text
            f = open(file_name, "w+")
            f.write(str(mess))
            f.close()
            doc = open(file_name, 'rb')
            mess_new = self.bot.send_document(grp_id, doc)
            os.remove(file_name)
            #if mess_new:
            #    mess_id_new = mess_new.message_id
            #    return mess_id_new
        except Exception as err:
            print("Error %s when reply message %s" % err, mess)
            #return 0

    def save_photo(self, url_lnk):
        file_name_uuid = uuid.uuid3(uuid.NAMESPACE_DNS, url_lnk)
        file_name = file_name_uuid.urn + '.jpg'
        try:
            urllib.request.urlretrieve(url_lnk, self.get_img_path(file_name))
        except Exception as err:
            print("Error %s when save photo" % err)
            return False
        return True


if __name__ == '__main__':
    bot = telebot.TeleBot(BOT_ID)
    _bot = BotImpl(bot)
    test = _bot.send_mess_html_channel("<i>Italic</i>", BOT_GROUP_CRC_ID)
    #test = _bot.send_mess_channel('tester')
    print(test)


