
__author__ = 'VTN-DHML-DHIP10'

import pymysql
from core.helpers.stringhelpers import check_regex_acc
from config import Development
from netaddr import IPNetwork, IPAddress
config = Development()
SERVER_NPMS = config.__getattribute__('SERVER_NPMS')
SERVER_NPMS_USERNAME = config.__getattribute__('SERVER_NPMS_USERNAME')
SERVER_NPMS_PASSWORD = config.__getattribute__('SERVER_NPMS_PASSWORD')


class TblBssNpmsImpl:
    def __init__(self):
        pass

    def get_conn(self):
        try:
            conn = pymysql.connect(host=SERVER_NPMS, user=SERVER_NPMS_USERNAME, password=SERVER_NPMS_PASSWORD)

        except Exception as err:
            print(err)
            return None
        return conn

    def get_vendor_node(self, node_name):
        chck_inp = check_regex_acc(node_name)
        res_lst = list()
        if chck_inp:
            sql_qury = "select VENDOR from npmsv2.CATA_MSC WHERE msc_code=%s group by vendor"

            conn = self.get_conn()
            if conn:
                cursor = conn.cursor()
                res_tbl_err_lst = []
                try:
                    cursor.execute(sql_qury, str(node_name))
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
                    return res_lst
        return res_lst

    def get_sub_from_node(self, node_name):
        chck_inp = check_regex_acc(node_name)
        res_lst = list()
        if chck_inp:
            sql_qury = "select msc_code as MSC, update_time as Ngay, hour_id as Khung_gio, sum(register_sub) AS Thue_bao_register, sum(attach_sub) as Thue_bao_attach from npmsv2.msc_lac_sub_rp where msc_code=%s and insert_time = (select max(insert_time) from npmsv2.msc_lac_sub_rp where msc_code=%s and insert_time >=CURDATE()-1)"

            conn = self.get_conn()
            if conn:
                cursor = conn.cursor()
                res_tbl_err_lst = []
                try:
                    parm = (node_name, node_name)
                    cursor.execute(sql_qury, parm)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
                    return res_lst
        return res_lst

    def get_prvn_from_node(self, node_name):
        chck_inp = check_regex_acc(node_name)
        res_lst = list()
        if chck_inp:
            sql_qury = "select province_code as Tinh_phuc_vu from npmsv2.msc_lac_sub_rp where msc_code=%s and insert_time = (select max(insert_time) from npmsv2.msc_lac_sub_rp where msc_code=%s and insert_time >=CURDATE()-1) group by province_code"

            conn = self.get_conn()
            if conn:
                cursor = conn.cursor()
                res_tbl_err_lst = []
                try:
                    parm = (node_name, node_name)
                    cursor.execute(sql_qury, parm)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
                    return res_lst
        return res_lst

    def get_bsc_rnc_from_node(self, node_name):
        chck_inp = check_regex_acc(node_name)
        res_lst = list()
        if chck_inp:
            sql_qury = "select bsc_rnc as BSC_RNC from npmsv2.msc_lac_sub_rp where msc_code=%s and insert_time = (select max(insert_time) from npmsv2.msc_lac_sub_rp where msc_code=%s and insert_time >=CURDATE()-1) group by bsc_rnc"

            conn = self.get_conn()
            if conn:
                cursor = conn.cursor()
                res_tbl_err_lst = []
                try:
                    parm = (node_name, node_name)
                    cursor.execute(sql_qury, parm)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
                    return res_lst
        return res_lst

    def get_sub_on_bsc_rnc_from_node(self, node_name):
        chck_inp = check_regex_acc(node_name)
        res_lst = list()
        if chck_inp:
            sql_qury = "select bsc_rnc as BSC_RNC , update_time as Ngay, hour_id as Khung_gio, sum(register_sub) AS Thue_bao_register, sum(attach_sub) as Thue_bao_attach from npmsv2.msc_lac_sub_rp where bsc_rnc=%s and insert_time = (select max(insert_time) from npmsv2.msc_lac_sub_rp where bsc_rnc=%s and insert_time >=CURDATE()-1)"

            conn = self.get_conn()
            if conn:
                cursor = conn.cursor()
                res_tbl_err_lst = []
                try:
                    parm = (node_name, node_name)
                    cursor.execute(sql_qury, parm)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
                    return res_lst
        return res_lst

    def get_prvn_from_bsc_rnc_from_node(self, node_name):
        chck_inp = check_regex_acc(node_name)
        res_lst = list()
        if chck_inp:
            sql_qury = "select province_code as Tinh_phuc_vu from npmsv2.msc_lac_sub_rp where BSC_RNC=%s and insert_time>=CURDATE()-1 group by province_code"

            conn = self.get_conn()
            if conn:
                cursor = conn.cursor()
                res_tbl_err_lst = []
                try:
                    parm = (node_name)
                    cursor.execute(sql_qury, parm)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
                    return res_lst
        return res_lst

    def get_msc_from_bsc_rnc_from_node(self, node_name):
        chck_inp = check_regex_acc(node_name)
        res_lst = list()
        if chck_inp:
            sql_qury = "SELECT msc_code as MSC FROM npmsv2.msc_lac_sub_rp where bsc_rnc=%s and insert_time>=CURDATE()-1 group by msc_code"

            conn = self.get_conn()
            if conn:
                cursor = conn.cursor()
                res_tbl_err_lst = []
                try:
                    parm = (node_name)
                    cursor.execute(sql_qury, parm)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
                    return res_lst
        return res_lst

    def get_subs_from_prvn(self, prvn_name):
        chck_inp = check_regex_acc(prvn_name)

        res_lst = list()
        if chck_inp:
            sql_qury = "select update_time as Ngay, hour_id as Khung_gio, sum(register_sub) AS Thue_bao_register, sum(attach_sub) as Thue_bao_attach from npmsv2.msc_lac_sub_rp where hour_id=(select hour_id from npmsv2.msc_lac_sub_rp where insert_time = (select max(insert_time) from npmsv2.msc_lac_sub_rp where insert_time >=CURDATE()-1) group by hour_id) and update_time=(select update_time from npmsv2.msc_lac_sub_rp where insert_time = (select max(insert_time) from npmsv2.msc_lac_sub_rp where insert_time >=CURDATE()-1) group by update_time) and province_code=%s"

            conn = self.get_conn()
            if conn:
                cursor = conn.cursor()
                res_tbl_err_lst = []
                try:
                    parm = (prvn_name)
                    cursor.execute(sql_qury, parm)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
                    return res_lst
        return res_lst

    def get_subs_all(self):
        chck_inp = True
        res_lst = list()
        if chck_inp:
            sql_qury = "select update_time as Ngay, hour_id as Khung_gio, sum(register_sub) AS Thue_bao_register, sum(attach_sub) as Thue_bao_attach from npmsv2.msc_lac_sub_rp where hour_id=(select hour_id from npmsv2.msc_lac_sub_rp where insert_time = (select max(insert_time) from npmsv2.msc_lac_sub_rp where insert_time >=CURDATE()-1) group by hour_id) and update_time=(select update_time from npmsv2.msc_lac_sub_rp where insert_time = (select max(insert_time) from npmsv2.msc_lac_sub_rp where insert_time >=CURDATE()-1) group by update_time)"

            conn = self.get_conn()
            if conn:
                cursor = conn.cursor()
                res_tbl_err_lst = []
                try:
                    cursor.execute(sql_qury)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
                    return res_lst
        return res_lst

    def get_cell_loct_info(self, cell_name):
        chck_inp = check_regex_acc(cell_name)
        res_lst = list()
        if chck_inp:
            sql_qury = "select village_name as Phuong_xa, district_name as Quan_huyen,province_name AS Tinh_ThanhPho,x AS Vy_do,y AS Kinh_do,LAC,CI,VENDOR as Vendor,CELL_STATUS AS Trang_thai_cell,APP_TIME AS Thoi_gian_cap_nhat from npmsv2.CELL_INFO WHERE CELL_CODE=%s"

            conn = self.get_conn()
            if conn:
                cursor = conn.cursor()
                res_tbl_err_lst = []
                try:
                    cursor.execute(sql_qury, cell_name)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
                    return res_lst
        return res_lst


if __name__ == '__main__':
    tbl = TblBssNpmsImpl()
    res = tbl.get_bsc_rnc_from_node("MSHL04")
    res1 = tbl.get_subs_from_prvn("HNI")
    res2 = tbl.get_subs_all()
    res3 = tbl.get_cell_loct_info("5HC1024")
    print(res)



