__author__ = 'VTN-DHML-DHIP10'
import requests, sys, json, time
from pprint import pprint
import getpass
from core.database.impl.elasticsearch.tbl_top_fw_impl import TblTopFwImpl
from config import Development

config = Development()
FW_USERNAME = config.__getattribute__('FW_USERNAME')
FW_PASSWORD = config.__getattribute__('FW_PASSWORD')
FW_FTG_LST = config.__getattribute__('FW_FTG_LST')


class FGT(object):
    def __init__(self, ip, name, vdoom, ver):
        self.name = name
        self.ip = ip
        self.vdoom = vdoom
        self.url_prefix = 'https://' + self.ip
        self.ver = ver
        self.session = requests.session()

    def update_csrf(self):
        for cookie in self.session.cookies:
            if cookie.name == 'ccsrftoken':
                csrftoken = cookie.value[1:-1]
                self.session.headers.update({'X-CSRFTOKEN': csrftoken})

    def login(self):
        url = self.url_prefix + '/logincheck'
        res_lgn = True
        try:
            res = self.session.post(url,
                                    data='username=' + FW_USERNAME + '&secretkey=' + FW_PASSWORD,
                                    verify=False)
            self.update_csrf()
        except:
            print("Error login into firewall %s" % self.ip)
            self.update_csrf()
            return False

        if res.text.find('error') != -1:
            print('LOGIN fail')
            res_lgn = False
        else:
            print('LOGIN succeed')

        return res_lgn

    def login_with_user(self, user, pas):
        url = self.url_prefix + '/logincheck'
        res_lgn = True
        try:
            res = self.session.post(url,
                                    data='username=' + user + '&secretkey=' + pas,
                                    verify=False)
            self.update_csrf()

        except:
            print("Error login into firewall %s" % self.ip)
            self.update_csrf()
            return False

        if res.text.find('error') != -1:
            print('LOGIN fail')
            res_lgn = False
        else:
            print('LOGIN succeed')

        return res_lgn

    def logout(self):
        url = self.url_prefix + '/logout'
        res = self.session.post(url)
        print('LOGOUT')

    def get(self, url_postfix, params=None, data=None, verbose=True):
        url = self.url_prefix + url_postfix
        res = None
        retry = 0
        while retry < 3:
            try:
                res = self.session.get(url, params=params, data=data)
                retry += 1
                self.update_csrf()
                return res

            except:
                time.sleep(15)
                print('Sleep when patch API ' + str(url))
                retry += 1

        return res

    def post(self, url_postfix, params=None, data=None, verbose=True):
        url = self.url_prefix + url_postfix
        res = self.session.post(url, params=params, data=data)
        self.update_csrf()
        return self.check_response(res, verbose)

    def put(self, url_postfix, params=None, data=None, verbose=True):
        url = self.url_prefix + url_postfix
        res = self.session.put(url, params=params, data=data)
        self.update_csrf()
        return self.check_response(res, verbose)

    def delete(self, url_postfix, params=None, data=None, verbose=True):
        url = self.url_prefix + url_postfix
        res = self.session.delete(url, params=params, data=data)
        self.update_csrf()
        return self.check_response(res, verbose)

    def get_v1(self, url_postfix, params=None, data=None, verbose=True):
        url = self.url_prefix + url_postfix
        payload = params
        if params:
            if 'request' in params:
                payload = 'request' + '=' + params['request']
            elif 'json' in params:
                payload = 'json' + '=' + params['json']

        res = self.session.get(url, params=payload, data=data)
        self.update_csrf()
        return self.check_response(res, verbose)

    def post_v1(self, url_postfix, params=None, data=None, verbose=True):
        url = self.url_prefix + url_postfix
        payload = params
        if params:
            if 'request' in params:
                payload = 'request' + '=' + params['request']
            elif 'json' in params:
                payload = 'json' + '=' + params['json']
        res = self.session.post(url, params=payload, data=data)
        self.update_csrf()
        return self.check_response(res, verbose)

    def put_v1(self, url_postfix, params=None, data=None, verbose=True):
        url = self.url_prefix + url_postfix
        payload = params
        if params:
            if 'request' in params:
                payload = 'request' + '=' + params['request']
            elif 'json' in params:
                payload = 'json' + '=' + params['json']
        res = self.session.put(url, params=payload, data=data)
        self.update_csrf()
        return self.check_response(res, verbose)

    def delete_v1(self, url_postfix, params=None, data=None, verbose=True):
        url = self.url_prefix + url_postfix
        payload = params
        if params:
            if 'request' in params:
                payload = 'request' + '=' + params['request']
            elif 'json' in params:
                payload = 'json' + '=' + params['json']
        res = self.session.delete(url, params=payload, data=data)
        self.update_csrf()
        return self.check_response(res, verbose)

    @staticmethod
    def check_response(response, verbose=True):
        if verbose:
            print('{0} {1}'.format(response.request.method,
                                   response.request.url))
        if response.status_code == 200:
            try:
                res = response.json()
            except:
                if verbose:
                    print('Fail invalid JSON response')
                    print(response.text)
                return False

            else:
                if not res:
                    if verbose:
                        print("JSON data is empty")
                        print(response.text)
                    return False
                if 'status' in res:
                    if res['status'] != 'success':
                        if verbose:
                            print('JSON error {0}\n{1}'.format(res['error'], res))
                        return False
                if 'http_status' in res:
                    if res['http_status'] != 200:
                        if verbose:
                            print('JSON error {0}\n{1}'.format(res['error'], res))
                        return False
                if 'http_method' in res:
                    if res['http_method'] != response.request.method:
                        if verbose:
                            print('Incorrect METHOD request {0},\
                                  response {1}'.format(response.request.method,
                                                       res['http_method']))
                        return False
                if 'results' in res:
                    if not res['results']:
                        if verbose:
                            pass
                        return False
                if verbose:
                    print('Succeed with status: {0}'.format(response.status_code))
                    pprint(res)
                return True
        else:
            try:
                res = response.json()
            except:
                pass
                if verbose:
                    print('Fail with status: {0}'.format(response.status_code))
            else:
                pass
                if verbose:
                    print('Fail with status: {0}'.format(response.status_code))
            finally:
                if verbose:
                    print(response.text)
                return False

    def get_top_session(self, tme_stmp, src_intf_chk):
        rst_lst = []
        try:
            if self.ver >= 6.0:
                rst_sess = self.get('/api/v2/monitor/fortiview/statistics/', params={"realtime": True,
                                                                                     "report_by": 'destination',
                                                                                     "vdom": self.vdoom
                                                                                     }).text
            else:
                rst_sess = self.get('/api/v2/monitor/firewall/session-top', params={"sort_by": "session",
                                                                                    "vdom": self.vdoom,
                                                                                    "report_by": "destination",
                                                                                    "summary": True
                                                                                    }).text

            res_jsn = json.loads(rst_sess)
            ress_top = []
            # phan tich json tra ra object TBL TOP FW de insert
            if res_jsn:
                if 'results' in res_jsn:
                    if self.ver >= 6.0:
                        res_top_ver = res_jsn['results']
                        if 'details' in res_top_ver:
                            ress_top = res_top_ver['details']
                    else:
                        ress_top = res_jsn['results']

                    if ress_top and isinstance(ress_top, list):
                        for x in ress_top:
                            # phan tach list tim ra object top session fw
                            addr = ''
                            des_addr = ''
                            src_addr = ''
                            cntry = ''
                            dest_port = 0
                            dest_intf = ''
                            src_intf = ''
                            rcv_mega_byte = 0.0
                            sent_mega_byte = 0.0
                            rx_pkt = 0
                            tx_pkt = 0
                            rx_bw = 0
                            tx_bw = 0
                            sess = 0
                            tx_shrp_drop = 0
                            if 'address' in x:
                                addr = x['address']
                            elif 'dstaddr' in x:
                                addr = x['dstaddr']
                                des_addr = x['dstaddr']
                            if 'srcaddr' in x:
                                src_addr = x['srcaddr']
                            if 'country' in x:
                                cntry = x['country']
                            if 'dst_port' in x:
                                dest_port = x['dst_port']
                            if 'dstintf' in x:
                                dest_intf = x['dstintf']
                            if 'srcintf' in x:
                                src_intf = x['srcintf']
                            if 'rcvdbyte' in x:
                                rcv_byte_int = x['rcvdbyte']
                                if rcv_byte_int and isinstance(rcv_byte_int, int):
                                    rcv_mega_byte = float(rcv_byte_int / 1000000)
                            elif 'rx_bytes' in x:
                                rcv_byte_int = x['rx_bytes']
                                if rcv_byte_int and isinstance(rcv_byte_int, int):
                                    rcv_mega_byte = float(rcv_byte_int / 1000000)

                            if 'sentbyte' in x:
                                sent_byte_int = x['sentbyte']
                                if sent_byte_int and isinstance(sent_byte_int, int):
                                    sent_mega_byte = float(sent_byte_int / 1000000)
                            elif 'tx_bytes' in x:
                                sent_byte_int = x['tx_bytes']
                                if sent_byte_int and isinstance(sent_byte_int, int):
                                    sent_mega_byte = float(sent_byte_int / 1000000)

                            totl_byte = rcv_mega_byte + sent_mega_byte
                            if 'rx_packets' in x:
                                rx_pkt = x['rx_packets']
                            if 'tx_packets' in x:
                                tx_pkt = x['tx_packets']
                            if 'rx_bandwidth' in x:
                                rx_bw = x['rx_bandwidth']
                            if 'tx_bandwidth' in x:
                                tx_bw = x['tx_bandwidth']
                            if 'sessions' in x:
                                sess = x['sessions']
                            elif 'session' in x:
                                sess = x['session']
                            if 'tx_shaper_drops' in x:
                                tx_shrp_drop = x['tx_shaper_drops']

                            if sess and addr and totl_byte and src_intf.upper() == src_intf_chk.upper():
                                top_fw_obj = TblTopFwImpl(node_name=self.name, node_ip=self.ip, timestamp=tme_stmp,
                                                          session=sess, sent_byte=sent_mega_byte,
                                                          recv_byte=rcv_mega_byte, src_intf=src_intf,
                                                          totl_byte=totl_byte, address=addr, country=cntry,
                                                          dest_port=dest_port, dest_intf=dest_intf, rx_packet=rx_pkt,
                                                          tx_packet=tx_pkt, rx_bandwidth=rx_bw, tx_bandwidth=tx_bw,
                                                          tx_sharper_drops=tx_shrp_drop, src_addr=src_addr,
                                                          des_addr=des_addr)
                                rst_lst.append(top_fw_obj)
        except Exception as err:
            print("Error when get top session of fw %s" % self.ip)
            print(err)
            return rst_lst

        return rst_lst

    def get_top_volume(self, tme_stmp, src_intf_chk):
        rst_lst = []
        try:
            # top_num_prm = str(top_num) + "|=>bytes"
            rst_vlm = self.get('/api/v2/monitor/firewall/session-top', params={"sort_by": "bytes",
                                                                               "vdom": self.vdoom,
                                                                               "report_by": "destination",
                                                                               "summary": True
                                                                               }).text

            res_jsn = json.loads(rst_vlm)

            # phan tich json tra ra object TBL TOP FW de insert
            if res_jsn:
                if 'results' in res_jsn:
                    ress_top = res_jsn['results']
                    if ress_top and isinstance(ress_top, list):
                        for x in ress_top:
                            # phan tach list tim ra object top session fw
                            addr = ''
                            src_addr = ''
                            des_addr = ''
                            cntry = ''
                            dest_port = 0
                            dest_intf = ''
                            src_intf = ''
                            rcv_mega_byte = 0.0
                            sent_mega_byte = 0.0
                            rx_pkt = 0
                            tx_pkt = 0
                            rx_bw = 0
                            tx_bw = 0
                            sess = 0
                            tx_shrp_drop = 0
                            if 'address' in x:
                                addr = x['address']
                            if 'country' in x:
                                cntry = x['country']
                            if 'dst_port' in x:
                                dest_port = x['dst_port']
                            if 'dstintf' in x:
                                dest_intf = x['dstintf']
                            if 'srcintf' in x:
                                src_intf = x['srcintf']
                            if 'rcvdbyte' in x:
                                rcv_byte_int = x['rcvdbyte']
                                if rcv_byte_int and isinstance(rcv_byte_int, int):
                                    rcv_mega_byte = float(rcv_byte_int / 1000000)
                            elif 'rx_bytes' in x:
                                rcv_byte_int = x['rx_bytes']
                                if rcv_byte_int and isinstance(rcv_byte_int, int):
                                    rcv_mega_byte = float(rcv_byte_int / 1000000)
                            if 'sentbyte' in x:
                                sent_byte_int = x['sentbyte']
                                if sent_byte_int and isinstance(sent_byte_int, int):
                                    sent_mega_byte = float(sent_byte_int / 1000000)
                            elif 'tx_bytes' in x:
                                sent_byte_int = x['tx_bytes']
                                if sent_byte_int and isinstance(sent_byte_int, int):
                                    sent_mega_byte = float(sent_byte_int / 1000000)
                            totl_byte = rcv_mega_byte + sent_mega_byte
                            if 'rx_packets' in x:
                                rx_pkt = x['rx_packets']
                            if 'tx_packets' in x:
                                tx_pkt = x['tx_packets']
                            if 'rx_bandwidth' in x:
                                rx_bw = x['rx_bandwidth']
                            if 'tx_bandwidth' in x:
                                tx_bw = x['tx_bandwidth']
                            if 'sessions' in x:
                                sess = x['sessions']
                            elif 'session' in x:
                                sess = x['session']

                            if 'tx_shaper_drops' in x:
                                tx_shrp_drop = x['tx_shaper_drops']

                            if sess and addr and totl_byte and src_intf.upper() == src_intf_chk:
                                top_fw_obj = TblTopFwImpl(node_name=self.name, node_ip=self.ip, timestamp=tme_stmp,
                                                          session=sess, sent_byte=sent_mega_byte,
                                                          recv_byte=rcv_mega_byte, src_intf=src_intf,
                                                          totl_byte=totl_byte, address=addr, country=cntry,
                                                          dest_port=dest_port, dest_intf=dest_intf, rx_packet=rx_pkt,
                                                          tx_packet=tx_pkt, rx_bandwidth=rx_bw, tx_bandwidth=tx_bw,
                                                          tx_sharper_drops=tx_shrp_drop, src_addr=src_addr,
                                                          des_addr=des_addr)
                                rst_lst.append(top_fw_obj)
        except Exception as err:
            print("Error when get top volume of fw %s" % self.ip)
            print(err)

            return rst_lst

        return rst_lst

    def config_download(self):
        # /api/v2/monitor/fortiview/statistics/?realtime=True&report_by=destination&count=2
        rst_vlm = self.get('/api/v2/monitor/fortiview/statistics/', params={"realtime": True,
                                                                            "report_by": 'destination',
                                                                            "count": 2,
                                                                            "vdom": self.vdoom
                                                                            }).text
        res_jsn = json.loads(rst_vlm)
        return res_jsn


if __name__ == '__main__':
    """
    fw_nme = 'FW'
    fw_ip = '10.60.91.140'
    fw_vdoom = 'root'
    fw_version = 5.0
    fw_port = 'port33'
    fgt = FGT(fw_ip, fw_nme, fw_vdoom, fw_version)
    res_lgn = fgt.login()
    if res_lgn:
        res_top_ses = fgt.get_top_session('2019-07-13T00:00:01', fw_port)
        print(res_top_ses)

    """
    # Check all list
    res_fw_lst = []
    for x in FW_FTG_LST:
        fw_nme = x['name']
        fw_ip = x['ip']
        fw_vdoom = x['vdoom']
        fw_version = x['version']
        fw_port = x['port']
        fgt = FGT(fw_ip, fw_nme, fw_vdoom, fw_version)
        res_lgn = fgt.login()
        if res_lgn:
            res_top_ses = fgt.get_top_session('2019-07-13T00:00:01', fw_port)
            res_top_vol = fgt.get_top_volume('2019-07-13T00:00:01', fw_port)
            if res_top_ses:
                res_fw_lst = res_fw_lst + res_top_ses
            if res_top_vol:
                res_fw_lst = res_fw_lst + res_top_vol
            print("Session and Volume of %s:" % fw_ip)
            print(len(res_top_ses))
            print(len(res_top_vol))
            fgt.logout()

        # ip = input("Type IP of Firewall > ")
        # ip = "10.60.141.156"
        ## vdom = input("Type vdom of Firewall > ")
        # vdom = "root"
        ## acc_fw = input("Type acc for login firewall > ")
        # acc_fw = "noc_ip"
        ## pass_fw = getpass.getpass("Type pass for login firewall > ")
        # pass_fw = "N0c_ip@321"
        # fgt = FGT(ip, 'test')
        # fgt.login(acc_fw, pass_fw)
        # fgt.get('/api/v2/cmdb')


    #
    # Chose one of them
    """
    fgt.get('/api/v2/cmdb/firewall/address', params={"action":"schema", "vdom":vdom})
    fgt.get('/api/v2/cmdb/firewall/address', params={"action":"default", "vdom":vdom})
    fgt.get('/api/v2/cmdb/firewall/address', params={"vdom":vdom})
    fgt.post('/api/v2/cmdb/firewall/address', params={"vdom":vdom},
                                              data={"json":{"name":"attacker1",
                                                            "subnet":"1.1.1.1 255.255.255.255"}},
                                              verbose=True)
    fgt.post('/api/v2/cmdb/firewall.service/custom', params={"vdom":vdom},
                                                     data={"json":{"name":"server1_port",
                                                                   "tcp-portrange":80}},
                                                     verbose=True)
    fgt.put('/api/v2/cmdb/firewall/address/address1', params={"vdom":vdom},
                                              data={"json":{"name":"address2"}})
    fgt.post('/api/v2/cmdb/firewall/policy', params={"vdom":vdom},
                                             data={"json":{"policyid":0,
                                                           "srcintf":[{"name":"lan"}],
                                                           "srcaddr":[{"name":"all"}],
                                                           "dstintf":[{"name":"wan1"}],
                                                           "dstaddr":[{"name":"all"}],
                                                           "service":[{"name":"ALL"}],
                                                           "schedule":"always",
                                                           "action":"accept"}})
    fgt.put('/api/v2/cmdb/firewall/policy/1', params={"vdom":vdom,"action":"move", "after":2})
    fgt.delete('/api/v2/cmdb/firewall/address/address2', params={"vdom":vdom})
    fgt.delete('/api/v2/cmdb/firewall/address', params={"vdom":vdom})
    """

    # Example of Monitor API requests
    # Uncomment below to run other sample requests
    # """
    # fgt.get('/api/v2/monitor')
    # fgt.get('/api/v2/monitor/firewall/policy', params={"vdom":vdom})
    # fgt.post('/api/v2/monitor/firewall/policy/clear_counters', params={"vdom":vdom, "policy":"[4,7]"})
    # fgt.get('/api/v2/monitor/firewall/session-top', params={"report_by":"source",
    #                                                         "sort_by":"bytes",
    #                                                         "vdom":vdom})
    # result_session = fgt.get('/api/v2/monitor/firewall/session-top', params={"sort_by": "session",
    #                                                                         "vdom": "root",
    #                                                                         "report_by": "destination",
    #                                                                         "summary": True,
    #                                                                         "count": 20}).text
#
# res_json = json.loads(result_session)
# print(res_json)
#
# result_volume = fgt.get('/api/v2/monitor/firewall/session-top', params={"sort_by": "bytes",
#                                                                        "vdom": "root",
#                                                                        "src_interface": "port33",
#                                                                        "report_by": "destination",
#                                                                        "summary": True,
#                                                                        "count": "20|=>bytes"}).text

#
# # """
# print(result_session)
# print(result_volume)
# res_top_ses_lst = fgt.get_top_session(20, '2019-07-11T10:15:00')
# res_top_vol_lst = fgt.get_top_volume(20, '2019-07-11T10:15:00')
#
# fgt.logout()
