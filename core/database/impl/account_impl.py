from flask_login import LoginManager, UserMixin
from config import Config, Development
import pymysql
config = Development()
SERVER_HOST = config.__getattribute__('SERVER_HOST')
SERVER_HOST_USER = config.__getattribute__('SERVER_HOST_USER')
SERVER_HOST_PASS = config.__getattribute__('SERVER_HOST_PASS')


def get_by_username(userid):
    user_obj = User(0, '', '', True)
    user_find = user_obj.find_by_user_id(userid)

    return user_find


class UserNotFoundError(Exception):
    def __init__(self, message):
        self.message = message


class User(UserMixin):

    def __init__(self, id, username, password, active=True):
        self.id = id
        self.username = username
        self.password = password
        self.active = active

    def get_id(self):
        return self.id

    def is_active(self):
        return self.active

    def get_conn(self):
        ip_server = ''

        con = pymysql.connect(host=SERVER_HOST, port=3306, user=SERVER_HOST_USER, password=SERVER_HOST_PASS,
                                  db='luuluong', cursorclass=pymysql.cursors.DictCursor)
        return con

    def find_by_username(self, username):
        connection = self.get_conn()
        cursor = connection.cursor()

        try:
            cursor.execute('SELECT Id, username, password FROM username WHERE username=%s', username)
            rows = cursor.fetchall()
            if len(rows) == 1:
                id, usr, passwd = rows[0]['Id'], rows[0]['username'], rows[0]['password']
                if usr and passwd:
                    return (id, usr, passwd)
            return None
        except Exception as err:
            print(err)
        finally:
            connection.close()

    def find_by_user_id(self, user_id):
        connection = self.get_conn()
        cursor = connection.cursor()

        try:
            sql = 'SELECT Id, username, password FROM username WHERE Id=' + str(user_id)
            cursor.execute(sql)
            rows = cursor.fetchall()
            if len(rows) == 1:
                id, usr, passwd = rows[0]['Id'], rows[0]['username'], rows[0]['password']
                if usr and passwd:
                    return User(id, usr, passwd)
            return None
        except Exception as err:
            print(err)
            return None
        finally:
            connection.close()

    def find_type_by_id(self, user_id):
        connection = self.get_conn()
        cursor = connection.cursor()

        try:
            sql = 'SELECT type FROM username WHERE Id=' + str(user_id)
            cursor.execute(sql)
            rows = cursor.fetchall()
            if len(rows) == 1:
                type = rows[0]['type']
                if type:
                    return type
            return None
        except Exception as err:
            print(err)
            return None
        finally:
            connection.close()

    def save_to_db(self):
        connection = self.get_conn()
        cursor = connection.cursor()

        try:
            cursor.execute('INSERT INTO username (username, password) VALUES (?, ?)', (self.username, self.password))
        except:
            cursor.execute('CREATE TABLE username (id INTEGER PRIMARY KEY, username TEXT, password TEXT)')
            raise UserNotFoundError('The table `users` did not exist, but it was created. Run the registration again.')
        finally:
            connection.commit()
            connection.close()

    def __repr__(self):
        return "%s/%s" % (self.username, self.password)


class UserRepository:

    def __init__(self):
        self.users = dict()
        self.users_id_dict = dict()
        self.identifier = 0

    def save_user(self, user):
        self.users_id_dict.setdefault(user.id, user)
        self.users.setdefault(user.username, user)

    def get_user(self, username):
        return self.users.get(username)

    def get_user_by_id(self, userid):
        return self.users_id_dict.get(userid)

    def next_index(self):
        self.identifier += 1
        return self.identifier