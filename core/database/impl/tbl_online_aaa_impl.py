__author__ = 'VTN-DHML-DHIP10'
import pymysql
import time
from config import Config, Development
from core.helpers.stringhelpers import check_regex_acc
from core.helpers.date_helpers import get_date_now, get_date_minus_format_ipms
config = Development()
SERVER_AAA_LST = config.__getattribute__('SERVER_AAA_LST')
SERVER_AAA_USERNAME = config.__getattribute__('SERVER_AAA_USERNAME')
SERVER_AAA_PASSWORD = config.__getattribute__('SERVER_AAA_PASSWORD')


class TblOnlineAaaImpl:
    def __init__(self, area):
        self.area = area.upper()

    def get_conn(self):
        ip_server = ''
        for x in SERVER_AAA_LST:
            if 'area' in x:
                area_x = x['area'].upper()
                if area_x == self.area:
                    if 'ip' in x:
                        ip_server = x['ip']

        if ip_server:
            con = pymysql.connect(host=ip_server, port=3306, user=SERVER_AAA_USERNAME, password=SERVER_AAA_PASSWORD,
                                  db='SESSIONS', cursorclass=pymysql.cursors.DictCursor)
            return con
        return None

    def find_acc_from_ip(self, ip_addr):
        conn = self.get_conn()
        result_acc_lst = []
        if conn:
            try:
                with conn.cursor() as cursor:
                    # check voi vendor la Juniper
                    sql = "SELECT A.username, A.nasportid, A.ipaddr, A.policy_up, A.policy_down, A.starttime, " \
                          "func_nasport_interface_juniper(A.brasport) " \
                          " AS sub_interface, B.brasname, A.bras AS ip_service,B.ipdcn,A.status, A.serviceclass," \
                          "A.lasttime, A.stopcause FROM online A, brasinfo B WHERE A.`bras` = B.`bras` " \
                          "AND A.ipaddr  = '" + str(ip_addr) + "' And B.vendor like '%JUNIPER%'"

                    cursor.execute(sql)
                    result = cursor.fetchall()
                    if result:
                        result_acc_lst = result_acc_lst + result

                    sql = "SELECT A.username, A.nasportid, A.ipaddr, A.policy_up, A.policy_down, A.starttime, " \
                          "func_nasport_interface_zte(A.brasport) " \
                          "AS sub_interface, B.brasname, A.bras AS ip_service,B.ipdcn,A.status, A.serviceclass," \
                          "A.lasttime, A.stopcause FROM online A, brasinfo B WHERE A.`bras` = B.`bras` " \
                          "AND A.ipaddr  = '" + str(ip_addr) + "'  And B.vendor like '%ZTE%'"

                    cursor.execute(sql)
                    result = cursor.fetchall()
                    if result:
                        result_acc_lst = result_acc_lst + result
                    if not result_acc_lst:
                        # cau lenh khac vay
                        sql = "select * from online where ipaddr = '" + str(ip_addr) + "'"
                        cursor.execute(sql)
                        result = cursor.fetchall()
                        if result:
                            result_acc_lst = result_acc_lst + result

            except Exception as err:
                # print(err)
                return result_acc_lst

            time.sleep(1)
            return result_acc_lst

    def find_acc(self, acc_lst):
        div_lst = [acc_lst[x:x+900] for x in range(0, len(acc_lst), 900)]
        conn = self.get_conn()
        # print(div_lst)
        # request
        result_acc_lst = list()
        if div_lst:
            for acc_sml_lst in div_lst:
                cond = ', '.join('"{0}"'.format(w) for w in acc_sml_lst)
                if conn:
                    try:
                        with conn.cursor() as cursor:
                            # check voi vendor la Juniper
                            sql = "SELECT A.username, A.nasportid, func_nasport_interface_juniper(A.brasport) " \
                                  " AS sub_interface, B.brasname, A.bras AS ip_service,B.ipdcn,A.status, A.serviceclass," \
                                  "A.lasttime, A.stopcause FROM online A, brasinfo B WHERE A.`bras` = B.`bras` "\
                                   "AND A.username IN ({})  And B.vendor like '%JUNIPER%'".format(cond)

                            cursor.execute(sql)
                            result = cursor.fetchall()
                            if result:
                                result_acc_lst = result_acc_lst + result

                            sql = "SELECT A.username, A.nasportid, func_nasport_interface_zte(A.brasport) " \
                                  "AS sub_interface, B.brasname, A.bras AS ip_service,B.ipdcn,A.status, A.serviceclass," \
                                  "A.lasttime, A.stopcause FROM online A, brasinfo B WHERE A.`bras` = B.`bras` "\
                                  "AND A.username IN ({})  And B.vendor like '%ZTE%'".format(cond)

                            cursor.execute(sql)
                            result = cursor.fetchall()
                            if result:
                                result_acc_lst = result_acc_lst + result

                    except Exception as err:
                        # print(err)
                        return result_acc_lst
                    time.sleep(1)

        conn.close()
        return result_acc_lst

    def find_acc_status(self, acc_lst):
        div_lst = [acc_lst[x:x+900] for x in range(0, len(acc_lst), 900)]
        conn = self.get_conn()
        # print(div_lst)
        # request
        result_acc_lst = list()
        if div_lst:
            for acc_sml_lst in div_lst:
                cond = ', '.join('"{0}"'.format(w) for w in acc_sml_lst)
                if conn:
                    try:
                        with conn.cursor() as cursor:
                            # check voi vendor la Juniper
                            sql = "SELECT A.username, A.mac , A.ipaddr, A.nasportid, func_nasport_interface_juniper(A.brasport) " \
                                  " AS sub_interface, B.brasname, A.bras AS ip_service,B.ipdcn,A.status, A.starttime, " \
                                  "A.serviceclass, A.policy_up, A.policy_down, " \
                                  "A.lasttime, A.stopcause FROM online A, brasinfo B WHERE A.`bras` = B.`bras` " \
                                  "AND A.username IN ({})  And B.vendor like '%JUNIPER%'".format(cond)

                            cursor.execute(sql)
                            result = cursor.fetchall()
                            if result:
                                result_acc_lst = result_acc_lst + result

                            sql = "SELECT A.username,A.mac, A.ipaddr, A.nasportid, func_nasport_interface_zte(A.brasport) " \
                                  "AS sub_interface, B.brasname, A.bras AS ip_service,B.ipdcn,A.status, A.starttime," \
                                  "A.serviceclass, A.policy_up, A.policy_down, " \
                                  "A.lasttime, A.stopcause FROM online A, brasinfo B WHERE A.`bras` = B.`bras` " \
                                  "AND A.username IN ({})  And B.vendor like '%ZTE%'".format(cond)

                            cursor.execute(sql)
                            result = cursor.fetchall()
                            if result:
                                result_acc_lst = result_acc_lst + result
                            else:
                                sql = "SELECT username, mac, ipaddr, status, starttime, lasttime, nasportid, " \
                                      "serviceclass, policy_up, policy_down FROM online " \
                                      "WHERE username IN ({}) ".format(
                                    cond)

                                cursor.execute(sql)
                                result = cursor.fetchall()
                                if result:
                                    result_acc_lst = result_acc_lst + result

                    except Exception as err:
                        # print(err)
                        return result_acc_lst
                    time.sleep(1)

        conn.close()
        return result_acc_lst

    def total_find_dev_online(self, key_srch):

        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res = 0
        if chck_sql:
            conn = self.get_conn()
            # print(div_lst)
            # request
            if conn:
                try:
                    with conn.cursor() as cursor:
                        # check voi vendor la Juniper
                        sql = "select count(username) from SESSIONS.online where nasportid like '%{}%' and status != 'stop'".format(
                            key_srch)

                        cursor.execute(sql)
                        data = cursor.fetchall()

                        if data:
                            if 'count(username)' in data[0]:
                                res = data[0]['count(username)']

                            else:
                                print('nothing')

                except Exception as err:
                    print("Error %s when check device online " % str(err))
                    return res
                finally:
                    cursor.close()
                    conn.close()
                    time.sleep(1)

        return res

    def total_find_dev_stop_last_15m(self, key_srch):

        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res = 0

        if chck_sql:
            conn = self.get_conn()

            if conn:
                try:
                    with conn.cursor() as cursor:
                        # check voi vendor la Juniper
                        sql = "select count(username) from SESSIONS.online where " \
                              "nasportid like '%{}%' and status = 'stop' and " \
                              "TIMESTAMPDIFF(SECOND, lasttime, NOW()) <= 900".format(
                            key_srch)

                        cursor.execute(sql)
                        data = cursor.fetchall()

                        if data:
                            if 'count(username)' in data[0]:
                                res = data[0]['count(username)']
                            else:
                                print('nothing')

                except Exception as err:
                    print("Error %s when find device stop last 15 mins " % str(err))
                    return res
                finally:
                    cursor.close()
                    conn.close()
                    time.sleep(1)

        return res



    def find_dev(self, key_srch):

        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        if chck_sql:
            conn = self.get_conn()
            # print(div_lst)
            # request
            if conn:
                try:
                    with conn.cursor() as cursor:
                        # check voi vendor la Juniper
                        sql = "select username from SESSIONS.online where nasportid like '%{}%' ".format(
                            key_srch)

                        cursor.execute(sql)
                        res_lst = cursor.fetchall()

                except Exception as err:
                    print("Error %s when check device online " % str(err))
                    return res_lst
                finally:
                    cursor.close()
                    conn.close()

        return res_lst

if __name__ == '__main__':
    _tbl_obj = TblOnlineAaaImpl('KV1')
    #print("Total online")
    res_lst = _tbl_obj.find_dev('LSN0435')

    print("Total offline last 15 mins")

    #print('test')
