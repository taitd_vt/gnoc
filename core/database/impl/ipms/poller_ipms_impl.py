__author__ = 'vtn-dhml-dhip10'
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.helpers.tenant_helpers import get_tenant_id

class PollerIpmsImpl:
    def __init__(self, poller_id, poller_server, poller_ip):
        self.poller_id = poller_id
        self.poller_server = poller_server
        self.poller_ip = poller_ip

    def get(self, tent_id, **kwargs):

        _api_intf_grp = ApiSpringIpmsImpl('poller', tent_id)
        res_lst = []
        try:
            res_lst = _api_intf_grp.get(kwargs)
            if res_lst:
                for x in res_lst:
                    if 'poller_id' in x and 'poller_server' in x and 'poller_ip' in x:
                        poll_obj = PollerIpmsImpl(x['poller_id'], x['poller_server'], x['poller_ip'])
                        res_lst.append(poll_obj)

        except Exception as err:
            print(err)
            return res_lst
        return res_lst

    def find_poll(self, poll_id):
        tent_id = get_tenant_id(self.poller_ip)
        res_lst = None
        if tent_id:
            _api_intf_grp = ApiSpringIpmsImpl('poller', tent_id)

            try:
                res_lst = _api_intf_grp.get(dict(searchPollerId=poll_id))
                if res_lst:
                    for x in res_lst:
                        if 'poller_id' in x and 'poller_server' in x and 'poller_ip' in x:
                            poll_obj = PollerIpmsImpl(x['poller_id'], x['poller_server'], x['poller_ip'])
                            return poll_obj

            except Exception as err:
                print(err)
                return res_lst
        return res_lst

    def lst(self, tent_id):

        _api_intf_grp = ApiSpringIpmsImpl('poller', tent_id)
        res_obj_lst = []
        try:
            res_lst = _api_intf_grp.lst()
            if res_lst:
                for x in res_lst:
                    if 'poller_id' in x and 'poller_server' in x and 'poller_ip' in x:
                        poll_obj = PollerIpmsImpl(x['poller_id'], x['poller_server'], x['poller_ip'])
                        res_obj_lst.append(poll_obj)

        except Exception as err:
            print(err)
            return res_obj_lst
        return res_obj_lst

    @staticmethod
    def find(poll_id, poll_lst):
        if poll_lst:
            for x in poll_lst:
                if hasattr(x, 'poller_id'):
                    if poll_id == x.poller_id:
                        return x
        return None
