__author__ = 'VTN-DHML-DHIP10'
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl


class HostGroupIpmsImpl:
    def __init__(self, grp_id, grp_name):
        self.group_id = grp_id
        self.group_name = grp_name

    @staticmethod
    def find_group_name(grp_name, tent_id):
        _api_fnd_grp_name = ApiSpringIpmsImpl('hostGroup', tent_id)
        res_lst = _api_fnd_grp_name.get(dict(searchGroupName=grp_name))
        return res_lst











