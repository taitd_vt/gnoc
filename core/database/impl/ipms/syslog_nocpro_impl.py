__author__ = 'vtn-dhml-dhip10'
import telebot
import time
import pymysql
from config import Config, Development
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.helpers.tenant_helpers import get_tenant_id, get_tenant_syslog_id
from core.helpers.date_helpers import get_time_format_now, get_date_minus, get_date_minus_format_elk, \
    convert_date_str_to_date_obj_spring
from core.helpers.fix_helper import find_group_name_overthreshold
from core.helpers.ipms_helpers import get_alarm_type
from core.helpers.fix_helper import filter_interface_name_ipms
from core.helpers.int_helpers import convert_string_to_float
from core.database.impl.bot.bot_impl import BotImpl
from core.threading.device_thread.dev_show_interface_drop import DeviceShowInterfaceDropThread
from core.threading.device_thread.dev_show_interface_crc_thread import DeviceShowInterfaceCrcThread
config = Development()
BOT_ID = config.__getattribute__('BOT_ID')
BOT_GROUP_OVER_THRESHOLD_ID = config.__getattribute__('BOT_GROUP_OVER_THRESHOLD_ID')
BOT_GROUP_IP_OVERTHRESHOLD_INTERNATIONAL = config.__getattribute__('BOT_GROUP_IP_OVERTHRESHOLD_INTERNATIONAL')
BOT_GROUP_CRC_ID = config.__getattribute__('BOT_GROUP_CRC_ID')
BOT_GROUP_ID = -218379033
SERVER_LOG_USER = config.__getattribute__('SERVER_LOG_USER')
SERVER_LOG_PASS = config.__getattribute__('SERVER_LOG_PASS')
SERVER_LOG_IP_LST = config.__getattribute__('SERVER_LOG_IP_LST')


class SyslogNocproIpmsImpl:
    def __init__(self, area):
        self.area = area
        self.node_name = None
        self.intf = None
        self.intf_desc = None
        self.current = None
        self.group_name = None
        self.msg = None
        self.link = None

    def get_conn(self):
        conn = None
        for x in SERVER_LOG_IP_LST:
            x_area = x['area']
            if x_area.upper() == self.area.upper():
                if x_area.upper() == 'KV3':
                    db_name = 'syslogng'
                else:
                    db_name = 'openlog'
                try:
                    conn = pymysql.connect(host=x['ip'],
                                           user=SERVER_LOG_USER,
                                           password=SERVER_LOG_PASS,
                                           db=db_name)

                except Exception as err:
                    print(err)
                    return None
        return conn

    def find_cbs(self, date_time, mes_fnd, fltr, fltr_dplc):
        tent_id = get_tenant_syslog_id(self.area)
        if tent_id:
            _api = ApiSpringIpmsImpl('syslog/nocpro', tent_id)
            data_srch = {
                # "msg": "FOIP&over threshold-2@packet error",
                "msg": mes_fnd,
                # "msg": "FOIP",
                "datetime": date_time
            }
            res_lst = _api.search(data_srch)
            res_syslog_lst = list()
            res_syslog_no_dplc = []
            msg_and_splt = []
            msg_or_splt = []

            if not res_lst:
                # select manual
                # tach truong mes_fnd bang dau &
                msg_and_splt_tmp = mes_fnd.split("&")
                for msg_and in msg_and_splt_tmp:
                    if msg_and.find('@') >= 0:
                        msg_or_splt_tmp = msg_and.split("@")
                        for msg_or in msg_or_splt_tmp:
                            if msg_or not in msg_and_splt:
                                msg_or_splt.append(msg_or)
                    else:
                        msg_and_splt.append(msg_and)
                # fix truong hop co chu T o giua
                if str(date_time).find("T") >= 0:
                    date_time = str(date_time).replace("T", " ")
                sql_find = "Select * from log_nocpro where datetime > '" + str(date_time) + "'"
                # ghep them and va or
                num_and = 0
                num_or = 0
                for msg_and in msg_and_splt:
                    if num_and == 0:
                        sql_find += " and msg like '%" + str(msg_and) + "%' "
                    else:
                        sql_find += " and msg like '%" + str(msg_and) + "%' "
                    num_and += 1
                for msg_or in msg_or_splt:
                    if num_or == 0:
                        sql_find += " and (msg like '%" + str(msg_or) + "%' "
                    else:
                        sql_find += " or msg like '%" + str(msg_or) + "%' "
                    num_or += 1
                # them phan dong ngoac vao cho full
                sql_find += ")"

                conn = self.get_conn()
                if conn:
                    try:
                        with conn.cursor() as cursor:
                            # check voi vendor la Juniper
                            cursor.execute(sql_find)
                            data = cursor.fetchall()
                            col_name_lst = [x[0] for x in cursor.description]
                            res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    except Exception as err:
                        print(str(err))
                    conn.close()
                    time.sleep(1)
            if res_lst:
                for x in res_lst:
                    try:
                        if 'datetime' in x:
                            date_time = x['datetime']
                        if 'msg' in x:
                            # tach link
                            syslog_msg = x['msg']
                            if syslog_msg:
                                msg_lst = syslog_msg.split("|")
                                len_msg_lst = len(msg_lst)
                                if len_msg_lst >= 11:
                                    node_name = msg_lst[1]
                                    node_ip = msg_lst[2]
                                    intf = msg_lst[3]
                                    intf_desc = msg_lst[4]
                                    group_name = msg_lst[5]
                                    alarm_type = msg_lst[6]
                                    alarm_name = get_alarm_type(alarm_type)
                                    current = msg_lst[7]
                                    syslog_msg = msg_lst[9]
                                    link = msg_lst[10]
                                    chck_fltr = True

                                    if alarm_type == '10':
                                        # Packet error type
                                        # fix lai link chut do ipms day sai viewplot cho viewerror
                                        if link.find('gplot') >= 0:
                                            link = link.replace('gplot', 'viewerror')
                                        # check current
                                        pos_pkt = current.find("Pkt")
                                        if pos_pkt > 0:
                                            cur_flt_str = current[:pos_pkt]
                                            cur_flt = convert_string_to_float(cur_flt_str)
                                            if cur_flt <= 5:
                                                chck_fltr = False
                                    else:
                                        if link.find('gplot') >= 0:
                                            link = link.replace('gplot', 'viewplot')
                                    if alarm_type == '1':
                                        pos_cur = current.find("%")

                                        if pos_cur > 0 > intf.upper().find('DROP'):
                                            cur_flt_str = current[:pos_cur]
                                            cur_flt = convert_string_to_float(cur_flt_str)
                                            if 0 < cur_flt < 95:
                                                chck_fltr = False
                                        else:
                                            chck_fltr = True

                                    # them mot chut filter
                                    # doi voi over threshold  la lon hon bang 95
                                    # doi voi packet error phai lon hon 5 packet
                                    if fltr:
                                        if chck_fltr:
                                            syslog_dict = dict(node_name=node_name,
                                                               node_ip=node_ip,
                                                               interface=intf,
                                                               interface_description=intf_desc,
                                                               group_name=group_name,
                                                               alarm_type=alarm_type,
                                                               current=current,
                                                               syslog_msg=syslog_msg,
                                                               link=link,
                                                               date_time=date_time,
                                                               alarm_name=alarm_name)

                                            res_syslog_lst.append(syslog_dict)
                                    else:
                                        syslog_dict = dict(node_name=node_name,
                                                           node_ip=node_ip,
                                                           interface=intf,
                                                           interface_description=intf_desc,
                                                           group_name=group_name,
                                                           alarm_type=alarm_type,
                                                           current=current,
                                                           syslog_msg=syslog_msg,
                                                           link=link,
                                                           date_time=date_time,
                                                           alarm_name=alarm_name)

                                        res_syslog_lst.append(syslog_dict)

                    except Exception as err:
                        print("Error %s when get syslog " % err)
                if not fltr_dplc:
                    # neu khong co filter duplicate thi return luon khong can loc trung nua
                    return res_syslog_lst
                elif res_syslog_lst:

                    # duyet de check duplicate
                    for sysl in res_syslog_lst:

                        if res_syslog_no_dplc:
                            chck_dplc = False
                            node_name = str(sysl['node_name']).strip()
                            intf = str(sysl['interface']).strip()

                            for sysl_no_dplc in res_syslog_no_dplc:
                                node_name_no_dplc = str(sysl_no_dplc['node_name']).strip()
                                intf_no_dplc = str(sysl_no_dplc['interface']).strip()
                                if node_name == node_name_no_dplc and intf == intf_no_dplc:
                                    chck_dplc = True
                                    break
                            if not chck_dplc:
                                res_syslog_no_dplc.append(sysl)
                                chck_dplc = False
                        else:
                            res_syslog_no_dplc.append(sysl)

            return res_syslog_no_dplc

    @staticmethod
    def get_intf_id(mess):
        # http://192.168.251.26/?target=gplot&id%5B%5D=3109213&f=2019-11-19+18:02:03&t=2019-11-20+18:02:03
        res = ''
        pos_5d = mess.find('5D')
        pos_f = mess.find('&f')
        if pos_5d > 0 and pos_f > 0:
            res = mess[pos_5d + 3: pos_f]
        return res

    def send_bot(self, res_lst, _bot):
        if res_lst:
            _bot = BotImpl(_bot)
            for x in res_lst:
                mess_id = 0
                grp_name_new = x["group_name"]
                node_name_new = x['node_name'].upper()
                capt = "Alarm Name:" + x['alarm_name'] + "\n" \
                       + "Node Name:" + x['node_name'] + "\n" \
                       + "Interface:" + x["interface"] + "\n" \
                       + "Description:" + x["interface_description"] + "\n" \
                       + "Current:" + str(x["current"]) + "\n" \
                       + "Group Name:" + x["group_name"] + "\n" \
                       + "DateTime:" + x["date_time"]

                link = x['link']
                if link.find('gplot') >= 0:
                    link = link.replace('gplot', 'viewplot')

                # check packet error cua POP
                node_name = x['node_name'].upper()
                intf_name = x['interface'].upper()
                chck_drop = False
                chck_crc = False
                if intf_name.upper().find('DROP') >= 0:
                    chck_drop = True
                if x['alarm_name'] == 'packet error':
                    chck_crc = True

                pos_mns = intf_name.find('-')
                node_ip = x['node_ip']

                # fix lai interface ID tren IPMS nen VIPA khong hieu

                # fix lai interface Drop tai vi co chua ca UDP TCP SYNC DROP nen VIPA khong hieu
                pos_undr = intf_name.find('_')
                if pos_undr >= 0:
                    intf_name = intf_name[:pos_undr]
                    intf_name = intf_name.strip()
                else:
                    if pos_mns >= 0:
                        pos_mns_2 = intf_name.find('-', pos_mns + 1)
                        if pos_mns_2 > 0:
                            intf_name = intf_name[:pos_mns_2 - 1]
                            intf_name = intf_name.strip()
                        else:
                            intf_name = intf_name[:pos_mns - 1]
                            intf_name = intf_name.strip()

                if node_name.find('POP') >= 0 and chck_drop:
                    pass

                # check packet error
                if chck_crc:
                    date_time_now = get_time_format_now()
                    # thay doi cau truc. Chi day canh bao khi CRC change > 0 hoac khi khong vao duoc thiet bi

                    _dev_crc = DeviceShowInterfaceCrcThread(False, date_time_now, node_ip, intf_name,
                                                            _bot, link, capt, mess_id, BOT_GROUP_CRC_ID)
                    _dev_crc.start()
                else:
                    sys_ystd_2_lst = []
                    sys_las_week_2_lst = []
                    # Truoc khi gui overthreshold can check them xem co bao nhieu canh bao nua trong vong 24h
                    # Interface id
                    chck_fltr = filter_interface_name_ipms(x['interface'].upper())
                    if not chck_fltr:
                        intf_id = self.get_intf_id(x['link'])

                        mess_check_2 = ''
                        mess_check = 'FOIP&' + str(intf_id) + "&" + x['alarm_name']
                        # over threshold 2
                        if x['syslog_msg'].find('over threshold 2') >= 0:
                            mess_check_2 = 'FOIP&' + str(intf_id) + "&over threshold-2"

                        time_now = get_time_format_now()

                        time_ystd = get_date_minus(time_now, 60 * 24)
                        time_lst_week = get_date_minus(time_now, 60 * 24 * 7)

                        time_ystd_elk = get_date_minus_format_elk(time_ystd, 60 * 7)
                        time_las_week_elk = get_date_minus_format_elk(time_lst_week, 60 * 7)

                        sys_ystd_lst = self.find_cbs(time_ystd_elk, mess_check, False, False)
                        sys_las_week_lst = self.find_cbs(time_las_week_elk, mess_check, False, False)

                        if sys_ystd_lst:
                            capt += '\n \nTotal number in last 24h: ' + str(len(sys_ystd_lst)) + '\n'

                        if sys_las_week_lst:
                            capt += 'Total number in last 7d: ' + str(len(sys_las_week_lst)) + '\n'

                        if mess_check_2:
                            sys_ystd_2_lst = self.find_cbs(time_ystd_elk, mess_check_2, False, False)
                            sys_las_week_2_lst = self.find_cbs(time_las_week_elk, mess_check_2, False, False)

                            if sys_ystd_2_lst:
                                capt += '\n \nTotal number over 2 in last 24h: ' + str(len(sys_ystd_2_lst)) + '\n'

                            if sys_las_week_2_lst:
                                capt += 'Total number over 2 in last 7d: ' + str(len(sys_las_week_2_lst)) + '\n'

                        chck_over_intr = find_group_name_overthreshold(grp_name_new)
                        if len(sys_ystd_lst) <= 3:
                            if chck_over_intr:
                                _bot.send_photo(link, capt, BOT_GROUP_IP_OVERTHRESHOLD_INTERNATIONAL)
                            else:
                                _bot.send_photo(link, capt, BOT_GROUP_OVER_THRESHOLD_ID)
                        elif len(sys_ystd_2_lst) <= 3:
                            if x['syslog_msg'].find('over threshold 2') >= 0:
                                if chck_over_intr:
                                    _bot.send_photo(link, capt, BOT_GROUP_IP_OVERTHRESHOLD_INTERNATIONAL)
                                else:
                                    _bot.send_photo(link, capt, BOT_GROUP_OVER_THRESHOLD_ID)
                            elif len(sys_ystd_2_lst) > 0:
                                if chck_over_intr:
                                    _bot.send_photo(link, capt, BOT_GROUP_IP_OVERTHRESHOLD_INTERNATIONAL)
                                else:
                                    _bot.send_photo(link, capt, BOT_GROUP_OVER_THRESHOLD_ID)

    def send_to_group_bot(self, res_lst, _bot, group_id):
        if res_lst:
            _bot = BotImpl(_bot)
            for x in res_lst:
                mess_id = 0
                grp_name_new = x['group_name']
                capt = "Alarm Name:" + x['alarm_name'] + "\n" \
                       + "Node Name:" + x['node_name'] + "\n" \
                       + "Interface:" + x["interface"] + "\n" \
                       + "Description:" + x["interface_description"] + "\n" \
                       + "Current:" + str(x["current"]) + "\n" \
                       + "Group Name:" + x["group_name"] + "\n" \
                       + "DateTime:" + x["date_time"]

                link = x['link']
                if link.find('gplot') >= 0:
                    link = link.replace('gplot', 'viewplot')

                # check packet error cua POP
                node_name = x['node_name'].upper()
                intf_name = x['interface'].upper()
                chck_drop = False
                chck_crc = False
                if intf_name.upper().find('DROP') >= 0:
                    chck_drop = True
                if x['alarm_name'] == 'packet error':
                    chck_crc = True

                pos_mns = intf_name.find('-')
                node_ip = x['node_ip']

                # fix lai interface ID tren IPMS nen VIPA khong hieu

                # fix lai interface Drop tai vi co chua ca UDP TCP SYNC DROP nen VIPA khong hieu
                pos_undr = intf_name.find('_')
                if pos_undr >= 0:
                    intf_name = intf_name[:pos_undr]
                    intf_name = intf_name.strip()
                else:
                    if pos_mns >= 0:
                        pos_mns_2 = intf_name.find('-', pos_mns + 1)
                        if pos_mns_2 > 0:
                            intf_name = intf_name[:pos_mns_2 - 1]
                            intf_name = intf_name.strip()
                        else:
                            intf_name = intf_name[:pos_mns - 1]
                            intf_name = intf_name.strip()

                if node_name.find('POP') >= 0 and chck_drop:
                    # check interface pop drop
                    # date_time_now = get_time_format_now()
                    pass
                    # _dev_pop_drop = DeviceShowInterfaceDropThread(False, date_time_now, node_ip, 'Cisco', intf_name,
                    #                                              _bot, link, capt, mess_id)
                    # _dev_pop_drop.start()
                # check packet error
                if chck_crc:

                    mess_id = _bot.send_photo(link, capt, group_id)

                else:
                    sys_ystd_2_lst = []
                    sys_las_week_2_lst = []
                    # Truoc khi gui overthreshold can check them xem co bao nhieu canh bao nua trong vong 24h
                    # Interface id
                    chck_fltr = filter_interface_name_ipms(x['interface'].upper())
                    if not chck_fltr:
                        intf_id = self.get_intf_id(x['link'])

                        mess_check_2 = ''
                        mess_check = 'FOIP&' + str(intf_id) + "&" + x['alarm_name']
                        # over threshold 2
                        if x['syslog_msg'].find('over threshold 2') >= 0:
                            mess_check_2 = 'FOIP&' + str(intf_id) + "&over threshold-2"

                        time_now = get_time_format_now()

                        time_ystd = get_date_minus(time_now, 60 * 24)
                        time_lst_week = get_date_minus(time_now, 60 * 24 * 7)

                        time_ystd_elk = get_date_minus_format_elk(time_ystd, 60 * 7)
                        time_las_week_elk = get_date_minus_format_elk(time_lst_week, 60 * 7)

                        sys_ystd_lst = self.find_cbs(time_ystd_elk, mess_check, False, False)
                        sys_las_week_lst = self.find_cbs(time_las_week_elk, mess_check, False, False)

                        if sys_ystd_lst:
                            capt += '\n \nTotal number in last 24h: ' + str(len(sys_ystd_lst)) + '\n'

                        if sys_las_week_lst:
                            capt += 'Total number in last 7d: ' + str(len(sys_las_week_lst)) + '\n'

                        if mess_check_2:
                            sys_ystd_2_lst = self.find_cbs(time_ystd_elk, mess_check_2, False, False)
                            sys_las_week_2_lst = self.find_cbs(time_las_week_elk, mess_check_2, False, False)

                            if sys_ystd_2_lst:
                                capt += '\n \nTotal number over 2 in last 24h: ' + str(len(sys_ystd_2_lst)) + '\n'

                            if sys_las_week_2_lst:
                                capt += 'Total number over 2 in last 7d: ' + str(len(sys_las_week_2_lst)) + '\n'

                        chck_over_intr = find_group_name_overthreshold(grp_name=grp_name_new)
                        if len(sys_ystd_lst) <= 3:
                            if chck_over_intr:
                                _bot.send_photo(link, capt, BOT_GROUP_IP_OVERTHRESHOLD_INTERNATIONAL)
                            else:
                                _bot.send_photo(link, capt, group_id)
                        elif len(sys_ystd_2_lst) <= 3:
                            if x['syslog_msg'].find('over threshold 2') >= 0:
                                if chck_over_intr:
                                    _bot.send_photo(link, capt, BOT_GROUP_IP_OVERTHRESHOLD_INTERNATIONAL)
                                else:
                                    _bot.send_photo(link, capt, group_id)
                            elif len(sys_ystd_2_lst) > 0:
                                if chck_over_intr:
                                    _bot.send_photo(link, capt, BOT_GROUP_IP_OVERTHRESHOLD_INTERNATIONAL)
                                else:
                                    _bot.send_photo(link, capt, group_id)
                time.sleep(0.3)

    def send_to_group_bot_telethon(self, res_lst):
        evnt_lst = []
        if res_lst:
            # gui vao telethon chi can gui danh sach message va danh sach link

            for x in res_lst:
                mess_id = 0
                grp_name_new = x['group_name']
                capt = "Alarm Name:" + x['alarm_name'] + "\n" \
                       + "Node Name:" + x['node_name'] + "\n" \
                       + "Interface:" + x["interface"] + "\n" \
                       + "Description:" + x["interface_description"] + "\n" \
                       + "Current:" + str(x["current"]) + "\n" \
                       + "Group Name:" + x["group_name"] + "\n" \
                       + "DateTime:" + x["date_time"]

                link = x['link']
                if link.find('gplot') >= 0:
                    link = link.replace('gplot', 'viewplot')

                # check packet error cua POP
                node_name = x['node_name'].upper()
                intf_name = x['interface'].upper()
                chck_drop = False
                chck_crc = False
                if intf_name.upper().find('DROP') >= 0:
                    chck_drop = True
                if x['alarm_name'] == 'packet error':
                    chck_crc = True

                pos_mns = intf_name.find('-')
                node_ip = x['node_ip']

                # fix lai interface ID tren IPMS nen VIPA khong hieu

                # fix lai interface Drop tai vi co chua ca UDP TCP SYNC DROP nen VIPA khong hieu
                pos_undr = intf_name.find('_')
                if pos_undr >= 0:
                    intf_name = intf_name[:pos_undr]
                    intf_name = intf_name.strip()
                else:
                    if pos_mns >= 0:
                        pos_mns_2 = intf_name.find('-', pos_mns + 1)
                        if pos_mns_2 > 0:
                            intf_name = intf_name[:pos_mns_2 - 1]
                            intf_name = intf_name.strip()
                        else:
                            intf_name = intf_name[:pos_mns - 1]
                            intf_name = intf_name.strip()

                if node_name.find('POP') >= 0 and chck_drop:
                    # check interface pop drop
                    # date_time_now = get_time_format_now()
                    pass
                    # _dev_pop_drop = DeviceShowInterfaceDropThread(False, date_time_now, node_ip, 'Cisco', intf_name,
                    #                                              _bot, link, capt, mess_id)
                    # _dev_pop_drop.start()
                # check packet error
                if chck_crc:
                    evnt_lst.append(dict(link=link, message=capt))


                else:
                    sys_ystd_2_lst = []
                    sys_las_week_2_lst = []
                    # Truoc khi gui overthreshold can check them xem co bao nhieu canh bao nua trong vong 24h
                    # Interface id
                    chck_fltr = filter_interface_name_ipms(x['interface'].upper())
                    if not chck_fltr:
                        intf_id = self.get_intf_id(x['link'])

                        mess_check_2 = ''
                        mess_check = 'FOIP&' + str(intf_id) + "&" + x['alarm_name']
                        # over threshold 2
                        if x['syslog_msg'].find('over threshold 2') >= 0:
                            mess_check_2 = 'FOIP&' + str(intf_id) + "&over threshold-2"

                        time_now = get_time_format_now()

                        time_ystd = get_date_minus(time_now, 60 * 24)
                        time_lst_week = get_date_minus(time_now, 60 * 24 * 7)

                        time_ystd_elk = get_date_minus_format_elk(time_ystd, 60 * 7)
                        time_las_week_elk = get_date_minus_format_elk(time_lst_week, 60 * 7)

                        sys_ystd_lst = self.find_cbs(time_ystd_elk, mess_check, False, False)
                        sys_las_week_lst = self.find_cbs(time_las_week_elk, mess_check, False, False)

                        if sys_ystd_lst:
                            capt += '\n \nTotal number in last 24h: ' + str(len(sys_ystd_lst)) + '\n'

                        if sys_las_week_lst:
                            capt += 'Total number in last 7d: ' + str(len(sys_las_week_lst)) + '\n'

                        if mess_check_2:
                            sys_ystd_2_lst = self.find_cbs(time_ystd_elk, mess_check_2, False, False)
                            sys_las_week_2_lst = self.find_cbs(time_las_week_elk, mess_check_2, False, False)

                            if sys_ystd_2_lst:
                                capt += '\n \nTotal number over 2 in last 24h: ' + str(len(sys_ystd_2_lst)) + '\n'

                            if sys_las_week_2_lst:
                                capt += 'Total number over 2 in last 7d: ' + str(len(sys_las_week_2_lst)) + '\n'

                        if len(sys_ystd_lst) <= 3:

                            evnt_lst.append(dict(link=link, message=capt))
                        elif len(sys_ystd_2_lst) <= 3:
                            if x['syslog_msg'].find('over threshold 2') >= 0:
                                evnt_lst.append(dict(link=link, message=capt))
                            elif len(sys_ystd_2_lst) > 0:
                                evnt_lst.append(dict(link=link, message=capt))

        return evnt_lst


if __name__ == '__main__':
    '''
    time_now = get_time_format_now()
    time_last = get_date_minus_format_elk(time_now, 60 * 7)
    while True:
        _obj = SyslogNocproIpmsImpl('KV3')
        _obj_test = dict(alarm_name='over-threshold', node_name='HKG_POP3_PEERING_ASR',
                         interface='Bundle-Ether45_class-default_egress_Droped',
                         interface_description='HHT9403CRT16.CKV.CRS16.02_BE303',
                         current='200.00%',
                         group_name='IPBN_FOIP_KV3_DROP_POP',
                         date_time='2019-10-03T16:20:02+0700',
                         link='http://10.73.224.68/?target=gplot&id%5B%5D=2758433&f=2019-10-02+17:28:02&t=2019-10-03+17:28:02',
                         node_ip='125.235.255.246')

        _obj_test_lst = [_obj_test]
        _bot = telebot.AsyncTeleBot(BOT_ID)
        tst = _obj.send_bot(_obj_test_lst, _bot)
        print(tst)
    '''
    _test = SyslogNocproIpmsImpl('KV3')
    mess_check = 'FOIP&over threshold@Packet'
    time_now = get_time_format_now()
    time_ystd = get_date_minus(time_now, 60 * 24)
    time_lst_week = get_date_minus(time_now, 60 * 24 * 1)

    time_ystd_frm = get_date_minus_format_elk(time_now, 60 * 10)
    time_las_week_frm = get_date_minus_format_elk(time_lst_week, 60 * 7)

    #sys_fnd_ystd_lst = _test.find_cbs(time_ystd_frm, mess_check, False, False)
    sys_fnd_las_week_lst = _test.find_cbs(time_las_week_frm, mess_check, True, True)
    _bot = telebot.AsyncTeleBot(BOT_ID)
    test = _test.send_bot(sys_fnd_las_week_lst, _bot)
    print(sys_fnd_las_week_lst)