__author__ = 'VTN-DHML-DHIP10'
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl


class InterfaceGroupNameIpmsImpl:
    def __init__(self, group_id, group_name):
        self.group_id = group_id
        self.group_name = group_name

    @staticmethod
    def get_grp_lst(tent_id, grp_nme):
        _api_intf_grp_nme = ApiSpringIpmsImpl('interfaceGroupName', tent_id)
        res_lst = []
        try:
            grp_lst = _api_intf_grp_nme.get(dict(searchGroupName=grp_nme))
            res_lst = []
            if grp_lst:
                for x in grp_lst:
                    if 'group_name' in x and 'group_id' in x:
                        group_name_x = x['group_name'].upper()
                        chk_thrh = group_name_x.find('RAC')
                        if chk_thrh > 0:
                            pass
                        else:
                            intf_grp_nme_obj = InterfaceGroupNameIpmsImpl(x['group_id'], x['group_name'])
                            res_lst.append(intf_grp_nme_obj)

        except Exception as err:
            print('Error %s when get group list %s ' % str(err), str(grp_nme))
            # return res_lst

        return res_lst

    @staticmethod
    def get_from_grp_id(tent_id, grp_id):
        _api_intf_grp_nme = ApiSpringIpmsImpl('interfaceGroupName', tent_id)
        res_lst = []
        try:
            grp_lst = _api_intf_grp_nme.get(dict(searchGroupId=grp_id))
            res_lst = []
            if grp_lst:
                for x in grp_lst:
                    if 'group_name' in x and 'group_id' in x:
                        group_name_x = x['group_name'].upper()
                        chk_thrh = group_name_x.find('RAC')
                        if chk_thrh > 0:
                            pass
                        else:
                            intf_grp_nme_obj = InterfaceGroupNameIpmsImpl(x['group_id'], x['group_name'])
                            res_lst.append(intf_grp_nme_obj)

        except Exception as err:
            print('Error %s when get from group id %s ' % str(err), str(grp_id))
            # return res_lst

        return res_lst

    @staticmethod
    def lst(tent_id):
        _api_intf_grp_nme = ApiSpringIpmsImpl('interfaceGroupName', tent_id)
        res_lst = []
        try:
            grp_lst = _api_intf_grp_nme.lst()
            res_lst = []
            if grp_lst:
                for x in grp_lst:
                    if 'group_name' in x and 'group_id' in x:
                        group_name_x = x['group_name'].upper()
                        chk_thrh = group_name_x.find('RAC')
                        if chk_thrh > 0:
                            pass
                        else:
                            intf_grp_nme_obj = InterfaceGroupNameIpmsImpl(x['group_id'], x['group_name'])
                            res_lst.append(intf_grp_nme_obj)

        except Exception as err:
            print('Errow %s when list ' % str(err))
            # return res_lst

        return res_lst

    @staticmethod
    def find_grp_obj_in_lst(grp_lst, grp_id):
        if grp_lst:
            for x in grp_lst:
                if hasattr(x, 'group_id'):
                    if x.group_id == grp_id:
                        return x
        return None