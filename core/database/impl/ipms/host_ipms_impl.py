__author__ = 'vtn-dhml-dhip10'
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.database.impl.ipms.host_grp_ipms_impl import HostGroupIpmsImpl


class HostIpmsImpl:
    def __init__(self, rid, ip, vgroup, hostname, sysDescr, poller):
        self.rid = rid
        self.ip = ip
        self.vgroup = vgroup
        self.hostname = hostname
        self.sysDescr = sysDescr
        self.poller = poller
        self.vendor = None
        self.type = None

    def get_vendor(self):
        if self.sysDescr.upper().find('CISCO') >= 0:
            if self.sysDescr.upper().find('ASR'):
                self.vendor = 'cisco'
                self.type = 'ASR'
                return self
            elif self.sysDescr.upper().find('CRS') >= 0:
                self.vendor = 'cisco'
                self.type = 'CRS'
                return self

        elif self.sysDescr.upper().find('HUAWEI') >= 0:
            self.vendor = 'huawei'
            self.type = ''
            return self
        elif self.sysDescr.upper().find('JUNIPER') >= 0:
            self.vendor = 'juniper'
            self.type = ''
            return self
        elif self.sysDescr.upper().find('ZTE') >= 0:
            self.vendor = 'zte'
            self.type = ''
            return self

    @staticmethod
    def find_host(tent_id, **kwargs):
        _api_fnd_grp_name = ApiSpringIpmsImpl('host', tent_id)
        res_lst = _api_fnd_grp_name.get(kwargs)
        res_host_obj_lst = list()
        if res_lst:
            for x in res_lst:
                if 'rid' in x and 'ip' in x and 'vgroup' in x and 'hostname' in x and 'sysDescr' in x and 'poller' in x:
                    host_obj = HostIpmsImpl(rid=x['rid'], ip=x['ip'], vgroup=x['vgroup'], hostname=x['hostname'],
                                            sysDescr=x['sysDescr'], poller=x['poller'])
                    res_host_obj_lst.append(host_obj)

        return res_host_obj_lst

    def find_host_in_grp_from_name(self, grp_name, tent_id):
        host_grp_obj = HostGroupIpmsImpl(0, '')
        host_lst = []
        try:
            host_grp_lst = host_grp_obj.find_group_name(grp_name, tent_id)

            if host_grp_lst:
                for x in host_grp_lst:
                    if 'group_id' in x:
                        find_dct = dict(searchGroup=str(x['group_id']))
                        host_lst += self.find_host(tent_id, **find_dct)
        except Exception as err:
            print(err)
            return host_lst
        return host_lst

    @staticmethod
    def find_intf_lst(tent_id, rid):
        #intf_lst = []
        #intf_ipms_obj = InterfaceImpsImpl(0, '', 0, 0, '', '')
#
        #try:
        #    intf_lst = intf_ipms_obj.find_intf_from_host_id(tent_id, rid)
        #except Exception as err:
        #    print(err)
        #    return intf_lst
        #return intf_lst
        pass


if __name__ == '__main__':
    _host_ipms = HostIpmsImpl(0, '', 0, '', '', '')
    _host_lst = _host_ipms.find_host_in_grp_from_name('FOIP_IPBN', 'kv1026')
    print(_host_lst)
