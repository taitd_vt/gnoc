__author__ = 'VTN-DHML-DHIP10'
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.database.impl.ipms.intf_grp_name_imps_impl import InterfaceGroupNameIpmsImpl
import json


class InterfaceGroupIpmsImpl:
    def __init__(self, group_id, iid):
        self.group_id = group_id
        self.iid = iid

    @staticmethod
    def get(tent_id, grp_name):
        intf_grp_nme_obj = InterfaceGroupNameIpmsImpl(0, '')
        _api_intf_grp = ApiSpringIpmsImpl('interfaceGroup', tent_id)
        res_lst = []
        try:
            grp_name_lst = intf_grp_nme_obj.get_grp_lst(tent_id, grp_name)

            if grp_name_lst:
                for x in grp_name_lst:
                    if hasattr(x, 'group_id'):
                        grp_id = x.group_id
                        intf_grp_lst = _api_intf_grp.get(dict(searchGroupId=str(grp_id)))
                        if intf_grp_lst:
                            res_lst = res_lst + intf_grp_lst
        except Exception as err:
            print('Error %s when get group name %s ' % str(err), str(grp_name))
            return res_lst
        return res_lst

    def check_exst(self, tent_id):
        _api_intf_grp = ApiSpringIpmsImpl('interfaceGroup', tent_id)
        data_dict = dict(searchGroupInterfaceId=self.group_id, iid=self.iid)
        res_lst = _api_intf_grp.get(data_dict)
        if res_lst:
            return True
        return False

    def insert(self, tent_id):
        _api_intf_grp = ApiSpringIpmsImpl('interfaceGroup', tent_id)
        data_dict = dict(iid=self.iid, group_id=self.group_id)
        data_json = json.dumps(data_dict)
        # check exist
        chck_exst = self.check_exst(tent_id)
        if not chck_exst:
            res = _api_intf_grp.post(data_json)
            return res
        return False


if __name__ == '__main__':
    intf_obj = InterfaceGroupIpmsImpl(0, 0)
    intf_lst = intf_obj.get('kv1026', 'FOIP')

