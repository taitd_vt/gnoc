__author__ = 'vtn-dhml-dhip10'
import json
import uuid

from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.database.impl.ipms.intf_grp_ipms_impl import InterfaceGroupIpmsImpl
from core.database.impl.ipms.intf_grp_name_imps_impl import InterfaceGroupNameIpmsImpl
from core.helpers.int_helpers import convert_string_to_int
from core.helpers.date_helpers import get_date_minus_format_ipms, convert_date_to_ipms, \
    get_date_now_gmt
from core.helpers.tenant_helpers import get_tenant_id
from core.helpers.list_helpers import get_val_exst_in_lst
from core.database.impl.ipms.host_ipms_impl import HostIpmsImpl
from core.database.impl.ipms.poller_ipms_impl import PollerIpmsImpl
from core.helpers.stringhelpers import get_content
from core.helpers.date_helpers import convert_date_to_elastic, get_date_now
from core.database.impl.elasticsearch.tbl_intf_stat_impl import TblIntfStatImpl



class InterfaceImpsImpl:
    def __init__(self, id, name, rid, speed, description, status):
        self.id = id
        self.name = name
        self.rid = rid
        self.speed = speed
        self.description = description
        self.status = status

    def get_max_in(self, time_begin, time_end, tenant_id, poller_name, rid):

        api_spring_intf_traff_ipms = ApiSpringIpmsImpl('interfaceTraffic/maxInBetween', tenant_id)
        srch_intf_traf_dct = {'poller': poller_name,
                              'rid': rid,
                              'id': self.id,
                              'dateTimeBegin': time_begin,
                              'dateTimeEnd': time_end
                              }

        max_in_intf = api_spring_intf_traff_ipms.get(srch_intf_traf_dct)
        # do rate tinh theo Byte nen phai * them 8
        if max_in_intf:
            max_in_intf *= 8
        else:
            max_in_intf = 0
        return max_in_intf

    def get_max_out(self, time_begin, time_end, tenant_id, poller_name, rid):

        api_spring_intf_traff_ipms = ApiSpringIpmsImpl('interfaceTraffic/maxOutBetween', tenant_id)
        srch_intf_traf_dct = {'poller': poller_name,
                              'rid': rid,
                              'id': self.id,
                              'dateTimeBegin': time_begin,
                              'dateTimeEnd': time_end
                              }
        max_out_intf = api_spring_intf_traff_ipms.get(srch_intf_traf_dct)
        # do rate tinh theo Byte nen phai * them 8
        if max_out_intf:
            max_out_intf *= 8
        else:
            max_out_intf = 0
        return max_out_intf

    def get_max_custom(self, time_begin, time_end, tenant_id, poller_name, rid):

        api_spring_intf_traff_ipms = ApiSpringIpmsImpl('interfaceTraffic/maxCustomBetween', tenant_id)
        srch_intf_traf_dct = {'poller': poller_name,
                              'rid': rid,
                              'id': self.id,
                              'dateTimeBegin': time_begin,
                              'dateTimeEnd': time_end}
        max_out_intf = api_spring_intf_traff_ipms.get(srch_intf_traf_dct)
        # custom khong can nhan 8
        if max_out_intf:
            max_out_intf *= 1
        else:
            max_out_intf = 0
        return max_out_intf

    def check_link_physical(self):
        if self.status == 'active' and self.name.find('gress') < 0 and self.name.find('.') < 0:
            return True
        else:
            return False

    @staticmethod
    def get_intf_from_iid(iid, tent_id):
        api_intf_ipms = ApiSpringIpmsImpl('interface', tent_id)
        iid_int = convert_string_to_int(iid)
        intf_obj = None
        if iid_int > 0:
            res_lst = api_intf_ipms.get(dict(searchId=iid_int))
            if res_lst and len(res_lst) == 1:
                x = res_lst[0]
                try:
                    if 'id' in x and 'name' in x and 'rid' in x and 'speed' in x and 'status' in x:
                        if 'description' not in x:
                            x['description'] = ''

                        intf_obj = InterfaceImpsImpl(id=x['id'],
                                                     name=x['name'],
                                                     rid=x['rid'],
                                                     speed=x['speed'],
                                                     description=x['description'],
                                                     status=x['status'])

                except Exception as err:
                    print('Error %s when get interface from iid  %s ' % str(err), str(iid))
                    return intf_obj

        return intf_obj

    @staticmethod
    def find_intf_from_host_id(tent_id, rid):
        api_intf = ApiSpringIpmsImpl('interface', tent_id)
        intf_lst = api_intf.get(dict(searchRid=rid))
        return intf_lst

    @staticmethod
    def find_intf(tent_id, **kwargs):
        _api_fnd = ApiSpringIpmsImpl('interface', tent_id)
        res_lst = _api_fnd.get(kwargs)
        res_host_obj_lst = list()
        if res_lst:
            for x in res_lst:
                try:
                    if 'id' in x and 'name' in x and 'rid' in x and 'speed' in x and 'status' in x:
                            if 'description' not in x:
                                x['description'] = ''

                            intf_obj = InterfaceImpsImpl(id=x['id'],
                                                         name=x['name'],
                                                         rid=x['rid'],
                                                         speed=x['speed'],
                                                         description=x['description'],
                                                         status=x['status'])
                            res_host_obj_lst.append(intf_obj)
                except Exception as err:
                    print("Error %s when find host %s on tent_id %s " % (str(err), x.rid, tent_id))

        return res_host_obj_lst

    # ham tim cac interface trong cac group name co ten gan giong group name. Chia ra lam 2 list. 1 list la danh sach interface
    # 1 list la group id va interface id
    def find_intf_from_grp_name(self, tent_id, grp_name):
        _intf_grp = InterfaceGroupIpmsImpl(0, 0)

        intf_grp_lst = _intf_grp.get(tent_id, grp_name)
        res_intf_lst = []
        intf_gr_id_dct_lst = []
        intf_id_lst = []

        if intf_grp_lst:
            for x in intf_grp_lst:
                try:

                    if 'iid' in x and 'group_id' in x:
                        intf_id = x['iid']
                        grp_id = x['group_id']

                        # Loc duplicate
                        if intf_id not in intf_id_lst:
                            intf_obj = self.get_intf_from_iid(intf_id, tent_id)
                            res_intf_lst.append(intf_obj)
                            intf_id_lst.append(intf_id)
                            # cho vao list cac dict de tranh lap group_id
                            intf_id_exst_lst, indx_grp_id = get_val_exst_in_lst(intf_gr_id_dct_lst, 'group_id', grp_id,
                                                                                'interface_id')
                            if intf_id_exst_lst:
                                # neu da co roi thi append interface_id vao trong group do
                                intf_id_exst_lst.append(intf_id)
                                intf_gr_id_dct_lst[indx_grp_id]['interface_id'] = intf_id_exst_lst
                            else:
                                # them moi
                                intf_gr_id_dct_lst.append(dict(group_id=grp_id, interface_id=[intf_id]))
                except Exception as err:
                    print('Error %s when find interface %s from group name %s' % str(err), str(grp_name),
                          str(intf_grp_lst))
                    # return res_intf_lst, intf_gr_id_dct_lst

        return res_intf_lst, intf_gr_id_dct_lst

    @staticmethod
    def find_grp_id_in_intf_id_lst(intf_id, intf_grp_lst):
        try:
            for x in intf_grp_lst:

                if 'group_id' in x and 'interface_id' in x:
                    grp_id_x = x['group_id']
                    intf_id_lst_x = x['interface_id']
                    if isinstance(intf_id_lst_x, list):
                        if intf_id in intf_id_lst_x:
                            return grp_id_x
        except Exception as err:
            print('Error %s when find group id in interface id list %s in %s' % str(err), str(intf_id),
                  str(intf_grp_lst))
            return 0
        return 0

    def get_host_obj(self, tent_id):
        host_obj = None
        try:
            if self.rid:
                _host_impl = HostIpmsImpl(self.rid, '', '', '', '', '')
                hst_lst = _host_impl.find_host(tent_id, **dict(searchRid=self.rid))

                if hst_lst:
                    if isinstance(hst_lst, list):
                        if len(hst_lst) == 1:
                            host_obj = hst_lst[0]
                            # host_poll = host_obj.poller
                            if host_obj:
                                return host_obj
        except Exception as err:
            print('Error %s when get Poller name ' % err)
            return host_obj
        return host_obj


if __name__ == '__main__':
    _intf_obj = InterfaceImpsImpl(0, '', 0, 0, '', '')
    poll_impl = PollerIpmsImpl(0, '', '')
    _intf_grp_name_impl = InterfaceGroupNameIpmsImpl(0, '')
    intf_grp_uplk_lst = list()
    intf_grp_tim_dct = dict(tent_id='tim038', gmt=2, group_name='GLOBAL_TIMOR_IPBN_IGW_UPLINK', area="TIMOR")
    intf_grp_cam_dct = dict(tent_id='cam134', gmt=0, group_name='GLOBAL_CAM_IPBN_IGW_UPLINK', area="CAMBODIA")
    intf_grp_moz_dct = dict(tent_id='moz022', gmt=-5, group_name='FOIP_IPBN_MOV_IGW_UPLINK', area="MOZAMBIQUE")
    intf_grp_hai_dct = dict(tent_id='hai018', gmt=-12, group_name='GLOBAL_HAITI_IPBN_IGW_UPLINK', area="HAITI")
    intf_grp_per_dct = dict(tent_id='per070', gmt=-12, group_name='GLOBAL_PERU_IPBN_IGW_UPLINK', area="PERU")
    intf_grp_bur_dct = dict(tent_id='bur040', gmt=-5, group_name='GLOBAL_BURUNDI_IPBN_IGW_UPLINK', area="BURUNDI")
    # intf_grp_uplk_lst.append(intf_grp_tim_dct)
    # intf_grp_uplk_lst.append(intf_grp_cam_dct)
    # intf_grp_uplk_lst.append(intf_grp_moz_dct)
    # intf_grp_uplk_lst.append(intf_grp_hai_dct)
    # intf_grp_uplk_lst.append(intf_grp_# per_dct)
    intf_grp_uplk_lst.append(intf_grp_bur_dct)


    #tent_area = 'tim038'
    #tent_area = 'cam134'
    #tent_area = 'moz022'
    #tent_area = 'hai018'
    #tent_area = 'per070'
    #tent_area = 'bur041'

    #area = 'BURUNDI'
    #grp_name = 'GLOBAL_TIMOR_IPBN_IGW_UPLINK'
    #grp_name = 'GLOBAL_CAM_IPBN_IGW_UPLINK'
    #grp_name = 'FOIP_IPBN_MOV_IGW_UPLINK'
    #grp_name = 'GLOBAL_HAITI_IPBN_IGW_UPLINK'
    #grp_name = 'GLOBAL_PERU_IPBN_IGW_UPLINK'
    #grp_name = 'GLOBAL_BURUNDI_IPBN_IGW_UPLINK'

    for area_obj in intf_grp_uplk_lst:
        tent_area = area_obj['tent_id']
        area = area_obj['area']
        date_time_now = get_date_now_gmt(area_obj['gmt'])
        date_time_ipms_now = convert_date_to_ipms(date_time_now)
        date_time_ipms_last_15_mins = get_date_minus_format_ipms(date_time_now, 15)
        date_time_elastic_now = convert_date_to_elastic(get_date_now())
        intf_lst = _intf_obj.find_intf_from_grp_name(area_obj['tent_id'], area_obj['group_name'])
        poll_lst = poll_impl.lst(tent_area)

        if intf_lst:
            for x in intf_lst[0]:
                x_host = x.get_host_obj(tent_area)
                if x_host:
                    poll_name = x_host.poller
                    poll_obj = poll_impl.find(poll_name, poll_lst)
                    if poll_obj:
                        hst_tent = get_tenant_id(poll_obj.poller_ip)
                        max_in = x.get_max_in(date_time_ipms_last_15_mins, date_time_ipms_now, hst_tent, poll_name, x.rid)
                        max_out = x.get_max_out(date_time_ipms_last_15_mins, date_time_ipms_now, hst_tent, poll_name, x.rid)
                        node_begin = x_host.hostname
                        intf_desc = x.description
                        net_type = 'IPBN_UPSTREAM'
                        content_name = get_content(net_type, intf_desc)
                        max_intf = max_in
                        err = 'None'
                        tbl_intf_stat = TblIntfStatImpl(node_begin=x_host.hostname,
                                                        interface_name=x.name,
                                                        ip=x_host.ip,
                                                        area=area,
                                                        description=intf_desc,
                                                        node_type='IGW',
                                                        interface_status="UP",
                                                        interface_type="TRAFFFIC",
                                                        cable_name='',
                                                        speed=x.speed,
                                                        interface_id=x.id,
                                                        network_type='IPBN_UPSTREAM',
                                                        timestamp=date_time_elastic_now,
                                                        max=max_intf,
                                                        node_place=x_host.hostname[:2],
                                                        max_in=max_in,
                                                        max_out=max_out,
                                                        error=err,
                                                        class_map='',
                                                        content=content_name)

                        tbl_intf_stat_json = json.dumps(tbl_intf_stat, default=lambda o: o.__dict__)
                        id1 = uuid.uuid3(uuid.NAMESPACE_DNS, str(x.id) + node_begin +
                                         x.name + content_name + net_type + "_" + area + "_" +
                                         date_time_elastic_now)
                        srch_dct = dict(interface_id=x.id, timestamp=date_time_elastic_now,
                                        area=area, network_type=net_type)
                        hits = es.query_search_must(srch_dct, "tbl_interface_international_status")
                        if not hits:
                            # insert new
                            res = es.create_document("tbl_interface_international_status", id1.urn, "_doc", tbl_intf_stat_json)
                            print(res)

                        else:
                            print(json.dumps(hits, indent=4))
                            id1.urn = ''