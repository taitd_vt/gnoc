__author__ = 'VTN-DHML-DHIP10'
import pymysql
import json
import os
import uuid
from core.helpers.date_helpers import convert_date_to_elk_from_ipms, get_date_yesterday_format_ipms, \
    get_date_before_yesterday_format_ipms
from config import Development
from core.helpers.stringhelpers import check_regex_acc
config = Development()
SERVER_PCTT = config.__getattribute__('SERVER_PCTT')
SERVER_PCTT_PORT = config.__getattribute__('SERVER_PCTT_PORT')
SERVER_PCTT_SERVICE_NAME = config.__getattribute__('SERVER_PCTT_SERVICE_NAME')
SERVER_PCTT_USERNAME = config.__getattribute__('SERVER_PCTT_USERNAME')
SERVER_PCTT_PASSWORD = config.__getattribute__('SERVER_PCTT_PASSWORD')


class TblPcttAppImpl:
    def __init__(self):
        pass

    def get_conn(self):
        try:
            conn = pymysql.connect(host=SERVER_PCTT, user=SERVER_PCTT_USERNAME, password=SERVER_PCTT_PASSWORD,
                                   db=SERVER_PCTT_SERVICE_NAME, port=int(SERVER_PCTT_PORT))

        except Exception as err:
            print(err)
            return None
        return conn

    def get_pos_down_lst(self, key_srch):
        sql_qury = "select tb.STATION_CODE, tb.STATION_ID, count(tb.DEVICE_CODE)  total_cabinet, sum(tb.tudown) cabinet_down, case  when  count(tb.DEVICE_CODE) = sum(tb.tudown_halted)    and count(tb.DEVICE_CODE) !=0 then 1 else 0 end vitri_down from   ( SELECT d.STATION_id, d.DEVICE_CODE,s.station_code, case when d.ALARM_STATUS=1 then 1 else 0 end tudown,  case  when d.ALARM_STATUS=1 or h.halted=1  then 1 else 0 end tudown_halted FROM pctt_app.SOS_DEVICE d  left join pctt_app.SOS_HALTED h on  h.CABINET_CODE = d.DEVICE_CODE left join pctt_app.SOS_STATION_STATUS s  on d.station_id = s.station_id   where d.NETWORK_TYPE='ACCESS_MOBILE' and d.NATION_CODE ='VNM' and s.NATION_CODE='VNM')  tb WHERE tb.STATION_CODE like '%" + str(key_srch) + "%'  group by tb.station_code;"
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res = 0
        res_lst = []
        if chck_sql:
            try:
                conn = self.get_conn()
                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_qury)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return list()
                    finally:
                        cursor.close()
                        conn.close()
            except Exception as err:
                conn = None
                print(err)

        return res_lst


if __name__ == '__main__':

    _tbl = TblPcttAppImpl(None)
    _tbl_new_lst = _tbl.get_pos_down_lst('QNM')
    print(_tbl_new_lst)


