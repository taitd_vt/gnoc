__author__ = 'VTN-DHML-DHIP10'
import cx_Oracle
import json
import os
import uuid
from core.helpers.date_helpers import convert_date_to_elk_from_ipms, get_date_yesterday_format_ipms, \
    get_date_before_yesterday_format_ipms
from config import Development

config = Development()
SERVER_NOCPRO = config.__getattribute__('SERVER_NOCPRO')
SERVER_NOCPRO_PORT = config.__getattribute__('SERVER_NOCPRO_PORT')
SERVER_NOCPRO_SERVICE_NAME = config.__getattribute__('SERVER_NOCPRO_SERVICE_NAME')
SERVER_NOCPRO_USERNAME = config.__getattribute__('SERVER_NOCPRO_USERNAME')
SERVER_NOCPRO_PASSWORD = config.__getattribute__('SERVER_NOCPRO_PASSWORD')


class TblSyslogCnttCoreImpl:
    def __init__(self, pool):
        self.pool = pool

    def get_conn(self):
        try:
            print("Connecto to NOcpro manual")
            dsn_str = cx_Oracle.makedsn(SERVER_NOCPRO, SERVER_NOCPRO_PORT, service_name=SERVER_NOCPRO_SERVICE_NAME)
            conn = cx_Oracle.connect(user=SERVER_NOCPRO_USERNAME, password=SERVER_NOCPRO_PASSWORD, dsn=dsn_str,
                                     encoding='UTF-8', nencoding='UTF-8')

        except Exception as err:
            print(err)
            return None
        return conn

    def get_alarm_repeat_lst(self):
        sql_qury = "with alarm_it as(select fault_name, fault_group_id, device_code, device_ip,SL from (select fault_name, fault_group_id, device_code, device_ip, count(*) SL from nocprov4_app.it_alarm_clear a where 1=1 and a.fault_group_id not in ('100','35','520','1721') and a.start_time >=  sysdate -1 and a.fault_level_id = 1 and a.alarm_monitor_type in (1,2,3,4,5,6) and a.fault_name not like 'Gộp%' group by fault_name, fault_group_id, device_code, device_ip ) where SL>5 ) select b.fault_name,a.fault_group_id,b.content,b.start_time, a.sl from alarm_it a, NOCPROV4_APP.IT_ALARM_CURRENT b where 1=1 and a.fault_name = b.fault_name and a.device_ip = b.device_ip and a.fault_group_id = b.fault_group_id and a.device_code = b.device_code "
        res_lst = list()
        try:
            conn = self.pool.acquire()
        except Exception as err:
            conn = None
            print(err)

        chck_pool = True
        if not conn:
            conn = self.get_conn()
            chck_pool = False
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]

            except Exception as err:
                print(err)
                return list()
            finally:
                cursor.close()
                if chck_pool:
                    self.pool.release(conn)
                else:
                    conn.close()

        return res_lst

    def get_alarm_new_lst(self):
        sql_qury = "select fault_name,fault_group_id, content, start_time from nocprov4_app.it_alarm_current a where 1=1 and a.fault_group_id in ('34') AND ((SYSDATE - a.CREATE_TIME)*60*24) >=15 and a.alarm_monitor_type not in (0,7,8) and a.fault_level_id = 1 and a.system_code in (select b.service_code from anhnt221.BOT_CNTT_SERVICE_CODE b) and a.fault_name in ( 'Process Down/Up', '[Cur] Tồn queue table theo số lượng', 'Process check url', 'Process update log', '[Cur] Table không được cập nhật', 'Process Ram', 'Process CPU' ) union all select fault_name, fault_group_id,content, start_time from nocprov4_app.it_alarm_current a where 1=1 and a.fault_group_id in ('86','87') AND ((SYSDATE - a.CREATE_TIME)*60*24) >=15 and a.alarm_monitor_type not in (0,7,8) and a.fault_level_id = 1 and a.fault_name in ('Giám sát server Down/Up', 'Mất Kết Nối Của Database', 'Số Lượng Session Inactive', 'Số Lượng Session Active', 'Cảnh báo Packet loss', 'Cảnh báo dung lượng CPU sử dụng', 'Cảnh báo dung lượng HDD sử dụng', 'Cảnh báo dung lượng RAM sử dụng', 'Instance Database Down')  union all select fault_name, fault_group_id,content, start_time from nocprov4_app.it_alarm_current a where 1=1 and a.fault_group_id in ('33') AND ((SYSDATE - a.CREATE_TIME)*60*24) >=15 and a.alarm_monitor_type not in (0,7,8) and a.fault_level_id in (1,2)  union all select fault_name, fault_group_id, content, start_time from nocprov4_app.it_alarm_current a where 1=1 and a.fault_group_id in ('104') AND ((SYSDATE - a.CREATE_TIME)*60*24) >=15 and a.alarm_monitor_type not in (0,7,8) and a.fault_level_id = 1 and content not like '%thangtd1%' and content not like '%quinguyen%' union all select fault_name, fault_group_id, content, start_time from nocprov4_app.it_alarm_current a where 1=1 and a.fault_group_id not in ('100','35','520','1721', '33', '34','86','87','1922','104') AND ((SYSDATE - a.CREATE_TIME)*60*24) >=15 and a.alarm_monitor_type not in (0,7,8) and a.fault_level_id = 1"
        res_lst = list()
        chck_pool = True
        try:
            conn = self.pool.acquire()
        except Exception as err:
            conn = None
            print(err)

        if not conn:
            conn = self.get_conn()
            chck_pool = False
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]

            except Exception as err:
                print(err)
                return list()
            finally:
                cursor.close()
                if chck_pool:
                    self.pool.release(conn)
                else:
                    conn.close()
        return res_lst


if __name__ == '__main__':
    dsn_str = cx_Oracle.makedsn(SERVER_NOCPRO, SERVER_NOCPRO_PORT, service_name=SERVER_NOCPRO_SERVICE_NAME)
    pool_nocpro = cx_Oracle.SessionPool(min=10,
                                        max=10, increment=1, threaded=True, dsn=dsn_str,
                                        user=SERVER_NOCPRO_USERNAME, password=SERVER_NOCPRO_PASSWORD,
                                        encoding='UTF-8', nencoding='UTF-8')
    _tbl = TblSyslogCnttCoreImpl(pool_nocpro)
    _tbl_new_lst = _tbl.get_alarm_new_lst()
    _tbl_clear_lst = _tbl.get_alarm_repeat_lst()


