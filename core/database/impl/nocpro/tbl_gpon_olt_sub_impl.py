#!/D:\python36 new
# -*- coding: utf8 -*-
import cx_Oracle

from core.helpers.stringhelpers import check_regex_acc
from core.helpers.date_helpers import get_date_now, convert_date_str_to_date_obj_ipms
from config import Development
import os
import sys

config = Development()
SERVER_NOCPRO = config.__getattribute__('SERVER_NOCPRO')
SERVER_NOCPRO_PORT = config.__getattribute__('SERVER_NOCPRO_PORT')
SERVER_NOCPRO_SERVICE_NAME = config.__getattribute__('SERVER_NOCPRO_SERVICE_NAME')
SERVER_NOCPRO_USERNAME = config.__getattribute__('SERVER_NOCPRO_USERNAME')
SERVER_NOCPRO_PASSWORD = config.__getattribute__('SERVER_NOCPRO_PASSWORD')


class TblGponOltSubImpl:
    def __init__(self, pool):
        self.pool = pool

    def get_conn(self):
        try:
            chck_pool = True
            try:
                conn = self.pool.acquire()
            except Exception as err:
                conn = None
                print(err)

            if not conn:
                print("Connecto to NOcpro manual")
                dsn_str = cx_Oracle.makedsn(SERVER_NOCPRO, SERVER_NOCPRO_PORT, service_name=SERVER_NOCPRO_SERVICE_NAME)
                conn = cx_Oracle.connect(user=SERVER_NOCPRO_USERNAME, password=SERVER_NOCPRO_PASSWORD,
                                         dsn=dsn_str, encoding='UTF-8', nencoding='UTF-8')
                chck_pool = False

        except Exception as err:
            print(err)
            return None, False
        return conn, chck_pool

    def get_total_cur(self, key_srch):
        # check key_srch co ma doc khong:
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res = 0

        if chck_sql:
            if key_srch:
                sql_orc = "select COUNT(*) from (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                          "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                          "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                          "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                          "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                          "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CUR where " \
                          "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%'))"

            else:
                sql_orc = "SELECT COUNT(*) FROM (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                          "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                          "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                          "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                          "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                          "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CUR where " \
                          "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%') "

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    if res_lst:
                        if 'COUNT(*)' in res_lst[0]:
                            res = res_lst[0]['COUNT(*)']

                except Exception as err:
                    print(err)
                    return res
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res

    def get_total_sub_ftth(self, key_srch):
        # check key_srch co ma doc khong:
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res = 0

        if chck_sql:
            if key_srch:
                sql_orc = "select COUNT(1) from NOCPROV4_APP.CAT_NIMS_SUB where  " \
                          "device_code LIKE '%" + key_srch + "%' and PRODUCT_TYPE  = 'F'"

            else:
                sql_orc = "select COUNT(1) from NOCPROV4_APP.CAT_NIMS_SUB where " \
                          "PRODUCT_TYPE in ='F'"

            conn, type_conn = self.get_conn()
            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    if res_lst:
                        if 'COUNT(1)' in res_lst[0]:
                            res = res_lst[0]['COUNT(1)']

                except Exception as err:
                    print(err)
                    return res
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res

    def find_total_offline_cable_olt(self, key_srch):

        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []

        if chck_sql:
            conn, type_conn = self.get_conn()
            if conn:

                try:
                    with conn.cursor() as cursor:
                        # check voi vendor la Juniper
                        sql = "with node_down as ( select a.device_code, a.node_code, count(*) Tong_account, ( select COUNT(*) from nocprov4_app.GPON_OLT_SUB_CUR b where 1=1 and b.service_code = 'ALARM_GPON_BROKEN_CABLE' and b.trouble_type = 'Đứt cáp trục/nhánh' and b.is_collected_alarm != 1 and a.node_code = b.nodecode ) Account_offline from NOCPROV4_APP.CAT_NIMS_SUB a where 1=1 and device_code = :key_srch group by a.device_code, a.node_code ), nguyen_nhan as ( select nodecode, trouble_type, count(*) from nocprov4_app.GPON_OLT_SUB_CUR where 1=1 and device_code = :key_srch and service_code = 'ALARM_GPON_BROKEN_CABLE' and trouble_type = 'Đứt cáp trục/nhánh' and is_collected_alarm != 1 group by nodecode, trouble_type ) select x.device_code, x.node_code OLT_Nhanh,x.tong_account, x.Account_offline, nvl(y.trouble_type,' ') Nguyen_nhan from node_down x full join nguyen_nhan y on x.node_code = y.nodecode order by node_code asc"

                        cursor.execute(sql, key_srch=key_srch)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print("Error %s when find device stop last 15 mins " % str(err))
                    return res_lst
                finally:

                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
                    return res_lst

        return res_lst

    def get_total_sub_multiscreen(self, key_srch):
        # check key_srch co ma doc khong:
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res = 0

        if chck_sql:
            if key_srch:
                sql_orc = "select COUNT(1) from NOCPROV4_APP.CAT_NIMS_SUB where  " \
                          "device_code LIKE '%" + key_srch + "%' and PRODUCT_TYPE  = 'U'"

            else:
                sql_orc = "select COUNT(1) from NOCPROV4_APP.CAT_NIMS_SUB where " \
                          "PRODUCT_TYPE in ='U'"

            conn, type_conn = self.get_conn()
            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    if res_lst:
                        if 'COUNT(1)' in res_lst[0]:
                            res = res_lst[0]['COUNT(1)']

                except Exception as err:
                    print(err)
                    return res
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res

    def get_sub_multiscreen(self, key_srch):
        # check key_srch co ma doc khong:
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''
        if chck_sql:
            if key_srch:
                sql_orc = "select ACCOUNT_ISDN from NOCPROV4_APP.CAT_NIMS_SUB where  " \
                          "device_code LIKE '%" + key_srch + "%' and PRODUCT_TYPE  = 'U'"

            conn, type_conn = self.get_conn()
            if conn and sql_orc != '':
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_total_trouble_cur(self, key_srch, trouble_id):
        # check key_srch co ma doc khong:
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res = 0
        if chck_sql:
            if key_srch:
                sql_orc = "select COUNT(*) from (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                          "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                          "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                          "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                          "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                          "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CUR where " \
                          "start_time >= trunc(sysdate-1) and trouble_type_id = " + str(trouble_id) + " and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%'))"

            else:
                sql_orc = "SELECT COUNT(*) FROM (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                          "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                          "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                          "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                          "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                          "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CUR where " \
                          "start_time >= trunc(sysdate-1) and trouble_type_id = " + str(trouble_id) + " and account_customer not LIKE '%|%') "

            conn, type_conn = self.get_conn()
            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    if res_lst:
                        if 'COUNT(*)' in res_lst[0]:
                            res = res_lst[0]['COUNT(*)']

                except Exception as err:
                    print(err)
                    return res
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res

    def get_total_all(self, key_srch):
        # check key_srch co ma doc khong:
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res = 0
        if chck_sql:
            if key_srch:
                sql_orc = "select COUNT(*) from (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                          "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                          "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                          "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                          "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                          "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CLEAR where " \
                          "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%') union all select " \
                          "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                          "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                          "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, " \
                          "FULLNAME, NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME," \
                          "DEVICE_TYPE_NAME,N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.GPON_OLT_SUB_CUR where " \
                          "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%') union all select " \
                          "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                          "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                          "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                          "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                          "N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.gpon_olt_sub_his where " \
                          "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%'))  " \
                          ""

            else:
                sql_orc = "SELECT COUNT(*) FROM (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                          "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                          "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                          "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                          "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                          "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CLEAR where " \
                          "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' union all select " \
                          "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                          "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                          "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, " \
                          "FULLNAME, NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME," \
                          "DEVICE_TYPE_NAME,N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.GPON_OLT_SUB_CUR where " \
                          "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' union all select " \
                          "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                          "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                          "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                          "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                          "N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.gpon_olt_sub_his where " \
                          "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%')"

            conn, type_conn = self.get_conn()
            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    if res_lst:
                        if 'COUNT(*)' in res_lst[0]:
                            res = res_lst[0]['COUNT(*)']

                except Exception as err:
                    print(err)
                    return res
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release()
                    else:
                        conn.close()
        return res

    def get_count_cus_err(self, key_srch):
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res = 0
        if chck_sql:
            if key_srch:
                sql_orc = "select COUNT(*) from (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                          "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                          "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                          "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                          "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                          "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CUR where " \
                          "start_time >= trunc(sysdate-1) and (trouble_type like '%Đứt cáp trục/nhánh%' " \
                          "or trouble_type like '%Mất dịch vụ%' or trouble_type like '%Đứt cáp trục%' " \
                          "or trouble_type like '%Đứt cáp thuê bao%' or trouble_type like '%Lỗi thiết bị%'  " \
                          "or trouble_type like '%Sự cố mạng metro%') " \
                          "and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' " \
                          "or account_customer LIKE '%" + key_srch + "%'))"


            else:
                sql_orc = "select COUNT(*) from (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                          "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                          "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                          "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                          "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                          "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CUR where " \
                          "start_time >= trunc(sysdate-1) and (trouble_type like '%Đứt cáp trục/nhánh%' " \
                          "or trouble_type like '%Mất dịch vụ%' or trouble_type like '%Đứt cáp trục%' " \
                          "or trouble_type like '%Đứt cáp thuê bao%' or trouble_type like '%Lỗi thiết bị%'  " \
                          "or trouble_type like '%Sự cố mạng metro%') " \
                          "and account_customer not LIKE '%|%')"

            conn, type_conn = self.get_conn()
            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    if res_lst:
                        if 'COUNT(*)' in res_lst[0]:
                            res = res_lst[0]['COUNT(*)']

                except Exception as err:
                    print(err)
                    return res
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res

    def get_total_trouble_all(self, key_srch, trouble_id):
        # check key_srch co ma doc khong:
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res = 0
        if chck_sql:
            if key_srch:
                sql_orc = "select COUNT(*) from (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                          "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                          "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                          "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                          "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                          "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CLEAR where " \
                          "start_time >= trunc(sysdate-1) and trouble_type_id = " + str(trouble_id) + " and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%') union all select " \
                          "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                          "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                          "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, " \
                          "FULLNAME, NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME," \
                          "DEVICE_TYPE_NAME,N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.GPON_OLT_SUB_CUR where " \
                          "start_time >= trunc(sysdate-1) and trouble_type_id = " + str(trouble_id) + " and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%') union all select " \
                          "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                          "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                          "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                          "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                          "N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.gpon_olt_sub_his where " \
                          "start_time >= trunc(sysdate-1) and trouble_type_id = " + str(trouble_id) + " and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%'))  " \
                          ""

            else:
                sql_orc = "SELECT COUNT(*) FROM (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                          "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                          "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                          "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                          "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                          "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CLEAR where " \
                          "start_time >= trunc(sysdate-1) and trouble_type_id = " + str(trouble_id) + " and account_customer not LIKE '%|%' union all select " \
                          "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                          "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                          "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, " \
                          "FULLNAME, NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME," \
                          "DEVICE_TYPE_NAME,N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.GPON_OLT_SUB_CUR where " \
                          "start_time >= trunc(sysdate-1) and trouble_type_id = " + str(trouble_id) + " and account_customer not LIKE '%|%' union all select " \
                          "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                          "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                          "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                          "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                          "N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.gpon_olt_sub_his where " \
                          "start_time >= trunc(sysdate-1) and trouble_type_id = " + str(trouble_id) + " and account_customer not LIKE '%|%')"

            conn, type_conn = self.get_conn()
            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    if res_lst:
                        if 'COUNT(*)' in res_lst[0]:
                            res = res_lst[0]['COUNT(*)']

                except Exception as err:
                    print(err)
                    return res
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res

    def get_page_all_lst(self, key_srch, page_num, page_size):
        key_srch = key_srch.strip()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        try:
            if chck_sql and page_num >= 0 and page_size > 0:
                row_num_high = (page_num * page_size) + 1
                row_num_low = ((page_num - 1) * page_size) + 1
                if key_srch:
                    sql_orc = "SELECT * FROM (SELECT a.*, rownum r__ FROM ((select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                              "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                              "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                              "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CLEAR where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%') union all select " \
                              "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                              "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                              "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, " \
                              "FULLNAME, NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME," \
                              "DEVICE_TYPE_NAME,N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.GPON_OLT_SUB_CUR where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%') union all select " \
                              "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                              "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                              "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.gpon_olt_sub_his where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%') ) ) a " \
                              "WHERE rownum < " + str(row_num_high) + " ) WHERE r__ >= (" + str(row_num_low) + ")"
                else:
                    sql_orc = "SELECT * FROM (SELECT a.*, rownum r__ FROM (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                              "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                              "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                              "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CLEAR where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' union all select " \
                              "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                              "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                              "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, " \
                              "FULLNAME, NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME," \
                              "DEVICE_TYPE_NAME,N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.GPON_OLT_SUB_CUR where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' union all select " \
                              "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                              "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                              "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.gpon_olt_sub_his where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' ) a " \
                              "WHERE rownum < " + str(row_num_high) + ") WHERE r__ >= (" + str(row_num_low) + ")"
                conn, type_conn = self.get_conn()
                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
                        return res_lst
        except Exception as err:
            print('Error %s when get Cell list' % err)
            return res_lst

    def get_page_all_trouble_lst(self, key_srch, trouble_id, page_num, page_size):
        key_srch = key_srch.strip()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        try:
            if chck_sql and page_num >= 0 and page_size > 0:
                row_num_high = (page_num * page_size) + 1
                row_num_low = ((page_num - 1) * page_size) + 1
                if key_srch:
                    sql_orc = "SELECT * FROM (SELECT a.*, rownum r__ FROM ((select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                              "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                              "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                              "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CLEAR where " \
                              "start_time >= trunc(sysdate-1) and trouble_type_id = " + str(trouble_id) + " and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%') union all select " \
                              "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                              "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                              "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, " \
                              "FULLNAME, NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME," \
                              "DEVICE_TYPE_NAME,N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.GPON_OLT_SUB_CUR where " \
                              "start_time >= trunc(sysdate-1) and trouble_type_id = " + str(trouble_id) + " and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%') union all select " \
                              "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                              "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                              "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.gpon_olt_sub_his where " \
                              "start_time >= trunc(sysdate-1) and trouble_type_id = " + str(trouble_id) + " and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%') ) ) a " \
                              "WHERE rownum < " + str(row_num_high) + " ) WHERE r__ >= (" + str(row_num_low) + ")"
                else:
                    sql_orc = "SELECT * FROM (SELECT a.*, rownum r__ FROM (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                              "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                              "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                              "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CLEAR where " \
                              "start_time >= trunc(sysdate-1) and trouble_type_id = " + str(trouble_id) + " and account_customer not LIKE '%|%' union all select " \
                              "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                              "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                              "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, " \
                              "FULLNAME, NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME," \
                              "DEVICE_TYPE_NAME,N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.GPON_OLT_SUB_CUR where " \
                              "start_time >= trunc(sysdate-1) and trouble_type_id = " + str(trouble_id) + " and account_customer not LIKE '%|%' union all select " \
                              "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                              "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                              "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.gpon_olt_sub_his where " \
                              "start_time >= trunc(sysdate-1) and trouble_type_id = " + str(trouble_id) + " and account_customer not LIKE '%|%' ) a " \
                              "WHERE rownum < " + str(row_num_high) + ") WHERE r__ >= (" + str(row_num_low) + ")"
                conn, type_conn = self.get_conn()
                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
                        return res_lst
        except Exception as err:
            print('Error %s when get Cell list' % err)
            return res_lst

    def get_page_cur_lst(self, key_srch, page_num, page_size):
        key_srch = key_srch.strip()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        try:
            if chck_sql and page_num >= 0 and page_size > 0:
                row_num_high = (page_num * page_size) + 1
                row_num_low = ((page_num - 1) * page_size) + 1
                if key_srch:
                    sql_orc = "SELECT * FROM (SELECT a.*, rownum r__ FROM ((select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                              "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                              "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                              "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CUR where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%')  ) ) a " \
                              "WHERE rownum < " + str(row_num_high) + " ) WHERE r__ >= (" + str(row_num_low) + ")"
                else:
                    sql_orc = "SELECT * FROM (SELECT a.*, rownum r__ FROM (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                              "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                              "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                              "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CUR where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' ) a " \
                              "WHERE rownum < " + str(row_num_high) + ") WHERE r__ >= (" + str(row_num_low) + ")"
                conn, type_conn = self.get_conn()
                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
                        return res_lst
        except Exception as err:
            print('Error %s when get Cell list' % err)
            return res_lst

    def get_page_cur_trouble_type_lst(self, key_srch, trouble_id, page_num, page_size):
        key_srch = key_srch.strip()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        try:
            if chck_sql and page_num >= 0 and page_size > 0:
                row_num_high = (page_num * page_size) + 1
                row_num_low = ((page_num - 1) * page_size) + 1
                if key_srch:
                    sql_orc = "SELECT * FROM (SELECT a.*, rownum r__ FROM ((select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                              "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                              "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                              "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CUR where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' and trouble_type_id = " + str(trouble_id) + " and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%')  ) ) a " \
                              "WHERE rownum < " + str(row_num_high) + " ) WHERE r__ >= (" + str(row_num_low) + ")"
                else:
                    sql_orc = "SELECT * FROM (SELECT a.*, rownum r__ FROM (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                              "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                              "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                              "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CUR where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' and trouble_type_id = " + str(trouble_id) + ") a " \
                              "WHERE rownum < " + str(row_num_high) + ") WHERE r__ >= (" + str(row_num_low) + ")"
                conn, type_conn = self.get_conn()
                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
                        return res_lst
        except Exception as err:
            print('Error %s when get Cell list' % err)
            return res_lst

    def get_cur_lst(self, key_srch):
        key_srch = key_srch.strip()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        try:
            if chck_sql:
                if key_srch:
                    sql_orc = "SELECT * FROM (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                              "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                              "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                              "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CUR where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' and " \
                              "(device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%')) "

                else:
                    sql_orc = "SELECT * FROM (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                              "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                              "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                              "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CUR where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' ) " \

                conn, type_conn = self.get_conn()
                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
                        return res_lst
        except Exception as err:
            print('Error %s when get Cell list' % err)
            return res_lst

    def get_cur_trouble_lst(self, key_srch, trouble_id):
        key_srch = key_srch.strip()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        try:
            if chck_sql:
                if key_srch:
                    sql_orc = "SELECT * FROM (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                              "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                              "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                              "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CUR where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' and trouble_type_id = " + str(trouble_id) + " and " \
                              "(device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%')) "

                else:
                    sql_orc = "SELECT * FROM (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                              "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                              "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                              "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CUR where " \
                              "start_time >= trunc(sysdate-1) and trouble_type_id = " + str(trouble_id) + " and account_customer not LIKE '%|%' ) " \

                conn, type_conn = self.get_conn()
                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
                        return res_lst
        except Exception as err:
            print('Error %s when get Cell list' % err)
            return res_lst

    def get_all_lst(self, key_srch):
        key_srch = key_srch.strip()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        try:
            if chck_sql:
                if key_srch:
                    sql_orc = "SELECT * FROM (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                              "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                              "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                              "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CLEAR where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%') union all select " \
                              "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                              "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                              "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, " \
                              "FULLNAME, NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME," \
                              "DEVICE_TYPE_NAME,N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.GPON_OLT_SUB_CUR where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%') union all select " \
                              "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                              "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                              "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.gpon_olt_sub_his where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%')) " \

                else:
                    sql_orc = "SELECT * FROM (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                              "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                              "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                              "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CLEAR where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' union all select " \
                              "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                              "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                              "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, " \
                              "FULLNAME, NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME," \
                              "DEVICE_TYPE_NAME,N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.GPON_OLT_SUB_CUR where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' union all select " \
                              "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                              "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                              "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.gpon_olt_sub_his where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' )  " \

                conn, type_conn = self.get_conn()
                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
                        return res_lst
        except Exception as err:
            print('Error %s when get Cell list' % err)
            return res_lst

    def get_all_trouble_lst(self, key_srch, trouble_id):
        key_srch = key_srch.strip()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        try:
            if chck_sql:
                if key_srch:
                    sql_orc = "SELECT * FROM (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                              "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                              "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                              "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CLEAR where " \
                              "start_time >= trunc(sysdate-1)  and trouble_type_id = " + str(trouble_id) + " and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%') union all select " \
                              "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                              "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                              "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, " \
                              "FULLNAME, NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME," \
                              "DEVICE_TYPE_NAME,N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.GPON_OLT_SUB_CUR where " \
                              "start_time >= trunc(sysdate-1)  and trouble_type_id = " + str(trouble_id) + " and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%') union all select " \
                              "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                              "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                              "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.gpon_olt_sub_his where " \
                              "start_time >= trunc(sysdate-1) and trouble_type_id = " + str(trouble_id) + " and account_customer not LIKE '%|%' and (device_code LIKE '%" + key_srch + "%' or account_customer LIKE '%" + key_srch + "%')) " \

                else:
                    sql_orc = "SELECT * FROM (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                              "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                              "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                              "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CLEAR where " \
                              "start_time >= trunc(sysdate-1)  and trouble_type_id = " + str(trouble_id) + " and account_customer not LIKE '%|%' union all select " \
                              "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                              "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                              "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, " \
                              "FULLNAME, NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME," \
                              "DEVICE_TYPE_NAME,N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.GPON_OLT_SUB_CUR where " \
                              "start_time >= trunc(sysdate-1)  and trouble_type_id = " + str(trouble_id) + " and account_customer not LIKE '%|%' union all select " \
                              "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                              "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                              "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.gpon_olt_sub_his where " \
                              "start_time >= trunc(sysdate-1) and trouble_type_id = " + str(trouble_id) + " and account_customer not LIKE '%|%' )  " \

                conn, type_conn = self.get_conn()
                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
                        return res_lst
        except Exception as err:
            print('Error %s when get Cell list' % err)
            return res_lst

    def get_page_exact_lst(self, key_srch, page_num, page_size):
        chck_sql = check_regex_acc(key_srch)
        res_lst = []
        try:
            if chck_sql and page_num > 0 and page_size > 0:
                row_num_high = (page_num * page_size) + 1
                row_num_low = ((page_num - 1) * page_size) + 1
                if key_srch:
                    sql_orc = "SELECT * FROM (SELECT a.*, rownum r__ FROM ((select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                              "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                              "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                              "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CLEAR where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' and (device_code = '%" + key_srch + "%' or account_customer = '% " + key_srch + "%') union all select " \
                              "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                              "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                              "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, " \
                              "FULLNAME, NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME," \
                              "DEVICE_TYPE_NAME,N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.GPON_OLT_SUB_CUR where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' and (device_code = '%" + key_srch + "%' or account_customer = '% " + key_srch + "%') union all select " \
                              "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                              "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                              "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.gpon_olt_sub_his where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' and (device_code = '%" + key_srch + "%' or account_customer = '% " + key_srch + "%')) ) a " \
                              "WHERE rownum < " + str(row_num_high) + " ) WHERE r__ >= (" + str(row_num_low) + ")"
                else:
                    sql_orc = "SELECT * FROM (SELECT a.*, rownum r__ FROM (select ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE," \
                              "VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL,AREA_CODE, AREA_NAME, " \
                              "PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, DISTRICT_NAME, " \
                              "ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from NOCPROV4_APP.GPON_OLT_SUB_CLEAR where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' union all select " \
                              "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                              "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                              "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, " \
                              "FULLNAME, NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME," \
                              "DEVICE_TYPE_NAME,N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.GPON_OLT_SUB_CUR where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' union all select " \
                              "ID,START_TIME,END_TIME,CONTENT,DEVICE_CODE,VENDOR_CODE,PORT_LOGIC,PORT_PHYSICAL," \
                              "AREA_CODE, AREA_NAME, PROVINCE_CODE, PROVINCE_NAME, DISTRICT_CODE, " \
                              "DISTRICT_NAME, ACCOUNT_GLINE, ACCOUNT_CUSTOMER, PHONE, ADDRESS, FULLNAME, " \
                              "NODECODE, TROUBLE_TYPE, VILLAGE_CODE, VILLAGE_NAME,DEVICE_TYPE_NAME," \
                              "N_ACCOUNTS,N_ACCOUNT_AFFECT from nocprov4_app.gpon_olt_sub_his where " \
                              "start_time >= trunc(sysdate-1) and account_customer not LIKE '%|%' ) a " \
                              "WHERE rownum < " + str(row_num_high) + ") WHERE r__ >= (" + str(row_num_low) + ")"
                conn,type_conn = self.get_conn()
                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
                        return res_lst
        except Exception as err:
            print('Error %s when get Cell list' % err)
            return res_lst

    def update_note_new(self, id_lst, note_new):
        chck_sql = check_regex_acc(note_new)
        res_lst = []
        try:
            if chck_sql:
                id_lst_str = ''
                for x in id_lst:
                    id_lst_str += str(x) + ','
                id_lst_str = id_lst_str[:-1]

                if id_lst_str:

                    conn, type_conn = self.get_conn()
                    if conn:
                        cursor = conn.cursor()

                        try:
                            cursor.execute("UPDATE NOCPROV4_APP.GPON_OLT_SUB_CLEAR SET note=:note where ID =:id", note=note_new, id=102564614)
                            conn.commit()

                        except Exception as err:
                            print(err)
                            return res_lst
                        finally:
                            cursor.close()
                            if type_conn:
                                self.pool.release(conn)
                            else:
                                conn.close()
                            return res_lst
        except Exception as err:
            print('Error %s when get Cell list' % err)
            return res_lst

    def get_count_device(self):
        # check key_srch co ma doc khong:
                res_lst = []

                sql_orc = "SELECT N_ACCOUNT_AFFECT, DEVICE_CODE, TROUBLE_TYPE, START_TIME from " \
                          "NOCPROV4_APP.GPON_OLT_SUB_CUR WHERE " \
                          "start_time >= sysdate-15/1440 " \
                          "and (trouble_type = 'Đứt cáp trục' " \
                          "or trouble_type = 'Đứt cáp trục/nhánh' " \
                          "or trouble_type = 'Lỗi thiết bị' or trouble_type = 'Sự cố mạng Metro') " \
                          "and is_collected_alarm = 1" \
                          "order by device_code"

                conn, type_conn = self.get_conn()
                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)

                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
                return res_lst

    def find_trouble_type_acc(self, key_srch):
        # check key_srch co ma doc khong:
                res_lst = []

                sql_orc = "SELECT TROUBLE_TYPE, START_TIME, ACCOUNT_CUSTOMER from " \
                          "NOCPROV4_APP.GPON_OLT_SUB_CUR WHERE " \
                          "start_time >= trunc(sysdate-1) AND ACCOUNT_CUSTOMER like '%" + key_srch + "%'"

                conn, type_conn = self.get_conn()
                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)

                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
                return res_lst

    def find_trouble_type_dev(self, key_srch):
        # check key_srch co ma doc khong:
                res_lst = []

                sql_orc = "SELECT TROUBLE_TYPE, SUM(N_ACCOUNT_AFFECT) as SUM_ACC from " \
                          "NOCPROV4_APP.GPON_OLT_SUB_CUR WHERE is_collected_alarm = 1 AND " \
                          "start_time >= sysdate-20/1440 AND DEVICE_CODE like '%" + key_srch + "%' " \
                          "GROUP BY TROUBLE_TYPE"

                conn, type_conn = self.get_conn()
                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)

                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
                return res_lst


if __name__ == '__main__':
    time_now = get_date_now()
    id_lst = [102564614, 102564994, 102565024]
    _obj = TblGponOltSubImpl(None)
    res_updt = _obj.find_total_offline_cable_olt("BNH0002OLT01")
    print(res_updt)

