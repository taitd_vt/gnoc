__author__ = 'VTN-DHML-DHIP10'
import cx_Oracle


class TblAfeCatDataFreeLoad:
    def __init__(self):
        pass

    def get_conn(self):
        try:
            dsn_str = cx_Oracle.makedsn('10.60.94.151', '1521', service_name='gatepro')
            conn = cx_Oracle.connect(user='chientx1', password='chientx1', dsn=dsn_str)


        except Exception as err:
            print(err)
            return None
        return conn

    def get_lst(self):
        sql_qury = "SELECT * FROM nocprov4_app.AFE_CAT_DATA_FREE_LOAD"
        res_lst = list()
        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]

            except Exception as err:
                print(err)
                return list()
            finally:
                cursor.close()
                conn.close()
        return res_lst

    def find(self, key_srch):
        res_free_load_lst = []
        conn = self.get_conn()
        cbn_bkk_obj = None

        if conn:
                cursor = conn.cursor()
                try:
                    # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                    cursor.execute("select * from  nocprov4_app.AFE_CAT_DATA_FREE_LOAD where DEVICE_NAME = :key_srch", key_srch=key_srch)

                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    # do mac dinh chi co 1 object nen return luon ra vay :D
                    if res_lst:
                        for x in res_lst:
                            res_free_load_lst.append(x)

                except Exception as err:
                    print(err)
                    return res_free_load_lst
                finally:
                    cursor.close()
                    conn.close()
                    return res_free_load_lst

    def find_like(self, key_srch):
        res_fnd_lst = []
        conn = self.get_conn()

        if conn:
                cursor = conn.cursor()
                try:
                    # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                    cursor.execute("select * from  nocprov4_app.AFE_CAT_DATA_FREE_LOAD where DEVICE_NAME like '%" + key_srch + "%' or "
                                   "SYSTEM_CODE like '%" + key_srch + "%' or FAULT_NAME like '%" + key_srch + "%' or "
                                   "DEST_NAME like '%" + key_srch + "%' or USER_UPDATE like '%" + key_srch + "%' or "
                                   "DESCRIPTION like '%" + key_srch + "%'")

                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    # do mac dinh chi co 1 object nen return luon ra vay :D
                    if res_lst:
                        for x in res_lst:

                            res_fnd_lst.append(x)

                except Exception as err:
                    print(err)
                    return res_fnd_lst
                finally:
                    cursor.close()
                    conn.close()
                    return res_fnd_lst


if __name__ == '__main__':
    tbl = TblAfeCatDataFreeLoad()
    tbl.get_conn()
    tbl.get_lst()
