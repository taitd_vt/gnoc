#!/D:\python36 new
# -*- coding: utf8 -*-
import cx_Oracle

from core.helpers.stringhelpers import check_regex_acc
from core.helpers.date_helpers import get_date_now, convert_date_str_to_date_obj_ipms
from config import Development
import os
import sys

config = Development()
SERVER_NOCPRO = config.__getattribute__('SERVER_NOCPRO')
SERVER_NOCPRO_PORT = config.__getattribute__('SERVER_NOCPRO_PORT')
SERVER_NOCPRO_SERVICE_NAME = config.__getattribute__('SERVER_NOCPRO_SERVICE_NAME')
SERVER_NOCPRO_USERNAME = config.__getattribute__('SERVER_NOCPRO_USERNAME')
SERVER_NOCPRO_PASSWORD = config.__getattribute__('SERVER_NOCPRO_PASSWORD')
SERVER_NOCPRO_USERNAME2 = config.__getattribute__('SERVER_NOCPRO_USERNAME2')
SERVER_NOCPRO_PASSWORD2 = config.__getattribute__('SERVER_NOCPRO_PASSWORD2')


class TblBssCoreImpl:
    def __init__(self, pool):
        self.pool = pool

    def get_conn(self):
        try:
            chck_pool = True
            try:
                conn = self.pool.acquire()
            except Exception as err:
                conn = None
                print(err)

            if not conn:
                print("Connecto to NOCPRO manual")
                dsn_str = cx_Oracle.makedsn(SERVER_NOCPRO, SERVER_NOCPRO_PORT, service_name=SERVER_NOCPRO_SERVICE_NAME)
                conn = cx_Oracle.connect(user=SERVER_NOCPRO_USERNAME2, password=SERVER_NOCPRO_PASSWORD2,
                                         dsn=dsn_str, encoding='UTF-8', nencoding='UTF-8')
                chck_pool = False

        except Exception as err:
            print(err)
            return None, False
        return conn, chck_pool

    def get_fault_node(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res = 0
        sql_orc = ''
        res_lst = []
        if chck_sql:
            if key_srch:
                sql_orc = "select FAULT_NAME,FAULT_LEVEL_ID,START_TIME  from   NOCPROV4_APP.CORE_ALARM_CURRENT " \
                          "where  upper(DEVICE_CODE) LIKE '%" + key_srch + "%' and ALARM_MONITOR_TYPE in (1,2,3,4,5,6)"

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    if res_lst:
                        for res in res_lst:
                            if 'FAULT_LEVEL_ID' in res:
                                falt_lvl_id = res['FAULT_LEVEL_ID']
                                falt_name = ''
                                if falt_lvl_id == 1:
                                    falt_name = 'CRITICAL'
                                elif falt_lvl_id == 2:
                                    falt_name = 'MAJOR'
                                elif falt_lvl_id == 3:
                                    falt_name = 'MINOR'
                                elif falt_lvl_id == 4:
                                    falt_name = 'WARNING'
                                else:
                                    falt_name = 'UNEXPECTED'
                                res['FAULT_LEVEL_ID'] = falt_name

                except Exception as err:
                    print(err)
                    return res
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_fault_network(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res = 0
        sql_orc = ''
        res_lst = []
        if chck_sql:
            if key_srch:
                sql_orc = "select DEVICE_CODE,FAULT_NAME,FAULT_LEVEL_ID,START_TIME from NOCPROV4_APP.CORE_ALARM_CURRENT " \
                          "where  NETWORK_ID = " + key_srch + " and ALARM_MONITOR_TYPE in (1,2,3,4,5,6) " \
                          "ALARM_STATUS = 0"

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    if res_lst:
                        for res in res_lst:
                            if 'FAULT_LEVEL_ID' in res:
                                falt_lvl_id = res['FAULT_LEVEL_ID']
                                falt_name = ''
                                if falt_lvl_id == 1:
                                    falt_name = 'CRITICAL'
                                elif falt_lvl_id == 2:
                                    falt_name = 'MAJOR'
                                elif falt_lvl_id == 3:
                                    falt_name = 'MINOR'
                                elif falt_lvl_id == 4:
                                    falt_name = 'WARNING'
                                else:
                                    falt_name = 'UNEXPECTED'
                                res['FAULT_LEVEL_ID'] = falt_name

                except Exception as err:
                    print(err)
                    return res
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_fault_network_critical(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res = 0
        sql_orc = ''
        res_lst = []
        if chck_sql:
            if key_srch:
                sql_orc = "select DEVICE_CODE,FAULT_NAME,FAULT_LEVEL_ID,START_TIME from NOCPROV4_APP.CORE_ALARM_CURRENT " \
                          "where  NETWORK_ID = " + key_srch + " and ALARM_MONITOR_TYPE in (1,2,3,4,5,6) " \
                          "ALARM_STATUS = 0 AND FAULT_LEVEL_ID = 1"

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    if res_lst:
                        for res in res_lst:
                            if 'FAULT_LEVEL_ID' in res:
                                falt_lvl_id = res['FAULT_LEVEL_ID']
                                falt_name = ''
                                if falt_lvl_id == 1:
                                    falt_name = 'CRITICAL'
                                elif falt_lvl_id == 2:
                                    falt_name = 'MAJOR'
                                elif falt_lvl_id == 3:
                                    falt_name = 'MINOR'
                                elif falt_lvl_id == 4:
                                    falt_name = 'WARNING'
                                else:
                                    falt_name = 'UNEXPECTED'
                                res['FAULT_LEVEL_ID'] = falt_name

                except Exception as err:
                    print(err)
                    return res
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst
    def get_fault_network_major(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res = 0
        sql_orc = ''
        res_lst = []
        if chck_sql:
            if key_srch:
                sql_orc = "select DEVICE_CODE,FAULT_NAME,FAULT_LEVEL_ID,START_TIME from NOCPROV4_APP.CORE_ALARM_CURRENT " \
                          "where  NETWORK_ID = " + key_srch + " and ALARM_MONITOR_TYPE in (1,2,3,4,5,6) " \
                          "ALARM_STATUS = 0 AND FAULT_LEVEL_ID = 2"

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    if res_lst:
                        for res in res_lst:
                            if 'FAULT_LEVEL_ID' in res:
                                falt_lvl_id = res['FAULT_LEVEL_ID']
                                falt_name = ''
                                if falt_lvl_id == 1:
                                    falt_name = 'CRITICAL'
                                elif falt_lvl_id == 2:
                                    falt_name = 'MAJOR'
                                elif falt_lvl_id == 3:
                                    falt_name = 'MINOR'
                                elif falt_lvl_id == 4:
                                    falt_name = 'WARNING'
                                else:
                                    falt_name = 'UNEXPECTED'
                                res['FAULT_LEVEL_ID'] = falt_name

                except Exception as err:
                    print(err)
                    return res
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst
    def get_fault_critical_node(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "select FAULT_NAME,FAULT_LEVEL_ID,START_TIME  from NOCPROV4_APP.CORE_ALARM_CURRENT " \
                          "where  upper(DEVICE_CODE) LIKE '%" + key_srch + "%' and ALARM_MONITOR_TYPE IN (1,2,3,4,5,6) " \
                                                                    "and FAULT_LEVEL_ID=1"

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    for res in res_lst:
                        if 'FAULT_LEVEL_ID' in res:
                            falt_lvl_id = res['FAULT_LEVEL_ID']
                            falt_name = ''
                            if falt_lvl_id == 1:
                                falt_name = 'CRITICAL'
                            elif falt_lvl_id == 2:
                                falt_name = 'MAJOR'
                            elif falt_lvl_id == 3:
                                falt_name = 'MINOR'
                            elif falt_lvl_id == 4:
                                falt_name = 'WARNING'
                            else:
                                falt_name = 'UNEXPECTED'
                            res['FAULT_LEVEL_ID'] = falt_name

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_fault_major_node(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "select FAULT_NAME,FAULT_LEVEL_ID,START_TIME  from NOCPROV4_APP.CORE_ALARM_CURRENT " \
                          "where upper(DEVICE_CODE) LIKE '%" + key_srch + "%' and ALARM_MONITOR_TYPE IN (1,2,3,4,5,6)" \
                                                                   " and FAULT_LEVEL_ID=2"

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    for res in res_lst:
                        if 'FAULT_LEVEL_ID' in res:
                            falt_lvl_id = res['FAULT_LEVEL_ID']
                            falt_name = ''
                            if falt_lvl_id == 1:
                                falt_name = 'CRITICAL'
                            elif falt_lvl_id == 2:
                                falt_name = 'MAJOR'
                            elif falt_lvl_id == 3:
                                falt_name = 'MINOR'
                            elif falt_lvl_id == 4:
                                falt_name = 'WARNING'
                            else:
                                falt_name = 'UNEXPECTED'
                            res['FAULT_LEVEL_ID'] = falt_name

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_count_alarm(self):
        chck_sql = True
        res = 0
        sql_orc = ''

        if chck_sql:

            sql_orc = "SELECT COUNT(ALARM_ID) FROM NOCPROV4_APP.CORE_ALARM_CURRENT WHERE ALARM_MONITOR_TYPE " \
                      "IN (1,2,3,4,5,6) " \
                      "AND (NETWORK_ID='6' OR NETWORK_ID='33' OR NETWORK_ID='13' OR NETWORK_ID='14')"

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    if res_lst:
                        for res in res_lst:
                            if 'COUNT(ALARM_ID)' in res:
                                count_alarm_id = res['COUNT(ALARM_ID)']
                                return count_alarm_id

                except Exception as err:
                    print(err)
                    return res
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res

    def get_count_alarm_critical(self):
        chck_sql = True
        res = 0
        sql_orc = ''

        if chck_sql:

            sql_orc = "SELECT COUNT(ALARM_ID) FROM NOCPROV4_APP.CORE_ALARM_CURRENT WHERE ALARM_MONITOR_TYPE " \
                      "IN (1,2,3,4,5,6) " \
                      "AND (NETWORK_ID='6' OR NETWORK_ID='33' OR NETWORK_ID='13' OR NETWORK_ID='14')" \
                      "and FAULT_LEVEL_ID=1"

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    if res_lst:
                        for res in res_lst:
                            if 'COUNT(ALARM_ID)' in res:
                                count_alarm_id = res['COUNT(ALARM_ID)']
                                return count_alarm_id

                except Exception as err:
                    print(err)
                    return res
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()

        return res

    def get_cell_info(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:

                sql_orc = "Select CELL_NAME AS MA_CELL,ATS_STATUS AS TRANG_THAI_ATS,GENERATOR_STATUS AS TRANG_THAI_PHAT_DIEN,WO_TROUBLE_CODE AS WORK_ORDER,TROUBLE_CODE AS TICKET From NOCPROV4_APP.acc_cabinet_fault_cur where upper(cell_name)=:key_srch group by cell_name,ats_status,generator_status,WO_TROUBLE_CODE,TROUBLE_CODE"

                conn, type_conn = self.get_conn()

                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc, key_srch=key_srch)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]
                        return res_lst

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
                        return res_lst
        return res_lst

    def get_cabinet_info(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:

                sql_orc = "Select CELL_NAME AS MA_CELL,ATS_STATUS AS TRANG_THAI_ATS,GENERATOR_STATUS AS TRANG_THAI_PHAT_DIEN,WO_TROUBLE_CODE AS WORK_ORDER,TROUBLE_CODE AS TICKET From NOCPROV4_APP.acc_cabinet_fault_cur where upper(cabinet_name_nims)=:key_srch group by cell_name,ats_status,generator_status,WO_TROUBLE_CODE,TROUBLE_CODE"

                conn, type_conn = self.get_conn()

                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc, key_srch=key_srch)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]
                        return res_lst

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
                        return res_lst
        return res_lst

    def get_alarm_6h(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:

                sql_orc = "select *  from   NOCPROV4_APP.CORE_ALARM_CURRENT where  upper(DEVICE_CODE) = '" + key_srch + "' " \
                                                                                                                 "and ALARM_MONITOR_TYPE IN (1,2,3,4,5,6) and START_TIME>SYSDATE-6/24 " \
                                                                                                                 "ORDER BY START_TIME DESC"

                conn, type_conn = self.get_conn()

                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]
                        return res_lst

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
                        return res_lst
        return res_lst

    def get_fault_secondary_node(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "select *  from   NOCPROV4_APP.CORE_ALARM_CURRENT where  upper(DEVICE_CODE) = '" + key_srch + "'  " \
                                                                                                                 "and ALARM_MONITOR_TYPE =0"

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    for res in res_lst:
                        if 'FAULT_LEVEL_ID' in res:
                            falt_lvl_id = res['FAULT_LEVEL_ID']
                            falt_name = ''
                            if falt_lvl_id == 1:
                                falt_name = 'CRITICAL'
                            elif falt_lvl_id == 2:
                                falt_name = 'MAJOR'
                            elif falt_lvl_id == 3:
                                falt_name = 'MINOR'
                            elif falt_lvl_id == 4:
                                falt_name = 'WARNING'
                            else:
                                falt_name = 'UNEXPECTED'
                            res['FAULT_LEVEL_ID'] = falt_name

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_kpi_psr_msc(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "select SYS_DATETIME AS Time,COLUMN1 AS PSR from MDDATA.PERF_8119_0 where " \
                          "upper(DEVICE_CODE) LIKE '%" + key_srch + "%' AND SYS_DATETIME >SYSDATE-1.5/24 AND ROWNUM <= 1 " \
                                                             "ORDER BY SYS_DATETIME DESC "

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_kpi_lusr_msc(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "select sys_datetime AS Time,LUSR from MDDATA.PERF_1502_3 where " \
                          "SYS_DATETIME >SYSDATE-1.5/24 AND upper(OBJECT_INSTANCE) = :key_srch AND ROWNUM <= 1 " \
                          "ORDER BY SYS_DATETIME DESC "

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_kpi_hlr(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = 'select SYS_DATETIME AS Time,COLUMN1 AS "%MAP_SR" from MDDATA.PERF_9021_4 ' \
                          'where upper(DEVICE_CODE) = :key_srch AND SYS_DATETIME >SYSDATE-1.5/24 AND ROWNUM <= 1 ' \
                          'ORDER BY SYS_DATETIME DESC'

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_kpi_hs(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = 'select SYS_DATETIME AS Time,COLUMN1 AS "%DIAMETER_SR" from MDDATA.PERF_9021_5 ' \
                          'where upper(DEVICE_CODE) = :key_srch AND SYS_DATETIME > SYSDATE-1.5/24 ' \
                          'AND ROWNUM <= 1 ' \
                          'ORDER BY SYS_DATETIME DESC '

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_kpi_ggsn(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc_erc = ''
        sql_orc_nka = ''

        if chck_sql:
            if key_srch:
                sql_orc_erc = 'select DATA_TIME AS Time,COUNTER_6 AS "%PDP",COUNTER_2 AS "%PDP SR",COUNTER_15 ' \
                              'AS "PGW Bearer",COUNTER_18 as "%Bear",COUNTER_13 as "%Total(GN)"  ' \
                              'from  NOCPROV4_APP.VASIN_LOAD_PS2G_CUR where  ' \
                              'upper(DEVICE_CODE) = :key_srch AND DATA_TYPE= :data_type '

                sql_orc_nka = 'select DATA_TIME AS "TIME", COUNTER_2 AS"%PDP",COUNTER_3 AS "%PDP-SR",COUNTER_7 ' \
                              'AS"%(UL+DL)"  from   NOCPROV4_APP.VASIN_LOAD_PS2G_CUR where  ' \
                              'upper(DEVICE_CODE) = :key_srch AND SERVICE_CODE= :data_type'

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc_erc, key_srch=key_srch, data_type='KPI_GGSN_ERICSSON')
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    cursor.execute(sql_orc_nka, key_srch=key_srch, data_type='KPI_VASIN_PS_GGSN_NOKIA')
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst += [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_kpi_sgsn(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc_erc = ''
        sql_orc_nka = ''

        if chck_sql:
            if key_srch:
                sql_orc_erc = 'select DATA_TIME As Time,COUNTER_2 AS "%SAU_2G",COUNTER_5 AS "%ATT_SR_2G",COUNTER_16 ' \
                              'AS "%SAU_3G",COUNTER_19 AS "%ATT_SR_3G",COUNTER_32 AS "%SAU_4G",COUNTER_35 ' \
                              'AS "%ATT_SR_4G" from NOCPROV4_APP.VASIN_LOAD_PS2G_CUR ' \
                              'where upper(DEVICE_CODE) = :key_srch and ' \
                              'device_type_name= :device_type_name AND (DATA_TYPE= :data_type_1 )'

                sql_orc_nka = 'select COUNTER_13 AS "%PDP SR", COUNTER_14 AS "%ATT SR", COUNTER_4 AS "%SAU 2G"  from ' \
                              'NOCPROV4_APP.VASIN_LOAD_PS2G_CUR where upper(DEVICE_CODE)= :key_srch  and ' \
                              'device_type_name= :data_type_1 AND DATA_TYPE= :data_type_2 '

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc_erc, key_srch=key_srch, device_type_name='SGSN',
                                   data_type_1='KPI_SGSN_ERICSSON')

                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    cursor.execute(sql_orc_nka, key_srch=key_srch, data_type_1='SGSN', data_type_2='KPI')

                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst += [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_kpi_smsc(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = 'select DATA_TIME As Time,COUNTER_5 AS"%MO Success",COUNTER_9 ' \
                          'as "%MT Success",COUNTER_13 as "%AO Success",COUNTER_17 as "%AT Success" ' \
                          'from NOCPROV4_APP.VASIN_LOAD_SMSC_CUR WHERE upper(DEVICE_CODE) ' \
                          '= :key_srch AND SERVICE_CODE= :service_code ' \
                          'AND (PARAM_1 IS NULL) ORDER BY DATA_TIME DESC'

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch, service_code='KPI_VASIN_VSMSC')
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_kpi_crbt(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = 'select DATA_TIME AS Time,COUNTER_5 AS "%CSSR",COUNTER_4 AS "%CAPS",COUNTER_3 ' \
                          'AS CAP from NOCPROV4_APP.VASIN_LOAD_CRBT_CUR WHERE ' \
                          'upper(DEVICE_CODE) = :key_srch  '

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_kpi_dsc(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = 'SELECT data_time "TIME",last "Total Transactions Success Rate" ' \
                          'FROM NOCPROV4_APP.CORE_LOAD_DSC_ERIC_CARD_CUR WHERE ' \
                          'COUNTER_NAME= :counter_name and upper(device_code)= :key_srch ' \
                          'and rownum=1 order by data_time desc '

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, counter_name='Total Transactions Success Rate',
                                   key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_cell_flap_30m(self):
        res_lst = []

        #sql_orc = "select LEVEL3_NAME province,type_station,cabinet_name,cell_name, count(*) count  from (select LEVEL3_NAME,type_station,cabinet_name,node_code,cell_name from NOCPROV4_APP.acc_cabinet_fault_cur a where a.occured_time >= TO_NUMBER(TO_CHAR((sysdate-30/24/60),'yyyyMMddHH24miss'))  and to_number(to_char(to_date(a.occured_time,'yyyyMMddHH24miss'),'HH24')) >=5 and a.alarm_monitor_type > 0 and (to_date(a.end_time,'yyyyMMddHH24miss')-to_date(a.occured_time,'yyyyMMddHH24miss'))*24*60 < 15 AND a.cabinet_name is not null  AND a.fault_group_id = 8 AND a.IS_BTS_DOWN = 0 AND NVL(a.IS_COLLECTED_ALARM, 0) <= 1 AND a.LEVEL3_CODE IS NOT NULL AND a.LEVEL3_CODE NOT IN ('TEST','TT1') AND a.CELL_NAME is not null AND a.alarm_status = 1 AND a.CR_NUMBER is null UNION ALL select LEVEL3_NAME,type_station,cabinet_name,node_code,cell_name from NOCPROV4_APP.acc_cabinet_fault_ass a where a.occured_time >= TO_NUMBER(TO_CHAR((sysdate-30/24/60),'yyyyMMddHH24miss'))  and to_number(to_char(to_date(a.occured_time,'yyyyMMddHH24miss'),'HH24')) >=5 and a.alarm_monitor_type > 0 and (to_date(a.end_time,'yyyyMMddHH24miss')-to_date(a.occured_time,'yyyyMMddHH24miss'))*24*60 < 15 AND a.cabinet_name is not null  AND a.fault_group_id = 8 AND a.IS_BTS_DOWN = 0 AND NVL(a.IS_COLLECTED_ALARM, 0) <= 1 AND a.LEVEL3_CODE IS NOT NULL AND a.LEVEL3_CODE NOT IN ('TEST','TT1') AND CELL_NAME is not null AND a.CR_NUMBER is null ) GROUP BY LEVEL3_NAME,type_station,cabinet_name,node_code,cell_name having count(*)>2 ORDER BY PROVINCE"
        sql_orc = "select LEVEL3_NAME province,type_station,cabinet_name,STATION_CODE_NIMS,node_code,cell_name, count(*) count  from (select LEVEL3_NAME,type_station,cabinet_name,STATION_CODE_NIMS,node_code,cell_name from NOCPROV4_APP.acc_cabinet_fault_cur a where a.occured_time >= TO_NUMBER(TO_CHAR((sysdate-30/24/60),'yyyyMMddHH24miss'))  and to_number(to_char(to_date(a.occured_time,'yyyyMMddHH24miss'),'HH24')) >=5 and a.alarm_monitor_type > 0 and (to_date(a.end_time,'yyyyMMddHH24miss')-to_date(a.occured_time,'yyyyMMddHH24miss'))*24*60 < 15 AND a.cabinet_name is not null  AND a.fault_group_id = 8 AND a.IS_BTS_DOWN = 0 AND NVL(a.IS_COLLECTED_ALARM, 0) <= 1 AND a.LEVEL3_CODE IS NOT NULL AND a.LEVEL3_CODE NOT IN ('TEST','TT1') AND a.CELL_NAME is not null AND a.alarm_status = 1 AND a.CR_NUMBER is null UNION ALL select LEVEL3_NAME,type_station,cabinet_name,STATION_CODE_NIMS,node_code,cell_name from NOCPROV4_APP.acc_cabinet_fault_ass a where a.occured_time >= TO_NUMBER(TO_CHAR((sysdate-30/24/60),'yyyyMMddHH24miss'))  and to_number(to_char(to_date(a.occured_time,'yyyyMMddHH24miss'),'HH24')) >=5 and a.alarm_monitor_type > 0 and (to_date(a.end_time,'yyyyMMddHH24miss')-to_date(a.occured_time,'yyyyMMddHH24miss'))*24*60 < 15 AND a.cabinet_name is not null  AND a.fault_group_id = 8 AND a.IS_BTS_DOWN = 0 AND NVL(a.IS_COLLECTED_ALARM, 0) <= 1 AND a.LEVEL3_CODE IS NOT NULL AND a.LEVEL3_CODE NOT IN ('TEST','TT1') AND CELL_NAME is not null AND a.CR_NUMBER is null) GROUP BY LEVEL3_NAME,type_station,cabinet_name,STATION_CODE_NIMS,node_code,cell_name having count(*)>2 ORDER by province"
        conn, type_conn = self.get_conn()

        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_orc)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                # province, type_station, cabinet_name, node_code, cell_name, count
            except Exception as err:
                print(err)
                return res_lst
            finally:
                cursor.close()
                if type_conn:
                    self.pool.release(conn)
                else:
                    conn.close()

        return res_lst

    def get_cabinet_flap_30m(self):
        res_lst = []

        #sql_orc = "select LEVEL3_NAME province,type_station,cabinet_name,count(*) count from ( select LEVEL3_NAME,type_station,cabinet_name,node_code from NOCPROV4_APP.acc_cabinet_fault_cur a where a.occured_time >= TO_NUMBER(TO_CHAR((sysdate-30/24/60),'yyyyMMddHH24miss'))  and to_number(to_char(to_date(a.occured_time,'yyyyMMddHH24miss'),'HH24')) >=5 and a.alarm_monitor_type > 0 and (to_date(a.end_time,'yyyyMMddHH24miss')-to_date(a.occured_time,'yyyyMMddHH24miss'))*24*60 < 15 AND a.fault_group_id = 2 AND NVL(a.IS_COLLECTED_ALARM, 0) <= 1 AND a.LEVEL3_CODE IS NOT NULL AND a.LEVEL3_CODE NOT IN ('TEST','TT1') AND a.cabinet_name is not null AND a.alarm_status = 1 AND a.CR_NUMBER is null UNION ALL select LEVEL3_NAME,type_station,cabinet_name,node_code  from NOCPROV4_APP.acc_cabinet_fault_ass a where a.occured_time >= TO_NUMBER(TO_CHAR((sysdate-30/24/60),'yyyyMMddHH24miss'))  and to_number(to_char(to_date(a.occured_time,'yyyyMMddHH24miss'),'HH24')) >=5 and a.alarm_monitor_type > 0 and (to_date(a.end_time,'yyyyMMddHH24miss')-to_date(a.occured_time,'yyyyMMddHH24miss'))*24*60 < 15 AND a.fault_group_id = 2 AND NVL(a.IS_COLLECTED_ALARM, 0) <= 1 AND a.LEVEL3_CODE IS NOT NULL AND a.LEVEL3_CODE NOT IN ('TEST','TT1') AND a.cabinet_name is not null AND a.CR_NUMBER is null) GROUP BY LEVEL3_NAME,type_station,cabinet_name,node_code having count(*)>2 ORDER BY PROVINCE"
        sql_orc = "select LEVEL3_NAME province,type_station,cabinet_name,STATION_CODE_NIMS,node_code,count(*) count  from ( select LEVEL3_NAME,type_station,cabinet_name,STATION_CODE_NIMS,node_code from NOCPROV4_APP.acc_cabinet_fault_cur a where a.occured_time >= TO_NUMBER(TO_CHAR((sysdate-30/24/60),'yyyyMMddHH24miss'))  and to_number(to_char(to_date(a.occured_time,'yyyyMMddHH24miss'),'HH24')) >=5 and a.alarm_monitor_type > 0 and (to_date(a.end_time,'yyyyMMddHH24miss')-to_date(a.occured_time,'yyyyMMddHH24miss'))*24*60 < 15 AND a.fault_group_id = 2 AND NVL(a.IS_COLLECTED_ALARM, 0) <= 1 AND a.LEVEL3_CODE IS NOT NULL AND a.LEVEL3_CODE NOT IN ('TEST','TT1') AND a.cabinet_name is not null AND a.alarm_status = 1 AND a.CR_NUMBER is null UNION ALL select LEVEL3_NAME,type_station,cabinet_name,STATION_CODE_NIMS,node_code  from NOCPROV4_APP.acc_cabinet_fault_ass a where a.occured_time >= TO_NUMBER(TO_CHAR((sysdate-30/24/60),'yyyyMMddHH24miss'))  and to_number(to_char(to_date(a.occured_time,'yyyyMMddHH24miss'),'HH24')) >=5 and a.alarm_monitor_type > 0 and (to_date(a.end_time,'yyyyMMddHH24miss')-to_date(a.occured_time,'yyyyMMddHH24miss'))*24*60 < 15 AND a.fault_group_id = 2 AND NVL(a.IS_COLLECTED_ALARM, 0) <= 1 AND a.LEVEL3_CODE IS NOT NULL AND a.LEVEL3_CODE NOT IN ('TEST','TT1') AND a.cabinet_name is not null AND a.CR_NUMBER is null ) GROUP BY LEVEL3_NAME,type_station,cabinet_name,STATION_CODE_NIMS,node_code having count(*)>2 ORDER by province"
        conn, type_conn = self.get_conn()

        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_orc)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                # province, type_station, cabinet_name, node_code, count
            except Exception as err:
                print(err)
                return res_lst
            finally:
                cursor.close()
                if type_conn:
                    self.pool.release(conn)
                else:
                    conn.close()

        return res_lst

    def get_station_alm(self, key_name):
        res_lst = []

        sql_orc = "Select CELL_NAME AS MA_CELL,TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss') as THOI_GIAN,FAULT_NAME AS CANH_BAO,cabinet_name_nims From NOCPROV4_APP.acc_cabinet_fault_cur  where upper(STATION_CODE_NIMS)= :key_srch and (WO_TROUBLE_CODE is null or substr(WO_TROUBLE_CODE,1,5) not in ('WO_MR')) group by cell_name,FAULT_NAME,TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss'),cabinet_name_nims"
        conn, type_conn = self.get_conn()

        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_orc, key_srch=str(key_name))
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                # province, type_station, cabinet_name, node_code, count
            except Exception as err:
                print(err)
                return res_lst
            finally:
                cursor.close()
                if type_conn:
                    self.pool.release(conn)
                else:
                    conn.close()

        return res_lst

    def get_cabinet_province_flap_30m(self):
        res_lst = []

        sql_orc = "Select province, count(*) TU_NHAY from ( select LEVEL3_NAME province,type_station,cabinet_name,node_code,count(*) count  from ( select LEVEL3_NAME,type_station,cabinet_name,node_code from NOCPROV4_APP.acc_cabinet_fault_cur a where a.occured_time >= TO_NUMBER(TO_CHAR((sysdate-30/24/60),'yyyyMMddHH24miss'))  and to_number(to_char(to_date(a.occured_time,'yyyyMMddHH24miss'),'HH24')) >=5 and a.alarm_monitor_type > 0 and (to_date(a.end_time,'yyyyMMddHH24miss')-to_date(a.occured_time,'yyyyMMddHH24miss'))*24*60 < 15 AND a.fault_group_id = 2 AND NVL(a.IS_COLLECTED_ALARM, 0) <= 1 AND a.LEVEL3_CODE IS NOT NULL AND a.LEVEL3_CODE NOT IN ('TEST','TT1') AND a.cabinet_name is not null AND a.alarm_status = 1 AND a.CR_NUMBER is null UNION ALL select LEVEL3_NAME,type_station,cabinet_name,node_code  from NOCPROV4_APP.acc_cabinet_fault_ass a where a.occured_time >= TO_NUMBER(TO_CHAR((sysdate-30/24/60),'yyyyMMddHH24miss'))  and to_number(to_char(to_date(a.occured_time,'yyyyMMddHH24miss'),'HH24')) >=5 and a.alarm_monitor_type > 0 and (to_date(a.end_time,'yyyyMMddHH24miss')-to_date(a.occured_time,'yyyyMMddHH24miss'))*24*60 < 15 AND a.fault_group_id = 2 AND NVL(a.IS_COLLECTED_ALARM, 0) <= 1 AND a.LEVEL3_CODE IS NOT NULL AND a.LEVEL3_CODE NOT IN ('TEST','TT1') AND a.cabinet_name is not null AND a.CR_NUMBER is null ) GROUP BY LEVEL3_NAME,type_station,cabinet_name,node_code having count(*)>2 ORDER BY count(*) DESC ) group by province"

        conn, type_conn = self.get_conn()

        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_orc)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                # province, type_station, cabinet_name, node_code, count
            except Exception as err:
                print(err)
                return res_lst
            finally:
                cursor.close()
                if type_conn:
                    self.pool.release(conn)
                else:
                    conn.close()

        return res_lst

    def get_cell_province_flap_30m(self):
        res_lst = []

        sql_orc = "select province, count(*) CELL_NHAY from ( select LEVEL3_NAME province,type_station,cabinet_name,node_code,cell_name, count(*) count  from (select LEVEL3_NAME,type_station,cabinet_name,node_code,cell_name from NOCPROV4_APP.acc_cabinet_fault_cur a where a.occured_time >= TO_NUMBER(TO_CHAR((sysdate-30/24/60),'yyyyMMddHH24miss'))  and to_number(to_char(to_date(a.occured_time,'yyyyMMddHH24miss'),'HH24')) >=5 and a.alarm_monitor_type > 0 and (to_date(a.end_time,'yyyyMMddHH24miss')-to_date(a.occured_time,'yyyyMMddHH24miss'))*24*60 < 15 AND a.cabinet_name is not null  AND a.fault_group_id = 8 AND a.IS_BTS_DOWN = 0 AND NVL(a.IS_COLLECTED_ALARM, 0) <= 1 AND a.LEVEL3_CODE IS NOT NULL AND a.LEVEL3_CODE NOT IN ('TEST','TT1') AND a.CELL_NAME is not null AND a.alarm_status = 1 AND a.CR_NUMBER is null UNION ALL select LEVEL3_NAME,type_station,cabinet_name,node_code,cell_name from NOCPROV4_APP.acc_cabinet_fault_ass a where a.occured_time >= TO_NUMBER(TO_CHAR((sysdate-30/24/60),'yyyyMMddHH24miss'))  and to_number(to_char(to_date(a.occured_time,'yyyyMMddHH24miss'),'HH24')) >=5 and a.alarm_monitor_type > 0 and (to_date(a.end_time,'yyyyMMddHH24miss')-to_date(a.occured_time,'yyyyMMddHH24miss'))*24*60 < 15 AND a.cabinet_name is not null  AND a.fault_group_id = 8 AND a.IS_BTS_DOWN = 0 AND NVL(a.IS_COLLECTED_ALARM, 0) <= 1 AND a.LEVEL3_CODE IS NOT NULL AND a.LEVEL3_CODE NOT IN ('TEST','TT1') AND CELL_NAME is not null AND a.CR_NUMBER is null ) GROUP BY LEVEL3_NAME,type_station,cabinet_name,node_code,cell_name having count(*)>2 ORDER BY count(*) DESC ) group by province"

        conn, type_conn = self.get_conn()

        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_orc)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                # province, type_station, cabinet_name, node_code, count
            except Exception as err:
                print(err)
                return res_lst
            finally:
                cursor.close()
                if type_conn:
                    self.pool.release(conn)
                else:
                    conn.close()

        return res_lst

    def check_cell_cabinet_alarm(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = 'Select FAULT_NAME, UPDATE_TIME As Time_Start ' \
                          'From NOCPROV4_APP.acc_cabinet_fault_cur where ' \
                          'upper(CABINET_NAME) in (:key_srch) or upper(CELL_NAME) in (:key_srch) '

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def check_cell_alarm_frst(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "Select CELL_NAME AS MA_CELL,TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss') as THOI_GIAN,FAULT_NAME AS CANH_BAO From NOCPROV4_APP.acc_cabinet_fault_cur where upper(cell_name)=:key_srch group by cell_name,FAULT_NAME,TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss')"

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def check_cell_alarm_frst(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "Select CELL_NAME AS MA_CELL,TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss') as THOI_GIAN,FAULT_NAME AS CANH_BAO From NOCPROV4_APP.acc_cabinet_fault_cur where upper(cell_name)=:key_srch group by cell_name,FAULT_NAME,TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss')"

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def check_cabinet_alarm_frst(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                #sql_orc = "Select CELL_NAME AS MA_CELL,TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss') as THOI_GIAN,FAULT_NAME AS CANH_BAO From NOCPROV4_APP.acc_cabinet_fault_cur  where cabinet_name_nims=:key_srch group by cell_name,FAULT_NAME,TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss')"
                sql_orc = "Select CELL_NAME AS MA_CELL,TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss') as THOI_GIAN,FAULT_NAME AS CANH_BAO From NOCPROV4_APP.acc_cabinet_fault_cur where upper(cabinet_name_nims)=:key_srch and (WO_TROUBLE_CODE is null or  substr(WO_TROUBLE_CODE,1,5) not in ('WO_MR')) group by cell_name,FAULT_NAME,TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss')"
            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_station_from_cell_2nd(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "SELECT NIMS_STATION_CODE AS MA_TRAM FROM NOCPROV4_APP.ACC_CATA_CELL WHERE upper(CELL_CODE)=:key_srch GROUP BY NIMS_STATION_CODE"

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_station_from_cabinet_2nd(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "SELECT NIMS_STATION_CODE AS MA_TRAM FROM NOCPROV4_APP.ACC_CATA_CELL WHERE upper(nims_cabinet_code)=:key_srch  group by nims_station_code"

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_cabinet_from_cell_3rd(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "SELECT NIMS_CABINET_CODE AS DANH_SACH_TU FROM NOCPROV4_APP.ACC_CATA_CELL WHERE NIMS_STATION_CODE=(SELECT NIMS_STATION_CODE FROM NOCPROV4_APP.ACC_CATA_CELL WHERE upper(CELL_CODE)=:key_srch GROUP BY NIMS_STATION_CODE) GROUP BY NIMS_CABINET_CODE"

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_cabinets_from_station_3rd(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "SELECT NIMS_CABINET_CODE AS DANH_SACH_TU FROM NOCPROV4_APP.ACC_CATA_CELL  WHERE NIMS_STATION_CODE=(SELECT NIMS_STATION_CODE AS MA_TRAM FROM NOCPROV4_APP.ACC_CATA_CELL WHERE upper(nims_cabinet_code)=:key_srch group by nims_station_code)  GROUP BY NIMS_CABINET_CODE"

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_cabinet_list_from_cell(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "SELECT NIMS_CABINET_CODE AS TỦ,CELL_CODE AS DANH_SACH_CELL FROM NOCPROV4_APP.ACC_CATA_CELL  WHERE NIMS_STATION_CODE=(SELECT NIMS_STATION_CODE FROM NOCPROV4_APP.ACC_CATA_CELL WHERE upper(CELL_CODE)=:key_srch GROUP BY NIMS_STATION_CODE) "

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_cell_list_from_cabinet_4th(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "SELECT NIMS_CABINET_CODE AS TỦ,CELL_CODE AS DANH_SACH_CELL FROM NOCPROV4_APP.ACC_CATA_CELL  WHERE NIMS_STATION_CODE=(SELECT NIMS_STATION_CODE FROM NOCPROV4_APP.ACC_CATA_CELL WHERE upper(CELL_CODE)=:key_srch GROUP BY NIMS_STATION_CODE) "

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_cell_list_from_cabinet_for_cabinet_4th(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "SELECT NIMS_CABINET_CODE AS TỦ,CELL_CODE AS DANH_SACH_CELL FROM NOCPROV4_APP.ACC_CATA_CELL  WHERE NIMS_STATION_CODE=(SELECT NIMS_STATION_CODE FROM NOCPROV4_APP.ACC_CATA_CELL WHERE upper(nims_cabinet_code)=:key_srch GROUP BY NIMS_STATION_CODE) "

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_important_alarm_10m(self):
        res_lst = []

        sql_orc = "SELECT DEVICE_CODE as NODE_MANG,COUNTRY_CODE AS THI_TRUONG,START_TIME as THOI_GIAN, FAULT_LEVEL_ID as MUC_DO,FAULT_NAME as CANH_BAO, REASON as NGUYEN_NHAN, INFLUENCE as ANH_HUONG,ADDITION_INFO as THONG_TIN_THEM,count(*) SO_LUONG FROM NOCPROV4_APP.CORE_ALARM_CURRENT WHERE  ALARM_MONITOR_TYPE IN (1,2,3,4,5,6) AND NETWORK_ID IN (6,33,13,14) AND START_TIME >SYSDATE-1/4/24 AND START_TIME <SYSDATE-1/6/24 and FAULT_NAME in ('SUA SS7 DESTINATION INACCESSIBLE','SCCP DISTURBANCE','M3UA route is unavailable','DPC is restricted','REPT-LKSTO: link set prohibited','M3UA DESTINATION INACCESSIBLE','M3UA destination entity is inaccessible','MESSAGE TRANSFER PART 3 LINK AND ROUTE UNAVAILABLE','LOW TRAFFIC CAPACITY ON CIRCUIT GROUP','UNIT RESTARTED','ROUTE SET UNAVAILABLE','LINK SET UNAVAILABLE','M3UA link set failure','INCORRECT WORKING STATE','POWER SUPPLY FAILURE','M3UA Linkset Fault','VMGW OUT OF SERVICE','WO-EX UNIT FAULTY','Route is restricted','UNIT TYPE HAS NO REDUNDANCY','MTP3 link set failure','KPI PSR giảm đột biến dưới 90%','SIGNALLING LINK CONGESTION LEVEL EXCEEDED','SIZE ALTERATION OF DATA FILES SIZE CHANGE REQUIRED','CP CLUSTER SIZE ALTERATION OF DATA FILES SIZE CHANGE REQUIRED','M3UA link congestion') group by DEVICE_CODE,COUNTRY_CODE,FAULT_LEVEL_ID,FAULT_NAME,REASON,INFLUENCE,ADDITION_INFO,START_TIME union all (SELECT DEVICE_CODE as NODE_MANG,COUNTRY_CODE AS THI_TRUONG,START_TIME as THOI_GIAN, FAULT_LEVEL_ID as MUC_DO,FAULT_NAME as CANH_BAO, REASON as NGUYEN_NHAN, INFLUENCE as ANH_HUONG,ADDITION_INFO as THONG_TIN_THEM,count(*) SO_LUONG FROM NOCPROV4_APP.CORE_ALARM_CURRENT WHERE ALARM_MONITOR_TYPE IN (1,2,3,4,5,6) AND NETWORK_ID IN (6,33,13,14) AND START_TIME >SYSDATE-1/4/24 AND START_TIME <SYSDATE-1/6/24 and FAULT_NAME in ('Abnormal Service Counter','ASP down detected','Billing center does not take CDR files in time','Board Dynamic Source overload','Board Fault','Board offline','Board Port Fault','Boot Failure','Card Down','Card Missing','CPCI Board fault','CPGW ALARM DISCONNECT TO PGW','DB_DISCONNECTED','Diameter Link Down','Diameter Peer Down','Diameter Signaling Controller, Overload','Diameter Signaling Controller, Peer Instance unavailable','Diameter Signaling Controller, SCTP Association Congestion','DisconnectionSGWAndMME','DisconnectionSGWAndPGW','DNS Link Down','EM_DOWN','ethLinkDown','Fabric Data Error','Fabric Redundancy Lost','HDD_SERVER_OVER_THRESHOLD','Heartbeat Failure','INCORRECT WORKING STATE','LAG Constituent State Down','LAG State Down','Linecard Bandwidth Degrade','Link down','M3UA office inaccessible','M3UA Route Unavailable','M3UA Signaling Link Congestion','M3UA_ASP_DOWN','MANAGED OBJECT FAILED','MEDIA_DISCONNECTED','MYSQL_DOWN','NE Is Disconnected','No response from the OCS','No response from the PCRF','nocSupervisedMeasCri','NODE_DISCONNECT','NODE_HAS_GONE_DOWN','NODE_PGWCP_STATUS','NODE_SGWCP_STATUS','NODE_SPGWDP_STATUS','OCS_DISCONNECT','One DNS Server No Response','OSPF Neighbor Down','PERF_SMSC_HDD DISK_USAGE vuot nguong','PGW_CREATE_BEARER_SR_5M','Physical entity is reset','ranRncUnreachable','redEthMultiFaultNet1','Reduced Session Resilience','SGs Route Set fail','ss7M3uaRemoteSpUnavlForUP','ss7SccpRemoteSPCUnavail','The high usage of DB table') group by DEVICE_CODE,COUNTRY_CODE,FAULT_LEVEL_ID,FAULT_NAME,REASON,INFLUENCE,ADDITION_INFO,START_TIME) UNION ALL(SELECT DEVICE_CODE as NODE_MANG,COUNTRY_CODE AS THI_TRUONG , START_TIME as THOI_GIAN, FAULT_LEVEL_ID as MUC_DO,FAULT_NAME as CANH_BAO, REASON as NGUYEN_NHAN, INFLUENCE as ANH_HUONG,ADDITION_INFO as THONG_TIN_THEM,count(*) SO_LUONG FROM NOCPROV4_APP.CORE_ALARM_CURRENT WHERE ALARM_MONITOR_TYPE IN (1,2,3,4,5,6) AND NETWORK_ID IN (6,33,13,14) AND FAULT_LEVEL_ID=1 AND START_TIME >SYSDATE-1/4/24 AND START_TIME <SYSDATE-1/6/24 and FAULT_NAME in ('KPI_APPLICATION_AVGRESPONSETIME_ADDTONEBOX_OVER_THRESHOLD_GE','KPI_APPLICATION_AVGRESPONSETIME_PRESENTTONE_OVER_THRESHOLD_GE','KPI_APPLICATION_AVGRESPONSETIME_REGISTER_OVER_THRESHOLD_GE','KPI_APPLICATION_MAXRESPONSETIME_ADDTONEBOX_OVER_THRESHOLD_GE','KPI_APPLICATION-RECURING_MAXRESPONSETIME_RBTEXTENSION_OVER_THRESHOLD_GE','KPI_APPLICATION-RECURING_TOTAL-CHARGE_CHARGE_OVER_THRESHOLD_GE','KPI_COBRA_TPS_AVRRESTIMEMICRO_OVER_THRESHOLD_GE','KPI_COBRA_TPS_MAXRESTIMEMICRO_OVER_THRESHOLD_GE','KPI_COBRA_TPS_MAXRESTIMEMICRO_OVER_THRESHOLD_GE (G)','KPI_COBRA_TPS_MAXRESTIMEMICRO_OVER_THRESHOLD_GE (IAE)','KPI_COBRA_TPS_MINRESTIMEMICRO_OVER_THRESHOLD_GE','KPI_GGSN_ERICSSON PDP_SR giam so voi chu ky truoc','KPI_GGSN_ERICSSON TOTAL_GN giam so voi chu ky truoc','KPI_GGSN_ERICSSON TOTAL_GN_PERCENT vuot nguong','KPI_GGSN_NOKIA PDP_SR giam so voi chu ky truoc','KPI_SGSN_ERICSSON ATT_SR_2G(%) giam so voi chu ky truoc','KPI_SGSN_ERICSSON ATT_SR_3G(%) suy giam','KPI_SGSN_ERICSSON PDP_SR_2G(%) suy giam','KPI_VSMSC_NODE AO_SUCCESS_RATIO suy giam','KPI_VSMSC_NODE AT_SUCCESS_RATIO suy giam','KPI_VSMSC_NODE MO_SUCCESS_RATIO suy giam','KPI_VSMSC_NODE MT_SUCCESS_RATIO suy giam','PACKET_LOSS_SERVER_OVER_THRESHOLD','PERF_GGSN_ERICSSON_HDD HDD vuot nguong','PERF_GGSN_NOKIA_CPU_MEM CPU_PERCENT vuot nguong','PERF_SGSN_ERICSSON_CPU CPU vuot nguong') group by DEVICE_CODE,COUNTRY_CODE,FAULT_LEVEL_ID,FAULT_NAME,REASON,INFLUENCE,ADDITION_INFO,START_TIME)"

        conn, type_conn = self.get_conn()

        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_orc)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                # province, type_station, cabinet_name, node_code, cell_name, count
            except Exception as err:
                print(err)
                return res_lst
            finally:
                cursor.close()
                if type_conn:
                    self.pool.release(conn)
                else:
                    conn.close()

        return res_lst

    def get_important_alarm_left_last_30m(self):
        res_lst = []

        sql_orc = "SELECT DEVICE_CODE as NODE_MANG,COUNTRY_CODE AS THI_TRUONG,START_TIME as THOI_GIAN, FAULT_LEVEL_ID as MUC_DO,FAULT_NAME as CANH_BAO, REASON as NGUYEN_NHAN, INFLUENCE as ANH_HUONG,ADDITION_INFO as THONG_TIN_THEM,count(*) SO_LUONG FROM NOCPROV4_APP.CORE_ALARM_CURRENT WHERE  ALARM_MONITOR_TYPE IN (1,2,3,4,5,6) AND NETWORK_ID IN (6,33,13,14) AND START_TIME >SYSDATE-1/2/24 AND START_TIME <SYSDATE-1/6/24 and FAULT_NAME in ('SUA SS7 DESTINATION INACCESSIBLE','SCCP DISTURBANCE','M3UA route is unavailable','DPC is restricted','REPT-LKSTO: link set prohibited','M3UA DESTINATION INACCESSIBLE','M3UA destination entity is inaccessible','MESSAGE TRANSFER PART 3 LINK AND ROUTE UNAVAILABLE','LOW TRAFFIC CAPACITY ON CIRCUIT GROUP','UNIT RESTARTED','ROUTE SET UNAVAILABLE','LINK SET UNAVAILABLE','M3UA link set failure','INCORRECT WORKING STATE','POWER SUPPLY FAILURE','M3UA Linkset Fault','VMGW OUT OF SERVICE','WO-EX UNIT FAULTY','Route is restricted','UNIT TYPE HAS NO REDUNDANCY','MTP3 link set failure','KPI PSR giảm đột biến dưới 90%','SIGNALLING LINK CONGESTION LEVEL EXCEEDED','SIZE ALTERATION OF DATA FILES SIZE CHANGE REQUIRED','CP CLUSTER SIZE ALTERATION OF DATA FILES SIZE CHANGE REQUIRED','M3UA link congestion') group by DEVICE_CODE,COUNTRY_CODE,FAULT_LEVEL_ID,FAULT_NAME,REASON,INFLUENCE,ADDITION_INFO,START_TIME union all (SELECT DEVICE_CODE as NODE_MANG,COUNTRY_CODE AS THI_TRUONG,START_TIME as THOI_GIAN, FAULT_LEVEL_ID as MUC_DO,FAULT_NAME as CANH_BAO, REASON as NGUYEN_NHAN, INFLUENCE as ANH_HUONG,ADDITION_INFO as THONG_TIN_THEM,count(*) SO_LUONG FROM NOCPROV4_APP.CORE_ALARM_CURRENT WHERE ALARM_MONITOR_TYPE IN (1,2,3,4,5,6) AND NETWORK_ID IN (6,33,13,14) AND START_TIME >SYSDATE-1/4/24 AND START_TIME <SYSDATE-1/6/24 and FAULT_NAME in ('Abnormal Service Counter','ASP down detected','Billing center does not take CDR files in time','Board Dynamic Source overload','Board Fault','Board offline','Board Port Fault','Boot Failure','Card Down','Card Missing','CPCI Board fault','CPGW ALARM DISCONNECT TO PGW','DB_DISCONNECTED','Diameter Link Down','Diameter Peer Down','Diameter Signaling Controller, Overload','Diameter Signaling Controller, Peer Instance unavailable','Diameter Signaling Controller, SCTP Association Congestion','DisconnectionSGWAndMME','DisconnectionSGWAndPGW','DNS Link Down','EM_DOWN','ethLinkDown','Fabric Data Error','Fabric Redundancy Lost','HDD_SERVER_OVER_THRESHOLD','Heartbeat Failure','INCORRECT WORKING STATE','LAG Constituent State Down','LAG State Down','Linecard Bandwidth Degrade','Link down','M3UA office inaccessible','M3UA Route Unavailable','M3UA Signaling Link Congestion','M3UA_ASP_DOWN','MANAGED OBJECT FAILED','MEDIA_DISCONNECTED','MYSQL_DOWN','NE Is Disconnected','No response from the OCS','No response from the PCRF','nocSupervisedMeasCri','NODE_DISCONNECT','NODE_HAS_GONE_DOWN','NODE_PGWCP_STATUS','NODE_SGWCP_STATUS','NODE_SPGWDP_STATUS','OCS_DISCONNECT','One DNS Server No Response','OSPF Neighbor Down','PERF_SMSC_HDD DISK_USAGE vuot nguong','PGW_CREATE_BEARER_SR_5M','Physical entity is reset','ranRncUnreachable','redEthMultiFaultNet1','Reduced Session Resilience','SGs Route Set fail','ss7M3uaRemoteSpUnavlForUP','ss7SccpRemoteSPCUnavail','The high usage of DB table') group by DEVICE_CODE,COUNTRY_CODE,FAULT_LEVEL_ID,FAULT_NAME,REASON,INFLUENCE,ADDITION_INFO,START_TIME) UNION ALL(SELECT DEVICE_CODE as NODE_MANG,COUNTRY_CODE AS THI_TRUONG , START_TIME as THOI_GIAN, FAULT_LEVEL_ID as MUC_DO,FAULT_NAME as CANH_BAO, REASON as NGUYEN_NHAN, INFLUENCE as ANH_HUONG,ADDITION_INFO as THONG_TIN_THEM,count(*) SO_LUONG FROM NOCPROV4_APP.CORE_ALARM_CURRENT WHERE ALARM_MONITOR_TYPE IN (1,2,3,4,5,6) AND NETWORK_ID IN (6,33,13,14) AND FAULT_LEVEL_ID=1 AND START_TIME >SYSDATE-1/4/24 AND START_TIME <SYSDATE-1/6/24 and FAULT_NAME in ('KPI_APPLICATION_AVGRESPONSETIME_ADDTONEBOX_OVER_THRESHOLD_GE','KPI_APPLICATION_AVGRESPONSETIME_PRESENTTONE_OVER_THRESHOLD_GE','KPI_APPLICATION_AVGRESPONSETIME_REGISTER_OVER_THRESHOLD_GE','KPI_APPLICATION_MAXRESPONSETIME_ADDTONEBOX_OVER_THRESHOLD_GE','KPI_APPLICATION-RECURING_MAXRESPONSETIME_RBTEXTENSION_OVER_THRESHOLD_GE','KPI_APPLICATION-RECURING_TOTAL-CHARGE_CHARGE_OVER_THRESHOLD_GE','KPI_COBRA_TPS_AVRRESTIMEMICRO_OVER_THRESHOLD_GE','KPI_COBRA_TPS_MAXRESTIMEMICRO_OVER_THRESHOLD_GE','KPI_COBRA_TPS_MAXRESTIMEMICRO_OVER_THRESHOLD_GE (G)','KPI_COBRA_TPS_MAXRESTIMEMICRO_OVER_THRESHOLD_GE (IAE)','KPI_COBRA_TPS_MINRESTIMEMICRO_OVER_THRESHOLD_GE','KPI_GGSN_ERICSSON PDP_SR giam so voi chu ky truoc','KPI_GGSN_ERICSSON TOTAL_GN giam so voi chu ky truoc','KPI_GGSN_ERICSSON TOTAL_GN_PERCENT vuot nguong','KPI_GGSN_NOKIA PDP_SR giam so voi chu ky truoc','KPI_SGSN_ERICSSON ATT_SR_2G(%) giam so voi chu ky truoc','KPI_SGSN_ERICSSON ATT_SR_3G(%) suy giam','KPI_SGSN_ERICSSON PDP_SR_2G(%) suy giam','KPI_VSMSC_NODE AO_SUCCESS_RATIO suy giam','KPI_VSMSC_NODE AT_SUCCESS_RATIO suy giam','KPI_VSMSC_NODE MO_SUCCESS_RATIO suy giam','KPI_VSMSC_NODE MT_SUCCESS_RATIO suy giam','PACKET_LOSS_SERVER_OVER_THRESHOLD','PERF_GGSN_ERICSSON_HDD HDD vuot nguong','PERF_GGSN_NOKIA_CPU_MEM CPU_PERCENT vuot nguong','PERF_SGSN_ERICSSON_CPU CPU vuot nguong') group by DEVICE_CODE,COUNTRY_CODE,FAULT_LEVEL_ID,FAULT_NAME,REASON,INFLUENCE,ADDITION_INFO,START_TIME)"

        conn, type_conn = self.get_conn()

        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_orc)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                # province, type_station, cabinet_name, node_code, cell_name, count
            except Exception as err:
                print(err)
                return res_lst
            finally:
                cursor.close()
                if type_conn:
                    self.pool.release(conn)
                else:
                    conn.close()

        return res_lst

    def get_important_alarm_left_last_1h(self):
        res_lst = []

        sql_orc = "SELECT DEVICE_CODE as NODE_MANG,COUNTRY_CODE AS THI_TRUONG,START_TIME as THOI_GIAN, FAULT_LEVEL_ID as MUC_DO,FAULT_NAME as CANH_BAO, REASON as NGUYEN_NHAN, INFLUENCE as ANH_HUONG,ADDITION_INFO as THONG_TIN_THEM,count(*) SO_LUONG FROM NOCPROV4_APP.CORE_ALARM_CURRENT WHERE  ALARM_MONITOR_TYPE IN (1,2,3,4,5,6) AND NETWORK_ID IN (6,33,13,14) AND START_TIME >SYSDATE-1/24 AND START_TIME <SYSDATE-1/6/24 and FAULT_NAME in ('SUA SS7 DESTINATION INACCESSIBLE','SCCP DISTURBANCE','M3UA route is unavailable','DPC is restricted','REPT-LKSTO: link set prohibited','M3UA DESTINATION INACCESSIBLE','M3UA destination entity is inaccessible','MESSAGE TRANSFER PART 3 LINK AND ROUTE UNAVAILABLE','LOW TRAFFIC CAPACITY ON CIRCUIT GROUP','UNIT RESTARTED','ROUTE SET UNAVAILABLE','LINK SET UNAVAILABLE','M3UA link set failure','INCORRECT WORKING STATE','POWER SUPPLY FAILURE','M3UA Linkset Fault','VMGW OUT OF SERVICE','WO-EX UNIT FAULTY','Route is restricted','UNIT TYPE HAS NO REDUNDANCY','MTP3 link set failure','KPI PSR giảm đột biến dưới 90%','SIGNALLING LINK CONGESTION LEVEL EXCEEDED','SIZE ALTERATION OF DATA FILES SIZE CHANGE REQUIRED','CP CLUSTER SIZE ALTERATION OF DATA FILES SIZE CHANGE REQUIRED','M3UA link congestion') group by DEVICE_CODE,COUNTRY_CODE,FAULT_LEVEL_ID,FAULT_NAME,REASON,INFLUENCE,ADDITION_INFO,START_TIME union all (SELECT DEVICE_CODE as NODE_MANG,COUNTRY_CODE AS THI_TRUONG,START_TIME as THOI_GIAN, FAULT_LEVEL_ID as MUC_DO,FAULT_NAME as CANH_BAO, REASON as NGUYEN_NHAN, INFLUENCE as ANH_HUONG,ADDITION_INFO as THONG_TIN_THEM,count(*) SO_LUONG FROM NOCPROV4_APP.CORE_ALARM_CURRENT WHERE ALARM_MONITOR_TYPE IN (1,2,3,4,5,6) AND NETWORK_ID IN (6,33,13,14) AND START_TIME >SYSDATE-1/4/24 AND START_TIME <SYSDATE-1/6/24 and FAULT_NAME in ('Abnormal Service Counter','ASP down detected','Billing center does not take CDR files in time','Board Dynamic Source overload','Board Fault','Board offline','Board Port Fault','Boot Failure','Card Down','Card Missing','CPCI Board fault','CPGW ALARM DISCONNECT TO PGW','DB_DISCONNECTED','Diameter Link Down','Diameter Peer Down','Diameter Signaling Controller, Overload','Diameter Signaling Controller, Peer Instance unavailable','Diameter Signaling Controller, SCTP Association Congestion','DisconnectionSGWAndMME','DisconnectionSGWAndPGW','DNS Link Down','EM_DOWN','ethLinkDown','Fabric Data Error','Fabric Redundancy Lost','HDD_SERVER_OVER_THRESHOLD','Heartbeat Failure','INCORRECT WORKING STATE','LAG Constituent State Down','LAG State Down','Linecard Bandwidth Degrade','Link down','M3UA office inaccessible','M3UA Route Unavailable','M3UA Signaling Link Congestion','M3UA_ASP_DOWN','MANAGED OBJECT FAILED','MEDIA_DISCONNECTED','MYSQL_DOWN','NE Is Disconnected','No response from the OCS','No response from the PCRF','nocSupervisedMeasCri','NODE_DISCONNECT','NODE_HAS_GONE_DOWN','NODE_PGWCP_STATUS','NODE_SGWCP_STATUS','NODE_SPGWDP_STATUS','OCS_DISCONNECT','One DNS Server No Response','OSPF Neighbor Down','PERF_SMSC_HDD DISK_USAGE vuot nguong','PGW_CREATE_BEARER_SR_5M','Physical entity is reset','ranRncUnreachable','redEthMultiFaultNet1','Reduced Session Resilience','SGs Route Set fail','ss7M3uaRemoteSpUnavlForUP','ss7SccpRemoteSPCUnavail','The high usage of DB table') group by DEVICE_CODE,COUNTRY_CODE,FAULT_LEVEL_ID,FAULT_NAME,REASON,INFLUENCE,ADDITION_INFO,START_TIME) UNION ALL(SELECT DEVICE_CODE as NODE_MANG,COUNTRY_CODE AS THI_TRUONG , START_TIME as THOI_GIAN, FAULT_LEVEL_ID as MUC_DO,FAULT_NAME as CANH_BAO, REASON as NGUYEN_NHAN, INFLUENCE as ANH_HUONG,ADDITION_INFO as THONG_TIN_THEM,count(*) SO_LUONG FROM NOCPROV4_APP.CORE_ALARM_CURRENT WHERE ALARM_MONITOR_TYPE IN (1,2,3,4,5,6) AND NETWORK_ID IN (6,33,13,14) AND FAULT_LEVEL_ID=1 AND START_TIME >SYSDATE-1/4/24 AND START_TIME <SYSDATE-1/6/24 and FAULT_NAME in ('KPI_APPLICATION_AVGRESPONSETIME_ADDTONEBOX_OVER_THRESHOLD_GE','KPI_APPLICATION_AVGRESPONSETIME_PRESENTTONE_OVER_THRESHOLD_GE','KPI_APPLICATION_AVGRESPONSETIME_REGISTER_OVER_THRESHOLD_GE','KPI_APPLICATION_MAXRESPONSETIME_ADDTONEBOX_OVER_THRESHOLD_GE','KPI_APPLICATION-RECURING_MAXRESPONSETIME_RBTEXTENSION_OVER_THRESHOLD_GE','KPI_APPLICATION-RECURING_TOTAL-CHARGE_CHARGE_OVER_THRESHOLD_GE','KPI_COBRA_TPS_AVRRESTIMEMICRO_OVER_THRESHOLD_GE','KPI_COBRA_TPS_MAXRESTIMEMICRO_OVER_THRESHOLD_GE','KPI_COBRA_TPS_MAXRESTIMEMICRO_OVER_THRESHOLD_GE (G)','KPI_COBRA_TPS_MAXRESTIMEMICRO_OVER_THRESHOLD_GE (IAE)','KPI_COBRA_TPS_MINRESTIMEMICRO_OVER_THRESHOLD_GE','KPI_GGSN_ERICSSON PDP_SR giam so voi chu ky truoc','KPI_GGSN_ERICSSON TOTAL_GN giam so voi chu ky truoc','KPI_GGSN_ERICSSON TOTAL_GN_PERCENT vuot nguong','KPI_GGSN_NOKIA PDP_SR giam so voi chu ky truoc','KPI_SGSN_ERICSSON ATT_SR_2G(%) giam so voi chu ky truoc','KPI_SGSN_ERICSSON ATT_SR_3G(%) suy giam','KPI_SGSN_ERICSSON PDP_SR_2G(%) suy giam','KPI_VSMSC_NODE AO_SUCCESS_RATIO suy giam','KPI_VSMSC_NODE AT_SUCCESS_RATIO suy giam','KPI_VSMSC_NODE MO_SUCCESS_RATIO suy giam','KPI_VSMSC_NODE MT_SUCCESS_RATIO suy giam','PACKET_LOSS_SERVER_OVER_THRESHOLD','PERF_GGSN_ERICSSON_HDD HDD vuot nguong','PERF_GGSN_NOKIA_CPU_MEM CPU_PERCENT vuot nguong','PERF_SGSN_ERICSSON_CPU CPU vuot nguong') group by DEVICE_CODE,COUNTRY_CODE,FAULT_LEVEL_ID,FAULT_NAME,REASON,INFLUENCE,ADDITION_INFO,START_TIME)"

        conn, type_conn = self.get_conn()

        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_orc)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                # province, type_station, cabinet_name, node_code, cell_name, count
            except Exception as err:
                print(err)
                return res_lst
            finally:
                cursor.close()
                if type_conn:
                    self.pool.release(conn)
                else:
                    conn.close()

        return res_lst

    def get_important_alarm_series(self):
        res_lst = []

        sql_orc = "SELECT DEVICE_CODE AS NODE_MANG, COUNTRY_CODE AS THI_TRUONG,START_TIME AS THOI_GIAN, FAULT_LEVEL_ID AS MUC_DO,FAULT_NAME AS CANH_BAO,COUNT (*) SO_LUONG FROM NOCPROV4_APP.CORE_ALARM_CURRENT WHERE ALARM_MONITOR_TYPE IN (1,2,3,4,5,6) AND (NETWORK_ID='6' OR NETWORK_ID='33' OR NETWORK_ID='13' OR NETWORK_ID='14') AND START_TIME >SYSDATE-1/6/24 AND START_TIME <SYSDATE-1/12/24 AND (SELECT COUNT(ALARM_ID) FROM NOCPROV4_APP.CORE_ALARM_CURRENT WHERE ALARM_MONITOR_TYPE IN (1,2,3,4,5,6) AND (NETWORK_ID='6' OR NETWORK_ID='33' OR NETWORK_ID='13' OR NETWORK_ID='14') AND START_TIME >SYSDATE-1/6/24 and START_TIME <SYSDATE-1/12/24)>=50 GROUP BY DEVICE_CODE,COUNTRY_CODE,START_TIME,FAULT_LEVEL_ID,FAULT_NAME"

        conn, type_conn = self.get_conn()

        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_orc)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                # province, type_station, cabinet_name, node_code, cell_name, count
            except Exception as err:
                print(err)
                return res_lst
            finally:
                cursor.close()
                if type_conn:
                    self.pool.release(conn)
                else:
                    conn.close()

        return res_lst

    def get_alarm_error_connect(self):
        res_lst = []

        #sql_orc = "SELECT DEVICE_CODE AS NODE_MANG,INSERT_TIME AS THOI_GIAN,ALARM_NAME ASCANH_BAO,SERVICE_NAME AS TIEN_TRINH FROM NOCPROV4_APP.SERVICE_MONITOR_CURRENT WHERE NETWORK_CODE IN ('CORE','VASIN','PSTN','NGN') AND DEVICE_CODE NOT IN('MWHT05','MSHL18','TONH02','TONH02','IWPD01','IGHT01','IGPD01','IGHT01','IGPD01','MSHL05','GSPD03','MWHL05','MSNH14','MWHT07','MWHT09','STNH02','MSHL05','MWNH14','DWH_2_HCM','PPG_2_HCM','MMS_ALBUM_2','PPG_2_HNI','PPG_1_HNI','CUSTOMER_CARE_1','WAPGW_HNI_10.57.52.25','DWH_1_HNI','DWH_2_HNI','EMMHL01','PPG_1_HCM','EMMHT0101','SGHL22','MMSC_OMM_SERVER','MMS_RELAY1','MMS_RELAY1','MMSC_OMM_SERVER','U2HL01','U2HL01','PS3GHW','PS3GHW','DNSHK02','DNSHT05','DNSHL03','DNSHT06','DNSHK01','EDNS1','EDNS2','MMS_RELAY1','MMSC_1') ORDER BY START_TIME DESC"
        sql_orc = "SELECT DEVICE_CODE AS NODE_MANG,INSERT_TIME AS THOI_GIAN,ALARM_NAME ASCANH_BAO,SERVICE_NAME AS TIEN_TRINH FROM NOCPROV4_APP.SERVICE_MONITOR_CURRENT WHERE NETWORK_CODE IN ('CORE','VASIN','PSTN','NGN') AND DEVICE_CODE NOT IN('MWHT05','MSHL18','TONH02','TONH02','IWPD01','IGHT01','IGPD01','IGHT01','IGPD01','MSHL05','GSPD03','MWHL05','MSNH14','MWHT07','MWHT09','STNH02','MSHL05','MWNH14','DWH_2_HCM','PPG_2_HCM','MMS_ALBUM_2','PPG_2_HNI','PPG_1_HNI','CUSTOMER_CARE_1','WAPGW_HNI_10.57.52.25','DWH_1_HNI','DWH_2_HNI','EMMHL01','PPG_1_HCM','EMMHT0101','SGHL22','MMSC_OMM_SERVER','MMS_RELAY1','MMS_RELAY1','MMSC_OMM_SERVER','U2HL01','U2HL01','PS3GHW','PS3GHW','DNSHK02','DNSHT05','DNSHL03','DNSHT06','DNSHK01','EDNS1','EDNS2','MMS_RELAY1','MMSC_1','GSPV01','GSPV02','GWPV01-1','GWPV01-2','GWPV01-3','GWPV02-1','GWPV02-2','GWPV02-3') ORDER BY START_TIME DESC"

        conn, type_conn = self.get_conn()

        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_orc)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                # province, type_station, cabinet_name, node_code, cell_name, count
            except Exception as err:
                print(err)
                return res_lst
            finally:
                cursor.close()
                if type_conn:
                    self.pool.release(conn)
                else:
                    conn.close()

        return res_lst

    def get_smoke_node_15m(self):
        res_lst = []

        sql_orc = "SELECT cbname as CABINET FROM (SELECT cbname, LISTAGG(loai, ',') WITHIN GROUP (ORDER BY loai) loai FROM(Select cabinet_name_nims cbname, 'KHOI' loai From NOCPROV4_APP.acc_cabinet_fault_cur where cabinet_name_nims is not null and TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss')>SYSDATE-1/4/24 and fault_name in (select fault_name From NOCPROV4_APP.acc_cabinet_fault_cur where fault_name like '%KHOI%' group by fault_name) union all Select cabinet_name_nims, 'DOWN' loai From NOCPROV4_APP.acc_cabinet_fault_cur where cabinet_name_nims is not null and TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss')>SYSDATE-1/4/24 and fault_name in (select fault_name from nocprov4_app.acc_fault_defined where fault_group_id=2 group by fault_name)) GROUP BY cbname) WHERE instr(loai,'KHOI',1)<>0 and instr(loai,'DOWN',1)<>0"

        conn, type_conn = self.get_conn()

        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_orc)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                # province, type_station, cabinet_name, node_code, cell_name, count
            except Exception as err:
                print(err)
                return res_lst
            finally:
                cursor.close()
                if type_conn:
                    self.pool.release(conn)
                else:
                    conn.close()

        return res_lst

    def get_smoke_node_30m(self):
        res_lst = []

        sql_orc = "SELECT cbname as CABINET FROM (SELECT cbname, LISTAGG(loai, ',') WITHIN GROUP (ORDER BY loai) loai FROM(Select cabinet_name_nims cbname, 'KHOI' loai From NOCPROV4_APP.acc_cabinet_fault_cur where cabinet_name_nims is not null and TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss')>SYSDATE-1/2/24 and fault_name in (select fault_name From NOCPROV4_APP.acc_cabinet_fault_cur where fault_name like '%KHOI%' group by fault_name) union all Select cabinet_name_nims, 'DOWN' loai From NOCPROV4_APP.acc_cabinet_fault_cur where cabinet_name_nims is not null and TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss')>SYSDATE-1/2/24 and fault_name in (select fault_name from nocprov4_app.acc_fault_defined where fault_group_id=2 group by fault_name)) GROUP BY cbname) WHERE instr(loai,'KHOI',1)<>0 and instr(loai,'DOWN',1)<>0"

        conn, type_conn = self.get_conn()

        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_orc)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                # province, type_station, cabinet_name, node_code, cell_name, count
            except Exception as err:
                print(err)
                return res_lst
            finally:
                cursor.close()
                if type_conn:
                    self.pool.release(conn)
                else:
                    conn.close()

        return res_lst

    def get_smoke_node_1h(self):
        res_lst = []

        sql_orc = "SELECT cbname as CABINET FROM (SELECT cbname, LISTAGG(loai, ',') WITHIN GROUP (ORDER BY loai) loai FROM(Select cabinet_name_nims cbname, 'KHOI' loai From NOCPROV4_APP.acc_cabinet_fault_cur where cabinet_name_nims is not null and TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss')>SYSDATE-1/24 and fault_name in (select fault_name From NOCPROV4_APP.acc_cabinet_fault_cur where fault_name like '%KHOI%' group by fault_name) union all Select cabinet_name_nims, 'DOWN' loai From NOCPROV4_APP.acc_cabinet_fault_cur where cabinet_name_nims is not null and TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss')>SYSDATE-1/24 and fault_name in (select fault_name from nocprov4_app.acc_fault_defined where fault_group_id=2 group by fault_name)) GROUP BY cbname) WHERE instr(loai,'KHOI',1)<>0 and instr(loai,'DOWN',1)<>0"

        conn, type_conn = self.get_conn()

        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_orc)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                # province, type_station, cabinet_name, node_code, cell_name, count
            except Exception as err:
                print(err)
                return res_lst
            finally:
                cursor.close()
                if type_conn:
                    self.pool.release(conn)
                else:
                    conn.close()

        return res_lst

    def get_vendor(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        vend = ''
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = 'SELECT VENDOR_NAME FROM NOCPROV4_APP.V_CAT_VENDOR WHERE VENDOR_ID=(select VENDOR_ID from NOCPROV4_APP.CAT_DEVICE where upper(device_code)=:key_srch and country_code=:country_code AND VENDOR_ID IS NOT NULL)'

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch, country_code='VNM')
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    for res in res_lst:
                        if 'VENDOR_NAME' in res:
                            return res['VENDOR_NAME']

                except Exception as err:
                    print(err)
                    return vend
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return vend

    def get_performance_rnc(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''
        vend = self.get_vendor(key_srch=key_srch)
        if vend.upper() == 'ZTE':
            sql_orc = 'select cpu AS "%CPU", object_code AS Unit_max_CPU,data_time AS TIME  from ( SELECT device_code, object_code, cpu,data_time, row_number() over (partition by device_code order by cpu desc) my_rank  FROM NOCPROV4_APP.CORE_LOAD_RNC_ZTE_CARD_CUR  WHERE upper(device_code) = :key_srch AND (CPU IS NOT NULL) ) where my_rank =1'
        elif vend.upper() == 'NOKIA':
            sql_orc = 'select cpu  AS "%CPU", object_code AS Unit_max_CPU, data_time AS TIME  from ( SELECT device_code, object_code, cpu,data_time, row_number() over (partition by device_code order by cpu desc) my_rank  FROM NOCPROV4_APP.CORE_LOAD_RNC_NK_CARD_CUR WHERE upper(device_code) = :key_srch and (CPU is not null) ) where my_rank =1'
        elif vend.upper() == 'ERICSSON':
            sql_orc = 'select cpu  AS "%CPU", object_code AS Unit_max_CPU, data_time AS TIME  from ( SELECT device_code, object_code, cpu,data_time, row_number() over (partition by device_code order by cpu desc) my_rank  FROM NOCPROV4_APP.CORE_LOAD_RNC_ERIC_CARD_CUR WHERE upper(device_code) = :key_srch and (CPU is not null) ) where my_rank =1'
        elif vend.upper() == 'HUAWEI':
            sql_orc = 'select cpu  AS "%CPU", object_code AS Unit_max_CPU, data_time AS TIME  from ( SELECT device_code, object_code, cpu,data_time, row_number() over (partition by device_code order by cpu desc) my_rank  FROM  NOCPROV4_APP.CORE_LOAD_RNC_HW_CARD_CUR WHERE upper(device_code) = :key_srch and (CPU is not null) ) where my_rank =1'

        if chck_sql:
            if key_srch and sql_orc:

                conn, type_conn = self.get_conn()

                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc, key_srch=key_srch)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
        return res_lst

    def get_performance_bsc(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''
        vend = self.get_vendor(key_srch=key_srch)
        if vend.upper() == 'NOKIA':
            sql_orc = 'SELECT cpu_max AS "%CPU", CARD_MAX AS UNIT_MAX_CPU, data_time AS TIME FROM NOCPROV4_APP.CORE_LOAD_BSC_NOKIA_CUR WHERE upper(DEVICE_CODE)= :key_srch AND cpu_max is not null'
        elif vend.upper() == 'ERICSSON':
            sql_orc = 'SELECT CPU AS "%CPU", DATA_TIME AS TIME FROM NOCPROV4_APP.CORE_LOAD_BSC_ERIC_CUR where upper(DEVICE_CODE)=:key_srch AND cpu is not null'
        elif vend.upper() == 'HUAWEI':
            sql_orc = 'SELECT CPU  AS "%CPU", DATA_TIME AS TIME FROM NOCPROV4_APP.CORE_LOAD_BSC_HUAWEI_CUR where upper(DEVICE_CODE)=:key_srch AND cpu is not null'

        if chck_sql:
            if key_srch and sql_orc:

                conn, type_conn = self.get_conn()

                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc, key_srch=key_srch)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
        return res_lst

    def get_performance_msc(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''
        vend = self.get_vendor(key_srch=key_srch)
        if vend.upper() == 'NOKIA':
            sql_orc = 'select cpu "%CPU", object_code AS Unit_max_CPU,data_time "TIME" from ( SELECT device_code, object_code, cpu,data_time, row_number() over (partition by device_code order by cpu desc) my_rank  FROM NOCPROV4_APP.CORE_LOAD_NSS_NK_CARD_CUR  WHERE upper(device_code) = :key_srch AND (CPU IS NOT NULL) ) where my_rank =1'
        elif vend.upper() == 'ERICSSON':
            sql_orc = 'select cpu "%CPU", object_code "Unit max CPU",data_time "TIME" from ( SELECT device_code, object_code, cpu,data_time, row_number() over (partition by device_code order by cpu desc) my_rank  FROM NOCPROV4_APP.CORE_LOAD_NSS_ERIC_CARD_CUR  WHERE upper(device_code) = :key_srch AND (CPU IS NOT NULL) ) where my_rank =1'
        elif vend.upper() == 'HUAWEI':
            sql_orc = 'select cpu "%CPU", object_code "Unit max CPU",data_time "TIME" from ( SELECT device_code, object_code, cpu,data_time, row_number() over (partition by device_code order by cpu desc) my_rank  FROM NOCPROV4_APP.CORE_LOAD_NSS_HW_CARD_CUR  WHERE upper(device_code) = :key_srch AND (CPU IS NOT NULL) ) where my_rank =1'

        if chck_sql:
            if key_srch and sql_orc:

                conn, type_conn = self.get_conn()

                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc, key_srch=key_srch)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
        return res_lst

    def get_performance_hlr(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''
        vend = self.get_vendor(key_srch=key_srch)
        if vend.upper() == 'ERICSSON':
            sql_orc = 'select object_code "Unit max CPU",data_time "TIME", cpu "%CPU" from ( SELECT device_code, object_code, cpu,data_time, row_number() over (partition by device_code order by cpu desc) my_rank  FROM NOCPROV4_APP.CORE_LOAD_NSS_ERIC_CARD_CUR  WHERE upper(device_code) = :key_srch AND (CPU IS NOT NULL) ) where my_rank =1'

        if chck_sql:
            if key_srch and sql_orc:

                conn, type_conn = self.get_conn()

                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc, key_srch=key_srch)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
        return res_lst

    def get_performance_stp(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = 'select CARD_NAME "Unit max CPU",DATA_TIME "TIME",CPU "%CPU",MSU_TVG "MSU_TVG of unit" from  (     SELECT CARD_NAME ,DATA_TIME,CPU,MSU_TVG, row_number() over (partition by DEVICE_CODE ORDER BY DATA_TIME DESC,CPU DESC) my_rank      FROM NOCPROV4_APP.CORE_LOAD_NSS_STP_CARD_CUR WHERE DATA_TIME >SYSDATE-1.5/24 AND upper(DEVICE_CODE)=:key_srch ) WHERE my_rank =1'

        if chck_sql:
            if key_srch and sql_orc:

                conn, type_conn = self.get_conn()

                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc, key_srch=key_srch)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
        return res_lst

    def get_performance_sgsn(self, key_srch):
        # check key_srch co ma doc khong: quan trong check lai phan nay
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = []
        vend = self.get_vendor(key_srch=key_srch)
        if vend.upper() == 'NOKIA':
            sql_orc = [
                "select DATA_TIME AS TIME,CARD_NAME AS Unit_max_cpu,COUNTER_1 AS CPU_of_unit, COUNTER_2 AS Memory_of_unit    from  (select DEVICE_CODE,DATA_TIME,CARD_NAME,COUNTER_1, COUNTER_2, row_number() over (partition by device_code order by COUNTER_1 desc) my_rank  from   NOCPROV4_APP.VASIN_LOAD_PS2G_CUR  where device_type_name='SGSN' and upper(device_code)=:key_srch AND DATA_TYPE='PERF_SGSN_NOKIA_CPU_MEM' AND COUNTER_1 IS NOT NULL  ) where my_rank=1"]
        elif vend.upper() == 'ERICSSON':
            sql_orc = [
                "select DATA_TIME AS TIME,CARD_NAME AS Unit_max_CPU,COUNTER_1 AS CPU  from  (select DEVICE_CODE,DATA_TIME,CARD_NAME,COUNTER_1, row_number() over (partition by device_code order by COUNTER_1 desc) my_rank  from   NOCPROV4_APP.VASIN_LOAD_PS2G_CUR  where device_type_name='SGSN' and upper(device_code)=:key_srch AND DATA_TYPE='PERF_SGSN_ERICSSON_CPU'  ) where my_rank=1",
                "select DATA_TIME AS TIME,CARD_NAME AS Unit_max_memory,COUNTER_2 AS Memory   from  (select DEVICE_CODE,DATA_TIME,CARD_NAME,COUNTER_2, row_number() over (partition by device_code order by COUNTER_2 desc) my_rank  from   NOCPROV4_APP.VASIN_LOAD_PS2G_CUR  where device_type_name='SGSN' and upper(device_code)=:key_srch AND DATA_TYPE='PERF_SGSN_ERICSSON_MEM' AND COUNTER_2 IS NOT NULL  ) where my_rank=1"
                ]

        if chck_sql:
            if key_srch and sql_orc:

                conn, type_conn = self.get_conn()

                if conn:
                    cursor = conn.cursor()

                    try:
                        for _sql in sql_orc:
                            cursor.execute(_sql, key_srch=key_srch)
                            data = cursor.fetchall()
                            col_name_lst = [x[0] for x in cursor.description]
                            res_lst += [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
        return res_lst

    def generate_cursor(self, sql_orc, outp_lst, **kwargs):
        # B1: check tham so dau vao co hop le hay khong
        # muc tieu la lay ra du lieu dua theo args la list cac tham so dau ra. Mac dinh [] la all
        # kwargs la tham so dau vao de thuc thi cua sql
        chck_sql = True
        res_lst = []

        for key, val in kwargs.items():
            chck_sql = check_regex_acc(val)
            if not chck_sql:
                break
        if chck_sql:
                conn, type_conn = self.get_conn()
                if conn:
                    cursor = conn.cursor()

                    try:
                        if kwargs:
                            cursor.execute(sql_orc, kwargs)
                        else:
                            cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                        if outp_lst:
                            if res_lst:
                                # chi return 1 so gia tri trong args
                                for res in res_lst:
                                    for key, val in res.items():
                                        if key not in outp_lst:
                                            res.pop(key, None)

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()

        return res_lst

    def get_performance_ggsn_test(self, key_srch):
        outp_lst = []
        key_srch = str(key_srch).upper()
        sql_vend = 'SELECT VENDOR_NAME FROM NOCPROV4_APP.V_CAT_VENDOR WHERE VENDOR_ID=(select VENDOR_ID from NOCPROV4_APP.CAT_DEVICE where upper(device_code)=:key_srch and country_code=:country_code AND VENDOR_ID IS NOT NULL)'
        vend_dct = dict(key_srch=key_srch, country_code='VNM')
        vend_outp_lst = ['VENDOR_NAME']
        vend = self.generate_cursor(sql_vend,
                                    vend_outp_lst,
                                    **vend_dct)
        res_ggsn = []
        if vend:
            sql_orc = ''
            if str(vend[0]['VENDOR_NAME']).upper() == 'NOKIA':
                sql_orc = ["select DATA_TIME AS TIME,CARD_NAME AS Unit_max_cpu, COUNTER_1 AS CPU_of_unit from  (select DEVICE_CODE,DATA_TIME,CARD_NAME,COUNTER_1, COUNTER_2, row_number() over (partition by device_code order by COUNTER_1 desc) my_rank  from   NOCPROV4_APP.VASIN_LOAD_PS2G_CUR  where device_type_name='GGSN' and upper(device_code)=:key_srch AND DATA_TYPE='PERF_GGSN_NOKIA_CPU_MEM' AND COUNTER_1 IS NOT NULL  ) where my_rank=1"]
            elif str(vend[0]['VENDOR_NAME']).upper() == 'ERICSSON':
                sql_orc = [
                    "select DATA_TIME AS TIME,CARD_NAME AS Unit_max_cpu,COUNTER_1 As CPU_of_unit, COUNTER_2 AS Memory_of_unit    from  (select DEVICE_CODE,DATA_TIME,CARD_NAME,COUNTER_1, COUNTER_2, row_number() over (partition by device_code order by COUNTER_1 desc) my_rank  from   NOCPROV4_APP.VASIN_LOAD_PS2G_CUR  where device_type_name='GGSN' and upper(device_code)=:key_srch AND DATA_TYPE='PERF_GGSN_ERICSSON_CPU_MEM' AND COUNTER_1 IS NOT NULL  ) where my_rank=1"
                ]

            for sql in sql_orc:
                res_ggsn += self.generate_cursor(sql,
                                                outp_lst,
                                                **dict(key_srch=key_srch))
            print(res_ggsn)

        return res_ggsn

    def get_performance_ggsn(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''
        vend = self.get_vendor(key_srch=key_srch)
        if vend.upper() == 'NOKIA':
            sql_orc = "select DATA_TIME AS TIME,CARD_NAME AS Unit_max_cpu, COUNTER_1 AS CPU_of_unit from  (select DEVICE_CODE,DATA_TIME,CARD_NAME,COUNTER_1, COUNTER_2, row_number() over (partition by device_code order by COUNTER_1 desc) my_rank  from   NOCPROV4_APP.VASIN_LOAD_PS2G_CUR  where device_type_name='GGSN' and upper(device_code)=:key_srch AND DATA_TYPE='PERF_GGSN_NOKIA_CPU_MEM' AND COUNTER_1 IS NOT NULL  ) where my_rank=1"
        elif vend.upper() == 'ERICSSON':
            sql_orc = "select DATA_TIME AS TIME,CARD_NAME AS Unit_max_cpu,COUNTER_1 As CPU_of_unit, COUNTER_2 AS Memory_of_unit    from  (select DEVICE_CODE,DATA_TIME,CARD_NAME,COUNTER_1, COUNTER_2, row_number() over (partition by device_code order by COUNTER_1 desc) my_rank  from   NOCPROV4_APP.VASIN_LOAD_PS2G_CUR  where device_type_name='GGSN' and upper(device_code)=:key_srch AND DATA_TYPE='PERF_GGSN_ERICSSON_CPU_MEM' AND COUNTER_1 IS NOT NULL  ) where my_rank=1"

        if chck_sql:
            if key_srch and sql_orc:

                conn, type_conn = self.get_conn()

                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc, key_srch=key_srch)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
        return res_lst

    def get_performance_smsc(self, key_srch):
        # check key_srch co ma doc khong:
        key_srch = str(key_srch).upper()
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = "select DATA_TIME AS TIME,PARAM_1 AS Module,COUNTER_10 AS CPU,COUNTER_9 AS Memory_of_module_max_CPU  from  (select DATA_TIME,PARAM_1,COUNTER_10,COUNTER_9, row_number() over (partition by device_code order by COUNTER_10 desc) my_rank  from NOCPROV4_APP.VASIN_LOAD_SMSC_CUR where upper(DEVICE_CODE)=:key_srch AND DATA_TYPE='PERF_VASIN_SMSC_CPU_MEM_HDD'  ) where my_rank=1"

        if chck_sql:
            if key_srch and sql_orc:

                conn, type_conn = self.get_conn()

                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc, key_srch=key_srch)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
        return res_lst

    def get_alarm_smoke_10m(self):
        sql = "SELECT cbname as CABINET FROM (SELECT cbname, LISTAGG(loai, ',') WITHIN GROUP (ORDER BY loai) loai FROM(Select cabinet_name_nims cbname, 'KHOI' loai From NOCPROV4_APP.acc_cabinet_fault_cur where cabinet_name_nims is not null and TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss')>SYSDATE-1/4/24 and fault_name in (select fault_name From NOCPROV4_APP.acc_cabinet_fault_cur where fault_name like '%KHOI%' group by fault_name) union all Select cabinet_name_nims, 'DOWN' loai From NOCPROV4_APP.acc_cabinet_fault_cur where cabinet_name_nims is not null and TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss')>SYSDATE-1/4/24 and fault_name in (select fault_name from nocprov4_app.acc_fault_defined where fault_group_id=2 group by fault_name)) GROUP BY cbname) WHERE instr(loai,'KHOI',1)<>0 and instr(loai,'DOWN',1)<>0"
        res_lst = self.generate_cursor(sql, [], **dict())
        return res_lst

    def get_alarm_smoke_30m(self):
        sql = "SELECT cbname as CABINET FROM (SELECT cbname, LISTAGG(loai, ',') WITHIN GROUP (ORDER BY loai) loai FROM(Select cabinet_name_nims cbname, 'KHOI' loai From NOCPROV4_APP.acc_cabinet_fault_cur where cabinet_name_nims is not null and TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss')>SYSDATE-1/2/24 and fault_name in (select fault_name From NOCPROV4_APP.acc_cabinet_fault_cur where fault_name like '%KHOI%' group by fault_name) union all Select cabinet_name_nims, 'DOWN' loai From NOCPROV4_APP.acc_cabinet_fault_cur where cabinet_name_nims is not null and TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss')>SYSDATE-1/2/24 and fault_name in (select fault_name from nocprov4_app.acc_fault_defined where fault_group_id=2 group by fault_name)) GROUP BY cbname) WHERE instr(loai,'KHOI',1)<>0 and instr(loai,'DOWN',1)<>0"
        res_lst = self.generate_cursor(sql, [], **dict())
        return res_lst

    def get_alarm_smoke_60m(self):
        sql = "SELECT cbname as CABINET FROM (SELECT cbname, LISTAGG(loai, ',') WITHIN GROUP (ORDER BY loai) loai FROM(Select cabinet_name_nims cbname, 'KHOI' loai From NOCPROV4_APP.acc_cabinet_fault_cur where cabinet_name_nims is not null and TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss')>SYSDATE-1/24 and fault_name in (select fault_name From NOCPROV4_APP.acc_cabinet_fault_cur where fault_name like '%KHOI%' group by fault_name) union all Select cabinet_name_nims, 'DOWN' loai From NOCPROV4_APP.acc_cabinet_fault_cur where cabinet_name_nims is not null and TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss')>SYSDATE-1/24 and fault_name in (select fault_name from nocprov4_app.acc_fault_defined where fault_group_id=2 group by fault_name)) GROUP BY cbname) WHERE instr(loai,'KHOI',1)<>0 and instr(loai,'DOWN',1)<>0"
        res_lst = self.generate_cursor(sql, [], **dict())
        return res_lst

    def get_alarm_secondary_node(self, key_srch):
        key_srch = str(key_srch).upper()
        sql = "select FAULT_NAME, FAULT_LEVEL_ID AS FAULT_LEVEL, START_TIME from NOCPROV4_APP.CORE_ALARM_CURRENT where  upper(DEVICE_CODE) =:key_srch and ALARM_MONITOR_TYPE =0"
        res_lst = self.generate_cursor(sql, [], **dict(key_srch=key_srch))
        # them phan muc do
        for res in res_lst:
            if 'FAULT_LEVEL' in res:
                faul_id = res['FAULT_LEVEL']
                if str(faul_id) == '1':
                    res['FAULT_LEVEL'] = 'CRITICAL'
                elif str(faul_id) == '2':
                    res['FAULT_LEVEL'] = 'MAJOR'
                elif str(faul_id) == '3':
                    res['FAULT_LEVEL'] = 'MINOR'
                elif str(faul_id) == '4':
                    res['FAULT_LEVEL'] = 'WARNING'
                else:
                    res['FAULT_LEVEL'] = 'Unexpected'
        return res_lst

    def find_district_node_lst(self, node_lst):

            # check key_srch co ma doc khong:

            res_lst = []
            num = 0
            node_str = ''
            for x in node_lst:
                key_srch = str(x).upper()
                if key_srch:
                    chck_sql = check_regex_acc(key_srch)
                else:
                    chck_sql = True
                if chck_sql:
                    if num == 0:
                        node_str += "'" + str(x) + "'"
                    else:
                        node_str += ",'" + str(x) + "'"
                    num += 1

            sql_orc = "select distinct(a.station_code_nims), (select b.location_name from nocprov4_app.cat_location b where b.location_code = a.level4_code) Ma_huyen from NOCPROV4_APP.ACC_CAT_CABINET a where a.station_code_nims in(" + node_str + ")"

            if sql_orc:

                    conn, type_conn = self.get_conn()

                    if conn:
                        cursor = conn.cursor()

                        try:
                            cursor.execute(sql_orc)
                            data = cursor.fetchall()
                            col_name_lst = [x[0] for x in cursor.description]
                            res_lst = [dict(zip(col_name_lst, row)) for row in data]

                        except Exception as err:
                            print(err)
                            return res_lst
                        finally:
                            cursor.close()
                            if type_conn:
                                self.pool.release(conn)
                            else:
                                conn.close()
            return res_lst

    def get_performance_cpu_msc(self):
        # check key_srch co ma doc khong:
        chck_sql = True
        res_lst = []
        sql_orc = "select * from ( select * from ( select device_code,cpu,object_code AS Unit_max_CPU,data_time AS TIME from  ( SELECT device_code, object_code, cpu,data_time, row_number() over (partition by device_code order by cpu desc) my_rank  FROM NOCPROV4_APP.CORE_LOAD_NSS_ERIC_CARD_CUR  WHERE device_code IN ('MSHL01','MSHL07','MSHL06','MSHL08','MSPD26','MSPD27','MSPD28,''MSPD29','MSPD30','MSNH17','MSNH18','MSHK07') AND (CPU IS NOT NULL)) where my_rank =1 union all select device_code,cpu ,object_code AS Unit_max_CPU,data_time AS TIME from ( SELECT device_code, object_code, cpu,data_time, row_number() over (partition by device_code order by cpu desc) my_rank  FROM NOCPROV4_APP.CORE_LOAD_NSS_HW_CARD_CUR  where 1=1 and device_code in ('MSHK01','MSHK02','MSHK03','MSHK04','MSHT03','MSHT04','MSHT05','MSHT06','MSHT07','MSHT08','MSHT09','MSHT11','MSHT12','MSNH01','MSNH03','MSNH07','MSNH08','MSPL05','MSPL06','MSPL07','MSPL08','MSPL10','MSPL11') AND (CPU IS NOT NULL) ) where my_rank =1  union all select device_code,cpu ,object_code AS Unit_max_CPU,data_time AS TIME from ( SELECT device_code, object_code, cpu,data_time, row_number() over (partition by device_code order by cpu desc) my_rank  FROM NOCPROV4_APP.CORE_LOAD_NSS_NK_CARD_CUR  where 1=1 and device_code = '[NODE MANG]'  AND (CPU IS NOT NULL) ) where my_rank =1  )a order by cpu desc ) where rownum <4"

        if chck_sql:
            if sql_orc:

                conn, type_conn = self.get_conn()

                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
        return res_lst

    def get_performance_cpu_hlr(self):
        chck_sql = True
        res_lst = []
        sql_orc = "select * from  ( select  device_code,cpu,object_code AS Unit_max_CPU ,data_time AS TIME from  ( SELECT device_code, object_code, cpu,data_time, row_number() over (partition by device_code order by cpu desc) my_rank  FROM NOCPROV4_APP.CORE_LOAD_NSS_ERIC_CARD_CUR  WHERE device_code in ('HLHL01','HLHL02','HLHT05','HLHT06','HLPD03','HLPL07','HLPV04') AND (CPU IS NOT NULL) ) where my_rank =1 order by cpu desc ) where rownum <4"

        if chck_sql:
            if sql_orc:

                conn, type_conn = self.get_conn()

                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
        return res_lst

    def get_performance_cpu_sgsn(self):
        chck_sql = True
        res_lst = []
        sql_orc = "select * from ( select device_code,COUNTER_1 CPU ,DATA_TIME AS TIME,CARD_NAME AS Unit_max_CPU from (select DEVICE_CODE,DATA_TIME,CARD_NAME,COUNTER_1, row_number() over (partition by device_code order by COUNTER_1 desc) my_rank  from   NOCPROV4_APP.VASIN_LOAD_PS2G_CUR  where device_type_name='SGSN'  AND DATA_TYPE='PERF_SGSN_ERICSSON_CPU' ) where my_rank=1 order by cpu desc ) where rownum < 4"

        if chck_sql:
            if sql_orc:

                conn, type_conn = self.get_conn()

                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
        return res_lst

    def get_performance_pdp_bear_sgsn(self):
        chck_sql = True
        res_lst = []
        #sql_orc = "select * from  (select DEVICE_CODE,counter_14 as PDP_Bearer,DATA_TIME from  NOCPROV4_APP.VASIN_LOAD_PS2G_CUR where 1=1 AND DATA_TYPE='KPI_GGSN_ERICSSON'  order by PDP_Bearer desc ) where rownum < 4"
        sql_orc = "select * from ( select DEVICE_CODE,counter_14 as PDP_Bearer,DATA_TIME from NOCPROV4_APP.VASIN_LOAD_PS2G_CUR where 1=1 AND DATA_TYPE='KPI_GGSN_ERICSSON' and counter_14 is not null order by PDP_Bearer desc) where rownum < 4"
        if chck_sql:
            if sql_orc:

                conn, type_conn = self.get_conn()

                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
        return res_lst

    def get_performance_throughout_ggsn(self):
        chck_sql = True
        res_lst = []
        #sql_orc = "select * from ( select DEVICE_CODE,COUNTER_13 as THROUGHPUT,DATA_TIME  from  NOCPROV4_APP.VASIN_LOAD_PS2G_CUR where DATA_TYPE='KPI_GGSN_ERICSSON'  order by THROUGHPUT desc ) where rownum <4"
        sql_orc = "select * from ( select DEVICE_CODE,COUNTER_13 as THROUGHPUT,DATA_TIME from NOCPROV4_APP.VASIN_LOAD_PS2G_CUR where DATA_TYPE='KPI_GGSN_ERICSSON' and COUNTER_13 is not null order by THROUGHPUT desc ) where rownum <4"
        if chck_sql:
            if sql_orc:

                conn, type_conn = self.get_conn()

                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
        return res_lst

if __name__ == '__main__':
    time_now = get_date_now()
    id_lst = [102564614, 102564994, 102565024]
    dsn_str = cx_Oracle.makedsn(SERVER_NOCPRO, SERVER_NOCPRO_PORT, service_name=SERVER_NOCPRO_SERVICE_NAME)
    pool_nocpro = cx_Oracle.SessionPool(min=4,
                                        max=20, increment=1, threaded=True, dsn=dsn_str,
                                        user=SERVER_NOCPRO_USERNAME, password=SERVER_NOCPRO_PASSWORD,
                                        encoding='UTF-8', nencoding='UTF-8')
    _obj = TblBssCoreImpl(pool_nocpro)

    _test1 = _obj.find_district_node_lst(['QNM0480', 'QNM0513'])
    print(_test1)

