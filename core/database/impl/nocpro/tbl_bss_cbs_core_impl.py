#!/D:\python36 new
# -*- coding: utf8 -*-
import cx_Oracle

from core.helpers.stringhelpers import check_regex_acc
from core.helpers.date_helpers import get_date_now, convert_date_str_to_date_obj_ipms
from config import Development
import os
import sys

config = Development()
SERVER_CBS = config.__getattribute__('SERVER_CBS')
SERVER_CBS_PORT = config.__getattribute__('SERVER_CBS_PORT')
SERVER_CBS_SERVICE_NAME = config.__getattribute__('SERVER_CBS_SERVICE_NAME')
SERVER_CBS_USERNAME = config.__getattribute__('SERVER_CBS_USERNAME')
SERVER_CBS_PASSWORD = config.__getattribute__('SERVER_CBS_PASSWORD')


class TblBssCbsCoreImpl:
    def __init__(self, pool):
        self.pool = pool

    def get_conn(self):
        try:
            chck_pool = True
            try:
                conn = self.pool.acquire()
            except Exception as err:
                conn = None
                print(err)

            if not conn:
                print("Connecto to CBS manual")
                dsn_str = cx_Oracle.makedsn(SERVER_CBS, SERVER_CBS_PORT, service_name=SERVER_CBS_SERVICE_NAME)
                conn = cx_Oracle.connect(user=SERVER_CBS_USERNAME, password=SERVER_CBS_PASSWORD,
                                         dsn=dsn_str, encoding='UTF-8', nencoding='UTF-8')
                chck_pool = False
            # print(conn.version)

        except Exception as err:
            print(err)
            return None, False
        return conn, chck_pool

    def get_kpi_psr_msc(self, key_srch):
        # check key_srch co ma doc khong:
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "select SYS_DATETIME AS Time,COLUMN1 AS PSR from MDDATA.PERF_8119_0 where " \
                          "DEVICE_CODE LIKE '%" + key_srch + "%' AND SYS_DATETIME >SYSDATE-1.5/24 AND " \
                          "SYS_DATETIME = (select max(SYS_DATETIME) from MDDATA.PERF_8119_0 where DEVICE_CODE LIKE '%" + key_srch + "%') " \
                          "ORDER BY SYS_DATETIME DESC "

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_kpi_detail_rnc_now(self):
        # check key_srch co ma doc khong:
        res_lst = []
        sql_orc = ''

        sql_orc = "select DEVICE_CODE AS Node,SYS_DATETIME as TIME, COLUMN1 as CS_CCSR,COLUMN3 as CS_CDR,COLUMN9 as VOICE_TRAFFIC,COLUMN11 as CS_RAB_SR,COLUMN15 as CS_RAB_ATT ,COLUMN16 as CS_TAB_SUCCESS,COLUMN18 as CS_RRC_SR,COLUMN13 as CS_IRAT_HOSR,COLUMN2 as PS_CSSR,COLUMN7 as PS_CDR,COLUMN12 as PS_RAB_CR,COLUMN8 as PS_TRAFFIC, COLUMN10 as SHO_SR,COLUMN14 as PS_IRAT_HOSR from (select device_code,SYS_DATETIME, COLUMN1,COLUMN3,COLUMN9,COLUMN11,COLUMN15 ,COLUMN16 ,COLUMN18 ,COLUMN2 ,COLUMN7 ,COLUMN12 ,COLUMN8 , COLUMN10 ,COLUMN13 ,COLUMN14 ,row_number() over (partition by device_code order by SYS_DATETIME desc) my_rank from MDDATA.PERF_8093_0 WHERE DEVICE_CODE in (select DEVICE_CODE from MDDATA.PERF_8093_0 WHERE sys_datetime >sysdate-4/24 group by device_code)and sys_datetime >sysdate-1/5) where my_rank=1"

        conn, type_conn = self.get_conn()

        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_orc)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]

            except Exception as err:
                print(err)
                return res_lst
            finally:
                cursor.close()
                if type_conn:
                    self.pool.release(conn)
                else:
                    conn.close()
        return res_lst

    def get_kpi_detail_rnc_last(self):
        # check key_srch co ma doc khong:
        res_lst = []
        sql_orc = ''

        sql_orc = "select DEVICE_CODE AS Node,SYS_DATETIME as TIME, COLUMN1 as CS_CCSR,COLUMN3 as CS_CDR,COLUMN9 as VOICE_TRAFFIC,COLUMN11 as CS_RAB_SR,COLUMN15 as CS_RAB_ATT ,COLUMN16 as CS_TAB_SUCCESS,COLUMN18 as CS_RRC_SR,COLUMN13 as CS_IRAT_HOSR,COLUMN2 as PS_CSSR,COLUMN7 as PS_CDR,COLUMN12 as PS_RAB_CR,COLUMN8 as PS_TRAFFIC, COLUMN10 as SHO_SR,COLUMN14 as PS_IRAT_HOSR from (select device_code,SYS_DATETIME, COLUMN1,COLUMN3,COLUMN9,COLUMN11,COLUMN15 ,COLUMN16 ,COLUMN18 ,COLUMN2 ,COLUMN7 ,COLUMN12 ,COLUMN8 , COLUMN10 ,COLUMN13 ,COLUMN14 ,row_number() over (partition by device_code order by SYS_DATETIME desc) my_rank from MDDATA.PERF_8093_0 WHERE DEVICE_CODE in (select DEVICE_CODE from MDDATA.PERF_8093_0 WHERE sys_datetime >sysdate-4/24 group by device_code)and sys_datetime >sysdate-1/5) where my_rank=2"

        conn, type_conn = self.get_conn()

        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_orc)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]

            except Exception as err:
                print(err)
                return res_lst
            finally:
                cursor.close()
                if type_conn:
                    self.pool.release(conn)
                else:
                    conn.close()
        return res_lst

    def get_kpi_rnc(self, key_srch):
        # check key_srch co ma doc khong:
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "select SYS_DATETIME AS TIME, COLUMN1 AS CS_CCSR,COLUMN3 AS CS_CDR, COLUMN9 AS VOICE_TRAFFIC,COLUMN11 AS CS_RAB_SR,COLUMN15 AS CS_RAB_ATT,COLUMN16 AS CS_TAB_SUCCESS,COLUMN18 AS CS_RRC_SR,COLUMN13 AS CS_IRAT_HOSR,COLUMN2 AS PS_CSSR,COLUMN7 AS PS_CDR,COLUMN12 AS PS_RAB_CR,COLUMN8 AS PS_TRAFFIC, COLUMN10 AS SHO_SR,COLUMN14 AS PS_IRAT_HOSR from  (select SYS_DATETIME, COLUMN1,COLUMN3,COLUMN9,COLUMN11,COLUMN15 ,COLUMN16 ,COLUMN18 ,COLUMN2 ,COLUMN7 ,COLUMN12 ,COLUMN8 , COLUMN10 ,COLUMN13 ,COLUMN14 ,row_number() over (partition by device_code order by SYS_DATETIME desc) my_rank  from MDDATA.PERF_8093_0   WHERE DEVICE_CODE= :key_srch and sys_datetime >sysdate-1/12 ) where my_rank=1 "

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_kpi_bsc_2g(self, key_srch):
        # check key_srch co ma doc khong:
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "select SYS_DATETIME AS TIME, CSSR,TCH AS TCR,TCH_TRAF,CDR,SDR,SCR,CELL_CSSR_BAD,CELL_CDR_BAD from  (select SYS_DATETIME ,CSSR,TCH,TCH_TRAF,CDR,SDR,SCR,CELL_CSSR_BAD,CELL_CDR_BAD,row_number() over (partition by device_code order by SYS_DATETIME desc) my_rank  from MDDATA.PERF_2010_3   WHERE DEVICE_CODE=:key_srch and sys_datetime >sysdate-1/12 ) where my_rank=1 "

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_kpi_access_station(self, key_srch, type):
        # check key_srch co ma doc khong:
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                if type == '2G':
                    sql_orc = "SELECT SYS_DATETIME AS TIME, CDR AS CDR,CSSR AS CSSR,COLUMN6 AS DATAVOLUME,COLUMN3 AS TCR,COLUMN4 AS UL_DATAVOLUME,COLUMN5 AS DL_DATAVOLUME,column10 AS UTDR,column11 AS DTDR,column13 AS DTESR,HOSR AS HOSR,HO_ATT AS HO_ATT,HO_SUC AS HO_SUC,NUM_FAIL_CS AS NUM_FAIL_CS,column2 AS PDRAC,RX_QUAL_DL AS RX_QUAL_DL,RX_QUAL_UL AS RX_QUAL_UL,SCR AS SCR,SDCCH_ASS_SUCC AS SDCCH_ASS_SUCC,COLUMN9 AS SDCCH_ASS_SUCC_NEW,SDCCH_ATT AS SDCCH_ATT,SDCCH_DROP AS SDCCH_DROP,COLUMN8 AS SDCCH_DROP_NEW,SDR AS SDR,COLUMN7 AS SDR_NEW,TCH AS TCH,TCH_ASS_ATT AS TCH_ASS_ATT,TCH_ASS_SUCC AS TCH_ASS_SUCC,TCH_DROP AS TCH_DROP,TCH_TRAF AS TCH_TRAF,column1 AS downtime,column12 AS UTESR,column14 AS UTCR,column15 AS DTCR FROM (   SELECT SYS_DATETIME,CDR,CSSR,COLUMN6,COLUMN3,COLUMN4,COLUMN5,column10,column11,column13,HOSR,HO_ATT,HO_SUC,NUM_FAIL_CS,column2,RX_QUAL_DL,RX_QUAL_UL,SCR,SDCCH_ASS_SUCC,COLUMN9,SDCCH_ATT,SDCCH_DROP,COLUMN8,SDR,COLUMN7,TCH,TCH_ASS_ATT,TCH_ASS_SUCC,TCH_DROP,TCH_TRAF,column1,column12,column14,column15,row_number() over (partition by OBJECT_INSTANCE order by SYS_DATETIME desc) my_rank   FROM MDDATA.PERF_2004_5 WHERE SYS_DATETIME > SYSDATE-20/24 AND OBJECT_INSTANCE=:key_srch ) WHERE MY_RANK=1"
                elif type == '3G':
                    sql_orc = 'SELECT SYS_DATETIME  AS TIME, CS_CSSR,CS_CDR,CS_CALL_ATT,CS_RAB_SR,CS_RAB_ATT,CS_RAB_SUCC,CS_RRC_SR,CS_RRC_ATT,CS_RRC_SUCC,MUM_FAIL_CS,NUM_CS_CALL_DROP,RAB_CS_CONG,CS_IRAT_HOSR,CS_IRAT_HOSR_ATT,CS_IRAT_HOSR_SUCC,PS_CSSR,PS_CDR,PS_CALL_ATT,PS_RAB_SR,PS_RAB_ATT,PS_RAB_SUCC,PS_RRC_SR,PS_RRC_ATT,PS_RRC_SUCC,NUM_FAIL_PS,NUM_PS_CALL_DROP,RAB_PS_CONG,PS_IRAT_HOSR,PS_IRAT_HOSR_ATT,PS_IRAT_HOSR_SUCC,PS_TRAF,SHO_ATT,SHO_SUCC,SHOSR,VOICE_TRAF FROM ( SELECT SYS_DATETIME, CS_CSSR,CS_CDR,CS_CALL_ATT,CS_RAB_SR,CS_RAB_ATT,CS_RAB_SUCC,CS_RRC_SR,CS_RRC_ATT,CS_RRC_SUCC,MUM_FAIL_CS,NUM_CS_CALL_DROP,RAB_CS_CONG,CS_IRAT_HOSR,CS_IRAT_HOSR_ATT,CS_IRAT_HOSR_SUCC,PS_CSSR,PS_CDR,PS_CALL_ATT,PS_RAB_SR,PS_RAB_ATT,PS_RAB_SUCC,PS_RRC_SR,PS_RRC_ATT,PS_RRC_SUCC,NUM_FAIL_PS,NUM_PS_CALL_DROP,RAB_PS_CONG,PS_IRAT_HOSR,PS_IRAT_HOSR_ATT,PS_IRAT_HOSR_SUCC,PS_TRAF,SHO_ATT,SHO_SUCC,SHOSR,VOICE_TRAF, row_number() over (partition by OBJECT_INSTANCE order by SYS_DATETIME desc) my_rank FROM MDDATA.PERF_2005_5 WHERE OBJECT_INSTANCE=:key_srch AND SYS_DATETIME >SYSDATE-20/24 ) WHERE MY_RANK=1'
                elif type == '4G':
                    sql_orc = 'SELECT SYS_DATETIME "TIME", column3 AS CSFB_SR_4G,column17 AS CSFB_SR_4G_Fail,column9 AS DL_Cell_Throughput_Mbps,column7 AS DL_User_Throughput_Mpbs,column18 AS ERAB_Congestion,column13 AS ERAB_Abnormal_Release,column5 AS ERAB_CR,column14 AS ERAB_Release,column1 AS ERAB_SR,column16 AS Num_ERAB_Fail,column15 AS Num_PS_fail,column2 AS PS_CSSR,column11 AS Ps_traffic,column19 AS RRC_congestion,column12 AS RTWP_ERICSSON,column22 AS RTWP_NOKIA,column20 AS SINR_Pucch_Ericsson,column23 AS SINR_Pucch_Nokia,column21 AS SINR_Pusch_Ericsson,column24 AS SINR_Pusch_Nokia,column10 AS UL_Cell_Throughput_Mbps,column8 AS UL_User_Throughput_Mbps,column6 AS ePS_CDR,column31 AS rrc_att,column28 AS tu_prb_dl,column29 AS tu_prb_ul,column25 AS cell_downtime_auto,column26 AS cell_downtime_manual,column27 AS cell_downtime_total,column30 AS max_rrc_connected_user,column32 AS Call_Attempt,column33 AS UL_PLR,column4 AS eRRC_CR FROM (SELECT SYS_DATETIME, column1,column10,column11,column12,column13,column14,column15,column16,column17,column18,column19,column2,column20,column21,column22,column23,column24,column25,column26,column27,column28,column29,column3,column30,column31,column32,column33,column4,column5,column6,column7,column8,column9,row_number() over (partition by OBJECT_INSTANCE order by SYS_DATETIME desc) my_rank FROM MDDATA.PERF_9004_0 WHERE OBJECT_INSTANCE=:key_srch AND SYS_DATETIME > SYSDATE-20/24 )WHERE MY_RANK=1'
            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_mca_2g(self, key_srch):
        # check key_srch co ma doc khong:
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "select object_instance AS CELL, sys_datetime AS TIME, column2 AS Call_attempt, column1 AS MCA, column3 AS MCA_Rate FROM ( select object_instance, sys_datetime , column2 , column1, column3,row_number() over (partition by device_code order by SYS_DATETIME desc) my_rank from MDDATA.PERF_9041_0 where object_instance=:key_srch AND SYS_DATETIME >SYSDATE-2 ) WHERE my_rank=1"

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_mca_3g(self, key_srch):
        # check key_srch co ma doc khong:
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "select object_instance AS CELL, sys_datetime AS TIME, column2 AS Call_attempt, column1 AS MCA, column3 AS MCA_Rate FROM ( select object_instance, sys_datetime , column2 , column1, column3,row_number() over (partition by device_code order by SYS_DATETIME desc) my_rank from MDDATA.PERF_9042_0 where object_instance=:key_srch AND SYS_DATETIME >SYSDATE-2 ) WHERE my_rank=1"

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_mca_4g(self, key_srch):
        # check key_srch co ma doc khong:
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "select object_instance AS CELL, sys_datetime AS TIME, column2 AS Call_attempt, column1 AS MCA, column3 AS MCA_Rate FROM ( select object_instance, sys_datetime , column2 , column1, column3,row_number() over (partition by device_code order by SYS_DATETIME desc) my_rank from MDDATA.PERF_9043_0 where object_instance=:key_srch AND SYS_DATETIME >SYSDATE-2 ) WHERE my_rank=1"

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_kpi_lusr_msc(self, key_srch):
        # check key_srch co ma doc khong:
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "select sys_datetime AS Time,LUSR from MDDATA.PERF_1502_3 where " \
                          "SYS_DATETIME >SYSDATE-1.5/24 AND DEVICE_CODE LIKE '%" + key_srch + "%' " \
                          "AND SYS_DATETIME = (select max(SYS_DATETIME) from " \
                          "MDDATA.PERF_1502_3 where DEVICE_CODE LIKE '%" + key_srch + "%') " \
                          "ORDER BY SYS_DATETIME DESC "

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_kpi_hlr(self, key_srch):
        # check key_srch co ma doc khong:
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = 'select SYS_DATETIME AS Time,COLUMN1 AS "%MAP_SR" from MDDATA.PERF_9021_4 ' \
                          'where DEVICE_CODE = :key_srch AND SYS_DATETIME >SYSDATE-1.5/24 AND ROWNUM <= 1 ' \
                          'ORDER BY SYS_DATETIME DESC'

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_performance_gmsc(self, key_srch):
        # check key_srch co ma doc khong:
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = 'select OBJECT_INSTANCE AS Unit_max_CPU,SYS_DATETIME AS TIME, COLUMN1 AS CPU from  (     SELECT OBJECT_INSTANCE,SYS_DATETIME,COLUMN1, row_number() over (partition by DEVICE_CODE ORDER BY SYS_DATETIME DESC,COLUMN1 DESC) my_rank      FROM MDDATA.PERF_9020_1 WHERE SYS_DATETIME >SYSDATE-1.5/24 AND DEVICE_CODE=:key_srch ) WHERE my_rank =1 '

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc, key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_kpi_hs(self, key_srch):
        # check key_srch co ma doc khong:
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = 'select SYS_DATETIME AS Time,COLUMN1 AS "%DIAMETER_SR" from MDDATA.PERF_9021_5 ' \
                          'where DEVICE_CODE = :key_srch AND SYS_DATETIME > SYSDATE-1.5/24 ' \
                          'AND ROWNUM <= 1 ' \
                          'ORDER BY SYS_DATETIME DESC '

            conn, type_conn = self.get_conn()

            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc,key_srch=key_srch)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    if type_conn:
                        self.pool.release(conn)
                    else:
                        conn.close()
        return res_lst

    def get_performance_cpu_gmsc(self):
        chck_sql = True
        res_lst = []
        sql_orc = "select * from(select DEVICE_CODE,COLUMN1 AS CPU,OBJECT_INSTANCE AS Unit_max_CPU, SYS_DATETIME AS TIME from  (SELECT DEVICE_CODE,OBJECT_INSTANCE,SYS_DATETIME,COLUMN1, row_number() over (partition by DEVICE_CODE ORDER BY SYS_DATETIME DESC,COLUMN1 DESC) my_rank  FROM MDDATA.PERF_9020_1 WHERE SYS_DATETIME >SYSDATE-1.5/24  ) WHERE my_rank =1 order by cpu desc ) where rownum <4"

        if chck_sql:
            if sql_orc:

                conn, type_conn = self.get_conn()

                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
        return res_lst

    def get_kpi_ims_sbc_gmsc(self):
        chck_sql = True
        res_lst = []
        sql_orc = 'select DEVICE_CODE,OBJECT_INSTANCE as Unit_max_CPU,COLUMN1 as CPLoad,COLUMN3 as CpRegUsers , COLUMN4 as CpSession,SYS_DATETIME as TIME from(SELECT DEVICE_CODE,OBJECT_INSTANCE,SYS_DATETIME,COLUMN1,COLUMN3,COLUMN4, row_number() over (partition by DEVICE_CODE ORDER BY SYS_DATETIME DESC,COLUMN1 DESC) my_rank FROM MDDATA.PERF_9048_2 WHERE SYS_DATETIME >SYSDATE-1.5/24) WHERE my_rank =1 order by COLUMN1 desc'

        if chck_sql:
            if sql_orc:

                conn, type_conn = self.get_conn()

                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
        return res_lst

    def get_kpi_top_cpu_ims_sbc_gmsc(self):
        chck_sql = True
        res_lst = []
        sql_orc = 'select * from (select DEVICE_CODE,OBJECT_INSTANCE AS UNIT_MAX_CPU,COLUMN1 AS CPLOAD,SYS_DATETIME AS TIME from ( SELECT DEVICE_CODE,OBJECT_INSTANCE,SYS_DATETIME,COLUMN1, row_number() over (partition by DEVICE_CODE ORDER BY SYS_DATETIME DESC,COLUMN1 DESC) my_rank FROM MDDATA.PERF_9048_2 WHERE SYS_DATETIME >SYSDATE-1.5/24 ) WHERE my_rank =1 order by COLUMN1 desc ) where rownum <4'

        if chck_sql:
            if sql_orc:

                conn, type_conn = self.get_conn()

                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
        return res_lst

    def get_kpi_top_reg_ims_sbc_gmsc(self):
        chck_sql = True
        res_lst = []
        sql_orc = 'select * from (select DEVICE_CODE,OBJECT_INSTANCE AS UNIT_MAX,COLUMN3 as CPREGUSERS,SYS_DATETIME AS TIME from ( SELECT DEVICE_CODE,OBJECT_INSTANCE,SYS_DATETIME,COLUMN3, row_number() over (partition by DEVICE_CODE ORDER BY SYS_DATETIME DESC,COLUMN3 DESC) my_rank FROM MDDATA.PERF_9048_2 WHERE SYS_DATETIME >SYSDATE-1.5/24 ) WHERE my_rank =1 order by COLUMN3 desc  ) where rownum < 4'

        if chck_sql:
            if sql_orc:

                conn, type_conn = self.get_conn()

                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
        return res_lst

    def get_kpi_top_session_ims_sbc_gmsc(self):
        chck_sql = True
        res_lst = []
        sql_orc = 'select * from ( select DEVICE_CODE,OBJECT_INSTANCE AS UNIT_MAX,COLUMN4 as CPSESSION,SYS_DATETIME AS TIME from ( SELECT DEVICE_CODE,OBJECT_INSTANCE,SYS_DATETIME,COLUMN4, row_number() over (partition by DEVICE_CODE ORDER BY SYS_DATETIME DESC,COLUMN4 DESC) my_rank FROM MDDATA.PERF_9048_2 WHERE SYS_DATETIME >SYSDATE-1.5/24 ) WHERE my_rank =1 order by COLUMN4 desc ) where rownum <4'

        if chck_sql:
            if sql_orc:

                conn, type_conn = self.get_conn()

                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        if type_conn:
                            self.pool.release(conn)
                        else:
                            conn.close()
        return res_lst





if __name__ == '__main__':
    time_now = get_date_now()
    id_lst = [102564614, 102564994, 102565024]
    _obj = TblBssCoreImpl(None)
    res_updt = _obj.get_kpi_psr_msc("MSPD27")

