__author__ = 'VTN-DHML-DHIP10'
import cx_Oracle
import json
import os
import uuid
from core.helpers.date_helpers import convert_date_to_elk_from_ipms, get_date_yesterday_format_ipms, \
    get_date_before_yesterday_format_ipms
from config import Development
from core.helpers.stringhelpers import check_regex_acc
from core.helpers.list_helpers import convert_lst_to_str
config = Development()
SERVER_NOCPRO = config.__getattribute__('SERVER_NOCPRO')
SERVER_NOCPRO_PORT = config.__getattribute__('SERVER_NOCPRO_PORT')
SERVER_NOCPRO_SERVICE_NAME = config.__getattribute__('SERVER_NOCPRO_SERVICE_NAME')
SERVER_NOCPRO_USERNAME = config.__getattribute__('SERVER_NOCPRO_USERNAME')
SERVER_NOCPRO_PASSWORD = config.__getattribute__('SERVER_NOCPRO_PASSWORD')
SERVER_NOCPRO_USERNAME2 = config.__getattribute__('SERVER_NOCPRO_USERNAME2')
SERVER_NOCPRO_PASSWORD2 = config.__getattribute__('SERVER_NOCPRO_PASSWORD2')
#os.environ['NLS_LANG'] = 'VIETNAMESE_VIETNAM.VN8MSWIN1258'


class TblIpSyslogNewCoreNocpro:
    def __init__(self, alarm_monitor_type, alarm_name, alarm_note, alarm_type, area_code, auto_clear,
                 confitrm_threshold, count, country_code, date_time, device_id, device_important, device_ip,
                 device_name, ems_server, fault_group_id, group_customer_name, incident_group_code, insert_time,
                 is_collected_alarm, key_word_new, kpi_confirm_status,
                 kpi_ticket_status, kv_code, kv_name, message,
                 network_class_code, network_class_id, network_type_code, network_type_id, nims_device_code,
                 province_code, table_syslog, transfer_2nd_time, transfer_2nd_type, trouble_code, trouble_create_time,
                 trouble_status_str, user_1st_view, user_second_transfer, user_send_trouble, vendor, id_syslog):
        self.alarm_monitor_type = alarm_monitor_type
        self.alarm_name = alarm_name
        self.alarm_note = alarm_note
        self.alarm_type = alarm_type
        self.area_code = area_code
        self.auto_clear = auto_clear
        self.confirm_threshold = confitrm_threshold
        self.count = count
        self.country_code = country_code
        self.date_time = date_time
        self.device_id = device_id
        self.device_important = device_important
        self.device_ip = device_ip
        self.device_name = device_name
        self.ems_server = ems_server
        self.fault_group_id = fault_group_id
        self.group_customer_name = group_customer_name
        self.incident_group_code = incident_group_code
        self.insert_time = insert_time
        self.is_collected_alarm = is_collected_alarm
        self.key_word_new = key_word_new
        self.kpi_confirm_status = kpi_confirm_status
        self.kpi_ticket_status = kpi_ticket_status
        self.kv_code = kv_code
        self.kv_name = kv_name
        self.message = message
        self.network_class_code = network_class_code
        self.network_class_id = network_class_id
        self.network_type_code = network_type_code
        self.network_type_id = network_type_id
        self.nims_device_code = nims_device_code
        self.province_code = province_code
        self.table_syslog = table_syslog
        self.transfer_2nd_time = transfer_2nd_time
        self.transfer_2nd_type = transfer_2nd_type
        self.trouble_code = trouble_code
        self.trouble_create_time = trouble_create_time
        self.trouble_status_str = trouble_status_str
        self.user_1st_view = user_1st_view
        self.user_second_transfer = user_second_transfer
        self.user_send_trouble = user_send_trouble
        self.vendor = vendor
        self.id_syslog = id_syslog


class TblIpSyslogClearHistoryCoreNocpro:
    def __init__(self, alarm_monitor_type, alarm_name, alarm_note, alarm_type, area_code, auto_clear,
                 confitrm_threshold, count, country_code, date_time, device_id, device_important, device_ip,
                 device_name, ems_server, fault_group_id,
                 group_customer_name, incident_group_code, insert_time, is_collected_alarm, key_word_new,
                 kpi_confirm_status, kpi_ticket_status,  kv_code, kv_name, message,
                 network_class_code, network_class_id, network_type_code, network_type_id, nims_device_code,
                 province_code, table_syslog, transfer_2nd_time, transfer_2nd_type, trouble_code, trouble_create_time,
                 trouble_status_str, user_1st_view, user_second_transfer, user_send_trouble, vendor, id_syslog,
                 end_time,
                 influence):
        self.alarm_monitor_type = alarm_monitor_type
        self.alarm_name = alarm_name
        self.alarm_note = alarm_note
        self.alarm_type = alarm_type
        self.area_code = area_code
        self.auto_clear = auto_clear
        self.confirm_threshold = confitrm_threshold
        self.count = count
        self.country_code = country_code
        self.date_time = date_time
        self.device_id = device_id
        self.device_important = device_important
        self.device_ip = device_ip
        self.device_name = device_name
        self.ems_server = ems_server
        self.fault_group_id = fault_group_id
        self.group_customer_name = group_customer_name
        self.incident_group_code = incident_group_code
        self.insert_time = insert_time
        self.is_collected_alarm = is_collected_alarm
        self.key_word_new = key_word_new
        self.kpi_confirm_status = kpi_confirm_status
        self.kpi_ticket_status = kpi_ticket_status
        self.kv_code = kv_code
        self.kv_name = kv_name
        self.message = message
        self.network_class_code = network_class_code
        self.network_class_id = network_class_id
        self.network_type_code = network_type_code
        self.network_type_id = network_type_id
        self.nims_device_code = nims_device_code
        self.province_code = province_code
        self.table_syslog = table_syslog
        self.transfer_2nd_time = transfer_2nd_time
        self.transfer_2nd_type = transfer_2nd_type
        self.trouble_code = trouble_code
        self.trouble_create_time = trouble_create_time
        self.trouble_status_str = trouble_status_str
        self.user_1st_view = user_1st_view
        self.user_second_transfer = user_second_transfer
        self.user_send_trouble = user_send_trouble
        self.vendor = vendor
        self.id_syslog = id_syslog
        self.end_time = end_time
        self.influence = influence


class TblIpSyslogCoreElk:
    def __init__(self, time_bgn, time_end):
        self.time_bgn = time_bgn
        self.time_end = time_end
        self.es_tbl = 'tbl_nocpro_syslog_core'

    def get_conn(self):
        try:

            dsn_str = cx_Oracle.makedsn(SERVER_NOCPRO, SERVER_NOCPRO_PORT, service_name=SERVER_NOCPRO_SERVICE_NAME)
            conn = cx_Oracle.connect(user=SERVER_NOCPRO_USERNAME, password=SERVER_NOCPRO_PASSWORD, dsn=dsn_str)


        except Exception as err:
            print(err)
            return None
        return conn

    def get_new_lst(self):
        sql_qury = "SELECT * FROM NOCPROV4_IP.IP_ALARM_SYSLOG_CURRENT_CORE WHERE " \
                   "date_time >=to_date('" + self.time_bgn + "','yyyy-mm-dd') and  date_time<to_date('" + self.time_end + \
                   "','yyyy-mm-dd') and alarm_level in ('CRITICAL', 'MAJOR', 'MINOR') and end_time - date_time > 0.09" \
                   "and table_syslog in ('log_dcn','log_ipbn', 'log_mpbn', 'log_ps')"
        res_lst = list()
        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]

            except Exception as err:
                print(err)
                return list()
            finally:
                cursor.close()
                conn.close()
        return res_lst

    def get_clr_lst(self):
        sql_qury = "SELECT * FROM NOCPROV4_IP.IP_ALARM_SYSLOG_CLEAR_CORE WHERE " \
                   "date_time >=to_date('" + self.time_bgn + "','yyyy-mm-dd') and  date_time<to_date('" + self.time_end + \
                   "','yyyy-mm-dd') and alarm_level in ('CRITICAL', 'MAJOR', 'MINOR') and end_time - date_time > 0.09" \
                   "and table_syslog in ('log_dcn','log_ipbn', 'log_mpbn', 'log_ps')"
        res_lst = list()
        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]

            except Exception as err:
                print(err)
                return list()
            finally:
                cursor.close()
                conn.close()
        return res_lst

    def get_his_lst(self):
        sql_qury = "SELECT * FROM NOCPROV4_IP.IP_ALARM_SYSLOG_HISTORY_CORE WHERE " \
                   "date_time >=to_date('" + self.time_bgn + "','yyyy-mm-dd') and  date_time<to_date('" + self.time_end + \
                   "','yyyy-mm-dd') and alarm_level in ('CRITICAL', 'MAJOR', 'MINOR') and end_time - date_time > 0.09" \
                   "and table_syslog in ('log_dcn','log_ipbn', 'log_mpbn', 'log_ps')"
        res_lst = list()
        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]

            except Exception as err:
                print(err)
                return list()
            finally:
                cursor.close()
                conn.close()
        return res_lst


    def inst_els(self, syslog_type, syslog_obj_lst):
        for x in syslog_obj_lst:
            try:
                if x['DATE_TIME']:
                        x['DATE_TIME'] = convert_date_to_elk_from_ipms(str(x['DATE_TIME']))
                if x['TRANSFER_2ND_TIME']:
                        x['TRANSFER_2ND_TIME'] = convert_date_to_elk_from_ipms(str(x['TRANSFER_2ND_TIME']))
                if x['INSERT_TIME']:
                        x['INSERT_TIME'] = convert_date_to_elk_from_ipms(str(x['INSERT_TIME']))
                if x['TROUBLE_CREATE_TIME']:
                        x['TROUBLE_CREATE_TIME'] = convert_date_to_elk_from_ipms(str(x['TROUBLE_CREATE_TIME']))
                if x['END_TIME']:
                        x['END_TIME'] = convert_date_to_elk_from_ipms(str(x['END_TIME']))

                if syslog_type.upper() == 'NEW':
                    tbl_syslog_core = TblIpSyslogNewCoreNocpro(alarm_monitor_type=x['ALARM_MONITOR_TYPE'],
                                                               alarm_name=x['ALARM_NAME'],
                                                               alarm_note=x['ALARM_NOTE'],
                                                               alarm_type=x['ALARM_TYPE'],
                                                               area_code=x['AREA_CODE'],
                                                               auto_clear=x['AUTO_CLEAR'],
                                                               confitrm_threshold=x['CONFIRM_THRESHOLD'],
                                                               count=x['COUNT'],
                                                               country_code=x['COUNTRY_CODE'],
                                                               date_time=x['DATE_TIME'],
                                                               device_id=x['DEVICE_ID'],
                                                               device_important=x['DEVICE_IMPORTANT'],
                                                               device_ip=x['DEVICE_IP'],
                                                               device_name=x['DEVICE_NAME'],
                                                               ems_server=x['EMS_SERVER'],
                                                               fault_group_id=x['FAULT_GROUP_ID'],
                                                               group_customer_name=x['GROUP_CUSTOMER'],
                                                               incident_group_code=x['INCIDENT_GROUP_CODE'],
                                                               insert_time=x['INSERT_TIME'],
                                                               is_collected_alarm=x['IS_COLLECTED_ALARM'],
                                                               key_word_new=x['KEY_WORD_NEW'],
                                                               kpi_confirm_status=x['KPI_CONFIRM_STATUS'],
                                                               kpi_ticket_status=x['KPI_TICKET_STATUS'],
                                                               kv_code=x['KV_CODE'],
                                                               kv_name=x['KV_NAME'],
                                                               message=x['MESSAGE'],
                                                               network_class_code=x['NETWORK_CLASS_CODE'],
                                                               network_class_id=x['NETWORK_CLASS_ID'],
                                                               network_type_code=x['NETWORK_TYPE_CODE'],
                                                               network_type_id=x['NETWORK_TYPE_ID'],
                                                               nims_device_code=x['NIMS_DEVICE_CODE'],
                                                               province_code=x['PROVINCE_CODE'],
                                                               table_syslog=x['TABLE_SYSLOG'],
                                                               transfer_2nd_time=x['TRANSFER_2ND_TIME'],
                                                               transfer_2nd_type=x['TRANSFER_2ND_TYPE'],
                                                               trouble_code=x['TROUBLE_CODE'],
                                                               trouble_create_time=x['TROUBLE_CREATE_TIME'],
                                                               trouble_status_str=x['TROUBLE_STATUS'],
                                                               user_1st_view=x['USER_1ST_VIEW'],
                                                               user_second_transfer=x['USER_2ND_TRANSFER'],
                                                               user_send_trouble=x['USER_SEND_TROUBLE'],
                                                               vendor=x['VENDOR'],
                                                               id_syslog=x['ID'])


                else:
                    tbl_syslog_core = TblIpSyslogClearHistoryCoreNocpro(alarm_monitor_type=x['ALARM_MONITOR_TYPE'],
                                                                        alarm_name=x['ALARM_NAME'],
                                                                        alarm_note=x['ALARM_NOTE'],
                                                                        alarm_type=x['ALARM_TYPE'],
                                                                        area_code=x['AREA_CODE'],
                                                                        auto_clear=x['AUTO_CLEAR'],
                                                                        confitrm_threshold=x['CONFIRM_THRESHOLD'],
                                                                        count=x['COUNT'],
                                                                        country_code=x['COUNTRY_CODE'],
                                                                        date_time=x['DATE_TIME'],
                                                                        device_id=x['DEVICE_ID'],
                                                                        device_important=x['DEVICE_IMPORTANT'],
                                                                        device_ip=x['DEVICE_IP'],
                                                                        device_name=x['DEVICE_NAME'],
                                                                        ems_server=x['EMS_SERVER'],
                                                                        fault_group_id=x['FAULT_GROUP_ID'],
                                                                        group_customer_name=x['GROUP_CUSTOMER'],
                                                                        incident_group_code=x['INCIDENT_GROUP_CODE'],
                                                                        insert_time=x['INSERT_TIME'],
                                                                        is_collected_alarm=x['IS_COLLECTED_ALARM'],
                                                                        key_word_new=x['KEY_WORD_NEW'],
                                                                        kpi_confirm_status=x['KPI_CONFIRM_STATUS'],
                                                                        kpi_ticket_status=x['KPI_TICKET_STATUS'],
                                                                        kv_code=x['KV_CODE'],
                                                                        kv_name=x['KV_NAME'],
                                                                        message=x['MESSAGE'],
                                                                        network_class_code=x['NETWORK_CLASS_CODE'],
                                                                        network_class_id=x['NETWORK_CLASS_ID'],
                                                                        network_type_code=x['NETWORK_TYPE_CODE'],
                                                                        network_type_id=x['NETWORK_TYPE_ID'],
                                                                        nims_device_code=x['NIMS_DEVICE_CODE'],
                                                                        province_code=x['PROVINCE_CODE'],
                                                                        table_syslog=x['TABLE_SYSLOG'],
                                                                        transfer_2nd_time=x['TRANSFER_2ND_TIME'],
                                                                        transfer_2nd_type=x['TRANSFER_2ND_TYPE'],
                                                                        trouble_code=x['TROUBLE_CODE'],
                                                                        trouble_create_time=x['TROUBLE_CREATE_TIME'],
                                                                        trouble_status_str=x['TROUBLE_STATUS'],
                                                                        user_1st_view=x['USER_1ST_VIEW'],
                                                                        user_second_transfer=x['USER_2ND_TRANSFER'],
                                                                        user_send_trouble=x['USER_SEND_TROUBLE'],
                                                                        vendor=x['VENDOR'],
                                                                        id_syslog=x['ID'],
                                                                        end_time=x['END_TIME'],
                                                                        influence=x['INFLUENCE'])
                tbl_json = json.dumps(tbl_syslog_core, default=lambda o: o.__dict__)
                id_new_log = uuid.uuid3(uuid.NAMESPACE_DNS, str(tbl_syslog_core.id_syslog) + syslog_type)
                srch_dct = dict(_id=id_new_log.urn)
                hits = es.query_search_must(srch_dct, self.es_tbl)
                if not hits:
                    # insert new
                    res = es.create_document(self.es_tbl, id_new_log.urn, "_doc", tbl_json)

                else:
                    print('Duplicate ID %s' % id_new_log)

            except Exception as err:
                print(err)


class TblNocproImpl:
    def __init__(self, pool):
        self.pool = pool

    def get_conn(self):
        try:
            chck_pool = True
            try:
                conn = self.pool.acquire()
            except Exception as err:
                conn = None
                print(err)

            if not conn:
                print("Connecto to NOCPRO manual")
                dsn_str = cx_Oracle.makedsn(SERVER_NOCPRO, SERVER_NOCPRO_PORT, service_name=SERVER_NOCPRO_SERVICE_NAME)
                conn = cx_Oracle.connect(user=SERVER_NOCPRO_USERNAME2, password=SERVER_NOCPRO_PASSWORD2,
                                         dsn=dsn_str, encoding='UTF-8', nencoding='UTF-8')
                chck_pool = False

        except Exception as err:
            print(err)
            return None, False
        return conn, chck_pool

    def find_srt_lst(self, srt_name, dist_name):
        if srt_name:
            chck_sql = check_regex_acc(srt_name)
            if chck_sql and dist_name != '':
                chck_sql = check_regex_acc(dist_name)
        else:
            chck_sql = True
        res = 0
        dist_name = ''
        res_lst = []
        if chck_sql:
            if dist_name == '':
                sql_qury = "with alarm as ( select a.event_time,a.reason,a.province_name, a.device_name, a.area_code, b.location_name, b.parent_id from NOCPROV4_IP.IP_ALARM_CURRENT_ACCESS a, nocprov4_app.cat_location b where 1=1 and a.area_code = b.location_code and a.country_code = 'VNM' and a.alarm_monitor_type in (1,2,3,4,5,6)  and a.event_type = 'Node down' and a.network_class_code = 'SITE_ROUTER' group by a.event_time,a.reason,a.province_name, a.device_name, a.area_code, b.location_name,b.parent_id),location as (select * from nocprov4_app.cat_location where status = 1 ) select  x.device_name Ma_thietbi,x.event_time Down,x.reason NN, y.location_name Huyen from alarm x, location y where x.parent_id = y.location_id and x.device_name like '%" + str(srt_name) +"%'"
            else:
                sql_qury = "with alarm as ( select a.event_time,a.reason,a.province_name, a.device_name, a.area_code, b.location_name, b.parent_id from NOCPROV4_IP.IP_ALARM_CURRENT_ACCESS a, nocprov4_app.cat_location b where 1=1 and a.area_code = b.location_code and a.country_code = 'VNM' and a.alarm_monitor_type in (1,2,3,4,5,6)  and a.event_type = 'Node down' and a.network_class_code = 'SITE_ROUTER' group by a.event_time,a.reason,a.province_name, a.device_name, a.area_code, b.location_name,b.parent_id),location as (select * from nocprov4_app.cat_location where status = 1 ) select  x.device_name Ma_thietbi,x.event_time Down,x.reason NN, y.location_name Huyen from alarm x, location y where x.parent_id = y.location_id and x.device_name like '%" + str(
                    srt_name) + "%' and y.location_name like '%" + str(dist_name) +"%'"
            res_lst = list()
            conn, type_conn = self.get_conn()
            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_qury)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
        return res_lst

    def find_olt_lst(self, olt_name, dist_name):
        if olt_name:
            chck_sql = check_regex_acc(olt_name)
            if chck_sql and dist_name != '':
                chck_sql = check_regex_acc(dist_name)
        else:
            chck_sql = True
        res = 0
        dist_name = ''
        res_lst = []
        if chck_sql:
            if dist_name == '':
                sql_qury = "with alarm as (select a.start_time,a.reason,a.province_code,a.location_id,b.location_name, a.device_name, b.parent_id from nocprov4_app.GPON_ALARM_CURRENT a, nocprov4_app.cat_location b where 1=1 and a.province_code = b.location_code and a.country_code = 'VNM' and a.alarm_monitor_type in (1,2,3,4,5,6) and (fault_name like '%Mất giám sát%' or fault_name like 'The link between the server and the NE is broken' or fault_name like '%Polling Event%') and device_code like '%OLT%' group by a.start_time,a.reason,a.province_code,a.location_id,b.location_name, a.device_name, b.parent_id ), location as ( select * from nocprov4_app.cat_location where status = 1  ) select  x.device_name Ma_thietbi,x.start_time Down,x.reason NN,  (select z.location_name from nocprov4_app.cat_location z where z.location_id = y.parent_id) Huyen from alarm x, location y where x.location_id = y.location_id and x.device_name like '%" + str(olt_name)+ "%'"
            else:
                sql_qury = "with alarm as (select a.start_time,a.reason,a.province_code,a.location_id,b.location_name, a.device_name, b.parent_id from nocprov4_app.GPON_ALARM_CURRENT a, nocprov4_app.cat_location b where 1=1 and a.province_code = b.location_code and a.country_code = 'VNM' and a.alarm_monitor_type in (1,2,3,4,5,6) and (fault_name like '%Mất giám sát%' or fault_name like 'The link between the server and the NE is broken' or fault_name like '%Polling Event%') and device_code like '%OLT%' group by a.start_time,a.reason,a.province_code,a.location_id,b.location_name, a.device_name, b.parent_id ), location as ( select * from nocprov4_app.cat_location where status = 1  ) select  x.device_name Ma_thietbi,x.start_time Down,x.reason NN,  (select z.location_name from nocprov4_app.cat_location z where z.location_id = y.parent_id) Huyen from alarm x, location y where x.location_id = y.location_id and x.device_name like '%" + str(
                    olt_name) + "%' and z.location_name like '%" + str(dist_name)+ "%'"

            res_lst = list()
            conn, type_conn = self.get_conn()
            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_qury)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
        return res_lst

    def find_sw_lst(self, sw_name, dist_name):
        if sw_name:
            chck_sql = check_regex_acc(sw_name)
            if chck_sql and dist_name != '':
                chck_sql = check_regex_acc(dist_name)
        else:
            chck_sql = True
        res = 0
        dist_name = ''
        res_lst = []
        if chck_sql:
            if dist_name == '':
                sql_qury = "with alarm as (select a.event_time,a.reason,a.province_name, a.device_name, a.area_code, b.location_name, b.parent_id from NOCPROV4_IP.IP_ALARM_CURRENT_ACCESS a, nocprov4_app.cat_location b where 1=1 and a.area_code = b.location_code and a.country_code = 'VNM' and a.alarm_monitor_type in (1,2,3,4,5,6)  and a.event_type = 'Node down' and a.network_class_code = 'SWITCH' group by a.event_time,a.reason,a.province_name, a.device_name, a.area_code, b.location_name, b.parent_id),location as (select * from nocprov4_app.cat_location where status = 1) select  x.device_name Ma_thietbi,x.event_time Down,x.reason NN,y.location_name Huyen from alarm x, location y where x.parent_id = y.location_id and x.device_name like '%" + str(sw_name) + "%'"
            else:
                sql_qury = "with alarm as (select a.event_time,a.reason,a.province_name, a.device_name, a.area_code, b.location_name, b.parent_id from NOCPROV4_IP.IP_ALARM_CURRENT_ACCESS a, nocprov4_app.cat_location b where 1=1 and a.area_code = b.location_code and a.country_code = 'VNM' and a.alarm_monitor_type in (1,2,3,4,5,6)  and a.event_type = 'Node down' and a.network_class_code = 'SWITCH' group by a.event_time,a.reason,a.province_name, a.device_name, a.area_code, b.location_name, b.parent_id),location as (select * from nocprov4_app.cat_location where status = 1) select  x.device_name Ma_thietbi,x.event_time Down,x.reason NN,y.location_name Huyen from alarm x, location y where x.parent_id = y.location_id and x.device_name like '%" + str(
                    sw_name) + "%' and y.location_name like '%" + str(dist_name) + "%'"
            res_lst = list()
            conn, type_conn = self.get_conn()
            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_qury)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
        return res_lst

    def find_station_lst(self, sttn_name, dist_name):
        if sttn_name:
            chck_sql = check_regex_acc(sttn_name)
            if chck_sql and dist_name != '':
                chck_sql = check_regex_acc(dist_name)
        else:
            chck_sql = True
        res = 0
        dist_name = ''
        res_lst = []
        if chck_sql:
            if dist_name == '':
                if sttn_name in ['KV1', 'KV2', 'KV3']:
                    sql_qury = "SELECT d.MA_TRAM,COUNT(*) AS SO_TU,d.HUYEN,d.ZONE from  ( select a.CABINET_NAME MA_TU, a.STATION_CODE_NIMS MA_TRAM,a.LEVEL4_NAME HUYEN,a.LEVEL3_NAME TINH,a.DEPT2_CODE ZONE,TO_DATE(a.OCCURED_TIME, 'YYYYMMDDHH24MISS') TIME_DOWN,c.item_name LOAI_TRAM,a.REASON_FINAL_NAME NN  from NOCPROV4_APP.ACC_CABINET_FAULT_CUR a, NOCPROV4_APP.CAT_STATION b, NOCPROV4_APP.cat_item c  WHERE a.FAULT_GROUP_ID = '2'  AND a.ALARM_MONITOR_TYPE ='1'  AND a.END_TIME IS NULL  and a.STATION_CODE_NIMS = b.STATION_CODE  and b.STATION_TYPE_ID=c.item_id and a.DEPT2_CODE like '%" + str(sttn_name) + "%'  )d  GROUP BY d.MA_TRAM, d.HUYEN,d.ZONE"
                else:
                    sql_qury = "SELECT d.MA_TRAM,COUNT(*) AS SO_TU,d.HUYEN from (select a.CABINET_NAME MA_TU, a.STATION_CODE_NIMS MA_TRAM,a.LEVEL4_NAME HUYEN,a.LEVEL3_NAME TINH,TO_DATE(a.OCCURED_TIME, 'YYYYMMDDHH24MISS') TIME_DOWN,c.item_name LOAI_TRAM,a.REASON_FINAL_NAME NN from NOCPROV4_APP.ACC_CABINET_FAULT_CUR a, NOCPROV4_APP.CAT_STATION b, NOCPROV4_APP.cat_item c WHERE a.FAULT_GROUP_ID = '2' AND a.ALARM_MONITOR_TYPE ='1' AND a.END_TIME IS NULL and a.STATION_CODE_NIMS = b.STATION_CODE and b.STATION_TYPE_ID=c.item_id and a.STATION_CODE_NIMS like '%" + str(sttn_name) +"%' )d GROUP BY d.MA_TRAM, d.HUYEN"
            else:
                sql_qury = "with alarm as (select a.event_time,a.reason,a.province_name, a.device_name, a.area_code, b.location_name, b.parent_id from NOCPROV4_IP.IP_ALARM_CURRENT_ACCESS a, nocprov4_app.cat_location b where 1=1 and a.area_code = b.location_code and a.country_code = 'VNM' and a.alarm_monitor_type in (1,2,3,4,5,6)  and a.event_type = 'Node down' and a.network_class_code = 'SWITCH' group by a.event_time,a.reason,a.province_name, a.device_name, a.area_code, b.location_name, b.parent_id),location as (select * from nocprov4_app.cat_location where status = 1) select  x.device_name Ma_thietbi,x.event_time Down,x.reason NN,y.location_name Huyen from alarm x, location y where x.parent_id = y.location_id and x.device_name like '%" + str(
                    sttn_name) + "%' and y.location_name like '%" + str(dist_name) + "%'"
            res_lst = list()
            conn, type_conn = self.get_conn()
            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_qury)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
        return res_lst

    def find_cell_lst(self, sttn_name, dist_name):
        if sttn_name:
            chck_sql = check_regex_acc(sttn_name)
            if chck_sql and dist_name != '':
                chck_sql = check_regex_acc(dist_name)
        else:
            chck_sql = True
        res = 0
        dist_name = ''
        res_lst = []
        if chck_sql:
            if dist_name == '':
                if sttn_name in ['KV1', 'KV2', 'KV3']:
                    sql_qury = "select a.CABINET_NAME MA_TU, a.STATION_CODE_NIMS MA_TRAM,a.LEVEL4_NAME HUYEN,a.LEVEL3_NAME TINH,a.DEPT2_CODE ZONE,TO_DATE(a.OCCURED_TIME, 'YYYYMMDDHH24MISS') TIME_DOWN,c.item_name LOAI_TRAM,a.REASON_FINAL_NAME NN  from NOCPROV4_APP.ACC_CABINET_FAULT_CUR a, NOCPROV4_APP.CAT_STATION b, NOCPROV4_APP.cat_item c  WHERE a.FAULT_GROUP_ID = '2' AND a.ALARM_MONITOR_TYPE ='1'  AND a.END_TIME IS NULL  AND a.STATION_CODE_NIMS = b.STATION_CODE  AND b.STATION_TYPE_ID=c.item_id  AND a.DEPT2_CODE like '" + str(sttn_name) + "'   ORDER BY TIME_DOWN DESC				"
                else:
                    sql_qury = "select a.CABINET_NAME MA_TU, a.STATION_CODE_NIMS MA_TRAM,a.LEVEL4_NAME HUYEN,a.LEVEL3_NAME TINH,TO_DATE(a.OCCURED_TIME, 'YYYYMMDDHH24MISS') TIME_DOWN,c.item_name LOAI_TRAM,a.REASON_FINAL_NAME NN from NOCPROV4_APP.ACC_CABINET_FAULT_CUR a, NOCPROV4_APP.CAT_STATION b, NOCPROV4_APP.cat_item c WHERE a.FAULT_GROUP_ID = '2' AND a.ALARM_MONITOR_TYPE ='1' AND a.END_TIME IS NULL and a.STATION_CODE_NIMS = b.STATION_CODE and b.STATION_TYPE_ID=c.item_id and a.STATION_CODE_NIMS like '%" + str(sttn_name) +"%' ORDER BY TIME_DOWN DESC"
            else:
                sql_qury = "select a.CABINET_NAME MA_TU, a.STATION_CODE_NIMS MA_TRAM,a.LEVEL4_NAME HUYEN,a.LEVEL3_NAME TINH,TO_DATE(a.OCCURED_TIME, 'YYYYMMDDHH24MISS') TIME_DOWN,c.item_name LOAI_TRAM,a.REASON_FINAL_NAME NN from NOCPROV4_APP.ACC_CABINET_FAULT_CUR a, NOCPROV4_APP.CAT_STATION b, NOCPROV4_APP.cat_item c WHERE a.FAULT_GROUP_ID = '2' AND a.ALARM_MONITOR_TYPE ='1' AND a.END_TIME IS NULL and a.STATION_CODE_NIMS = b.STATION_CODE and b.STATION_TYPE_ID=c.item_id and a.STATION_CODE_NIMS like '%" + str(sttn_name) +"%' ORDER BY TIME_DOWN DESC"

            res_lst = list()
            conn, type_conn = self.get_conn()
            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_qury)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
        return res_lst

    def find_pwr_off_lst(self, prvn_name, dist_name):

        if prvn_name:
            chck_sql = check_regex_acc(prvn_name)
            if chck_sql and dist_name != '':
                chck_sql = check_regex_acc(dist_name)
        else:
            chck_sql = True
        res = 0
        res_lst = []
        if chck_sql:
            if dist_name != '':
                sql_qury = "SELECT a.STATION_CODE_NIMS Trạm,a.TYPE_STATION TỦ,a.LEVEL4_NAME HUYEN,a.DEPT3_CODE TINH,c.ACC_CABINET_TYPE_DETAIL_NAME CABINET_TYPE,c1.ACC_CABINET_TYPE_DETAIL_NAME DETAIL, case when (select max(a1.alarm_id) from NOCPROV4_APP.ACC_CABINET_FAULT_CUR a1  where a1.fault_name like '%CAN NGUON%' and a1.station_code_nims = a.station_code_nims) is null then 'Khong' else 'Co' end  CanNguon, case when (select max(a1.alarm_id) from NOCPROV4_APP.ACC_CABINET_FAULT_CUR a1  where a1.fault_name like '%CHAY MAY NO%' and a1.station_code_nims = a.station_code_nims) is null then 'Khong' else 'Co' end ChayMayNo FROM NOCPROV4_APP.ACC_CABINET_FAULT_CUR a,nocprov4_app.acc_cabinet_type_detail c,nocprov4_app.acc_cabinet_type_detail c1 WHERE a.CABINET_TYPE_2 = c.ACC_CABINET_TYPE_DETAIL_ID(+) AND a.CABINET_TYPE_22 = c1.ACC_CABINET_TYPE_DETAIL_ID(+) AND upper(FAULT_NAME) like '%MAT DIEN%' AND END_TIME IS NULL AND ALARM_MONITOR_TYPE in (1,2,3,4,5,6) AND STATION_CODE_NIMS IS NOT NULL AND upper(a.LEVEL4_NAME) like '%" + str(dist_name) + "%' AND upper(a.DEPT3_CODE) like '%" + str(prvn_name) + "%'"
            else:
                if prvn_name in ['KV1', 'KV2', 'KV3']:
                    sql_qury = "SELECT a.STATION_CODE_NIMS Trạm,a.TYPE_STATION TỦ,a.LEVEL4_NAME HUYEN,a.DEPT3_CODE TINH,a.DEPT2_CODE ZONE,c.ACC_CABINET_TYPE_DETAIL_NAME CABINET_TYPE,c1.ACC_CABINET_TYPE_DETAIL_NAME DETAIL,  case when (select max(a1.alarm_id)  from NOCPROV4_APP.ACC_CABINET_FAULT_CUR a1   where a1.fault_name like '%CAN NGUON%'  and a1.station_code_nims = a.station_code_nims) is null then 'Khong' else 'Co' end  CanNguon,  case when (select max(a1.alarm_id)  from NOCPROV4_APP.ACC_CABINET_FAULT_CUR a1   where a1.fault_name like '%CHAY MAY NO%' and a1.station_code_nims = a.station_code_nims) is null  then 'Khong' else 'Co' end ChayMayNo  FROM NOCPROV4_APP.ACC_CABINET_FAULT_CUR a,nocprov4_app.acc_cabinet_type_detail c,nocprov4_app.acc_cabinet_type_detail c1 WHERE a.CABINET_TYPE_2 = c.ACC_CABINET_TYPE_DETAIL_ID(+) AND a.CABINET_TYPE_22 = c1.ACC_CABINET_TYPE_DETAIL_ID(+) AND upper(FAULT_NAME) like '%MAT DIEN%'  AND END_TIME IS NULL AND ALARM_MONITOR_TYPE in (1,2,3,4,5,6)  AND STATION_CODE_NIMS IS NOT NULL  AND upper(a.DEPT2_CODE) like '%" + str(prvn_name) + "%'"
                else:
                    sql_qury = "SELECT a.STATION_CODE_NIMS Trạm,a.TYPE_STATION TỦ,a.LEVEL4_NAME HUYEN,a.DEPT3_CODE TINH,c.ACC_CABINET_TYPE_DETAIL_NAME CABINET_TYPE,c1.ACC_CABINET_TYPE_DETAIL_NAME DETAIL, case when (select max(a1.alarm_id) from NOCPROV4_APP.ACC_CABINET_FAULT_CUR a1  where a1.fault_name like '%CAN NGUON%' and a1.station_code_nims = a.station_code_nims) is null then 'Khong' else 'Co' end  CanNguon, case when (select max(a1.alarm_id) from NOCPROV4_APP.ACC_CABINET_FAULT_CUR a1  where a1.fault_name like '%CHAY MAY NO%' and a1.station_code_nims = a.station_code_nims) is null then 'Khong' else 'Co' end ChayMayNo FROM NOCPROV4_APP.ACC_CABINET_FAULT_CUR a,nocprov4_app.acc_cabinet_type_detail c,nocprov4_app.acc_cabinet_type_detail c1 WHERE a.CABINET_TYPE_2 = c.ACC_CABINET_TYPE_DETAIL_ID(+) AND a.CABINET_TYPE_22 = c1.ACC_CABINET_TYPE_DETAIL_ID(+) AND upper(FAULT_NAME) like '%MAT DIEN%' AND END_TIME IS NULL AND ALARM_MONITOR_TYPE in (1,2,3,4,5,6) AND STATION_CODE_NIMS IS NOT NULL AND upper(a.DEPT3_CODE) like '%" + str(prvn_name) + "%'"

            res_lst = list()
            conn, type_conn = self.get_conn()
            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_qury)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
        return res_lst

    def find_high_temp_lst(self, prvn_name, dist_name):
        if prvn_name:
            chck_sql = check_regex_acc(prvn_name)
            if chck_sql and dist_name != '':
                chck_sql = check_regex_acc(dist_name)
        else:
            chck_sql = True
        res = 0
        res_lst = []
        if chck_sql:
            if dist_name != '':
                if prvn_name in ['KV1', 'KV2', 'KV3']:
                    sql_qury = "SELECT a.STATION_CODE_NIMS Trạm, a.TYPE_STATION Tủ,a.LEVEL4_NAME HUYEN,a.DEPT3_CODE TINH,a.DEPT2_CODE ZONE,NVL(SUBSTR(regexp_substr(a.SAP_INFO,'Temperature:\s+" + '"(\S+)"' + "'),13),'Khong co thong tin') NHIETDO,c.ACC_CABINET_TYPE_DETAIL_NAME CABINET_TYPE,c1.ACC_CABINET_TYPE_DETAIL_NAME DETAIL,  case when (select max(a1.alarm_id)  from NOCPROV4_APP.ACC_CABINET_FAULT_CUR a1   where a1.fault_name like 'MAT DIEN%'  and a1.station_code_nims = a.station_code_nims) is null  then 'Khong' else 'Co' end  Matdien,  case when (select max(a1.alarm_id)  from NOCPROV4_APP.ACC_CABINET_FAULT_CUR a1   where a1.FAULT_GROUP_ID = 29 and a1.NODE_CODE = 'POWER'  and a1.station_code_nims = a.station_code_nims) is null  then 'Khong' else 'Co' end canhbaocodienkhac  FROM NOCPROV4_APP.ACC_CABINET_FAULT_CUR a,nocprov4_app.acc_cabinet_type_detail c,nocprov4_app.acc_cabinet_type_detail c1  WHERE a.CABINET_TYPE_2 = c.ACC_CABINET_TYPE_DETAIL_ID(+)  AND a.CABINET_TYPE_22 = c1.ACC_CABINET_TYPE_DETAIL_ID(+)  AND upper(FAULT_NAME) like '%NHIET DO CAO%'  AND END_TIME IS NULL AND ALARM_MONITOR_TYPE in (1,2,3,4,5,6)  AND upper(a.DEPT2_CODE) like '%" + str(prvn_name) + "%' "
                else:
                    sql_qury = "SELECT a.STATION_CODE_NIMS Trạm, a.TYPE_STATION Tủ,a.LEVEL4_NAME HUYEN,a.DEPT3_CODE TINH,NVL(SUBSTR(regexp_substr(a.NOTE,'Temperature:\s+" + '"(\S+)"' + "'),13),'Khong co thong tin') NHIETDO,c.ACC_CABINET_TYPE_DETAIL_NAME CABINET_TYPE,c1.ACC_CABINET_TYPE_DETAIL_NAME DETAIL, case when (select max(a1.alarm_id) from NOCPROV4_APP.ACC_CABINET_FAULT_CUR a1  where a1.fault_name like 'MAT DIEN%' and a1.station_code_nims = a.station_code_nims) is null then 'Khong' else 'Co' end  Matdien, case when (select max(a1.alarm_id) from NOCPROV4_APP.ACC_CABINET_FAULT_CUR a1  where a1.FAULT_GROUP_ID = 29 and a1.NODE_CODE = 'POWER' and a1.station_code_nims = a.station_code_nims) is null then 'Khong' else 'Co' end canhbaocodienkhac FROM NOCPROV4_APP.ACC_CABINET_FAULT_CUR a,nocprov4_app.acc_cabinet_type_detail c,nocprov4_app.acc_cabinet_type_detail c1 WHERE a.CABINET_TYPE_2 = c.ACC_CABINET_TYPE_DETAIL_ID(+) AND a.CABINET_TYPE_22 = c1.ACC_CABINET_TYPE_DETAIL_ID(+) AND upper(FAULT_NAME) like '%NHIET DO CAO%' AND END_TIME IS NULL AND ALARM_MONITOR_TYPE in (1,2,3,4,5,6) AND upper(a.LEVEL4_NAME) like '%" + str(dist_name) + "%' AND upper(a.DEPT3_CODE) like '%" + str(prvn_name) + "%'"
            else:
                if prvn_name in ['KV1', 'KV2', 'KV3']:
                    sql_qury = "SELECT a.STATION_CODE_NIMS Trạm, a.TYPE_STATION Tủ,a.LEVEL4_NAME HUYEN,a.DEPT3_CODE TINH,a.DEPT2_CODE ZONE,NVL(SUBSTR(regexp_substr(a.SAP_INFO,'Temperature:\s+" + '"(\S+)"' + "'),13),'Khong co thong tin') NHIETDO,c.ACC_CABINET_TYPE_DETAIL_NAME CABINET_TYPE,c1.ACC_CABINET_TYPE_DETAIL_NAME DETAIL,  case when (select max(a1.alarm_id)  from NOCPROV4_APP.ACC_CABINET_FAULT_CUR a1   where a1.fault_name like 'MAT DIEN%'  and a1.station_code_nims = a.station_code_nims) is null  then 'Khong' else 'Co' end  Matdien,  case when (select max(a1.alarm_id)  from NOCPROV4_APP.ACC_CABINET_FAULT_CUR a1   where a1.FAULT_GROUP_ID = 29 and a1.NODE_CODE = 'POWER'  and a1.station_code_nims = a.station_code_nims) is null  then 'Khong' else 'Co' end canhbaocodienkhac  FROM NOCPROV4_APP.ACC_CABINET_FAULT_CUR a,nocprov4_app.acc_cabinet_type_detail c,nocprov4_app.acc_cabinet_type_detail c1  WHERE a.CABINET_TYPE_2 = c.ACC_CABINET_TYPE_DETAIL_ID(+)  AND a.CABINET_TYPE_22 = c1.ACC_CABINET_TYPE_DETAIL_ID(+)  AND upper(FAULT_NAME) like '%NHIET DO CAO%'  AND END_TIME IS NULL AND ALARM_MONITOR_TYPE in (1,2,3,4,5,6)  AND upper(a.DEPT2_CODE) like '%" + str(prvn_name) + "%' "
                else:
                    sql_qury = "SELECT a.STATION_CODE_NIMS Trạm, a.TYPE_STATION Tủ,a.LEVEL4_NAME HUYEN,a.DEPT3_CODE TINH,NVL(SUBSTR(regexp_substr(a.NOTE,'Temperature:\s+" + '"(\S+)"' + "'),13),'Khong co thong tin') NHIETDO,c.ACC_CABINET_TYPE_DETAIL_NAME CABINET_TYPE,c1.ACC_CABINET_TYPE_DETAIL_NAME DETAIL, case when (select max(a1.alarm_id) from NOCPROV4_APP.ACC_CABINET_FAULT_CUR a1  where a1.fault_name like 'MAT DIEN%' and a1.station_code_nims = a.station_code_nims) is null then 'Khong' else 'Co' end  Matdien, case when (select max(a1.alarm_id) from NOCPROV4_APP.ACC_CABINET_FAULT_CUR a1  where a1.FAULT_GROUP_ID = 29 and a1.NODE_CODE = 'POWER' and a1.station_code_nims = a.station_code_nims) is null then 'Khong' else 'Co' end canhbaocodienkhac FROM NOCPROV4_APP.ACC_CABINET_FAULT_CUR a,nocprov4_app.acc_cabinet_type_detail c,nocprov4_app.acc_cabinet_type_detail c1 WHERE a.CABINET_TYPE_2 = c.ACC_CABINET_TYPE_DETAIL_ID(+) AND a.CABINET_TYPE_22 = c1.ACC_CABINET_TYPE_DETAIL_ID(+) AND upper(FAULT_NAME) like '%NHIET DO CAO%' AND END_TIME IS NULL AND ALARM_MONITOR_TYPE in (1,2,3,4,5,6) AND upper(a.DEPT3_CODE) like '%" + str(
                        prvn_name) + "%' "

            res_lst = list()
            conn, type_conn = self.get_conn()
            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_qury)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
        return res_lst

    def find_high_temp_bb_lst(self, prvn_name, dist_name):
        if prvn_name:
            chck_sql = check_regex_acc(prvn_name)
            if chck_sql and dist_name != '':
                chck_sql = check_regex_acc(dist_name)
        else:
            chck_sql = True
        res = 0
        res_lst = []
        if chck_sql:
            if prvn_name == 'KV1' or prvn_name == 'KV2' or prvn_name == 'KV3':
                sql_qury = "select xxx.STATION_CODE_NIMS MA_TRAM,xxx.CABINET_NAME MA_TU,xxx.LEVEL2_CODE ZONE,xxx.DEPT3_CODE TINH, xxx.LEVEL4_NAME HUYEN, yyy.ITEM_NAME STATION_TYPE,xxx.FAULT_NAME,xxx.TIME_START from ( select STATION_CODE_NIMS,CABINET_NAME,LEVEL2_CODE,DEPT3_CODE, LEVEL4_NAME, FAULT_NAME,TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss') TIME_START from nocprov4_app.ACC_CABINET_FAULT_CUR WHERE END_TIME IS NULL and FAULT_NAME LIKE '%NHIET DO%' )xxx,  ( select a.STATION_CODE,a.STATION_TYPE_ID,b.ITEM_ID,b.ITEM_NAME from nocprov4_app.CAT_STATION a,nocprov4_app.cat_item b WHERE a.STATION_TYPE_ID = b.ITEM_ID and a.STATION_TYPE_ID IN ('7043','7046','7050','7044','7058','7042','7032','7047','7056','7055','7061','7048','7052','7045','7054') )yyy WHERE xxx.STATION_CODE_NIMS =yyy.STATION_CODE and LEVEL2_CODE like '%" + str(prvn_name) + "%'   ORDER BY xxx.TIME_START DESC"
            elif dist_name != '':
                sql_qury = "select xxx.STATION_CODE_NIMS MA_TRAM,xxx.CABINET_NAME MA_TU,xxx.DEPT3_CODE TINH, xxx.LEVEL4_NAME HUYEN, yyy.ITEM_NAME STATION_TYPE,xxx.FAULT_NAME,xxx.TIME_START from ( select STATION_CODE_NIMS,CABINET_NAME,DEPT3_CODE, LEVEL4_NAME, FAULT_NAME,TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss') TIME_START from nocprov4_app.ACC_CABINET_FAULT_CUR WHERE END_TIME IS NULL and FAULT_NAME LIKE '%NHIET DO%' )xxx,  ( select a.STATION_CODE,a.STATION_TYPE_ID,b.ITEM_ID,b.ITEM_NAME from nocprov4_app.CAT_STATION a,nocprov4_app.cat_item b WHERE a.STATION_TYPE_ID = b.ITEM_ID and a.STATION_TYPE_ID IN ('7043','7046','7050','7044','7058','7042','7032','7047','7056','7055','7061','7048','7052','7045','7054') )yyy WHERE xxx.STATION_CODE_NIMS =yyy.STATION_CODE and DEPT3_CODE like '%" + str(prvn_name) + "%' and upper(LEVEL4_NAME) like '%" + str(dist_name) + "%' ORDER BY xxx.TIME_START DESC"
            else:
                sql_qury = "select xxx.STATION_CODE_NIMS MA_TRAM,xxx.CABINET_NAME MA_TU,xxx.DEPT3_CODE TINH, xxx.LEVEL4_NAME HUYEN, yyy.ITEM_NAME STATION_TYPE,xxx.FAULT_NAME,xxx.TIME_START from ( select STATION_CODE_NIMS,CABINET_NAME,DEPT3_CODE, LEVEL4_NAME, FAULT_NAME,TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss') TIME_START from nocprov4_app.ACC_CABINET_FAULT_CUR WHERE END_TIME IS NULL and FAULT_NAME LIKE '%NHIET DO%' )xxx,  ( select a.STATION_CODE,a.STATION_TYPE_ID,b.ITEM_ID,b.ITEM_NAME from nocprov4_app.CAT_STATION a,nocprov4_app.cat_item b WHERE a.STATION_TYPE_ID = b.ITEM_ID and a.STATION_TYPE_ID IN ('7043','7046','7050','7044','7058','7042','7032','7047','7056','7055','7061','7048','7052','7045','7054') )yyy WHERE xxx.STATION_CODE_NIMS =yyy.STATION_CODE and DEPT3_CODE like '%" + str(prvn_name) + "%' ORDER BY xxx.TIME_START DESC"

            res_lst = list()
            conn, type_conn = self.get_conn()
            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_qury)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
        return res_lst

    def find_open_door_bb_lst(self, prvn_name, dist_name):
        if prvn_name:
            chck_sql = check_regex_acc(prvn_name)
            if chck_sql and dist_name != '':
                chck_sql = check_regex_acc(dist_name)
        else:
            chck_sql = True
        res = 0
        res_lst = []
        if chck_sql:
            if prvn_name == 'KV1' or prvn_name == 'KV2' or prvn_name == 'KV3':
                sql_qury = "select xxx.STATION_CODE_NIMS MA_TRAM,xxx.CABINET_NAME MA_TU,xxx.LEVEL2_CODE ZONE,xxx.DEPT3_CODE TINH, xxx.LEVEL4_NAME HUYEN, yyy.ITEM_NAME STATION_TYPE,xxx.FAULT_NAME,xxx.TIME_START from ( select STATION_CODE_NIMS,CABINET_NAME,LEVEL2_CODE,DEPT3_CODE, LEVEL4_NAME, FAULT_NAME,TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss') TIME_START from nocprov4_app.ACC_CABINET_FAULT_CUR WHERE END_TIME IS NULL and FAULT_NAME LIKE '%MO CUA%' )xxx,  ( select a.STATION_CODE,a.STATION_TYPE_ID,b.ITEM_ID,b.ITEM_NAME from nocprov4_app.CAT_STATION a,nocprov4_app.cat_item b WHERE a.STATION_TYPE_ID = b.ITEM_ID and a.STATION_TYPE_ID IN ('7043','7046','7050','7044','7058','7042','7032','7047','7056','7055','7061','7048','7052','7045','7054') )yyy WHERE xxx.STATION_CODE_NIMS =yyy.STATION_CODE and LEVEL2_CODE like '%" + str(prvn_name) +"%'   ORDER BY xxx.TIME_START DESC"
            elif dist_name != '':
                sql_qury = "select xxx.STATION_CODE_NIMS MA_TRAM,xxx.CABINET_NAME MA_TU,xxx.DEPT3_CODE TINH, xxx.LEVEL4_NAME HUYEN, yyy.ITEM_NAME STATION_TYPE,xxx.FAULT_NAME,xxx.TIME_START from ( select STATION_CODE_NIMS,CABINET_NAME,DEPT3_CODE, LEVEL4_NAME, FAULT_NAME,TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss') TIME_START from nocprov4_app.ACC_CABINET_FAULT_CUR WHERE END_TIME IS NULL and FAULT_NAME LIKE '%MO CUA%' )xxx,  ( select a.STATION_CODE,a.STATION_TYPE_ID,b.ITEM_ID,b.ITEM_NAME from nocprov4_app.CAT_STATION a,nocprov4_app.cat_item b WHERE a.STATION_TYPE_ID = b.ITEM_ID and a.STATION_TYPE_ID IN ('7043','7046','7050','7044','7058','7042','7032','7047','7056','7055','7061','7048','7052','7045','7054') )yyy WHERE xxx.STATION_CODE_NIMS =yyy.STATION_CODE and DEPT3_CODE like '%" + str(prvn_name) +"%' and upper(LEVEL4_NAME) like '%" + str(dist_name) + "%' ORDER BY xxx.TIME_START DESC"
            else:
                sql_qury = "select xxx.STATION_CODE_NIMS MA_TRAM,xxx.CABINET_NAME MA_TU,xxx.DEPT3_CODE TINH, xxx.LEVEL4_NAME HUYEN, yyy.ITEM_NAME STATION_TYPE,xxx.FAULT_NAME,xxx.TIME_START from ( select STATION_CODE_NIMS,CABINET_NAME,DEPT3_CODE, LEVEL4_NAME, FAULT_NAME,TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss') TIME_START from nocprov4_app.ACC_CABINET_FAULT_CUR WHERE END_TIME IS NULL and FAULT_NAME LIKE '%MO CUA%' )xxx,  ( select a.STATION_CODE,a.STATION_TYPE_ID,b.ITEM_ID,b.ITEM_NAME from nocprov4_app.CAT_STATION a,nocprov4_app.cat_item b WHERE a.STATION_TYPE_ID = b.ITEM_ID and a.STATION_TYPE_ID IN ('7043','7046','7050','7044','7058','7042','7032','7047','7056','7055','7061','7048','7052','7045','7054') )yyy WHERE xxx.STATION_CODE_NIMS =yyy.STATION_CODE and DEPT3_CODE like '%" + str(prvn_name) +"%' ORDER BY xxx.TIME_START DESC"

            res_lst = list()
            conn, type_conn = self.get_conn()
            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_qury)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
        return res_lst

    def find_power_off_bb_lst(self, prvn_name, dist_name):
        if prvn_name:
            chck_sql = check_regex_acc(prvn_name)
            if chck_sql and dist_name != '':
                chck_sql = check_regex_acc(dist_name)
        else:
            chck_sql = True
        res = 0
        res_lst = []
        if chck_sql:
            if prvn_name == 'KV1' or prvn_name == 'KV2' or prvn_name == 'KV3':
                sql_qury = "select xxx.STATION_CODE_NIMS MA_TRAM,xxx.CABINET_NAME MA_TU,xxx.LEVEL2_CODE ZONE,xxx.DEPT3_CODE TINH, xxx.LEVEL4_NAME HUYEN, yyy.ITEM_NAME STATION_TYPE,xxx.FAULT_NAME,xxx.TIME_START from ( select STATION_CODE_NIMS,CABINET_NAME,LEVEL2_CODE,DEPT3_CODE, LEVEL4_NAME, FAULT_NAME,TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss') TIME_START from nocprov4_app.ACC_CABINET_FAULT_CUR WHERE END_TIME IS NULL and FAULT_NAME LIKE '%MAT DIEN%' )xxx,  ( select a.STATION_CODE,a.STATION_TYPE_ID,b.ITEM_ID,b.ITEM_NAME from nocprov4_app.CAT_STATION a,nocprov4_app.cat_item b WHERE a.STATION_TYPE_ID = b.ITEM_ID and a.STATION_TYPE_ID IN ('7043','7046','7050','7044','7058','7042','7032','7047','7056','7055','7061','7048','7052','7045','7054') )yyy WHERE xxx.STATION_CODE_NIMS =yyy.STATION_CODE and LEVEL2_CODE like '%" + str(prvn_name) +"%' ORDER BY xxx.TIME_START DESC"
            elif dist_name != '':
                sql_qury = "select xxx.STATION_CODE_NIMS MA_TRAM,xxx.CABINET_NAME MA_TU,xxx.DEPT3_CODE TINH, xxx.LEVEL4_NAME HUYEN, yyy.ITEM_NAME STATION_TYPE,xxx.FAULT_NAME,xxx.TIME_START from ( select STATION_CODE_NIMS,CABINET_NAME,DEPT3_CODE, LEVEL4_NAME, FAULT_NAME,TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss') TIME_START from nocprov4_app.ACC_CABINET_FAULT_CUR WHERE END_TIME IS NULL and FAULT_NAME LIKE '%MAT DIEN%' )xxx,  ( select a.STATION_CODE,a.STATION_TYPE_ID,b.ITEM_ID,b.ITEM_NAME from nocprov4_app.CAT_STATION a,nocprov4_app.cat_item b WHERE a.STATION_TYPE_ID = b.ITEM_ID and a.STATION_TYPE_ID IN ('7043','7046','7050','7044','7058','7042','7032','7047','7056','7055','7061','7048','7052','7045','7054') )yyy WHERE xxx.STATION_CODE_NIMS =yyy.STATION_CODE and DEPT3_CODE like '%" + str(prvn_name) +"%' and upper(LEVEL4_NAME) like '%" + str(dist_name) + "%' ORDER BY xxx.TIME_START DESC"
            else:
                sql_qury = "select xxx.STATION_CODE_NIMS MA_TRAM,xxx.CABINET_NAME MA_TU,xxx.DEPT3_CODE TINH, xxx.LEVEL4_NAME HUYEN, yyy.ITEM_NAME STATION_TYPE,xxx.FAULT_NAME,xxx.TIME_START from ( select STATION_CODE_NIMS,CABINET_NAME,DEPT3_CODE, LEVEL4_NAME, FAULT_NAME,TO_DATE(OCCURED_TIME,'yyyy/mm/dd/ hh24miss') TIME_START from nocprov4_app.ACC_CABINET_FAULT_CUR WHERE END_TIME IS NULL and FAULT_NAME LIKE '%MAT DIEN%' )xxx,  ( select a.STATION_CODE,a.STATION_TYPE_ID,b.ITEM_ID,b.ITEM_NAME from nocprov4_app.CAT_STATION a,nocprov4_app.cat_item b WHERE a.STATION_TYPE_ID = b.ITEM_ID and a.STATION_TYPE_ID IN ('7043','7046','7050','7044','7058','7042','7032','7047','7056','7055','7061','7048','7052','7045','7054') )yyy WHERE xxx.STATION_CODE_NIMS =yyy.STATION_CODE and DEPT3_CODE like '%" + str(prvn_name) +"%'  ORDER BY xxx.TIME_START DESC"

            res_lst = list()
            conn, type_conn = self.get_conn()
            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_qury)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
        return res_lst

    def find_node_acc_down(self, node_acc_lst):
        # co hai truong hop. Neu node acc lst dau vao la string thi giu nguyen. Neu la list thi tach ra va chuyen thanh str
        if isinstance(node_acc_lst, list):
            node_acc_lst_str = convert_lst_to_str(node_acc_lst, ",", "'")
        else:
            node_acc_lst_str = str(node_acc_lst)
        if node_acc_lst_str and node_acc_lst_str != '':
            chck_sql = check_regex_acc(node_acc_lst_str)
        else:
            chck_sql = False
        res = 0
        res_lst = []

        if chck_sql:
            sql_qury = "select DEVICE_NAME as NAME from NOCPROV4_IP.IP_ALARM_CURRENT_ACCESS WHERE event_type ='Node down' and device_name IN (" + str(node_acc_lst_str) + ")"

            res_lst = list()
            conn, type_conn = self.get_conn()
            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_qury)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
        return res_lst

    def find_node_core_down(self, node_acc_lst):
        # co hai truong hop. Neu node acc lst dau vao la string thi giu nguyen. Neu la list thi tach ra va chuyen thanh str
        if isinstance(node_acc_lst, list):
            node_acc_lst_str = convert_lst_to_str(node_acc_lst, ",", "'")
        else:
            node_acc_lst_str = str(node_acc_lst)
        if node_acc_lst_str and node_acc_lst_str != '':
            chck_sql = check_regex_acc(node_acc_lst_str)
        else:
            chck_sql = False
        res = 0
        res_lst = []

        if chck_sql:
            sql_qury = "select DEVICE_NAME AS NAME from NOCPROV4_IP.IP_ALARM_CURRENT_CORE WHERE EVENT_TYPE = 'Node down' and device_name IN (" + str(node_acc_lst_str) + ")"

            res_lst = list()
            conn, type_conn = self.get_conn()
            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_qury)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return list()
                finally:
                    cursor.close()
                    conn.close()
        return res_lst

if __name__ == '__main__':
    #yes_date = get_date_yesterday_format_ipms()
    #bef_yes_date = get_date_before_yesterday_format_ipms()
    '''
    day_lst = ['2019-05-01', '2019-05-02', '2019-05-03', '2019-05-04', '2019-05-05', '2019-05-06', '2019-05-07', '2019-05-08', '2019-05-09', '2019-05-10',
               '2019-05-11', '2019-05-12', '2019-05-13', '2019-05-14', '2019-05-15', '2019-05-16', '2019-05-17', '2019-05-18', '2019-05-19', '2019-05-20',
               '2019-05-21', '2019-05-22', '2019-05-23', '2019-05-24', '2019-05-25', '2019-05-26', '2019-05-27']
    n = len(day_lst)
    for i in range(0, n-1):

        _tbl = TblIpSyslogCoreElk(day_lst[i], day_lst[i+1])
        _tbl_new_lst = _tbl.get_new_lst()
        _tbl_clear_lst = _tbl.get_clr_lst()
        _tbl_his_lst = _tbl.get_his_lst()
        if _tbl_new_lst:
            _tbl.inst_els('NEW', _tbl_new_lst)
        if _tbl_clear_lst:
            _tbl.inst_els('CLEAR', _tbl_clear_lst)
        if _tbl_his_lst:
            _tbl.inst_els('HISTORY', _tbl_his_lst)
    '''
    node_lst = ['QNM0538SRT01', 'QNM0408SRT01']
    _obj = TblNocproImpl(None)
    res = _obj.find_high_temp_lst('KV1', '')
    print(res)
