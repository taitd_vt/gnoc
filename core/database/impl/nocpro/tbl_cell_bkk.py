#!/D:\python36 new
# -*- coding: utf8 -*-
import cx_Oracle

from core.helpers.stringhelpers import check_regex_acc
from core.helpers.date_helpers import get_date_now, convert_date_str_to_date_obj_ipms
from config import Development
import os
import sys

config = Development()
SERVER_NOCPRO = config.__getattribute__('SERVER_NOCPRO')
SERVER_NOCPRO_PORT = config.__getattribute__('SERVER_NOCPRO_PORT')
SERVER_NOCPRO_SERVICE_NAME = config.__getattribute__('SERVER_NOCPRO_SERVICE_NAME')
SERVER_NOCPRO_USERNAME = config.__getattribute__('SERVER_NOCPRO_USERNAME')
SERVER_NOCPRO_PASSWORD = config.__getattribute__('SERVER_NOCPRO_PASSWORD')


class TblCellBkk:
    def __init__(self, cell_name, reason, document, status, expire_date):
        self.cell_name = cell_name
        self.reason = reason
        self.document = document
        self.status = status
        self.expire_date = expire_date

    @staticmethod
    def get_conn():
        try:
            dsn_str = cx_Oracle.makedsn(SERVER_NOCPRO, SERVER_NOCPRO_PORT, service_name=SERVER_NOCPRO_SERVICE_NAME)
            conn = cx_Oracle.connect(user=SERVER_NOCPRO_USERNAME, password=SERVER_NOCPRO_PASSWORD,
                                     dsn=dsn_str,encoding='UTF-8', nencoding='UTF-8')

        except Exception as err:
            print(err)
            return None
        return conn

    def get_total(self, key_srch):
        # check key_srch co ma doc khong:
        chck_sql = check_regex_acc(key_srch)
        res = 0
        if chck_sql:
            if key_srch:
                sql_orc = "SELECT COUNT(*) from cellbkk WHERE " \
                          "CELL_NAME LIKE '%" + key_srch + "%' " \
                                                           "OR REASON LIKE '%" + key_srch + "%' " \
                                                           "OR DOCUMENT LIKE '%" + key_srch + "%' "

            else:
                sql_orc = "SELECT COUNT(*) FROM cellbkk"

            conn = self.get_conn()
            if conn:
                cursor = conn.cursor()

                try:
                    cursor.execute(sql_orc)
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    if res_lst:
                        if 'COUNT(*)' in res_lst[0]:
                            res = res_lst[0]['COUNT(*)']

                except Exception as err:
                    print(err)
                    return res
                finally:
                    cursor.close()
                    conn.close()
        return res

    def get_cell_lst(self, key_srch, page_num, page_size):
        chck_sql = check_regex_acc(key_srch)
        res_lst = []
        try:
            if chck_sql and page_num > 0 and page_size > 0:
                row_num_high = (page_num * page_size) + 1
                row_num_low = ((page_num - 1) * page_size) + 1
                if key_srch:
                    sql_orc = "SELECT * FROM (SELECT a.*, rownum r__ FROM (SELECT * FROM cellbkk " \
                              "WHERE CELL_NAME LIKE '%" + key_srch + "%' OR REASON LIKE '%" + key_srch + "%' " \
                              "OR DOCUMENT LIKE '%" + key_srch + "%' ) a " \
                              "WHERE rownum < " + str(row_num_high) + ") WHERE r__ >= (" + str(row_num_low) + ")"
                else:
                    sql_orc = "SELECT * FROM (SELECT a.*, rownum r__ FROM (SELECT * FROM cellbkk ) a " \
                              "WHERE rownum < " + str(row_num_high) + ") WHERE r__ >= (" + str(row_num_low) + ")"
                conn = self.get_conn()
                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        conn.close()
                        return res_lst
        except Exception as err:
            print('Error %s when get Cell list' % err)
            return res_lst

    def get_cell_exact_lst(self, key_srch, page_num, page_size):
        chck_sql = check_regex_acc(key_srch)
        res_lst = []
        try:
            if chck_sql and page_num > 0 and page_size > 0:
                row_num_high = (page_num * page_size) + 1
                row_num_low = ((page_num - 1) * page_size) + 1
                if key_srch:
                    sql_orc = "SELECT * FROM (SELECT a.*, rownum r__ FROM (SELECT * FROM cellbkk " \
                              "WHERE CELL_NAME = :key_srch OR REASON = :key_srch " \
                              "OR DOCUMENT = :key_srch ) a " \
                              "WHERE rownum < " + str(row_num_high) + ") WHERE r__ >= (" + str(row_num_low) + ")"
                else:
                    sql_orc = "SELECT * FROM (SELECT a.*, rownum r__ FROM (SELECT * FROM cellbkk ) a " \
                              "WHERE rownum < " + str(row_num_high) + ") WHERE r__ >= (" + str(row_num_low) + ")"
                conn = self.get_conn()
                if conn:
                    cursor = conn.cursor()

                    try:
                        cursor.execute(sql_orc, key_srch=key_srch)
                        data = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_lst = [dict(zip(col_name_lst, row)) for row in data]

                    except Exception as err:
                        print(err)
                        return res_lst
                    finally:
                        cursor.close()
                        conn.close()
                        return res_lst
        except Exception as err:
            print('Error %s when get Cell list' % err)
            return res_lst

    def update(self, cell_name_new, reason, document, status, exp_date):
        chck_sql = check_regex_acc(cell_name_new)
        res_lst = True
        # Mac dinh kieu exp_date la YYYY-mm-dd HH:ii:ss
        expire_date_new = convert_date_str_to_date_obj_ipms(exp_date)
        if chck_sql and expire_date_new:

            sql_orc = "UPDATE cellbkk SET CELL_NAME = :cell_name_new, DOCUMENT = :document_new," \
                      "REASON = :reason_new, STATUS = :status_new, EXPIRE_DATE = : expire_date_new " \
                      "WHERE CELL_NAME = :cell_name_old "

            conn = self.get_conn()
            if conn:
                cursor = conn.cursor()
                try:
                    # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                    cursor.execute(sql_orc, cell_name_old=self.cell_name,
                                   cell_name_new=cell_name_new,
                                   reason_new=reason,
                                   document_new=document,
                                   status_new=status,
                                   expire_date_new=expire_date_new)
                    conn.commit()

                except Exception as err:
                    print(err)
                    return False
                finally:
                    cursor.close()
                    conn.close()
                    return res_lst

    def find(self, key_srch):
        res_lst = []
        conn = self.get_conn()
        cbn_bkk_obj = None

        if conn:
                cursor = conn.cursor()
                try:
                    # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                    cursor.execute("select * from  cellbkk where cell_name = :key_srch", key_srch=key_srch)

                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    # do mac dinh chi co 1 object nen return luon ra vay :D
                    if res_lst:
                        for x in res_lst:
                            if 'CELL_NAME' in x and 'REASON' in x and 'DOCUMENT' in x and 'STATUS' in x and 'EXPIRE_DATE' in x :

                                cbn_bkk_obj = TblCellBkk(x['CELL_NAME'], x['REASON'],
                                                        x['DOCUMENT'], x['STATUS'],
                                                        x['EXPIRE_DATE'])

                except Exception as err:
                    print(err)
                    return None
                finally:
                    cursor.close()
                    conn.close()
                    return cbn_bkk_obj

    def find_like(self, key_srch):
        res_bkk_fnd_lst = []
        conn = self.get_conn()
        cbn_bkk_obj = None

        if conn:
                cursor = conn.cursor()
                try:
                    # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                    cursor.execute("select * from  cellbkk where cell_name like '%" + key_srch + "%' or document like '%" + key_srch + "%' or reason like '%" + key_srch + "%'")

                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]
                    # do mac dinh chi co 1 object nen return luon ra vay :D
                    if res_lst:
                        for x in res_lst:
                            if 'CELL_NAME' in x and 'REASON' in x and 'DOCUMENT' in x \
                                    and 'STATUS' in x and 'EXPIRE_DATE' in x:

                                res_bkk_fnd_lst.append(x)

                except Exception as err:
                    print(err)
                    return res_bkk_fnd_lst
                finally:
                    cursor.close()
                    conn.close()
                    return res_bkk_fnd_lst

    def get_lst(self):
        res_lst = []
        conn = self.get_conn()
        if conn:
                cursor = conn.cursor()
                try:
                    # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                    cursor.execute("select * from  cellbkk ")
                    data = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in data]

                except Exception as err:
                    print(err)
                    return res_lst
                finally:
                    cursor.close()
                    conn.close()
                    return res_lst

    def insert(self):
        res = True
        conn = self.get_conn()
        if conn:
                cursor = conn.cursor()
                try:
                    # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                    cursor.prepare("INSERT into cellbkk(CELL_NAME, DOCUMENT, REASON, STATUS, EXPIRE_DATE) "
                                   "VALUES (:cbn_nme, :docm, :resn, :stat, :exp_date)")
                    obj_dct = dict(cbn_nme=self.cell_name, docm=self.document, resn=self.reason,
                                   exp_date=self.expire_date, stat=self.status)
                    cursor.execute(None, obj_dct)
                    conn.commit()

                except Exception as err:
                    print(err)
                    return False
                finally:
                    cursor.close()
                    conn.close()
                    return res

    def delete(self):
        res = True
        conn = self.get_conn()
        if conn:
                cursor = conn.cursor()
                try:
                    # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                    cursor.execute("DELETE from cellbkk WHERE cell_name = :cell_name",
                                   cell_name=self.cell_name)
                    conn.commit()

                except Exception as err:
                    print(err)
                    return False
                finally:
                    cursor.close()
                    conn.close()
                    return res

if __name__ == '__main__':
    time_now = get_date_now()
    str123 = 'TTKT đề nghị PKTHT xác định thời gian backup của acquy và thống nhất'
    cell = TblCellBkk('HNI123456', str123, '100', 1, time_now)
    res_get = cell.find(cell.cell_name)
    if not res_get:
        res_ins = cell.insert()

    res_get = cell.find(cell.cell_name)
    if res_get:
        res_update = cell.update('HNI123456PPP', '101', 'Tester2', 1, '2019-09-20 00:01:02')
        res_get = cell.find('HNI123456PPP')
        if res_get:
            cell.cell_name = 'HNI123456PPP'
            res_del = cell.delete()

    # cell_lst = cell.edit_cell('BKN018P', 'BKN018Pz', str123, 0, 1, '19-SEP-19')
    #cell_fnd_lst = cell.insert()

