__author__ = 'VTN-DHML-DHIP10'
import cx_Oracle
from config import Development
config = Development()
SERVER_NOCPRO = config.__getattribute__('SERVER_NOCPRO')
SERVER_NOCPRO_PORT = config.__getattribute__('SERVER_NOCPRO_PORT')
SERVER_NOCPRO_SERVICE_NAME = config.__getattribute__('SERVER_NOCPRO_SERVICE_NAME')
SERVER_NOCPRO_USERNAME = config.__getattribute__('SERVER_NOCPRO_USERNAME')
SERVER_NOCPRO_PASSWORD = config.__getattribute__('SERVER_NOCPRO_PASSWORD')


class TblFreeTask:
    def __init__(self, time_bgn, time_end):
        self.time_bgn = time_bgn
        self.time_end = time_end

    def get_conn(self):
        try:
            dsn_str = cx_Oracle.makedsn(SERVER_NOCPRO, SERVER_NOCPRO_PORT, service_name=SERVER_NOCPRO_SERVICE_NAME)
            conn = cx_Oracle.connect(user=SERVER_NOCPRO_USERNAME, password=SERVER_NOCPRO_PASSWORD, dsn=dsn_str)

        except Exception as err:
            print(err)
            return None
        return conn

    def get_total_secondary_ip(self):
        tot_alm = 0
        sql_qury = "select count(*)  from NOCPROV4_IP.IP_ALARM_SYSLOG_HISTORY_CORE WHERE " \
                   "date_time >=to_date('" + self.time_bgn + "','yyyy-mm-dd') and  date_time<to_date('" + self.time_end + \
                   "','yyyy-mm-dd') and table_syslog in ('log_dcn', 'log_ipbn', 'log_mpbn', 'log_ps') " \
                   "and kv_code in ('KV1', 'KV2', 'KV3') and alarm_level in ('CRITICAL', 'MAJOR', 'MINOR') " \
                   "and end_time - date_time > 0.09"

        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                if res_lst:
                    if len(res_lst) == 1:
                        tot_alm = res_lst[0]['COUNT(*)']
            except Exception as err:
                print(err)
                return tot_alm
            finally:
                cursor.close()
                conn.close()
        return tot_alm

    def get_total_first_ip(self):
        tot_alm = 0
        sql_qury = "select count(*)  from NOCPROV4_IP.IP_ALARM_SYSLOG_HISTORY_CORE WHERE " \
                   "date_time >=to_date('" + self.time_bgn + "','yyyy-mm-dd') and  date_time<to_date('" + self.time_end + \
                   "','yyyy-mm-dd') and table_syslog in ('log_dcn', 'log_ipbn', 'log_mpbn', 'log_ps') " \
                   "and kv_code in ('KV1', 'KV2', 'KV3') and alarm_level not in ('CRITICAL', 'MAJOR', 'MINOR') " \
                   "and end_time - date_time > 0.09"

        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                if res_lst:
                    if len(res_lst) == 1:
                        tot_alm = res_lst[0]['COUNT(*)']
            except Exception as err:
                print(err)
                return tot_alm
            finally:
                cursor.close()
                conn.close()
        return tot_alm

    def get_total_alm_tbl_syslog_ip(self, tbl_syslog):
        tot_alm = 0
        sql_qury = "select count(*)  from NOCPROV4_IP.IP_ALARM_SYSLOG_HISTORY_CORE WHERE " \
                   "date_time >=to_date('" + self.time_bgn + "','yyyy-mm-dd') and  date_time<to_date('" + self.time_end + \
                   "','yyyy-mm-dd') and table_syslog = '" + tbl_syslog + "' " \
                   "and kv_code in ('KV1', 'KV2', 'KV3') and alarm_level in ('CRITICAL', 'MAJOR', 'MINOR') " \
                   "and end_time - date_time > 0.09"

        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                if res_lst:
                    if len(res_lst) == 1:
                        tot_alm = res_lst[0]['COUNT(*)']
            except Exception as err:
                print(err)
                return tot_alm
            finally:
                cursor.close()
                conn.close()
        return tot_alm

    def get_total_alm_cbs_ip(self):
        tot_alm = 0
        sql_qury = "select count(*)  from NOCPROV4_IP.IP_ALARM_SYSLOG_HISTORY_CORE WHERE " \
                   "date_time >=to_date('" + self.time_bgn + "','yyyy-mm-dd') and  date_time<to_date('" + self.time_end + \
                   "','yyyy-mm-dd') and table_syslog = 'log_nocpro'  " \
                   "and kv_code in ('KV1', 'KV2', 'KV3') and alarm_level in ('CRITICAL', 'MAJOR', 'MINOR') " \
                   "and end_time - date_time > 0.09 and group_name_cbs like '%FOIP%'"

        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                if res_lst:
                    if len(res_lst) == 1:
                        tot_alm = res_lst[0]['COUNT(*)']
            except Exception as err:
                print(err)
                return tot_alm
            finally:
                cursor.close()
                conn.close()
        return tot_alm

    def get_top_ip_alarm(self):
        sql_qury = "select alarm_name, count(alarm_name) as num from NOCPROV4_IP.IP_ALARM_SYSLOG_HISTORY_CORE WHERE " \
                   "date_time >=to_date('" + self.time_bgn + "','yyyy-mm-dd') and  date_time<to_date('" + self.time_end + \
                   "','yyyy-mm-dd') and table_syslog in ('log_dcn', 'log_ipbn', 'log_mpbn', 'log_ps') " \
                   "and kv_code in ('KV1', 'KV2', 'KV3') and alarm_level in ('CRITICAL', 'MAJOR', 'MINOR') " \
                   "and end_time - date_time > 0.09" \
                   "group by alarm_name order by num desc fetch first 20 rows only"
        res_lst = list()
        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                for x in res_lst:
                    # them message
                    if 'ALARM_NAME' in x:
                        alrm_name = x['ALARM_NAME']
                        sql_mess = "select message from NOCPROV4_IP.IP_ALARM_SYSLOG_HISTORY_CORE WHERE " \
                                   "alarm_name in('" + alrm_name + "') and date_time >= to_date('" + self.time_bgn + "', 'yyyy-mm-dd') and  date_time < to_date('" + self.time_end + "','yyyy-mm-dd') " \
                                   "and table_syslog in ('log_dcn', 'log_ipbn', 'log_mpbn', 'log_ps') " \
                                   "and alarm_level in ('CRITICAL', 'MAJOR', 'MINOR')and end_time - date_time > 0.09" \
                                   "group by (alarm_name, message) fetch first 1 rows only"
                        cursor.execute(sql_mess)
                        data_mess = cursor.fetchall()
                        col_name_lst = [x[0] for x in cursor.description]
                        res_mess_lst = [dict(zip(col_name_lst, row)) for row in data_mess]
                        if res_mess_lst:
                            if isinstance(res_mess_lst, list):
                                if 'MESSAGE' in res_mess_lst[0]:
                                    msg = res_mess_lst[0]['MESSAGE']
                                    x['MESSAGE'] = msg

            except Exception as err:
                print(err)
                return list()
            finally:
                cursor.close()
                conn.close()
        return res_lst

    def get_top_node_alarm_in_tbl_syslog(self, tbl_sys):
        sql_qury = "select device_name, count(device_name) as num from NOCPROV4_IP.IP_ALARM_SYSLOG_HISTORY_CORE WHERE " \
                   "date_time >=to_date('" + self.time_bgn + "','yyyy-mm-dd') and  date_time<to_date('" + self.time_end + \
                   "','yyyy-mm-dd') and table_syslog = '" + tbl_sys + "' " \
                   "and kv_code in ('KV1', 'KV2', 'KV3') and alarm_level in ('CRITICAL', 'MAJOR', 'MINOR') " \
                   "and end_time - date_time > 0.09" \
                   "group by device_name order by num desc fetch first 10 rows only"
        res_lst = list()
        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                for x in res_lst:
                    # them ALARM lap lai nhieu
                    if 'DEVICE_NAME' in x:
                        dev_name = x['DEVICE_NAME']
                        sql_alm = "select  alarm_name, count(alarm_name) as num from NOCPROV4_IP.IP_ALARM_SYSLOG_HISTORY_CORE WHERE " \
                                   "DEVICE_NAME = '" + dev_name + "' and date_time >= to_date('" + self.time_bgn + "', 'yyyy-mm-dd') and  date_time < to_date('" + self.time_end + "','yyyy-mm-dd') " \
                                   "and table_syslog = '" + tbl_sys + "' " \
                                   "and alarm_level in ('CRITICAL', 'MAJOR', 'MINOR')and end_time - date_time > 0.09" \
                                   " group by (alarm_name) order by num desc fetch first 5 rows only"

                        cursor.execute(sql_alm)
                        data_alm = cursor.fetchall()
                        col_alm_lst = [x[0] for x in cursor.description]
                        res_alm_lst = [dict(zip(col_alm_lst, row)) for row in data_alm]
                        if res_alm_lst:
                            if isinstance(res_alm_lst, list):
                                if 'ALARM_NAME' in res_alm_lst[0]:
                                    alm_nme = res_alm_lst[0]['ALARM_NAME']
                                    x['ALARM_NAME'] = alm_nme
                                    # Them Message
                                    sql_mess = "select message from NOCPROV4_IP.IP_ALARM_SYSLOG_HISTORY_CORE WHERE " \
                                               "alarm_name in('" + alm_nme + "') and " \
                                               "date_time >= to_date('" + self.time_bgn + "', 'yyyy-mm-dd') and  " \
                                               "date_time < to_date('" + self.time_end + "','yyyy-mm-dd') and " \
                                               "table_syslog = '" + tbl_sys + "' and alarm_level in ('CRITICAL', 'MAJOR', 'MINOR') and " \
                                               "end_time - date_time > 0.09 and " \
                                               "DEVICE_NAME = '" + dev_name + "' group by (alarm_name, message) fetch first 1 rows only"

                                    cursor.execute(sql_mess)
                                    data_mess = cursor.fetchall()
                                    col_name_lst = [x[0] for x in cursor.description]
                                    res_mess_lst = [dict(zip(col_name_lst, row)) for row in data_mess]
                                    if res_mess_lst:
                                        if isinstance(res_mess_lst, list):
                                            if 'MESSAGE' in res_mess_lst[0]:
                                                msg = res_mess_lst[0]['MESSAGE']
                                                x['MESSAGE'] = msg

            except Exception as err:
                print(err)
                return list()
            finally:
                cursor.close()
                conn.close()
        return res_lst

    def get_top_cbs_ip(self):
        res_lst = []
        sql_qury = "select  alarm_name, device_name, network_type_code, kv_code, count(alarm_name) as num " \
                   "from NOCPROV4_IP.IP_ALARM_SYSLOG_HISTORY_CORE WHERE "\
                   "date_time >= to_date('" + self.time_bgn + "','yyyy-mm-dd') and  " \
                   "date_time < to_date('" + self.time_end + "','yyyy-mm-dd') and " \
                   "table_syslog = 'log_nocpro' and alarm_level in ('CRITICAL', 'MAJOR', 'MINOR') and " \
                   "end_time - date_time > 0.09 and group_name_cbs like '%FOIP%' group by " \
                   "alarm_name, device_name, network_type_code, kv_code order by num desc fetch first 30 rows only"

        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]

            except Exception as err:
                print(err)
                return res_lst
            finally:
                cursor.close()
                conn.close()
        return res_lst

    def get_top_cbs_intf_ip(self):
        res_lst = []
        sql_qury = "select alarm_name, device_name, network_type_code, kv_code, port, description_cbs, count(alarm_name) as num " \
                   "from NOCPROV4_IP.IP_ALARM_SYSLOG_HISTORY_CORE WHERE "\
                   "date_time >= to_date('" + self.time_bgn + "','yyyy-mm-dd') and  " \
                   "date_time < to_date('" + self.time_end + "','yyyy-mm-dd') and " \
                   "table_syslog = 'log_nocpro' and alarm_level in ('CRITICAL', 'MAJOR', 'MINOR') and " \
                   "end_time - date_time > 0.09 and group_name_cbs like '%FOIP%' group by " \
                   "alarm_name, device_name, network_type_code, kv_code, port, description_cbs " \
                   "order by num desc fetch first 50 rows only"

        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]

            except Exception as err:
                print(err)
                return res_lst
            finally:
                cursor.close()
                conn.close()
        return res_lst


if __name__ == '__main__':
    tbl = TblFreeTask('2019-06-10', '2019-06-11')
    num_tot_sec = tbl.get_total_secondary_ip()
    num_tot_pri = tbl.get_total_first_ip()

    num_tot_dcn = tbl.get_total_alm_tbl_syslog_ip('log_dcn')
    tot_dcn_lst = tbl.get_top_node_alarm_in_tbl_syslog('log_dcn')

    num_tot_ipbn = tbl.get_total_alm_tbl_syslog_ip('log_ipbn')
    tot_ipbn_lst = tbl.get_top_node_alarm_in_tbl_syslog('log_ipbn')

    num_tot_mpbn = tbl.get_total_alm_tbl_syslog_ip('log_mpbn')
    tot_mpbcn_lst = tbl.get_top_node_alarm_in_tbl_syslog('log_mpbn')

    num_tot_ps = tbl.get_total_alm_tbl_syslog_ip('log_ps')
    tot_ps_lst = tbl.get_top_node_alarm_in_tbl_syslog('log_ps')

