__author__ = 'VTN-DHML-DHIP10'
from config import Development
import requests
import json
from core.helpers.api_helpers import api_delete_retry, api_get_retry, api_post_retry, api_put_retry, api_patch_retry
config = Development()
API_SPRING_IPMS_SERVER = config.__getattribute__('API_SPRING_IPMS_SERVER')
API_SPRING_IPMS_USER = config.__getattribute__('API_SPRING_IPMS_USER')
API_SPRING_IPMS_PASS = config.__getattribute__('API_SPRING_IPMS_PASS')
API_SPRING_IPMS_PORT = config.__getattribute__('API_SPRING_IPMS_PORT')
API_SPRING_IPMS_SERVER_TENANT = config.__getattribute__('API_SPRING_IPMS_SERVER_TENANT')


class TblBrasHsiInfoImpl:
    def __init__(self, data):
        if data:
            self.__dict__ = data

    def get(self, id):

        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/bras_hsi_info/id=" + str(id)
        headers_json = {"X-TENANT-ID": API_SPRING_IPMS_SERVER_TENANT, "username": API_SPRING_IPMS_USER,
                        "password": API_SPRING_IPMS_PASS,
                        "Content-Type": "application/json"}
        # r = requests.get(url_api, headers=headers_json)
        r = api_get_retry(url_api, headers_json)
        if r:
            if r.ok:
                result_json = json.loads(r.text)
                return result_json
            else:
                return dict()

    def list(self):
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/bras_hsi_info/"
        headers_json = {"X-TENANT-ID": API_SPRING_IPMS_SERVER_TENANT, "username": API_SPRING_IPMS_USER,
                        "password": API_SPRING_IPMS_PASS,
                        "Content-Type": "application/json"}
        # r = requests.get(url_api, headers=headers_json)
        r = api_get_retry(url_api, headers_json)
        if r:
            if r.ok:
                result_json = json.loads(r.text)
                return result_json
            else:
                return list()

    def put(self, id_ring, **kwargs):
        result = False
        if 'id_bras' not in kwargs:
            kwargs['id_bras'] = id_ring
        data_json = json.dumps(kwargs)

        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/bras_hsi_info/id=" + str(id_ring)
        headers_json = {"X-TENANT-ID": API_SPRING_IPMS_SERVER_TENANT, "username": API_SPRING_IPMS_USER,
                        "password": API_SPRING_IPMS_PASS,
                        "Content-Type": "application/json"}
        # r = requests.put(url_api, headers=headers_json, data=data_json)
        r = api_put_retry(url_api, headers_json, data_json)
        if r:
            if r.ok:
                result_req = r.text
                if result_req == '':
                    return True

            else:
                return result

    def search(self, **kwargs):

        result = ''
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/bras_hsi_info/"
        headers_json = {"X-TENANT-ID": API_SPRING_IPMS_SERVER_TENANT, "username": API_SPRING_IPMS_USER,
                        "password": API_SPRING_IPMS_PASS,
                        "Content-Type": "application/json"}
        if 'id' in kwargs:
            id_bras = kwargs['id']
            url_api += 'searchIdVipaBras=' + str(id_bras)
            # r = requests.get(url_api, headers=headers_json)
            r = api_get_retry(url_api, headers_json)
            if r:

                if r.ok:
                    result_req = json.loads(r.text)
                    return result_req

        else:

            if 'bras_master_code' in kwargs:
                url_api += 'searchBrasPortVlan=' + kwargs['bras_master_code']
                if 'port_bras_master' in kwargs:
                    port_str = kwargs['port_bras_master']
                    # replace / thanh _ de truyen file cho de
                    port_str = port_str.replace("/", "_")

                    url_api += '/searchPort=' + port_str
                    if 'vlan_bras_master' in kwargs:
                        url_api += '/searchVlan=' + str(kwargs['vlan_bras_master'])
            elif 'bras_backup_code' in kwargs:
                url_api += 'searchBrasPortVlan=' + kwargs['bras_backup_code']
                if 'port_bras_backup' in kwargs:
                    port_str = kwargs['port_bras_backup']
                    # replace / thanh _ de truyen file cho de
                    port_str = port_str.replace("/", "_")
                    url_api += '/searchPort=' + port_str
                    if 'vlan_bras_backup' in kwargs:
                        url_api += '/searchVlan=' + str(kwargs['vlan_bras_backup'])

            else:
                return result

            # r = requests.get(url_api, headers=headers_json)
            r = api_get_retry(url_api, headers_json)
            if r:
                if r.ok:
                    result_req = json.loads(r.text)
                    return result_req

        return result

    def post(self, **kwargs):

        result = False
        data_json = json.dumps(kwargs)
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/bras_hsi_info/"
        headers_json = {"X-TENANT-ID": API_SPRING_IPMS_SERVER_TENANT, "username": API_SPRING_IPMS_USER,
                        "password": API_SPRING_IPMS_PASS,
                        "Content-Type": "application/json"}
        # do Thang BSI HSI info co id nen check bang no luon

        check_exist = self.search(**kwargs)
        if not check_exist:

            # r = requests.post(url_api, headers=headers_json, data=data_json)
            r = api_post_retry(url_api, headers_json, data_json)
            if r:
                if r.ok:
                    result_req = r.text
                    if result_req == '':
                        #print('Successful insert BRas HSI info ' + str(data_json))
                        return True
        else:
            # da ton tai roi update thoi
            result_check = check_exist
            if len(result_check) > 1:
                # print('Error insert BRAS HSI infobecause two many result search ' + str(kwargs))
                pass
            else:
                if 'id_bras' in result_check[0]:
                    id_ring = result_check[0]['id_bras']
                    res_put = self.put(id_ring, **kwargs)
                    if res_put:
                            #print('Successful update BRAS HSI info ' + str(kwargs))
                            return True
        return result

    @staticmethod
    def post_not_check(**kwargs):

        result = False
        data_json = json.dumps(kwargs)
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/bras_hsi_info/"
        headers_json = {"X-TENANT-ID": API_SPRING_IPMS_SERVER_TENANT, "username": API_SPRING_IPMS_USER,
                        "password": API_SPRING_IPMS_PASS,
                        "Content-Type": "application/json"}
        # do Thang BSI HSI info co id nen check bang no luon

        # r = requests.post(url_api, headers=headers_json, data=data_json)
        r = api_post_retry(url_api, headers_json, data_json)
        if r:
            if r.ok:
                result_req = r.text
                if result_req == '':
                    #print('Successful insert BRas HSI info ' + str(data_json))
                    return True

        return result

    def delete(self, id,):
        result = False
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/bras_hsi_info/id=" + str(id)
        headers_json = {"X-TENANT-ID": API_SPRING_IPMS_SERVER_TENANT, "username": API_SPRING_IPMS_USER,
                        "password": API_SPRING_IPMS_PASS,
                        "Content-Type": "application/json"}
        # r = requests.delete(url_api, headers=headers_json)
        r = api_delete_retry(url_api, headers_json)
        if r:
            if r.ok:
                result_req = r.text
                if result_req == '':
                    return True

        return result


if __name__ == '__main__':

    _tbl = TblBrasHsiInfoImpl("")
    _tbl_lst = _tbl.list()

    #_tbl_1 = TblRingInfoImpl(_tbl_dct)
    result_put = _tbl.delete(2)
    # print(result_put)

