import datetime, functools, requests, json, time
from config import Config, Development
from core.helpers.api_helpers import api_delete_retry, api_put_retry, api_post_retry, api_get_retry, api_patch_retry

config = Development()
API_IRON = config.__getattribute__('API_IRON')
#API_IRON = 'http://210.211.97.169:8061'


class ApiIron:
    def __init__(self, api_name, **api_search):
        self.api_name = api_name
        self.api_search = api_search
        if self.api_search and self.analytic_search(**api_search):
            self.api_url = API_IRON + '/' + self.api_name + '/' + self.analytic_search(**api_search)
        else:
            self.api_url = API_IRON + '/' + self.api_name

    @staticmethod
    def analytic_search(**kwargs):
        str_merge = ''
        if kwargs:
            begin_search = '?where={'
            str_search = begin_search
            for key, value in kwargs.items():
                if is_float(value):
                    str_search += '"' + str(key) + '":' + str(value) + ''
                elif value == "":
                    pass
                else:
                    str_search += '"' + str(key) + '":"' + str(value) + '",'
            end_search = '}'
            str_search += end_search
            if str_search == '?where={}':
                return ''
            else:
                pos_end = str_search.find('}')
                str_merge = str_search[:pos_end - 1] + '}'
        return str_merge

    def get_api(self):
        err = "Some error here"
        check_end = False
        count_loop = 0
        api_request_name = self.api_url
        page_next = 0
        result_lst = list()
        while not check_end:
            try:
                # r = requests.get(api_request_name)
                r = api_get_retry(api_request_name, '')
                if r:
                    count_loop += 1
                    if r.ok:
                        data_json = json.loads(r.text)
                        result_working = data_json.get('_items')
                        if len(result_working) > 0:
                            for x in result_working:
                                result_lst.append(x)
                            link_page = data_json.get("_links")
                            if 'next' in link_page:
                                next_page_dct = link_page.get('next')
                                next_page_href = str(next_page_dct.get('href'))
                                find_pos_question = next_page_href.find("page=")
                                if find_pos_question > 0:
                                    page_next = int(next_page_href[find_pos_question + 5:])
                                    api_request_name = API_IRON + "/" + str(next_page_href)
                                else:
                                    check_end = True
                            else:
                                return result_lst
                        else:
                            check_end = True
                    else:

                        time.sleep(3)
                        err = r.text
                    count_max = 4 + page_next
                    if count_loop > count_max:
                        return result_lst
            except Exception as e:
                print('Error %s when API to %s with searching %s' % (e, self.api_name, self.api_search))
                return result_lst
        return result_lst

    def search_api(self, **kwargs):

        err = "Some error here"
        check_end = False
        count_loop = 0
        api_request_name = API_IRON + '/' + self.api_name + '/' + self.analytic_search(**kwargs)
        page_next = 0
        result_lst = list()
        while not check_end:
            try:
                # r = requests.get(api_request_name)
                r = api_get_retry(api_request_name, '')
                if r:
                    count_loop += 1
                    if r.ok:
                        data_json = json.loads(r.text)
                        result_working = data_json.get('_items')
                        if len(result_working) > 0:
                            for x in result_working:
                                result_lst.append(x)
                            link_page = data_json.get("_links")
                            if 'next' in link_page:
                                next_page_dct = link_page.get('next')
                                next_page_href = str(next_page_dct.get('href'))
                                find_pos_question = next_page_href.find("page=")
                                if find_pos_question > 0:
                                    page_next = int(next_page_href[find_pos_question + 5:])
                                    api_request_name = API_IRON + "/" + str(next_page_href)
                                else:
                                    check_end = True
                            else:
                                return result_lst
                        else:
                            check_end = True
                    else:

                        time.sleep(3)
                        err = r.text
                    count_max = 4 + page_next
                    if count_loop > count_max:
                        return result_lst
            except Exception as e:
                print('Error %s when API to %s with searching %s' % (e, self.api_name, self.api_search))
                return result_lst
        return result_lst

    def post_api(self, **kwargs):
        # check exist
        err = "Some errors here"
        _api_lst = self.get_api()
        if len(_api_lst) > 1:
            # qua nhieu tai nguyen tim kiem
            err = 'Error two much thing to search when posting ' + self.api_url
            return (False, err)
        elif len(_api_lst) < 1:
            # post new
            headers = {'Content-Type': 'application/json'}
            payload = kwargs
            count_loop = 0
            check_end = False
            while not check_end:
                try:
                    # r = requests.post(self.api_url, json=payload, headers=headers)
                    r = api_post_retry(self.api_url, headers, payload)
                    if r:
                        if r.ok:
                            result = r.text
                            print(result)
                            return (True, result)
                        else:
                            count_loop += 1
                            time.sleep(3)
                            err = r.text
                        if count_loop > 4:
                            err = 'Error Max limit when posting ' + self.api_url
                            return (False, err)
                except Exception as e:
                    err = str(e) + 'when posting ' + self.api_url
                    print("Error %s when posting %s with searching %s" % (e, self.api_name, self.api_search))
                    return (False, err)
        else:
            print(_api_lst[0])
            obj = _api_lst[-1]
            id_obj = _api_lst[0].get('_id')
            etag_obj = _api_lst[0].get('_etag')
            _api_obj = API_IRON + '/' + self.api_name + '/' + id_obj
            headers = {'Content-Type': 'application/json', 'If-Match': etag_obj}
            payload = dict()
            for key, value in kwargs.items():
                if value:
                    payload[key] = value
            if payload:
                check_end = False
                count_loop = 0
                while not check_end:
                    try:
                        # r = requests.patch(_api_obj, json=payload, headers=headers)
                        r = api_patch_retry(_api_obj, headers, payload)
                        if r:
                            if r.ok:
                                result = r.text
                                err = 'Result patch of id {0} is {1}'.format(id_obj, result)
                                print(err)
                                return (True, err)
                            else:
                                time.sleep(3)
                                err = r.text
                                count_loop += 1
                            if count_loop > 3:
                                check_end = True
                    except Exception as e:
                        err = "Error " + str(e) + "when posting " + id_obj + " api " + self.api_url
                        return (False, err)
            else:
                return (False, err)

    def delete_api(self):
        err = 'Some error here'
        _obj_lst = self.get_api()
        if len(_obj_lst) == 1:
            obj_delete = _obj_lst[0]
            obj_etag = obj_delete['_etag']
            obj_id = obj_delete['_id']
            api_obj = API_IRON + '/' + self.api_name + '/' + obj_id

            check_end = False
            count_loop = 0
            while not check_end:
                try:
                    headers = {'Content-Type': 'application/json', 'If-Match': obj_etag}
                    # r = requests.delete(api_obj, headers=headers)
                    r = api_delete_retry(api_obj, headers)
                    if r:
                        if r.ok:
                            result = 'OK'
                            err = 'Result delete of id {0} is {1}'.format(obj_id, result)
                            print(err)
                            return (True, err)
                        else:
                            time.sleep(3)
                            count_loop += 1
                            err = r.text
                        if count_loop > 3:
                            check_end = True

                except Exception as e:
                    err = "Error " + str(e) + "when deleting " + obj_id + " api " + self.api_url
                    return (False, err)

        else:
            return (False, "No object")
        return (False, err)


def is_float(num):
    try:
        test = float(num)
        return True
    except Exception as e:
        return False


if __name__ == '__main__':
    _api = ApiIron(api_name='wallet_number', **dict(coin_name='USDT', wallet='Treasury'))
    _lst = _api.post_api(**dict(coin_name='USDT',
                                wallet="Treasury",
                                description="",
                                number=[{
                                    "number": 0,
                                    "date_time": "Sun, 19 Aug 2018 15:49:44 GMT",
                                    "price_btc": 0,
                                    "price_usd": 0
                                }]))
    print(_lst)
