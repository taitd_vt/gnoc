__author__ = 'VTN-DHML-DHIP10'
import cx_Oracle
import json
import os
import uuid
from core.helpers.date_helpers import convert_date_to_elk_from_ipms, get_date_yesterday_format_ipms, \
    get_date_before_yesterday_format_ipms
from config import Development

config = Development()
SERVER_GNOC = config.__getattribute__('SERVER_GNOC')
SERVER_GNOC_PORT = config.__getattribute__('SERVER_GNOC_PORT')
SERVER_GNOC_SERVICE_NAME = config.__getattribute__('SERVER_GNOC_SERVICE_NAME')
SERVER_GNOC_USERNAME = config.__getattribute__('SERVER_GNOC_USERNAME')
SERVER_GNOC_PASSWORD = config.__getattribute__('SERVER_GNOC_PASSWORD')


class TblSyslogGnocImpl:
    def __init__(self, pool):
        self.pool = pool

    def get_conn(self):
        try:
            print("Connecto to NOcpro manual")
            dsn_str = cx_Oracle.makedsn(SERVER_GNOC, SERVER_GNOC_PORT, service_name=SERVER_GNOC_SERVICE_NAME)
            conn = cx_Oracle.connect(user=SERVER_GNOC_USERNAME, password=SERVER_GNOC_PASSWORD, dsn=dsn_str,
                                     encoding='UTF-8', nencoding='UTF-8')

        except Exception as err:
            print(err)
            return None
        return conn

    def get_cr_no_mapping_alm_lst(self):
        sql_qury = "select CR_NUMBER, TITLE CR_NAME,  EARLIEST_START_TIME START_TIME, LATEST_START_TIME END_TIME from OPEN_PM.CR cr_a where cr_a.CR_ID NOT IN (select CR_ID from  OPEN_PM.CR_ALARM) and cr_a.state in (5,6) and cr_a.CR_NUMBER like '%CORE%' and cr_a.EARLIEST_START_TIME > SYSDATE-8/24 AND cr_a.LATEST_START_TIME<SYSDATE+1"
        res_lst = list()
        try:
            conn = self.pool.acquire()
        except Exception as err:
            conn = None
            print(err)

        chck_pool = True
        if not conn:
            conn = self.get_conn()
            chck_pool = False
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]

            except Exception as err:
                print(err)
                return list()
            finally:
                cursor.close()
                if chck_pool:
                    self.pool.release(conn)
                else:
                    conn.close()

        return res_lst

    def get_cr_no_mapping_ahdv_lst(self):
        sql_qury = "select CR_NUMBER, TITLE,  EARLIEST_START_TIME, LATEST_START_TIME from OPEN_PM.CR cr_a where cr_a.CR_ID NOT IN (select CR_ID from  OPEN_PM.CR_ALARM) and cr_a.state in (5,6) and cr_a.CR_NUMBER like '%CORE%' and SERVICE_AFFECTING =1 and cr_a.EARLIEST_START_TIME > SYSDATE-8/24 AND cr_a.LATEST_START_TIME<SYSDATE+1"
        res_lst = list()
        chck_pool = True
        try:
            conn = self.pool.acquire()
        except Exception as err:
            conn = None
            print(err)

        if not conn:
            conn = self.get_conn()
            chck_pool = False
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]

            except Exception as err:
                print(err)
                return list()
            finally:
                cursor.close()
                if chck_pool:
                    self.pool.release(conn)
                else:
                    conn.close()
        return res_lst


if __name__ == '__main__':
    dsn_str = cx_Oracle.makedsn(SERVER_NOCPRO, SERVER_NOCPRO_PORT, service_name=SERVER_NOCPRO_SERVICE_NAME)
    pool_nocpro = cx_Oracle.SessionPool(min=10,
                                        max=10, increment=1, threaded=True, dsn=dsn_str,
                                        user=SERVER_NOCPRO_USERNAME, password=SERVER_NOCPRO_PASSWORD,
                                        encoding='UTF-8', nencoding='UTF-8')
    _tbl = TblSyslogCnttCoreImpl(pool_nocpro)
    _tbl_new_lst = _tbl.get_alarm_new_lst()
    _tbl_clear_lst = _tbl.get_alarm_repeat_lst()


