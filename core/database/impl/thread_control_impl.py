from config import Development
from core.database.impl.api_impl import ApiIron
config = Development()
API_COIN_TRADE_HISTORY = config.__getattribute__('API_COIN_TRADE_HISTORY')


class ThreadControlImpl(ApiIron):
    def __init__(self, name):
        super().__init__('thread_control', **dict(name=name.upper()))
        self.name = name.upper()
        self.status = None
        self.last_update = None

    def get(self):
        thread_lst = self.get_api()
        if thread_lst:
            for x in thread_lst:
                return x
        else:
            return dict()

    def get_all(self):
        thread_lst = self.get_api()
        return thread_lst

    def get_status(self):
        thread_dct = self.get()
        status = ""
        try:
            if 'status' in thread_dct:
                status = thread_dct.get("status")
            return status
        except Exception as e:
            print('Error in get list  of number trade history of coin %s' % e)
            return status

    def get_date(self):
        date_last = 'Thu, 01 Jan 1970 00:00:00 GMT'
        thread_dct = self.get()
        try:
            if 'last_update' in thread_dct:
                date_last = thread_dct.get("last_update")
            return date_last
        except Exception as e:
            print('Error in get list  of number trade history of coin %s' % e)
            return date_last

    def check_exist(self):
        result = False
        try:
            thread_dct = self.get()

            if thread_dct:
                result = True
                return result
        except Exception as e:
            print('Check exist thread Control failed... %s' % e)
            return result
        return result

    def delete(self):
        try:
            name = self.name()
            coin_dct = self.get()
            if coin_dct == dict():
                print('There is no thread control of %s ' % name)
            else:
                res_del, err_del = self.delete_api()
                print('Result delete of thread control of %s is %s' % (name, res_del))
                return (res_del, err_del)

        except Exception as e:
            print("Error when delete thread control %s" % e)
            err = "Error when delete thread control " + str(e)
            return (False, err)

if __name__ == '__main__':
   pass

