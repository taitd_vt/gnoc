__author__ = 'VTN-DHML-DHIP10'

import pymysql
import json
from config import Development
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.helpers.stringhelpers import check_regex_acc
config = Development()
SERVER_HOST = config.__getattribute__('SERVER_HOST')
SERVER_HOST_USER = config.__getattribute__('SERVER_HOST_USER')
SERVER_HOST_PASS = config.__getattribute__('SERVER_HOST_PASS')
AREA_IP_MAIN_LST = config.__getattribute__('AREA_IP_MAIN_LST')
API_SPRING_IPMS_SERVER_TENANT = config.__getattribute__('API_SPRING_IPMS_SERVER_TENANT')


class TblDeynyWebWhitelistImpl:
    def __init__(self):
        pass

    def get_conn(self):
        try:
            conn = pymysql.connect(host=SERVER_HOST, user=SERVER_HOST_USER, password=SERVER_HOST_PASS, db='luuluong')

        except Exception as err:
            print(err)
            return None
        return conn

    def get_lst(self):
        sql_qury = "SELECT * FROM tbl_deny_web_whitelist"
        res_lst = list()
        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]

            except Exception as err:
                print(err)
                return list()
            finally:
                cursor.close()
                conn.close()
        return res_lst

    def get_web_lst(self):
        sql_qury = "SELECT web FROM tbl_deny_web_whitelist"
        res_lst = list()
        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]

            except Exception as err:
                print(err)
                return list()
            finally:
                cursor.close()
                conn.close()
        return res_lst

    def get(self, id_cdbr):
        sql_qury = "SELECT * FROM tbl_deny_web_whitelist WHERE Id=%s"
        res_lst = list()
        res_tbl_err_lst = []
        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury, id_cdbr)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                '''
                if res_lst:
                    for res in res_lst:
                        _tbl_fnd_obj = TblMopVmsaServerImpl()
                        _tbl_fnd_obj.node_code = res['node_code']
                        _tbl_fnd_obj.params = res['params']
                        _tbl_fnd_obj.template_id = res['template_id']
                        _tbl_fnd_obj.template_name = res['template_name']
                        _tbl_fnd_obj.type_run_mop = res['type_run_mop']
                        _tbl_fnd_obj.Id = res['Id']
                        res_tbl_err_lst.append(_tbl_fnd_obj)
                '''

            except Exception as err:
                print(err)
                return list()
            finally:
                cursor.close()
                conn.close()
        return res_lst

    def get_template_id(self, id_cdbr):
        sql_qury = "SELECT * FROM tbl_deny_web_whitelist WHERE template_id=%s"
        res_lst = list()
        res_tbl_err_lst = []
        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury, id_cdbr)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                if res_lst:
                    for res in res_lst:
                        _tbl_fnd_obj = TblMopVmsaServerImpl()
                        _tbl_fnd_obj.node_code = res['node_code']
                        _tbl_fnd_obj.params = res['params']
                        _tbl_fnd_obj.template_id = res['template_id']
                        _tbl_fnd_obj.template_name = res['template_name']
                        _tbl_fnd_obj.type_run_mop = res['type_run_mop']
                        _tbl_fnd_obj.Id = res['Id']
                        res_tbl_err_lst.append(_tbl_fnd_obj)

            except Exception as err:
                print(err)
                return list()
            finally:
                cursor.close()
                conn.close()
        return res_tbl_err_lst

    def find(self, template_id):
        res_free_load_lst = []
        conn = self.get_conn()

        if conn:
            cursor = conn.cursor()
            try:
                # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                cursor.execute("select * from  tbl_deny_web_whitelist where "
                               "(template_id like '%" + template_id + "%')  ")

                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                # do mac dinh chi co 1 object nen return luon ra vay :D
                if res_lst:
                    for res in res_lst:
                        try:
                            _tbl_fnd_obj = TblMopVmsaServerImpl()
                            _tbl_fnd_obj.node_code = res['node_code']
                            _tbl_fnd_obj.params = res['params']
                            _tbl_fnd_obj.template_id = res['template_id']
                            _tbl_fnd_obj.template_name = res['template_name']
                            _tbl_fnd_obj.type_run_mop = res['type_run_mop']
                            _tbl_fnd_obj.Id = res['Id']
                            res_free_load_lst.append(_tbl_fnd_obj)
                        except Exception as err:
                            print(err)

            except Exception as err:
                print(err)

            finally:
                cursor.close()
                conn.close()
                return res_free_load_lst

    def find_like(self, **kwargs):
        key_srch = ''
        page = 0
        page_size = 50
        if kwargs:
            if 'page' in kwargs:
                page = kwargs['page']
            if 'page_size' in kwargs:
                page_size = kwargs['page_size']
            if 'search' in kwargs:
                key_srch = kwargs['search']

        res_free_load_lst = []
        chck_key = check_regex_acc(key_srch)

        conn = self.get_conn()

        if conn and chck_key:
            if page > 0 and page_size >= 0:
                sql = "select * from  tbl_deny_web_whitelist where " \
                               "(template_id like '%" + key_srch + "%') or  (template_name like '%" + key_srch + "%') " \
                               "or (params like '%" + key_srch + "%') LIMIT " + str((page-1) * 50) + "," + str(page_size)
            else:
                sql = "select * from  tbl_deny_web_whitelist where " \
                               "(template_id like '%" + key_srch + "%') or  (template_name like '%" + key_srch + "%') " \
                               "or (params like '%" + key_srch + "%') "

            cursor = conn.cursor()
            try:
                # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                cursor.execute(sql)

                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                # do mac dinh chi co 1 object nen return luon ra vay :D
                if res_lst:
                    for res in res_lst:
                        try:
                            _tbl_fnd_obj = TblMopVmsaServerImpl()
                            _tbl_fnd_obj.node_code = res['node_code']
                            _tbl_fnd_obj.params = res['params']
                            _tbl_fnd_obj.template_id = res['template_id']
                            _tbl_fnd_obj.template_name = res['template_name']
                            _tbl_fnd_obj.type_run_mop = res['type_run_mop']
                            _tbl_fnd_obj.Id = res['Id']
                            res_free_load_lst.append(_tbl_fnd_obj)
                        except Exception as err:
                            print(err)

            except Exception as err:
                print(err)

            finally:
                cursor.close()
                conn.close()
                return res_free_load_lst
        return res_free_load_lst

    def check_exist(self, template_id):
        res_chk = False
        conn = self.get_conn()
        _tbl_fnd_obj = None
        if conn:
            cursor = conn.cursor()
            try:
                # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                cursor.execute("select * from  tbl_deny_web_whitelist where "
                               "(template_id =%s ) ", (template_id))

                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                # do mac dinh chi co 1 object nen return luon ra vay :D
                if res_lst:
                    for res in res_lst:
                        _tbl_fnd_obj = TblMopVmsaServerImpl()
                        _tbl_fnd_obj.node_code = res['node_code']
                        _tbl_fnd_obj.params = res['params']
                        _tbl_fnd_obj.template_id = res['template_id']
                        _tbl_fnd_obj.template_name = res['template_name']
                        _tbl_fnd_obj.type_run_mop = res['type_run_mop']
                        _tbl_fnd_obj.Id = res['Id']
                        res_chk = True

            except Exception as err:
                print(err)
                return res_chk, _tbl_fnd_obj
            finally:
                cursor.close()
                conn.close()
                return res_chk, _tbl_fnd_obj

    def insert(self, node_code, params, template_id, template_name, type_run_mop):

        chk_exst, tbl_exst = self.check_exist(template_id=template_id)
        if not chk_exst:
            conn = self.get_conn()
            cbn_bkk_obj = None

            sql_qry = "INSERT INTO tbl_deny_web_whitelist(node_code, params, `template_id`, template_name, " \
                      "type_run_mop)  " \
                      "VALUES (%s, %s, %s, %s, %s)"
            if conn:
                cursor = conn.cursor()
                try:
                    # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                    res = cursor.execute(sql_qry, (node_code, params, template_id, template_name, type_run_mop))
                    conn.commit()
                    print('Inserted into tbl_deny_web_whitelist OK')

                except Exception as err:
                    print(err)
                    return False
                finally:
                    cursor.close()
                    conn.close()
        else:
            # Update
            try:
                if getattr(tbl_exst, "Id"):
                    _id_tbl_node_exst = tbl_exst.Id
                    res_updt = self.update(node_code, params, template_id, template_name, type_run_mop, _id_tbl_node_exst)
                    if res_updt:
                        return True
                    else:
                        return False
            except Exception as err:
                print("Error %s when insert tbl node service" % err)
                return False
        return True

    def update(self, node_code, params, template_id, template_name, type_run_mop, id_tbl):

        conn = self.get_conn()
        cbn_bkk_obj = None
        sql_qry = "UPDATE tbl_deny_web_whitelist SET `node_code` = %s, `params` = %s , `template_id` = %s , " \
                  "`template_name` = %s ,  `type_run_mop` = %s   WHERE Id = %s"
        if conn:
            cursor = conn.cursor()
            try:
                # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                res = cursor.execute(sql_qry, (node_code, params, template_id, template_name, type_run_mop , id_tbl))
                conn.commit()
                print('Update into tbl_deny_web_whitelist OK')

            except Exception as err:
                print(err)
                return False
            finally:
                cursor.close()
                conn.close()
        return True

    def delete(self, id_tbl):

        conn = self.get_conn()
        cbn_bkk_obj = None
        sql_qry = "DELETE FROM tbl_deny_web_whitelist WHERE Id = %s"
        if conn:
            cursor = conn.cursor()
            try:
                # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                res = cursor.execute(sql_qry, (id_tbl))
                conn.commit()
                print('DELETE into tbl_deny_web_whitelist OK')

            except Exception as err:
                print(err)
                return False
            finally:
                cursor.close()
                conn.close()
        return True

    def list_page(self, page, page_size):


        sql_qury = "SELECT * FROM tbl_deny_web_whitelist LIMIT %s, %s"
        res_lst = list()
        res_tbl_err_lst = []
        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury, ((page-1) * 50, page_size))
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                if res_lst:
                    for res in res_lst:
                        _tbl_fnd_obj = TblMopVmsaServerImpl()
                        _tbl_fnd_obj.node_code = res['node_code']
                        _tbl_fnd_obj.params = res['params']
                        _tbl_fnd_obj.template_id = res['template_id']
                        _tbl_fnd_obj.template_name = res['template_name']
                        _tbl_fnd_obj.type_run_mop = res['type_run_mop']
                        _tbl_fnd_obj.Id = res['Id']
                        res_tbl_err_lst.append(_tbl_fnd_obj)

            except Exception as err:
                print(err)
                return list()
            finally:
                cursor.close()
                conn.close()

        return res_tbl_err_lst

    def total(self):


        sql_qury = "SELECT count(*) as count FROM tbl_deny_web_whitelist"
        _totl = 0

        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                if res_lst:
                    for res in res_lst:
                        _totl = res['count']
                        return _totl

            except Exception as err:
                print(err)
                return _totl
            finally:
                cursor.close()
                conn.close()

        return _totl

    def total_key_name(self, key_srch):

        sql_qury = "SELECT count(*) as count FROM tbl_deny_web_whitelist WHERE (template_id like '%" + key_srch + "%') " \
                   "or  (template_name like '%" + key_srch + "%') or (params like '%" + key_srch + "%')"
        _totl = 0

        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                if res_lst:
                    for res in res_lst:
                        _totl = res['count']
                        return _totl

            except Exception as err:
                print(err)
                return _totl
            finally:
                cursor.close()
                conn.close()

        return _totl


if __name__ == '__main__':
    tbl = TblDeynyWebWhitelistImpl()

    tbl_lst = tbl.get_web_lst()
    print(tbl_lst)

    #print(tbl_lst)


