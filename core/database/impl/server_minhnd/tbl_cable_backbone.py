#!/D:\python36 new
# -*- coding: utf8 -*-
import pymysql

from core.helpers.stringhelpers import check_regex_acc
from core.helpers.date_helpers import get_date_now, convert_date_str_to_date_obj_ipms
from core.helpers.int_helpers import convert_string_to_float
from config import Development
import os
import sys

config = Development()
SERVER_HOST = config.__getattribute__('SERVER_HOST')
SERVER_HOST_USER = config.__getattribute__('SERVER_HOST_USER')
SERVER_HOST_PASS = config.__getattribute__('SERVER_HOST_PASS')
SERVER_CBS_USERNAME = config.__getattribute__('SERVER_CBS_USERNAME')
SERVER_CBS_PASSWORD = config.__getattribute__('SERVER_CBS_PASSWORD')


class TblCableBackboneImpl:
    def __init__(self, pool):
        self.pool = pool

    def get_conn(self):
        try:
            conn = pymysql.connect(host=SERVER_HOST, user=SERVER_HOST_USER, password=SERVER_HOST_PASS, db='luuluong')

        except Exception as err:
            print(err)
            return None
        return conn

    def get_utilization_cable_max(self, key_srch):
        # check key_srch co ma doc khong:
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "SELECT * from tbl_cable_backbone WHERE timestamp like '%" + key_srch + "%' "

                try:
                    res_lst = self.pool.execute(sql_orc)

                except Exception as err:
                    print(err)
                    # tao ket noi connect manual de get
                    conn = self.get_conn()
                    if conn:
                        cursor = conn.cursor()
                        try:
                            cursor.execute(sql_orc)
                            data = cursor.fetchall()
                            col_name_lst = [x[0] for x in cursor.description]
                            res_lst = [dict(zip(col_name_lst, row)) for row in data]
                        except Exception as err1:
                            print(err1)
                        cursor.close()
                        conn.close()
                    return res_lst

        return res_lst

    def get_utilization_direction_now(self, key_srch):
        # check key_srch co ma doc khong:
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "select sum(max_all) , sum(capacity), timestamp from tbl_cable_backbone_max " \
                          "where timestamp >= '" + key_srch + "' group by timestamp order " \
                                                              "by timestamp desc limit 0,1 "
                try:
                    res_lst = self.pool.execute(sql_orc)

                except Exception as err:
                    print(err)
                    # tao ket noi connect manual de get
                    conn = self.get_conn()
                    if conn:
                        cursor = conn.cursor()
                        try:
                            cursor.execute(sql_orc)
                            data = cursor.fetchall()
                            col_name_lst = [x[0] for x in cursor.description]
                            res_lst = [dict(zip(col_name_lst, row)) for row in data]
                        except Exception as err1:
                            print(err1)
                        cursor.close()
                        conn.close()
                    return res_lst

        return res_lst

    def get_utilization_direction_max(self, key_srch):
        # check key_srch co ma doc khong:
        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''

        if chck_sql:
            if key_srch:
                sql_orc = "select sum(max_all), sum(capacity), timestamp from tbl_cable_backbone_max " \
                          "where timestamp >= '" + key_srch + "' group by timestamp order " \
                                                              "by sum(max_all) desc limit 0,1 "
                try:
                    res_lst = self.pool.execute(sql_orc)

                except Exception as err:
                    print(err)
                    # tao ket noi connect manual de get
                    conn = self.get_conn()
                    if conn:
                        cursor = conn.cursor()
                        try:
                            cursor.execute(sql_orc)
                            data = cursor.fetchall()
                            col_name_lst = [x[0] for x in cursor.description]
                            res_lst = [dict(zip(col_name_lst, row)) for row in data]
                        except Exception as err1:
                            print(err1)
                        cursor.close()
                        conn.close()
                return res_lst

        return res_lst

    def get_cable_direction_total(self, key_srch, type):

        if key_srch:
            chck_sql = check_regex_acc(key_srch)
        else:
            chck_sql = True

        # B1 Tinh ra Max Utilization tung huong B-N, B-T, T-N tu thoi diem key_srch
        # B2 Tinh ra time max roi tinh ra chi tiet tung huong cap va utilization
        # B3 List ra tong tung tuyen cap
        res_cabl_max_lst = []
        res_cabl_drct_lst = []
        res_drct_max_util_lst = []
        if chck_sql:
            if type == 'NOW':
                res_drct_max_util_lst = self.get_utilization_direction_now(key_srch=key_srch)
            else:
                res_drct_max_util_lst = self.get_utilization_direction_max(key_srch=key_srch)

            if res_drct_max_util_lst:
                if 'timestamp' in res_drct_max_util_lst[0]:
                    time_srch = str(res_drct_max_util_lst[0]['timestamp'])
                    # Do thoi gian lech giua hai bang tbl_cable_backbone va tbl_cable_backbone_max
                    # lay tinh den phut
                    time_srch_hour = time_srch[:13]
                    res_cabl_drct_detl_lst = self.get_utilization_cable_max(time_srch_hour)
                    for res_cabl_drct in res_cabl_drct_detl_lst:
                        try:
                            _cabl_speed = res_cabl_drct['speed']
                            _cabl_type = res_cabl_drct['cable_type']
                            _cabl_drct = res_cabl_drct['direction']
                            _cabl_traf_in = res_cabl_drct['traffic_in']
                            _cabl_traf_out = res_cabl_drct['traffic_out']
                            _cabl_and_drct = _cabl_type + _cabl_drct

                            if res_cabl_max_lst:
                                chck_exst = False
                                for x in res_cabl_max_lst:
                                    if x['cable_direction'] == _cabl_and_drct:
                                        x['traffic_in'] += _cabl_traf_in
                                        x['traffic_out'] += _cabl_traf_out
                                        x['speed'] += _cabl_speed
                                        chck_exst = True
                                        break
                                if not chck_exst:
                                    res_cabl_max_lst.append(dict(speed=_cabl_speed,
                                                                 cable_type=_cabl_type,
                                                                 direction=_cabl_drct,
                                                                 traffic_in=_cabl_traf_in,
                                                                 traffic_out=_cabl_traf_out,
                                                                 cable_direction=_cabl_and_drct))

                            else:
                                res_cabl_max_lst.append(dict(speed=_cabl_speed,
                                                             cable_type=_cabl_type,
                                                             direction=_cabl_drct,
                                                             traffic_in=_cabl_traf_in,
                                                             traffic_out=_cabl_traf_out,
                                                             cable_direction=_cabl_and_drct))

                            # them phan luu luong cho tung huong
                            if res_cabl_drct_lst:
                                chck_exst = False
                                for x in res_cabl_drct_lst:
                                    if x['direction'] == _cabl_drct:
                                        x['traffic_in'] += _cabl_traf_in
                                        x['traffic_out'] += _cabl_traf_out
                                        x['speed'] += _cabl_speed
                                        chck_exst = True
                                        break
                                if not chck_exst:
                                    res_cabl_drct_lst.append(dict(speed=_cabl_speed,
                                                                  direction=_cabl_drct,
                                                                  traffic_in=_cabl_traf_in,
                                                                  traffic_out=_cabl_traf_out
                                                                  ))

                            else:
                                res_cabl_drct_lst.append(dict(speed=_cabl_speed,
                                                              direction=_cabl_drct,
                                                              traffic_in=_cabl_traf_in,
                                                              traffic_out=_cabl_traf_out))

                        except Exception as err:
                            print("Error %s when get detail cable backbone" % str(err))
        return res_cabl_max_lst, res_cabl_drct_lst, res_drct_max_util_lst

    def get_congestion_direction(self, drct, drct_spd):
        if drct:
            chck_sql = check_regex_acc(drct)
        else:
            chck_sql = True
        res_lst = []
        sql_orc = ''
        # theo chi thi yeu cau lay 90 %
        drct_spd_int = convert_string_to_float(str(drct_spd))
        drct_spd_90 = 90 * drct_spd_int / 100
        if chck_sql:
            if drct_spd:
                sql_orc = "select timestamp , max_all, capacity from tbl_cable_backbone_max " \
                          "where max_all >= " + str(drct_spd_90) + " and direction = '" + str(drct) + "' " \
                          "order by timestamp desc limit 0,1 "
                try:
                    res_lst = self.pool.execute(sql_orc)

                except Exception as err:
                    print(err)
                    # tao ket noi connect manual de get
                    conn = self.get_conn()
                    if conn:
                        cursor = conn.cursor()
                        try:
                            cursor.execute(sql_orc)
                            data = cursor.fetchall()
                            col_name_lst = [x[0] for x in cursor.description]
                            res_lst = [dict(zip(col_name_lst, row)) for row in data]
                        except Exception as err1:
                            print(err1)
                        cursor.close()
                        conn.close()
                return res_lst

        return res_lst


if __name__ == '__main__':
    from core.database.impl.server_minhnd.server_mysql_connect import ServerMysqlConnect

    time_now = get_date_now()
    id_lst = [102564614, 102564994, 102565024]
    dbconfig = {
        "host": "192.168.251.15",
        "port": 3306,
        "user": "linhlk1",
        "password": "linhlk135",
        "database": "luuluong",
    }
    mysql_pool = ServerMysqlConnect(**dbconfig)

    _obj = TblCableBackboneImpl(mysql_pool)
    res_updt = _obj.get_utilization_cable_max("2020-03-08")
    print(res_updt)
    res_cabl = _obj.get_utilization_direction_max("2020-03-08")
    print(res_cabl)
