__author__ = 'VTN-DHML-DHIP10'
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.database.impl.ipms.host_ipms_impl import HostGroupIpmsImpl
from core.database.impl.ipms.intf_grp_name_imps_impl import InterfaceGroupNameIpmsImpl
from core.helpers.tenant_helpers import get_tenant_id
from core.helpers.int_helpers import convert_string_to_float
from core.helpers.date_helpers import get_date_now, convert_date_str_to_date_obj_spring, get_date_minus, \
    date_diff_in_seconds, \
    convert_date_obj_to_date_str_spring, get_date_now_format_elastic
import json
import pymysql
from config import Development

config = Development()
API_SPRING_IPMS_SERVER_TENANT = config.__getattribute__('API_SPRING_IPMS_SERVER_TENANT')
AREA_IP_MAIN_LST = config.__getattribute__('AREA_IP_MAIN_LST')


class TblInterfaceNotAddGroupImpl:
    def __init__(self, area, host, intf, description, interface_id, host_group_name, timestamp, speed, status):
        self.timestamp = timestamp
        self.area = area
        self.host = host
        self.intf = intf
        self.interface_id = interface_id
        self.host_group_name = host_group_name
        self.description = description
        self.speed = speed
        self.status = status
        self.id = None

    @staticmethod
    def total():
        _api_tbl_intf_not_impl = ApiSpringIpmsImpl('intf_not_add_group', API_SPRING_IPMS_SERVER_TENANT)
        _tot_str = _api_tbl_intf_not_impl.total()
        _tot_num = convert_string_to_float(_tot_str)
        return _tot_num

    @staticmethod
    def list():
        _api_tbl_intf_not_impl = ApiSpringIpmsImpl('intf_not_add_group', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_intf_not_impl.lst()
        res_lst = list()

        if _tbl_obj_lst:
            try:
                for _tbl_obj in _tbl_obj_lst:
                    _tbl_fnd_obj = TblInterfaceNotAddGroupImpl(timestamp=_tbl_obj['timestamp'],
                                                               area=_tbl_obj['area'],
                                                               host=_tbl_obj['host'],
                                                               intf=_tbl_obj['intf'],
                                                               description=_tbl_obj['description'],
                                                               interface_id=_tbl_obj['interface_id'],
                                                               host_group_name=_tbl_obj['host_group_name'],
                                                               speed=_tbl_obj['speed'],
                                                               status=_tbl_obj['status']
                                                               )
                    res_lst.append(_tbl_fnd_obj)

            except Exception as err:
                print("Error %s when list table Interface not add group " % str(err))
                return res_lst
        return res_lst

    def list_page(self, page, page_size):
        _api_tbl_intf_not_impl = ApiSpringIpmsImpl('intf_not_add_group', API_SPRING_IPMS_SERVER_TENANT)
        page_dct = dict(page=page, pageSize=page_size)
        _tbl_obj_lst = _api_tbl_intf_not_impl.get(page_dct)
        res_lst = list()

        if _tbl_obj_lst:
            try:
                for _tbl_obj in _tbl_obj_lst:
                    _tbl_fnd_obj = TblInterfaceNotAddGroupImpl(timestamp=_tbl_obj['timestamp'],
                                                               area=_tbl_obj['area'],
                                                               host=_tbl_obj['host'],
                                                               intf=_tbl_obj['intf'],
                                                               description=_tbl_obj['description'],
                                                               interface_id=_tbl_obj['interface_id'],
                                                               host_group_name=_tbl_obj['host_group_name'],
                                                               speed=_tbl_obj['speed'],
                                                               status=_tbl_obj['status']
                                                               )
                    res_lst.append(_tbl_fnd_obj)

            except Exception as err:
                print("Error %s when list table Interface not add group " % str(err))
                return res_lst
        return res_lst


    @staticmethod
    def find_like(**kwargs):
        _api_impl = ApiSpringIpmsImpl('intf_not_add_group', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_impl.get(kwargs)

        return _tbl_obj_lst

    @staticmethod
    def total_key_name(key_name):
        _api_impl = ApiSpringIpmsImpl('intf_not_add_group', API_SPRING_IPMS_SERVER_TENANT)

        try:
            _total_str = _api_impl.get(dict(searchTotal=key_name))
            _total_num = convert_string_to_float(_total_str)
            return _total_num

        except Exception as err:
                print("Error %s when get total of key name %s " % str(err), key_name)
                return 0

    def check_exist(self):
        _api_tbl_intf_not_impl = ApiSpringIpmsImpl('intf_not_add_group', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_intf_not_impl.get(dict(interface_id=self.interface_id,
                                                       area=self.area))
        check_exst = False
        if _tbl_obj_lst:
            try:
                for _tbl_obj in _tbl_obj_lst:
                    _tbl_fnd_obj = TblInterfaceNotAddGroupImpl(timestamp=_tbl_obj['timestamp'],
                                                               area=_tbl_obj['area'],
                                                               host=_tbl_obj['host'],
                                                               intf=_tbl_obj['intf'],
                                                               description=_tbl_obj['description'],
                                                               interface_id=_tbl_obj['interface_id'],
                                                               host_group_name=_tbl_obj['host_group_name'],
                                                               speed=_tbl_obj['speed'],
                                                               status=_tbl_obj['status']
                                                               )

                return True

            except Exception as err:
                print(err)
                return True
        return check_exst

    @staticmethod
    def find_time_less(time_less):
        res_lst = []

        _api_tbl_intf_not_impl = ApiSpringIpmsImpl('intf_not_add_group', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_intf_not_impl.get(dict(timestampLess=time_less))

        if _tbl_obj_lst:
            try:
                for _tbl_obj in _tbl_obj_lst:
                    _tbl_fnd_obj = TblInterfaceNotAddGroupImpl(timestamp=_tbl_obj['timestamp'],
                                                               area=_tbl_obj['area'],
                                                               host=_tbl_obj['host'],
                                                               intf=_tbl_obj['intf'],
                                                               description=_tbl_obj['description'],
                                                               interface_id=_tbl_obj['interface_id'],
                                                               host_group_name=_tbl_obj['host_group_name'],
                                                               speed=_tbl_obj['speed'],
                                                               status=_tbl_obj['status']
                                                               )
                    res_lst.append(_tbl_fnd_obj)

                return res_lst

            except Exception as err:
                print('Error %s when check time less than %s of interface not add group ' % str(err), str(time_less))
                return res_lst
        return res_lst

    def save(self):
        # DO API Holon tu dong cong time len 7h nen ta se tru date di 7h de insert cho chuan
        # time_stam_date = convert_date_str_to_date_obj_spring(self.timestamp)
        time_stam_minus_gmt = get_date_minus(self.timestamp, minute_mins=60 * 7)
        self.timestamp = convert_date_obj_to_date_str_spring(time_stam_minus_gmt)

        if self.timestamp:
            _api_intf_not_add_group_impl = ApiSpringIpmsImpl('intf_not_add_group', API_SPRING_IPMS_SERVER_TENANT)
            chk_exst = self.check_exist()

            if chk_exst:
                try:
                    _tbl_obj_lst = _api_intf_not_add_group_impl.get(dict(interface_id=self.interface_id,
                                                                         area=self.area))
                    _tbl_obj = _tbl_obj_lst[0]
                    _tbl_id = _tbl_obj['id']
                    if _tbl_id:
                        self.id = _tbl_id
                        _tbl_json = json.dumps(self, default=lambda o: o.__dict__)
                        res_put = _api_intf_not_add_group_impl.put(_tbl_id, _tbl_json)
                        if res_put:
                            return True
                except Exception as err:
                    print('Error %s when save tbl_interface_not_add_group ID %s' % str(err), str(self.id))
                    return False
            else:
                # post:
                try:
                    _tbl_json = json.dumps(self, default=lambda o: o.__dict__)
                    res_put = _api_intf_not_add_group_impl.post(_tbl_json)
                    if res_put:
                        return True
                except Exception as err:
                    print(err)
                    return False

        return False

    def save_not_chck_exist(self):
        # DO API Holon tu dong cong time len 7h nen ta se tru date di 7h de insert cho chuan
        # time_stam_date = convert_date_str_to_date_obj_spring(self.timestamp)
        time_stam_minus_gmt = get_date_minus(self.timestamp, minute_mins=60 * 7)
        self.timestamp = convert_date_obj_to_date_str_spring(time_stam_minus_gmt)

        if self.timestamp:
            _api_intf_not_add_group_impl = ApiSpringIpmsImpl('intf_not_add_group', API_SPRING_IPMS_SERVER_TENANT)

            # post:
            try:
                _tbl_json = json.dumps(self, default=lambda o: o.__dict__)
                res_put = _api_intf_not_add_group_impl.post(_tbl_json)
                if res_put:
                    return True
            except Exception as err:
                print(err)
                return False

        return False

    def delete(self):
        _api_intf_not_add_group_impl = ApiSpringIpmsImpl('intf_not_add_group', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_intf_not_add_group_impl.get(dict(interface_id=self.interface_id,
                                                             area=self.area))
        if _tbl_obj_lst:
            try:
                _tbl_obj = _tbl_obj_lst[0]
                _tbl_id = _tbl_obj['id']
                if _tbl_id:
                    res_del = _api_intf_not_add_group_impl.delete(_tbl_id)

                    if res_del:
                        return True

            except Exception as err:
                print(err)
                return False
        else:
            # post:

            return False
        return False

    def get_info_db(self):
        for x in AREA_IP_MAIN_LST:
            if x['area'] == self.area.upper():
                user_db = x['username']
                pass_db = x['password']
                db = x['db']
                ip = x['ip']
                return dict(username=user_db, password=pass_db, db=db, ip=ip)
        return dict(username='', password='', db='', ip='')

    def get_conn(self):
        conn_dct = self.get_info_db()
        if conn_dct:
            ip = conn_dct['ip']
            user_db = conn_dct['username']
            pass_db = conn_dct['password']
            db = conn_dct['db']
            try:
                conn = pymysql.connect(host=ip, user=user_db, passwd=pass_db, db=db)
                if conn:
                    return conn
            except Exception as err:
                print("Error %s when connect to db %s" % (str(err), ip))
                return None
        return None

    def get_intf_not_add_grp_srv(self):
        intf_grp_ser_lst = self.list()
        intf_not_add_in_serv = ''
        if intf_grp_ser_lst:
            intf_not_add_in_serv = '('
            for x in intf_grp_ser_lst:
                intf_not_add_in_serv = intf_not_add_in_serv + str(x.interface_id) + ','
            intf_not_add_in_serv = intf_not_add_in_serv[:-1]
            intf_not_add_in_serv += ')'
        if intf_not_add_in_serv != '':
            return intf_not_add_in_serv
        else:
            return ''

    def get_intf_not_add_grp(self, intf_grp_name):
        intf_grp_name_impl = InterfaceGroupNameIpmsImpl(0, intf_grp_name)
        conn_dct = self.get_info_db()
        conn = self.get_conn()
        intf_not_add_in_serv = self.get_intf_not_add_grp_srv()
        res_lst = []
        if conn_dct and conn:
            ip = conn_dct['ip']
            tnt_id = get_tenant_id(ip)
            res_lst = []
            hst_grp_lst = intf_grp_name_impl.get_grp_lst(tnt_id, intf_grp_name)
            if hst_grp_lst:
                grp_id_lst = '('
                for x in hst_grp_lst:
                    grp_id_lst += str(x.group_id) + ","
                # bo dau , cuoi cung
                grp_id_lst = grp_id_lst[:-1]
                grp_id_lst += ')'
                if intf_not_add_in_serv:
                    sql = "select h.hostname,h.ip,i.*, hg.group_name as host_group_name  " \
                        "from interface i JOIN host h ON h.rid=i.rid  " \
                        "JOIN host_group hg ON hg.group_id=h.vgroup " \
                        "where hg.group_name LIKE '%FOIP%' And (i.name NOT LIKE '%0.%' And i.name NOT LIKE '%jsrv.1%' " \
                        "And i.name NOT LIKE '%lc-%' And i.name NOT LIKE '%pfe-%' " \
                        "And i.name NOT LIKE '%pfh-%' And i.name NOT LIKE '%1.%' And i.name NOT LIKE '%2.%' " \
                        "And i.name NOT LIKE '%3.%' And i.name NOT LIKE '%irb%' And i.name NOT LIKE '%em1-%' " \
                        "And i.name NOT LIKE '%em0-%' And i.name NOT LIKE '%e1-%' And i.name NOT LIKE '%cstm%' " \
                        "And i.name NOT LIKE '%cau4-%' And i.name NOT LIKE '%4.%' And i.name NOT LIKE '%5.%' " \
                        "And i.name NOT LIKE '%6.%' And i.name NOT LIKE '%7.%' And i.name NOT LIKE '%8.%' " \
                        "And i.name NOT LIKE '%9.%' And i.name NOT LIKE '%Vlan%' And i.name NOT LIKE '%pass/discard%' " \
                        "And i.name NOT LIKE '%_ingress%' And i.name NOT LIKE '%QoS_SIGTRAN%' " \
                        "And i.name NOT LIKE '%class-default%' And i.name NOT LIKE '%_egress%' " \
                        "And i.name NOT LIKE '%BVI%' And i.name NOT LIKE '%power%' And i.name NOT LIKE '%-mpls layer%' " \
                        "And i.name NOT LIKE '%ppd%' And i.name NOT LIKE '%ppe%' And i.name NOT LIKE '%fab%' " \
                        "And i.name NOT LIKE '%Eth-Trunk%' And i.name NOT LIKE '%reth%'  " \
                        "And i.name NOT LIKE '%Bundle-Ether%' And i.name NOT LIKE '%bcm%' " \
                        "And i.name NOT LIKE '%Port-channel%' And i.name NOT LIKE '%NumOf%' " \
                        "And i.name NOT LIKE '%SumOf%' And i.name NOT LIKE '%W_Status%' And i.name NOT LIKE '%DelayRTT%' " \
                        "And i.name NOT LIKE '%.RTT%' And i.name NOT LIKE '%RTTAvg%' And i.name NOT LIKE '%RTTMax%' " \
                        "And i.name NOT LIKE '%RTTMin%' And i.name NOT LIKE '%dot1q pvc%'  " \
                        "And i.name NOT LIKE '%interface%'  And i.name NOT LIKE '%PPoE session%' " \
                        "And i.name NOT LIKE '%ae%' And i.name NOT LIKE '%smartgroup%' " \
                        "And i.name NOT LIKE '%EOBC%' And i.name NOT LIKE '%tunnel-te%' " \
                        "And i.name NOT LIKE '%me0%' And i.name NOT LIKE '%me1%' And i.name NOT LIKE '%vcp-%' " \
                        "And i.name NOT LIKE '%vme%' And i.name NOT LIKE '%ms-%' And i.name NOT LIKE '%tunnel%' " \
                        "And i.name NOT LIKE '%bond%' And i.name NOT LIKE '%irb%' And i.name NOT LIKE '%Vl%' " \
                        "And i.name NOT LIKE '%CPU%' And i.name NOT LIKE '%MEM%' And i.name NOT LIKE '%SES%' " \
                        "And i.name NOT LIKE '%TEMPERATURE%' And i.name NOT LIKE '%fan%' And i.name NOT LIKE '%Control%' " \
                        "And i.name NOT LIKE '%mgmt%' And i.name NOT LIKE '%POW%' And i.name NOT LIKE '%_ll_%' " \
                        "And i.name NOT LIKE '%em0%' And i.name NOT LIKE '%fxp%' And i.name NOT LIKE '%em1%') " \
                        "And i.id not in(SELECT distinct iid from interface_group WHERE group_id in " + grp_id_lst + ") " \
                        "And i.id not in " + intf_not_add_in_serv + " And i.status !='inactive' And (h.hostname NOT LIKE '%Allot%' And h.hostname NOT LIKE '%Sigma%' " \
                        "And h.hostname NOT LIKE '%Tera%' And h.hostname NOT LIKE '%DNS%' And h.hostname NOT LIKE '%AAA%')"
                else:
                    sql = "select h.hostname,h.ip,i.*, hg.group_name as host_group_name  " \
                        "from interface i JOIN host h ON h.rid=i.rid  " \
                        "JOIN host_group hg ON hg.group_id=h.vgroup " \
                        "where hg.group_name LIKE '%FOIP%' And (i.name NOT LIKE '%0.%' And i.name NOT LIKE '%jsrv.1%' " \
                        "And i.name NOT LIKE '%lc-%' And i.name NOT LIKE '%pfe-%' " \
                        "And i.name NOT LIKE '%pfh-%' And i.name NOT LIKE '%1.%' And i.name NOT LIKE '%2.%' " \
                        "And i.name NOT LIKE '%3.%' And i.name NOT LIKE '%irb%' And i.name NOT LIKE '%em1-%' " \
                        "And i.name NOT LIKE '%em0-%' And i.name NOT LIKE '%e1-%' And i.name NOT LIKE '%cstm%' " \
                        "And i.name NOT LIKE '%cau4-%' And i.name NOT LIKE '%4.%' And i.name NOT LIKE '%5.%' " \
                        "And i.name NOT LIKE '%6.%' And i.name NOT LIKE '%7.%' And i.name NOT LIKE '%8.%' " \
                        "And i.name NOT LIKE '%9.%' And i.name NOT LIKE '%Vlan%' And i.name NOT LIKE '%pass/discard%' " \
                        "And i.name NOT LIKE '%_ingress%' And i.name NOT LIKE '%QoS_SIGTRAN%' " \
                        "And i.name NOT LIKE '%class-default%' And i.name NOT LIKE '%_egress%' " \
                        "And i.name NOT LIKE '%BVI%' And i.name NOT LIKE '%power%' And i.name NOT LIKE '%-mpls layer%' " \
                        "And i.name NOT LIKE '%ppd%' And i.name NOT LIKE '%ppe%' And i.name NOT LIKE '%fab%' " \
                        "And i.name NOT LIKE '%Eth-Trunk%' And i.name NOT LIKE '%reth%'  " \
                        "And i.name NOT LIKE '%Bundle-Ether%' And i.name NOT LIKE '%bcm%' " \
                        "And i.name NOT LIKE '%Port-channel%' And i.name NOT LIKE '%NumOf%' " \
                        "And i.name NOT LIKE '%SumOf%' And i.name NOT LIKE '%W_Status%' And i.name NOT LIKE '%DelayRTT%' " \
                        "And i.name NOT LIKE '%.RTT%' And i.name NOT LIKE '%RTTAvg%' And i.name NOT LIKE '%RTTMax%' " \
                        "And i.name NOT LIKE '%RTTMin%' And i.name NOT LIKE '%dot1q pvc%'  " \
                        "And i.name NOT LIKE '%interface%'  And i.name NOT LIKE '%PPoE session%' " \
                        "And i.name NOT LIKE '%ae%' And i.name NOT LIKE '%smartgroup%' " \
                        "And i.name NOT LIKE '%EOBC%' And i.name NOT LIKE '%tunnel-te%' " \
                        "And i.name NOT LIKE '%me0%' And i.name NOT LIKE '%me1%' And i.name NOT LIKE '%vcp-%' " \
                        "And i.name NOT LIKE '%vme%' And i.name NOT LIKE '%ms-%' And i.name NOT LIKE '%tunnel%' " \
                        "And i.name NOT LIKE '%bond%' And i.name NOT LIKE '%irb%' And i.name NOT LIKE '%Vl%' " \
                        "And i.name NOT LIKE '%CPU%' And i.name NOT LIKE '%MEM%' And i.name NOT LIKE '%SES%' " \
                        "And i.name NOT LIKE '%TEMPERATURE%' And i.name NOT LIKE '%fan%' And i.name NOT LIKE '%Control%' " \
                        "And i.name NOT LIKE '%mgmt%' And i.name NOT LIKE '%POW%' And i.name NOT LIKE '%_ll_%' " \
                        "And i.name NOT LIKE '%em0%' And i.name NOT LIKE '%fxp%' And i.name NOT LIKE '%em1%') " \
                        "And i.id not in(SELECT distinct iid from interface_group WHERE group_id in " + grp_id_lst + ") " \
                        "And i.status !='inactive' And (h.hostname NOT LIKE '%Allot%' And h.hostname NOT LIKE '%Sigma%' " \
                        "And h.hostname NOT LIKE '%Tera%' And h.hostname NOT LIKE '%DNS%' And h.hostname NOT LIKE '%AAA%')"

                cursor = conn.cursor()
                try:
                    cursor.execute(sql)
                    conn.commit()
                    result = cursor.fetchall()
                    col_name_lst = [x[0] for x in cursor.description]
                    res_lst = [dict(zip(col_name_lst, row)) for row in result]
                    # print(res_lst)
                    # check tinh nang IPMS khi chua insert 8000 interface

                except Exception as err:
                    print('Error %s when get interface not add group' % err)
                    return res_lst
        return res_lst

    def check_and_save_intf(self):
        # {'hostname': 'CV1_HLC_01', 'ip': '10.60.30.6', 'id': 3067448, 'name': 'GigabitEthernet0/0/1/8 - 91', 'rid': 1876, 'speed': 1000000000, 'description': 'TPHL15_Card_IOC1-Ge1', 'status': 'active', 'host_group_name': 'FOIP_CV'}
        # mac dinh add vao group co chua ki tu FOIP NEW
        time_now = get_date_now()
        res_intf_not_add_lst = self.get_intf_not_add_grp('FOIP_NEW')
        for x in res_intf_not_add_lst:
            stat = x['status']
            if stat != 'custom':
                x['speed'] /= 1000000
            if x['description'] == '':
                x['description'] = ' '
            tbl_obj = TblInterfaceNotAddGroupImpl(area=self.area,
                                                  host=x['hostname'],
                                                  intf=x['name'],
                                                  description=x['description'],
                                                  interface_id=x['id'],
                                                  host_group_name=x['host_group_name'],
                                                  timestamp=time_now,
                                                  speed=x['speed'],
                                                  status=x['status'])
            tbl_obj.save_not_chck_exist()


if __name__ == '__main__':
   time_now = get_date_now()
   intf_impl = TblInterfaceNotAddGroupImpl('KV3', '', '', '', 0, 'FOIP_IPBN', time_now, 0, 'active')
   intf_impl.check_and_save_intf()

