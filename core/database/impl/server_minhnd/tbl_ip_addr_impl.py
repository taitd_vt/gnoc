
__author__ = 'VTN-DHML-DHIP10'

import pymysql
import json
from config import Development
from core.helpers.date_helpers import get_date_now_format_ipms
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from netaddr import IPNetwork, IPAddress
config = Development()
SERVER_HOST = config.__getattribute__('SERVER_HOST')
SERVER_HOST_USER = config.__getattribute__('SERVER_HOST_USER')
SERVER_HOST_PASS = config.__getattribute__('SERVER_HOST_PASS')
AREA_IP_MAIN_LST = config.__getattribute__('AREA_IP_MAIN_LST')
API_SPRING_IPMS_SERVER_TENANT = config.__getattribute__('API_SPRING_IPMS_SERVER_TENANT')


class TblIpServerImpl:
    def __init__(self):
        pass

    def get_conn(self):
        try:
            conn = pymysql.connect(host=SERVER_HOST, user=SERVER_HOST_USER, password=SERVER_HOST_PASS, db='luuluong')

        except Exception as err:
            print(err)
            return None
        return conn

    def get_lst(self):
        sql_qury = "SELECT * FROM tbl_ip_addr"
        res_lst = list()
        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()
            res_tbl_err_lst = []
            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]

            except Exception as err:
                print(err)
                return list()
            finally:
                cursor.close()
                conn.close()
                return res_lst
        return res_lst

    def check_ip_in_range(self, ip_addr, ip_addr_range):
        try:
            if IPAddress(str(ip_addr)) in IPNetwork(str(ip_addr_range)):
                return True
        except Exception as err:
            print("Error %s when check ip address %s in range ip %s" %(str(err), ip_addr, ip_addr_range))
            return False
        return False

    def get_newest_lst(self):
        sql_qury = "select * from tbl_rnc_cbs where timestamp in (select max(timestamp) from tbl_rnc_cbs tbl order by timestamp desc  ) "
        res_lst = list()
        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()
            res_tbl_err_lst = []
            try:
                cursor.execute(sql_qury)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                for res in res_lst:
                    _tbl_fnd_obj = TblRncCbsServerImpl()
                    if isinstance(res, dict):
                        for key, value in res.items():
                            _tbl_fnd_obj.key = res.get(key)

                        res_tbl_err_lst.append(_tbl_fnd_obj)
            except Exception as err:
                print(err)
                return list()
            finally:
                cursor.close()
                conn.close()
        return res_lst

    def find_timestamp_lst(self, timestamp):
        sql_qury = "select * from tbl_rnc_cbs where timestamp =%s "
        res_lst = list()
        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()
            res_tbl_err_lst = []
            try:
                cursor.execute(sql_qury, timestamp)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]

            except Exception as err:
                print(err)
                return list()
            finally:
                cursor.close()
                conn.close()
        return res_lst

    def get(self, id_cdbr):
        sql_qury = "SELECT * FROM tbl_rnc_cbs WHERE Id=%s"
        res_lst = list()
        res_tbl_err_lst = []
        conn = self.get_conn()
        if conn:
            cursor = conn.cursor()

            try:
                cursor.execute(sql_qury, id_cdbr)
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                if res_lst:
                    for res in res_lst:
                        _tbl_fnd_obj = TblRncCbsServerImpl()
                        if isinstance(res, dict):
                            for key, value in res.items():
                                _tbl_fnd_obj.key = res.get(key)

                            res_tbl_err_lst.append(_tbl_fnd_obj)

            except Exception as err:
                print(err)
                return list()
            finally:
                cursor.close()
                conn.close()
        return res_tbl_err_lst

    def find(self, node_name, timestamp_lst_hour_bgn, timestamp_lst_hour_end,
             timestamp_lst_week_bgn, timestamp_lst_week_end):
        res_lst = []
        conn = self.get_conn()
        sql = "select * from  tbl_rnc_cbs where (node like '%" + node_name + "%')  and ((timestamp > '" + timestamp_lst_hour_bgn + "' and timestamp < '" + timestamp_lst_hour_end + "'  )  or (timestamp > '" + timestamp_lst_week_bgn + "' and timestamp < '" + timestamp_lst_week_end + "' ))"
        if conn:
            cursor = conn.cursor()
            try:
                # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                cursor.execute(sql)

                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                # do mac dinh chi co 1 object nen return luon ra vay :D

            except Exception as err:
                print(err)

            finally:
                cursor.close()
                conn.close()
                return res_lst

    def check_exist(self, node_name, timestamp):
        res_chk = False
        conn = self.get_conn()
        _tbl_fnd_obj = None
        if conn:
            cursor = conn.cursor()
            try:
                # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                cursor.execute("select * from  tbl_rnc_cbs where "
                               "(node =%s and timestamp =%s) ", (node_name, timestamp))

                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                # do mac dinh chi co 1 object nen return luon ra vay :D
                if res_lst:
                    for res in res_lst:
                        if isinstance(res, dict):
                            for key, value in res.items():
                                _tbl_fnd_obj.key = res.get(key)

                            res_chk = True

            except Exception as err:
                print(err)
                return res_chk, _tbl_fnd_obj
            finally:
                cursor.close()
                conn.close()
                return res_chk, _tbl_fnd_obj

    def insert(self, **kwargs):

        chk_exst, tbl_exst = self.check_exist(node_name=kwargs['NODE'],
                                              timestamp=kwargs['TIME'])
        if not chk_exst:
            conn = self.get_conn()
            cbn_bkk_obj = None

            sql_qry = "INSERT INTO tbl_rnc_cbs(node, timestamp, `cs_ccsr`, cs_cdr, " \
                      "voice_traffic, cs_rab_sr, cs_rab_att, cs_tab_success, cs_rrc_sr, cs_irat_hosr, ps_cssr," \
                      "ps_cdr, ps_rab_cr, ps_traffic, sho_sr, ps_irat_hosr)  " \
                      "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
            if conn:
                cursor = conn.cursor()
                try:
                    # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                    res = cursor.execute(sql_qry, (kwargs['NODE'], kwargs['TIME'], kwargs['CS_CCSR'], kwargs['CS_CDR'],
                                                   kwargs['VOICE_TRAFFIC'],
                                                   kwargs['CS_RAB_SR'], kwargs['CS_RAB_ATT'], kwargs['CS_TAB_SUCCESS'],
                                                   kwargs['CS_RRC_SR'], kwargs['CS_IRAT_HOSR'], kwargs['PS_CSSR'],
                                                   kwargs['PS_CDR'], kwargs['PS_RAB_CR'], kwargs['PS_TRAFFIC'],
                                                   kwargs['SHO_SR'], kwargs['PS_IRAT_HOSR']))
                    conn.commit()
                    print('Inserted into tbl_rnc_cbs OK')

                except Exception as err:
                    print(err)
                    return False
                finally:
                    cursor.close()
                    conn.close()
        else:
            # Update
            try:
                if getattr(tbl_exst, "Id"):
                    _id_tbl_node_exst = tbl_exst.Id
                    res_updt = self.update(kwargs)
                    if res_updt:
                        return True
                    else:
                        return False
            except Exception as err:
                print("Error %s when insert tbl node service" % err)
                return False
        return True

    def update(self, **kwargs):

        conn = self.get_conn()
        cbn_bkk_obj = None
        sql_qry = "UPDATE tbl_rnc_cbs SET `node` = %s, `timestamp` = %s , `cs_ccsr` = %s , " \
                  "`cs_cdr` = %s ,  `voice_traffic` = %s , `cs_rab_sr` = %s , `cs_rab_att` = %s, " \
                  "`cs_tab_success` = %s, `cs_rrc_sr` = %s, `cs_irat_hosr` = %s, `ps_cssr` = %s, " \
                  "`ps_cdr` = %s, `ps_rab_cr` = %s, `ps_traffic` = %s, `sho_sr` = %s, `ps_irat_hosr` = %s" \
                  "WHERE Id = %s"
        if conn:
            cursor = conn.cursor()
            try:

                res = cursor.execute(sql_qry, (kwargs['NODE'], kwargs['TIME'], kwargs['CS_CCSR'], kwargs['CS_CDR'],
                                                   kwargs['VOICE_TRAFFIC'],
                                                   kwargs['CS_RAB_SR'], kwargs['CS_RAB_ATT'], kwargs['CS_TAB_SUCCESS'],
                                                   kwargs['CS_RRC_SR'], kwargs['CS_IRAT_HOSR'], kwargs['PS_CSSR'],
                                                   kwargs['PS_CDR'], kwargs['PS_RAB_CR'], kwargs['PS_TRAFFIC'],
                                                   kwargs['SHO_SR'], kwargs['PS_IRAT_HOSR'], kwargs['Id']))
                conn.commit()
                print('Update into tbl_rnc_cbs OK')

            except Exception as err:
                print(err)
                return False
            finally:
                cursor.close()
                conn.close()
        return True

    def delete(self, id_tbl):

        conn = self.get_conn()
        cbn_bkk_obj = None
        sql_qry = "DELETE FROM tbl_rnc_cbs WHERE Id = %s"
        if conn:
            cursor = conn.cursor()
            try:
                # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                res = cursor.execute(sql_qry, (id_tbl))
                conn.commit()
                print('DELETE into tbl_rnc_cbs OK')

            except Exception as err:
                print(err)
                return False
            finally:
                cursor.close()
                conn.close()
        return True

    def delete_all(self, timestamp):

        conn = self.get_conn()
        cbn_bkk_obj = None
        sql_qry = "DELETE FROM tbl_rnc_cbs WHERE timestamp < %s"
        if conn:
            cursor = conn.cursor()
            try:
                # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                res = cursor.execute(sql_qry, (timestamp))
                conn.commit()
                print('DELETE into tbl_rnc_cbs OK')

            except Exception as err:
                print(err)
                return False
            finally:
                cursor.close()
                conn.close()
        return True


if __name__ == '__main__':
    tbl = TblRncCbsServerImpl()
    '''
    tbl_lst = tbl.get(41)
    print(tbl_lst)
    tbl_lst = tbl.get_lst()
    print(tbl_lst)

    res_upd = tbl.insert('minhnd', 'intf', 'descrip', 1000, 2000, 3000, '1D', 'B-N', '2020-02-26 10:00:01', 1001)
    print(res_upd)

    res_upd = tbl.update('minhnd 123', 'intf 123', 'descrip 123', 1000, 2000, 3000, '1D', 'B-N', '2020-02-26 10:00:01',
                         1001, 1)
    print(res_upd)

    tbl_lst = tbl.get_lst()
    print(tbl_lst)

    res_del = tbl.delete(1)
    tbl_lst = tbl.get_lst()
    print(tbl_lst)
    '''
    test = tbl.get_lst()
    print(test)



