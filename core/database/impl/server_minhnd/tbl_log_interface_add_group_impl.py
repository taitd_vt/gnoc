__author__ = 'VTN-DHML-DHIP10'
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.helpers.int_helpers import convert_string_to_float
from core.helpers.date_helpers import get_date_now, convert_date_str_to_date_obj_spring, get_date_minus, \
    date_diff_in_seconds, \
    convert_date_obj_to_date_str_spring, get_date_now_format_elastic
import json

from config import Development

config = Development()
API_SPRING_IPMS_SERVER_TENANT = config.__getattribute__('API_SPRING_IPMS_SERVER_TENANT')


class TblLogInterfaceAddGroupImpl:
    def __init__(self, area, host_name, intf, description, interface_id, host_group_name, timestamp, speed, group_name,
                 user):
        self.host_name = host_name
        self.description = description
        self.area = area
        self.intf = intf
        self.speed = speed
        self.group_name = group_name
        self.user = user
        self.host_group_name = host_group_name
        self.host_name = host_name
        self.timestamp = timestamp
        self.interface_id = interface_id
        self.id_insert = None

    def list(self):
        _api_tbl_intf_not_impl = ApiSpringIpmsImpl('log_insert_intf_group', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_intf_not_impl.lst()
        res_lst = list()
        " "
        if _tbl_obj_lst:

                for _tbl_obj in _tbl_obj_lst:
                    try:
                        _tbl_fnd_obj = TblLogInterfaceAddGroupImpl(timestamp=_tbl_obj['timestamp'],
                                                                   host_name=_tbl_obj['host_name'],
                                                                   description=_tbl_obj['description'],
                                                                   intf=_tbl_obj['intf'],
                                                                   area=_tbl_obj['area'],
                                                                   speed=_tbl_obj['speed'],
                                                                   group_name=_tbl_obj['group_name'],
                                                                   interface_id=_tbl_obj['interface_id'],
                                                                   user=_tbl_obj['user'],
                                                                   host_group_name=_tbl_obj['host_group_name']
                                                                   )
                        _tbl_fnd_obj.id_insert = _tbl_obj['id_insert']
                        res_lst.append(_tbl_fnd_obj)
                    except Exception as err:
                        print("Error %s when list table Interface not add group " % str(err))

        return res_lst

    def list_page(self, page, page_size):
        _api_tbl_intf_not_impl = ApiSpringIpmsImpl('log_insert_intf_group', API_SPRING_IPMS_SERVER_TENANT)
        page_dct = dict(page=page, pageSize=page_size)
        _tbl_obj_lst = _api_tbl_intf_not_impl.get(page_dct)
        res_lst = list()

        if _tbl_obj_lst:

                for _tbl_obj in _tbl_obj_lst:
                    try:
                        _tbl_fnd_obj = TblLogInterfaceAddGroupImpl(timestamp=_tbl_obj['timestamp'],
                                                                   host_name=_tbl_obj['host_name'],
                                                                   description=_tbl_obj['description'],
                                                                   intf=_tbl_obj['intf'],
                                                                   area=_tbl_obj['area'],
                                                                   speed=_tbl_obj['speed'],
                                                                   group_name=_tbl_obj['group_name'],
                                                                   interface_id=_tbl_obj['interface_id'],
                                                                   user=_tbl_obj['user'],
                                                                   host_group_name=_tbl_obj['host_group_name']
                                                                   )
                        _tbl_fnd_obj.id_insert = _tbl_obj['id_insert']
                        res_lst.append(_tbl_fnd_obj)
                    except Exception as err:
                        print("Error %s when list table Interface not add group " % str(err))

        return res_lst

    @staticmethod
    def total():
        _api_tbl_intf_not_impl = ApiSpringIpmsImpl('log_insert_intf_group', API_SPRING_IPMS_SERVER_TENANT)
        _tot_str = _api_tbl_intf_not_impl.total()
        _tot_num = convert_string_to_float(_tot_str)
        return _tot_num

    @staticmethod
    def find_like(**kwargs):
        _api_impl = ApiSpringIpmsImpl('log_insert_intf_group', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_impl.get(kwargs)

        return _tbl_obj_lst

    @staticmethod
    def total_key_name(key_name):
        _api_impl = ApiSpringIpmsImpl('log_insert_intf_group', API_SPRING_IPMS_SERVER_TENANT)

        try:
            _total_str = _api_impl.get(dict(searchTotal=key_name))
            _total_num = convert_string_to_float(_total_str)
            return _total_num

        except Exception as err:
                print("Error %s when get total of key name %s " % str(err), key_name)
                return 0

    def check_exist(self):
        _api_tbl_intf_not_impl = ApiSpringIpmsImpl('log_insert_intf_group', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_intf_not_impl.get(dict(interface_id=self.interface_id,
                                                       area=self.area,
                                                       timestamp=self.timestamp))
        check_exst = False
        if _tbl_obj_lst:
            try:
                for _tbl_obj in _tbl_obj_lst:
                    _tbl_fnd_obj = TblLogInterfaceAddGroupImpl(timestamp=_tbl_obj['timestamp'],
                                                               host_name=_tbl_obj['host_name'],
                                                               description=_tbl_obj['description'],
                                                               intf=_tbl_obj['intf'],
                                                               area=_tbl_obj['area'],
                                                               speed=_tbl_obj['speed'],
                                                               group_name=_tbl_obj['group_name'],
                                                               interface_id=_tbl_obj['interface_id'],
                                                               user=_tbl_obj['user'],
                                                               host_group_name=_tbl_obj['host_group_name']
                                                               )

                    return True

            except Exception as err:
                print(err)
                return True
        return check_exst

    def save(self):
        # DO API Holon tu dong cong time len 7h nen ta se tru date di 7h de insert cho chuan

        time_stam_date = self.timestamp
        time_stam_minus_gmt = get_date_minus(time_stam_date, minute_mins=60 * 7)
        self.timestamp = convert_date_obj_to_date_str_spring(time_stam_minus_gmt)

        if self.timestamp:
            _api_impl = ApiSpringIpmsImpl('log_insert_intf_group', API_SPRING_IPMS_SERVER_TENANT)
            chk_exst = self.check_exist()

            if chk_exst:
                try:
                    _tbl_obj_lst = _api_impl.get(dict(area=self.area,
                                                      interface_id=self.interface_id,
                                                      timestamp=self.timestamp))
                    _tbl_obj = _tbl_obj_lst[0]
                    _tbl_id = _tbl_obj['id_insert']
                    if _tbl_id:
                        self.id_insert = _tbl_id
                        _tbl_json = json.dumps(self, default=lambda o: o.__dict__)
                        res_put = _api_impl.put(_tbl_id, _tbl_json)
                        if res_put:
                            return True
                except Exception as err:
                    print('Error %s when save tbl_interface_not_add_group ID %s' % str(err), str(self.id_insert))
                    return False
            else:
                # post:
                try:
                    _tbl_json = json.dumps(self, default=lambda o: o.__dict__)
                    res_put = _api_impl.post(_tbl_json)
                    if res_put:
                        return True
                except Exception as err:
                    print(err)
                    return False

        return False

    def delete(self):
        _api_impl = ApiSpringIpmsImpl('log_insert_intf_group', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_impl.get(dict(area=self.area,
                                          interface_id=self.interface_id,
                                          timestamp=self.timestamp))
        if _tbl_obj_lst:
            try:
                _tbl_obj = _tbl_obj_lst[0]
                _tbl_id = _tbl_obj['id_insert']
                if _tbl_id:
                    res_del = _api_impl.delete(_tbl_id)

                    if res_del:
                        return True

            except Exception as err:
                print(err)
                return False
        else:
            # post:

            return False
        return False


if __name__ == '__main__':
    time_ins = get_date_now_format_elastic()
    _tbl_kick = _tbl_fnd_obj = TblLogInterfaceAddGroupImpl(timestamp=time_ins,
                                                           host_name='CKV01',
                                                           description='CKV_to_CKV',
                                                           intf='xe-0/0/0',
                                                           area='KV1',
                                                           speed='9000',
                                                           group_name='Tester Minh',
                                                           interface_id='1001',
                                                           user='Minhnd',
                                                           host_group_name='Minhnd test group'
                                                           )
    _tbl_kick.save()
    _tbl_kick.host_name = 'CKV01_test_lan_2'
    _tbl_kick.save()


    #for x in _tbl_lst:
    #    x.delete()
    #print(_tbl_lst)
