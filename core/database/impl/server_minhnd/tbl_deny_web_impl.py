__author__ = 'VTN-DHML-DHIP10'
import requests
import json
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.helpers.date_helpers import get_date_now, convert_date_str_to_date_obj_spring, get_date_minus, date_diff_in_seconds, \
                                        convert_date_obj_to_date_str_spring, get_date_now_format_elastic
from core.helpers.stringhelpers import convert_web_name
from core.helpers.int_helpers import convert_string_to_int
from config import Development
config = Development()
API_SPRING_IPMS_SERVER_TENANT = config.__getattribute__('API_SPRING_IPMS_SERVER_TENANT')


class TblDenyWeb:
    def __init__(self, web, usr, cv, usr_order, timestamp, usr_acc, time_acc, status, time_check,
                 deny_or_not, status_run):
        self.timestamp = timestamp
        self.web = web
        self.usr = usr
        self.cv = cv
        self.usr_order = usr_order
        self.usr_acc = usr_acc
        self.time_acc = time_acc
        self.status = status
        self.time_check = time_check
        self.deny_or_not = deny_or_not
        self.status_run = status_run
        self.Id = None

    def total(self):
        _api_tbl_kick_usr_impl = ApiSpringIpmsImpl('deny_web', API_SPRING_IPMS_SERVER_TENANT)
        # do so luong ban ghi nhieu nen lay tong tat ca va lay lan luot theo cac page
        totl_web = _api_tbl_kick_usr_impl.total()
        return totl_web

    def lst_page(self, page, page_size):
        _api_tbl_kick_usr_impl = ApiSpringIpmsImpl('deny_web', API_SPRING_IPMS_SERVER_TENANT)
        # do so luong ban ghi nhieu nen lay tong tat ca va lay lan luot theo cac page
        page_lst = _api_tbl_kick_usr_impl.get(dict(page=page, pageSize=page_size))
        if not page_lst:
            return []
        return page_lst

    def lst(self):
        _api_tbl_kick_usr_impl = ApiSpringIpmsImpl('deny_web', API_SPRING_IPMS_SERVER_TENANT)
        # do so luong ban ghi nhieu nen lay tong tat ca va lay lan luot theo cac page
        totl_web = self.total()
        res_lst = []
        if totl_web > 0:
            page_size = 1000
            page_totl = round(totl_web / page_size, 0) + 1
            for page in range(0, int(page_totl) + 1):
                res_lst += self.lst_page(page=page,
                                         page_size=page_size)
        return res_lst

    def find_web_status_total(self, web_name, status):
        _api_tbl_kick_usr_impl = ApiSpringIpmsImpl('deny_web', API_SPRING_IPMS_SERVER_TENANT)
        # do so luong ban ghi nhieu nen lay tong tat ca va lay lan luot theo cac page
        find_dct = dict(searchStatusTotal=web_name, status=str(status))
        totl_web = _api_tbl_kick_usr_impl.get(find_dct)
        if totl_web:
            return convert_string_to_int(totl_web)
        return 0

    def find_web_status(self, web_name, status):
        _api_tbl_kick_usr_impl = ApiSpringIpmsImpl('deny_web', API_SPRING_IPMS_SERVER_TENANT)
        # do so luong ban ghi nhieu nen lay tong tat ca va lay lan luot theo cac page
        totl_web = self.find_web_status_total(web_name=web_name, status=status)
        res_lst = []
        if totl_web > 0:
            page_size = 1000
            page_totl = round(totl_web / page_size, 0) + 1
            for page in range(0, int(page_totl) + 1):
                find_dct = dict(searchStatus=web_name, status=str(status), page=page,
                                pageSize=page_size)
                res_lst += _api_tbl_kick_usr_impl.get(find_dct)
        return res_lst

    def check_exist(self):
        _api_tbl_kick_usr_impl = ApiSpringIpmsImpl('deny_web', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = self.find(self.web)
        check_exst = False
        if _tbl_obj_lst:
            try:

                return True

            except Exception as err:
                print(err)
                return True
        return check_exst

    def save(self):
        # DO API Holon tu dong cong time len 7h nen ta se tru date di 7h de insert cho chuan
        time_stam_date = convert_date_str_to_date_obj_spring(self.timestamp)
        time_stam_minus_gmt = get_date_minus(time_stam_date, 7*60)
        self.timestamp = convert_date_obj_to_date_str_spring(time_stam_minus_gmt)

        if self.timestamp:
            _api_deny_web_impl = ApiSpringIpmsImpl('deny_web', API_SPRING_IPMS_SERVER_TENANT)
            chk_exst = self.check_exist()
            if chk_exst:
                try:
                    _tbl_obj = self.find(self.web)
                    _tbl_id = _tbl_obj['Id']

                    if _tbl_id:
                        self.Id = _tbl_id
                        _tbl_json = json.dumps(self, default=lambda o: o.__dict__)
                        res_put = _api_deny_web_impl.put(_tbl_id, _tbl_json)

                        if res_put:
                            return True

                except Exception as err:
                    print('Error %s when save user ID %s' % (str(err), str(self.Id)))
                    return False
            else:
                # post:
                try:
                    _tbl_json = json.dumps(self, default=lambda o: o.__dict__)
                    res_put = _api_deny_web_impl.post(_tbl_json)
                    if res_put:
                        return True

                except Exception as err:
                    print(err)
                    return False

        return False

    def find(self, web_name):
        _api_deny_web_impl = ApiSpringIpmsImpl('deny_web', API_SPRING_IPMS_SERVER_TENANT)
        # do truong hop web_name co the co chua '/' nen phai remove di tranh truong hop api hieu nham url
        web_name_new = convert_web_name(web_name)

        _tbl_obj_lst = _api_deny_web_impl.get(dict(web=web_name_new))
        if _tbl_obj_lst:
            # return thang chinh xac nhat
            for _obj in _tbl_obj_lst:
                web_name_obj = _obj['web']
                if 'cv' in _obj:
                    cv = _obj['cv']
                else:
                    cv = ''
                if web_name_obj == web_name and str(cv).upper() == str(self.cv).upper():
                    return _obj
            return _tbl_obj_lst[0]
        else:
            # post:

            return dict()

    def find_time(self, timestamp):
        _api_deny_web_impl = ApiSpringIpmsImpl('deny_web', API_SPRING_IPMS_SERVER_TENANT)
        # do truong hop web_name co the co chua '/' nen phai remove di tranh truong hop api hieu nham url

        _tbl_obj_lst = _api_deny_web_impl.get(dict(timestampGreater=str(timestamp)))
        if not _tbl_obj_lst:
            return []
        return _tbl_obj_lst

    def delete(self):
        _api_deny_web_impl = ApiSpringIpmsImpl('deny_web', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = self.find(self.web)
        if _tbl_obj_lst:
            try:
                _tbl_obj = _tbl_obj_lst[0]
                _tbl_id = _tbl_obj['Id']
                if _tbl_id:
                    res_del = _api_deny_web_impl.delete(_tbl_id)

                    if res_del:
                        return True

            except Exception as err:
                print(err)
                return False
        else:
            # post:

            return False
        return False

    @staticmethod
    def req_web(url):
        res_conn = True
        status = set()
        # chuyen sang lower
        url = url.lower()
        try:
            # time out goc la 60. Fix ve 30 de xem ket qua ntn. 60 thi 1 ngay chi duoc 500 trang web
            res = requests.get(url, timeout=30)
            status.add(res.status_code)
            print(status)
            web_cnt = res.text
            if int(res.status_code) == 404:
                # page not found
                res_conn = False
            elif int(res.status_code) >= 400 and int(res.status_code) != 500 and web_cnt.find('404 Page') < 0:
                pass
            elif int(res.status_code) == 200 and web_cnt.find('404 Page') < 0:
                pass
            elif 300 <= int(res.status_code) <= 399:
                res_conn = False
            else:
                res_conn = False

        except Exception as err:
            print('Error Connect web %s Not OK' % str(err))
            res_conn = False

        finally:
            return res_conn

    def conn_web(self, web_name):
        res_conn = False
        time_chck = 0
        while not res_conn:
            if web_name.find('http') < 0:
                url = "http://" + web_name
                res_conn = self.req_web(url)
                if not res_conn:
                    url = "https://" + web_name
                    res_conn = self.req_web(url)

                else:
                    return res_conn
            else:
                url = web_name
                res_conn = self.req_web(url)
                if res_conn:
                    return res_conn
                else:
                    break

            time_chck += 1
            if time_chck > 1:
                break
            print(time_chck)
        print("Result when connect %s is %s" % (str(web_name), str(res_conn)))
        return res_conn


if __name__ == '__main__':
    time_ins = get_date_now_format_elastic()
    _tbl_kick = TblDenyWeb(web="tester",
                           usr="",
                           cv="cv",
                           usr_order="usr",
                           timestamp=time_ins,
                           usr_acc="acc",
                           time_acc=time_ins,
                           status=0,
                           time_check=time_ins,
                           deny_or_not='',
                           status_run='')
    #_tbl_get_lst = _tbl_kick.list()
    #_tbl_lst = _tbl_kick.save()
    test = _tbl_kick.conn_web('s1288.net')
    print(test)
    #print(_tbl_lst)

