__author__ = 'VTN-DHML-DHIP10'
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.helpers.int_helpers import convert_string_to_float
from core.helpers.tenant_helpers import get_tenant_id_from_area, get_tenant_id, get_main_ipms
from core.helpers.date_helpers import convert_date_obj_to_date_str_ipms
from core.helpers.date_helpers import get_date_now, convert_date_str_to_date_obj_spring, get_date_minus, \
    date_diff_in_seconds, \
    convert_date_obj_to_date_str_spring, get_date_now_format_elastic
import json
import pymysql
from core.database.impl.ipms.host_ipms_impl import HostIpmsImpl
from core.database.impl.ipms.intf_ipms_impl import InterfaceImpsImpl
from core.database.impl.ipms.poller_ipms_impl import PollerIpmsImpl
from config import Development

config = Development()
API_SPRING_IPMS_SERVER_TENANT = config.__getattribute__('API_SPRING_IPMS_SERVER_TENANT')
SERVER_HOST = config.__getattribute__('SERVER_HOST')
SERVER_HOST_USER = config.__getattribute__('SERVER_HOST_USER')
SERVER_HOST_PASS = config.__getattribute__('SERVER_HOST_PASS')


class TblUseronlineBrasImpl:
    def __init__(self, max, current, max_input, link_name, interface_id, rtg_url, time_begin, node_name,
                 area, efficient):
        self.Id = None
        self.idInterface = interface_id
        self.max = max
        self.current = current
        self.area = area
        self.maxInput = max_input
        self.linkName = link_name
        self.rtg_url = rtg_url
        self.nodeName = node_name
        self.efficient = efficient
        self.time_begin = time_begin

    def list(self):
        _api_tbl_intf_not_impl = ApiSpringIpmsImpl('useronline', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_intf_not_impl.lst()
        res_lst = list()
        " "
        if _tbl_obj_lst:

            for _tbl_obj in _tbl_obj_lst:
                try:
                    _tbl_fnd_obj = TblUseronlineBrasImpl(max=_tbl_obj['max'],
                                                         current=_tbl_obj['current'],
                                                         max_input=_tbl_obj['max_input'],
                                                         link_name=_tbl_obj['link_name'],
                                                         area=_tbl_obj['area'],
                                                         interface_id=_tbl_obj['interface_id'],
                                                         rtg_url=_tbl_obj['rtg_url'],
                                                         time_begin=_tbl_obj['time_begin'],
                                                         node_name=_tbl_obj['node_name'],
                                                         efficient=_tbl_obj['efficient']
                                                         )
                    _tbl_fnd_obj.Id = _tbl_obj['Id']
                    res_lst.append(_tbl_fnd_obj)
                except Exception as err:
                    print("Error %s when list table Useronline " % str(err))

        return res_lst

    def list_page(self, page, page_size):
        _api_tbl_intf_not_impl = ApiSpringIpmsImpl('useronline', API_SPRING_IPMS_SERVER_TENANT)
        page_dct = dict(page=page, pageSize=page_size)
        _tbl_obj_lst = _api_tbl_intf_not_impl.get(page_dct)
        res_lst = list()

        if _tbl_obj_lst:

            for _tbl_obj in _tbl_obj_lst:
                try:
                    _tbl_fnd_obj = TblUseronlineBrasImpl(max=_tbl_obj['max'],
                                                         current=_tbl_obj['current'],
                                                         max_input=_tbl_obj['max_input'],
                                                         link_name=_tbl_obj['link_name'],
                                                         area=_tbl_obj['area'],
                                                         interface_id=_tbl_obj['interface_id'],
                                                         rtg_url=_tbl_obj['rtg_url'],
                                                         time_begin=_tbl_obj['time_begin'],
                                                         node_name=_tbl_obj['node_name'],
                                                         efficient=_tbl_obj['efficient']
                                                         )
                    _tbl_fnd_obj.Id = _tbl_obj['Id']
                    res_lst.append(_tbl_fnd_obj)
                except Exception as err:
                    print("Error %s when list table Useronline " % str(err))

        return res_lst

    @staticmethod
    def total():
        _api_tbl_intf_not_impl = ApiSpringIpmsImpl('useronline', API_SPRING_IPMS_SERVER_TENANT)
        _tot_str = _api_tbl_intf_not_impl.total()
        _tot_num = convert_string_to_float(_tot_str)
        return _tot_num

    @staticmethod
    def find_like(**kwargs):
        _api_impl = ApiSpringIpmsImpl('useronline', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_impl.get(kwargs)

        return _tbl_obj_lst

    @staticmethod
    def total_key_name(key_name):
        _api_impl = ApiSpringIpmsImpl('useronline', API_SPRING_IPMS_SERVER_TENANT)

        try:
            _total_str = _api_impl.get(dict(searchTotal=key_name))
            _total_num = convert_string_to_float(_total_str)
            return _total_num

        except Exception as err:
            print("Error %s when get total of key name %s " % str(err), key_name)
            return 0

    def check_exist(self):
        _api_tbl_intf_not_impl = ApiSpringIpmsImpl('useronline', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_intf_not_impl.get(dict(interface_id=self.idInterface,
                                                       area=self.area,
                                                       timestamp=self.time_begin))
        check_exst = False
        if _tbl_obj_lst:
            try:
                for _tbl_obj in _tbl_obj_lst:
                    _tbl_fnd_obj = TblUseronlineBrasImpl(max=_tbl_obj['max'],
                                                         current=_tbl_obj['current'],
                                                         max_input=_tbl_obj['max_input'],
                                                         link_name=_tbl_obj['link_name'],
                                                         area=_tbl_obj['area'],
                                                         interface_id=_tbl_obj['interface_id'],
                                                         rtg_url=_tbl_obj['rtg_url'],
                                                         time_begin=_tbl_obj['time_begin'],
                                                         node_name=_tbl_obj['node_name'],
                                                         efficient=_tbl_obj['efficient']
                                                         )

                    return True

            except Exception as err:
                print(err)
                return True
        return check_exst

    def get_bras_lst(self):
        # Tim ra nhung node la BRAS
        hst_impl = HostIpmsImpl(0, '', 0, '', '', '')
        intf_impl = InterfaceImpsImpl(0, '', 0, 0, '', '')
        tent_id = get_tenant_id_from_area(self.area)
        tent_main_ip = get_main_ipms(self.area)
        poll_impl = PollerIpmsImpl(0, '',  tent_main_ip)
        res_user_lst = []

        if tent_id:
            fnd_bra_dct = dict(searchHostName='BR')
            bra_lst = hst_impl.find_host(tent_id, **fnd_bra_dct)
            if bra_lst:

                for x in bra_lst:
                    try:
                        fnd_user_dct = dict(searchRid=x.rid, searchInterfaceName='ser')
                        res_intf_lst = intf_impl.find_intf(tent_id, **fnd_user_dct)
                        if res_intf_lst:
                            # lay luu luong
                            for y in res_intf_lst:

                                poll_name = x.poller
                                if poll_name:
                                    poll_brs = poll_impl.find_poll(poll_name)
                                    if poll_brs:

                                        tent_bras_id = get_tenant_id(poll_brs.poller_ip)
                                        if tent_bras_id:
                                            # time_begin, time_end, tenant_id, poller_name, rid

                                            time_stam_minus_gmt = get_date_minus(self.time_begin, minute_mins=10)
                                            time_end_frm = convert_date_obj_to_date_str_ipms(self.time_begin)
                                            time_begin_frm = convert_date_obj_to_date_str_ipms(time_stam_minus_gmt)
                                            user_onl = y.get_max_custom(time_begin_frm,
                                                                        time_end_frm,
                                                                        tent_bras_id,
                                                                        x.poller,
                                                                        x.rid)
                                            max_user_onl = y.speed

                                            efficient = user_onl * 100 / max_user_onl

                                            tbl_user_obj = TblUseronlineBrasImpl(max=max_user_onl,
                                                                                 current=user_onl,
                                                                                 max_input=user_onl,
                                                                                 link_name=y.name,
                                                                                 interface_id=y.id,
                                                                                 rtg_url='',
                                                                                 time_begin=self.time_begin,
                                                                                 node_name=x.hostname,
                                                                                 area=self.area,
                                                                                 efficient=efficient)
                                            res_user_lst.append(tbl_user_obj)

                    except Exception as err:
                        print("Error %s when get Bras interface useronline " % str(err))

            # Tim ra nhung link la Useronline
            # Tim ra So useronline
            # ghi lai
        return res_user_lst

    def save(self):
        # DO API Holon tu dong cong time len 7h nen ta se tru date di 7h de insert cho chuan

        time_stam_date = self.time_begin
        time_stam_minus_gmt = get_date_minus(time_stam_date, minute_mins=60 * 7)
        self.time_begin = convert_date_obj_to_date_str_spring(time_stam_minus_gmt)

        if self.time_begin:
            _api_impl = ApiSpringIpmsImpl('useronline', API_SPRING_IPMS_SERVER_TENANT)
            chk_exst = self.check_exist()

            if chk_exst:
                try:
                    _tbl_obj_lst = _api_impl.get(dict(area=self.area,
                                                      interface_id=self.idInterface,
                                                      timestamp=self.time_begin))
                    _tbl_obj = _tbl_obj_lst[0]
                    _tbl_id = _tbl_obj['Id']
                    if _tbl_id:
                        self.Id = _tbl_id
                        _tbl_json = json.dumps(self, default=lambda o: o.__dict__)
                        res_put = _api_impl.put(_tbl_id, _tbl_json)
                        if res_put:
                            return True
                except Exception as err:
                    print('Error %s when save tbl_useronline_bras ID %s' % str(err), str(self.Id))
                    return False
            else:
                # post:
                try:
                    _tbl_json = json.dumps(self, default=lambda o: o.__dict__)
                    res_put = _api_impl.post(_tbl_json)
                    if res_put:
                        return True
                except Exception as err:
                    print(err)
                    return False

        return False

    def save_lst(self, user_onl_br_lst):
        for x in user_onl_br_lst:
            try:
                x.save()
            except Exception as err:
                print("Error %s when save list bras online" % str(err))

    def delete(self):
        _api_impl = ApiSpringIpmsImpl('useronline', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_impl.get(dict(area=self.area,
                                          interface_id=self.idInterface,
                                          timestamp=self.time_begin))
        if _tbl_obj_lst:
            try:
                _tbl_obj = _tbl_obj_lst[0]
                _tbl_id = _tbl_obj['Id']
                if _tbl_id:
                    res_del = _api_impl.delete(_tbl_id)

                    if res_del:
                        return True

            except Exception as err:
                print(err)
                return False
        else:
            # post:

            return False
        return False

    @staticmethod
    def get_latest_total_user(usr_lst):
        time_max = ''
        total = 0
        if usr_lst:
            try:
                usr_ltst = usr_lst[-1]
                if 'time_begin' in usr_ltst:
                    time_max = usr_ltst['time_begin']
                    usr_time_max_lst = list(filter(lambda usr_lst: usr_lst['time_begin'] == time_max, usr_lst))
                    usr_ipv4_lst = list(
                        filter(lambda usr_time_max_lst: usr_time_max_lst['linkName'] == 'User_IPv4_Online',
                               usr_time_max_lst))
                    if not usr_ipv4_lst:
                        usr_ipv4_lst = list(
                            filter(lambda usr_time_max_lst: usr_time_max_lst['linkName'] == 'UserOnline',
                                   usr_time_max_lst))

                    total = sum(int(x['maxInput']) for x in usr_ipv4_lst)
                    return total
            except Exception as err:
                print("Error %s when get latest total user" % err)
                return total
        return total

    @staticmethod
    def get_avg_reduce_abnormal(tme_lst, diff_range):
        avg = 0
        tme_flt_lst = []
        indx_tme = 0
        sum_ttl = 0
        tme_dct = dict()
        # time_lst.append(dict(time_begin=time_x, user=user_x))
        for time_dct in tme_lst:
            try:
                if tme_dct:
                    _usr_las = tme_dct.get('user')
                    _usr_now = time_dct.get('user')
                    if _usr_las > 0:
                        if abs((_usr_now - _usr_las) * 100 / _usr_las) > diff_range:
                            # thay doi bat thuong can check tiep. So sanh voi mau luc sau
                            tme_nxt_dct = tme_lst[indx_tme + 1]
                            tme_nxt_2_dct = tme_lst[indx_tme + 2]
                            if tme_nxt_dct:
                                _usr_nxt = tme_nxt_dct.get('user')
                                _usr_nxt_2 = tme_nxt_2_dct.get('user')

                                if abs((_usr_nxt - _usr_las) * 100 / _usr_las) > \
                                        diff_range and abs((_usr_nxt_2 - _usr_las) * 100 / _usr_las) > diff_range:
                                    tme_flt_lst.append(time_dct)
                                    sum_ttl += _usr_now
                                    tme_dct = dict(time_begin=time_dct.get('time_begin'), user=_usr_now)
                        else:
                            tme_flt_lst.append(time_dct)
                            sum_ttl += _usr_now
                            tme_dct = dict(time_begin=time_dct.get('time_begin'), user=_usr_now)
                else:
                    tme_dct = dict(time_begin=time_dct.get('time_begin'), user=time_dct.get('user'))
                    tme_flt_lst.append(tme_dct)
                    sum_ttl += time_dct.get('user')
                indx_tme += 1
            except Exception as err:
                print("Error %s when get avg reduce abnormal" % err)

        avg_ttl = round(sum_ttl / len(tme_flt_lst), 2)
        return avg_ttl


    def get_max_avg_total_user(self, usr_lst):
        max_ttl = 0
        avg_ttl = 0
        sum_ttl = 0
        max_tme = ''
        time_lst = []
        if usr_lst:
            try:
                for x in usr_lst:
                    if 'time_begin' in x:
                        time_x = x['time_begin']
                        user_x = x['user']
                        # tim trong time list. Neu co time begin roi thi cong vao max total. Neu khong co thi them moi
                        if time_lst:
                            chck_tme_new = True
                            for tme_usr in time_lst:
                                if 'time_begin' in tme_usr:
                                    tme_usr_bgn = tme_usr['time_begin']
                                    if time_x == tme_usr_bgn:
                                        if 'user' in tme_usr:
                                            tme_usr['user'] += user_x
                                            sum_ttl += user_x
                                            chck_tme_new = False
                            if chck_tme_new:
                                time_lst.append(dict(time_begin=time_x, user=user_x))
                                sum_ttl += user_x
                        else:
                            time_lst.append(dict(time_begin=time_x, user=user_x))
                            sum_ttl += user_x

                # tinh max
                if time_lst:
                    # tinh avg
                    avg_ttl = round(sum_ttl / len(time_lst), 2)
                    avg_ttl = self.get_avg_reduce_abnormal(time_lst, 10)
                    for tme in time_lst:
                        if 'user' in tme and 'time_begin' in tme:
                            usr_x = tme['user']
                            if usr_x > max_ttl:
                                max_ttl = usr_x
                                max_tme = tme['time_begin']
                return max_ttl, avg_ttl, max_tme

            except Exception as err:
                print("Error %s when get max total user" % err)
                return max_ttl, avg_ttl, max_tme
        return max_ttl, avg_ttl, max_tme

    @staticmethod
    def get_user_by_fld(fld_name, usr_lst):
        max_ttl = 0
        max_tme = ''
        time_lst = []
        if usr_lst:
            try:
                for x in usr_lst:
                    if 'time_begin' in x:
                        time_x = x['time_begin']
                        if 'linkName' in x:
                            link_name = x['linkName']
                            if link_name == fld_name and 'maxInput' in x:
                                user_x = x['maxInput']
                                # tim trong time list. Neu co time begin roi thi cong vao max total. Neu khong co thi them moi
                                if time_lst:
                                    chck_tme_new = True
                                    for tme_usr in time_lst:
                                        if 'time_begin' in tme_usr:
                                            tme_usr_bgn = tme_usr['time_begin']
                                            if time_x == tme_usr_bgn:
                                                if 'user' in tme_usr:
                                                    tme_usr['user'] += user_x
                                                    chck_tme_new = False
                                    if chck_tme_new:
                                        time_lst.append(dict(time_begin=time_x, user=user_x))
                                else:
                                    time_lst.append(dict(time_begin=time_x, user=user_x))

            except Exception as err:
                print("Error %s when get max total user" % err)
                return time_lst
        return time_lst

    def get_conn(self):
        try:
            conn = pymysql.connect(host=SERVER_HOST, user=SERVER_HOST_USER, password=SERVER_HOST_PASS, db='luuluong')

        except Exception as err:
            print(err)
            return None
        return conn

    def delete_all(self, timestamp):

        conn = self.get_conn()
        cbn_bkk_obj = None
        sql_qry = "DELETE FROM tbl_useronline_bras WHERE time_begin < %s"
        if conn:
            cursor = conn.cursor()
            try:
                # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                res = cursor.execute(sql_qry, (timestamp))
                conn.commit()
                print('DELETE into tbl_useronline_bras OK')

            except Exception as err:
                print(err)
                return False
            finally:
                cursor.close()
                conn.close()
        return True

    def count_all_greater(self, timestamp):

        conn = self.get_conn()
        cbn_bkk_obj = None
        sql_qry = "select count(*) as count from tbl_useronline_bras where time_begin > %s"
        if conn:
            cursor = conn.cursor()
            try:
                # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                res = cursor.execute(sql_qry, (timestamp))
                conn.commit()
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                if res_lst:
                    if 'count' in res_lst[0]:
                        return res_lst[0]['count']

                return 0
            except Exception as err:
                print(err)
                return 0
            finally:
                cursor.close()
                conn.close()
        return 0

if __name__ == '__main__':
    time_ins = get_date_now()
    _tbl_kick = _tbl_fnd_obj = TblUseronlineBrasImpl(max=9000,
                                                     current=500,
                                                     max_input=9000,
                                                     link_name='Useronline',
                                                     area='KV3',
                                                     interface_id=2004,
                                                     rtg_url='http://hehe',
                                                     time_begin=time_ins,
                                                     node_name='CKV09',
                                                     efficient=73
                                                     )
    from core.helpers.date_helpers import get_date_now, get_date_minus, convert_date_to_ipms
    time_now = get_date_now()
    time_lst_15_mins = get_date_minus(time_now, 145)

    time_now_conv = convert_date_to_ipms(time_now)
    time_lst_15_mins_conv = convert_date_to_ipms(time_lst_15_mins)

    _srch_tst_dct = dict(searchProvince='THA', timestampGreat=time_lst_15_mins_conv, timestampLess=time_now_conv)
    _res_lst = _tbl_kick.find_like(**_srch_tst_dct)

    #test_max, test_time = _tbl_kick.get_max_total_user(_res_lst)
    print(test_max)

    # for x in _tbl_lst:
    #    x.delete()
    # print(_tbl_lst)
