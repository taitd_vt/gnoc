import mysql.connector.pooling
import time
import pymysql
from config import Development
config = Development()
API_SPRING_IPMS_SERVER_TENANT = config.__getattribute__('API_SPRING_IPMS_SERVER_TENANT')

SERVER_HOST = "192.168.251.15"
SERVER_HOST_USER = "linhlk1"
SERVER_HOST_PASS = "linhlk135"


class ServerMysqlConnect(object):
    def __init__(self, host, port, user, password, database, pool_name="mypool", pool_size=5):
        res = {}
        self._host = host
        self._port = port
        self._user = user
        self._password = password
        self._database = database

        res = dict(host=self._host,
                   port=self._port,
                   user=self._user,
                   password=self._password,
                   database=self._database)

        self.dbconfig = res
        self.pool = self.create_pool(pool_name=pool_name,
                                     pool_size=pool_size)

    def get_conn(self):
        try:
            conn = pymysql.connect(host=SERVER_HOST,
                                   user=SERVER_HOST_USER,
                                   password=SERVER_HOST_PASS,
                                   db='luuluong')

        except Exception as err:
            print(err)
            return None
        return conn

    def create_pool(self, pool_name="mypool", pool_size=10):
        pool = mysql.connector.pooling.MySQLConnectionPool(pool_name=pool_name,
                                                           pool_size=pool_size,
                                                           pool_reset_session=True,
                                                           connection_timeout=100000,
                                                           **self.dbconfig)

        return pool

    def close(self, conn, cursor):
        cursor.close()
        conn.close()

    def reset_pool(self):
        pass

    def execute(self, sql, args=None, commit=False):
        conn = self.get_conn()
        cursor = conn.cursor()
        res = []
        #123
        try:
            if args:
                cursor.execute(sql, args)
            else:
                cursor.execute(sql)
            if commit is True:
                conn.commit()

            else:
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res = [dict(zip(col_name_lst, row)) for row in data]

        except Exception as err:
            print("Error %s when excute Mysql Pool " % str(err))
        finally:
            self.close(conn, cursor)
            #pass
        return res

    def executemany(self, sql, args, commit=False):
        """
        Execute with many args. Similar with executemany() function in pymysql.
        args should be a sequence.
        :param sql: sql clause
        :param args: args
        :param commit: commit or not.
        :return: if commit, return None, else, return result
        """
        # get connection form connection pool instead of create one.
        conn = self.get_conn()
        cursor = conn.cursor()
        res = []
        try:

            cursor.executemany(sql, args)
            if commit is True:
                conn.commit()

            else:
                res = cursor.fetchall()

                return res
        except Exception as err:
            print("Error %s when excute many Mysql Pools" % err)
        finally:
            self.close(conn, cursor)
            #pass
        return res


if __name__ == "__main__":
    dbconfig = {
        "host": "192.168.251.15",
        "port": 3306,
        "user": "linhlk1",
        "password": "linhlk135",
        "database": "luuluong",
    }
    date_time = '2020-02-18'
    mysql_pool = ServerMysqlConnect(**dbconfig)
    from core.database.impl.server_minhnd.tbl_interface_drop_impl import TblInterfaceDropImpl
    #sql = 'select date_format(t1.timestamp, "%Y-%m-%d %H:%i") as time_frm, t2.total from tbl_interface_drop t1 inner join (select sum(traffic_drop) as total, date_format(timestamp, "%Y-%m-%d %H:%i") as time_frm from tbl_interface_drop where timestamp like "%' + str(date_time) + '%" group by date_format(timestamp, "%Y-%m-%d %H:%i")) t2 on date_format(t1.timestamp, "%Y-%m-%d %H:%i") = t2.time_frm and t2.total > 0 group by date_format(t1.timestamp, "%Y-%m-%d %H:%i") order by t2.total desc limit 0,1'
    sql = ' SELECT * from tbl_interface_drop where timestamp like "%2020-03-12%"'
    _tbl = TblInterfaceDropImpl(date_time)
    # test...
    while True:
        t0 = time.time()
        for i in range(1000):
            res = mysql_pool.execute(_tbl.get_drop_max())
            res_now = mysql_pool.execute(_tbl.get_drop_now())
            print(res)
            print(res_now)
            print("Time ", str(i))
        print("time cousumed:", time.time() - t0)