class TblInterfaceDropImpl():
    def __init__(self, time_begin):
        self.time_begin = time_begin

    def get_max_time(self):
        sql = 'Select max(timestamp) from tbl_interface_drop'
        return sql

    def get_total_drop_on_time(self, time_stamp):
        sql = "SELECT sum(traffic_drop) from tbl_interface_drop where timestamp like '%" + str(time_stamp) + "%'"
        return sql

    def get_drop_now(self):
        # do co van de ve timestamp. Ta se tim max timestamp hien tai va select like de tim
        sql = 'select date_format(t1.timestamp, "%Y-%m-%d %H:%i") as time_frm, t2.total from tbl_interface_drop t1 inner join (select sum(traffic_drop) as total, date_format(timestamp, "%Y-%m-%d %H:%i") as time_frm from tbl_interface_drop where timestamp like "%' + str(self.time_begin) + '%" group by date_format(timestamp, "%Y-%m-%d %H:%i")) t2 on date_format(t1.timestamp, "%Y-%m-%d %H:%i") = t2.time_frm and t2.total > 0 group by date_format(t1.timestamp, "%Y-%m-%d %H:%i") order by time_frm desc limit 0,1'
        return sql

    def get_drop_max(self):
        sql = 'select date_format(t1.timestamp, "%Y-%m-%d %H:%i") as time_frm, t2.total from tbl_interface_drop t1 inner join (select sum(traffic_drop) as total, date_format(timestamp, "%Y-%m-%d %H:%i") as time_frm from tbl_interface_drop where timestamp >= "' + str(self.time_begin) + '" group by date_format(timestamp, "%Y-%m-%d %H:%i")) t2 on date_format(t1.timestamp, "%Y-%m-%d %H:%i") = t2.time_frm and t2.total > 0 group by date_format(t1.timestamp, "%Y-%m-%d %H:%i") order by t2.total desc limit 0,1'
        return sql

    def get_total_drop_node_now(self):
        sql = 'select round(sum(traffic_drop) * 8 / 1000000,2) as total, node_name, t2.timestamp from tbl_interface_drop t1 INNER JOIN (select timestamp from tbl_interface_drop where timestamp like "%' + str(self.time_begin) + '%" order by timestamp desc limit 0,1) t2 where t1.timestamp = t2.timestamp group by node_name'
        return sql

    def get_total_drop_node(self):
        sql = 'select round(sum(traffic_drop) * 8 / 1000000,2) as total, node_name, timestamp from tbl_interface_drop where timestamp like "%' + str(self.time_begin) + '%" group by node_name'
        return sql

    def get_total_drop_max_node_24h(self):
        sql = 'select round(sum(traffic_drop) * 8 / 1000000,2) as total, node_name from tbl_interface_drop where timestamp  >= "' + str(self.time_begin) + '" group by node_name'
        return sql

if __name__ == '__main__':
    from core.database.impl.server_minhnd.server_mysql_connect import ServerMysqlConnect
    from core.helpers.date_helpers import convert_date_obj_to_date_str_ipms
    _now_only_date = '2021-01-09'
    dbconfig = {
        "host": "192.168.251.15",
        "port": 3306,
        "user": "linhlk1",
        "password": "linhlk135",
        "database": "luuluong",
    }
    mysql_pool = ServerMysqlConnect(**dbconfig)
    res_info = ''

    _tbl = TblInterfaceDropImpl(_now_only_date)

    res = mysql_pool.execute(_tbl.get_max_time())
    if res:
        for key, val in res[0].items():
            time_drop = val
            time_drop_str = convert_date_obj_to_date_str_ipms(time_drop)
            pos_dbl_dot = str(time_drop_str).find(":")
            if pos_dbl_dot >= 0:
                time_drop_min = time_drop_str[:pos_dbl_dot + 3]
                if time_drop_min:
                    res_drop_lst = mysql_pool.execute(_tbl.get_total_drop_on_time(time_drop_min))
                    if res_drop_lst:
                        for key1, val1 in res_drop_lst[0].items():
                            if val1:
                                ttl_drop = round(val1 * 8 / 1000000, 2)
                                res_info += "Total Drop: " + str(ttl_drop) + " Mbps on " + str(
                                    time_drop) + "\n"
                                break
    print(res_info)