__author__ = 'VTN-DHML-DHIP10'
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.helpers.int_helpers import convert_string_to_float
import json
from config import Development
from core.helpers.date_helpers import get_date_now, convert_date_str_to_date_obj_spring, get_date_minus, \
    date_diff_in_seconds, \
    convert_date_obj_to_date_str_spring, get_date_now_format_elastic

config = Development()
API_SPRING_IPMS_SERVER_TENANT = config.__getattribute__('API_SPRING_IPMS_SERVER_TENANT')


class UpdateResultNocpros:
    def __init__(self, nodeCode):
        self.nodeCode = nodeCode
        self.ListCommands = ListCommands

    def __iter__(self):
        return iter(self.ListCommands)


class ListCommands():
    def __init__(self, GroupAction, result, Action, mapParamOutput, command, finalResult):
        self.GroupAction = GroupAction
        self.result = result
        self.Action = Action
        self.mapParamOutput = mapParamOutput
        self.command = command
        self.finalResult = finalResult


class TblMopVmsaResultServerImpl:
    def __init__(self, result, mopId, resultFinal, resultDetail):
        self.result = result
        self.mopId = mopId
        self.resultFinal = resultFinal
        self.resultDetail = resultDetail
        self.Id = None

    def get_not_duplicate_lst(self, dup_lst):
        # muc dich tim list khong bi duplicate
        res_lst = []
        for x in dup_lst:
            chek_exst = False
            if res_lst:
                for y in res_lst:
                    if self.check_duplicate(x):
                        chek_exst = True
                        break

                if not chek_exst:
                    res_lst.append(x)
            else:
                res_lst.append(x)
        return res_lst

    def get(self):
        _api_tbl_kpi_impl = ApiSpringIpmsImpl('vmsa_result', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_kpi_impl.get(dict(mopId=self.mopId))
        if _tbl_obj_lst:
            try:
                for _tbl_obj in _tbl_obj_lst:
                    _tbl_fnd_obj = TblMopVmsaResultServerImpl(result=_tbl_obj['result'],
                                                              mopId=_tbl_obj['mopId'],
                                                              resultFinal=_tbl_obj['resultFinal'],
                                                              resultDetail=_tbl_obj['resultDetail'],
                                                              )

                    _tbl_fnd_obj.Id = _tbl_obj['Id']
                    return _tbl_fnd_obj

            except Exception as err:
                print(err)
                return None

    def get_mop_id(self, mop_id):
        _api_tbl_kpi_impl = ApiSpringIpmsImpl('vmsa_result', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_kpi_impl.get(dict(mopId=mop_id))
        if _tbl_obj_lst:
            try:

                return _tbl_obj_lst[0]

            except Exception as err:
                print(err)
                return None

    def save(self):
        _api_tbl_kpi_impl = ApiSpringIpmsImpl('vmsa_result', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_kpi_impl.get(dict(mopId=self.mopId))
        # tru di 7h
        #time_stam_date = convert_date_str_to_date_obj_spring(self.timestamp)
        #time_stam_minus_gmt = get_date_minus(time_stam_date, minute_mins=60 * 7)
        #self.timestamp = convert_date_obj_to_date_str_spring(time_stam_minus_gmt)

        if _tbl_obj_lst:
            try:
                _tbl_obj = _tbl_obj_lst[0]
                _tbl_id = _tbl_obj['Id']
                if _tbl_id:
                    self.Id = _tbl_id
                    _tbl_json = json.dumps(self, default=lambda o: o.__dict__)
                    res_put = _api_tbl_kpi_impl.put(_tbl_id, _tbl_json)

                    if res_put:
                        return True

            except Exception as err:
                print(err)
                return False
        else:
            # post:
            try:
                _tbl_json = json.dumps(self, default=lambda o: o.__dict__)
                res_put = _api_tbl_kpi_impl.post(_tbl_json)
                if res_put:
                    return True

            except Exception as err:
                print(err)
                return False
        return False

    def delete(self):
        _api_tbl_kpi_impl = ApiSpringIpmsImpl('vmsa_result', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_kpi_impl.get(dict(mopId=self.mopId))
        if _tbl_obj_lst:
            try:
                _tbl_obj = _tbl_obj_lst[0]
                _tbl_id = _tbl_obj['Id']
                if _tbl_id:
                    res_del = _api_tbl_kpi_impl.delete(_tbl_id)

                    if res_del:
                        return True

            except Exception as err:
                print(err)
                return False
        else:
            # post:

            return False
        return False

    def pars_res_dtl(self, mop_id):
        _obj = self.get_mop_id(mop_id)
        _res_json_1 = []
        # Mac dinh cau truc cua result detail la :['nodeCode':'', 'ListCommands':[dict(GroupAction=x, Action=y,command=z)]]
        if _obj:
            if 'resultDetail' in _obj:
                _res_dtl_str = _obj['resultDetail']
                try:
                    _res_json_1 = json.loads(_res_dtl_str)

                except TypeError as err:
                    print("Error %s when load json" % str(err))
                    return []
        return _res_json_1

if __name__ == '__main__':
    _obj = TblMopVmsaResultServerImpl(result='',
                                      mopId=84420291,
                                      resultFinal='',
                                      resultDetail='')

    test_obj = _obj.pars_res_dtl(84420291)
    #print(test_obj)


