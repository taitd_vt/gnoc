__author__ = 'VTN-DHML-DHIP10'
import json
import time
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.database.impl.server_minhnd.tbl_ip_addr_impl import TblIpServerImpl
from core.helpers.stringhelpers import convert_web_name
from core.helpers.date_helpers import get_date_now, convert_date_str_to_date_obj_spring, get_date_minus, date_diff_in_seconds, \
                                        convert_date_obj_to_date_str_spring, get_date_now_format_elastic
from config import Development
from dns import resolver
config = Development()
API_SPRING_IPMS_SERVER_TENANT = config.__getattribute__('API_SPRING_IPMS_SERVER_TENANT')


class TblDenyWebLookUp:
    def __init__(self, web, ip_addr, timestamp, document, as_name, as_num, area, status,
                 check_rr, check_dns, timestamp_check_rr, timestamp_check_dns):
        self.timestamp = timestamp
        self.web = web
        self.ip_addr = ip_addr
        self.Id = None
        self.document = document
        self.as_name = as_name
        self.as_num = as_num
        self.area = area
        self.status = status
        self.check_rr = check_rr
        self.check_dns = check_dns
        self.timestamp_check_rr = timestamp_check_rr
        self.timestamp_check_dns = timestamp_check_dns

    def lst_page(self, page, page_size):
        _api_tbl_kick_usr_impl = ApiSpringIpmsImpl('deny_web_lookup', API_SPRING_IPMS_SERVER_TENANT)
        # do so luong ban ghi nhieu nen lay tong tat ca va lay lan luot theo cac page
        page_lst = _api_tbl_kick_usr_impl.get(dict(page=page, pageSize=page_size))
        if not page_lst:
            return []
        return page_lst

    def lst(self):
        _api_tbl_kick_usr_impl = ApiSpringIpmsImpl('deny_web_lookup', API_SPRING_IPMS_SERVER_TENANT)
        # do so luong ban ghi nhieu nen lay tong tat ca va lay lan luot theo cac page
        totl_web = self.total()
        res_lst = []
        if totl_web > 0:
            page_size = 1000
            page_totl = round(totl_web / page_size, 0) + 1
            for page in range(int(page_totl) + 1):
                res_lst += self.lst_page(page=page,
                                         page_size=page_size)
        return res_lst

    def total(self):
        _api_tbl_kick_usr_impl = ApiSpringIpmsImpl('deny_web_lookup', API_SPRING_IPMS_SERVER_TENANT)
        # do so luong ban ghi nhieu nen lay tong tat ca va lay lan luot theo cac page
        totl_web = _api_tbl_kick_usr_impl.total()
        return totl_web

    def check_exist(self):
        _api_tbl_kick_usr_impl = ApiSpringIpmsImpl('deny_web_lookup', API_SPRING_IPMS_SERVER_TENANT)
        web_name_new = convert_web_name(self.web)
        _tbl_obj_lst = _api_tbl_kick_usr_impl.get(dict(searchWeb=web_name_new,
                                                       ip=self.ip_addr))
        # kiem tra them xem co trung document khong ?
        check_exst = False
        if _tbl_obj_lst:
            try:
                for x in _tbl_obj_lst:
                    if 'document' in x:
                        docm = x['document']
                        if str(docm).upper() == str(self.document).upper():
                            return True

            except Exception as err:
                print(err)
                return check_exst
        return check_exst

    def find(self, web_name):
        _api_deny_web_impl = ApiSpringIpmsImpl('deny_web_lookup', API_SPRING_IPMS_SERVER_TENANT)
        # do truong hop web_name co the co chua '/' nen phai remove di tranh truong hop api hieu nham url
        web_name_new = convert_web_name(web_name)

        _tbl_obj_lst = _api_deny_web_impl.get(dict(web=web_name_new))
        if _tbl_obj_lst:

            return _tbl_obj_lst
        else:
            # post:
            return []

    def save(self):
        # DO API Holon tu dong cong time len 7h nen ta se tru date di 7h de insert cho chuan
        time_stam_date = convert_date_str_to_date_obj_spring(self.timestamp)
        time_stam_minus_gmt = get_date_minus(time_stam_date, 60*7)
        self.timestamp = convert_date_obj_to_date_str_spring(time_stam_minus_gmt)
        web_name_new = convert_web_name(self.web)
        if self.timestamp:
            _api_deny_web_impl = ApiSpringIpmsImpl('deny_web_lookup', API_SPRING_IPMS_SERVER_TENANT)
            chk_exst = self.check_exist()
            if chk_exst:
                try:
                    _tbl_obj_lst = _api_deny_web_impl.get(dict(searchWeb=web_name_new,
                                                               ip=self.ip_addr))
                    for x in _tbl_obj_lst:
                        if 'document' in x:
                            docm = x['document']
                            if str(docm).upper() == str(self.document).upper():
                                _tbl_obj = x
                                _tbl_id = _tbl_obj['Id']

                                if _tbl_id:
                                    self.Id = _tbl_id
                                    _tbl_json = json.dumps(self, default=lambda o: o.__dict__)
                                    res_put = _api_deny_web_impl.put(_tbl_id, _tbl_json)

                                    if res_put:
                                        return True

                except Exception as err:
                    print('Error %s when save user ID %s' % (str(err), str(self.Id)))
                    return False
            else:
                # post:
                try:
                    _tbl_json = json.dumps(self, default=lambda o: o.__dict__)
                    res_put = _api_deny_web_impl.post(_tbl_json)
                    if res_put:
                        return True

                except Exception as err:
                    print(err)
                    return False

        return False

    def delete(self):
        _api_deny_web_impl = ApiSpringIpmsImpl('deny_web_lookup', API_SPRING_IPMS_SERVER_TENANT)
        web_name_new = convert_web_name(self.web)
        _tbl_obj_lst = _api_deny_web_impl.get(dict(searchWeb=web_name_new,
                                                   ip=self.ip_addr))
        if _tbl_obj_lst:
            try:
                _tbl_obj = _tbl_obj_lst[0]
                _tbl_id = _tbl_obj['Id']
                if _tbl_id:
                    res_del = _api_deny_web_impl.delete(_tbl_id)

                    if res_del:
                        return True

            except Exception as err:
                print(err)
                return False
        else:
            # post:

            return False
        return False

    def find_as(self, ip_addr):
        tbl_as = TblIpServerImpl()
        as_lst = tbl_as.get_lst()
        # kiem tra ip co thuoc dai AS nao khong
        for x in as_lst:
            ip_as = x['ip_addr']
            if ip_as:
                res_chck_rnge = tbl_as.check_ip_in_range(ip_addr, ip_as)
                if res_chck_rnge:
                    return x

        return dict(Id=0,
                    ip_addr=ip_addr,
                    as_num='',
                    area='International',
                    ip_addr_type='',
                    as_name='')

    def nsl_web(self, web_name):

        res = resolver.Resolver()
        web_name = str(web_name).lower()
        # theo yeu cau moi chi nslookup nhung thang nao khong co /
        if web_name.find("/") >= 0:
            pos = web_name.find("/")
            web_name_new = web_name[:pos]
        else:
            web_name_new = web_name
        res.nameservers = ['8.8.8.8', '8.8.4.4', '203.113.131.1']
        res_as_lst = []
        # res.nameservers = ['203.113.131.1']
        time_chck = 0
        res_lst = []
        chck_ipv4 = True
        chck_ipv6 = True
        while time_chck < 20:
            if chck_ipv6:
                try:
                    answers_ipv6 = res.query(web_name_new, 'AAAA')
                    for rdata in answers_ipv6:
                        # them phan tim ra AS number va AS name
                        if rdata.address not in res_lst:
                            res_as = self.find_as(rdata.address)
                            res_as_lst.append(dict(ip_addr_range=res_as['ip_addr'],
                                                   ip_addr=rdata.address,
                                                   as_num=res_as['as_num'],
                                                   as_name=res_as['as_name'],
                                                   area=res_as['area']))
                            res_lst.append(rdata.address)

                except Exception as err:
                    print("Error %s when nslookup web %s" % (str(err), web_name))
                    chck_ipv6 = False
            if chck_ipv4:
                try:
                    answers_ipv4 = res.query(web_name_new, 'A')
                    for rdata in answers_ipv4:

                        if rdata.address not in res_lst:
                            res_as = self.find_as(rdata.address)
                            res_as_lst.append(dict(ip_addr_range=res_as['ip_addr'],
                                                   ip_addr=rdata.address,
                                                   as_num=res_as['as_num'],
                                                   as_name=res_as['as_name'],
                                                   area=res_as['area']))
                            res_lst.append(rdata.address)
                except Exception as err:
                    print("Error %s when nslookup web %s" % (str(err), web_name))
                    chck_ipv4 = False
            if chck_ipv4 or chck_ipv6:
                time.sleep(2)
            if not chck_ipv6 and not chck_ipv4:
                res_as_lst.append(dict(ip_addr_range='IP not found',
                                       ip_addr='IP not found',
                                       as_num='Not found',
                                       as_name='Not found',
                                       area='Not found'))
                return res_lst, res_as_lst
            time_chck += 1
        print("Result when nslookup %s is %s" % (str(web_name), str(res_lst)))
        return res_lst, res_as_lst


if __name__ == '__main__':
    time_ins = get_date_now_format_elastic()
    _tbl_kick = TblDenyWebLookUp(web="tester",
                                 timestamp=time_ins,
                                ip_addr="1.2.1.2",
                                 document='',
                                 as_name='',
                                 as_num='',
                                 area='',
                                 status='')
    _tbl_get_lst = _tbl_kick.nsl_web("sangtao.org")
    print(_tbl_get_lst)

