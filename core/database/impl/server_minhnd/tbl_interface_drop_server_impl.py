__author__ = 'VTN-DHML-DHIP10'
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.helpers.int_helpers import convert_string_to_float
import json
import pymysql
from config import Development
config = Development()
API_SPRING_IPMS_SERVER_TENANT = config.__getattribute__('API_SPRING_IPMS_SERVER_TENANT')
SERVER_HOST = config.__getattribute__('SERVER_HOST')
SERVER_HOST_USER = config.__getattribute__('SERVER_HOST_USER')
SERVER_HOST_PASS = config.__getattribute__('SERVER_HOST_PASS')
AREA_IP_MAIN_LST = config.__getattribute__('AREA_IP_MAIN_LST')


class TblInterfaceDrpoServerImpl:
    def __init__(self):
        pass

    def get_conn(self):
        try:
            conn = pymysql.connect(host=SERVER_HOST, user=SERVER_HOST_USER, password=SERVER_HOST_PASS, db='luuluong')

        except Exception as err:
            print(err)
            return None
        return conn

    def delete_all(self, timestamp):

        conn = self.get_conn()
        cbn_bkk_obj = None
        sql_qry = "DELETE FROM tbl_interface_drop WHERE timestamp < %s"
        if conn:
            cursor = conn.cursor()
            try:
                # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                res = cursor.execute(sql_qry, (timestamp))
                conn.commit()
                print('DELETE into tbl_rnc_cbs OK')

            except Exception as err:
                print(err)
                return False
            finally:
                cursor.close()
                conn.close()
        return True

    def count_all_greater(self, timestamp):

        conn = self.get_conn()
        cbn_bkk_obj = None
        sql_qry = "select count(*) as count from tbl_interface_drop where timestamp > %s"
        if conn:
            cursor = conn.cursor()
            try:
                # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                res = cursor.execute(sql_qry, (timestamp))
                conn.commit()
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                if res_lst:
                    if 'count' in res_lst[0]:
                        return res_lst[0]['count']

                return 0
            except Exception as err:
                print(err)
                return 0
            finally:
                cursor.close()
                conn.close()
        return 0


if __name__ == '__main__':
    tst = TblKpiServerImpl(0, '', 0, '', '', '', None, 0, 0, 0, 0,'')
    res = tst.count_all_greater('2020-08-17 05:00:00')
    print(res)

