__author__ = 'VTN-DHML-DHIP10'
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
import json
from config import Development
config = Development()
API_SPRING_IPMS_SERVER_TENANT = config.__getattribute__('API_SPRING_IPMS_SERVER_TENANT')


class TblKpiWaitServerImpl:
    def __init__(self, time_begin, area, poller, idInterface, status, speed, tenNode, tenInterface, description,
                 poller_ip, group_name, rid, status_interface, area_id_interface):
        self.time_begin = time_begin
        self.area = area
        self.poller = poller
        self.idInterface = idInterface
        self.status = status
        self.speed = speed
        self.tenNode = tenNode
        self.tenInterface = tenInterface
        self.description = description
        self.poller_ip = poller_ip
        self.group_name = group_name
        self.rid = rid
        self.status_interface = status_interface
        self.area_id_interface = area_id_interface
        self.Id = None

    def get(self, tme):
        _api_tbl_kpi_wait = ApiSpringIpmsImpl('tbl_kpi_wait', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_kpi_wait.get(dict(idInterface=self.idInterface, area=self.area))
        if _tbl_obj_lst:
            try:
                for _tbl_obj in _tbl_obj_lst:
                    _tbl_fnd_obj = TblKpiWaitServerImpl(time_begin=_tbl_obj['time_begin'], area=_tbl_obj['area'],
                                                        poller=_tbl_obj['poller'], idInterface=_tbl_obj['idInterface'],
                                                        status=_tbl_obj['status'], speed=_tbl_obj['speed'],
                                                        tenNode=_tbl_obj['tenNode'],
                                                        tenInterface=_tbl_obj['tenInterface'],
                                                        description=_tbl_obj['description'],
                                                        poller_ip=_tbl_obj['poller_ip'],
                                                        group_name=_tbl_obj['group_name'], rid=_tbl_obj['rid'],
                                                        status_interface=_tbl_obj['status_interface'],
                                                        area_id_interface=_tbl_obj['area_id_interface'])
                    if tme == _tbl_obj['time_begin']:
                        return _tbl_fnd_obj

            except Exception as err:
                print(err)
                return None

    @staticmethod
    def lst_wait():
        _api_tbl_kpi_wait = ApiSpringIpmsImpl('tbl_kpi_wait', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_kpi_wait.get(dict(searchStatus='WAIT'))
        res_lst = list()
        if _tbl_obj_lst:
            try:
                for _tbl_obj in _tbl_obj_lst:
                    _tbl_fnd_obj = TblKpiWaitServerImpl(time_begin=_tbl_obj['time_begin'], area=_tbl_obj['area'],
                                                        poller=_tbl_obj['poller'], idInterface=_tbl_obj['idInterface'],
                                                        status=_tbl_obj['status'], speed=_tbl_obj['speed'],
                                                        tenNode=_tbl_obj['tenNode'],
                                                        tenInterface=_tbl_obj['tenInterface'],
                                                        description=_tbl_obj['description'],
                                                        poller_ip=_tbl_obj['poller_ip'],
                                                        group_name=_tbl_obj['group_name'], rid=_tbl_obj['rid'],
                                                        status_interface=_tbl_obj['status_interface'],
                                                        area_id_interface=_tbl_obj['area_id_interface'])

                    res_lst.append(_tbl_fnd_obj)

            except Exception as err:
                print(err)
                return res_lst
        return res_lst

    @staticmethod
    def lst_area_wait(area):
        _api_tbl_kpi_wait = ApiSpringIpmsImpl('tbl_kpi_wait', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_kpi_wait.get(dict(searchStatusArea='WAIT', area=area))
        res_lst = list()
        if _tbl_obj_lst:
            try:
                for _tbl_obj in _tbl_obj_lst:
                    _tbl_fnd_obj = TblKpiWaitServerImpl(time_begin=_tbl_obj['time_begin'], area=_tbl_obj['area'],
                                                        poller=_tbl_obj['poller'], idInterface=_tbl_obj['idInterface'],
                                                        status=_tbl_obj['status'], speed=_tbl_obj['speed'],
                                                        tenNode=_tbl_obj['tenNode'],
                                                        tenInterface=_tbl_obj['tenInterface'],
                                                        description=_tbl_obj['description'],
                                                        poller_ip=_tbl_obj['poller_ip'],
                                                        group_name=_tbl_obj['group_name'], rid=_tbl_obj['rid'],
                                                        status_interface=_tbl_obj['status_interface'],
                                                        area_id_interface=_tbl_obj['area_id_interface'])

                    res_lst.append(_tbl_fnd_obj)

            except Exception as err:
                print(err)
                return res_lst
        return res_lst

    def save(self):
        _api_tbl_kpi_wait = ApiSpringIpmsImpl('tbl_kpi_wait', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_kpi_wait.get(dict(searchIdTimeArea=self.idInterface, area=self.area,
                                                  timeBegin=self.time_begin))
        if _tbl_obj_lst:
            try:
                _tbl_obj = _tbl_obj_lst[0]
                _tbl_id = _tbl_obj['Id']
                if _tbl_id:
                    self.Id = _tbl_id
                    _tbl_json = json.dumps(self, default=lambda o: o.__dict__)
                    res_put = _api_tbl_kpi_wait.put(_tbl_id, _tbl_json)

                    if res_put:
                        return True

            except Exception as err:
                print(err)
                return False
        else:
            # post:
            try:
                _tbl_json = json.dumps(self, default=lambda o: o.__dict__)

                res_put = _api_tbl_kpi_wait.post(_tbl_json)
                if res_put:
                        return True

            except Exception as err:
                print(err)
                return False
        return False

    def delete(self):
        _api_tbl_kpi_wait = ApiSpringIpmsImpl('tbl_kpi_wait', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_kpi_wait.get(dict(searchIdTimeArea=self.idInterface, area=self.area,
                                                  timeBegin=self.time_begin))
        if _tbl_obj_lst:
            try:
                _tbl_obj = _tbl_obj_lst[0]
                _tbl_id = _tbl_obj['Id']
                if _tbl_id:
                    self.Id = _tbl_id
                    res_del = _api_tbl_kpi_wait.delete(_tbl_id)

                    if res_del:
                        return True

            except Exception as err:
                print(err)
                return False
        else:
            # post:

            return False
        return False