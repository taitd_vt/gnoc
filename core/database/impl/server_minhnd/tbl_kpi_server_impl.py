__author__ = 'VTN-DHML-DHIP10'
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.helpers.int_helpers import convert_string_to_float
import json
import pymysql
from config import Development
config = Development()
API_SPRING_IPMS_SERVER_TENANT = config.__getattribute__('API_SPRING_IPMS_SERVER_TENANT')
SERVER_HOST = config.__getattribute__('SERVER_HOST')
SERVER_HOST_USER = config.__getattribute__('SERVER_HOST_USER')
SERVER_HOST_PASS = config.__getattribute__('SERVER_HOST_PASS')
AREA_IP_MAIN_LST = config.__getattribute__('AREA_IP_MAIN_LST')


class TblKpiServerImpl:
    def __init__(self, idInterface, group_name, speed, tenNode, tenInterface, description,
                 date, maxPeak, hieuSuat, maxIn, maxOut, area):
        self.date = date
        self.area = area
        self.idInterface = idInterface
        self.group_name = group_name
        self.speed = speed
        self.tenNode = tenNode
        self.tenInterface = tenInterface
        self.description = description
        self.maxPeak = maxPeak
        self.hieuSuat = hieuSuat
        self.maxIn = maxIn
        self.maxOut = maxOut
        self.idKPI = None

    def get(self):
        _api_tbl_kpi_impl = ApiSpringIpmsImpl('tbl_kpi', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_kpi_impl.get(dict(searchIdAreaTimeBegin=self.idInterface,
                                                  area=self.area, timeBegin=self.date))
        if _tbl_obj_lst:
            try:
                for _tbl_obj in _tbl_obj_lst:
                    _tbl_fnd_obj = TblKpiServerImpl(date=_tbl_obj['date'],
                                                    area=_tbl_obj['area'],
                                                    idInterface=_tbl_obj['idInterface'],
                                                    speed=_tbl_obj['speed'],
                                                    maxPeak=_tbl_obj['maxPeak'],
                                                    group_name=_tbl_obj['group_name'],
                                                    tenNode=_tbl_obj['tenNode'],
                                                    tenInterface=_tbl_obj['tenInterface'],
                                                    description=_tbl_obj['description'],
                                                    hieuSuat=_tbl_obj['hieuSuat'],
                                                    maxIn=_tbl_obj['maxIn'],
                                                    maxOut=_tbl_obj['maxOut'])

                    return _tbl_fnd_obj

            except Exception as err:
                print(err)
                return None

    def save(self):
        _api_tbl_kpi_impl = ApiSpringIpmsImpl('tbl_kpi', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_kpi_impl.get(dict(searchIdAreaTimeBegin=self.idInterface, area=self.area,
                                                  timeBegin=self.date))
        if _tbl_obj_lst:
            try:
                _tbl_obj = _tbl_obj_lst[0]
                _tbl_id = _tbl_obj['idKPI']
                if _tbl_id:
                    self.idKPI = _tbl_id
                    _tbl_json = json.dumps(self, default=lambda o: o.__dict__)
                    res_put = _api_tbl_kpi_impl.put(_tbl_id, _tbl_json)

                    if res_put:
                        return True

            except Exception as err:
                print(err)
                return False
        else:
            # post:
            try:
                _tbl_json = json.dumps(self, default=lambda o: o.__dict__)
                res_put = _api_tbl_kpi_impl.post(_tbl_json)
                if res_put:
                        return True

            except Exception as err:
                print(err)
                return False
        return False

    def delete(self):
        _api_tbl_kpi_impl = ApiSpringIpmsImpl('tbl_kpi', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_kpi_impl.get(dict(searchIdAreaTimeBegin=self.idInterface, area=self.area,
                                                  timeBegin=self.date))
        if _tbl_obj_lst:
            try:
                _tbl_obj = _tbl_obj_lst[0]
                _tbl_id = _tbl_obj['Id']
                if _tbl_id:
                    res_del = _api_tbl_kpi_impl.delete(_tbl_id)

                    if res_del:
                        return True

            except Exception as err:
                print(err)
                return False
        else:
            # post:

            return False
        return False

    def check_over_kpi(self):
        check = False
        tenIntf = str(self.tenInterface)
        tenIntf = tenIntf.upper()
        grpName = str(self.group_name)
        grpName = grpName.upper()

        try:
            if tenIntf.find('CPU') >= 0:
                if 40 < self.hieuSuat < 110:
                    return True
            elif tenIntf.find('MEM') >= 0:
                if 70 < self.hieuSuat < 110:
                    return True
            elif tenIntf.find('ONLINE') >= 0:
                if 85 < self.hieuSuat < 110:
                    return True
            elif tenIntf.find('POOL') >= 0:
                if 85 < self.hieuSuat < 110:
                    return True
            elif tenIntf.find('CEM') >= 0: # cem port pe2g co dinh
                    return False
            elif tenIntf.find('FAN') >= 0: # fan
                    return False
            elif tenIntf.find('POW') >= 0: # power
                    return False
            elif tenIntf.find('STR') >= 0: # hdd dns
                    return False

            if grpName.find('MPBN') >= 0:
                if 40 < self.hieuSuat < 110:
                    return True
            if isinstance(self.speed, float) or isinstance(self.speed, int):
                if self.speed > 5000000000:
                    if self.hieuSuat > 85:
                        return True
                elif self.speed > 0:
                    if self.hieuSuat > 70:
                        return True
        except Exception as err:
            print('Error %s when check KPI of %s %s' % (err, self.tenNode, self.tenInterface))
            return False
        return check

    def get_conn(self):
        try:
            conn = pymysql.connect(host=SERVER_HOST, user=SERVER_HOST_USER, password=SERVER_HOST_PASS, db='luuluong')

        except Exception as err:
            print(err)
            return None
        return conn

    def delete_all(self, timestamp):

        conn = self.get_conn()
        cbn_bkk_obj = None
        sql_qry = "DELETE FROM tbl_kpi WHERE date < %s"
        if conn:
            cursor = conn.cursor()
            try:
                # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                res = cursor.execute(sql_qry, (timestamp))
                conn.commit()
                print('DELETE into tbl_rnc_cbs OK')

            except Exception as err:
                print(err)
                return False
            finally:
                cursor.close()
                conn.close()
        return True

    def count_all_greater(self, timestamp):

        conn = self.get_conn()
        cbn_bkk_obj = None
        sql_qry = "select count(*) as count from tbl_kpi where date > %s"
        if conn:
            cursor = conn.cursor()
            try:
                # cursor.prepare("update CELLBKK set CELL_NAME = :cell_name where CELL_NAME = 'eTB006101zxc'")
                res = cursor.execute(sql_qry, (timestamp))
                conn.commit()
                data = cursor.fetchall()
                col_name_lst = [x[0] for x in cursor.description]
                res_lst = [dict(zip(col_name_lst, row)) for row in data]
                if res_lst:
                    if 'count' in res_lst[0]:
                        return res_lst[0]['count']

                return 0
            except Exception as err:
                print(err)
                return 0
            finally:
                cursor.close()
                conn.close()
        return 0


if __name__ == '__main__':
    tst = TblKpiServerImpl(0, '', 0, '', '', '', None, 0, 0, 0, 0,'')
    res = tst.count_all_greater('2020-08-17 05:00:00')
    print(res)

