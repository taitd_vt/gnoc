__author__ = 'VTN-DHML-DHIP10'
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.database.impl.server_minhnd.tbl_interface_not_add_group_impl import TblInterfaceNotAddGroupImpl
from core.helpers.date_helpers import get_date_now, convert_date_str_to_date_obj_spring, get_date_minus, \
    date_diff_in_seconds, \
    convert_date_obj_to_date_str_spring, get_date_now_format_elastic
from core.helpers.stringhelpers import check_contain_and_or_str
from core.helpers.int_helpers import convert_string_to_float
from core.database.impl.ipms.intf_grp_name_imps_impl import InterfaceGroupNameIpmsImpl
from core.database.impl.ipms.intf_grp_ipms_impl import InterfaceGroupIpmsImpl
from core.helpers.tenant_helpers import get_tenant_id, get_main_ipms, get_tenant_id_from_area
from core.database.impl.server_minhnd.tbl_log_interface_add_group_impl import TblLogInterfaceAddGroupImpl
import json
from config import Development

config = Development()
API_SPRING_IPMS_SERVER_TENANT = config.__getattribute__('API_SPRING_IPMS_SERVER_TENANT')


class TblInsertInterfaceGroupImpl:
    def __init__(self, area, interface_not_contain, host_contain, rule_description, interface_contain,
                 group_host_name, interface_description, interface_des_not_contain, group_name,
                 speed_greater_mbps, speed_less_mbps):

        self.group_name = group_name
        self.rule_description = rule_description
        self.area = area
        self.interface_not_contain = interface_not_contain
        self.host_contain = host_contain
        self.interface_contain = interface_contain
        self.group_host_name = group_host_name
        self.interface_description = interface_description
        self.interface_des_not_contain = interface_des_not_contain
        self.speed_greater_mbps = speed_greater_mbps
        self.speed_less_mbps = speed_less_mbps
        self.id_insert = None

    def list(self):
        _api_impl = ApiSpringIpmsImpl('insert_intf_group', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_impl.lst()
        res_lst = list()

        if _tbl_obj_lst:

            for _tbl_obj in _tbl_obj_lst:
                try:
                    _tbl_fnd_obj = TblInsertInterfaceGroupImpl(group_name=_tbl_obj['group_name'],
                                                               rule_description=_tbl_obj['rule_description'],
                                                               area=_tbl_obj['area'],
                                                               interface_not_contain=_tbl_obj['interface_not_contain'],
                                                               host_contain=_tbl_obj['host_contain'],
                                                               interface_contain=_tbl_obj['interface_contain'],
                                                               group_host_name=_tbl_obj['group_host_name'],
                                                               interface_description=_tbl_obj['interface_description'],
                                                               interface_des_not_contain=_tbl_obj[
                                                                   'interface_des_not_contain'],
                                                               speed_greater_mbps=_tbl_obj['speed_greater_mbps'],
                                                               speed_less_mbps=_tbl_obj['speed_less_mbps']
                                                               )
                    _tbl_fnd_obj.id_insert = _tbl_obj['id_insert']
                    res_lst.append(_tbl_fnd_obj)

                except Exception as err:
                    print("Error %s when list table Interface add group " % str(err))
                    return res_lst
        return res_lst

    @staticmethod
    def total():
        _api_tbl_intf_not_impl = ApiSpringIpmsImpl('insert_intf_group', API_SPRING_IPMS_SERVER_TENANT)
        _tot_str = _api_tbl_intf_not_impl.total()
        _tot_num = convert_string_to_float(_tot_str)
        return _tot_num

    @staticmethod
    def total_key_name(key_name):
        _api_impl = ApiSpringIpmsImpl('insert_intf_group', API_SPRING_IPMS_SERVER_TENANT)

        try:
            _total_str = _api_impl.get(dict(searchTotal=key_name))
            _total_num = convert_string_to_float(_total_str)
            return _total_num

        except Exception as err:
                print("Error %s when get total of key name %s " % str(err), key_name)
                return 0

    def list_page(self, page, page_size):
        _api_impl = ApiSpringIpmsImpl('insert_intf_group', API_SPRING_IPMS_SERVER_TENANT)
        page_dct = dict(page=page, pageSize=page_size)
        _tbl_obj_lst = _api_impl.get(page_dct)

        res_lst = list()

        if _tbl_obj_lst:

            for _tbl_obj in _tbl_obj_lst:
                try:
                    _tbl_fnd_obj = TblInsertInterfaceGroupImpl(group_name=_tbl_obj['group_name'],
                                                               rule_description=_tbl_obj['rule_description'],
                                                               area=_tbl_obj['area'],
                                                               interface_not_contain=_tbl_obj['interface_not_contain'],
                                                               host_contain=_tbl_obj['host_contain'],
                                                               interface_contain=_tbl_obj['interface_contain'],
                                                               group_host_name=_tbl_obj['group_host_name'],
                                                               interface_description=_tbl_obj['interface_description'],
                                                               interface_des_not_contain=_tbl_obj[
                                                                   'interface_des_not_contain'],
                                                               speed_greater_mbps=_tbl_obj['speed_greater_mbps'],
                                                               speed_less_mbps=_tbl_obj['speed_less_mbps']
                                                               )
                    _tbl_fnd_obj.id_insert = _tbl_obj['id_insert']
                    res_lst.append(_tbl_fnd_obj)

                except Exception as err:
                    print("Error %s when list table Interface add group " % str(err))
                    return res_lst
        return res_lst

    @staticmethod
    def find_like(**kwargs):
        _api_impl = ApiSpringIpmsImpl('insert_intf_group', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_impl.get(kwargs)

        return _tbl_obj_lst

    @staticmethod
    def get(id):
        _api_impl = ApiSpringIpmsImpl('insert_intf_group', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_impl.get(dict(id=id))
        if _tbl_obj_lst:
            try:
                _tbl_fnd_obj = TblInsertInterfaceGroupImpl(group_name=_tbl_obj_lst[0]['group_name'],
                                                           rule_description=_tbl_obj_lst[0]['rule_description'],
                                                           area=_tbl_obj_lst[0]['area'],
                                                           interface_not_contain=_tbl_obj_lst[0][
                                                               'interface_not_contain'],
                                                           host_contain=_tbl_obj_lst[0]['host_contain'],
                                                           interface_contain=_tbl_obj_lst[0]['interface_contain'],
                                                           group_host_name=_tbl_obj_lst[0]['group_host_name'],
                                                           interface_description=_tbl_obj_lst[0][
                                                               'interface_description'],
                                                           interface_des_not_contain=_tbl_obj_lst[0][
                                                               'interface_des_not_contain'],
                                                           speed_greater_mbps=_tbl_obj_lst[0]['speed_greater_mbps'],
                                                           speed_less_mbps=_tbl_obj_lst[0]['speed_less_mbps']
                                                           )
                _tbl_fnd_obj.id_insert = _tbl_obj_lst[0]['id_insert']

                return _tbl_fnd_obj
            except Exception as err:
                print(err)
                return None
        return None

    def check_exist(self):
        _api_impl = ApiSpringIpmsImpl('insert_intf_group', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_impl.get(dict(group_name=self.group_name,
                                          area=self.area,
                                          host_contain=self.host_contain,
                                          interface_contain=self.interface_contain,
                                          interface_description=self.interface_description,
                                          speed_greater_mbps=self.speed_greater_mbps,
                                          speed_less_mbps=self.speed_less_mbps
                                          ))
        check_exst = False
        if _tbl_obj_lst:

            for _tbl_obj in _tbl_obj_lst:
                try:
                    _tbl_fnd_obj = TblInsertInterfaceGroupImpl(group_name=_tbl_obj['group_name'],
                                                               rule_description=_tbl_obj['rule_description'],
                                                               area=_tbl_obj['area'],
                                                               interface_not_contain=_tbl_obj['interface_not_contain'],
                                                               host_contain=_tbl_obj['host_contain'],
                                                               interface_contain=_tbl_obj['interface_contain'],
                                                               group_host_name=_tbl_obj['group_host_name'],
                                                               interface_description=_tbl_obj['interface_description'],
                                                               interface_des_not_contain=_tbl_obj[
                                                                   'interface_des_not_contain'],
                                                               speed_greater_mbps=_tbl_obj['speed_greater_mbps'],
                                                               speed_less_mbps=_tbl_obj['speed_less_mbps']
                                                               )

                    return True

                except Exception as err:
                    print(err)
                    return True
        return check_exst

    def add(self):
        # DO API Holon tu dong cong time len 7h nen ta se tru date di 7h de insert cho chuan
        # time_stam_date = convert_date_str_to_date_obj_spring(self.timestamp)
        # time_stam_minus_gmt = get_date_minus(time_stam_date, minute_mins=60 * 7)
        # self.timestamp = convert_date_obj_to_date_str_spring(time_stam_minus_gmt)

        # if self.timestamp:
        _api_impl = ApiSpringIpmsImpl('insert_intf_group', API_SPRING_IPMS_SERVER_TENANT)
        chk_exst = self.check_exist()

        if chk_exst:
            return False
        else:
            # post:
            try:
                _tbl_json = json.dumps(self, default=lambda o: o.__dict__)
                res_put = _api_impl.post(_tbl_json)
                if res_put:
                    return True
            except Exception as err:
                print(err)
                return False

    def edit(self):
        # DO API Holon tu dong cong time len 7h nen ta se tru date di 7h de insert cho chuan
        # time_stam_date = convert_date_str_to_date_obj_spring(self.timestamp)
        # time_stam_minus_gmt = get_date_minus(time_stam_date, minute_mins=60 * 7)
        # self.timestamp = convert_date_obj_to_date_str_spring(time_stam_minus_gmt)

        # if self.timestamp:
        _api_impl = ApiSpringIpmsImpl('insert_intf_group', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_id = self.id_insert
        if _tbl_id:
            try:
                _tbl_json = json.dumps(self, default=lambda o: o.__dict__)
                res_put = _api_impl.put(_tbl_id, _tbl_json)
                if res_put:
                    return True
            except Exception as err:
                print('Error %s when save tbl_insert_interface_group ID %s' % str(err), str(self.id_insert))
                return False
        else:
            return False

    def delete(self):
        _api_impl = ApiSpringIpmsImpl('insert_intf_group', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_id = self.id_insert
        if _tbl_id:
            try:
                res_del = _api_impl.delete(_tbl_id)

                if res_del:
                    return True

            except Exception as err:
                print(err)
                return False
        else:
            # post:

            return False
        return False

    def get_grp_id(self, grp_name):
        intf_impl = InterfaceGroupNameIpmsImpl(0, grp_name)
        tent_id = get_tenant_id_from_area(self.area)

        if tent_id:
            grp_id_lst = intf_impl.get_grp_lst(tent_id, grp_name)
            if grp_id_lst:
                return grp_id_lst
        return []

    def check_and_insert(self):
        # list interface not add of this area
        time_now = get_date_now()
        _tbl_impl = TblInterfaceNotAddGroupImpl(area=self.area,
                                                host='',
                                                intf='',
                                                description='',
                                                interface_id=0,
                                                host_group_name='',
                                                timestamp=time_now,
                                                speed=0.0,
                                                status='active'
                                                )
        src_area_dct = dict(area=self.area)
        res_not_add = _tbl_impl.find_like(**src_area_dct)
        if res_not_add:
            # lay ra danh sach rule
            rule_lst = self.find_like(**dict(search=self.area))
            if rule_lst:
                # bat dau insert
                for x in res_not_add:
                    try:
                        if 'host' in x:
                            host_name = x['host']
                            if 'intf' in x:
                                intf = x['intf']
                                if 'interface_id' in x:
                                    intf_id = x['interface_id']
                                    if 'description' in x:
                                        intf_des = x['description']
                                    else:
                                        intf_des = ''

                                    if 'host_group_name' in x:
                                        host_group_name = x['host_group_name']
                                    else:
                                        host_group_name = ''
                                    if 'speed' in x:
                                        intf_speed = x['speed']
                                    else:
                                        intf_speed = 0

                                    for y in rule_lst:
                                        rule_grp_name = y['group_name']
                                        rule_host_ctn = y['host_contain']
                                        rule_intf_ctn = y['interface_contain']
                                        rule_intf_not_ctn = y['interface_not_contain']
                                        rule_grp_host_name = y['group_host_name']
                                        rule_intf_des = y['interface_description']
                                        rule_intf_des_not_ctn = y['interface_des_not_contain']
                                        rule_spd_grt = y['speed_greater_mbps']
                                        rule_spd_les = y['speed_less_mbps']

                                        chk_speed_les = False
                                        if rule_intf_ctn:
                                            chk_intf = check_contain_and_or_str(intf, rule_intf_ctn)
                                        else:
                                            chk_intf = True

                                        if chk_intf:
                                            if rule_intf_not_ctn:
                                                chk_intf_not_ctn = not check_contain_and_or_str(intf, rule_intf_not_ctn)
                                            else:
                                                chk_intf_not_ctn = True

                                            if chk_intf_not_ctn:
                                                if rule_host_ctn:
                                                    chk_host_ctn = check_contain_and_or_str(host_name, rule_host_ctn)
                                                else:
                                                    chk_host_ctn = True

                                                if chk_host_ctn:
                                                    if rule_grp_host_name:
                                                        chk_host_group_name = check_contain_and_or_str(host_group_name,
                                                                                                       rule_grp_host_name)
                                                    else:
                                                        chk_host_group_name = True
                                                    if chk_host_group_name:
                                                        if rule_intf_des:
                                                            chk_intf_des = check_contain_and_or_str(intf_des,
                                                                                                    rule_intf_des)
                                                        else:
                                                            chk_intf_des = True
                                                        if chk_intf_des:
                                                            if rule_intf_des_not_ctn:
                                                                chk_intf_des_not_ctn = not check_contain_and_or_str(intf_des,
                                                                                                            rule_intf_des_not_ctn)
                                                            else:
                                                                chk_intf_des_not_ctn = True
                                                            if chk_intf_des_not_ctn:
                                                                if rule_spd_grt >= 0:
                                                                    if intf_speed > rule_spd_grt:
                                                                        chk_speed_grt = True
                                                                    else:
                                                                        chk_speed_grt = False
                                                                    if chk_speed_grt:
                                                                        if rule_spd_les >= 0:
                                                                            if intf_speed < rule_spd_les:
                                                                                chk_speed_les = True
                                                                            else:
                                                                                chk_speed_les = False

                                                                        # match het cac rule thi insert
                                                                        if chk_speed_les:
                                                                            if rule_grp_name:
                                                                                # tim group id
                                                                                grp_id_lst = self.get_grp_id(
                                                                                    rule_grp_name)
                                                                                if grp_id_lst:
                                                                                    if len(grp_id_lst) == 1:
                                                                                        # insert tranh duplicate
                                                                                        try:
                                                                                            intf_grp_impl = InterfaceGroupIpmsImpl(
                                                                                                grp_id_lst[0].group_id,
                                                                                                int(intf_id))
                                                                                            tent_id = get_tenant_id_from_area(
                                                                                                self.area)
                                                                                            if tent_id:
                                                                                                res_ins = intf_grp_impl.insert(
                                                                                                    tent_id)
                                                                                                if res_ins:
                                                                                                    # Insert Ok roi thi xoa khoi interface not add
                                                                                                    _tbl_obj = TblInterfaceNotAddGroupImpl(timestamp=x['timestamp'],
                                                                                                                                           area=x['area'],
                                                                                                                                           host=x['host'],
                                                                                                                                           intf=x['intf'],
                                                                                                                                           description=x['description'],
                                                                                                                                           interface_id=x['interface_id'],
                                                                                                                                           host_group_name=x['host_group_name'],
                                                                                                                                           speed=x['speed'],
                                                                                                                                           status=x['status'])
                                                                                                    _tbl_obj.delete()
                                                                                                    # insert vao log:
                                                                                                    tbl_log_impl = TblLogInterfaceAddGroupImpl(
                                                                                                        area=self.area,
                                                                                                        host_name=host_name,
                                                                                                        intf=intf,
                                                                                                        description=intf_des,
                                                                                                        interface_id=intf_id,
                                                                                                        host_group_name=host_group_name,
                                                                                                        timestamp=get_date_now(),
                                                                                                        speed=intf_speed,
                                                                                                        group_name=rule_grp_name,
                                                                                                        user=''
                                                                                                        )
                                                                                                    tbl_log_impl.save()

                                                                                        except Exception as err:
                                                                                            print(
                                                                                                "Error %s when insert interface %s of rule %s" % (
                                                                                                str(err), str(intf_id),
                                                                                                rule_grp_name))
                    except Exception as err:
                        print("Error %s when insert interface %s of rule %s" % (str(err)))


if __name__ == '__main__':
    time_ins = get_date_now_format_elastic()
    _tbl_kick = _tbl_fnd_obj = TblInsertInterfaceGroupImpl(group_name='',
                                                           rule_description='',
                                                           area='KV1',
                                                           interface_not_contain='',
                                                           host_contain='',
                                                           interface_contain='',
                                                           group_host_name='',
                                                           interface_description='',
                                                           interface_des_not_contain='',
                                                           speed_greater_mbps=0,
                                                           speed_less_mbps=100000
                                                           )
    test = _tbl_kick.check_and_insert()


    # for x in _tbl_lst:
    #    x.delete()
    # print(_tbl_lst)
