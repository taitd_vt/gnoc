__author__ = 'VTN-DHML-DHIP10'
import requests
import json
from config import Development
from core.helpers.api_helpers import api_delete_retry, api_post_retry, api_put_retry, api_get_retry
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.helpers.int_helpers import convert_string_to_float
config = Development()
API_SPRING_IPMS_SERVER = config.__getattribute__('API_SPRING_IPMS_SERVER')
API_SPRING_IPMS_USER = config.__getattribute__('API_SPRING_IPMS_USER')
API_SPRING_IPMS_PASS = config.__getattribute__('API_SPRING_IPMS_PASS')
API_SPRING_IPMS_PORT = config.__getattribute__('API_SPRING_IPMS_PORT')
API_SPRING_IPMS_SERVER_TENANT = config.__getattribute__('API_SPRING_IPMS_SERVER_TENANT')


class TblCustomerVipImpl:
    def __init__(self):
        pass

    def get(self, id):
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/customer_vip/id=" + str(id)
        headers_json = {"X-TENANT-ID": API_SPRING_IPMS_SERVER_TENANT, "username": API_SPRING_IPMS_USER,
                        "password": API_SPRING_IPMS_PASS,
                        "Content-Type": "application/json"}
        #r = requests.get(url_api, headers=headers_json)
        r = api_get_retry(url_api, headers_json)
        if r:
            if r.ok:
                result_json = json.loads(r.text)
                return result_json
        else:
            return dict()

    def list(self):
        '''
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/customer_vip/"
        headers_json = {"X-TENANT-ID": API_SPRING_IPMS_SERVER_TENANT, "username": API_SPRING_IPMS_USER,
                        "password": API_SPRING_IPMS_PASS,
                        "Content-Type": "application/json"}
        r = requests.get(url_api, headers=headers_json)
        r = api_get_retry(url_api, headers_json)
        if r:
            if r.ok:
                result_json = json.loads(r.text)
                return result_json
        else:
            return list()
        '''
        res_lst =[]
        totl = self.total()
        if totl:
            page_size = 1000
            page_totl = round(totl / page_size, 0) + 1
            for page in range(1, int(page_totl) + 1):
                res_lst += self.list_page(page=page,
                                          page_size=page_size)
        return res_lst

    @staticmethod
    def total():
        _tot_num = 0
        try:
            _api_tbl_intf_not_impl = ApiSpringIpmsImpl('customer_vip', API_SPRING_IPMS_SERVER_TENANT)
            _tot_str = _api_tbl_intf_not_impl.total()
            _tot_num = convert_string_to_float(_tot_str)
        except Exception as err:
            print("error when get total customer vip")
            print(err)
            return _tot_num
        return _tot_num

    def list_page(self, page, page_size):
        _api_tbl_intf_not_impl = ApiSpringIpmsImpl('customer_vip', API_SPRING_IPMS_SERVER_TENANT)
        page_dct = dict(page=page, pageSize=page_size)
        _tbl_obj_lst = _api_tbl_intf_not_impl.get(page_dct)
        return _tbl_obj_lst

    @staticmethod
    def find_like(**kwargs):
        _api_impl = ApiSpringIpmsImpl('customer_vip', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_impl.get(kwargs)

        return _tbl_obj_lst

    @staticmethod
    def total_key_name(key_name):
        _api_impl = ApiSpringIpmsImpl('customer_vip', API_SPRING_IPMS_SERVER_TENANT)

        try:
            _total_str = _api_impl.get(dict(searchTotal=key_name))
            _total_num = convert_string_to_float(_total_str)
            return _total_num

        except Exception as err:
                print("Error %s when get total of key name %s " % str(err), key_name)
                return 0

    @staticmethod
    def total_key_name_status(key_name, status):
        _api_impl = ApiSpringIpmsImpl('customer_vip', API_SPRING_IPMS_SERVER_TENANT)

        try:
            _total_str = _api_impl.get(dict(searchStatusTotal=key_name, status=status))
            _total_num = convert_string_to_float(_total_str)
            return _total_num

        except Exception as err:
                print("Error %s when get total of key name %s " % str(err), key_name)
                return 0

    def put(self, id, **kwargs):
        result = False

        data_json = json.dumps(kwargs)

        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/customer_vip/id=" + str(id)
        headers_json = {"X-TENANT-ID": API_SPRING_IPMS_SERVER_TENANT, "username": API_SPRING_IPMS_USER,
                        "password": API_SPRING_IPMS_PASS,
                        "Content-Type": "application/json"}
        # r = requests.put(url_api, headers=headers_json, data=data_json)
        r = api_put_retry(url_api, headers_json, data_json)
        if r:
            if r.ok:
                result_req = r.text
                if result_req == 'OK':
                    return True

        else:
            return result

    def search(self, **kwargs):
        result = ''
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/customer_vip/"
        headers_json = {"X-TENANT-ID": API_SPRING_IPMS_SERVER_TENANT, "username": API_SPRING_IPMS_USER,
                        "password": API_SPRING_IPMS_PASS,
                        "Content-Type": "application/json"}
        for k, v in kwargs.items():
            if isinstance(v, str):
                v_rep = v.replace("/", "")
                v_rep = v_rep.replace(";", "|")
            if k == 'web':
                # do holonplatform khong nhan duoc dau ";" nen ta replace dau ";" bang dau |
                url_api += 'web=' + v_rep
            else:
                url_api += 'search=' + v_rep
            # r = requests.get(url_api, headers=headers_json)
            r = api_get_retry(url_api, headers_json)
            if r:
                if r.ok:
                    result_req = json.loads(r.text)
                    return result_req

        return result

    def post(self, **kwargs):
        result = False
        data_json = json.dumps(kwargs)
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/customer_vip/"
        headers_json = {"X-TENANT-ID": API_SPRING_IPMS_SERVER_TENANT, "username": API_SPRING_IPMS_USER,
                        "password": API_SPRING_IPMS_PASS,
                        "Content-Type": "application/json"}
        web_lst = self.search(**dict(web=kwargs['web']))
        web_obj_old_dct = dict()
        check_exist = False
        if web_lst:
            for web_obj in web_lst:
                web_name = web_obj['web']
                if web_name == kwargs['web']:
                    check_exist = True
                    web_obj_old_dct = web_obj
                    break

        if not check_exist:

            # r = requests.post(url_api, headers=headers_json, data=data_json)
            r = api_post_retry(url_api, headers_json, data_json)
            if r:
                if r.ok:
                    result_req = r.text
                    if result_req == '':
                        #print('Successful insert Ring info ' + str(data_json))
                        return True
                    elif result_req == 'OK':
                        result = True
        else:
            # da ton tai roi update thoi

                if 'Id' in web_obj_old_dct:
                    id_ring = web_obj_old_dct['Id']
                    kwargs['Id'] = id_ring
                    res_put = self.put(id_ring, **kwargs)
                    if res_put:
                        #print('Successful update Ring info ' + str(kwargs))
                        return True

        return result

    def delete(self, id):
        result = False
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/customer_vip/id=" + str(id)
        headers_json = {"X-TENANT-ID": API_SPRING_IPMS_SERVER_TENANT, "username": API_SPRING_IPMS_USER,
                        "password": API_SPRING_IPMS_PASS,
                        "Content-Type": "application/json"}
        # r = requests.delete(url_api, headers=headers_json)
        r = api_delete_retry(url_api, headers_json)
        if r:
            if r.ok:
                result_req = r.text
                if result_req == 'OK':
                    #print('Successful delete Ring info ' + str(id))
                    return True

        else:
            #print('Failed delete Ring info ' + str(id) + r.text)
            return result


if __name__ == '__main__':
    _tbl = TblCustomerVipImpl()
    _tbl_lst = _tbl.list()
    print(_tbl_lst)