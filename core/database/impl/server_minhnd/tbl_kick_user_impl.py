__author__ = 'VTN-DHML-DHIP10'
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.helpers.date_helpers import get_date_now, convert_date_str_to_date_obj_spring, get_date_minus, date_diff_in_seconds, \
                                        convert_date_obj_to_date_str_spring, get_date_now_format_elastic
import json
from config import Development
config = Development()
API_SPRING_IPMS_SERVER_TENANT = config.__getattribute__('API_SPRING_IPMS_SERVER_TENANT')


class TblKickUserImpl:
    def __init__(self, user_is_kicked, user_kick, timestamp, sgsn, result):
        self.timestamp = timestamp
        self.user_is_kicked = user_is_kicked
        self.user_kick = user_kick
        self.sgsn = sgsn
        self.result = result
        self.Id = None

    def list(self):
        _api_tbl_kick_usr_impl = ApiSpringIpmsImpl('tbl_kick_user', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_kick_usr_impl.lst()
        res_lst = list()

        if _tbl_obj_lst:
            try:
                for _tbl_obj in _tbl_obj_lst:
                    _tbl_fnd_obj = TblKickUserImpl(timestamp=_tbl_obj['timestamp'],
                                                   user_is_kicked=_tbl_obj['user_is_kicked'],
                                                   user_kick=_tbl_obj['user_kick'],
                                                   sgsn=_tbl_obj['sgsn'],
                                                   result=_tbl_obj['result']
                                                   )
                    res_lst.append(_tbl_fnd_obj)

            except Exception as err:
                print("Error %s when list table Kick user" % str(err))
                return res_lst
        return res_lst

    def check_exist(self):
        _api_tbl_kick_usr_impl = ApiSpringIpmsImpl('tbl_kick_user', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_kick_usr_impl.get(dict(searchUserTime=self.user_is_kicked,
                                                  userKick=self.user_kick, timestamp=self.timestamp))
        check_exst = False
        if _tbl_obj_lst:
            try:
                for _tbl_obj in _tbl_obj_lst:
                    _tbl_fnd_obj = TblKickUserImpl(timestamp=_tbl_obj['timestamp'],
                                                   user_is_kicked=_tbl_obj['user_is_kicked'],
                                                   user_kick=_tbl_obj['user_kick'],
                                                   sgsn=_tbl_obj['sgsn'],
                                                   result=_tbl_obj['result']
                                                   )

                return True

            except Exception as err:
                print(err)
                return True
        return check_exst

    def check_wait_time(self):
        date_now = get_date_now()

        _api_tbl_kick_usr_impl = ApiSpringIpmsImpl('tbl_kick_user', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_kick_usr_impl.get(dict(searchUserKick=self.user_kick))
        check = True
        if _tbl_obj_lst:
            try:
                for _tbl_obj in _tbl_obj_lst:
                    _tbl_fnd_obj = TblKickUserImpl(timestamp=_tbl_obj['timestamp'],
                                                   user_is_kicked=_tbl_obj['user_is_kicked'],
                                                   user_kick=_tbl_obj['user_kick'],
                                                   sgsn=_tbl_obj['sgsn'],
                                                   result=_tbl_obj['result'])

                    date_conv = convert_date_str_to_date_obj_spring(_tbl_obj['timestamp'])
                    date_diff_sec = date_diff_in_seconds(date_now, date_conv)
                    if date_diff_sec < 60:
                        return False

                return check

            except Exception as err:
                print('Error %s when check wait time of user %s ' % str(err), self.user_kick)
                return check
        return check

    def save(self):
        # DO API Holon tu dong cong time len 7h nen ta se tru date di 7h de insert cho chuan
        time_stam_date = convert_date_str_to_date_obj_spring(self.timestamp)
        time_stam_minus_gmt = get_date_minus(time_stam_date, minute_mins=60*7)
        self.timestamp = convert_date_obj_to_date_str_spring(time_stam_minus_gmt)

        if self.timestamp:
            _api_tbl_kick_user_impl = ApiSpringIpmsImpl('tbl_kick_user', API_SPRING_IPMS_SERVER_TENANT)
            chk_exst = self.check_exist()
            chk_wait_time = self.check_wait_time()
            if chk_wait_time:
                if chk_exst:
                    try:
                        _tbl_obj_lst = _api_tbl_kick_user_impl.get(dict(searchUserTime=self.user_is_kicked,
                                                          userKick=self.user_kick, timestamp=self.timestamp))

                        _tbl_obj = _tbl_obj_lst[0]
                        _tbl_id = _tbl_obj['Id']

                        if _tbl_id:

                            self.Id = _tbl_id
                            _tbl_json = json.dumps(self, default=lambda o: o.__dict__)
                            res_put = _api_tbl_kick_user_impl.put(_tbl_id, _tbl_json)

                            if res_put:
                                return True

                    except Exception as err:
                        print('Error %s when save user ID %s' % str(err), str(self.Id))
                        return False
                else:
                    # post:
                    try:
                        _tbl_json = json.dumps(self, default=lambda o: o.__dict__)
                        res_put = _api_tbl_kick_user_impl.post(_tbl_json)
                        if res_put:
                                return True

                    except Exception as err:
                        print(err)
                        return False
            else:
                return False
        return False

    def delete(self):
        _api_tbl_kick_user_impl = ApiSpringIpmsImpl('tbl_kick_user', API_SPRING_IPMS_SERVER_TENANT)
        _tbl_obj_lst = _api_tbl_kick_user_impl.get(dict(searchUserTime=self.user_is_kicked,
                                                  userKick=self.user_kick, timestamp=self.timestamp))
        if _tbl_obj_lst:
            try:
                _tbl_obj = _tbl_obj_lst[0]
                _tbl_id = _tbl_obj['Id']
                if _tbl_id:
                    res_del = _api_tbl_kick_user_impl.delete(_tbl_id)

                    if res_del:
                        return True

            except Exception as err:
                print(err)
                return False
        else:
            # post:

            return False
        return False


if __name__ == '__main__':
    time_ins = get_date_now_format_elastic()
    _tbl_kick = TblKickUserImpl('84962381911', 'minhnd', time_ins)
    _tbl_lst = _tbl_kick.save()
    print(_tbl_lst)

