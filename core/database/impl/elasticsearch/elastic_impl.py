__author__ = 'vtn-dhml-dhip10'
from elasticsearch import Elasticsearch
from elasticsearch import helpers


class ElasticSearchHost:
    def __init__(self, host, port):
        self.host = host
        self.port = port

    def get_elastic(self):
        es_host = Elasticsearch([{"host": self.host, "port": self.port}], sniff_on_start=True,
                                sniff_on_connection_fail=True, sniffer_timeout=60)
        return es_host

    def query_search_must(self, srch_dct, index_tbl):
        q = {
            'query': {
                'bool': {
                    'must': [{'match': {k: v}} for k, v in srch_dct.items()]
                }
            }
        }
        es_temp = self.get_elastic()
        result = es_temp.search(index=index_tbl, body=q)
        return result['hits']['hits']

    def query_search_must_time_range(self, srch_dct, tme_gte, tme_lte, index_tbl):
        # chu y search query time range dang fix luon timestamp
        q = {
            'query': {
                'bool': {
                    'must': [{'match': {k: v}} for k, v in srch_dct.items()],
                    'filter': {
                        'range': {
                            'timestamp': {
                                'gte': tme_gte,
                                'lte': tme_lte
                            }
                        }
                    }
                }
            }
        }
        es_temp = self.get_elastic()
        result = es_temp.search(index=index_tbl, body=q)
        return result['hits']['hits']

    def create_document(self, index_tbl, id, doc_type, body):
        es_temp = self.get_elastic()
        result = True
        try:
            result_insert = es_temp.create(index=index_tbl, doc_type=doc_type, body=body, id=id)
            if result_insert['result'] != 'created':
                result = False
                print('Failed')
            else:
                print('Success Insert:' + body)
        except Exception as err:
            print(err)
            result = False
            return result
        return result

    def find(self, index, doc_type, fld_lst, val):
        lst = []
        es_host = self.get_elastic()
        q = {
            "query": {
                "multi_match": {
                    "query": val,
                    #"fields": ["symptom", "node_code", "network", "group"]
                    "fields": fld_lst
                }
            }
        }
        res = helpers.scan(client=es_host,
                           index=index,
                           query=q,
                           preserve_order=True)
        for data in res:
            try:
                if '_source' in data:
                    data_dct = data['_source']
                    data_dct['_id'] = data['_id']
                    data_dct['_score'] = data['_score']
                    lst.append(data_dct)
            except Exception as err:
                print("Error %s when find query " % str(err))
        return lst

    def get_id(self, _id, index, doc_type):
        try:
            es_host = self.get_elastic()
            res = es_host.get(index=index,
                              id=_id,
                              doc_type=doc_type)
            if '_source' in res:
                res_dct = res['_source']
                res_dct['_id'] = _id
                return res_dct

        except Exception as err:
            return dict()
        return dict()