__author__ = 'VTN-DHML-DHIP10'
import uuid
import json


class TblTopFwImpl:
    def __init__(self, node_name, node_ip, timestamp, session, sent_byte, recv_byte, totl_byte,
                 address, country, dest_port, dest_intf, rx_packet, tx_packet, rx_bandwidth, tx_bandwidth,
                 tx_sharper_drops, src_addr, des_addr, src_intf):
        self.src_intf = src_intf
        self.node_name = node_name
        self.node_ip = node_ip
        self.country = country
        self.timestamp = timestamp
        self.session = session
        self.sent_byte = sent_byte
        self.recv_byte = recv_byte
        self.totl_byte = totl_byte
        self.address = address
        self.dest_port = dest_port
        self.dest_intf = dest_intf
        self.rx_packet = rx_packet
        self.tx_packet = tx_packet
        self.rx_bandwidth = rx_bandwidth
        self.tx_bandwidth = tx_bandwidth
        self.tx_sharper_drops = tx_sharper_drops
        self.src_addr = src_addr
        self.des_addr = des_addr

    def get_not_duplicate_lst(self, dup_lst):
        # muc dich tim list khong bi duplicate
        res_lst = []
        for x in dup_lst:
            chek_exst = False
            if res_lst:
                for y in res_lst:
                    if self.check_duplicate(x, y):
                        chek_exst = True
                        break

                if not chek_exst:
                    res_lst.append(x)
            else:
                res_lst.append(x)
        return res_lst

    @staticmethod
    def check_duplicate(_tbl_top_x, _tbl_top_y):
        chk_dup = False
        try:
            if _tbl_top_x.node_ip == _tbl_top_y.node_ip and _tbl_top_x.country == _tbl_top_y.country and _tbl_top_x.timestamp == _tbl_top_y.timestamp and _tbl_top_x.address == _tbl_top_y.address:
                chk_dup = True
        except Exception as err:
            print("Error when check duplicate top fw impl two object")
            print(err)
            return False
        return chk_dup

    def post(self):
        tbl_json = json.dumps(self, default=lambda o: o.__dict__)
        id1 = uuid.uuid3(uuid.NAMESPACE_DNS, str(self.node_ip) + self.country + self.address + self.timestamp)
        srch_dct = dict(node_ip=self.node_ip, timestamp=self.timestamp,
                        area=self.country, address=self.address)
        res = True
        hits = es.query_search_must(srch_dct, "tbl_top_fw")
        if not hits:
            # insert new
            res = es.create_document("tbl_top_fw", id1.urn, "_doc", tbl_json)
            print(res)
        else:
            print(json.dumps(hits, indent=4))
            id1.urn = ''
            res = False
        return res
