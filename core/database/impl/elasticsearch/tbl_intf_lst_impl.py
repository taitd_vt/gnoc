__author__ = 'VTN-DHML-DHIP10'


class TblInterfaceListImpl:
    def __init__(self, node_begin, interface_name, ip, description, interface_status, timestamp, interface_mode,
                 speed, node_place):
        self.node_begin = node_begin
        self.interface_name = interface_name
        self.ip = ip
        self.description = description
        self.interface_status = interface_status
        self.timestamp = timestamp
        self.interface_mode = interface_mode
        self.speed = speed
        self.node_place = node_place

