import datetime, functools, requests, json, time
from config import Config, Development
from core.helpers.date_helpers import get_date_now, convert_date_to_epoch, get_date_minus
from core.database.impl.api_impl import ApiIron
from app.api.elasticsearch.api_kibana import ApiKibana
config = Development()


class TblIntfStatImpl(ApiIron):

    def __init__(self, node_begin, interface_name, ip, area, description, node_type, interface_status, interface_type,
                 cable_name, speed, interface_id, network_type, timestamp, max, node_place, max_in, max_out, error,
                 class_map, content):
        self.node_begin = node_begin
        self.interface_name = interface_name
        self.ip = ip
        self.area = area
        self.description = description
        self.node_type = node_type
        self.interface_status = interface_status
        self.interface_type = interface_type
        self.cable_name = cable_name
        self.speed = speed
        self.interface_id = interface_id
        self.network_type = network_type
        self.timestamp = timestamp
        self.max = max
        self.node_place = node_place
        self.error = error
        self.max_in = max_in
        self.max_out = max_out
        self.class_map = class_map
        self.content = content
        # khoang cach giua cac lan chay tinh theo phut
        self.interv = 15

    def get(self, coin_id = ""):
        coin_lst = self.get_api()
        if coin_lst:
            for x in coin_lst:
                return x
        else:
            return dict()

    def all(self):
        '''get all device with paging'''
        coin_lst = self.get_api()
        return coin_lst


class TblKibana:
    def __init__(self):
        pass

    def is_capacity_from_last(self, last_mins):
        _now = get_date_now()

        # lay thoi gian tu 60p - 30p truoc do elasticsearch tre 15p moi co ket qua
        # gio tren elasticsearch la gio GMT
        # _now_end = get_date_minus(_now, -7 * 60 + 30)
        _now_end = get_date_minus(_now, 0)
        # _now_bgn = get_date_minus(_now, - 7 * 60 + 60)
        _now_bgn = get_date_minus(_now, last_mins)

        _now_end_epch = int(convert_date_to_epoch(_now_end) * 1000)
        _now_bgn_epch = int(convert_date_to_epoch(_now_bgn) * 1000)

        _api = ApiKibana(_now_bgn_epch, _now_end_epch, "tbl_interface_status")
        chck_res_kib = False
        num_rept = 0

        while not chck_res_kib:
            max_cpct, max_util = _api.get_max_cpct_util()
            if max_cpct > 0 and max_util > 0:
                chck_res_kib = True
                return chck_res_kib
            else:
                time.sleep(3)
            if num_rept > 5:
                return chck_res_kib
        return chck_res_kib


if __name__ == '__main__':
    kick = TblKibana()
    res = kick.is_capacity_from_last(4 * 60)
    print(res)




