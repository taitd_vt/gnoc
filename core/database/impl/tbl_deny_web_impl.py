__author__ = 'VTN-DHML-DHIP10'
from config import Development
from core.helpers.api_helpers import api_delete_retry, api_post_retry, api_put_retry, api_get_retry
import requests
import json
config = Development()
API_SPRING_IPMS_SERVER = config.__getattribute__('API_SPRING_IPMS_SERVER')
API_SPRING_IPMS_USER = config.__getattribute__('API_SPRING_IPMS_USER')
API_SPRING_IPMS_PASS = config.__getattribute__('API_SPRING_IPMS_PASS')
API_SPRING_IPMS_PORT = config.__getattribute__('API_SPRING_IPMS_PORT')
API_SPRING_IPMS_SERVER_TENANT = config.__getattribute__('API_SPRING_IPMS_SERVER_TENANT')


class TblDenyWebImpl:
    def __init__(self):
        pass

    def get(self,id):
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/deny_web/id=" + str(id)
        headers_json = {"X-TENANT-ID": API_SPRING_IPMS_SERVER_TENANT, "username": API_SPRING_IPMS_USER,
                        "password": API_SPRING_IPMS_PASS,
                        "Content-Type": "application/json"}
        #r = requests.get(url_api, headers=headers_json)
        r = api_get_retry(url_api, headers_json)
        if r:
            if r.ok:
                result_json = json.loads(r.text)
                return result_json
        else:
            return dict()

    def list(self):
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/deny_web/"
        headers_json = {"X-TENANT-ID": API_SPRING_IPMS_SERVER_TENANT, "username": API_SPRING_IPMS_USER,
                        "password": API_SPRING_IPMS_PASS,
                        "Content-Type": "application/json"}
        # r = requests.get(url_api, headers=headers_json)
        r = api_get_retry(url_api, headers_json)
        if r:
            if r.ok:
                result_json = json.loads(r.text)
                return result_json
        else:
            return list()

    def put(self, id, **kwargs):
        result = False

        data_json = json.dumps(kwargs)

        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/deny_web/id=" + str(id)
        headers_json = {"X-TENANT-ID": API_SPRING_IPMS_SERVER_TENANT, "username": API_SPRING_IPMS_USER,
                        "password": API_SPRING_IPMS_PASS,
                        "Content-Type": "application/json"}
        # r = requests.put(url_api, headers=headers_json, data=data_json)
        r = api_put_retry(url_api, headers_json, data_json)
        if r:
            if r.ok:
                result_req = r.text
                if result_req == '':
                    return True

        else:
            return result

    def search(self, **kwargs):
        result = ''
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/deny_web/"
        headers_json = {"X-TENANT-ID": API_SPRING_IPMS_SERVER_TENANT, "username": API_SPRING_IPMS_USER,
                        "password": API_SPRING_IPMS_PASS,
                        "Content-Type": "application/json"}
        for k, v in kwargs.items():
            if isinstance(v, str):
                v_rep = v.replace("/", "")
                v_rep = v_rep.replace(";", "|")
            if k == 'web':
                # do holonplatform khong nhan duoc dau ";" nen ta replace dau ";" bang dau |
                url_api += 'web=' + v_rep
            else:
                url_api += 'search=' + v_rep
            # r = requests.get(url_api, headers=headers_json)
            r = api_get_retry(url_api, headers_json)
            if r:
                if r.ok:
                    result_req = json.loads(r.text)
                    return result_req

        return result

    def post(self, **kwargs):
        result = False
        data_json = json.dumps(kwargs)
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/deny_web/"
        headers_json = {"X-TENANT-ID": API_SPRING_IPMS_SERVER_TENANT, "username": API_SPRING_IPMS_USER,
                        "password": API_SPRING_IPMS_PASS,
                        "Content-Type": "application/json"}
        check_exist = self.search(**kwargs)
        if not check_exist:

            # r = requests.post(url_api, headers=headers_json, data=data_json)
            r = api_post_retry(url_api, headers_json, data_json)
            if r:
                if r.ok:
                    result_req = r.text
                    if result_req == '':
                        #print('Successful insert Ring info ' + str(data_json))
                        return True
        else:
            # da ton tai roi update thoi
            result_check = check_exist
            if len(result_check) > 1:
                #print('Error because two many result search ' + str(kwargs))
                pass
            else:
                if 'Id' in result_check[0]:
                    id_ring = result_check[0]['Id']
                    kwargs['Id'] = id_ring

                    res_put = self.put(id_ring, **kwargs)
                    if res_put:
                        #print('Successful update Ring info ' + str(kwargs))
                        return True

        return result

    def delete(self, id,):
        result = False
        url_api = "http://" + API_SPRING_IPMS_SERVER + ":" + str(API_SPRING_IPMS_PORT) + "/deny_web/id=" + str(id)
        headers_json = {"X-TENANT-ID": API_SPRING_IPMS_SERVER_TENANT, "username": API_SPRING_IPMS_USER,
                        "password": API_SPRING_IPMS_PASS,
                        "Content-Type": "application/json"}
        # r = requests.delete(url_api, headers=headers_json)
        r = api_delete_retry(url_api, headers_json)
        if r:
            if r.ok:
                result_req = r.text
                if result_req == '':
                    #print('Successful delete Ring info ' + str(id))
                    return True

        else:
            #print('Failed delete Ring info ' + str(id) + r.text)
            return result
