__author__ = 'vtn-dhml-dhip10'
import threading
import time
from core.log.logger.logger import Logging
from core.threading.device_thread.dev_scan_backhaul_upstream_lst import DeviceScanBackhaulUpstreamListThread
from core.helpers.list_helpers import chunkIt, get_pop_lst
from config import Development
from core.database.impl.forti_os.get_api_forti_os import FGT
from core.database.impl.elasticsearch.tbl_top_fw_impl import TblTopFwImpl
from core.helpers.date_helpers import get_date_minus_format_ipms, convert_date_to_ipms, \
    convert_date_to_elastic, get_time_format_now
config = Development()
FW_FTG_LST = config.__getattribute__('FW_FTG_LST')


class ScanFwTopSessionVolumeThread(threading.Thread):
    def __init__(self, is_stop, period_time):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.period_time = period_time
        self.thread_name = 'Scan Firewall Top Session Volume Thread'

    def run(self):
        interval = self.period_time
        check_late = False
        time_scan = None
        time_now = get_time_format_now()
        time_next_interval = plus_time_sec(time_now, interval)
        while not self.is_stop:
            log_scan = Logging('Scan Firewall Top', 'Thread get data of FW Session and Volume')

            try:
                time_now = get_time_format_now()
                date_time_elastic_now = convert_date_to_elastic(time_now)
                if check_late and time_scan:
                    time_now = time_scan
                    check_late = False

                time_next_interval = plus_time_sec(time_now, interval)
                hour_now = str(time_now.hour)
                minute_now = str(time_now.minute)
                second_now = str(time_now.second)
                year_now = str(time_now.year)
                month_now = str(time_now.month)
                day_now = str(time_now.day)
                date_format = '{0}_{1}_{2}_{3}_{4}_{5}'.format(year_now, month_now, day_now, hour_now, minute_now,
                                                               second_now)
                name_log = 'log_scan_backhaul_' + date_format + '.txt'
                log_scan = Logging(name_log, 'Thread get data of Backhaul and Upstream')
                log_scan.create_log('info', 'Start getting data Backhaul and Upstream')

                # scan auto
                res_fw_lst = []
                for x in FW_FTG_LST:
                    fw_nme = x['name']
                    fw_ip = x['ip']
                    fw_vdoom = x['vdoom']
                    fw_version = x['version']
                    fw_port = x['port']
                    fgt = FGT(fw_ip, fw_nme, fw_vdoom, fw_version)
                    res_lgn = fgt.login()
                    if res_lgn:
                        res_top_ses = fgt.get_top_session(date_time_elastic_now, fw_port)
                        res_top_vol = fgt.get_top_volume(date_time_elastic_now, fw_port)
                        if res_top_ses:
                            res_fw_lst = res_fw_lst + res_top_ses
                        if res_top_vol:
                            res_fw_lst = res_fw_lst + res_top_vol
                        print("Session and Volume of %s:" % fw_ip)
                        print(len(res_top_ses))
                        print(len(res_top_vol))
                        fgt.logout()
                if res_fw_lst:
                    _tbl_top_fw_impl = TblTopFwImpl('', '', '', 0, 0, 0, 0, '', '', '', '', 0, 0, 0, 0, 0, '', '', '')
                    res_fw_not_dup_lst = _tbl_top_fw_impl.get_not_duplicate_lst(res_fw_lst)
                    if res_fw_not_dup_lst:
                        for x in res_fw_not_dup_lst:
                            x.post()

                time_later = get_time_format_now()
                time_diff = time_later - time_now
                time_diff_second = convert_timedelta_to_second(time_diff)
                print('Diff time scan:' + str(time_diff_second))
                if interval >= time_diff_second:
                    time_sleep = interval - time_diff_second
                    print('Sleeping for next ' + str(time_sleep) + " s")
                    time.sleep(time_sleep)
                else:
                    check_late = True
                    time_scan = time_next_interval

            except Exception as err:
                print(err)
                time_later = get_time_format_now()
                time_diff = time_later - time_now
                time_diff_second = convert_timedelta_to_second(time_diff)

                print('Diff time:' + str(time_diff_second))
                if interval >= time_diff_second:
                    time_sleep = interval - time_diff_second
                    print('Sleeping for ' + str(time_sleep) + " s")
                    time.sleep(time_sleep)
                else:
                    check_late = True
                    time_scan = time_next_interval

                log_scan.create_log('critical', err)

    def stop(self):
        self.is_stop = True




def plus_time_sec(tme, num):
    import datetime
    return tme + datetime.timedelta(seconds=num)


def convert_timedelta_to_second(time_delta):

    return time_delta.total_seconds()


if __name__ == '__main__':
    #test = ScanBackhaulUpstreamThread(False, 60 * 7)
    #test.run()
    pass
