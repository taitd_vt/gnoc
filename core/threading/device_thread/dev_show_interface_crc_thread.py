__author__ = 'vtn-dhml-dhip10'
import threading
import time
import telebot
from core.log.logger.logger import Logging
import os
import uuid
from core.parsing.vipa_parsing.device_vipa import DeviceVipa
from core.helpers.date_helpers import get_date_minus_format_ipms, convert_date_to_ipms, \
    convert_date_to_elastic, get_time_format_now, convert_timedelta_to_second
from core.helpers.list_helpers import chunkIt, get_pop_lst
from core.helpers.int_helpers import convert_string_to_int
from app.api.vipa.device.api_device import ApiDevice
from config import Config, Development

config = Development()
USER_TACCAS = config.__getattribute__('USER_TACCAS')
USER_TACCAS_2 = config.__getattribute__('USER_TACCAS_2')
PASS_LOCAL = config.__getattribute__('PASS_LOCAL')
PASS_TACCAS = config.__getattribute__('PASS_TACCAS')
PASS_TACCAS_2 = config.__getattribute__('PASS_TACCAS_2')
SERVER_PARSING = config.__getattribute__('SERVER_PARSING')
SERVER_PARSING_USER = config.__getattribute__('SERVER_PARSING_USER')
SERVER_PARSING_PASS = config.__getattribute__('SERVER_PARSING_PASS')
BOT_ID = config.__getattribute__('BOT_ID')
BOT_GROUP_ID = -218379033


class DeviceShowInterfaceCrcThread(threading.Thread):
    def __init__(self, is_stop, time_ins, ip, intf, bot, link, caption, bot_mess_id, group_id):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.time_ins = time_ins
        self.thread_name = 'ShowInterfaceDrop'
        self.ip = ip
        self.intf = intf
        self.bot = bot
        self.caption = caption
        self.link = link
        self.bot_mess_id = bot_mess_id
        self.group_id = group_id

    def get_path(self):
        pth = os.path.dirname(__file__)
        path_join = os.path.join(pth, "../../parsing/template")
        _api_node = ApiDevice(self.ip, 'VNM')
        node_code_vipa, node_vend, node_vers = _api_node.get_name_ven_ver()
        node_vend = node_vend.lower()
        path_join = os.path.join(path_join, node_vend)
        path_policy_map = os.path.join(path_join, "show_interface_crc.textfsm")

        return path_policy_map

    def get_crc(self):
        date_time_now = self.time_ins
        crc_num = 0
        try:
            _api_node = ApiDevice(self.ip, 'VNM')
            node_code_vipa, node_vend, node_vers = _api_node.get_name_ven_ver()
            pth_policy_cc = self.get_path()
            intf_crc_lst = []
            if node_code_vipa and node_vend and node_vers:
                if node_code_vipa.upper().find('POP') >= 0:
                    device_test = DeviceVipa(node_code_vipa, node_vend, node_vers, USER_TACCAS, PASS_LOCAL)
                    intf_crc_lst = device_test.get_crc_of_interface(self.intf, pth_policy_cc)
                    if not intf_crc_lst:
                        device_test = DeviceVipa(node_code_vipa, node_vend, node_vers, USER_TACCAS_2, PASS_TACCAS_2)
                        intf_crc_lst = device_test.get_crc_of_interface(self.intf, pth_policy_cc)
                else:
                    device_test = DeviceVipa(node_code_vipa, node_vend, node_vers, USER_TACCAS_2, PASS_TACCAS_2)
                    intf_crc_lst = device_test.get_crc_of_interface(self.intf, pth_policy_cc)

                    if not intf_crc_lst:
                        device_test = DeviceVipa(node_code_vipa, node_vend, node_vers, USER_TACCAS, PASS_LOCAL)
                        intf_crc_lst = device_test.get_crc_of_interface(self.intf, pth_policy_cc)

                if intf_crc_lst:
                    tmp_str = 'BOT check:\n Node:' + node_code_vipa + '\n Interface:' + self.intf + '\n CRC: \n'
                    print(tmp_str)
                    for intf_crc in intf_crc_lst:
                        tmp_str = tmp_str + str(intf_crc) + "\n"
                        if 'crc' in intf_crc:
                            crc_num_str = intf_crc['crc']
                            crc_num = convert_string_to_int(crc_num_str)

                    self.caption = self.caption + "\n" + tmp_str

                    if self.bot_mess_id > 0:
                        # mess_check_once = self.bot.reply_mess(self.bot_mess_id, self.caption, self.group_id)
                        try:
                            # Doi 1p roi check tiep
                            time.sleep(300)
                            intf_crc_lst = device_test.get_crc_of_interface(self.intf, pth_policy_cc)
                            tmp_str = 'BOT check Again:\n Node:' + node_code_vipa + '\n Interface:' + self.intf + '\n CRC: \n'
                            print(tmp_str)
                            for intf_crc in intf_crc_lst:
                                tmp_str = tmp_str + str(intf_crc) + "\n"
                                if 'crc' in intf_crc:
                                    crc_num_new_str = intf_crc['crc']
                                    crc_num_new = convert_string_to_int(crc_num_new_str)
                                    if crc_num_new:
                                        crc_chge = crc_num_new - crc_num
                                        tmp_str = tmp_str + ' CRC change:' + str(crc_chge) + "\n"
                                        # neu crc > 0 moi day
                                        if crc_chge > 0:
                                            self.caption = self.caption + "\n" + tmp_str
                                            mess_id = self.bot.send_photo(self.link, self.caption, self.group_id)

                        except Exception as err:
                            print("Error %s when reply message of drop packet" % err)
                            self.caption += "\n Error " + str(err) + " when reply message of crc packet"
                            mess_id = self.bot.send_photo(self.link, self.caption, self.group_id)
                            return intf_crc_lst
                    else:
                        # mess_check_once = self.bot.send_mess(self.caption, self.group_id)
                        try:
                            time.sleep(300)
                            intf_crc_lst = device_test.get_crc_of_interface(self.intf, pth_policy_cc)
                            tmp_str = 'BOT check interface CRC Again:' + node_code_vipa + ' ' + self.intf + 'CRC \n'
                            print(tmp_str)
                            for intf_crc in intf_crc_lst:
                                tmp_str = tmp_str + str(intf_crc) + "\n"
                                if 'crc' in intf_crc:
                                    crc_num_new_str = intf_crc['crc']
                                    crc_num_new = convert_string_to_int(crc_num_new_str)
                                    if crc_num_new:
                                        crc_chge = crc_num_new - crc_num
                                        tmp_str = tmp_str + ' CRC change:' + str(crc_chge) + "\n"
                                        self.caption = self.caption + "\n" + tmp_str
                                        if crc_chge > 0:
                                            mess_id = self.bot.send_photo(self.link, self.caption, self.group_id)

                        except Exception as err:
                            print("Error %s when reply message of crc packet" % err)
                            self.caption += "\n Error " + str(err) + " when reply message of crc packet"
                            mess_id = self.bot.send_mess(self.caption, self.group_id)
                            return intf_crc_lst
                else:
                    print('Check CRC fail because can not get CRC parameter')
                    self.caption += '\n Check CRC fail because can not get CRC parameter'
                    mess_id = self.bot.send_photo(self.link, self.caption, self.group_id)
            else:
                print("No node on Vipa for crc check")
                self.caption += '\n No node on Vipa for crc check'
                mess_id = self.bot.send_photo(self.link, self.caption, self.group_id)

            return intf_crc_lst
        except Exception as err:
            print("Error %s when get interface %s drop" % err, self.intf)
            self.caption += "\n Error " + str(err) + " when reply message of crc packet"
            mess_id = self.bot.send_photo(self.link, self.caption, self.group_id)
            return []

    def run(self):
        while not self.is_stop:
            log_scan = Logging('Scan Interface Drop', 'Thread get data of Interface Drop')

            try:
                time_now = get_time_format_now()
                hour_now = str(time_now.hour)
                minute_now = str(time_now.minute)
                second_now = str(time_now.second)
                year_now = str(time_now.year)
                month_now = str(time_now.month)
                day_now = str(time_now.day)
                date_format = '{0}_{1}_{2}_{3}_{4}_{5}'.format(year_now, month_now, day_now, hour_now, minute_now,
                                                               second_now)
                name_log = 'log_scan_interface_drop_' + date_format + '.txt'
                log_scan = Logging(name_log, 'Thread get data of Interface Drop')
                log_scan.create_log('info', 'Start getting data Interface drop')

                self.get_crc()
                self.is_stop = True
            except Exception as err:
                print(err)
                log_scan.create_log('critical', str(err) + 'while insert backhaul upstream')
                self.is_stop = True

    def stop(self):
        self.is_stop = True


if __name__ == '__main__':
    date_time_now = get_time_format_now()
    _obj_test = dict(alarm_name='over-threshold', node_name='HLC102.CKV.CRS16.01',
                     interface='TenGigE0/0/1/1 -45',
                     interface_description='BB_2B',
                     current='200.00%',
                     group_name='IPBN_FOIP_KV3_DROP_POP',
                     date_time='2020-07-29T17:20:02+0700',
                     link='http://192.168.251.25/?target=gplot&id%5B%5D=3009398&f=2020-07-02+17:28:02&t=2019-07-03+17:28:02',
                     node_ip='10.41.238.20')
    # NocproAlm[24425]: |HHT9602CRT22.PECD.ASR22.03|10.73.204.78|HundredGigE0/7/1/1 - 3072|To_SW_FNA_SGN5-5_Port10|FOIP_KV3_PECD|10|44.9722%||FOIP_KV3_PECD - HHT9602CRT22.PECD.ASR22.03 (10.73.204.78) - HundredGigE0/7/1/1 - 3072 (To_SW_FNA_SGN5-5_Port10) 2194508 Packet error is 3409 packets/s over 0.0033 packets/s 17:12:11 03/10|http://10.73.224.68/?target=gplot&id%5B%5D=2194508&f=2019-10-03+15:12:11&t=2019-10-03+19:12:11
    _bot = telebot.AsyncTeleBot(BOT_ID)
    #_dev = DeviceShowInterfaceCrcThread(False, date_time_now, '10.60.255.253 ', 'Cisco', 'GigabitEthernet0/1/0/39', _bot,
    #                                    'http://10.73.224.68/?target=gplot&id%5B%5D=1845493&f=2019-10-03+14:20:14&t=2019-10-03+18:20:14',
    #                                    'tester', 0)
    _dev_crc = DeviceShowInterfaceCrcThread(False, date_time_now, '10.56.255.247', 'TenGigE0/8/0/1',
                                            _bot, 'http://192.168.251.25/?target=gplot&id%5B%5D=3009398&f=2020-07-02+17:28:02&t=2019-07-03+17:28:02', 'tester', 123456, -345980469)
    _dev_crc.start()
