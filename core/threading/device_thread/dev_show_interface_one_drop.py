__author__ = 'vtn-dhml-dhip10'
import threading
import time
from core.log.logger.logger import Logging
from config import Development

config = Development()
SERVER_PARSING = config.__getattribute__('SERVER_PARSING')
SERVER_PARSING_USER = config.__getattribute__('SERVER_PARSING_USER')
SERVER_PARSING_PASS = config.__getattribute__('SERVER_PARSING_PASS')
import os, uuid
from core.parsing.vipa_parsing.device_vipa import DeviceVipa
from core.helpers.date_helpers import get_date_minus_format_ipms, convert_date_to_ipms, \
    convert_date_to_elastic, get_time_format_now, convert_timedelta_to_second
from core.helpers.list_helpers import chunkIt, get_pop_lst
from app.api.vipa.device.api_device import ApiDevice
from collections import namedtuple

USER_TACCAS = config.__getattribute__('USER_TACCAS')
PASS_LOCAL = config.__getattribute__('PASS_LOCAL')
PASS_TACCAS = config.__getattribute__('PASS_TACCAS')


class DeviceShowInterfaceOneDrop():
    def __init__(self, is_stop, time_ins, ip, intf, link, caption):
        self.is_stop = is_stop
        self.time_ins = time_ins
        self.thread_name = 'ShowInterfaceDrop'
        self.ip = ip
        self.intf = intf
        self.caption = caption
        self.link = link

    def get_path(self):
        pth = os.path.dirname(__file__)
        path_join = os.path.join(pth, "../../parsing/template")
        _api_node = ApiDevice(self.ip, 'VNM')
        node_code_vipa, node_vend, node_vers = _api_node.get_name_ven_ver()
        node_vend = node_vend.lower()
        path_join = os.path.join(path_join, node_vend)
        path_policy_map = os.path.join(path_join, "show_policy_map_interface_drop.textfsm")

        return path_policy_map

    def get_drop(self):
        date_time_now = self.time_ins
        time_now = date_time_now
        date_time_ipms_now = convert_date_to_ipms(date_time_now)
        date_time_elastic_now = convert_date_to_elastic(date_time_now)
        try:
            _api_node = ApiDevice(self.ip, 'VNM')
            node_code_vipa, node_vend, node_vers = _api_node.get_name_ven_ver()
            pth_policy_drop = self.get_path()
            intf_drop_lst =[]
            if node_code_vipa and node_vend and node_vers:
                device_test = DeviceVipa(node_code_vipa, node_vend, node_vers, USER_TACCAS, PASS_LOCAL)
                intf_drop_lst = device_test.get_drop_of_interface(self.intf, pth_policy_drop)
                tmp_str = 'BOT check:\n Node:' + node_code_vipa + ' \n interface:' + self.intf +'\n Drop \n'
                for intf_drop in intf_drop_lst:
                    tmp_str = tmp_str + str(intf_drop) + "\n"

                self.caption = self.caption + "\n" + tmp_str
            else:
                print("No node on Vipa for drop check")
                self.caption = self.caption + "\n" + "No node on Vipa for drop check"

            return self.caption
        except Exception as err:
            print("Error %s when get interface %s drop" % err, self.intf)
            self.caption = self.caption + "\n" + "Error when get interface drop"  + str(err)
            return self.caption

    def run(self):
        while not self.is_stop:
            log_scan = Logging('Scan Interface Drop', 'Thread get data of Interface Drop')

            try:
                time_now = get_time_format_now()
                hour_now = str(time_now.hour)
                minute_now = str(time_now.minute)
                second_now = str(time_now.second)
                year_now = str(time_now.year)
                month_now = str(time_now.month)
                day_now = str(time_now.day)
                date_format = '{0}_{1}_{2}_{3}_{4}_{5}'.format(year_now, month_now, day_now, hour_now, minute_now,
                                                               second_now)
                name_log = 'log_scan_interface_drop_' + date_format + '.txt'
                log_scan = Logging(name_log, 'Thread get data of Interface Drop')
                log_scan.create_log('info', 'Start getting data Interface drop')

                res = self.get_drop()
                self.is_stop = True
                return res
            except Exception as err:
                print(err)
                log_scan.create_log('critical', str(err) + 'while insert backhaul upstream')
                self.is_stop = True
                return str(err)

    def stop(self):
        self.is_stop = True


if __name__ == '__main__':
    date_time_now = get_time_format_now()
    _dev = DeviceShowInterfaceDropThread(False, date_time_now, '125.235.255.241', 'Cisco', 'Bundle-Ether33')
    _dev.start()
