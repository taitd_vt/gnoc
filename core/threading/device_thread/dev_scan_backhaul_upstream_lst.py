__author__ = 'vtn-dhml-dhip10'
import threading

from core.log.logger.logger import Logging
from config import Development

config = Development()
SERVER_PARSING = config.__getattribute__('SERVER_PARSING')
SERVER_PARSING_USER = config.__getattribute__('SERVER_PARSING_USER')
SERVER_PARSING_PASS = config.__getattribute__('SERVER_PARSING_PASS')
import os, uuid
from core.parsing.vipa_parsing.device_vipa import DeviceVipa
from core.database.impl.ipms.host_ipms_impl import HostIpmsImpl
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.helpers.tenant_helpers import get_tenant_id
from core.helpers.date_helpers import get_date_minus_format_ipms, convert_date_to_ipms, \
    convert_date_to_elastic, get_time_format_now
from core.helpers.list_helpers import chunkIt, get_pop_lst
from app.api.vipa.device.api_device import ApiDevice

from collections import namedtuple



class DeviceScanBackhaulUpstreamListThread(threading.Thread):
    def __init__(self, is_stop, time_ins, dev_type, dev_lst, vendor):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.time_ins = time_ins
        self.thread_name = 'ScanBackhaulUpstream'
        self.dev_type = dev_type
        self.dev_lst = dev_lst
        self.vendor = vendor

    def get_path(self):
        pth = os.path.dirname(__file__)
        path_join = os.path.join(pth, "../../parsing/template")
        path_join = os.path.join(path_join, self.vendor)
        path_upstream = os.path.join(path_join, "show_max_upstream.textfsm")
        path_backhaul = os.path.join(path_join, "show_max_backhaul.textfsm")
        path_bw = os.path.join(path_join, "show_interface_inc_bandwidth.textfsm")
        path_bw_inp_outp = os.path.join(path_join, "show_interface_input_output_bandwidth.textfsm")
        path_bw_class_map = os.path.join(path_join, "show_interface_traffic_class_map.textfsm")
        path_intf_lst_class_map = os.path.join(path_join, "show_interface_class_map.textfsm")
        path_intf_one_description = os.path.join(path_join, "show_interface_description_one_link.textfsm")
        return path_upstream, path_backhaul, path_bw, path_bw_inp_outp, \
               path_bw_class_map, path_intf_lst_class_map, path_intf_one_description

    def get_manl_lst(self, host_nme_lst):
        res_lst = list()
        for host_nme in host_nme_lst:
            res_host_manual_lst = list()
            api_spring_host_ipms = ApiSpringIpmsImpl('host', 'kv3068')
            srch_hst_name_manual_dct = {'searchHostName': host_nme}
            res_host_manual_lst = api_spring_host_ipms.get(srch_hst_name_manual_dct)

            if res_host_manual_lst:
                for x in res_host_manual_lst:
                    host_obj = namedtuple("HostIpms", x.keys())(*x.values())
                    host_ipms_obj = HostIpmsImpl(host_obj.rid, host_obj.ip, host_obj.vgroup,
                                                 host_obj.hostname, host_obj.sysDescr, host_obj.poller)

                    host_ipms_obj = host_ipms_obj.get_vendor()
                    res_lst.append(host_ipms_obj)
        return res_lst

    def insert_intf_manual(self):
        # Create an SSH client
        date_time_now = self.time_ins
        date_time_ipms_now = convert_date_to_ipms(date_time_now)
        date_time_ipms_last_15_mins = get_date_minus_format_ipms(date_time_now, 15)
        date_time_elastic_now = convert_date_to_elastic(date_time_now)
        elk_imp_lst = list()
        # client = paramiko.SSHClient()
        # Make sure that we add the remote server's SSH key automatically
        # client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        # Connect to the client
        try:
            # client.connect(SERVER_PARSING, username=SERVER_PARSING_USER, password=SERVER_PARSING_PASS)
            pth_upstrm, pth_bckhaul, pth_bw, pth_bw_inp_outp, pth_class_map, \
            pth_link_clsm, pth_link_one_des = self.get_path()

            # ID URN default
            id_urn_def = uuid.uuid3(uuid.NAMESPACE_DNS, '')
            # Xac dinh thuoc Host Group FOIP IPBN truoc

            api_spring_host_grp_ipms = ApiSpringIpmsImpl('hostGroup', 'kv3068')
            srch_grp_name_dct = {'searchGroupName': 'FOIP_IPBN'}
            group_name_lst = api_spring_host_grp_ipms.get(srch_grp_name_dct)
            group_id_lst = list()
            if group_name_lst:
                for x in group_name_lst:
                    if 'group_id' in x.keys():
                        group_id_lst.append(x['group_id'])

            # Check manual list
            host_manual_lst = self.get_manl_lst(self.dev_lst)
            id_intf_manual = 1
            intf_l3ll = 'Bundle-Ether99'

            if host_manual_lst:
                for x in host_manual_lst:
                    # find poller id and tenant id
                    poller_name = x.poller
                    rid = x.rid
                    check_poller = True
                    tenant_id = ''
                    host_name = x.hostname.upper()
                    host_type = 'POP_TRANSIT'

                    if poller_name:
                        # tim ip cua poller name
                        # tim poller iddi
                        api_spring_poll_ipms = ApiSpringIpmsImpl('poller', 'kv3068')
                        srch_poll_dct = {'searchPoller': poller_name}
                        res_poll_lst = api_spring_poll_ipms.get(srch_poll_dct)
                        if len(res_poll_lst) == 1:
                            if 'poller_ip' in res_poll_lst[0]:
                                poller_ip = res_poll_lst[0]['poller_ip']
                                tenant_id = get_tenant_id(poller_ip)

                        else:
                            print('Error There are two many or do not find poller id')
                            check_poller = False

                    # Create a raw shell
                    # shell = client.invoke_shell()
                    # tim host name cua vipa
                    _api_node = ApiDevice(x.ip, 'VNM')
                    node_code_vipa, node_vend, node_vers = _api_node.get_name_ven_ver()
                    if node_code_vipa and node_vend and node_vers:
                        device_test = DeviceVipa(node_code_vipa, node_vend, node_vers, 'diepnk', 'Khongbiet@888')

                        # check link qos
                        intf_class_map_lst = device_test.get_link_class_map_lst(pth_link_clsm)
                        if intf_class_map_lst:
                            for intf_clsm in intf_class_map_lst:
                                name_clsm_lst, rate_clsm = device_test.get_class_map_traff_link(intf_clsm,
                                                                                                pth_class_map)
                                # insert vao elasticsearch
                                if rate_clsm:
                                    intf_one_desc = device_test.get_description_link(intf_clsm, pth_link_one_des)
                                    intf_bw = device_test.get_bandwidth_link(intf_clsm, pth_bw)

                                    index_clsm = -1
                                    for name_clsm in name_clsm_lst:
                                        index_clsm += 1
                                        id_intf_manual, id_els, tbl_json = \
                                            device_test.insert_elastic_upstream_clsm(
                                                intf_clsm,
                                                intf_one_desc,
                                                x,
                                                date_time_ipms_last_15_mins,
                                                date_time_ipms_now,
                                                tenant_id,
                                                poller_name,
                                                date_time_elastic_now,
                                                'IPBN_UPSTREAM_QOS',
                                                id_intf_manual,
                                                host_type,
                                                rate_clsm[index_clsm],
                                                name_clsm, intf_bw)
                                        if id_els != '' and id_els != id_urn_def:
                                            elk_imp_lst.append(dict(id=id_els, data=tbl_json))
                                        else:
                                            print('Duplicate id')

                        # insert them interface bundle-ether 99 phuc vu luu luong L3LL
                        intf_l3ll_desc = device_test.get_description_link(intf_l3ll, pth_link_one_des)
                        if intf_l3ll_desc:
                            id_int_manual, id_els, tbl_json = \
                                device_test.insert_elastic_upstream_backhaul(intf_l3ll,
                                                                             intf_l3ll_desc,
                                                                             x,
                                                                             date_time_ipms_last_15_mins,
                                                                             date_time_ipms_now,
                                                                             tenant_id,
                                                                             poller_name,
                                                                             date_time_elastic_now,
                                                                             'IPBN_BACKHAUL_L3LL',
                                                                             id_intf_manual,
                                                                             host_type)
                            if id_els != '' and id_els != id_urn_def:
                                elk_imp_lst.append(dict(id=id_els, data=tbl_json))
                            else:
                                print('Duplicate id')

                        intf_bckhl_lst, des_bchk_lst = device_test.get_intf_desc_backhaul(pth_upstrm)

                        index_intf = 0
                        for x_intf in intf_bckhl_lst:
                            intf_desc = des_bchk_lst[index_intf]
                            id_int_manual, id_els, tbl_json = \
                                device_test.insert_elastic_upstream_backhaul(x_intf,
                                                                             intf_desc,
                                                                             x,
                                                                             date_time_ipms_last_15_mins,
                                                                             date_time_ipms_now,
                                                                             tenant_id,
                                                                             poller_name,
                                                                             date_time_elastic_now,
                                                                             'IPBN_BACKHAUL',
                                                                             id_intf_manual,
                                                                             host_type)
                            if id_els != '' and id_els != id_urn_def:
                                elk_imp_lst.append(dict(id=id_els, data=tbl_json))
                            else:
                                print('Duplicate id')

                            id_int_manual, id_els, tbl_json = \
                                device_test.insert_elastic_upstream_backhaul(x_intf,
                                                                             intf_desc,
                                                                             x,
                                                                             date_time_ipms_last_15_mins,
                                                                             date_time_ipms_now,
                                                                             tenant_id,
                                                                             poller_name,
                                                                             date_time_elastic_now,
                                                                             'IPBN_UPSTREAM',
                                                                             id_intf_manual,
                                                                             host_type)
                            if id_els != '' and id_els != id_urn_def:
                                elk_imp_lst.append(dict(id=id_els, data=tbl_json))
                            else:
                                print('Duplicate id')
                            index_intf += 1
        except Exception as err:
            print("Error when inserting manual device " + str(err))

        if elk_imp_lst:

            for x in elk_imp_lst:
                if 'id' in x and 'data' in x:
                    id_els = x['id']
                    tbl_json = x['data']
                    res = es.create_document("tbl_interface_status", id_els, "_doc", tbl_json)


    def insert_intf_auto(self):
        date_time_now = self.time_ins
        date_time_ipms_now = convert_date_to_ipms(date_time_now)
        date_time_ipms_last_15_mins = get_date_minus_format_ipms(date_time_now, 15)
        date_time_elastic_now = convert_date_to_elastic(date_time_now)
        elk_imp_lst = list()
        # client = paramiko.SSHClient()
        # Make sure that we add the remote server's SSH key automatically
        # client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        # Connect to the client
        try:
            # client.connect(SERVER_PARSING, username=SERVER_PARSING_USER, password=SERVER_PARSING_PASS)
            pth_upstrm, pth_bckhaul, pth_bw, pth_bw_inp_outp, pth_class_map, \
            pth_link_clsm, pth_link_one_des = self.get_path()

            # ID URN default
            id_urn_def = uuid.uuid3(uuid.NAMESPACE_DNS, '')

            id_intf_manual = 1
            intf_l3ll = 'Bundle-Ether99'

            for x_node in self.dev_lst:
                # find poller id and tenant id
                poller_name = x_node.poller
                rid = x_node.rid
                check_poller = True
                tenant_id = ''
                host_name = x_node.hostname.upper()
                if host_name.find('PEERING') > 0:
                    host_type = 'POP_PEERING'
                else:
                    host_type = 'POP_TRANSIT'

                if poller_name:
                    # tim ip cua poller name
                    # tim poller id
                    api_spring_poll_ipms = ApiSpringIpmsImpl('poller', 'kv3068')
                    srch_poll_dct = {'searchPoller': poller_name}
                    res_poll_lst = api_spring_poll_ipms.get(srch_poll_dct)
                    if len(res_poll_lst) == 1:
                        if 'poller_ip' in res_poll_lst[0]:
                            poller_ip = res_poll_lst[0]['poller_ip']
                            tenant_id = get_tenant_id(poller_ip)

                    else:
                        print('Error There are two many or do not find poller id')
                        check_poller = False

                        # Create a raw shell
                        # shell = client.invoke_shell()
                        # tim host name cua vipa
                _api_node = ApiDevice(x_node.ip, 'VNM')
                node_code_vipa, node_vend, node_vers = _api_node.get_name_ven_ver()
                if node_code_vipa and node_vend and node_vers:
                    device_test = DeviceVipa(node_code_vipa, node_vend, node_vers, 'dungnd18', 'dungnd18@123')

                    # check_telnet = device_test.telnet(shell)

                    # check link qos
                    intf_class_map_lst = device_test.get_link_class_map_lst(pth_link_clsm)
                    for intf_clsm in intf_class_map_lst:
                        name_clsm_lst, rate_clsm = device_test.get_class_map_traff_link(intf_clsm, pth_class_map)
                        # insert vao elasticsearch
                        if rate_clsm:
                            intf_one_desc = device_test.get_description_link(intf_clsm, pth_link_one_des)
                            intf_bw = device_test.get_bandwidth_link(intf_clsm, pth_bw)
                            index_clsm = -1
                            for name_clsm in name_clsm_lst:
                                index_clsm += 1
                                id_intf_manual, id_els, tbl_json = \
                                    device_test.insert_elastic_upstream_clsm(
                                        intf_clsm,
                                        intf_one_desc,
                                        x_node,
                                        date_time_ipms_last_15_mins,
                                        date_time_ipms_now,
                                        tenant_id, poller_name,
                                        date_time_elastic_now,
                                        'IPBN_UPSTREAM_QOS',
                                        id_intf_manual,
                                        host_type,
                                        rate_clsm[index_clsm],
                                        name_clsm,
                                        intf_bw)
                                if id_els != '' and id_els != id_urn_def:
                                    elk_imp_lst.append(dict(id=id_els, data=tbl_json))
                                else:
                                    print('Duplicate id')
                    # insert them interface bundle-ether 99 phuc vu luu luong L3LL
                    intf_l3ll_desc = device_test.get_description_link(intf_l3ll, pth_link_one_des)
                    if intf_l3ll_desc:
                        id_int_manual, id_els, tbl_json = \
                            device_test.insert_elastic_upstream_backhaul(intf_l3ll,
                                                                         intf_l3ll_desc,
                                                                         x_node,
                                                                         date_time_ipms_last_15_mins,
                                                                         date_time_ipms_now,
                                                                         tenant_id,
                                                                         poller_name,
                                                                         date_time_elastic_now,
                                                                         'IPBN_BACKHAUL_L3LL',
                                                                         id_intf_manual,
                                                                         host_type)
                        if id_els != '' and id_els != id_urn_def:
                            elk_imp_lst.append(dict(id=id_els, data=tbl_json))
                        else:
                            print('Duplicate id')
                    intf_lst, des_lst = device_test.get_intf_desc_upstream(pth_upstrm)
                    intf_bckhl_lst, des_bchk_lst = device_test.get_intf_desc_backhaul(pth_upstrm)
                    index_intf = 0
                    # co danh sach cong roi sau do show interface de tim list
                    for x_intf in intf_lst:
                        intf_desc = des_lst[index_intf]
                        id_int_manual, id_els, tbl_json = \
                            device_test.insert_elastic_upstream_backhaul(x_intf,
                                                                         intf_desc,
                                                                         x_node,
                                                                         date_time_ipms_last_15_mins,
                                                                         date_time_ipms_now,
                                                                         tenant_id,
                                                                         poller_name,
                                                                         date_time_elastic_now,
                                                                         'IPBN_UPSTREAM',
                                                                         id_intf_manual,
                                                                         host_type)
                        if id_els != '' and id_els != id_urn_def:
                            elk_imp_lst.append(dict(id=id_els, data=tbl_json))
                        else:
                            print('Duplicate id')
                        index_intf += 1
                    index_intf = 0
                    for x_intf in intf_bckhl_lst:
                        intf_desc = des_bchk_lst[index_intf]
                        id_int_manual, id_els, tbl_json = \
                            device_test.insert_elastic_upstream_backhaul(x_intf,
                                                                         intf_desc,
                                                                         x_node,
                                                                         date_time_ipms_last_15_mins,
                                                                         date_time_ipms_now,
                                                                         tenant_id,
                                                                         poller_name,
                                                                         date_time_elastic_now,
                                                                         'IPBN_BACKHAUL',
                                                                         id_intf_manual,
                                                                         host_type)
                        if id_els != '' and id_els != id_urn_def:
                            elk_imp_lst.append(dict(id=id_els, data=tbl_json))
                        else:
                            print('Duplicate id')
                        index_intf += 1
                        # exit device

        except Exception as err:
            print('Error when inserting auto device ' + str(err))

        if elk_imp_lst:

            for x in elk_imp_lst:
                if 'id' in x and 'data' in x:
                    id_els = x['id']
                    tbl_json = x['data']
                    res = es.create_document("tbl_interface_status", id_els, "_doc", tbl_json)


    def run(self):

        while not self.is_stop:
            self.is_stop = False
            log_scan = Logging('Scan Backhaul Upstream in List', 'Thread get data of Backhaul and Upstream')

            try:
                time_now = get_time_format_now()
                hour_now = str(time_now.hour)
                minute_now = str(time_now.minute)
                second_now = str(time_now.second)
                year_now = str(time_now.year)
                month_now = str(time_now.month)
                day_now = str(time_now.day)
                date_format = '{0}_{1}_{2}_{3}_{4}_{5}'.format(year_now, month_now, day_now, hour_now, minute_now,
                                                               second_now)
                name_log = 'log_scan_backhaul_' + date_format + '.txt'
                log_scan = Logging(name_log, 'Thread get data of Backhaul and Upstream')
                log_scan.create_log('info', 'Start getting data Backhaul and Upstream')
                if self.dev_type == 'manual':
                    self.insert_intf_manual()
                else:
                    self.insert_intf_auto()
                self.is_stop = True

            except Exception as err:
                print(err)
                self.is_stop = True
                log_scan.create_log('critical', str(err) + 'while insert backhaul upstream')

    def stop(self):
        self.is_stop = True


if __name__ == '__main__':
    date_time_now = get_time_format_now()
    hst_mnl_lst = ['HHT9602CRT24.IGW.ASR22.04', 'HCM_IW2_ASR9K_HHTN4', 'HKH9103CRT13.DGW.ASR12.01']
    test = DeviceScanBackhaulUpstreamListThread(False, date_time_now, 'manual', hst_mnl_lst, 'cisco')
    test.insert_intf_manual()

    # test auto
    pop_lst = get_pop_lst()
    host_lst_div = chunkIt(pop_lst, 2)
    test = DeviceScanBackhaulUpstreamListThread(False, date_time_now, 'auto', host_lst_div[1], 'cisco')
    #test.run()
