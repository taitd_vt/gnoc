__author__ = 'vtn-dhml-dhip10'
import threading
import time
import telebot
from core.log.logger.logger import Logging
from config import Development

config = Development()
SERVER_PARSING = config.__getattribute__('SERVER_PARSING')
SERVER_PARSING_USER = config.__getattribute__('SERVER_PARSING_USER')
SERVER_PARSING_PASS = config.__getattribute__('SERVER_PARSING_PASS')
BOT_ID = config.__getattribute__('BOT_ID')
BOT_GROUP_ID = -218379033
import os, uuid
from core.parsing.vipa_parsing.device_vipa import DeviceVipa
from core.helpers.date_helpers import get_date_minus_format_ipms, convert_date_to_ipms, \
    convert_date_to_elastic, get_time_format_now, convert_timedelta_to_second
from core.helpers.int_helpers import convert_string_to_int
from app.api.vipa.device.api_device import ApiDevice
from collections import namedtuple
from config import Config, Development
config = Development()
USER_TACCAS = config.__getattribute__('USER_TACCAS')
PASS_LOCAL = config.__getattribute__('PASS_LOCAL')
USER_TACCAS_2 = config.__getattribute__('USER_TACCAS_2')
PASS_TACCAS_2 = config.__getattribute__('PASS_TACCAS_2')


class DeviceShowInterfaceCrc():
    def __init__(self, is_stop, time_ins, ip, intf, link, caption):
        self.is_stop = is_stop
        self.time_ins = time_ins
        self.thread_name = 'ShowInterfaceDrop'
        self.ip = ip
        self.intf = intf
        self.caption = caption
        self.link = link

    def get_path(self):
        pth = os.path.dirname(__file__)
        path_join = os.path.join(pth, "../../parsing/template")
        _api_node = ApiDevice(self.ip, 'VNM')
        node_code_vipa, node_vend, node_vers = _api_node.get_name_ven_ver()
        node_vend = node_vend.lower()
        path_join = os.path.join(path_join, node_vend)
        path_policy_map = os.path.join(path_join, "show_interface_crc.textfsm")
        return path_policy_map

    def get_crc(self):
        crc_num = 0
        try:
            _api_node = ApiDevice(self.ip, 'VNM')
            node_code_vipa, node_vend, node_vers = _api_node.get_name_ven_ver()
            pth_policy_cc = self.get_path()
            intf_crc_lst = []
            if node_code_vipa and node_vend and node_vers:
                if node_code_vipa.upper().find('POP') >= 0:
                    device_test = DeviceVipa(node_code_vipa, node_vend, node_vers, USER_TACCAS, PASS_LOCAL)
                    intf_crc_lst = device_test.get_crc_of_interface(self.intf, pth_policy_cc)
                    if not intf_crc_lst:
                        device_test = DeviceVipa(node_code_vipa, node_vend, node_vers, USER_TACCAS_2, PASS_TACCAS_2)
                        intf_crc_lst = device_test.get_crc_of_interface(self.intf, pth_policy_cc)
                else:
                    device_test = DeviceVipa(node_code_vipa, node_vend, node_vers, USER_TACCAS_2, PASS_TACCAS_2)
                    intf_crc_lst = device_test.get_crc_of_interface(self.intf, pth_policy_cc)
                    if not intf_crc_lst:
                        device_test = DeviceVipa(node_code_vipa, node_vend, node_vers, USER_TACCAS, PASS_LOCAL)
                        intf_crc_lst = device_test.get_crc_of_interface(self.intf, pth_policy_cc)

                if intf_crc_lst:
                    tmp_str = '\n BOT check:\n Node:' + node_code_vipa + '\n Interface:' + self.intf + '\nCRC:'
                    for intf_crc in intf_crc_lst:
                        tmp_str = tmp_str + str(intf_crc) + "\n"
                        if 'crc' in intf_crc:
                            crc_num_str = intf_crc['crc']
                            crc_num = convert_string_to_int(crc_num_str)

                    self.caption = self.caption + tmp_str

                    return self.caption, crc_num

            else:
                print("No node on Vipa for drop check")
                self.caption = "No node on Vipa for drop check"
            return self.caption, crc_num
        except Exception as err:
            print("Error %s when get interface %s drop" % err, self.intf)
            self.caption = "Error %s when get interface %s drop" % err, self.intf
            return self.caption, crc_num

    def run(self):
            result = ''

            log_scan = Logging('Scan Interface Drop', 'Thread get data of Interface Drop')

            try:
                time_now = get_time_format_now()
                hour_now = str(time_now.hour)
                minute_now = str(time_now.minute)
                second_now = str(time_now.second)
                year_now = str(time_now.year)
                month_now = str(time_now.month)
                day_now = str(time_now.day)
                date_format = '{0}_{1}_{2}_{3}_{4}_{5}'.format(year_now, month_now, day_now, hour_now, minute_now,
                                                               second_now)
                name_log = 'log_scan_interface_drop_' + date_format + '.txt'
                log_scan = Logging(name_log, 'Thread get data of Interface Drop')
                log_scan.create_log('info', 'Start getting data Interface drop')

                result, crc_num = self.get_crc()
                return result, crc_num
            except Exception as err:
                print(err)
                log_scan.create_log('critical', str(err) + 'while insert backhaul upstream')
                result = err
                return result

    def stop(self):
        self.is_stop = True


if __name__ == '__main__':
    date_time_now = get_time_format_now()
    _obj_test = dict(alarm_name='over-threshold', node_name='HKG_POP3_PEERING_ASR',
                     interface='Bundle-Ether45_class-default_egress_Droped',
                     interface_description='HHT9403CRT16.CKV.CRS16.02_BE303',
                     current='200.00%',
                     group_name='IPBN_FOIP_KV3_DROP_POP',
                     date_time='2019-10-03T16:20:02+0700',
                     link='http://10.73.224.68/?target=gplot&id%5B%5D=2758433&f=2019-10-02+17:28:02&t=2019-10-03+17:28:02',
                     node_ip='125.235.255.246')
    #NocproAlm[24425]: |HHT9602CRT22.PECD.ASR22.03|10.73.204.78|HundredGigE0/7/1/1 - 3072|To_SW_FNA_SGN5-5_Port10|FOIP_KV3_PECD|10|44.9722%||FOIP_KV3_PECD - HHT9602CRT22.PECD.ASR22.03 (10.73.204.78) - HundredGigE0/7/1/1 - 3072 (To_SW_FNA_SGN5-5_Port10) 2194508 Packet error is 3409 packets/s over 0.0033 packets/s 17:12:11 03/10|http://10.73.224.68/?target=gplot&id%5B%5D=2194508&f=2019-10-03+15:12:11&t=2019-10-03+19:12:11
    _bot = telebot.AsyncTeleBot(BOT_ID)
    _dev = DeviceShowInterfaceCrc(is_stop=False,
                                  time_ins='2019-10-03T16:20:02+0700',
                                  ip='125.235.255.246',
                                  intf='Bundle-Ether45',
                                  link='',
                                  caption='')

    _dev.run()


