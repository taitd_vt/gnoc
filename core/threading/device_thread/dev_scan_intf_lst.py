__author__ = 'vtn-dhml-dhip10'
import threading

from core.log.logger.logger import Logging
from config import Development

config = Development()
SERVER_PARSING = config.__getattribute__('SERVER_PARSING')
SERVER_PARSING_USER = config.__getattribute__('SERVER_PARSING_USER')
SERVER_PARSING_PASS = config.__getattribute__('SERVER_PARSING_PASS')
import json
from core.parsing.get_intf_lst import GetInterfaceList
from core.database.impl.elasticsearch.tbl_intf_lst_impl import TblInterfaceListImpl
from core.helpers.date_helpers import convert_date_to_elastic, get_time_format_now
from core.helpers.list_helpers import chunkIt, get_pop_lst
from core.helpers.fix_helper import get_node_place


class DeviceScanInterfaceListThread(threading.Thread):
    def __init__(self, is_stop, time_ins, dev_lst, username, passwd):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.time_ins = time_ins
        self.thread_name = 'ScanInterfaceEmptyList'
        self.dev_lst = dev_lst
        self.username = username
        self.password = passwd

    def run(self):

        while not self.is_stop:
            self.is_stop = False
            log_scan = Logging('Scan Interface in List', 'Thread get interface of Device')

            try:
                time_now = get_time_format_now()
                time_es = convert_date_to_elastic(time_now)
                hour_now = str(time_now.hour)
                minute_now = str(time_now.minute)
                second_now = str(time_now.second)
                year_now = str(time_now.year)
                month_now = str(time_now.month)
                day_now = str(time_now.day)
                date_format = '{0}_{1}_{2}_{3}_{4}_{5}'.format(year_now, month_now, day_now, hour_now, minute_now,
                                                               second_now)
                name_log = 'log_scan_backhaul_' + date_format + '.txt'
                log_scan = Logging(name_log, 'Thread get interface of Device')
                log_scan.create_log('info', 'Start getting interface of Device')
                intf_lst = list()
                for x in self.dev_lst:
                    get_intf_obj = GetInterfaceList(x.hostname, x.ip, x.vendor, self.username, self.password)
                    intf_x_lst = get_intf_obj.get_intf_lst()
                    if intf_x_lst:
                        intf_lst = intf_lst + intf_x_lst

                # insert
                if intf_lst:
                    for x in intf_lst:
                        if 'node_begin' in x:
                            node_begn = x['node_begin']

                            if 'interface_name' in x:
                                intf_name = x['interface_name']
                                node_ip = x['ip']
                                if 'description' in x:
                                    intf_des = x['description']
                                    node_plce = get_node_place(node_begn, intf_des)
                                    intf_stat = x['interface_status']
                                    intf_mode = x['interface_mode']
                                    intf_speed = x['speed']
                                    _tbl_intf_stat_impl = TblInterfaceListImpl(node_begn, intf_name, node_ip,
                                                                               intf_des, intf_stat, time_es,
                                                                               intf_mode, intf_speed, node_plce)
                                    tbl_intf_json = json.dumps(_tbl_intf_stat_impl, default=lambda o: o.__dict__)
                                    if 'interface_id' in x:
                                        intf_id = x['interface_id']
                                        res = es.create_document('tbl_interface_list', intf_id, "_doc", tbl_intf_json)

                self.is_stop = True

            except Exception as err:
                print(err)
                self.is_stop = True
                log_scan.create_log('critical', str(err) + 'while insert backhaul upstream')

    def stop(self):
        self.is_stop = True


if __name__ == '__main__':
    date_time_now = get_time_format_now()
    #hst_mnl_lst = ['HHT9602CRT24.IGW.ASR22.04', 'HCM_IW2_ASR9K_HHTN4', 'HKH9103CRT13.DGW.ASR12.01']
    #test = DeviceScanBackhaulUpstreamListThread(False, date_time_now, 'manual', hst_mnl_lst,'cisco')
    # test auto
    pop_lst = get_pop_lst()
    host_lst_div = chunkIt(pop_lst, 2)
    test = DeviceScanInterfaceListThread(False, date_time_now, 'auto', host_lst_div[0], 'cisco')
    test.run()
