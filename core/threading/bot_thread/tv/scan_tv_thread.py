__author__ = 'vtn-dhml-dhip10'
import threading
import time
from core.log.logger.logger import Logging
from core.helpers.date_helpers import get_time_format_now, get_date_minus, get_date_minus_format_elk, \
    convert_date_str_to_date_obj_spring, convert_date_obj_to_date_str_spring, date_diff_in_seconds, \
    get_date_sec_format_elk, convert_date_soc_to_date_obj, get_date_now
from core.helpers.stringhelpers import convert_api_vipa_television_xml_to_dct
from app.api.television.api_soc_tv_server import ApiSocTvServer
from core.database.impl.bot.bot_impl import BotImpl
import telebot
from config import Config, Development
config = Development()
BOT_ID = config.__getattribute__('BOT_ID')
BOT_GROUP_IP_CDBR = config.__getattribute__('BOT_GROUP_IP_CDBR')
BOT_GROUP_VIP_CDBR_ID = config.__getattribute__('BOT_GROUP_VIP_CDBR_ID')
BOT_GROUP_IP_CDBR_KV1 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV1')
BOT_GROUP_IP_CDBR_KV2 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV2')
BOT_GROUP_IP_CDBR_KV3 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV3')

BOT_GROUP_ID = -218379033


class ScanTvThread(threading.Thread):
    def __init__(self, is_stop, period_time, _bot):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.period_time = period_time
        self.thread_name = 'Bot Scan CBS VIP TV'
        self._bot = _bot

    def run(self):
        interval = self.period_time
        _bot = BotImpl(self._bot)
        while not self.is_stop:
            log_scan = Logging('Scan CBS overthreshold and under of FOIP', 'Thread scan log over and under')
            print("Scan TV VIP start")
            try:
                # scan auto
                time_now_obj = get_date_now()
                _test = ApiSocTvServer()
                _req = _test.request()
                _res_lst = convert_api_vipa_television_xml_to_dct(_req)
                if _res_lst:
                    for _res in _res_lst:
                        res_info = ""
                        time_diff_sec = 0
                        area = 'KV1'
                        if 'problem_start_time' in _res:
                            ## YYYY/mm/dd HH:ii:ss
                            if 'area_name' in _res:
                                area_name = _res['area_name']
                                if area_name == 'Khu vực 1':
                                    area = 'KV1'
                                elif area_name == 'Khu vực 2':
                                    area = 'KV2'
                                elif area_name == 'Khu vực 3':
                                    area = 'KV3'

                            time_prob = _res['problem_start_time']
                            time_prob_obj = convert_date_soc_to_date_obj(time_prob)
                            if time_prob_obj:
                                time_diff_sec = date_diff_in_seconds(time_now_obj, time_prob_obj)

                        if 'acc_status' in _res:
                            acc_stat = _res['acc_status']
                            if acc_stat.upper() == 'Giật vỡ hình':
                                # kiem tra xem da offline lau chua
                                if time_diff_sec < self.period_time:
                                #if time_diff_sec < 24 * 60 * 60:
                                    for key, value in _res.items():
                                        if value:
                                            res_info += str(key) + ": " + str(value) + " \n"

                                    _bot.send_mess(res_info, BOT_GROUP_VIP_CDBR_ID)
                                    time.sleep(1)
                print("Scan TV VIP finish and go sleeping")
                time.sleep(interval)

            except Exception as err:
                print(err)
                log_scan.create_log('critical', err)
                time.sleep(interval)

    def stop(self):
        self.is_stop = True


def plus_time_sec(tme, num):
    import datetime
    return tme + datetime.timedelta(seconds=num)


def convert_timedelta_to_second(time_delta):
    return time_delta.total_seconds()


if __name__ == '__main__':
    config = Development()
    BOT_ID = config.__getattribute__('BOT_ID')
    TELEBOT_BOT_TOKEN = BOT_ID
    bot = telebot.AsyncTeleBot(BOT_ID)
    _scan = ScanTvThread(False, 15 * 60, _bot=bot)
    _scan.start()
    print("Tester")

