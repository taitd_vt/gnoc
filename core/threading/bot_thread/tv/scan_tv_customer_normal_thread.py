__author__ = 'vtn-dhml-dhip10'
import threading
import time
from core.log.logger.logger import Logging
from core.helpers.date_helpers import get_time_format_now, get_date_minus, get_date_minus_format_elk, \
    convert_date_str_to_date_obj_spring, convert_date_obj_to_date_str_spring, date_diff_in_seconds, \
    get_date_sec_format_elk, convert_date_soc_to_date_obj, get_date_now
from core.helpers.stringhelpers import convert_api_vipa_television_xml_to_dct
from app.api.television.api_soc_tv_server import ApiSocTvServer
from core.database.impl.bot.bot_impl import BotImpl
import telebot
from config import Config, Development
config = Development()
BOT_ID = config.__getattribute__('BOT_ID')
BOT_GROUP_IP_CDBR = config.__getattribute__('BOT_GROUP_IP_CDBR')
BOT_GROUP_IP_CDBR_KV1 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV1')
BOT_GROUP_IP_CDBR_KV2 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV2')
BOT_GROUP_IP_CDBR_KV3 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV3')
BOT_GROUP_ID = -218379033


class ScanTvCustomerNormalThread(threading.Thread):
    def __init__(self, is_stop, period_time, period_send_again, _bot):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.period_time = period_time
        self.period_send_again = period_send_again
        self.thread_name = 'Bot Scan CBS VIP TV'
        self._bot = _bot

    def run(self):
        interval = self.period_time
        num_time_send_again = self.period_send_again / interval
        _bot = BotImpl(self._bot)
        res_iptv_err_old_lst = []
        while not self.is_stop:
            log_scan = Logging('Scan Customer Normal IPTV of FOIP', 'Thread scan customer IPTV normal')
            print("Begin scan customer normal IPTV")
            try:
                # scan auto
                time_now = get_time_format_now()
                time_now_obj = get_date_now()
                _test = ApiSocTvServer()
                _req = _test.request_custom_normal()
                _res_lst = convert_api_vipa_television_xml_to_dct(_req)
                if _res_lst:
                    # Lay thong tin neu symptom = Giat vo hinh va co > 5 account cung 1 tram thi day ra.
                    # Tao ra 1 list cac device_code co hien tuong giat vo hinh va danh sach account.
                    res_iptv_err_lst = []

                    for _res in _res_lst:
                        res_info = ""
                        time_diff_sec = 0
                        area = 'KV1'
                        if 'acc_status' in _res:
                            acc_stat = _res['acc_status']
                            if 'area_name' in _res:
                                area_name = _res['area_name']
                                if area_name == 'Khu vực 1':
                                    area = 'KV1'
                                elif area_name == 'Khu vực 2':
                                    area = 'KV2'
                                elif area_name == 'Khu vực 3':
                                    area = 'KV3'

                            if acc_stat.upper() == 'ONLINE':
                                # kiem tra xem da offline lau chua
                                #if time_diff_sec < self.period_time:
                                if 'symptom' in _res:
                                    symp = _res['symptom']
                                    if 'device_code' in _res:
                                        dev_code = _res['device_code']
                                        if 'service_acc' in _res:
                                            serv_acc = _res['service_acc']
                                            if serv_acc:
                                                if symp == 'Giật vỡ hình':
                                                    chck_exst = False
                                                    for res_iptv in res_iptv_err_lst:
                                                        if res_iptv['device_code'] == dev_code and serv_acc not in res_iptv['service_acc']:
                                                            chck_exst = True
                                                            res_iptv['count'] = res_iptv['count'] + 1
                                                            res_iptv['service_acc'].append(serv_acc)
                                                    if not chck_exst:
                                                        # them moi
                                                        res_iptv_err_lst.append(dict(device_code=dev_code,
                                                                                     count=1,
                                                                                     service_acc=[serv_acc],
                                                                                     area=area
                                                                                     )
                                                                                )
                    for res_iptv_err in res_iptv_err_lst:
                        cnt = res_iptv_err['count']
                        if cnt > 5:
                            # kiem tra xem co nam trong danh sach old khong?
                            # Neu co kiem tra xem repeat_time cua device_code > num_send_again khong ? Neu lon hon thi day lai va gan repeat_time = 1
                            # Neu khong thi day canh bao va them vao trong list old
                            dev_code = res_iptv_err['device_code']
                            area = res_iptv_err['area']
                            chck_exst = False
                            for res_iptv_err_old in res_iptv_err_old_lst:
                                dev_code_old = res_iptv_err_old['device_code']
                                rpt_time = res_iptv_err_old['repeat_time']
                                if rpt_time > num_time_send_again and dev_code_old == dev_code:
                                    # bat dau day ra canh bao:
                                    chck_exst = True
                                    res_iptv_err_old['repeat_time'] = 1
                                    res_info = ''
                                    for key, value in res_iptv_err.items():
                                        if value:
                                            if key == 'device_code':
                                                key = 'Trạm có số lượng khách hàng giật vỡ hình nhiều'
                                            elif key == 'count':
                                                key = 'Số lượng khách hàng bị ảnh hưởng'
                                            elif key == 'service_acc':
                                                key = 'Danh sách khách hàng'
                                            res_info += str(key) + ": " + str(value) + " \n"
                                    if res_iptv_err_old['area'] == 'KV1':
                                        _bot.send_mess(res_info, BOT_GROUP_IP_CDBR_KV1)
                                    elif res_iptv_err_old['area'] == 'KV2':
                                        _bot.send_mess(res_info, BOT_GROUP_IP_CDBR_KV2)
                                    elif res_iptv_err_old['area'] == 'KV3':
                                        _bot.send_mess(res_info, BOT_GROUP_IP_CDBR_KV3)
                                    else:
                                        _bot.send_mess(res_info, BOT_GROUP_IP_CDBR)

                                    time.sleep(1)

                                elif dev_code_old == dev_code:
                                    res_iptv_err_old['repeat_time'] += 1
                                    chck_exst = True
                            if not chck_exst:
                                res_iptv_err_old_lst.append(dict(device_code=dev_code,
                                                                 repeat_time=1,
                                                                 area=area))
                                res_info = ''
                                for key, value in res_iptv_err.items():
                                    if value:
                                        if key == 'device_code':
                                            key = 'Trạm có số lượng khách hàng giật vỡ hình nhiều'
                                        elif key == 'count':
                                            key = 'Số lượng khách hàng bị ảnh hưởng'
                                        elif key == 'service_acc':
                                            key = 'Danh sách khách hàng'
                                        res_info += str(key) + ": " + str(value) + " \n"

                                if res_iptv_err['area'] == 'KV1':
                                    _bot.send_mess(res_info, BOT_GROUP_IP_CDBR_KV1)
                                elif res_iptv_err['area'] == 'KV2':
                                    _bot.send_mess(res_info, BOT_GROUP_IP_CDBR_KV2)
                                elif res_iptv_err['area'] == 'KV3':
                                    _bot.send_mess(res_info, BOT_GROUP_IP_CDBR_KV3)
                                else:
                                    _bot.send_mess(res_info, BOT_GROUP_IP_CDBR)
                                time.sleep(1)

                time.sleep(interval)

            except Exception as err:
                print(err)
                log_scan.create_log('critical', err)
                time.sleep(1)

    def stop(self):
        self.is_stop = True


def plus_time_sec(tme, num):
    import datetime
    return tme + datetime.timedelta(seconds=num)


def convert_timedelta_to_second(time_delta):
    return time_delta.total_seconds()


if __name__ == '__main__':
    config = Development()
    BOT_ID = config.__getattribute__('BOT_ID')
    TELEBOT_BOT_TOKEN = BOT_ID
    bot = telebot.AsyncTeleBot(BOT_ID)
    _scan = ScanTvCustomerNormalThread(False, 1 * 60, 4*60, _bot=bot)
    _scan.start()
    print("Tester")

