__author__ = 'vtn-dhml-dhip10'
import threading
import time
import json
import cx_Oracle
from core.helpers.stringhelpers import convert_api_vipa_xml_to_dct
from core.log.logger.logger import Logging
from core.helpers.date_helpers import get_time_format_now, get_date_minus, get_date_minus_format_elk, \
    convert_date_str_to_date_obj_spring, convert_date_obj_to_date_str_spring, date_diff_in_seconds, \
    get_date_sec_format_elk
from core.database.impl.ipms.syslog_nocpro_impl import SyslogNocproIpmsImpl
from core.database.impl.bot.bot_impl import BotImpl
from core.database.impl.nocpro.tbl_gpon_olt_sub_impl import TblGponOltSubImpl
from core.helpers.fix_helper import get_area, get_area_from_acc
from app.api.vipa.parsing.api_soc_parsing import ApiSocParsing
from config import Config, Development

config = Development()
BOT_ID = config.__getattribute__('BOT_ID')
BOT_GROUP_ID = -218379033
BOT_GROUP_WHITELIST = config.__getattribute__('BOT_GROUP_WHITELIST')
BOT_GROUP_VIP_CDBR_ID = config.__getattribute__('BOT_GROUP_VIP_CDBR_ID')
BOT_GROUP_IP_CDBR_KV1 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV1')
BOT_GROUP_IP_CDBR_KV2 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV2')
BOT_GROUP_IP_CDBR_KV3 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV3')
SERVER_NOCPRO = config.__getattribute__('SERVER_NOCPRO')
SERVER_NOCPRO_PORT = config.__getattribute__('SERVER_NOCPRO_PORT')
SERVER_NOCPRO_SERVICE_NAME = config.__getattribute__('SERVER_NOCPRO_SERVICE_NAME')
SERVER_NOCPRO_USERNAME = config.__getattribute__('SERVER_NOCPRO_USERNAME')
SERVER_NOCPRO_PASSWORD = config.__getattribute__('SERVER_NOCPRO_PASSWORD')
SERVER_NOCPRO_USERNAME2 = config.__getattribute__('SERVER_NOCPRO_USERNAME2')
SERVER_NOCPRO_PASSWORD2 = config.__getattribute__('SERVER_NOCPRO_PASSWORD2')
TELEBOT_BOT_TOKEN = BOT_ID
GROUP_CHAT_ID = int(BOT_GROUP_ID)


class ScanBotNocproErrorDeviceThread(threading.Thread):
    def __init__(self, is_stop, period_time, _bot, pool):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.period_time = period_time
        self.thread_name = 'Bot Scan Error BOT NOC'
        self._bot = _bot
        self.pool = pool

    def run(self):
        interval = self.period_time
        time_now = get_time_format_now()
        _obj = TblGponOltSubImpl(self.pool)

        while not self.is_stop:
            log_scan = Logging('Scan SOC of FOIP', 'Thread scan log over and under')
            _bot = BotImpl(self._bot)
            try:
                # scan auto
                res_err_lst = _obj.get_count_device()
                if res_err_lst:
                    res_flt_lst = self.sum_acc_lst(res_err_lst)

                    chck_err = False
                    res_info_kv1 = 'Tổng số thuê bao lỗi phát sinh trong 15p vừa qua: ' + "\n"
                    res_info_kv2 = 'Tổng số thuê bao lỗi phát sinh trong 15p vừa qua: ' + "\n"
                    res_info_kv3 = 'Tổng số thuê bao lỗi phát sinh trong 15p vừa qua: ' + "\n"
                    chck_res_info_kv1 = False
                    chck_res_info_kv2 = False
                    chck_res_info_kv3 = False
                    for res_err in res_flt_lst:
                        dev_code = res_err['DEVICE_CODE']
                        err_cnt = res_err['N_ACCOUNT_AFFECT']
                        trb_err = res_err['TROUBLE_TYPE']
                        time_err = res_err['START_TIME']
                        _area = get_area(dev_code)
                        if err_cnt >= 10:
                            if _area == 'KV1':
                                res_info_kv1 += "Thiết bị " + dev_code + " có " + str(err_cnt) + " " \
                                                "FTTH lỗi do " + trb_err + " vào lúc " + str(time_err) + "\n"
                                chck_res_info_kv1 = True
                            elif _area == 'KV2':
                                res_info_kv2 += "Thiết bị " + dev_code + " có " + str(err_cnt) + " " \
                                                "FTTH lỗi do " + trb_err + " vào lúc " + str(time_err) + "\n"
                                chck_res_info_kv2 = True
                            elif _area == 'KV3':
                                res_info_kv3 += "Thiết bị " + dev_code + " có " + str(err_cnt) + " " \
                                                "FTTH lỗi do " + trb_err + " vào lúc " + str(time_err) + "\n"
                                chck_res_info_kv3 = True
                            else:
                                res_info_kv1 += "Thiết bị " + dev_code + " có " + str(err_cnt) + " " \
                                                "FTTH lỗi do " + trb_err + " vào lúc " + str(time_err) + "\n"
                                chck_res_info_kv1 = True
                            chck_err = True
                    # print bot
                    if chck_err:
                        if res_info_kv1 and chck_res_info_kv1:
                            _bot.send_mess(res_info_kv1, BOT_GROUP_IP_CDBR_KV1)
                        if res_info_kv2 and chck_res_info_kv2:
                            _bot.send_mess(res_info_kv2, BOT_GROUP_IP_CDBR_KV2)
                        if res_info_kv3 and chck_res_info_kv3:
                            _bot.send_mess(res_info_kv3, BOT_GROUP_IP_CDBR_KV3)

                time.sleep(interval)

            except Exception as err:
                print(err)
                log_scan.create_log('critical', err)
                time.sleep(1)

    def sum_acc_lst(self, acc_lst):
        # acc_lst gom cac dictionary la DEVICE_CODE, TROUBLE_TYPE, N_ACCOUNT_AFFECT, START_TIME
        res_lst = []
        for acc_dct in acc_lst:
            try:
                # giai quyet bai toan trung device_code do co nhieu port GPON
                dev_code = acc_dct.get('DEVICE_CODE')
                cus_aff = acc_dct.get('N_ACCOUNT_AFFECT')
                trb_type = acc_dct.get('TROUBLE_TYPE')
                time_err = acc_dct.get('START_TIME')
                if res_lst:
                    chck_exst = False
                    for res in res_lst:
                        res_dev_code = res.get('DEVICE_CODE')
                        res_cus_aff = res.get('N_ACCOUNT_AFFECT')
                        res_trb_type = res.get('TROUBLE_TYPE')
                        if dev_code == res_dev_code and trb_type == res_trb_type:
                            res_cus_aff += cus_aff
                            res['N_ACCOUNT_AFFECT'] = res_cus_aff
                            chck_exst = True
                    if not chck_exst:
                        # tao moi
                        res_lst.append(dict(DEVICE_CODE=dev_code, N_ACCOUNT_AFFECT=cus_aff,
                                            TROUBLE_TYPE=trb_type, START_TIME=time_err))
                else:
                    res_lst.append(dict(DEVICE_CODE=dev_code, N_ACCOUNT_AFFECT=cus_aff,
                                        TROUBLE_TYPE=trb_type, START_TIME=time_err))

            except Exception as err:
                print(err)

        return res_lst

    def stop(self):
        self.is_stop = True




if __name__ == '__main__':
    import telebot
    bot = telebot.AsyncTeleBot(BOT_ID)
    dsn_str = cx_Oracle.makedsn(SERVER_NOCPRO, SERVER_NOCPRO_PORT, service_name=SERVER_NOCPRO_SERVICE_NAME)
    pool_nocpro = cx_Oracle.SessionPool(min=4,
                                        max=20, increment=1, threaded=True, dsn=dsn_str,
                                        user=SERVER_NOCPRO_USERNAME, password=SERVER_NOCPRO_PASSWORD,
                                        encoding='UTF-8', nencoding='UTF-8')

    _obj = ScanBotNocproErrorDeviceThread(False, 15 * 60, bot, pool_nocpro)
    _obj.start()
    print(_obj)