__author__ = 'vtn-dhml-dhip10'
import threading
import time
import json
import cx_Oracle
from core.helpers.stringhelpers import convert_api_vipa_xml_to_dct
from core.log.logger.logger import Logging
from core.helpers.date_helpers import get_time_format_now, get_date_minus, get_date_minus_format_elk, \
    convert_date_str_to_date_obj_spring, convert_date_obj_to_date_str_spring, date_diff_in_seconds, \
    get_date_sec_format_elk, get_date_now
from core.database.impl.ipms.syslog_nocpro_impl import SyslogNocproIpmsImpl
from core.database.impl.nocpro.tbl_syslog_cntt_core_impl import TblSyslogCnttCoreImpl
from core.database.impl.bot.bot_impl import BotImpl
from core.database.impl.aom.tbl_aom_impl import TblAomImpl
from core.database.impl.nocpro.tbl_bss_core_impl import TblBssCoreImpl
from core.helpers.fix_helper import get_area, get_area_from_acc
from app.api.vipa.parsing.api_soc_parsing import ApiSocParsing
from config import Config, Development
from core.database.impl.nocpro.tbl_gpon_olt_sub_impl import TblGponOltSubImpl
config = Development()
BOT_ID = config.__getattribute__('BOT_ID')
BOT_GROUP_ID = -218379033

BOT_GROUP_WHITELIST = config.__getattribute__('BOT_GROUP_WHITELIST')
BOT_GROUP_CNTT_ID = config.__getattribute__('BOT_GROUP_CNTT_ID')
SERVER_NOCPRO = config.__getattribute__('SERVER_NOCPRO')
SERVER_NOCPRO_PORT = config.__getattribute__('SERVER_NOCPRO_PORT')
SERVER_NOCPRO_SERVICE_NAME = config.__getattribute__('SERVER_NOCPRO_SERVICE_NAME')
SERVER_NOCPRO_USERNAME = config.__getattribute__('SERVER_NOCPRO_USERNAME')
SERVER_NOCPRO_PASSWORD = config.__getattribute__('SERVER_NOCPRO_PASSWORD')
SERVER_AOM = config.__getattribute__('SERVER_AOM')
SERVER_AOM_PORT = config.__getattribute__('SERVER_AOM_PORT')
SERVER_AOM_SERVICE_NAME = config.__getattribute__('SERVER_AOM_SERVICE_NAME')
SERVER_AOM_USERNAME = config.__getattribute__('SERVER_AOM_USERNAME')
SERVER_AOM_PASSWORD = config.__getattribute__('SERVER_AOM_PASSWORD')
BOT_GROUP_VIP_CDBR_ID = config.__getattribute__('BOT_GROUP_VIP_CDBR_ID')
BOT_GROUP_IP_CDBR_KV1 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV1')
BOT_GROUP_IP_CDBR_KV2 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV2')
BOT_GROUP_IP_CDBR_KV3 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV3')
BOT_GROUP_BSS_ACCESS_ID = config.__getattribute__('BOT_GROUP_BSS_ACCESS_ID')
TELEBOT_BOT_TOKEN = BOT_ID
GROUP_CHAT_ID = int(BOT_GROUP_ID)


class ScanBotAomThread(threading.Thread):
    def __init__(self, is_stop, period_time, _bot, pool_aom, pool_nocpro):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.period_time = period_time
        self.thread_name = 'Bot Scan AOM'
        self._bot = _bot
        self.pool_aom = pool_aom
        self.pool_nocpro = pool_nocpro


    def run(self):
        interval = self.period_time
        # create session pool

        # danh sach tram nhay can check truoc. Neu co thay doi thi lay res_change sau 30p
        _obj_bss_impl = TblBssCoreImpl(self.pool_nocpro)

        while not self.is_stop:
            log_scan = Logging('Scan AOM of FOIP', 'Thread scan log over and under')
            _bot = BotImpl(self._bot)
            _obj = TblAomImpl(self.pool_aom)
            _obj_cntt_nocpro_impl = TblSyslogCnttCoreImpl(self.pool_nocpro)
            _obj_gpon_olt_impl = TblGponOltSubImpl(self.pool_nocpro)

            try:
                # scan auto
                print("Pool Gpon Busy:" + str(_obj_cntt_nocpro_impl.pool.busy))
                print("Pool AOM Busy: " + str(_obj.pool.busy))

                res_err_lst = _obj.get_total_cur("123")
                res_new_cntt_lst = _obj_cntt_nocpro_impl.get_alarm_new_lst()
                res_rpt_cntt_lst = _obj_cntt_nocpro_impl.get_alarm_repeat_lst()

                res_info = ''
                time_now = get_date_now()
                chck_frst = False
                if res_err_lst:

                    res_info = ''

                    for res_err in res_err_lst:
                        try:
                            chck_time = False

                            service_name = res_err['SERVICE_NAME']
                            device_ip = res_err['DEVICE_IP']
                            alarm_content = res_err['ALARM_CONTENT']
                            extra_infor = res_err['EXTRA_INFOR']
                            current_value = res_err['CURRENT_VALUE']
                            counter_critical = res_err['COUNTER_CRITICAL']
                            app_name = res_err['APP_NAME']
                            start_time = res_err['START_TIME']
                            update_time = res_err['UPDATE_TIME']
                            if start_time:
                                diff_sec = date_diff_in_seconds(time_now, start_time)
                                if 0 < diff_sec <= 1000:
                                    chck_time = True

                            if chck_time:
                                if not chck_frst:
                                    res_info += "IP " + device_ip + " có lỗi " + str(service_name) + "\n" \
                                                                                                     "Chi tiết: " + alarm_content + "\n" \
                                                                                                                                    "Vào lúc: " + str(
                                        start_time) + "\n" + "Giá trị hiện tại: " + str(current_value) + "\n" \
                                                                                                         "Số lần Critical: " + str(
                                        counter_critical) + "\nApp Name: " + app_name

                                    chck_frst = True
                                else:
                                    res_info = "IP " + device_ip + " có lỗi " + str(service_name) + "\n" \
                                                                                                    "Chi tiết: " + alarm_content + "\n" \
                                                                                                                                   "Vào lúc: " + str(
                                        start_time) + "\n" + "Giá trị hiện tại: " + str(current_value) + "\n" \
                                                                                                         "Số lần Critical: " + str(
                                        counter_critical) + "\nApp Name: " + app_name

                                _bot.send_mess(res_info, BOT_GROUP_CNTT_ID)
                                time.sleep(0.5)
                        except Exception as err:
                            print("Error %s when scan bot AOM Thread" % err)

                if res_new_cntt_lst:

                    for res_err in res_new_cntt_lst:
                        try:

                            fal_name = res_err['FAULT_NAME']
                            start_time = res_err['START_TIME']
                            cnt_name = res_err['CONTENT']

                            if start_time:
                                diff_sec = date_diff_in_seconds(time_now, start_time)
                                if 0 < diff_sec <= 1000:
                                    if not chck_frst:
                                        res_info += "Cảnh báo mới: Lỗi " + str(fal_name) + "\n" \
                                                                                           "Chi tiết: " + cnt_name + "\n" \
                                                                                                                     "Vào lúc: " + str(
                                            start_time) + "\n"

                                        chck_frst = True
                                    else:
                                        res_info = "Cảnh báo mới: Lỗi " + str(fal_name) + "\n" \
                                                                                          "Chi tiết: " + cnt_name + "\n" \
                                                                                                                    "Vào lúc: " + str(
                                            start_time) + "\n"

                                    _bot.send_mess(res_info, BOT_GROUP_CNTT_ID)
                                    time.sleep(0.5)

                        except Exception as err:
                            print("Error %s when get result new CNTT list")
                # Bo di canh bao lap do CNTT yeu cau
                '''
                if res_rpt_cntt_lst:

                    for res_err in res_rpt_cntt_lst:
                        try:

                            fal_name = res_err['FAULT_NAME']
                            start_time = res_err['START_TIME']
                            cnt_name = res_err['CONTENT']
                            alm_num = res_err['SL']

                            if start_time:
                                diff_sec = date_diff_in_seconds(time_now, start_time)
                                if 0 < diff_sec <= 1000:
                                    if not chck_frst:
                                        res_info += "Cảnh báo lặp trong 24h: Số lần lặp lại: " + str(alm_num) + "\n" \
                                                                                                                "Lỗi " + str(
                                            fal_name) + "\n" \
                                                        "Chi tiết: " + cnt_name + "\n" \
                                                                                  "Vào lúc: " + str(start_time) + "\n"

                                        chck_frst = True
                                    else:
                                        res_info = "Cảnh báo lặp trong 24h: Số lần lặp lại: " + str(alm_num) + " \n" \
                                                                                                               "Lỗi " + str(
                                            fal_name) + "\n" \
                                                        "Chi tiết: " + cnt_name + "\n" \
                                                                                  "Vào lúc: " + str(start_time) + "\n"

                                    _bot.send_mess(res_info, BOT_GROUP_CNTT_ID)
                                    time.sleep(0.5)

                        except Exception as err:
                            print("Error %s when get result repeat CNTT list")
                '''
            except Exception as err:
                print(err)
                log_scan.create_log('critical', err)
                time.sleep(1)

            try:
                # scan auto
                res_err_lst = _obj_gpon_olt_impl.get_count_device()
                if res_err_lst:
                    res_flt_lst = self.sum_acc_lst(res_err_lst)

                    chck_err = False
                    res_info_kv1 = 'Tổng số thuê bao lỗi phát sinh trong 15p vừa qua: ' + "\n"
                    res_info_kv2 = 'Tổng số thuê bao lỗi phát sinh trong 15p vừa qua: ' + "\n"
                    res_info_kv3 = 'Tổng số thuê bao lỗi phát sinh trong 15p vừa qua: ' + "\n"
                    chck_res_info_kv1 = False
                    chck_res_info_kv2 = False
                    chck_res_info_kv3 = False
                    for res_err in res_flt_lst:
                        dev_code = res_err['DEVICE_CODE']
                        err_cnt = res_err['N_ACCOUNT_AFFECT']
                        trb_err = res_err['TROUBLE_TYPE']
                        time_err = res_err['START_TIME']
                        _area = get_area(dev_code)
                        if err_cnt >= 10:
                            if _area == 'KV1':
                                res_info_kv1 += "Thiết bị " + dev_code + " có " + str(err_cnt) + " " \
                                                                                                 "FTTH lỗi do " + trb_err + " vào lúc " + str(
                                    time_err) + "\n"
                                chck_res_info_kv1 = True
                            elif _area == 'KV2':
                                res_info_kv2 += "Thiết bị " + dev_code + " có " + str(err_cnt) + " " \
                                                                                                 "FTTH lỗi do " + trb_err + " vào lúc " + str(
                                    time_err) + "\n"
                                chck_res_info_kv2 = True
                            elif _area == 'KV3':
                                res_info_kv3 += "Thiết bị " + dev_code + " có " + str(err_cnt) + " " \
                                                                                                 "FTTH lỗi do " + trb_err + " vào lúc " + str(
                                    time_err) + "\n"
                                chck_res_info_kv3 = True
                            else:
                                res_info_kv1 += "Thiết bị " + dev_code + " có " + str(err_cnt) + " " \
                                                                                                 "FTTH lỗi do " + trb_err + " vào lúc " + str(
                                    time_err) + "\n"
                                chck_res_info_kv1 = True
                            chck_err = True
                    # print bot
                    if chck_err:
                        if res_info_kv1 and chck_res_info_kv1:
                            _bot.send_mess(res_info_kv1, BOT_GROUP_IP_CDBR_KV1)
                        if res_info_kv2 and chck_res_info_kv2:
                            _bot.send_mess(res_info_kv2, BOT_GROUP_IP_CDBR_KV2)
                        if res_info_kv3 and chck_res_info_kv3:
                            _bot.send_mess(res_info_kv3, BOT_GROUP_IP_CDBR_KV3)

            except Exception as err:
                print(err)
                log_scan.create_log('critical', err)
                time.sleep(1)

            time.sleep(interval)

            print("Pool Gpon Busy:" + str(_obj_cntt_nocpro_impl.pool.busy))
            print("Pool AOM Busy: " + str(_obj.pool.busy))

    def sum_acc_lst(self, acc_lst):
        # acc_lst gom cac dictionary la DEVICE_CODE, TROUBLE_TYPE, N_ACCOUNT_AFFECT, START_TIME
        res_lst = []
        for acc_dct in acc_lst:
            try:
                # giai quyet bai toan trung device_code do co nhieu port GPON
                dev_code = acc_dct.get('DEVICE_CODE')
                cus_aff = acc_dct.get('N_ACCOUNT_AFFECT')
                trb_type = acc_dct.get('TROUBLE_TYPE')
                time_err = acc_dct.get('START_TIME')
                if res_lst:
                    chck_exst = False
                    for res in res_lst:
                        res_dev_code = res.get('DEVICE_CODE')
                        res_cus_aff = res.get('N_ACCOUNT_AFFECT')
                        res_trb_type = res.get('TROUBLE_TYPE')
                        if dev_code == res_dev_code and trb_type == res_trb_type:
                            res_cus_aff += cus_aff
                            res['N_ACCOUNT_AFFECT'] = res_cus_aff
                            chck_exst = True
                    if not chck_exst:
                        # tao moi
                        res_lst.append(dict(DEVICE_CODE=dev_code, N_ACCOUNT_AFFECT=cus_aff,
                                            TROUBLE_TYPE=trb_type, START_TIME=time_err))
                else:
                    res_lst.append(dict(DEVICE_CODE=dev_code, N_ACCOUNT_AFFECT=cus_aff,
                                        TROUBLE_TYPE=trb_type, START_TIME=time_err))

            except Exception as err:
                print(err)

        return res_lst

    def updt_res_lst(self, res_old_lst, res_new_lst, key_wrd, key_val_count):
        res_chge_lst = []
        key_val_count = key_val_count.upper()
        key_wrd = key_wrd.upper()
        try:

            if not res_old_lst:
                return res_new_lst, res_new_lst
            else:
                # kiem tra res_old va cap nhat
                for res_new in res_new_lst:
                    # Kiem tra neu co ton tai trong list cu thi cap nhat value, neu khong ton tai thi them moi
                    for key, val in res_new.items():
                        if key.upper() == key_wrd.upper():
                            val_new = val
                            if key_val_count in res_new:
                                val_new_count = res_new[key_val_count]
                                chck_key = True
                                chck_updt = False
                                for res_old in res_old_lst:
                                    for key_old, val_old in res_old.items():
                                        if key_old.upper() == key_wrd.upper():
                                            if val_new == val_old:
                                                # cung 1 node
                                                if key_val_count in res_old:
                                                    val_old_count = res_old[key_val_count]
                                                    if val_old_count != val_new_count:
                                                        res_new['CHANGE'] = val_new_count - val_old_count
                                                        res_chge_lst.append(res_new)
                                                        res_old[key_val_count] = val_new_count
                                                    chck_updt = True
                                                    break
                                    if chck_updt:
                                        break

                                if not chck_updt:
                                    # them moi
                                    res_old_lst.append(res_new)
                                    res_chge_lst.append(res_new)
                                    break
        except Exception as err:
            print("Error %s when check duplicate of list" % err)

        return res_old_lst, res_chge_lst


    def stop(self):
        self.is_stop = True


def plus_time_sec(tme, num):
    import datetime
    return tme + datetime.timedelta(seconds=num)


def convert_timedelta_to_second(time_delta):
    return time_delta.total_seconds()


if __name__ == '__main__':
    import telebot

    dsn_aom_str = cx_Oracle.makedsn(SERVER_AOM, SERVER_AOM_PORT, service_name=SERVER_AOM_SERVICE_NAME)
    pool_aom = cx_Oracle.SessionPool(min=1,
                                     max=10, increment=1, threaded=True, dsn=dsn_aom_str,
                                     user=SERVER_AOM_USERNAME, password=SERVER_AOM_PASSWORD,
                                     encoding='UTF-8', nencoding='UTF-8')
    dsn_str = cx_Oracle.makedsn(SERVER_NOCPRO, SERVER_NOCPRO_PORT, service_name=SERVER_NOCPRO_SERVICE_NAME)
    pool_nocpro = cx_Oracle.SessionPool(min=4,
                                        max=20, increment=1, threaded=True, dsn=dsn_str,
                                        user=SERVER_NOCPRO_USERNAME, password=SERVER_NOCPRO_PASSWORD,
                                        encoding='UTF-8', nencoding='UTF-8')

    bot = telebot.AsyncTeleBot(BOT_ID)
    _obj = ScanBotAomThread(False, 1 * 60, bot, pool_aom=pool_aom,
                            pool_nocpro=pool_nocpro)
    _obj.start()
    print('112431243')
