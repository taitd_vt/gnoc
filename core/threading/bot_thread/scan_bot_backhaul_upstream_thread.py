__author__ = 'vtn-dhml-dhip10'

import threading
import time
from core.log.logger.logger import Logging
from app.api.elasticsearch.api_kibana import ApiKibana
from app.api.ipms.api_ipms import ApiIpms
from core.database.impl.server_minhnd.server_mysql_connect import ServerMysqlConnect
from core.helpers.date_helpers import get_time_format_now, get_date_minus, get_date_minus_format_elk, \
    convert_date_str_to_date_obj_spring, get_date_now, convert_date_to_epoch, get_date_yesterday_format_ipms, \
    get_only_date_now_format_ipms, get_date_now_format_ipms, convert_date_obj_to_date_str_ipms
from core.database.impl.server_minhnd.tbl_interface_drop_impl import TblInterfaceDropImpl
from core.database.impl.bot.bot_impl import BotImpl
from config import Config, Development

config = Development()
BOT_ID = config.__getattribute__('BOT_ID')
GROUP_ID_KPI_REPORT_LST = config.__getattribute__('GROUP_ID_KPI_REPORT_LST')
BOT_GROUP_INTERNET_VN = config.__getattribute__('BOT_GROUP_INTERNET_VN')
BOT_GROUP_ID = -218379033
TELEBOT_BOT_TOKEN = BOT_ID
GROUP_CHAT_ID = int(BOT_GROUP_ID)


class ScanBotBackhaulUpstreamThread(threading.Thread):
    def __init__(self, is_stop, period_time, _bot, mysql_pool):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.period_time = period_time
        self.thread_name = 'Bot Scan Backhaul Upstream'
        self._bot = _bot
        self.mysql_pool = mysql_pool

    @staticmethod
    def scan_intf_peak(intf_area, intf_type, intf_grp_id, intf_grp_name, thresh_1, thresh_2, thresh_3):
        _api_impl = ApiIpms(intf_area, intf_type)
        if intf_grp_name.upper().find('CPU') >= 0 or intf_grp_name.upper().find('MEM') >= 0 or \
                intf_grp_name.upper().find('FAN') >= 0:
            res_peak_lst = _api_impl.get_cust_peak_from_group_lst(intf_grp_id, 0)
        else:
            res_peak_lst = _api_impl.get_intf_peak_from_group_lst(intf_grp_id, 0)
        # liet ke ra ket qua co bao nhieu link vuot nguong 1, vuot nguong 2, vuot nguong 3 / tong so link
        # nguong 1 > nguong 2 > nguong 3

        totl_link = 0
        totl_thresh_1 = 0
        totl_thresh_2 = 0
        totl_thresh_3 = 0
        res_link_lst = []

        if res_peak_lst:
            for intf_peak in res_peak_lst:
                # sau khi da co danh sach interface peak
                try:
                    if 'hostname' in intf_peak:
                        traffic_in = round(intf_peak['maxi_60'] / 1000000000, 2)
                        traffic_out = round(intf_peak['maxo_60'] / 1000000000, 2)
                        speed = round(float(intf_peak['speed']) / 1000000000, 2)
                        if speed == 0:
                            speed = 0.01
                        perf = max(traffic_in, traffic_out) * 100 / speed
                        totl_link += 1
                        if perf >= thresh_1:
                            totl_thresh_1 += 1
                            res_link_lst.append(intf_peak)
                        elif perf >= thresh_2:
                            totl_thresh_2 += 1
                            res_link_lst.append(intf_peak)
                        elif perf >= thresh_3:
                            totl_thresh_3 += 1
                            res_link_lst.append(intf_peak)

                except Exception as err:
                    print("Error %s when get Interface Peak of Group Id SiteRouter Access List" % err)
        return dict(total=totl_link, thresh_1=totl_thresh_1, thresh_2=totl_thresh_2, thresh_3=totl_thresh_3, detail=res_link_lst)

    def run(self):
        interval = self.period_time
        # create bao cao theo mau:
        '''
        - Lưu lượng thời điểm 21h45: 4045/4460 (~ TU: 90.6%) OK
        - Tổng số link Backhau: 232 Link OK
            + Số link vượt KPI từ 93% - 98% là 20 Link OK 
            + Số link vượt KPI >98% là 62 Link OK 
        - Tổng số link upstream: 214 link OK 
            + Số link vượt KPI từ 93% - 98% là 33 Link
            + Số link vượt KPI >98% là 50 Link OK 
        - Tổng số link upstream transit: 43 link 
            + Số link vượt KPI từ 93% - 98% là 14 Link
            + Số link vượt KPI >98% là 23 Link
        - Lưu lượng drop: 18.9 GB
        '''

        while not self.is_stop:
            _now = get_date_now()
            _now_ipms = get_date_now_format_ipms()
            _now_only_date = get_only_date_now_format_ipms()
            _obj_impl = TblInterfaceDropImpl(_now_only_date)
            log_scan = Logging('Scan Backhaul Upstream of FOIP', 'Thread scan log over and under')
            _bot = BotImpl(self._bot)

            try:
                # scan auto
                res_info = '<b>Lưu lượng thời điểm ' + str(_now_ipms) + "  :  </b> "
                res_info_dom = '<b>Lưu lượng trong nước thời điểm ' + str(_now_ipms) + "  :   </b>\n"
                time_now = get_date_now()
                chck_frst = False
                totl_bchl_lst = []
                # Lay Capacity va Utilization
                _now_end = get_date_minus(_now, 0)
                # _now_bgn = get_date_minus(_now, - 7 * 60 + 60)
                _now_bgn = get_date_minus(_now, 30)
                _now_end_epch = int(convert_date_to_epoch(_now_end) * 1000)
                _now_bgn_epch = int(convert_date_to_epoch(_now_bgn) * 1000)
                _api = ApiKibana(_now_bgn_epch, _now_end_epch, "tbl_interface_status")
                max_cpct, max_util = _api.get_max_cpct_util()
                try:
                    if max_cpct > 0:
                        max_rate = round(max_util * 100 / max_cpct)
                        res_info += str(round(max_util, 2)) + "/" + str(round(max_cpct, 2)) + " Gbps (~ TU: " + str(
                            max_rate) + " %) \n"
                        # chinh lai time cho do bi trung
                        chck_dup = False
                        time_plus = 0
                        while not chck_dup:
                            # _now_bgn = get_date_minus(_now, - 7 * 60 + 60)
                            _now_bgn = get_date_minus(_now, 10 + time_plus)
                            _now_bgn_epch = int(convert_date_to_epoch(_now_bgn) * 1000)
                            _api.time_epch_bgn = _now_bgn_epch
                            totl_bchl_lst = _api.get_total_backhaul_link()
                            if totl_bchl_lst:
                                chck_dup = True
                                break
                            else:
                                time_plus += 5
                            if time_plus >= 30:
                                chck_dup = True
                        # Lay tong so link backhaul

                        totl_bchl_lnk = 0
                        totl_upst_lnk = 0
                        for x in totl_bchl_lst:
                            if 'key' in x:
                                netw_type_x = x['key']
                                if netw_type_x.upper() == 'IPBN_BACKHAUL':
                                    if 'doc_count' in x:
                                        totl_bchl_lnk = x['doc_count']
                                elif netw_type_x.upper() == 'IPBN_UPSTREAM':
                                    if 'doc_count' in x:
                                        totl_upst_lnk = x['doc_count']
                        # Lay so link vuot KPI
                        totl_bchl_upstream_over_lst = _api.get_total_congestion_backhaul_upstream()
                        num_98 = 0
                        num_95 = 0
                        num_93 = 0
                        for x in totl_bchl_upstream_over_lst:
                            if 'network_type' in x:
                                netw_type = x['network_type']
                                if netw_type.upper().find('BACKHAUL') >= 0:
                                    res_info += "<b>- Tổng số link Backhaul:" + str(totl_bchl_lnk) + "</b>\n"
                                    if 'performance_98' in x:
                                        num_98 = x['performance_98']
                                    if 'performance_95_98' in x:
                                        num_95 = x['performance_95_98']
                                    if 'performance_93' in x:
                                        num_93 = x['performance_93']
                                    res_info += "+ Số link Backhaul vượt KPI từ 93% - 98% là: " + str(
                                        num_93 + num_95) + "\n"
                                    res_info += "+ Số link Backhaul vượt KPI > 98% là: " + str(num_98) + "\n"
                                else:
                                    res_info += "- <b>Tổng số link Upstream:" + str(totl_upst_lnk) + "</b>\n"
                                    if 'performance_98' in x:
                                        num_98 = x['performance_98']
                                    if 'performance_95_98' in x:
                                        num_95 = x['performance_95_98']
                                    if 'performance_93' in x:
                                        num_93 = x['performance_93']
                                    res_info += "+ Số link Upstream vượt KPI từ 93% - 98% là: " + str(
                                        num_93 + num_95) + "\n"
                                    res_info += "+ Số link Upstream vượt KPI > 98% là: " + str(num_98) + "\n"

                        # them tong so link upstream transit
                        num_98 = 0
                        num_95 = 0
                        num_93 = 0

                        totl_trans_lst = _api.get_total_upstream_transit_link()
                        totl_trans_lnk = 0
                        for x in totl_trans_lst:
                            if 'doc_count' in x:
                                totl_trans_lnk = x['doc_count']
                        res_info += "<b>- Tổng số link upstream transit: " + str(totl_trans_lnk) + "</b>\n"
                        totl_cngs_trans_lst = _api.get_total_congestion_upstream_transit()

                        for x in totl_cngs_trans_lst:
                            if 'network_type' in x:
                                netw_type = x['network_type']
                                if netw_type.upper().find('UPSTREAM') >= 0:
                                    if 'performance_98' in x:
                                        num_98 = x['performance_98']
                                    if 'performance_95_98' in x:
                                        num_95 = x['performance_95_98']
                                    if 'performance_93' in x:
                                        num_93 = x['performance_93']
                                    res_info += "+ Số link Transit vượt KPI từ 93% - 98% là: " + str(
                                        num_93 + num_95) + "\n"
                                    res_info += "+ Số link Transit vượt KPI > 98% là: " + str(num_98) + "\n"

                        # tinh so luong drop
                        '''
                        res = self.mysql_pool.execute(_obj_impl.get_drop_now())
                        if res:
                            time_drop = res[0]['time_frm']
                            ttl_drop = round(res[0]['total'] * 8 / 1000000, 2)
                            res_info += "- Lưu lượng drop:" + str(ttl_drop) + " Mbps on " + str(time_drop) + "\n"
                        '''
                        res = self.mysql_pool.execute(_obj_impl.get_max_time())
                        if res:
                            for key, val in res[0].items():
                                time_drop = val
                                time_drop_str = convert_date_obj_to_date_str_ipms(time_drop)
                                pos_dbl_dot = str(time_drop_str).find(":")
                                if pos_dbl_dot >= 0:
                                    time_drop_min = time_drop_str[:pos_dbl_dot + 3]
                                    if time_drop_min:
                                        res_drop_lst = self.mysql_pool.execute(
                                            _obj_impl.get_total_drop_on_time(time_drop_min))
                                        if res_drop_lst:
                                            for key1, val1 in res_drop_lst[0].items():
                                                if val1:
                                                    ttl_drop = round(val1 * 8 / 1000000, 2)
                                                    res_info += "Total Drop: " + str(ttl_drop) + " Mbps on " + str(
                                                        time_drop) + "\n"
                                                    break
                    else:
                        res_info = "Error with API Kibana. Capacity = 0"
                        _bot.send_mess(res_info, BOT_GROUP_INTERNET_VN)
                except Exception as err:
                    _bot.send_mess(str(err), BOT_GROUP_INTERNET_VN)
                    print(str(err))
                '''
                Them phan: co bao nhieu link CT,PE, CKV vuot KPI
                            co bao nhieu link DGW trong nuoc vuot KPI
                            co bao nhieu link trong CPU MEM nghen trong IPBN, MPBN, PS
                '''
                res_kpi_cpu_dct = dict()
                res_kpi_mem_dct = dict()
                res_kpi_dom_dct = dict()
                res_kpi_prt_dct = dict()
                res_kpi_pe_p_mpbn_dct = dict()

                res_kpi_cpu_fw_dct = dict()
                res_kpi_mem_fw_dct = dict()
                res_kpi_ses_fw_dct = dict()
                res_kpi_bb_inter_dct = dict()
                res_kpi_bb_inner_dct = dict()

                for x in GROUP_ID_KPI_REPORT_LST:
                    # tuy loai phan nhom ma tinh theo nguong khac nhau
                    grp_name = x['group_name']
                    if grp_name.upper().find('CORE_CPU') >= 0:
                        res_tmp_cpu_dct = self.scan_intf_peak(x['area'], x['type'], x['group_id'], x['group_name'],
                                                              thresh_1=70, thresh_2=65, thresh_3=60)
                        if res_kpi_cpu_dct:
                            # dict(total=totl_link, thresh_1=totl_thresh_1, thresh_2=totl_thresh_2, thresh_3=totl_thresh_3)
                            res_kpi_cpu_dct['total'] = res_tmp_cpu_dct['total'] + res_kpi_cpu_dct['total']
                            res_kpi_cpu_dct['thresh_1'] = res_tmp_cpu_dct['thresh_1'] + res_kpi_cpu_dct['thresh_1']
                            res_kpi_cpu_dct['thresh_2'] = res_tmp_cpu_dct['thresh_2'] + res_kpi_cpu_dct['thresh_2']
                            res_kpi_cpu_dct['thresh_3'] = res_tmp_cpu_dct['thresh_3'] + res_kpi_cpu_dct['thresh_3']
                            res_kpi_cpu_dct['detail'] = res_tmp_cpu_dct['detail'] + res_kpi_cpu_dct['detail']
                        else:
                            res_kpi_cpu_dct = res_tmp_cpu_dct
                    elif grp_name.upper().find('VASCP_CPU') >= 0:
                        res_tmp_kpi_cpu_fw_dct = self.scan_intf_peak(x['area'], x['type'], x['group_id'], x['group_name'],
                                                              thresh_1=70, thresh_2=65, thresh_3=60)
                        if res_kpi_cpu_fw_dct:
                            # dict(total=totl_link, thresh_1=totl_thresh_1, thresh_2=totl_thresh_2, thresh_3=totl_thresh_3)
                            res_kpi_cpu_fw_dct['total'] = res_kpi_cpu_fw_dct['total'] + res_tmp_kpi_cpu_fw_dct['total']
                            res_kpi_cpu_fw_dct['thresh_1'] = res_kpi_cpu_fw_dct['thresh_1'] + res_tmp_kpi_cpu_fw_dct['thresh_1']
                            res_kpi_cpu_fw_dct['thresh_2'] = res_kpi_cpu_fw_dct['thresh_2'] + res_tmp_kpi_cpu_fw_dct['thresh_2']
                            res_kpi_cpu_fw_dct['thresh_3'] = res_kpi_cpu_fw_dct['thresh_3'] + res_tmp_kpi_cpu_fw_dct['thresh_3']
                            res_kpi_cpu_fw_dct['detail'] = res_kpi_cpu_fw_dct['detail'] + res_tmp_kpi_cpu_fw_dct['detail']
                        else:
                            res_kpi_cpu_fw_dct = res_tmp_kpi_cpu_fw_dct

                    elif grp_name.upper().find('CORE_MEM') >= 0:
                        res_tmp_mem_dct = self.scan_intf_peak(x['area'], x['type'], x['group_id'], x['group_name'],
                                                              thresh_1=95, thresh_2=90, thresh_3=85)
                        if res_kpi_mem_dct:
                            # dict(total=totl_link, thresh_1=totl_thresh_1, thresh_2=totl_thresh_2, thresh_3=totl_thresh_3)
                            res_kpi_mem_dct['total'] = res_kpi_mem_dct['total'] + res_tmp_mem_dct['total']
                            res_kpi_mem_dct['thresh_1'] = res_kpi_mem_dct['thresh_1'] + res_tmp_mem_dct['thresh_1']
                            res_kpi_mem_dct['thresh_2'] = res_kpi_mem_dct['thresh_2'] + res_tmp_mem_dct['thresh_2']
                            res_kpi_mem_dct['thresh_3'] = res_kpi_mem_dct['thresh_3'] + res_tmp_mem_dct['thresh_3']
                            res_kpi_mem_dct['detail'] = res_kpi_mem_dct['detail'] + res_tmp_mem_dct['detail']
                        else:
                            res_kpi_mem_dct = res_tmp_mem_dct
                    elif grp_name.upper().find('VASCP_MEM') >= 0:
                        res_tmp_mem_fw_dct = self.scan_intf_peak(x['area'], x['type'], x['group_id'], x['group_name'],
                                                              thresh_1=95, thresh_2=90, thresh_3=70)
                        if res_kpi_mem_fw_dct:
                            # dict(total=totl_link, thresh_1=totl_thresh_1, thresh_2=totl_thresh_2, thresh_3=totl_thresh_3)
                            res_kpi_mem_fw_dct['total'] = res_kpi_mem_fw_dct['total'] + res_tmp_mem_fw_dct['total']
                            res_kpi_mem_fw_dct['thresh_1'] = res_kpi_mem_fw_dct['thresh_1'] + res_tmp_mem_fw_dct['thresh_1']
                            res_kpi_mem_fw_dct['thresh_2'] = res_kpi_mem_fw_dct['thresh_2'] + res_tmp_mem_fw_dct['thresh_2']
                            res_kpi_mem_fw_dct['thresh_3'] = res_kpi_mem_fw_dct['thresh_3'] + res_tmp_mem_fw_dct['thresh_3']
                            res_kpi_mem_fw_dct['detail'] = res_kpi_mem_fw_dct['detail'] + res_tmp_mem_fw_dct['detail']
                        else:
                            res_kpi_mem_fw_dct = res_tmp_mem_fw_dct
                    elif grp_name.upper().find('SESSION') >= 0:
                        res_tmp_ses_fw_dct = self.scan_intf_peak(x['area'], x['type'], x['group_id'], x['group_name'],
                                                              thresh_1=95, thresh_2=90, thresh_3=80)
                        if res_kpi_ses_fw_dct:
                            # dict(total=totl_link, thresh_1=totl_thresh_1, thresh_2=totl_thresh_2, thresh_3=totl_thresh_3)
                            res_kpi_ses_fw_dct['total'] = res_kpi_ses_fw_dct['total'] + res_tmp_ses_fw_dct['total']
                            res_kpi_ses_fw_dct['thresh_1'] = res_kpi_ses_fw_dct['thresh_1'] + res_tmp_ses_fw_dct['thresh_1']
                            res_kpi_ses_fw_dct['thresh_2'] = res_kpi_ses_fw_dct['thresh_2'] + res_tmp_ses_fw_dct['thresh_2']
                            res_kpi_ses_fw_dct['thresh_3'] = res_kpi_ses_fw_dct['thresh_3'] + res_tmp_ses_fw_dct['thresh_3']
                            res_kpi_ses_fw_dct['detail'] = res_kpi_ses_fw_dct['detail'] + res_tmp_ses_fw_dct['detail']
                        else:
                            res_kpi_ses_fw_dct = res_tmp_ses_fw_dct
                    elif grp_name.upper().find('KPI_PE_P1') >= 0: # PE P mang MPBN
                        res_tmp_kpi_pe_p_mpbn_dct = self.scan_intf_peak(x['area'], x['type'], x['group_id'], x['group_name'],
                                                              thresh_1=10000, thresh_2=9999, thresh_3=60) # chi lay lon hon 60
                        if res_kpi_pe_p_mpbn_dct:
                            # dict(total=totl_link, thresh_1=totl_thresh_1, thresh_2=totl_thresh_2, thresh_3=totl_thresh_3)
                            try:
                                res_kpi_pe_p_mpbn_dct['total'] = res_kpi_pe_p_mpbn_dct['total'] + res_tmp_kpi_pe_p_mpbn_dct['total']
                                res_kpi_pe_p_mpbn_dct['thresh_1'] = res_kpi_pe_p_mpbn_dct['thresh_1'] + res_tmp_kpi_pe_p_mpbn_dct['thresh_1']
                                res_kpi_pe_p_mpbn_dct['thresh_2'] = res_kpi_pe_p_mpbn_dct['thresh_2'] + res_tmp_kpi_pe_p_mpbn_dct['thresh_2']
                                res_kpi_pe_p_mpbn_dct['thresh_3'] = res_kpi_pe_p_mpbn_dct['thresh_3'] + res_tmp_kpi_pe_p_mpbn_dct['thresh_3']
                                res_kpi_pe_p_mpbn_dct['detail'] = res_kpi_pe_p_mpbn_dct['detail'] + res_tmp_kpi_pe_p_mpbn_dct[
                                    'detail']
                            except Exception as err:
                                print(err)
                        else:
                            res_kpi_pe_p_mpbn_dct = res_tmp_kpi_pe_p_mpbn_dct
                    elif grp_name.upper().find('BB_NOIKV') >= 0: # Backbone noi khu vuc
                        res_tmp_kpi_bb_inner_dct = self.scan_intf_peak(x['area'], x['type'], x['group_id'], x['group_name'],
                                                              thresh_1=95, thresh_2=90, thresh_3=85)
                        if res_kpi_bb_inner_dct:
                            # dict(total=totl_link, thresh_1=totl_thresh_1, thresh_2=totl_thresh_2, thresh_3=totl_thresh_3)
                            try:
                                res_kpi_bb_inner_dct['total'] = res_kpi_bb_inner_dct['total'] + res_tmp_kpi_bb_inner_dct['total']
                                res_kpi_bb_inner_dct['thresh_1'] = res_kpi_bb_inner_dct['thresh_1'] + res_tmp_kpi_bb_inner_dct['thresh_1']
                                res_kpi_bb_inner_dct['thresh_2'] = res_kpi_bb_inner_dct['thresh_2'] + res_tmp_kpi_bb_inner_dct['thresh_2']
                                res_kpi_bb_inner_dct['thresh_3'] = res_kpi_bb_inner_dct['thresh_3'] + res_tmp_kpi_bb_inner_dct['thresh_3']
                                res_kpi_bb_inner_dct['detail'] = res_kpi_bb_inner_dct['detail'] + \
                                                                   res_tmp_kpi_bb_inner_dct['detail']
                            except Exception as err:
                                print(err)
                        else:
                            res_kpi_bb_inner_dct = res_tmp_kpi_bb_inner_dct
                    elif grp_name.upper().find('BACKBONE') >= 0: #Backbone lien khu vuc
                        res_tmp_kpi_bb_inter_dct = self.scan_intf_peak(x['area'], x['type'], x['group_id'], x['group_name'],
                                                              thresh_1=95, thresh_2=90, thresh_3=85)
                        if res_kpi_bb_inter_dct:
                            # dict(total=totl_link, thresh_1=totl_thresh_1, thresh_2=totl_thresh_2, thresh_3=totl_thresh_3)
                            try:
                                res_kpi_bb_inter_dct['total'] = res_kpi_bb_inter_dct['total'] + res_tmp_kpi_bb_inter_dct['total']
                                res_kpi_bb_inter_dct['thresh_1'] = res_kpi_bb_inter_dct['thresh_1'] + res_tmp_kpi_bb_inter_dct['thresh_1']
                                res_kpi_bb_inter_dct['thresh_2'] = res_kpi_bb_inter_dct['thresh_2'] + res_tmp_kpi_bb_inter_dct['thresh_2']
                                res_kpi_bb_inter_dct['thresh_3'] = res_kpi_bb_inter_dct['thresh_3'] + res_tmp_kpi_bb_inter_dct['thresh_3']
                                res_kpi_bb_inter_dct['detail'] = res_kpi_bb_inter_dct['detail'] + \
                                                                   res_tmp_kpi_bb_inter_dct['detail']
                            except Exception as err:
                                print(err)
                        else:
                            res_kpi_bb_inter_dct = res_tmp_kpi_bb_inter_dct
                    elif grp_name.upper().find('DOMESTIC') >= 0:
                        res_tmp_dom_dct = self.scan_intf_peak(x['area'], x['type'], x['group_id'], x['group_name'],
                                                              thresh_1=100000, thresh_2=98, thresh_3=93)
                        if res_kpi_dom_dct:
                            # dict(total=totl_link, thresh_1=totl_thresh_1, thresh_2=totl_thresh_2, thresh_3=totl_thresh_3)
                            res_kpi_dom_dct['total'] = res_kpi_dom_dct['total'] + res_tmp_dom_dct['total']
                            res_kpi_dom_dct['thresh_1'] = res_kpi_dom_dct['thresh_1'] + res_tmp_dom_dct['thresh_1']
                            res_kpi_dom_dct['thresh_2'] = res_kpi_dom_dct['thresh_2'] + res_tmp_dom_dct['thresh_2']
                            res_kpi_dom_dct['thresh_3'] = res_kpi_dom_dct['thresh_3'] + res_tmp_dom_dct['thresh_3']
                            res_kpi_dom_dct['detail'] = res_kpi_dom_dct['detail'] + res_tmp_dom_dct['detail']
                        else:
                            res_kpi_dom_dct = res_tmp_dom_dct
                    else:
                        res_tmp_prt_dct = self.scan_intf_peak(x['area'], x['type'], x['group_id'], x['group_name'],
                                                              thresh_1=95, thresh_2=90, thresh_3=85)
                        if res_kpi_prt_dct:
                            # dict(total=totl_link, thresh_1=totl_thresh_1, thresh_2=totl_thresh_2, thresh_3=totl_thresh_3)
                            res_kpi_prt_dct['total'] = res_kpi_prt_dct['total'] + res_tmp_prt_dct['total']
                            res_kpi_prt_dct['thresh_1'] = res_kpi_prt_dct['thresh_1'] + res_tmp_prt_dct['thresh_1']
                            res_kpi_prt_dct['thresh_2'] = res_kpi_prt_dct['thresh_2'] + res_tmp_prt_dct['thresh_2']
                            res_kpi_prt_dct['thresh_3'] = res_kpi_prt_dct['thresh_3'] + res_tmp_prt_dct['thresh_3']
                            res_kpi_prt_dct['detail'] = res_kpi_prt_dct['detail'] + res_tmp_prt_dct['detail']
                        else:
                            res_kpi_prt_dct = res_tmp_prt_dct

                res_info_dom_txt = ''

                res_info_dom += "- <b>Tổng số link Peering trong nuoc: " + str(res_kpi_dom_dct['total']) + "</b>\n"
                res_info_dom += "+ Số link vượt KPI từ 93% - 98% là: " + str(res_kpi_dom_dct['thresh_3']) + "\n"
                res_info_dom += "+ Số link vượt KPI > 98% là: " + str(res_kpi_dom_dct['thresh_2']) + "\n"
                # them vao doan txt
                res_info_dom_txt = "Peering trong nuoc\n"
                for x in res_kpi_dom_dct['detail']:
                    res_info_dom_txt += str(x)
                    res_info_dom_txt += "\n"
                res_info_dom_txt += "==============================\n"
                res_info_dom += "- <b>Tổng số MEM IPCORE (PE, CKV): " + str(res_kpi_mem_dct['total']) + "</b>\n"
                res_info_dom += "+ Số link MEM từ 85% - 90% là: " + str(res_kpi_mem_dct['thresh_3']) + "\n"
                res_info_dom += "+ Số link MEM từ 90% - 95% là: " + str(res_kpi_mem_dct['thresh_2']) + "\n"
                res_info_dom += "+ Số link MEM > 95% là: " + str(res_kpi_mem_dct['thresh_1']) + "\n"

                res_info_dom_txt += "Mem IPCore trong nuoc\n"
                for x in res_kpi_mem_dct['detail']:
                    res_info_dom_txt += str(x)
                    res_info_dom_txt += "\n"
                res_info_dom_txt += "==============================\n"

                res_info_dom += "- <b>Tổng số CPU IPCORE (PE, CKV): " + str(res_kpi_cpu_dct['total']) + "</b>\n"
                res_info_dom += "+ Số link CPU từ 60% - 65% là: " + str(res_kpi_cpu_dct['thresh_3']) + "\n"
                res_info_dom += "+ Số link CPU từ 65% - 70% là: " + str(res_kpi_cpu_dct['thresh_2']) + "\n"
                res_info_dom += "+ Số link CPU > 70% là: " + str(res_kpi_cpu_dct['thresh_1']) + "\n"

                res_info_dom_txt += "CPU IPCore trong nuoc\n"
                for x in res_kpi_cpu_dct['detail']:
                    res_info_dom_txt += str(x)
                    res_info_dom_txt += "\n"
                res_info_dom_txt += "==============================\n"

                res_info_dom += "- <b>Firewall VASCP: " + "</b>\n"
                res_info_dom += "+ Số CPU vượt KPI 60% là: " + str(res_kpi_cpu_fw_dct['thresh_3'] + res_kpi_cpu_fw_dct['thresh_2'] + res_kpi_cpu_fw_dct['thresh_1']) + "\n"
                res_info_dom += "+ Số MEM vượt KPI 70% là: " + str(res_kpi_mem_fw_dct['thresh_3'] + res_kpi_mem_fw_dct['thresh_2'] + res_kpi_mem_fw_dct['thresh_1']) + "\n"
                res_info_dom += "+ Số session vượt KPI từ 80% là: " + str(res_kpi_ses_fw_dct['thresh_3'] + res_kpi_ses_fw_dct['thresh_2'] + res_kpi_ses_fw_dct['thresh_1']) + "\n"

                res_info_dom_txt += "FW Vascp CPU\n"
                for x in res_kpi_cpu_fw_dct['detail']:
                    res_info_dom_txt += str(x)
                    res_info_dom_txt += "\n"
                res_info_dom_txt += "==============================\n"

                res_info_dom_txt += "FW Vascp MEM\n"
                for x in res_kpi_mem_fw_dct['detail']:
                    res_info_dom_txt += str(x)
                    res_info_dom_txt += "\n"
                res_info_dom_txt += "==============================\n"

                res_info_dom_txt += "FW Vascp Session\n"
                for x in res_kpi_ses_fw_dct['detail']:
                    res_info_dom_txt += str(x)
                    res_info_dom_txt += "\n"
                res_info_dom_txt += "==============================\n"


                res_info_dom += "- <b>Tổng số link Core Tinh, PE: " + str(res_kpi_prt_dct['total']) + "</b>\n"
                res_info_dom += "+ Số link vượt KPI từ 85% - 90% là: " + str(res_kpi_prt_dct['thresh_3']) + "\n"
                res_info_dom += "+ Số link vượt KPI từ 90% - 95% là: " + str(res_kpi_prt_dct['thresh_2']) + "\n"
                res_info_dom += "+ Số link vượt KPI > 95% là: " + str(res_kpi_prt_dct['thresh_1']) + "\n"

                res_info_dom_txt += "Core Tinh Pe\n"
                for x in res_kpi_prt_dct['detail']:
                    res_info_dom_txt += str(x)
                    res_info_dom_txt += "\n"
                res_info_dom_txt += "==============================\n"

                res_info_dom += "- <b>Tổng số link CORE MPBN: " + str(res_kpi_pe_p_mpbn_dct['total']) + "</b>\n"
                res_info_dom += "+ Số link vượt KPI > 60% là: " + str(res_kpi_pe_p_mpbn_dct['thresh_3']) + "\n"

                res_info_dom_txt += "Core MPBN\n"
                for x in res_kpi_pe_p_mpbn_dct['detail']:
                    res_info_dom_txt += str(x)
                    res_info_dom_txt += "\n"
                res_info_dom_txt += "==============================\n"

                res_info_dom += "- <b>Tổng số link Backbone Liên KV " + str(res_kpi_bb_inter_dct['total']) + "</b>\n"
                res_info_dom += "+ Số link vượt KPI từ 85% - 90% là: " + str(res_kpi_bb_inter_dct['thresh_3']) + "\n"
                res_info_dom += "+ Số link vượt KPI từ 90% - 95% là: " + str(res_kpi_bb_inter_dct['thresh_2']) + "\n"
                res_info_dom += "+ Số link vượt KPI > 95% là: " + str(res_kpi_bb_inter_dct['thresh_1']) + "\n"

                res_info_dom_txt += "Backbone LIEN kv\n"
                for x in res_kpi_bb_inter_dct['detail']:
                    res_info_dom_txt += str(x)
                    res_info_dom_txt += "\n"
                res_info_dom_txt += "==============================\n"

                res_info_dom += "<b>- Tổng số link  Backbone Nội Vùng: " + str(res_kpi_bb_inner_dct['total']) + "</b>\n"
                res_info_dom += "+ Số link vượt KPI từ 85% - 90% là: " + str(res_kpi_bb_inner_dct['thresh_3']) + "\n"
                res_info_dom += "+ Số link vượt KPI từ 90% - 95% là: " + str(res_kpi_bb_inner_dct['thresh_2']) + "\n"
                res_info_dom += "+ Số link vượt KPI > 95% là: " + str(res_kpi_bb_inner_dct['thresh_1']) + "\n"

                res_info_dom_txt += "Backbone Noi kv\n"
                for x in res_kpi_bb_inner_dct['detail']:
                    res_info_dom_txt += str(x)
                    res_info_dom_txt += "\n"
                res_info_dom_txt += "==============================\n"


                _bot.send_mess_html_channel(res_info, BOT_GROUP_INTERNET_VN)
                time.sleep(1)
                _bot.send_mess_html_channel(res_info_dom, BOT_GROUP_INTERNET_VN)
                time.sleep(1)

                _bot.send_mess_html_channel("Chi tiet:", BOT_GROUP_INTERNET_VN)
                _bot.send_mess_by_file(grp_id=BOT_GROUP_INTERNET_VN,
                                       mess=res_info_dom_txt)

            except Exception as err:
                print(err)
                log_scan.create_log('critical', err)
                time.sleep(1)

            time.sleep(interval)

    def stop(self):
        self.is_stop = True


def plus_time_sec(tme, num):
    import datetime
    return tme + datetime.timedelta(seconds=num)


def convert_timedelta_to_second(time_delta):
    return time_delta.total_seconds()


if __name__ == '__main__':
    import telebot

    '''
    dsn_aom_str = cx_Oracle.makedsn(SERVER_AOM, SERVER_AOM_PORT, service_name=SERVER_AOM_SERVICE_NAME)
    pool_aom = cx_Oracle.SessionPool(min=1,
                                     max=10, increment=1, threaded=True, dsn=dsn_aom_str,
                                     user=SERVER_AOM_USERNAME, password=SERVER_AOM_PASSWORD,
                                     encoding='UTF-8', nencoding='UTF-8')
    dsn_str = cx_Oracle.makedsn(SERVER_NOCPRO, SERVER_NOCPRO_PORT, service_name=SERVER_NOCPRO_SERVICE_NAME)
    pool_nocpro = cx_Oracle.SessionPool(min=4,
                                        max=20, increment=1, threaded=True, dsn=dsn_str,
                                        user=SERVER_NOCPRO_USERNAME, password=SERVER_NOCPRO_PASSWORD,
                                        encoding='UTF-8', nencoding='UTF-8')
    '''
    dbconfig = {
        "host": "192.168.251.15",
        "port": 3306,
        "user": "linhlk1",
        "password": "linhlk135",
        "database": "luuluong",
    }
    mysql_pool = ServerMysqlConnect(**dbconfig)

    bot = telebot.AsyncTeleBot(BOT_ID)
    _obj = ScanBotBackhaulUpstreamThread(False, 15 * 60, bot, mysql_pool=mysql_pool)
    _obj.start()
    print('112431243')
