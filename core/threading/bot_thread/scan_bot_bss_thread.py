__author__ = 'vtn-dhml-dhip10'
import threading
import time
import json
import cx_Oracle
from core.helpers.stringhelpers import convert_api_vipa_xml_to_dct
from core.log.logger.logger import Logging
from core.helpers.date_helpers import get_time_format_now, get_date_minus, get_date_minus_format_elk, \
    convert_date_str_to_date_obj_spring, convert_date_obj_to_date_str_spring, date_diff_in_seconds, \
    get_date_sec_format_elk, get_date_now, get_date_now_format_ipms
from core.database.impl.ipms.syslog_nocpro_impl import SyslogNocproIpmsImpl
from core.database.impl.nocpro.tbl_syslog_cntt_core_impl import TblSyslogCnttCoreImpl
from core.database.impl.bot.bot_impl import BotImpl
from core.database.impl.aom.tbl_aom_impl import TblAomImpl
from core.database.impl.nocpro.tbl_bss_core_impl import TblBssCoreImpl
from core.database.impl.nocpro.tbl_bss_cbs_core_impl import TblBssCbsCoreImpl
from core.helpers.fix_helper import get_area, get_area_from_acc
from app.api.vipa.parsing.api_soc_parsing import ApiSocParsing
from config import Config, Development
from core.database.impl.nocpro.tbl_gpon_olt_sub_impl import TblGponOltSubImpl

config = Development()
BOT_ID = config.__getattribute__('BOT_ID')
BOT_GROUP_ID = -218379033

BOT_GROUP_WHITELIST = config.__getattribute__('BOT_GROUP_WHITELIST')
BOT_GROUP_CNTT_ID = config.__getattribute__('BOT_GROUP_CNTT_ID')
SERVER_NOCPRO = config.__getattribute__('SERVER_NOCPRO')
SERVER_NOCPRO_PORT = config.__getattribute__('SERVER_NOCPRO_PORT')
SERVER_NOCPRO_SERVICE_NAME = config.__getattribute__('SERVER_NOCPRO_SERVICE_NAME')
SERVER_NOCPRO_USERNAME = config.__getattribute__('SERVER_NOCPRO_USERNAME')
SERVER_NOCPRO_PASSWORD = config.__getattribute__('SERVER_NOCPRO_PASSWORD')
SERVER_AOM = config.__getattribute__('SERVER_AOM')
SERVER_AOM_PORT = config.__getattribute__('SERVER_AOM_PORT')
SERVER_AOM_SERVICE_NAME = config.__getattribute__('SERVER_AOM_SERVICE_NAME')
SERVER_AOM_USERNAME = config.__getattribute__('SERVER_AOM_USERNAME')
SERVER_AOM_PASSWORD = config.__getattribute__('SERVER_AOM_PASSWORD')
BOT_GROUP_VIP_CDBR_ID = config.__getattribute__('BOT_GROUP_VIP_CDBR_ID')
BOT_GROUP_IP_CDBR_KV1 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV1')
BOT_GROUP_IP_CDBR_KV2 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV2')
BOT_GROUP_IP_CDBR_KV3 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV3')
BOT_GROUP_BSS_ACCESS_ID = config.__getattribute__('BOT_GROUP_BSS_ACCESS_ID')
BOT_GROUP_BSS_ID = config.__getattribute__('BOT_GROUP_BSS_ID')
TELEBOT_BOT_TOKEN = BOT_ID
GROUP_CHAT_ID = int(BOT_GROUP_ID)


class ScanBotBssThread(threading.Thread):
    def __init__(self, is_stop, period_time, _bot, pool_nocpro, pool_cbs):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.period_time = period_time
        self.thread_name = 'Bot Scan AOM'
        self._bot = _bot
        self.pool_nocpro = pool_nocpro
        self.pool_cbs = pool_cbs

    def run(self):
        interval = self.period_time
        # create session pool
        '''
        dsn_str = cx_Oracle.makedsn(SERVER_NOCPRO, SERVER_NOCPRO_PORT, service_name=SERVER_NOCPRO_SERVICE_NAME)
        pool_nocpro = cx_Oracle.SessionPool(min=1,
                                            max=20, increment=1, threaded=True, dsn=dsn_str,
                                            user=SERVER_NOCPRO_USERNAME, password=SERVER_NOCPRO_PASSWORD,
                                            encoding='UTF-8', nencoding='UTF-8')
        '''
        # danh sach tram nhay can check truoc. Neu co thay doi thi lay res_change sau 30p
        _obj_bss_impl = TblBssCoreImpl(self.pool_nocpro)
        _obj_bss_cbs_impl = TblBssCbsCoreImpl(self.pool_cbs)

        while not self.is_stop:
            log_scan = Logging('Scan BSS of FOIP', 'Thread scan log over and under')
            _now_only_date = get_date_now_format_ipms()
            _bot = BotImpl(self._bot)
            _obj_cntt_nocpro_impl = TblSyslogCnttCoreImpl(self.pool_nocpro)
            _obj_gpon_olt_impl = TblGponOltSubImpl(self.pool_nocpro)
            # bao cao dinh ky tai BSS
            '''
            "Báo cáo tải hệ thống tải 2020 lúc 01hh00:
            - NSS: 
                + MSS:  MSPD27 (38%) MSHT04 (10%) MSHT10 (17%)
                + HLR:  HLPD03 (22%) HLHL01 (20%)  HLHL02 (21%)
                + GMSC: GSHL02 (9%) GSPL06 (8%)  GSPD01 (7%)
            - IMS:
                + Max CPU:SBHL05 (pmp_5/CPU:40)
                + Max Session:SBHL05 (pmp_5/Session:40)
                + Max Reg User:SBHL05 (pmp_5/Reg:40)
            - VAS/PS: 
                + SGSN(%CPU): SGHL08 (32%), SGHL04 (32%), SGHL05 (34%)
                + GGSN(%pdp+bear): GGHT11 (93.83%), GGHT12 (84.36%), GGHT13 (84.35%)
                + GGSN(%THROUGHPUT): GGHT07 (33.67%), GGHT08 (33.31%), GGHT10 (30.39%)"		
                
            '''
            res_cpu_msc_lst = _obj_bss_impl.get_performance_cpu_msc()
            res_cpu_hlr_lst = _obj_bss_impl.get_performance_cpu_hlr()
            res_cpu_gmsc_lst = _obj_bss_cbs_impl.get_performance_cpu_gmsc()

            res_ims_cpu_sbc_lst = _obj_bss_cbs_impl.get_kpi_top_cpu_ims_sbc_gmsc()
            res_ims_session_sbc_lst = _obj_bss_cbs_impl.get_kpi_top_session_ims_sbc_gmsc()
            res_ims_reg_user_sbc_lst = _obj_bss_cbs_impl.get_kpi_top_reg_ims_sbc_gmsc()

            res_cpu_sgsn_lst = _obj_bss_impl.get_performance_cpu_sgsn()
            res_pdp_bear_ggsn_lst = _obj_bss_impl.get_performance_pdp_bear_sgsn()
            res_thrg_ggsn_lst = _obj_bss_impl.get_performance_throughout_ggsn()

            res_info_per_bss = 'Báo cáo tải hệ thống tải lúc ' + str(_now_only_date) + ":\n"
            res_info_per_bss += "- NSS:\n"
            res_info_per_bss += "    + MSS: "
            for x in res_cpu_msc_lst:
                for key, val in x.items():
                    if key.upper().find('TIME') < 0:
                        if key.upper().find('DEVICE_CODE') >= 0:
                            res_info_per_bss += "" + str(val) + " "
                        elif key.upper().find('PDP_BEARER') >= 0 or key.upper().find(
                                'THROUGHPUT') >= 0 or key.upper() == 'CPU':
                            res_info_per_bss += "(" + str(val) + " %) "
            res_info_per_bss += '\n'

            res_info_per_bss += "    + HLR: "
            for x in res_cpu_hlr_lst:
                for key, val in x.items():
                    if key.upper().find('TIME') < 0:
                        if key.upper().find('DEVICE_CODE') >= 0:
                            res_info_per_bss += "" + str(val) + " "
                        elif key.upper().find('PDP_BEARER') >= 0 or key.upper().find(
                                'THROUGHPUT') >= 0 or key.upper() == 'CPU':
                            res_info_per_bss += "(" + str(val) + " %) "
            res_info_per_bss += '\n'

            res_info_per_bss += "    + GMSC: "
            for x in res_cpu_gmsc_lst:
                for key, val in x.items():
                    if key.upper().find('TIME') < 0:
                        if key.upper().find('DEVICE_CODE') >= 0:
                            res_info_per_bss += "" + str(val) + " "
                        elif key.upper().find('PDP_BEARER') >= 0 or key.upper().find(
                                'THROUGHPUT') >= 0 or key.upper() == 'CPU':
                            res_info_per_bss += "(" + str(val) + " %) "
            res_info_per_bss += '\n'

            # them phan IMS
            res_info_per_bss += "- IMS:\n"
            # ket qua mong muon Device code (PMP/CPU/REG/Sessiong),
            print('IMS CPU')
            res_info_per_bss += "    + Max CPU%: "
            for x in res_ims_cpu_sbc_lst:
                print(x)
                if 'DEVICE_CODE' in x and 'UNIT_MAX_CPU' in x and 'CPLOAD' in x:
                    res_info_per_bss += x['DEVICE_CODE'] + '(' + x['UNIT_MAX_CPU'] + '/CPU:' + str(x['CPLOAD']) + ') '
            res_info_per_bss += '\n'

            res_info_per_bss += "    + Max Session%: "
            for x in res_ims_session_sbc_lst:
                print(x)
                if 'DEVICE_CODE' in x and 'UNIT_MAX' in x and 'CPSESSION' in x:
                    res_info_per_bss += x['DEVICE_CODE'] + '(' + x['UNIT_MAX'] + '/SESSION:' + str(x['CPSESSION']) + ') '

            res_info_per_bss += '\n'

            res_info_per_bss += "    + Max RegUser: "
            for x in res_ims_reg_user_sbc_lst:
                print(x)
                if 'DEVICE_CODE' in x and 'UNIT_MAX' in x and 'CPREGUSERS' in x :
                    res_info_per_bss += x['DEVICE_CODE'] + '(' + x['UNIT_MAX'] + '/REG:' + str(x['CPREGUSERS']) + ') '

            res_info_per_bss += '\n'

            res_info_per_bss += "- VAS/PS:\n"
            res_info_per_bss += "    + SGSN(%CPU): "
            for x in res_cpu_sgsn_lst:
                for key, val in x.items():
                    if key.upper().find('TIME') < 0:
                        if key.upper().find('DEVICE_CODE') >= 0:
                            res_info_per_bss += "" + str(val) + " "
                        elif key.upper().find('PDP_BEARER') >= 0 or key.upper().find(
                                'THROUGHPUT') >= 0 or key.upper() == 'CPU':
                            res_info_per_bss += "(" + str(val) + " %) "
            res_info_per_bss += '\n'

            res_info_per_bss += "    + GGSN(%pdp+bear): "
            for x in res_pdp_bear_ggsn_lst:
                for key, val in x.items():
                    if key.upper().find('TIME') < 0:
                        if key.upper().find('DEVICE_CODE') >= 0:
                            res_info_per_bss += "" + str(val) + " "
                        elif key.upper().find('PDP_BEARER') >= 0 or key.upper().find(
                                'THROUGHPUT') >= 0 or key.upper() == 'CPU':
                            res_info_per_bss += "(" + str(val) + " %) "
            res_info_per_bss += '\n'

            res_info_per_bss += "    + GGSN(%THROUGHPUT):"
            for x in res_thrg_ggsn_lst:
                for key, val in x.items():
                    if key.upper().find('TIME') < 0:
                        if key.upper().find('DEVICE_CODE') >= 0:
                            res_info_per_bss += "" + str(val) + " "
                        elif key.upper().find('PDP_BEARER') >= 0 or key.upper().find(
                                'THROUGHPUT') >= 0 or key.upper() == 'CPU':
                            res_info_per_bss += "(" + str(val) + " %) "
            res_info_per_bss += '\n'
            _bot.send_mess(res_info_per_bss, BOT_GROUP_BSS_ID)

            # chan 2 lan thi chay check cell, cabinet flap
            res_cell_flap_new_lst = _obj_bss_impl.get_cell_flap_30m()
            res_cell_flap_prvn_new_lst = _obj_bss_impl.get_cell_province_flap_30m()
            res_cbn_flap_new_lst = _obj_bss_impl.get_cabinet_flap_30m()
            res_cbn_flap_prvn_new_lst = _obj_bss_impl.get_cabinet_province_flap_30m()

            res_cell_flap_info = ''
            #res_cell_flap_prvn_info = 'Thống kê danh sách Tỉnh có cell nháy trong 30p: \n'
            res_cbn_flap_info = ''
            #res_cbn_flap_prvn_info = 'Thống kê danh sách Tỉnh có trạm nháy trong 30p: \n'
            '''
            if res_cbn_flap_prvn_new_lst:
                for res_cbn_chge in res_cbn_flap_prvn_new_lst:
                    try:
                        res_cbn_flap_prvn_info += res_cbn_chge["PROVINCE"] + " " \
                                                  "(" + str(res_cbn_chge["TU_NHAY"]) + ") \n"
                    except Exception as err:
                        print("Error %s when check province cell " % err)
                _bot.send_mess(res_cbn_flap_prvn_info, BOT_GROUP_BSS_ACCESS_ID)
                # Tram nhay

            if res_cbn_flap_new_lst:
                for res_cell_chge in res_cbn_flap_new_lst:
                    res_info = ''
                    for key, val in res_cell_chge.items():
                        res_info += str(key) + " : " + str(val) + "\n"
                    _bot.send_mess(res_cbn_flap_info, BOT_GROUP_BSS_ACCESS_ID)
                    _bot.send_mess(res_info, BOT_GROUP_BSS_ACCESS_ID)
                    time.sleep(1)
            # nghi giua hiep 300s
            
            time.sleep(300)

            if res_cell_flap_prvn_new_lst:
                for res_cell_flap in res_cell_flap_prvn_new_lst:
                    try:
                        res_cell_flap_prvn_info += res_cell_flap["PROVINCE"] + " (" \
                                                   "" + str(res_cell_flap["CELL_NHAY"]) + ") \n"
                    except Exception as err:
                        print("Error %s when check province cell " % err)

                _bot.send_mess(res_cell_flap_prvn_info, BOT_GROUP_BSS_ACCESS_ID)
                time.sleep(1)
            # Tram moi bi loi
            if res_cell_flap_new_lst:
                _bot.send_mess(res_cell_flap_info, BOT_GROUP_BSS_ACCESS_ID)
                for res_cell_chge in res_cell_flap_new_lst:
                    res_info = ''
                    for key, val in res_cell_chge.items():
                        res_info += str(key) + " : " + str(val) + "\n"
                    _bot.send_mess(res_info, BOT_GROUP_BSS_ACCESS_ID)
                time.sleep(1)
            '''
            time.sleep(interval)

            print("Pool Gpon Busy:" + str(_obj_cntt_nocpro_impl.pool.busy))

    def sum_acc_lst(self, acc_lst):
        # acc_lst gom cac dictionary la DEVICE_CODE, TROUBLE_TYPE, N_ACCOUNT_AFFECT, START_TIME
        res_lst = []
        for acc_dct in acc_lst:
            try:
                # giai quyet bai toan trung device_code do co nhieu port GPON
                dev_code = acc_dct.get('DEVICE_CODE')
                cus_aff = acc_dct.get('N_ACCOUNT_AFFECT')
                trb_type = acc_dct.get('TROUBLE_TYPE')
                time_err = acc_dct.get('START_TIME')
                if res_lst:
                    chck_exst = False
                    for res in res_lst:
                        res_dev_code = res.get('DEVICE_CODE')
                        res_cus_aff = res.get('N_ACCOUNT_AFFECT')
                        res_trb_type = res.get('TROUBLE_TYPE')
                        if dev_code == res_dev_code and trb_type == res_trb_type:
                            res_cus_aff += cus_aff
                            res['N_ACCOUNT_AFFECT'] = res_cus_aff
                            chck_exst = True
                    if not chck_exst:
                        # tao moi
                        res_lst.append(dict(DEVICE_CODE=dev_code, N_ACCOUNT_AFFECT=cus_aff,
                                            TROUBLE_TYPE=trb_type, START_TIME=time_err))
                else:
                    res_lst.append(dict(DEVICE_CODE=dev_code, N_ACCOUNT_AFFECT=cus_aff,
                                        TROUBLE_TYPE=trb_type, START_TIME=time_err))

            except Exception as err:
                print(err)

        return res_lst

    def updt_res_lst(self, res_old_lst, res_new_lst, key_wrd, key_val_count):
        res_chge_lst = []
        key_val_count = key_val_count.upper()
        key_wrd = key_wrd.upper()
        try:

            if not res_old_lst:
                return res_new_lst, res_new_lst
            else:
                # kiem tra res_old va cap nhat
                for res_new in res_new_lst:
                    # Kiem tra neu co ton tai trong list cu thi cap nhat value, neu khong ton tai thi them moi
                    for key, val in res_new.items():
                        if key.upper() == key_wrd.upper():
                            val_new = val
                            if key_val_count in res_new:
                                val_new_count = res_new[key_val_count]
                                chck_key = True
                                chck_updt = False
                                for res_old in res_old_lst:
                                    for key_old, val_old in res_old.items():
                                        if key_old.upper() == key_wrd.upper():
                                            if val_new == val_old:
                                                # cung 1 node
                                                if key_val_count in res_old:
                                                    val_old_count = res_old[key_val_count]
                                                    if val_old_count != val_new_count:
                                                        res_new['CHANGE'] = val_new_count - val_old_count
                                                        res_chge_lst.append(res_new)
                                                        res_old[key_val_count] = val_new_count
                                                    chck_updt = True
                                                    break
                                    if chck_updt:
                                        break

                                if not chck_updt:
                                    # them moi
                                    res_old_lst.append(res_new)
                                    res_chge_lst.append(res_new)
                                    break
        except Exception as err:
            print("Error %s when check duplicate of list" % err)

        return res_old_lst, res_chge_lst

    def stop(self):
        self.is_stop = True


def plus_time_sec(tme, num):
    import datetime
    return tme + datetime.timedelta(seconds=num)


def convert_timedelta_to_second(time_delta):
    return time_delta.total_seconds()


if __name__ == '__main__':
    import telebot

    SERVER_NOCPRO = config.__getattribute__('SERVER_NOCPRO')
    SERVER_NOCPRO_PORT = config.__getattribute__('SERVER_NOCPRO_PORT')
    SERVER_NOCPRO_SERVICE_NAME = config.__getattribute__('SERVER_NOCPRO_SERVICE_NAME')
    SERVER_NOCPRO_USERNAME = config.__getattribute__('SERVER_NOCPRO_USERNAME')
    SERVER_NOCPRO_PASSWORD = config.__getattribute__('SERVER_NOCPRO_PASSWORD')
    SERVER_AOM = config.__getattribute__('SERVER_AOM')
    SERVER_AOM_PORT = config.__getattribute__('SERVER_AOM_PORT')
    SERVER_AOM_SERVICE_NAME = config.__getattribute__('SERVER_AOM_SERVICE_NAME')
    SERVER_AOM_USERNAME = config.__getattribute__('SERVER_AOM_USERNAME')
    SERVER_AOM_PASSWORD = config.__getattribute__('SERVER_AOM_PASSWORD')
    SERVER_CBS = config.__getattribute__('SERVER_CBS')
    SERVER_CBS_PORT = config.__getattribute__('SERVER_CBS_PORT')
    SERVER_CBS_SERVICE_NAME = config.__getattribute__('SERVER_CBS_SERVICE_NAME')
    SERVER_CBS_USERNAME = config.__getattribute__('SERVER_CBS_USERNAME')
    SERVER_CBS_PASSWORD = config.__getattribute__('SERVER_CBS_PASSWORD')
    SERVER_HOST = config.__getattribute__('SERVER_HOST')
    SERVER_HOST_USER = config.__getattribute__('SERVER_HOST_USER')
    SERVER_HOST_PASS = config.__getattribute__('SERVER_HOST_PASS')

    dsn_str = cx_Oracle.makedsn(SERVER_NOCPRO, SERVER_NOCPRO_PORT, service_name=SERVER_NOCPRO_SERVICE_NAME)
    pool_nocpro = cx_Oracle.SessionPool(min=4,
                                        max=20, increment=1, threaded=True, dsn=dsn_str,
                                        user=SERVER_NOCPRO_USERNAME, password=SERVER_NOCPRO_PASSWORD,
                                        encoding='UTF-8', nencoding='UTF-8')



    dsn_cbs_str = cx_Oracle.makedsn(SERVER_CBS, SERVER_CBS_PORT, service_name=SERVER_CBS_SERVICE_NAME)
    pool_cbs = cx_Oracle.SessionPool(min=1,
                                     max=10, increment=1, threaded=True, dsn=dsn_cbs_str,
                                     user=SERVER_CBS_USERNAME, password=SERVER_CBS_PASSWORD,
                                     encoding='UTF-8', nencoding='UTF-8')

    bot = telebot.AsyncTeleBot(BOT_ID)
    _obj = ScanBotBssThread(False, 30 * 60, bot, pool_nocpro=pool_nocpro,
                            pool_cbs=pool_cbs)
    _obj.start()
    print('112431243')
