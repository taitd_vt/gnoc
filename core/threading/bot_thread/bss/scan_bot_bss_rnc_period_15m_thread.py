__author__ = 'vtn-dhml-dhip10'
import threading
import time
import datetime
import json
import cx_Oracle
from core.helpers.stringhelpers import convert_api_vipa_xml_to_dct
from core.log.logger.logger import Logging
from core.helpers.date_helpers import get_time_format_now, get_date_minus, get_date_minus_format_elk, \
    convert_date_str_to_date_obj_spring, convert_date_obj_to_date_str_spring, date_diff_in_seconds, \
    get_date_sec_format_elk, get_date_now
from core.database.impl.ipms.syslog_nocpro_impl import SyslogNocproIpmsImpl
from core.database.impl.nocpro.tbl_syslog_cntt_core_impl import TblSyslogCnttCoreImpl
from core.database.impl.bot.bot_impl import BotImpl
from core.database.impl.aom.tbl_aom_impl import TblAomImpl
from core.database.impl.nocpro.tbl_bss_cbs_core_impl import TblBssCbsCoreImpl
from core.helpers.fix_helper import get_area, get_area_from_acc
from app.api.vipa.parsing.api_soc_parsing import ApiSocParsing
from config import Config, Development

from core.helpers.fix_helper import get_alarm_level

config = Development()
BOT_ID = config.__getattribute__('BOT_ID')
SERVER_CBS = config.__getattribute__('SERVER_CBS')
SERVER_CBS_PORT = config.__getattribute__('SERVER_CBS_PORT')
SERVER_CBS_SERVICE_NAME = config.__getattribute__('SERVER_CBS_SERVICE_NAME')
SERVER_CBS_USERNAME = config.__getattribute__('SERVER_CBS_USERNAME')
SERVER_CBS_PASSWORD = config.__getattribute__('SERVER_CBS_PASSWORD')

BOT_GROUP_BSS_ID = config.__getattribute__('BOT_GROUP_BSS_ID')
TELEBOT_BOT_TOKEN = BOT_ID


class ScanBotBssRncPeriod15mThread(threading.Thread):
    def __init__(self, is_stop, _bot, pool_cbs):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.period_time = 13 * 60
        self.thread_name = 'Bot Scan BSS RNC Period 2 times'
        self._bot = _bot
        self.pool_cbs = pool_cbs

    @staticmethod
    def find_rnc_in_lst(node_code, rnc_lst):
        res = dict()
        for rnc_dct in rnc_lst:
            try:
                if rnc_dct['NODE']:
                    if rnc_dct['NODE'] == node_code:
                        return rnc_dct

            except Exception as err:
                print("Error %s when find RNC in list 15m period" % str(err))
        return res

    def run(self):
        interval = self.period_time
        # create session pool
        # danh sach tram nhay can check truoc. Neu co thay doi thi lay res_change sau 30p
        _obj_bss_impl = TblBssCbsCoreImpl(self.pool_cbs)

        while not self.is_stop:
            log_scan = Logging('Scan AOM of FOIP', 'Thread scan log over and under')
            _bot = BotImpl(self._bot)

            # chan 2 lan thi chay check cell, cabinet flap

            res_rnc_last_lst = _obj_bss_impl.get_kpi_detail_rnc_last()
            res_rnc_now_lst = _obj_bss_impl.get_kpi_detail_rnc_now()

            res_cs_cdr_lst = []

            for rnc_dct in res_rnc_now_lst:
                try:
                    node_name = rnc_dct['NODE']
                    node_cs_cdr_now = rnc_dct['CS_CDR']
                    node_ps_cdr_now = rnc_dct['PS_CDR']
                    node_ps_traf_now = rnc_dct['PS_TRAFFIC']

                    node_last_dct = self.find_rnc_in_lst(node_code=node_name,
                                                         rnc_lst=res_rnc_last_lst)
                    if node_last_dct:
                        node_cs_cdr_last = node_last_dct['CS_CDR']
                        node_ps_cdr_last = node_last_dct['PS_CDR']
                        node_ps_traf_last = node_last_dct['PS_TRAFFIC']

                        if node_cs_cdr_now >= node_cs_cdr_last * 1.5 and node_cs_cdr_last > 0:
                            # res_cs_cdr_lst.append(dict(node_last=node_last_dct,
                            #                           node_now=rnc_dct,
                            #                           type_err="cs_cdr"))
                            pass
                        elif node_ps_cdr_now >= node_ps_cdr_last * 3 and node_ps_cdr_last > 0:
                            res_cs_cdr_lst.append(dict(node_last=node_last_dct,
                                                       node_now=rnc_dct,
                                                       type_err="ps_cdr"))
                        elif node_ps_traf_last * 0.7 >= node_ps_traf_now > 0:
                            res_cs_cdr_lst.append(dict(node_last=node_last_dct,
                                                       node_now=rnc_dct,
                                                       type_err="ps_traf"))

                except Exception as err:
                    print("Error %s when get RNC of Period 15m" % str(err))

            if res_cs_cdr_lst:
                for res_cs_cdr_dct in res_cs_cdr_lst:
                    try:
                        type_err = res_cs_cdr_dct['type_err']
                        if type_err == 'cs_cdr':
                            res_info = '***Tăng CS_CDR 150 % đột biến: *** \n'
                        elif type_err == 'ps_cdr':
                            res_info = '***Tăng PS_CDR 150 % đột biến: *** \n'
                        elif type_err == 'ps_traf':
                            res_info = '***Giảm PS_TRAFFIC 30 % đột biến: *** \n'
                        else:
                            res_info = ''

                        res_info += '***15 phút trước: *** \n'
                        for key, val in res_cs_cdr_dct['node_last'].items():
                            if val:
                                res_info += str(key) + ": " + str(val) + "\n"
                        res_info += '***Hiện tại: *** \n'
                        for key, val in res_cs_cdr_dct['node_now'].items():
                            if val:
                                res_info += str(key) + ": " + str(val) + "\n"
                        _bot.send_mess(res_info, BOT_GROUP_BSS_ID)

                        time.sleep(1)
                    except Exception as err:
                        print("Error %s when check list cs cdr rnc " % err)

            time.sleep(interval)


if __name__ == '__main__':
    import telebot

    bot = telebot.AsyncTeleBot(BOT_ID)
    dsn_cbs_str = cx_Oracle.makedsn(SERVER_CBS, SERVER_CBS_PORT, service_name=SERVER_CBS_SERVICE_NAME)
    pool_cbs = cx_Oracle.SessionPool(min=1,
                                     max=10, increment=1, threaded=True, dsn=dsn_cbs_str,
                                     user=SERVER_CBS_USERNAME, password=SERVER_CBS_PASSWORD,
                                     encoding='UTF-8', nencoding='UTF-8')

    _obj = ScanBotBssRncPeriod15mThread(False, bot, pool_cbs)
    _obj.start()
    print('112431243')
