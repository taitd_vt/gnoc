__author__ = 'vtn-dhml-dhip10'
import threading
import time
import datetime
import json
import cx_Oracle
from core.helpers.stringhelpers import convert_api_vipa_xml_to_dct
from core.log.logger.logger import Logging
from core.helpers.date_helpers import get_time_format_now, get_date_minus, get_date_minus_format_elk, \
    convert_date_str_to_date_obj_spring, convert_date_obj_to_date_str_spring, date_diff_in_seconds, \
    get_date_sec_format_elk, get_date_now
from core.database.impl.ipms.syslog_nocpro_impl import SyslogNocproIpmsImpl
from core.database.impl.nocpro.tbl_syslog_cntt_core_impl import TblSyslogCnttCoreImpl
from core.database.impl.bot.bot_impl import BotImpl
from core.database.impl.aom.tbl_aom_impl import TblAomImpl
from core.database.impl.nocpro.tbl_bss_core_impl import TblBssCoreImpl
from core.helpers.fix_helper import get_area, get_area_from_acc
from app.api.vipa.parsing.api_soc_parsing import ApiSocParsing
from config import Config, Development
from core.database.impl.nocpro.tbl_gpon_olt_sub_impl import TblGponOltSubImpl
from core.helpers.fix_helper import get_alarm_level
config = Development()
BOT_ID = config.__getattribute__('BOT_ID')
BOT_GROUP_ID = -218379033

BOT_GROUP_WHITELIST = config.__getattribute__('BOT_GROUP_WHITELIST')
BOT_GROUP_CNTT_ID = config.__getattribute__('BOT_GROUP_CNTT_ID')
SERVER_NOCPRO = config.__getattribute__('SERVER_NOCPRO')
SERVER_NOCPRO_PORT = config.__getattribute__('SERVER_NOCPRO_PORT')
SERVER_NOCPRO_SERVICE_NAME = config.__getattribute__('SERVER_NOCPRO_SERVICE_NAME')
SERVER_NOCPRO_USERNAME = config.__getattribute__('SERVER_NOCPRO_USERNAME')
SERVER_NOCPRO_PASSWORD = config.__getattribute__('SERVER_NOCPRO_PASSWORD')
SERVER_AOM = config.__getattribute__('SERVER_AOM')
SERVER_AOM_PORT = config.__getattribute__('SERVER_AOM_PORT')
SERVER_AOM_SERVICE_NAME = config.__getattribute__('SERVER_AOM_SERVICE_NAME')
SERVER_AOM_USERNAME = config.__getattribute__('SERVER_AOM_USERNAME')
SERVER_AOM_PASSWORD = config.__getattribute__('SERVER_AOM_PASSWORD')
BOT_GROUP_VIP_CDBR_ID = config.__getattribute__('BOT_GROUP_VIP_CDBR_ID')
BOT_GROUP_IP_CDBR_KV1 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV1')
BOT_GROUP_IP_CDBR_KV2 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV2')
BOT_GROUP_IP_CDBR_KV3 = config.__getattribute__('BOT_GROUP_IP_CDBR_KV3')
BOT_GROUP_BSS_ACCESS_ID = config.__getattribute__('BOT_GROUP_BSS_ACCESS_ID')
BOT_GROUP_BSS_ID = config.__getattribute__('BOT_GROUP_BSS_ID')
TELEBOT_BOT_TOKEN = BOT_ID
GROUP_CHAT_ID = int(BOT_GROUP_ID)


class ScanBotBssImportant10mThread(threading.Thread):
    def __init__(self, is_stop, _bot, pool_nocpro):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.period_time = 5 * 60
        self.thread_name = 'Bot Scan AOM'
        self._bot = _bot
        self.pool_nocpro = pool_nocpro

    def run(self):
        interval = self.period_time
        # create session pool
        '''
        dsn_str = cx_Oracle.makedsn(SERVER_NOCPRO, SERVER_NOCPRO_PORT, service_name=SERVER_NOCPRO_SERVICE_NAME)
        pool_nocpro = cx_Oracle.SessionPool(min=1,
                                            max=20, increment=1, threaded=True, dsn=dsn_str,
                                            user=SERVER_NOCPRO_USERNAME, password=SERVER_NOCPRO_PASSWORD,
                                            encoding='UTF-8', nencoding='UTF-8')
        '''
        # danh sach tram nhay can check truoc. Neu co thay doi thi lay res_change sau 30p
        _obj_bss_impl = TblBssCoreImpl(self.pool_nocpro)

        while not self.is_stop:
            log_scan = Logging('Scan AOM of FOIP', 'Thread scan log over and under')
            _bot = BotImpl(self._bot)

            # chan 2 lan thi chay check cell, cabinet flap
            time_now = datetime.datetime.now()
            future_50m = datetime.datetime(time_now.year, time_now.month, time_now.day, time_now.hour, 50)
            future_40m = datetime.datetime(time_now.year, time_now.month, time_now.day, time_now.hour, 40)
            future_20m = datetime.datetime(time_now.year, time_now.month, time_now.day, time_now.hour, 20)
            future_10m = datetime.datetime(time_now.year, time_now.month, time_now.day, time_now.hour, 10)

            if 10 > time_now.minute > 0:
                time_to_sleep = (future_10m - time_now).seconds
                if time_to_sleep > 0:
                    time.sleep(time_to_sleep)
            elif 20 > time_now.minute > 10:
                time_to_sleep = (future_20m - time_now).seconds
                if time_to_sleep > 0:
                    time.sleep(time_to_sleep)
            elif 40 > time_now.minute > 20:
                time_to_sleep = (future_40m - time_now).seconds
                if time_to_sleep > 0:
                    time.sleep(time_to_sleep)
            elif 50 > time_now.minute > 40:
                time_to_sleep = (future_50m - time_now).seconds
                if time_to_sleep > 0:
                    time.sleep(time_to_sleep)
            elif time_now.minute > 50:
                future_10m += datetime.timedelta(hours=1)
                time_to_sleep = (future_10m - time_now).seconds
                if time_to_sleep > 0:
                    time.sleep(time_to_sleep)

            res_10m_lst = _obj_bss_impl.get_important_alarm_10m()
            res_smke_10m_lst = _obj_bss_impl.get_smoke_node_15m()
            res_info = '**Thống kê danh sách cảnh báo quan trọng 10p tồn:** \n'

            if res_10m_lst:
                frst_elmt = 0
                for res_cbn_chge in res_10m_lst:
                    try:
                        frst_elmt += 1
                        if frst_elmt > 1:
                            res_info = ''
                        for key, value in res_cbn_chge.items():
                            if value:
                                if key.upper() == 'MUC_DO':
                                    val_conv = get_alarm_level(str(value))
                                    res_info += str(key) + ": " + str(val_conv) + " \n"
                                else:
                                    res_info += str(key) + ": " + str(value) + " \n"

                        _bot.send_mess(res_info, BOT_GROUP_BSS_ID)
                        time.sleep(1)
                    except Exception as err:
                        print("Error %s when check province cell " % err)
            res_info = '**Thống kê danh sách trạm down có cảnh báo khói 10p tồn:** \n'

            if res_smke_10m_lst:

                for res_cbn_chge in res_smke_10m_lst:
                    try:

                        for key, value in res_cbn_chge.items():
                            if value:
                                res_info += str(key) + ": " + str(value) + " \n"

                        _bot.send_mess(res_info, BOT_GROUP_BSS_ACCESS_ID)
                        time.sleep(1)
                    except Exception as err:
                        print("Error %s when check province cell " % err)

            time.sleep(interval)


if __name__ == '__main__':
    import telebot

    bot = telebot.AsyncTeleBot(BOT_ID)
    dsn_str = cx_Oracle.makedsn(SERVER_NOCPRO, SERVER_NOCPRO_PORT, service_name=SERVER_NOCPRO_SERVICE_NAME)
    pool_nocpro = cx_Oracle.SessionPool(min=1,
                                        max=20, increment=1, threaded=True, dsn=dsn_str,
                                        user=SERVER_NOCPRO_USERNAME, password=SERVER_NOCPRO_PASSWORD,
                                        encoding='UTF-8', nencoding='UTF-8')
    _obj = ScanBotBssImportant10mThread(False, bot, pool_nocpro)
    _obj.start()
    print('112431243')
