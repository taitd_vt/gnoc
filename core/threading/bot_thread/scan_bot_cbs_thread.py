__author__ = 'vtn-dhml-dhip10'
import threading
import time
from core.log.logger.logger import Logging
from core.helpers.date_helpers import get_time_format_now, get_date_minus, get_date_minus_format_elk, \
    convert_date_str_to_date_obj_spring, convert_date_obj_to_date_str_spring, date_diff_in_seconds, \
    get_date_sec_format_elk
from core.database.impl.ipms.syslog_nocpro_impl import SyslogNocproIpmsImpl
from core.database.impl.bot.bot_impl import BotImpl
import telebot
from config import Config, Development
config = Development()
BOT_ID = config.__getattribute__('BOT_ID')
BOT_GROUP_ID = -218379033


class ScanBotCbsThread(threading.Thread):
    def __init__(self, is_stop, period_time, area, _bot, num_time_last=0):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.period_time = period_time
        self.thread_name = 'Bot Scan CBS'
        self.area = area
        self._bot = _bot
        self.num_time_last = num_time_last

    def run(self):
        interval = self.period_time
        time_now = get_time_format_now()
        time_last = get_date_minus_format_elk(time_now, 60 * 7 + self.num_time_last)
        #time_last = get_date_minus_format_elk(time_now, 60 * 1 * 60 * 5)
        while not self.is_stop:
            log_scan = Logging('Scan CBS overthreshold and under of FOIP', 'Thread scan log over and under')

            try:
                # scan auto
                print("Begin starting get Syslog Overthreshold and Packet error of %s" % self.area)
                _obj = SyslogNocproIpmsImpl(self.area)
                _host_lst = _obj.find_cbs(time_last, "FOIP&error", True, True)
                if _host_lst:

                    time_last_new = _host_lst[-1]['date_time']
                    time_last_frm = convert_date_str_to_date_obj_spring(time_last_new)
                    time_last = get_date_sec_format_elk(time_last_frm, 60 * 60 * 7 - 2)

                    test = _obj.send_bot(_host_lst, self._bot)
                print("Da chay CBS %s xong va di ngu" % self.area)

                _host_lst = _obj.find_cbs(time_last, "FOIP&over threshold", True, True)
                if _host_lst:
                    print("Host list is " + str(_host_lst))
                    time_last_new = _host_lst[-1]['date_time']
                    time_last_frm = convert_date_str_to_date_obj_spring(time_last_new)
                    time_last = get_date_sec_format_elk(time_last_frm, 60 * 60 * 7 - 2)

                    test = _obj.send_bot(_host_lst, self._bot)
                print("Da chay CBS %s xong va di ngu" % self.area)
                time.sleep(interval)

            except Exception as err:
                print(err)
                log_scan.create_log('critical', err)
                time.sleep(interval)

    def stop(self):
        self.is_stop = True


def plus_time_sec(tme, num):
    import datetime
    return tme + datetime.timedelta(seconds=num)


def convert_timedelta_to_second(time_delta):
    return time_delta.total_seconds()


if __name__ == '__main__':
    BOT_ID = config.__getattribute__('BOT_ID')
    TELEBOT_BOT_TOKEN = BOT_ID
    bot = telebot.AsyncTeleBot(BOT_ID)
    _scan_kv1 = ScanBotCbsThread(False, 5 * 60, 'KV2', bot)
    _scan_kv1.start()
