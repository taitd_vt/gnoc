__author__ = 'vtn-dhml-dhip10'
import threading
import time
import json
from core.helpers.stringhelpers import convert_api_vipa_xml_to_dct
from core.log.logger.logger import Logging
from core.helpers.date_helpers import get_time_format_now, get_date_minus, get_date_minus_format_elk, \
    convert_date_str_to_date_obj_spring, convert_date_obj_to_date_str_spring, date_diff_in_seconds, \
    get_date_sec_format_elk
from core.database.impl.ipms.syslog_nocpro_impl import SyslogNocproIpmsImpl
from core.database.impl.bot.bot_impl import BotImpl
from core.database.impl.server_minhnd.tbl_customer_vip_impl import TblCustomerVipImpl
from app.api.vipa.parsing.api_soc_parsing import ApiSocParsing
from config import Config, Development

config = Development()
BOT_ID = config.__getattribute__('BOT_ID')
BOT_GROUP_ID = -218379033
BOT_GROUP_WHITELIST = config.__getattribute__('BOT_GROUP_WHITELIST')
BOT_GROUP_VIP_CDBR_ID = config.__getattribute__('BOT_GROUP_VIP_CDBR_ID')
BOT_GROUP_SUPER_VIP = config.__getattribute__('BOT_GROUP_SUPER_VIP')
BOT_GROUP_VIP_KHDN_ID = config.__getattribute__('BOT_GROUP_VIP_KHDN_ID')
TELEBOT_BOT_TOKEN = BOT_ID
GROUP_CHAT_ID = int(BOT_GROUP_ID)


class ScanBotSocThread(threading.Thread):
    def __init__(self, is_stop, period_time, area, _bot):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.period_time = period_time
        self.thread_name = 'Bot Scan Soc'
        self.area = area
        self._bot = _bot

    def run(self):
        interval = self.period_time
        time_now = get_time_format_now()
        time_last = get_date_minus_format_elk(time_now, 60 * 7)
        data_cus_old_lst = []
        data_cus_vip_old_lst = []
        _bot = BotImpl(self._bot)
        frst_run = 0
        _tbl_cus_vip_obj = TblCustomerVipImpl()
        while not self.is_stop:
            log_scan = Logging('Scan SOC of FOIP', 'Thread scan log over and under')
            print("Scan SOC of Account VIP and super VIP NEWW")
            try:
                # scan auto

                _api = ApiSocParsing()
                _cus_info = _api.request()
                _cus_lst_str = convert_api_vipa_xml_to_dct(_cus_info)
                _cus_conv_dct = json.loads(_cus_lst_str)
                # them phan check danh sach khach hang VIP. Neu co khach hang VIP bi nt vao group SUPER VIP
                _tbl_cus_vip_lst = _tbl_cus_vip_obj.list()
                if _cus_conv_dct:
                    if 'data' in _cus_conv_dct:
                        data_cus_lst = _cus_conv_dct['data']
                        print("Danh sach khach hang VIP loi: " + str(len(data_cus_lst)) + " nguoi")
                        try:
                            data_flt_new_lst = []
                            data_vip_lst = []
                            for x in data_cus_lst:
                                try:
                                    serv_type = ''
                                    tel_serv_id = ''
                                    symp = ''
                                    resn = ''
                                    if 'service_type' in x:
                                        serv_type = x['service_type']
                                    if 'tel_service_id' in x:
                                        tel_serv_id = str(x['tel_service_id'])
                                    if 'reason' in x:
                                        resn = x['reason']
                                    if 'user_type' in x:
                                        usr_type = x['user_type']
                                    else:
                                        usr_type = ''

                                    chck_send = False
                                    if 'symptom' in x:
                                        symp = x['symptom']
                                    if serv_type == 'F' or tel_serv_id == '28':
                                        if symp == 'Mất dịch vụ':
                                            chck_send = True

                                    elif serv_type == 'U' or symp == 'Giật vỡ hình':
                                        chck_send = True
                                    # THEM LOAI KHACH HANG VIP VAO GROUP
                                    if usr_type.find('VIP') >= 0:
                                        chck_send = True

                                    if chck_send:
                                        if not resn:
                                            resn = 'Chưa xác định'
                                        if 'phone_number' in x:
                                            phne_num = str(x['phone_number'])
                                        else:
                                            phne_num = ''

                                        if 'user_full_name' in x:
                                            usr_full = str(x['user_full_name'])
                                        else:
                                            usr_full = ''

                                        if 'user_address' in x:
                                            usr_add = str(x['user_address'])
                                        else:
                                            usr_add = ''

                                        data_flt_new_lst.append(dict(service_acc=x['service_acc'],
                                                                     user_type=x['user_type'],
                                                                     symptom=x['symptom'],
                                                                     problem_start_time=x['problem_start_time'],
                                                                     user_full_name=usr_full,
                                                                     user_address=usr_add,
                                                                     phone_number=phne_num,
                                                                     reason=resn))
                                        # check khach hang co trong list khong ?
                                        for cust_vip in _tbl_cus_vip_lst:
                                            if str(x['service_acc']).upper() == cust_vip['account'].upper():
                                                data_vip_lst.append(dict(service_acc=x['service_acc'],
                                                                         user_type=x['user_type'],
                                                                         symptom=x['symptom'],
                                                                         problem_start_time=x['problem_start_time'],
                                                                         user_full_name=usr_full,
                                                                         user_address=usr_add,
                                                                         phone_number=phne_num,
                                                                         reason=resn,
                                                                         name_vip=cust_vip['name'],
                                                                         departure=cust_vip['departure'],
                                                                         position=cust_vip['position'],
                                                                         center=cust_vip['center'],
                                                                         phone_vip=cust_vip['number']))
                                                break
                                except Exception as err:
                                    print("Error %s when append data filter new" % err)

                            # kiem trung de nhan tin
                            # Loc danh sach khach hang SUPER VIP
                            print("Kiem trung nhan tin SOC")
                            if data_vip_lst:
                                for data_cus_new in data_vip_lst:
                                    if 'service_acc' in data_cus_new:
                                        cus_new_acc = data_cus_new['service_acc']
                                    else:
                                        cus_new_acc = ''
                                    chck_exst = False
                                    for data_cus_old in data_cus_vip_old_lst:
                                        if 'service_acc' in data_cus_old:
                                            cus_old_acc = data_cus_old['service_acc']
                                        else:
                                            cus_old_acc = ''
                                        if cus_old_acc == cus_new_acc:
                                            chck_exst = True
                                            break
                                    if not chck_exst:
                                        # Acc moi xuat hien

                                        mess = "Lỗi khách hàng: " + str(data_cus_new['service_acc']) + "\n" + \
                                               "Loại khách hàng: " + str(data_cus_new['user_type']) + "\n" + \
                                               "Nguyên nhân: " + str(data_cus_new['reason']) + "\n" + \
                                               "Loại lỗi: " + str(data_cus_new['symptom']) + "\n" + \
                                               "Thời gian: " + str(data_cus_new['problem_start_time']) + "\n" + \
                                               "Tên Khách hàng VIP: " + str(data_cus_new['name_vip']) + "\n" + \
                                               "Phòng ban VIP: " + str(data_cus_new['departure']) + "\n" + \
                                               "Trung tâm: " + str(data_cus_new['center']) + "\n" + \
                                               "Vị trí đảm nhiệm: " + str(data_cus_new['position']) + "\n" + \
                                               "Địa chỉ: " + str(data_cus_new['user_address']) + "\n" + \
                                               "SĐT: " + str(data_cus_new['phone_number']) + "\n"

                                        if frst_run > 0:
                                            _bot.send_mess(mess, BOT_GROUP_SUPER_VIP)
                                            if str(data_cus_new['user_type']).strip() == 'VIP_SUPPER_KHDN':
                                                _bot.send_mess(mess, BOT_GROUP_VIP_KHDN_ID)

                                data_cus_vip_old_lst = data_vip_lst
                            elif data_vip_lst:
                                data_cus_vip_old_lst = data_vip_lst

                            # Danh sach khach hang VIP Thuong

                            if data_flt_new_lst:
                                for data_cus_new in data_flt_new_lst:
                                    if 'service_acc' in data_cus_new:
                                        cus_new_acc = data_cus_new['service_acc']
                                    else:
                                        cus_new_acc = ''

                                    chck_exst = False
                                    for data_cus_old in data_cus_old_lst:
                                        if 'service_acc' in data_cus_old:
                                            cus_old_acc = data_cus_old['service_acc']
                                        else:
                                            cus_old_acc = ''
                                        if cus_old_acc == cus_new_acc:
                                            chck_exst = True
                                            break
                                    if not chck_exst:
                                        # Acc moi xuat hien

                                        mess = "Lỗi khách hàng: " + str(data_cus_new['service_acc']) + "\n" + \
                                               "Loại khách hàng: " + str(data_cus_new['user_type']) + "\n" + \
                                               "Nguyên nhân: " + str(data_cus_new['reason']) + "\n" + \
                                               "Loại lỗi: " + str(data_cus_new['symptom']) + "\n" + \
                                               "Thời gian: " + str(data_cus_new['problem_start_time']) + "\n" + \
                                               "Tên Khách hàng: " + str(data_cus_new['user_full_name']) + "\n" + \
                                               "Địa chỉ: " + str(data_cus_new['user_address']) + "\n" + \
                                               "SĐT: " + str(data_cus_new['phone_number'])

                                        if frst_run > 0:
                                            _bot.send_mess(mess, BOT_GROUP_VIP_CDBR_ID)
                                            if str(data_cus_new['user_type']).strip() == 'VIP_SUPPER_KHDN':
                                                _bot.send_mess(mess, BOT_GROUP_VIP_KHDN_ID)
                                data_cus_old_lst = data_flt_new_lst
                            elif data_flt_new_lst:
                                data_cus_old_lst = data_flt_new_lst

                        except Exception as err:
                            print("error when scan soc")
                            print(err)

                    print("Da chay SOC xong")
                    frst_run += 1
                    time.sleep(interval)

            except Exception as err:
                print("error when scan soc")
                print(err)
                frst_run += 1
                log_scan.create_log('critical', err)
                time.sleep(interval)
                ##

    def stop(self):
        self.is_stop = True


def plus_time_sec(tme, num):
    import datetime
    return tme + datetime.timedelta(seconds=num)


def convert_timedelta_to_second(time_delta):
    return time_delta.total_seconds()


if __name__ == '__main__':
    _tbl = ScanBotSocThread(False, 100, 'kv1', None)
    _tbl.start()
    pass