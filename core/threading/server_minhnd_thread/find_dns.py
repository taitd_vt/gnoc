from dns import resolver

res = resolver.Resolver()
res.nameservers = ['8.8.8.8']

answers_ipv6 = res.query('google.com', 'AAAA')
answers_ipv4 = res.query('google.com', 'A')

for rdata in answers_ipv4:
    print (rdata.address)
for rdata in answers_ipv6:
    print (rdata.address)