__author__ = 'vtn-dhml-dhip10'
import threading
import time

from core.log.logger.logger import Logging
from core.threading.device_thread.dev_scan_backhaul_upstream_lst import DeviceScanBackhaulUpstreamListThread
from core.helpers.list_helpers import chunkIt, get_pop_lst
from core.database.impl.server_minhnd.tbl_deny_web_impl import TblDenyWeb
from core.helpers.date_helpers import get_date_now_format_elastic


class ScanWebConnectListThread(threading.Thread):
    def __init__(self, is_stop, web_lst):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.web_lst = web_lst
        self.thread_name = 'Scan Web List can visit normally ?'

    def run(self):

        while not self.is_stop:
            log_scan = Logging('Scan Web List can visit normally ?', 'Thread Scan Web can visit normally ?')
            print("Thread test web connect list")
            try:
                time_now = get_time_format_now()

                hour_now = str(time_now.hour)
                minute_now = str(time_now.minute)
                second_now = str(time_now.second)
                year_now = str(time_now.year)
                month_now = str(time_now.month)
                day_now = str(time_now.day)
                date_format = '{0}_{1}_{2}_{3}_{4}_{5}'.format(year_now, month_now, day_now, hour_now, minute_now,
                                                               second_now)
                name_log = 'log_scan_connect_web_' + date_format + '.txt'
                log_scan = Logging(name_log, 'Thread Scan Web List can visit normally ?')
                log_scan.create_log('info', 'Start getting data Scan Web can visit normally ?')

                # scan auto
                time_ins = get_date_now_format_elastic()
                _obj_impl = TblDenyWeb(web="tester",
                                       usr="",
                                       cv="cv",
                                       usr_order="usr",
                                       timestamp=time_ins,
                                       usr_acc="acc",
                                       time_acc=time_ins,
                                       status=0,
                                       time_check=time_ins,
                                       deny_or_not='',
                                       status_run=''
                                       )

                for y in self.web_lst:
                    try:
                        res_con = _obj_impl.conn_web(y['web'])
                        if res_con:
                            stat = 0
                        else:
                            stat = 1
                        # update result
                        _obj_y = _obj_impl.find(y['web'])
                        if _obj_y:
                            if 'usr_acc' not in _obj_y:
                                _obj_y['usr_acc'] = ''
                            if 'usr_order' not in _obj_y:
                                _obj_y['usr_order'] = ''
                            if 'cv' not in _obj_y:
                                _obj_y['cv'] = ''
                            if 'usr' not in _obj_y:
                                _obj_y['usr'] = ''
                            if 'time_acc' not in _obj_y:
                                _obj_y['time_acc'] = None
                            if 'status_run' not in _obj_y:
                                _obj_y['status_run'] = ''
                            if 'deny_or_not' not in _obj_y:
                                _obj_y['deny_or_not'] = 'WAITING'
                            _obj_y_impl = TblDenyWeb(web=y['web'],
                                                     usr=_obj_y['usr'],
                                                     cv=_obj_y['cv'],
                                                     usr_order=_obj_y['usr_order'],
                                                     timestamp=_obj_y['timestamp'],
                                                     usr_acc=_obj_y['usr_acc'],
                                                     time_acc=_obj_y['time_acc'],
                                                     status=stat,
                                                     time_check=get_date_now_format_elastic(),
                                                     deny_or_not=_obj_y['deny_or_not'],
                                                     status_run=_obj_y['status_run'])
                            res_save = _obj_y_impl.save()
                            print("Save web %s result is %s" % (y['web'], str(res_save)))
                            time.sleep(1)
                    except Exception as err:
                        print("Error %s when check web %s" % (str(err), y['web']))
                print("Finish Thread test web connect list")
            except Exception as err:
                print(err)
                log_scan.create_log('critical', err)
            self.stop()

    def stop(self):
        self.is_stop = True


def get_time_format_now():
    import datetime
    return datetime.datetime.now()


def plus_time_sec(tme, num):
    import datetime
    return tme + datetime.timedelta(seconds=num)


def convert_timedelta_to_second(time_delta):

    return time_delta.total_seconds()


if __name__ == '__main__':
    test = ScanWebConnectListThread(False, ['viettan.org', 'duocviet.org', 'nghiepdoanbaochi.org'])
    test.run()
    pass
