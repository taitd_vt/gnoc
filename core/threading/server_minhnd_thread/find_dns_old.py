import socket
import csv
import time

def nslookup_web(web_name):
    res_ip_list = []
    web_name = web_name.strip()
    try:
        for x in range(0, 10):
            ip_list = []
            ais = socket.getaddrinfo(web_name, 0, 0, 0, 0)
            for result in ais:
                ip_list.append(result[-1][0])
            ip_list = list(set(ip_list))
            for res_ip in ip_list:
                if res_ip not in res_ip_list:
                    res_ip_list.append(res_ip)
            time.sleep(2)
    except Exception as err:
        print("Error %s when check nslookup %s" % (err, web_name))

    return res_ip_list


test = nslookup_web('google.com')
print(test)

