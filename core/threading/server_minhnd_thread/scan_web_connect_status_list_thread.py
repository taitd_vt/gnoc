__author__ = 'vtn-dhml-dhip10'
import threading
import time

from core.log.logger.logger import Logging
from core.threading.server_minhnd_thread.scan_web_connect_list_thread import ScanWebConnectListThread
from core.threading.server_minhnd_thread.scan_web_nslookup_list_thread import ScanWebNslookupListThread
from core.helpers.list_helpers import chunkIt, get_pop_lst
from core.database.impl.server_minhnd.tbl_deny_web_impl import TblDenyWeb
from core.helpers.date_helpers import get_date_now_format_elastic


class ScanWebConnectStatusListThread(threading.Thread):
    def __init__(self, is_stop, period_time):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.period_time = period_time
        self.thread_name = 'Scan Web can visit normally ?'

    def run(self):
        interval = self.period_time
        check_late = False
        time_scan = None
        time_now = get_time_format_now()
        time_next_interval = plus_time_sec(time_now, interval)
        while not self.is_stop:
            log_scan = Logging('Scan Web can visit normally ?', 'Thread Scan Web can visit normally ?')

            try:
                time_now = get_time_format_now()
                if check_late and time_scan:
                    time_now = time_scan
                    check_late = False

                time_next_interval = plus_time_sec(time_now, interval)
                hour_now = str(time_now.hour)
                minute_now = str(time_now.minute)
                second_now = str(time_now.second)
                year_now = str(time_now.year)
                month_now = str(time_now.month)
                day_now = str(time_now.day)
                date_format = '{0}_{1}_{2}_{3}_{4}_{5}'.format(year_now, month_now, day_now, hour_now, minute_now,
                                                               second_now)
                name_log = 'log_scan_connect_web_' + date_format + '.txt'
                log_scan = Logging(name_log, 'Thread Scan Web can visit normally ?')
                log_scan.create_log('info', 'Start getting data Scan Web can visit normally ?')

                # scan auto
                time_ins = get_date_now_format_elastic()
                _obj_impl = TblDenyWeb(web="tester",
                                       usr="",
                                       cv="cv",
                                       usr_order="usr",
                                       timestamp=time_ins,
                                       usr_acc="acc",
                                       time_acc=time_ins,
                                       status=0,
                                       time_check=time_ins,
                                       status_run='',
                                       deny_or_not='')

                web_lst = _obj_impl.find_web_status(".", 2)

                if web_lst:
                    web_lst_div = chunkIt(web_lst, 10)
                    for y_lst in web_lst_div:
                        scan_auto = ScanWebConnectListThread(False, y_lst)
                        scan_auto.start()

                        #scan_nslookup = ScanWebNslookupListThread(False, y_lst)
                        #scan_nslookup.start()
                time_later = get_time_format_now()
                time_diff = time_later - time_now
                time_diff_second = convert_timedelta_to_second(time_diff)

                print('Diff time scan:' + str(time_diff_second))
                if interval >= time_diff_second:
                    time_sleep = interval - time_diff_second
                    print('Sleeping for next ' + str(time_sleep) + " s")
                    if time_sleep > 0:
                        time.sleep(time_sleep)
                else:
                    check_late = True
                    time_scan = time_next_interval

            except Exception as err:
                print(err)
                time_later = get_time_format_now()
                time_diff = time_later - time_now
                time_diff_second = convert_timedelta_to_second(time_diff)

                print('Diff time:' + str(time_diff_second))
                if interval >= time_diff_second:
                    time_sleep = interval - time_diff_second
                    print('Sleeping for ' + str(time_sleep) + " s")
                    if time_sleep > 0:
                        time.sleep(time_sleep)
                else:
                    check_late = True
                    time_scan = time_next_interval

                log_scan.create_log('critical', err)

    def stop(self):
        self.is_stop = True


def get_time_format_now():
    import datetime
    return datetime.datetime.now()


def plus_time_sec(tme, num):
    import datetime
    return tme + datetime.timedelta(seconds=num)


def convert_timedelta_to_second(time_delta):

    return time_delta.total_seconds()


if __name__ == '__main__':
    test = ScanWebConnectStatusListThread(False, 60 * 60 * 24)
    test.run()
    pass
