__author__ = 'vtn-dhml-dhip10'
import threading
import time

from core.log.logger.logger import Logging
from core.database.impl.bot.bot_impl import BotImpl
from core.threading.device_thread.dev_scan_backhaul_upstream_lst import DeviceScanBackhaulUpstreamListThread
from core.helpers.list_helpers import chunkIt, get_pop_lst
from core.task.send_email_viettel import SendEmailViettel
from core.database.impl.server_minhnd.tbl_deny_web_look_up_impl import TblDenyWebLookUp
from core.database.impl.server_minhnd.tbl_deny_web_impl import TblDenyWeb
from core.helpers.date_helpers import get_date_now_format_elastic
from core.helpers.stringhelpers import convert_web_name
from config import Development

config = Development()
BOT_DENY_WEB = config.__getattribute__('BOT_DENY_WEB')


class ScanWebNslookupListThread(threading.Thread):
    def __init__(self, is_stop, scan_new_day, web_lst, bot):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.web_lst = web_lst
        self.scan_new_day = scan_new_day
        self.thread_name = 'Scan Web List can visit normally ?'
        self.bot = bot

    def run(self):
        _bot = BotImpl(self.bot)
        while not self.is_stop:
            log_scan = Logging('Scan Web List can visit normally ?', 'Thread Scan Web can visit normally ?')
            print("Start Nslookup List Thread")
            try:
                time_now = get_time_format_now()

                hour_now = str(time_now.hour)
                minute_now = str(time_now.minute)
                second_now = str(time_now.second)
                year_now = str(time_now.year)
                month_now = str(time_now.month)
                day_now = str(time_now.day)
                date_format = '{0}_{1}_{2}_{3}_{4}_{5}'.format(year_now, month_now, day_now, hour_now, minute_now,
                                                               second_now)
                name_log = 'log_scan_connect_web_' + date_format + '.txt'
                log_scan = Logging(name_log, 'Thread Scan Web List can visit normally ?')
                log_scan.create_log('info', 'Start getting data Scan Web can visit normally ?')

                # scan auto
                time_ins = get_date_now_format_elastic()
                _obj_look_impl = TblDenyWebLookUp(web='',
                                                  ip_addr='',
                                                  timestamp=get_date_now_format_elastic(),
                                                  as_name='',
                                                  as_num='',
                                                  area='',
                                                  document='',
                                                  status='',
                                                  check_rr='',
                                                  check_dns='',
                                                  timestamp_check_rr=None,
                                                  timestamp_check_dns=None)

                res_send_emal = []
                look_up = 0
                for y in self.web_lst:
                    try:
                        # vi ly do chi nslookup duoc ngoai http va https va cac subdomain nen phai regex lai web name
                        web_name_new = convert_web_name(y['web'])
                        doc_web = y['cv']
                        if doc_web not in res_send_emal:
                            res_send_emal.append(doc_web)

                        print("Looking up web " + str(web_name_new))
                        res_web_lst = _obj_look_impl.find(web_name_new)
                        res_ip_lst = []
                        res_ip_doc_lst = []
                        if res_web_lst:
                            for res_web in res_web_lst:
                                if 'ip_addr' in res_web:
                                    ip_addr = res_web['ip_addr']
                                    if ip_addr not in res_ip_lst:
                                        res_ip_lst.append(ip_addr)
                                    docm = ''
                                    if 'document' in res_web:
                                        docm = res_web['document']
                                    res_ip_doc_lst.append(dict(web=res_web['web'],
                                                               ip_addr=ip_addr,
                                                               document=docm))

                        res_look_lst, res_as_lst = _obj_look_impl.nsl_web(web_name_new)

                        if res_as_lst:
                            # so sanh voi database xem co trung nao khong ?
                            for res_look in res_as_lst:
                                res_ip_look = res_look['ip_addr']
                                if res_ip_look not in res_ip_lst:
                                    # append impl and post new
                                    look_up += 1
                                    if 'check_rr' not in y:
                                        y['check_rr'] = ''
                                    if 'check_dns' not in y:
                                        y['check_dns'] = ''
                                    if 'timestamp_check_rr' not in y:
                                        y['timestamp_check_rr'] = None
                                    if 'timestamp_check_dns' not in y:
                                        y['timestamp_check_dns'] = None

                                    _obj_y_impl = TblDenyWebLookUp(web=y['web'],
                                                                   ip_addr=res_ip_look,
                                                                   timestamp=get_date_now_format_elastic(),
                                                                   as_name=res_look['as_name'],
                                                                   as_num=res_look['as_num'],
                                                                   area=res_look['area'],
                                                                   document=doc_web,
                                                                   status='runned',
                                                                   check_rr=y['check_rr'],
                                                                   check_dns=y['check_dns'],
                                                                   timestamp_check_rr=y['timestamp_check_rr'],
                                                                   timestamp_check_dns=y['timestamp_check_dns'])
                                    res_save = _obj_y_impl.save()
                                    print("Save nslookup web %s result is %s" % (y['web'], str(res_save)))
                                else:
                                    # do co tinh trang cong van trung nhau danh sach web nen update luon cong van khac.
                                    # neu la ip cu ma cong van moi thi cung update web
                                    chck_res_ip_doc = False
                                    for res_ip_doc in res_ip_doc_lst:
                                        if y['web'] == res_ip_doc['web'] and res_ip_look == res_ip_doc['ip_addr'] \
                                                and y['cv'] == res_ip_doc['document']:
                                            chck_res_ip_doc = True
                                            break
                                    if not chck_res_ip_doc:
                                        look_up += 1
                                        if 'check_rr' not in y:
                                            y['check_rr'] = ''
                                        if 'check_dns' not in y:
                                            y['check_dns'] = ''
                                        if 'timestamp_check_rr' not in y:
                                            y['timestamp_check_rr'] = None
                                        if 'timestamp_check_dns' not in y:
                                            y['timestamp_check_dns'] = None

                                        _obj_y_impl = TblDenyWebLookUp(web=y['web'],
                                                                       ip_addr=res_ip_look,
                                                                       timestamp=get_date_now_format_elastic(),
                                                                       as_name=res_look['as_name'],
                                                                       as_num=res_look['as_num'],
                                                                       area=res_look['area'],
                                                                       document=doc_web,
                                                                       status='runned',
                                                                       check_rr=y['check_rr'],
                                                                       check_dns=y['check_dns'],
                                                                       timestamp_check_rr=y['timestamp_check_rr'],
                                                                       timestamp_check_dns=y['timestamp_check_dns'])
                                        res_save = _obj_y_impl.save()
                                        print("Save nslookup web new document %s result is %s" % (y['web'], str(res_save)))

                                _obj_deny = TblDenyWeb(web=y['web'],
                                                       usr=y['usr'],
                                                       cv=y['cv'],
                                                       usr_order=y['usr_order'],
                                                       timestamp=y['timestamp'],
                                                       usr_acc=y['usr_acc'],
                                                       time_acc=y['time_acc'],
                                                       status=y['status'],
                                                       time_check=y['time_check'],
                                                       deny_or_not=y['deny_or_not'],
                                                       status_run='runned')
                                _obj_deny.save()

                        time.sleep(1)
                    except Exception as err:
                        print("Error %s when check web %s" % (str(err), y['web']))
                        # khi loi check thi cap nhat trang thai la runned luon
                        _obj_deny = TblDenyWeb(web=y['web'],
                                               usr=y['usr'],
                                               cv=y['cv'],
                                               usr_order=y['usr_order'],
                                               timestamp=y['timestamp'],
                                               usr_acc=y['usr_acc'],
                                               time_acc=y['time_acc'],
                                               status=y['status'],
                                               time_check=y['time_check'],
                                               deny_or_not=y['deny_or_not'],
                                               status_run='runned error')
                        _obj_deny.save()
                print("Finish nslookup web short")
                # neu la loai lien tuc quet co ket qua moi se gui mail tu dong toi fo_ip
                if self.scan_new_day:
                    if look_up > 0:
                        cont = "Finish scan ip of web in documents:" + str(res_send_emal) + " with " + str(look_up) + \
                               " results."
                        obj_send_email = SendEmailViettel(subject="Lookup IP Address of Web " + str(res_send_emal),
                                                          content=cont)
                        # Tam thoi bo gui mail
                        #obj_send_email.send_email()
                        _bot.send_mess(cont, BOT_DENY_WEB)
                        look_up = 0
                        res_send_emal = []

            except Exception as err:
                print(err)
                log_scan.create_log('critical', err)
            print("Finish checking nslookup list thread")
            self.stop()

    def stop(self):
        self.is_stop = True


def get_time_format_now():
    import datetime
    return datetime.datetime.now()


def plus_time_sec(tme, num):
    import datetime
    return tme + datetime.timedelta(seconds=num)


def convert_timedelta_to_second(time_delta):
    return time_delta.total_seconds()


if __name__ == '__main__':
    test = ScanWebNslookupListThread(False, ['viettan.org', 'duocviet.org', 'nghiepdoanbaochi.org'])
    test.run()
    pass
