__author__ = 'vtn-dhml-dhip10'
import threading
import time
import datetime
from core.log.logger.logger import Logging
from core.threading.server_minhnd_thread.scan_web_connect_list_thread import ScanWebConnectListThread
from core.threading.server_minhnd_thread.scan_web_nslookup_list_thread import ScanWebNslookupListThread
from core.helpers.list_helpers import chunkIt, get_pop_lst
from core.database.impl.server_minhnd.tbl_deny_web_impl import TblDenyWeb
from core.helpers.stringhelpers import convert_web_name
from core.task.send_email_viettel import SendEmailViettel
from core.database.impl.server_minhnd.tbl_deny_web_look_up_impl import TblDenyWebLookUp
from core.database.impl.server_minhnd.tbl_deny_web_whitelist import TblDeynyWebWhitelistImpl
from core.helpers.date_helpers import get_date_now_format_elastic, get_date_now_only_format_ipms
from core.database.impl.bot.bot_impl import BotImpl
from config import Development

config = Development()
BOT_DENY_WEB = config.__getattribute__('BOT_DENY_WEB')


class ScanWebNslookupThread(threading.Thread):
    def __init__(self, is_stop, period_time, scan_new_day, bot):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.period_time = period_time
        self.scan_new_day = scan_new_day
        self.thread_name = 'Scan Web can visit normally ?'
        self.bot = bot

    def run(self):
        interval = self.period_time
        check_late = False
        time_scan = None
        time_now = get_time_format_now()
        time_ipms_now = get_date_now_only_format_ipms()
        time_hour_run = -1
        _bot = BotImpl(self.bot)
        _tbl_deny_whte_impl = TblDeynyWebWhitelistImpl()
        while not self.is_stop:

            print("Start scan nslookup thread nslookup " + str(time_now))
            try:
                tbl_whte_lst = _tbl_deny_whte_impl.get_web_lst()
                # scan auto
                time_ins = get_date_now_format_elastic()
                _obj_impl = TblDenyWeb(web="tester",
                                       usr="",
                                       cv="cv",
                                       usr_order="usr",
                                       timestamp=time_ins,
                                       usr_acc="acc",
                                       time_acc=time_ins,
                                       status=0,
                                       time_check=get_time_format_now(),
                                       deny_or_not='WAITING',
                                       status_run='')
                if self.scan_new_day:
                    web_lst = _obj_impl.find_time(time_ipms_now)

                else:
                    web_lst = _obj_impl.lst()

                print("Tong trang web trong ngay:" + str(len(web_lst)))

                # scan_nslookup = ScanWebNslookupListThread(False, web_lst)
                # scan_nslookup.start()
                _obj_look_impl = TblDenyWebLookUp(web="tester",
                                                  ip_addr='',
                                                  timestamp=time_ins,
                                                  document='',
                                                  as_name='',
                                                  as_num='',
                                                  area='',
                                                  status='',
                                                  check_rr='',
                                                  check_dns='',
                                                  timestamp_check_rr=None,
                                                  timestamp_check_dns=None)
                res_send_emal = []
                look_up = 0
                # theo yeu cau moi can cap nhat danh sach. Ta se tim nhung status nao = '' hoac not run chay truoc
                # sau do lay ra thang nao status nhu the cho chay, danh dau tag la running. Chay xong oanh dau runned
                web_nvr_run = []
                web_running = []
                web_runned = []
                for y in web_lst:
                    stts = ''
                    usr_acc = ''
                    if 'usr_acc' not in y:
                        y['usr_acc'] = ''
                    if 'time_acc' not in y:
                        y['time_acc'] = None
                    if 'usr_order' not in y:
                        y['usr_order'] = ''
                    if 'status_run' not in y:
                        y['status_run'] = ''
                    if 'status' not in y:
                        y['status'] = 0
                    if 'time_check' not in y:
                        y['time_check'] = None
                    if 'deny_or_not' not in y:
                        y['deny_or_not'] = None
                    if 'check_rr' not in y:
                        y['check_rr'] = ''
                    if 'check_dns' not in y:
                        y['check_dns'] = ''
                    if 'timestamp_check_rr' not in y:
                        y['timestamp_check_rr'] = None
                    if 'timestamp_check_dns' not in y:
                        y['timestamp_check_dns'] = None
                    if 'usr' not in y:
                        y['usr'] = ''

                    web_name = y['web']
                    chck_whte = True
                    name_dngr_web = ''
                    for x in tbl_whte_lst:
                        if web_name.find(x['web']) >= 0:
                            chck_whte = False
                            name_dngr_web = x['web']
                            break
                    if not chck_whte:
                        _obj_deny = TblDenyWeb(web=y['web'],
                                               usr=y['usr'],
                                               cv=y['cv'],
                                               usr_order=y['usr_order'],
                                               timestamp=y['timestamp'],
                                               usr_acc=y['usr_acc'],
                                               time_acc=y['time_acc'],
                                               status=y['status'],
                                               time_check=y['time_check'],
                                               deny_or_not=y['deny_or_not'],
                                               status_run='Web is in dangerous list' + str(name_dngr_web))
                        _obj_deny.save()
                        _obj_y_impl = TblDenyWebLookUp(web=y['web'],
                                                       ip_addr='Web is in dangerous list' + str(name_dngr_web),
                                                       timestamp=get_date_now_format_elastic(),
                                                       as_name='',
                                                       as_num='',
                                                       area='',
                                                       document=y['cv'],
                                                       status='runned',
                                                       check_rr=y['check_rr'],
                                                       check_dns=y['check_dns'],
                                                       timestamp_check_rr=y['timestamp_check_rr'],
                                                       timestamp_check_dns=y['timestamp_check_dns'])
                        res_save = _obj_y_impl.save()
                    else:
                        if 'status' in y:
                            stts = y['status_run']
                            if stts == '':
                                if self.scan_new_day:
                                    stts = 'running'
                                # kiem tra co nam trong white list web khong?

                                web_nvr_run.append(y)
                                # update status to running
                                _obj_deny = TblDenyWeb(web=y['web'],
                                                       usr=y['usr'],
                                                       cv=y['cv'],
                                                       usr_order=y['usr_order'],
                                                       timestamp=y['timestamp'],
                                                       usr_acc=y['usr_acc'],
                                                       time_acc=y['time_acc'],
                                                       status=y['status'],
                                                       time_check=y['time_check'],
                                                       deny_or_not=y['deny_or_not'],
                                                       status_run=stts)
                                _obj_deny.save()
                            elif stts == 'running':
                                pass
                            else:
                                if self.scan_new_day:
                                    stts = 'running'
                                web_runned.append(y)
                                # chuyen trang thai ve running roi chay tip
                                # do 1h moi chay 1 lan cac trang web da chay nen can phai gan lai khi trung yeu cau

                        else:
                            if self.scan_new_day:
                                stts = 'running'
                            web_nvr_run.append(y)
                            _obj_deny = TblDenyWeb(web=y['web'],
                                                   usr=y['usr'],
                                                   cv=y['cv'],
                                                   usr_order=y['usr_order'],
                                                   timestamp=y['timestamp'],
                                                   usr_acc=y['usr_acc'],
                                                   time_acc=y['time_acc'],
                                                   status=y['status'],
                                                   time_check=y['time_check'],
                                                   deny_or_not=y['deny_or_not'],
                                                   status_run=stts)
                            _obj_deny.save()
                if web_runned:
                    # do so luong web runned se nhieu. Nen chi cho web runned chay tu phut 10 den 15 thoi
                    time_now = get_time_format_now()
                    if not self.scan_new_day:
                        _scan_web_nsl_up_lst_1 = ScanWebNslookupListThread(False, self.scan_new_day, web_runned,
                                                                           self.bot)
                        _scan_web_nsl_up_lst_1.start()
                        for y in web_runned:
                            _obj_deny = TblDenyWeb(web=y['web'],
                                                   usr=y['usr'],
                                                   cv=y['cv'],
                                                   usr_order=y['usr_order'],
                                                   timestamp=y['timestamp'],
                                                   usr_acc=y['usr_acc'],
                                                   time_acc=y['time_acc'],
                                                   status=y['status'],
                                                   time_check=y['time_check'],
                                                   deny_or_not=y['deny_or_not'],
                                                   status_run=y['status_run'])
                            _obj_deny.save()
                    elif 55 >= time_now.minute >= 15:
                        if time_now.hour != time_hour_run:
                            print("Start nslookup web runned for " + str(len(web_runned)) + " webs.")
                            _scan_web_nsl_up_lst_1 = ScanWebNslookupListThread(False, self.scan_new_day, web_runned,
                                                                               self.bot)
                            _scan_web_nsl_up_lst_1.start()
                            time_hour_run = time_now.hour
                            for y in web_runned:
                                _obj_deny = TblDenyWeb(web=y['web'],
                                                       usr=y['usr'],
                                                       cv=y['cv'],
                                                       usr_order=y['usr_order'],
                                                       timestamp=y['timestamp'],
                                                       usr_acc=y['usr_acc'],
                                                       time_acc=y['time_acc'],
                                                       status=y['status'],
                                                       time_check=y['time_check'],
                                                       deny_or_not=y['deny_or_not'],
                                                       status_run='running')
                                _obj_deny.save()

                if web_nvr_run:
                    print("Start nslookup web never run for " + str(len(web_nvr_run)) + " webs.")
                    _scan_web_nsl_up_lst_2 = ScanWebNslookupListThread(False, self.scan_new_day, web_nvr_run, self.bot)
                    _scan_web_nsl_up_lst_2.start()
                print("Sleep nslookup web for " + str(self.period_time) + " seconds.")
                time.sleep(self.period_time)

            except Exception as err:
                print(err)

    def stop(self):
        self.is_stop = True


def get_time_format_now():
    import datetime
    return datetime.datetime.now()


def plus_time_sec(tme, num):
    import datetime
    return tme + datetime.timedelta(seconds=num)


def convert_timedelta_to_second(time_delta):
    return time_delta.total_seconds()


if __name__ == '__main__':
    test = ScanWebNslookupThread(False, 60 * 1, True, None)
    test.run()
    pass
