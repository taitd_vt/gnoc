__author__ = 'vtn-dhml-dhip10'
import threading
import time
from core.helpers.date_helpers import get_date_minus_format_ipms, get_date_minus_format_elk
from core.database.impl.ipms.syslog_nocpro_impl import SyslogNocproIpmsImpl
from core.log.logger.logger import Logging
from core.threading.server_minhnd_thread.scan_web_connect_list_thread import ScanWebConnectListThread
from core.threading.server_minhnd_thread.scan_web_nslookup_list_thread import ScanWebNslookupListThread
from core.helpers.list_helpers import chunkIt, get_pop_lst
from core.database.impl.server_minhnd.tbl_kpi_server_impl import TblKpiServerImpl
from core.database.impl.server_minhnd.tbl_interface_drop_server_impl import TblInterfaceDrpoServerImpl
from core.database.impl.server_minhnd.tbl_useronline_bras_impl import TblUseronlineBrasImpl
from core.database.impl.elasticsearch.tbl_intf_stat_impl import TblKibana
from core.helpers.date_helpers import get_date_now_format_elastic
from core.database.impl.bot.bot_impl import BotImpl
from config import Development
config = Development()
BOT_GROUP_CHECK_THREAD_ID = config.__getattribute__('BOT_GROUP_CHECK_THREAD_ID')
BOT_GROUP_VIP_KHDN_ID = config.__getattribute__('BOT_GROUP_VIP_KHDN_ID')


class ScanServerWorkThread(threading.Thread):
    def __init__(self, is_stop, period_time, bot):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.period_time = period_time
        self.thread_name = 'Scan Server work normally ?'
        self.bot = bot

    def run(self):
        # interval = seconds
        interval = self.period_time
        check_late = False
        time_scan = None
        time_now = get_time_format_now()
        time_ago = get_date_minus_format_ipms(time_now, int(interval / 60))
        time_30_days_ago = get_date_minus_format_ipms(time_now, 60 * 48 * 15)
        time_60_days_ago = get_date_minus_format_ipms(time_now, 60 * 48 * 30)
        _tbl_kpi = TblKpiServerImpl(0, '', 0, '', '', '', None, 0, 0, 0, 0, '')
        _tbl_drop = TblInterfaceDrpoServerImpl()
        _tbl_usr = TblUseronlineBrasImpl(0 , 0, 0, '', 0, '', None, '', '', '')
        _tbl_kbn = TblKibana()
        _bot = BotImpl(self.bot)

        while not self.is_stop:
            time_now = get_time_format_now()
            time_ago = get_date_minus_format_ipms(time_now, int(interval / 60))
            time_30_days_ago = get_date_minus_format_ipms(time_now, 60 * 48 * 15)
            time_60_days_ago = get_date_minus_format_ipms(time_now, 60 * 48 * 30)
            print("Start scan server working ?")
            try:
                # scan KPI
                res_kpi = _tbl_kpi.count_all_greater(time_ago)
                res_del = _tbl_kpi.delete_all(time_30_days_ago)
                # scan drop
                res_drop = _tbl_drop.count_all_greater(time_ago)
                res_drop_del = _tbl_kpi.delete_all(time_60_days_ago)
                # scan useronline BRAS
                res_usr = _tbl_usr.count_all_greater(time_ago)
                res_usr_del = _tbl_usr.delete_all(time_60_days_ago)
                # scan kibana
                res_kbn = _tbl_kbn.is_capacity_from_last(int(interval / 60))
                if res_kpi > 0 and res_drop > 0 and res_usr > 0 and res_kbn:
                    _bot.send_mess("Everything is fine", BOT_GROUP_CHECK_THREAD_ID)
                    print("Every thing is fine")
                elif res_kpi <= 0:
                    _bot.send_mess("KPI is problem", BOT_GROUP_CHECK_THREAD_ID)
                    print("KPI is problem")
                elif res_drop <= 0:
                    _bot.send_mess("Drop thread is problem", BOT_GROUP_CHECK_THREAD_ID)
                    print("Drop is problem")
                elif res_usr <= 0:
                    _bot.send_mess("Useronline BRAS is problem", BOT_GROUP_CHECK_THREAD_ID)
                    print("Useronline is problem")
                else:
                    _bot.send_mess("Kibana is problem", BOT_GROUP_CHECK_THREAD_ID)
                # Kiem tra trang thai ket noi syslog cua Kibana 8688 co ok khong ?
                time_last = get_date_minus_format_elk(time_now, 60 * 7 + 6 * 60)

                for _area in ['KV1', 'KV2', 'KV3']:
                    _obj = SyslogNocproIpmsImpl(_area)
                    _host_lst = _obj.find_cbs(time_last, "FOIP&error", True, True)
                    if len(_host_lst) == 0:
                        _host_over_lst = _obj.find_cbs(time_last, "FOIP&over threshold", True, True)
                        if len(_host_over_lst) == 0:
                            _bot.send_mess("Spring 8688 of over threshold and CRC of " + _area + " is problem",
                                           BOT_GROUP_CHECK_THREAD_ID)

                print("Finish Thread test Server connect list")

            except Exception as err:
                print(err)
            time.sleep(interval)

    def stop(self):
        self.is_stop = True


def get_time_format_now():
    import datetime
    return datetime.datetime.now()


def plus_time_sec(tme, num):
    import datetime
    return tme + datetime.timedelta(seconds=num)


def convert_timedelta_to_second(time_delta):

    return time_delta.total_seconds()


if __name__ == '__main__':
    import telebot
    BOT_ID = config.__getattribute__('BOT_ID')
    TELEBOT_BOT_TOKEN = BOT_ID
    bot = telebot.AsyncTeleBot(BOT_ID)
    test = ScanServerWorkThread(False, 60 * 60 * 4, bot)
    test.start()
    pass
