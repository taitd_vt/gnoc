__author__ = 'vtn-dhml-dhip10'
import threading
import time

from core.log.logger.logger import Logging
from core.threading.device_thread.dev_scan_intf_lst import DeviceScanInterfaceListThread
from core.helpers.list_helpers import chunkIt, get_pop_lst, get_pop_manual_lst


class ScanDeviceInterfaceLstThread(threading.Thread):
    def __init__(self, is_stop, period_time):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.period_time = period_time
        self.thread_name = 'ScanDeviceInterfaceList'

    def run(self):
        interval = self.period_time
        check_late = False
        time_scan = None
        time_now = get_time_format_now()
        time_next_interval = plus_time_sec(time_now, interval)
        while not self.is_stop:
            log_scan = Logging('Scan Interface List', 'Thread get data of interface of device')

            try:
                time_now = get_time_format_now()
                if check_late and time_scan:
                    time_now = time_scan
                    check_late = False

                time_next_interval = plus_time_sec(time_now, interval)
                hour_now = str(time_now.hour)
                minute_now = str(time_now.minute)
                second_now = str(time_now.second)
                year_now = str(time_now.year)
                month_now = str(time_now.month)
                day_now = str(time_now.day)
                date_format = '{0}_{1}_{2}_{3}_{4}_{5}'.format(year_now, month_now, day_now, hour_now, minute_now,
                                                               second_now)
                name_log = 'log_scan_intf_dev_' + date_format + '.txt'
                log_scan = Logging(name_log, 'Thread get data of interface of device')
                log_scan.create_log('info', 'Start getting data of interface of device')

                pop_lst = list()
                pop_mnl_lst = get_pop_manual_lst()
                if pop_mnl_lst:
                    dev_mnl_scan = DeviceScanInterfaceListThread(False, time_now, pop_mnl_lst, "thunv5", "Viettel#321#")
                    dev_mnl_scan.start()

                pop_auto_lst = get_pop_lst()
                # divide to 3

                dev_scan = DeviceScanInterfaceListThread(False, time_now, pop_auto_lst, "dungnd18", "dungnd18@123")
                dev_scan.start()

                time_later = get_time_format_now()
                time_diff = time_later - time_now
                time_diff_second = convert_timedelta_to_second(time_diff)

                print('Diff time scan:' + str(time_diff_second))
                if interval >= time_diff_second:
                    time_sleep = interval - time_diff_second
                    print('Sleeping for next ' + str(time_sleep) + " s")
                    time.sleep(time_sleep)
                else:
                    check_late = True
                    time_scan = time_next_interval

            except Exception as err:
                print(err)
                time_later = get_time_format_now()
                time_diff = time_later - time_now
                time_diff_second = convert_timedelta_to_second(time_diff)

                print('Diff time:' + str(time_diff_second))
                if interval >= time_diff_second:
                    time_sleep = interval - time_diff_second
                    print('Sleeping for ' + str(time_sleep) + " s")
                    time.sleep(time_sleep)
                else:
                    check_late = True
                    time_scan = time_next_interval

                log_scan.create_log('critical', err)

    def stop(self):
        self.is_stop = True


def get_time_format_now():
    import datetime
    return datetime.datetime.now()


def plus_time_sec(tme, num):
    import datetime
    return tme + datetime.timedelta(seconds=num)


def convert_timedelta_to_second(time_delta):

    return time_delta.total_seconds()


if __name__ == '__main__':
    test = ScanDeviceInterfaceLstThread(False, 60 * 60 * 12)
    test.run()
    pass
