__author__ = 'vtn-dhml-dhip10'
import threading
import time

from core.log.logger.logger import Logging
from core.threading.device_thread.dev_scan_backhaul_upstream_lst import DeviceScanBackhaulUpstreamListThread
from core.helpers.list_helpers import chunkIt, get_pop_lst


class ScanBackhaulUpstreamThread(threading.Thread):
    def __init__(self, is_stop, period_time):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.period_time = period_time
        self.thread_name = 'ScanBackhaulUpstream'

    def run(self):
        interval = self.period_time
        check_late = False
        time_scan = None
        time_now = get_time_format_now()
        time_next_interval = plus_time_sec(time_now, interval)
        while not self.is_stop:
            log_scan = Logging('Scan Backhaul', 'Thread get data of Backhaul and Upstream')

            try:
                time_now = get_time_format_now()
                if check_late and time_scan:
                    time_now = time_scan
                    check_late = False

                time_next_interval = plus_time_sec(time_now, interval)
                hour_now = str(time_now.hour)
                minute_now = str(time_now.minute)
                second_now = str(time_now.second)
                year_now = str(time_now.year)
                month_now = str(time_now.month)
                day_now = str(time_now.day)
                date_format = '{0}_{1}_{2}_{3}_{4}_{5}'.format(year_now, month_now, day_now, hour_now, minute_now,
                                                               second_now)
                name_log = 'log_scan_backhaul_' + date_format + '.txt'
                log_scan = Logging(name_log, 'Thread get data of Backhaul and Upstream')
                log_scan.create_log('info', 'Start getting data Backhaul and Upstream')

                hst_mnl_lst = ['HHT9602CRT24.IGW.ASR22.04', 'HCM_IW2_ASR9K_HHTN4', 'HKH9103CRT13.DGW.ASR12.01']
                dev_mnl = DeviceScanBackhaulUpstreamListThread(False, time_now, 'manual', hst_mnl_lst,'cisco')
                dev_mnl.start()

                # scan auto
                pop_lst = get_pop_lst()

                if pop_lst:
                    pop_lst_div = chunkIt(pop_lst, 3)
                    for y in pop_lst_div:
                        dev_auto = DeviceScanBackhaulUpstreamListThread(False, time_now, 'auto', y, 'cisco')
                        dev_auto.start()

                time_later = get_time_format_now()
                time_diff = time_later - time_now
                time_diff_second = convert_timedelta_to_second(time_diff)

                print('Diff time scan:' + str(time_diff_second))
                if interval >= time_diff_second:
                    time_sleep = interval - time_diff_second
                    print('Sleeping for next ' + str(time_sleep) + " s")
                    time.sleep(time_sleep)
                else:
                    check_late = True
                    time_scan = time_next_interval

            except Exception as err:
                print(err)
                time_later = get_time_format_now()
                time_diff = time_later - time_now
                time_diff_second = convert_timedelta_to_second(time_diff)

                print('Diff time:' + str(time_diff_second))
                if interval >= time_diff_second:
                    time_sleep = interval - time_diff_second
                    print('Sleeping for ' + str(time_sleep) + " s")
                    time.sleep(time_sleep)
                else:
                    check_late = True
                    time_scan = time_next_interval

                log_scan.create_log('critical', err)

    def stop(self):
        self.is_stop = True


def get_time_format_now():
    import datetime
    return datetime.datetime.now()


def plus_time_sec(tme, num):
    import datetime
    return tme + datetime.timedelta(seconds=num)


def convert_timedelta_to_second(time_delta):

    return time_delta.total_seconds()


if __name__ == '__main__':
    test = ScanBackhaulUpstreamThread(False, 60 * 7)
    test.run()
    pass
