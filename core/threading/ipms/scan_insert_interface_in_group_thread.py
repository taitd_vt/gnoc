__author__ = 'vtn-dhml-dhip10'
import threading
import time
from core.log.logger.logger import Logging
from core.database.impl.server_minhnd.tbl_insert_interface_group_impl import TblInsertInterfaceGroupImpl
from core.threading.device_thread.dev_scan_backhaul_upstream_lst import DeviceScanBackhaulUpstreamListThread
from core.helpers.list_helpers import chunkIt, get_pop_lst
from config import Development
from core.database.impl.forti_os.get_api_forti_os import FGT
from core.database.impl.elasticsearch.tbl_top_fw_impl import TblTopFwImpl
from core.helpers.date_helpers import get_date_minus_format_ipms, convert_date_to_ipms, \
    convert_date_to_elastic, get_time_format_now

config = Development()
FW_FTG_LST = config.__getattribute__('FW_FTG_LST')


class ScanInsertInterfaceInGroupThread(threading.Thread):
    def __init__(self, is_stop, period_time, area):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.period_time = period_time
        self.thread_name = 'Scan and Insert Interface In Group'
        self.area = area

    def run(self):
        interval = self.period_time
        check_late = False
        time_scan = None
        time_now = get_time_format_now()
        time_next_interval = plus_time_sec(time_now, interval)
        while not self.is_stop:
            log_scan = Logging('Scan And Insert Interface In Group', 'Thread scan and insert interface in Group')

            try:
                time_now = get_time_format_now()
                date_time_elastic_now = convert_date_to_elastic(time_now)
                if check_late and time_scan:
                    time_now = time_scan
                    check_late = False

                time_next_interval = plus_time_sec(time_now, interval)

                # scan auto
                _tbl_impl = TblInsertInterfaceGroupImpl(group_name='',
                                                        rule_description='',
                                                        area=self.area,
                                                        interface_not_contain='',
                                                        host_contain='',
                                                        interface_contain='',
                                                        group_host_name='',
                                                        interface_description='',
                                                        interface_des_not_contain='',
                                                        speed_greater_mbps=0,
                                                        speed_less_mbps=100000
                                                        )
                test = _tbl_impl.check_and_insert()

                time_later = get_time_format_now()
                time_diff = time_later - time_now
                time_diff_second = convert_timedelta_to_second(time_diff)
                print('Diff time scan:' + str(time_diff_second))
                if interval >= time_diff_second:
                    time_sleep = interval - time_diff_second
                    print('Sleeping for next ' + str(time_sleep) + " s")
                    time.sleep(time_sleep)
                else:
                    check_late = True
                    time_scan = time_next_interval

            except Exception as err:
                print(err)
                time_later = get_time_format_now()
                time_diff = time_later - time_now
                time_diff_second = convert_timedelta_to_second(time_diff)

                print('Diff time:' + str(time_diff_second))
                if interval >= time_diff_second:
                    time_sleep = interval - time_diff_second
                    print('Sleeping for ' + str(time_sleep) + " s")
                    time.sleep(time_sleep)
                else:
                    check_late = True
                    time_scan = time_next_interval

                log_scan.create_log('critical', err)

    def stop(self):
        self.is_stop = True


def plus_time_sec(tme, num):
    import datetime
    return tme + datetime.timedelta(seconds=num)


def convert_timedelta_to_second(time_delta):
    return time_delta.total_seconds()


if __name__ == '__main__':
    test = ScanInsertInterfaceInGroupThread(False, 60 * 5, 'KV1')
    test.start()
    pass
