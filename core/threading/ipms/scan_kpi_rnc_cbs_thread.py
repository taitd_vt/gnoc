__author__ = 'vtn-dhml-dhip10'
import threading
import time
from core.log.logger.logger import Logging
from core.database.impl.server_minhnd.tbl_insert_interface_group_impl import TblInsertInterfaceGroupImpl
from core.threading.device_thread.dev_scan_backhaul_upstream_lst import DeviceScanBackhaulUpstreamListThread
from core.helpers.list_helpers import chunkIt, get_pop_lst
from config import Development
from core.database.impl.forti_os.get_api_forti_os import FGT
from core.database.impl.elasticsearch.tbl_top_fw_impl import TblTopFwImpl
from core.helpers.date_helpers import get_date_minus_format_ipms, convert_date_to_ipms, \
    convert_date_to_elastic, get_time_format_now, convert_date_ipms_to_date_obj, get_date_minus_format_ipms
from core.database.impl.server_minhnd.tbl_useronline_bras_impl import TblUseronlineBrasImpl
from core.database.impl.server_minhnd.tbl_rnc_cbs_impl import TblRncCbsServerImpl
config = Development()
BOT_GROUP_BSS_ID = config.__getattribute__('BOT_GROUP_BSS_ID')


class ScanKpiRncCbsThread(threading.Thread):
    def __init__(self, is_stop, period_time, _bot):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.period_time = period_time
        self._bot = _bot
        self.thread_name = 'Scan KPI RNC CBS Thread'

    def run(self):
        interval = self.period_time
        check_late = False
        time_scan = None
        time_now = get_time_format_now()
        time_next_interval = plus_time_sec(time_now, interval)
        while not self.is_stop:
            log_scan = Logging('Scan And Insert BRAS ONLINE', 'Thread scan and insert BRAS ONLINE')

            try:
                time_now = get_time_format_now()

                if check_late and time_scan:
                    time_now = time_scan
                    check_late = False

                time_next_interval = plus_time_sec(time_now, interval)

                # scan auto
                _tbl_obj = TblRncCbsServerImpl()
                _res_new_lst = _tbl_obj.get_newest_lst()
                if _res_new_lst:
                    _res_time_now = _res_new_lst[0]['timestamp']
                    _res_time_now_date = _res_time_now

                    _time_lst_15m = get_date_minus_format_ipms(_res_time_now_date, 15)
                    _time_lst_1d = get_date_minus_format_ipms(_res_time_now_date, 24 * 60)
                    _time_lst_1w = get_date_minus_format_ipms(_res_time_now_date, 24 * 60 * 7)

                    _res_last_15m_lst = _tbl_obj.find_timestamp_lst(_time_lst_15m)
                    _res_last_1d_lst = _tbl_obj.find_timestamp_lst(_time_lst_1d)
                    _res_last_1w_lst = _tbl_obj.find_timestamp_lst(_time_lst_1w)

                    for _res_new in _res_new_lst:
                        try:
                            _node_name = _res_new['node']
                            _ps_cdr_now = _res_new['ps_cdr']
                            _ps_traf_now = _res_new['ps_traffic']
                            _time_now = _res_new['timestamp']

                            _node_find_15m_dct = self.find_node_name_in_lst(_node_name, _res_last_15m_lst)
                            _node_find_1d_dct = self.find_node_name_in_lst(_node_name, _res_last_1d_lst)
                            _node_find_1w_dct = self.find_node_name_in_lst(_node_name, _res_last_1w_lst)

                            if _node_find_15m_dct:
                                _ps_cdr_15m = _node_find_15m_dct['ps_cdr']
                                _ps_traf_15m = _node_find_15m_dct['ps_traffic']
                                _time_15m = _res_new['timestamp']

                                if _node_find_1w_dct:
                                    _ps_cdr_1w = _node_find_1w_dct['ps_cdr']
                                    _ps_traf_1w = _node_find_1w_dct['ps_traffic']
                                    _time_1w = _res_new['timestamp']

                                    _res_info = ''
                                    if _ps_traf_now <= 0.5 * _ps_traf_15m and _ps_traf_now <= 0.5 * _ps_traf_1w:
                                        _res_info += "***PS_TRAFFIC RNC vượt ngưỡng ***\n"
                                        _res_info += "Node: " + _node_name + "\n"
                                        _res_info += "PS_TRAFFIC Now: " + str(_ps_traf_now) + "\n"
                                        _res_info += "PS_TRAFFIC Last 15m: " + str(_ps_traf_15m) + "\n"
                                        _res_info += "PS_TRAFFIC Last 1w: " + str(_ps_traf_1w) + "\n"
                                        self._bot.send_mess(_res_info, BOT_GROUP_BSS_ID)

                                if _node_find_1d_dct:
                                    _ps_cdr_1d = _node_find_1d_dct['ps_cdr']
                                    _time_1w = _res_new['timestamp']

                                    _res_info = ''
                                    if _ps_cdr_now >= 2 * _ps_cdr_15m and _ps_cdr_now >= 2 * _ps_cdr_1d:
                                        _res_info += "***PS_CDR RNC vượt ngưỡng ***\n"
                                        _res_info += "Node: " + _node_name + "\n"
                                        _res_info += "PS_CDR Now: " + str(_ps_cdr_now) + "\n"
                                        _res_info += "PS_CDR Last 15m: " + str(_ps_cdr_15m) + "\n"
                                        _res_info += "PS_CDR Last 1w: " + str(_ps_cdr_1d) + "\n"
                                        self._bot.send_mess(_res_info, BOT_GROUP_BSS_ID)

                        except Exception as err:
                            print("Error %s when find result KPI RNC CBS" % str(err))

                time_later = get_time_format_now()
                time_diff = time_later - time_now
                time_diff_second = convert_timedelta_to_second(time_diff)
                print('Diff time scan:' + str(time_diff_second))
                if interval >= time_diff_second:
                    time_sleep = interval - time_diff_second
                    print('Sleeping for next ' + str(time_sleep) + " s")
                    time.sleep(time_sleep)
                else:
                    check_late = True
                    time_scan = time_next_interval

            except Exception as err:
                print(err)
                time_later = get_time_format_now()
                time_diff = time_later - time_now
                time_diff_second = convert_timedelta_to_second(time_diff)

                print('Diff time:' + str(time_diff_second))
                if interval >= time_diff_second:
                    time_sleep = interval - time_diff_second
                    print('Sleeping for ' + str(time_sleep) + " s")
                    time.sleep(time_sleep)
                else:
                    check_late = True
                    time_scan = time_next_interval

                log_scan.create_log('critical', err)

    @staticmethod
    def find_node_name_in_lst(node_name, node_lst):
        res = dict()
        for x in node_lst:
            if 'node' in x:
                node_name_x = x['node']
                if node_name_x == node_name:
                    return x
        return res

    def stop(self):
        self.is_stop = True


def plus_time_sec(tme, num):
    import datetime
    return tme + datetime.timedelta(seconds=num)


def convert_timedelta_to_second(time_delta):
    return time_delta.total_seconds()


if __name__ == '__main__':
    config = Development()
    BOT_ID = config.__getattribute__('BOT_ID')
    TELEBOT_BOT_TOKEN = BOT_ID
    import telebot
    bot = telebot.AsyncTeleBot(BOT_ID)

    test = ScanKpiRncCbsThread(False, 60 * 5, bot)
    test.start()
    pass
