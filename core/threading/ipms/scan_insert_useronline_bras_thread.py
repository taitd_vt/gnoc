__author__ = 'vtn-dhml-dhip10'
import threading
import time
from core.log.logger.logger import Logging
from core.database.impl.server_minhnd.tbl_insert_interface_group_impl import TblInsertInterfaceGroupImpl
from core.threading.device_thread.dev_scan_backhaul_upstream_lst import DeviceScanBackhaulUpstreamListThread
from core.helpers.list_helpers import chunkIt, get_pop_lst
from config import Development
from core.database.impl.forti_os.get_api_forti_os import FGT
from core.database.impl.elasticsearch.tbl_top_fw_impl import TblTopFwImpl
from core.helpers.date_helpers import get_date_minus_format_ipms, convert_date_to_ipms, \
    convert_date_to_elastic, get_time_format_now
from core.database.impl.server_minhnd.tbl_useronline_bras_impl import TblUseronlineBrasImpl

config = Development()
FW_FTG_LST = config.__getattribute__('FW_FTG_LST')


class ScanInsertUseronlineBrasThread(threading.Thread):
    def __init__(self, is_stop, period_time, area_lst):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.period_time = period_time
        self.thread_name = 'Scan And Insert BRAS ONLINE'
        self.area_lst = area_lst

    def run(self):
        interval = self.period_time
        check_late = False
        time_scan = None
        time_now = get_time_format_now()
        time_next_interval = plus_time_sec(time_now, interval)
        while not self.is_stop:
            log_scan = Logging('Scan And Insert BRAS ONLINE', 'Thread scan and insert BRAS ONLINE')

            try:
                time_now = get_time_format_now()
                date_time_elastic_now = convert_date_to_elastic(time_now)
                if check_late and time_scan:
                    time_now = time_scan
                    check_late = False

                time_next_interval = plus_time_sec(time_now, interval)

                # scan auto
                for x in self.area_lst:
                    _tbl_kick = _tbl_fnd_obj = TblUseronlineBrasImpl(max=1,
                                                                     current=1,
                                                                     max_input=1,
                                                                     link_name='Useronline',
                                                                     area=x,
                                                                     interface_id=1,
                                                                     rtg_url='',
                                                                     time_begin=time_now,
                                                                     node_name='',
                                                                     efficient=1
                                                                     )
                    res_lst = _tbl_kick.get_bras_lst()
                    if res_lst:
                        _tbl_fnd_obj.save_lst(res_lst)

                time_later = get_time_format_now()
                time_diff = time_later - time_now
                time_diff_second = convert_timedelta_to_second(time_diff)
                print('Diff time scan:' + str(time_diff_second))
                if interval >= time_diff_second:
                    time_sleep = interval - time_diff_second
                    print('Sleeping for next ' + str(time_sleep) + " s")
                    time.sleep(time_sleep)
                else:
                    check_late = True
                    time_scan = time_next_interval

            except Exception as err:
                print(err)
                time_later = get_time_format_now()
                time_diff = time_later - time_now
                time_diff_second = convert_timedelta_to_second(time_diff)

                print('Diff time:' + str(time_diff_second))
                if interval >= time_diff_second:
                    time_sleep = interval - time_diff_second
                    print('Sleeping for ' + str(time_sleep) + " s")
                    time.sleep(time_sleep)
                else:
                    check_late = True
                    time_scan = time_next_interval

                log_scan.create_log('critical', err)

    def stop(self):
        self.is_stop = True


def plus_time_sec(tme, num):
    import datetime
    return tme + datetime.timedelta(seconds=num)


def convert_timedelta_to_second(time_delta):
    return time_delta.total_seconds()


if __name__ == '__main__':
    area_lst = ['KV1', 'KV2', 'KV3']
    test = ScanInsertUseronlineBrasThread(False, 60 * 5, area_lst)
    test.start()
    pass
