__author__ = 'vtn-dhml-dhip10'
import threading
import time
from core.log.logger.logger import Logging
from app.api.vipa.ring.api_ring import ApiVipaRing
from core.database.impl.tbl_ring_info_impl import TblRingInfoImpl
from core.database.impl.tbl_bras_hsi_info_impl import TblBrasHsiInfoImpl
from core.helpers.date_helpers import get_only_date_now_format_ipms
from core.helpers.date_helpers import get_time_format_now


class ScanRingInfoVipaThread(threading.Thread):
    def __init__(self, is_stop, period_time):
        threading.Thread.__init__(self)
        self.is_stop = is_stop
        self.period_time = period_time
        self.thread_name = 'ScanRingInfoVipa'

    def run(self):
        interval = self.period_time
        check_late = False
        time_scan = None
        time_now = get_time_format_now()
        time_next_interval = plus_time_sec(time_now, interval)
        while not self.is_stop:
            self.is_stop = False
            log_scan = Logging('Scan Ring info Vipa', 'Thread get data of Ring Vipa')

            try:
                time_now = get_time_format_now()
                time_now_frt_ipms = get_only_date_now_format_ipms()

                if check_late and time_scan:
                    time_now = time_scan
                    check_late = False

                time_next_interval = plus_time_sec(time_now, interval)
                hour_now = str(time_now.hour)
                minute_now = str(time_now.minute)
                second_now = str(time_now.second)
                year_now = str(time_now.year)
                month_now = str(time_now.month)
                day_now = str(time_now.day)
                date_format = '{0}_{1}_{2}_{3}_{4}_{5}'.format(year_now, month_now, day_now, hour_now, minute_now,
                                                               second_now)
                name_log = 'log_scan_ring_info_' + date_format + '.txt'

                log_scan = Logging(name_log, 'Thread get data of Ring info')
                log_scan.create_log('info', 'Start getting data of ring info')

                _api_vpa_rng = ApiVipaRing()
                _tbl_rng_obj = TblRingInfoImpl('')
                # delete het luon do tai DB
                _api_vpa_rng.delete_ring_all()
                print('Delete Ring done')
                # get all
                srt_rng_lst = _api_vpa_rng.get_ring_info()
                if srt_rng_lst:
                    for srt_rng in srt_rng_lst:
                        srt_rng['last_update'] = time_now_frt_ipms
                        _tbl_rng_obj.post_not_check(**srt_rng)

                _api_vpa_rng.delete_bras_all()
                _tbl_bras_info_obj = TblBrasHsiInfoImpl('')
                bras_info_lst = _api_vpa_rng.get_bras_hsi_info()
                if srt_rng_lst:
                    for bras_info in bras_info_lst:
                        bras_info['last_update'] = time_now_frt_ipms
                        _tbl_bras_info_obj.post_not_check(**bras_info)

                print('Done all Ring info')

                time_later = get_time_format_now()
                time_diff = time_later - time_now
                time_diff_second = convert_timedelta_to_second(time_diff)

                print('Diff time:' + str(time_diff_second))
                if interval >= time_diff_second:
                    time_sleep = interval - time_diff_second
                    print('Sleeping for ' + str(time_sleep) + " s")
                    time.sleep(time_sleep)
                else:
                    check_late = True
                    time_scan = time_next_interval
                self.is_stop = True

            except Exception as err:
                print(err)
                time_later = get_time_format_now()
                time_diff = time_later - time_now
                time_diff_second = convert_timedelta_to_second(time_diff)

                print('Diff time:' + str(time_diff_second))
                if interval >= time_diff_second:
                    time_sleep = interval - time_diff_second
                    print('Sleeping for ' + str(time_sleep) + " s")
                    time.sleep(time_sleep)
                else:
                    check_late = True
                    time_scan = time_next_interval

                log_scan.create_log('critical', err)
                self.is_stop = True

    def stop(self):
        self.is_stop = True



def get_time_format_now():
    import datetime
    return datetime.datetime.now()

def plus_time_sec(tme, num):
    import datetime
    return tme + datetime.timedelta(seconds=num)


def convert_timedelta_to_second(time_delta):

    return time_delta.total_seconds()


if __name__ == '__main__':
    test = ScanRingInfoVipaThread(False, 60 * 15)
    test.run()

