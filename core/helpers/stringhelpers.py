import random
import string
import xmltodict
import json
from core.helpers.int_helpers import convert_string_to_int

def generate_random_keystring(length):
    return ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(length))


def divide_str(mess, mess_len):
    n = len(mess)
    mess_lst = []
    if n > mess_len:
        page = round(n / mess_len) + 2
        offset = 0
        i = 1
        chck_end = False
        while not chck_end:
            # them tim \n de cho no dep
            if offset == 0:
                offset = (i - 1) * mess_len
            if n < offset + mess_len:
                str_page = mess[offset: n]
                chck_end = True
            else:
                str_page = mess[offset: offset + mess_len]
                str_entr_pos = mess.find("\n", offset + mess_len, n)
                if str_entr_pos > 0:
                    str_page += mess[offset + mess_len: str_entr_pos]
                    offset = str_entr_pos
                else:
                    offset = offset + mess_len
            i += 1
            mess_lst.append(str_page)
    else:
        mess_lst.append(mess)
    return mess_lst


def convert_intf_short(vendor, intf_name):
    result = ""
    if vendor.upper() == 'CISCO':
        if intf_name[0:2] == 'Te':
            intf_name_behind = intf_name[2:]
            intf_name_new = 'TenGigE' + intf_name_behind
            return intf_name_new.strip()
        elif intf_name[0:2] == 'Hu':
            intf_name_behind = intf_name[2:]
            intf_name_new = 'HundredGigE' + intf_name_behind
            return intf_name_new.strip()
        elif intf_name[0:2] == 'Gi':
            intf_name_behind = intf_name[2:]
            intf_name_new = 'GigabitEthernet' + intf_name_behind
            return intf_name_new.strip()
    return result.strip()


def get_name_upstream(desc):
    undl_1 = desc.find("_")
    undl_2 = desc.find("_", undl_1 + 1)
    space_pos = desc.find(" ", undl_1 + 1)
    if undl_2 > 1 and undl_1 > 1:
        name_upstream = desc[undl_1 + 1:undl_2]
    elif space_pos > 0 and undl_1 > 1:
        name_upstream = desc[undl_1 + 1:space_pos]
    else:
        name_upstream = desc[undl_1 + 1:]

    return name_upstream.upper()


def is_node_manual(node):
    check = False
    if node.find('HKH9103CRT13.DGW.ASR12') >= 0 or node.find('HCM_IW2') >= 0 or node.find('HHT9602CRT24.IGW') >= 0:
        return True
    return check


def is_link_phy(intf):
    check = True
    intf = intf.upper()
    if intf.find('.') >= 0 or intf.find('BUNDLE') >= 0 or intf.find('BV') >= 0 or intf.find('ETH-TRUNK') >= 0:
        return False
    return check


def get_name_service_clsm(clsm_name):
    clsm_name = clsm_name.upper()
    if clsm_name.find('3G') >= 0:
        return '2G_3G'
    elif clsm_name.find('FTTH_UCTT') >= 0:
        return 'ADSL_FTTH'
    elif clsm_name.find('CTH') >= 0:
        return 'CTH'
    elif clsm_name.find('THOAI') >= 0:
        return 'THOAI_QUOC_TE'
    else:
        return clsm_name.replace('CM_', '')


def get_content(net_tpe, intf_desc):
    result = ''
    if net_tpe.upper().find('IPBN_UPSTREAM') >= 0:
        # mac dinh xac dinh desc la Upstream_tenContent_
        intf_desc = intf_desc.upper()
        pos_ustm = intf_desc.find('UPSTREAM')
        if pos_ustm >= 0:
            pos_undr = intf_desc.find('_', pos_ustm)
            if pos_undr > 0:
                pos_undr_sec = intf_desc.find('_', pos_undr + 1)
                if pos_undr_sec > 0:
                    result = intf_desc[pos_undr + 1: pos_undr_sec]
                else:
                    result = intf_desc[pos_undr + 1:]
                return result

    return result


def get_cable_name(net_tpe, intf_desc):
    result = ''
    if net_tpe.upper().find('IPBN_BACKHAUL') >= 0:
        # mac dinh xac dinh desc la Upstream_tenContent_
        intf_desc = intf_desc.upper()
        pos_cbl = intf_desc.find('CAP')
        if pos_cbl >= 0:
            pos_undr = intf_desc.find('_', pos_cbl)
            if pos_undr > 0:
                pos_undr_sec = intf_desc.find('_', pos_undr + 1)
                if pos_undr_sec > 0:
                    result = intf_desc[pos_undr + 1: pos_undr_sec]
                else:
                    result = intf_desc[pos_undr + 1:]
                return result

    return result


def check_regex_acc(acc_name):
    result = True
    acc_name = acc_name.upper()
    if len(acc_name) > 0:
        if acc_name.find('DELETE') > 0 or acc_name.find('INSERT') > 0  \
                or acc_name.find('UPDATE') > 0 or acc_name.find('%') > 0:
            return False
    else:
        return False
    return result


def convert_xml_to_dct(xml_str):
    result = xmltodict.parse(xml_str)

    return result


def convert_api_vipa_xml_to_dct(xml_str):
    result = xmltodict.parse(xml_str)
    if result:
        if 'S:Envelope' in result:
            res_env = result['S:Envelope']
            if 'S:Body' in res_env:
                res_body = res_env['S:Body']
                if 'ns2:getDataJsonResponse' in res_body:
                    res_data_json = res_body['ns2:getDataJsonResponse']
                    if 'return' in res_data_json:
                        res_retn = res_data_json['return']
                        if 'dataJson' in res_retn:
                            res_data = res_retn['dataJson']
                            if res_data:
                                return res_data
    return ''


def convert_api_vmsa_get_template_info_param_xml_to_dct(xml_str):
    result = xmltodict.parse(xml_str)
    res_data = dict()
    if result:
        if 'S:Envelope' in result:
            res_env = result['S:Envelope']
            if 'S:Body' in res_env:
                res_body = res_env['S:Body']
                if 'ns2:getTemplateInfoParamsResponse' in res_body:
                    res_data_json = res_body['ns2:getTemplateInfoParamsResponse']
                    if 'return' in res_data_json:
                        res_retn = res_data_json['return']
                        if 'resultCode' in res_retn:
                            res_data['resultCode'] = res_retn['resultCode']
                        if 'resultMessage' in res_retn:
                            res_data['resultMessage'] = res_retn['resultMessage']
                        if 'templateInfoParams' in res_retn:
                            res_temp_param = res_retn['templateInfoParams']
                            if res_temp_param:
                                res_data['templateId'] = res_temp_param['templateId']
                            if res_data:
                                return res_data
    return res_data


def convert_api_vipa_television_xml_to_dct(xml_str):
    res = []
    try:
        result = xmltodict.parse(xml_str)

        if result:
            if 'S:Envelope' in result:
                res_env = result['S:Envelope']
                if 'S:Body' in res_env:
                    res_body = res_env['S:Body']
                    if 'ns2:getDataJsonResponse' in res_body:
                        res_data_json = res_body['ns2:getDataJsonResponse']
                        if 'return' in res_data_json:
                            res_retn = res_data_json['return']
                            if 'dataJson' in res_retn:
                                res_data = res_retn['dataJson']
                                if res_data:
                                    res_data_lst = json.loads(res_data)
                                    if res_data_lst:
                                        if 'data' in res_data_lst:
                                            res = res_data_lst['data']
                                            return res
    except Exception as err:
        print("Error %s when convert API VIPA Television " + str(err))
    return res


def convert_api_soc_xml_to_dct(xml_str):
    result = xmltodict.parse(xml_str)
    res_data = dict()
    if result:
        if 'S:Envelope' in result:
            res_env = result['S:Envelope']
            if 'S:Body' in res_env:
                res_body = res_env['S:Body']
                if 'ns2:getDataJsonResponse' in res_body:
                    res_data_json = res_body['ns2:getDataJsonResponse']
                    if 'return' in res_data_json:
                        res_retn = res_data_json['return']
                        if 'dataJson' in res_retn:
                            res_data = res_retn['dataJson']
                            if res_data:
                                return res_data
    return res_data


def convert_api_television_check_user_to_dct(xml_str):
    result = xmltodict.parse(xml_str)
    res_data = dict()
    if result:
        try:
            if 'SOAP-ENV:Envelope' in result:
                res_env = result['SOAP-ENV:Envelope']
                if 'SOAP-ENV:Body' in res_env:
                    res_body = res_env['SOAP-ENV:Body']
                    if 'ns1:GetSTBStatusRes' in res_body:
                        res_data_json = res_body['ns1:GetSTBStatusRes']
                        if res_data_json:
                            # bo bot di ns1: trong phan key
                            for k, v in res_data_json.items():
                                pos = k.find('ns1:')
                                skip_ver = False
                                if pos >= 0:
                                    k = k[pos + 4:]
                                    if k.upper() == 'OSTYPE':
                                        k = 'Service Type'
                                        if v.upper() == 'IPTV':
                                            v = 'STB IPTV'
                                        else:
                                            v = 'STB Hybrid'
                                    elif k.upper() == 'MWVER':
                                        k = "Firmware STB"
                                    elif k.upper() == 'CHANNELID':
                                        k = "Channel is watching"
                                    elif k.upper() == 'STATUS':
                                        if str(v) == '0':
                                            v = 'offline'
                                        elif str(v) == '1':
                                            v = 'standby'
                                        elif str(v) == '2':
                                            v = 'Channel LiveTV'
                                        elif str(v) == '3':
                                            v = 'VOD'
                                        elif str(v) == '4':
                                            v = 'TSTV'
                                        elif str(v) == '5':
                                            v = 'TVOD'
                                        elif str(v) == '6':
                                            v = 'NPVR'
                                        else:
                                            v = 'Unexpected'
                                    elif k.upper() == 'OSVER':
                                        skip_ver = True
                                    elif k.upper() == 'LOADERVER':
                                        skip_ver = True
                                    elif k.upper() == 'FREQMODULATION':
                                        skip_ver = True
                                    elif k.upper() == 'STBERR':
                                        skip_ver = True
                                    if not skip_ver:
                                        res_data[k] = v
                            # Them phan check ket qua
                            result = True
                            pwr = 0
                            snr = 0
                            dec_err = 0
                            mlr = 0
                            del_fac = 0
                            ber = 0.0
                            cpu = 0
                            mem = 0
                            tx = 0
                            rx = 0
                            pkt_loss = 0
                            pkt_delay = 0
                            vmos = 0
                            jit = 0
                            ip_pkt_los = 0
                            delay = 0
                            zap_time = 0

                            reason = ''
                            if 'Pwr' in res_data:
                                pwr = float(res_data['Pwr'])
                            if 'BER' in res_data:
                                ber = float(res_data['BER'])
                            if 'CPU' in res_data:
                                cpu = float(res_data['CPU'])
                            if 'Memory' in res_data:
                                mem = float(res_data['Memory'])
                            if 'Tx' in res_data:
                                tx = float(res_data['Tx'])
                            if 'PktLoss' in res_data:
                                pkt_loss = float(res_data['PktLoss'])
                            if 'PktDelay' in res_data:
                                pkt_delay = float(res_data['PktDelay'])
                            if 'Vmos' in res_data:
                                vmos = float(res_data['Vmos'])
                            if 'Delay' in res_data:
                                delay = float(res_data['Delay'])
                            if 'Jitter' in res_data:
                                jit = float(res_data['Jitter'])
                            if 'IPktLoss' in res_data:
                                ip_pkt_los = float(res_data['IPktLoss'])
                            if 'ZappingTime' in res_data:
                                zap_time = float(res_data['ZappingTime'])
                            if 'SNR' in res_data:
                                snr = float(res_data['SNR'])
                            if 'DecodeErr' in res_data:
                                dec_err = float(res_data['DecodeErr'])
                            if 'Mlr' in res_data:
                                mlr = float(res_data['Mlr'])
                            if 'DelayFactor' in res_data:
                                del_fac = float(res_data['DelayFactor'])

                            if 'Service Type' in res_data:
                                val = res_data['Service Type']
                                if val == 'STB Hybrid':
                                    if 67 < pwr or pwr < 47:
                                        result = False
                                        reason += "PWR over threshold: 67> x >47.\n"
                                    if ber > 0.000006:
                                        result = False
                                        reason += "BER over threshold 10^-6\n"
                                    if cpu > 80:
                                        result = False
                                        reason += "CPU over threshold 80\n"
                                    if mem > 80:
                                        result = False
                                        reason += "Memory over threshold 80\n"
                                    if tx > 40000:
                                        result = False
                                        reason += "Tx over threshold 40000\n"
                                    if rx > 400000:
                                        result = False
                                        reason += "Rx over threshold 400000\n"
                                    if pkt_loss > 10:
                                        result = False
                                        reason += "Packet Loss over threshold 10\n"
                                    if pkt_delay > 3000:
                                        result = False
                                        reason += "Packet Delay over threshold 3000\n"
                                    if vmos < 2:
                                        result = False
                                        reason += "Vmos lower than threshold 2\n"
                                    if delay > 1000:
                                        result = False
                                        reason += "Delay over than threshold 1000\n"
                                    if jit > 2000:
                                        result = False
                                        reason += "Jitter over than threshold 2000\n"
                                    if ip_pkt_los > 500:
                                        result = False
                                        reason += "IPktLoss over than threshold 500\n"
                                    if zap_time > 500:
                                        result = False
                                        reason += "Zapping Time over than threshold 500\n"
                                    if snr < 32:
                                        result = False
                                        reason += "SNR lower threshold: <32.\n"
                                    if dec_err > 200:
                                        result = False
                                        reason += "Decode Error over threshold: >200.\n"
                                    if mlr > 5000:
                                        result = False
                                        reason += "MLR over threshold: >5000.\n"
                                    if del_fac > 3000:
                                        result = False
                                        reason += "Delay Factor over threshold: >3000.\n"

                                else:
                                    if dec_err > 20:
                                        result = False
                                        reason += "Decode Error over threshold: >20.\n"

                                    if cpu > 80:
                                        result = False
                                        reason += "CPU over threshold 80\n"
                                    if mem > 80:
                                        result = False
                                        reason += "Memory over threshold 80\n"
                                    if tx > 40000:
                                        result = False
                                        reason += "Tx over threshold 40000\n"
                                    if rx > 400000:
                                        result = False
                                        reason += "Rx over threshold 400000\n"
                                    if pkt_loss > 10:
                                        result = False
                                        reason += "Packet Loss over threshold 10\n"
                                    if pkt_delay > 3000:
                                        result = False
                                        reason += "Packet Delay over threshold 3000\n"
                                    if vmos < 2:
                                        result = False
                                        reason += "Vmos lower than threshold 2\n"
                                    if delay > 1000000:
                                        result = False
                                        reason += "Delay over than threshold 10^6\n"
                                    if jit > 2000000:
                                        result = False
                                        reason += "Jitter over than threshold 2.10^6\n"
                                    if ip_pkt_los > 500:
                                        result = False
                                        reason += "IPktLoss over than threshold 500\n"
                                    if zap_time > 500000:
                                        result = False
                                        reason += "Zapping Time over than threshold 500.000\n"
                                    if dec_err > 200:
                                        result = False
                                        reason += "Decode Error over threshold: > 200.\n"
                                    if mlr > 5000:
                                        result = False
                                        reason += "MLR over threshold: > 5000.\n"
                                    if del_fac > 3000.000:
                                        result = False
                                        reason += "Delay Factor over threshold: >3.10^6.\n"
                            res_data['result'] = str(result)
                            if not result:
                                res_data['reason'] = reason
                            return res_data
                    elif 'SOAP-ENV:Fault' in res_env:
                        # co loi xay ra khi tra cuu
                        res_faul = res_env['SOAP-ENV:Fault']
                        code_faul = ''
                        str_faul = ''
                        if 'faultcode' in res_faul:
                            code_faul = res_faul['faultcode']
                        if 'faultstring' in res_faul:
                            str_faul = res_faul['faultstring']
                        if code_faul and str_faul:
                            res_data['Lỗi'] = code_faul

                            if str(code_faul) == '300':
                                res_info = "Lỗi khi tra cứu: Thiết bị đã tắt nguồn"
                                res_data['Lỗi chi tiet'] = res_info
                            elif str(code_faul) == '404':
                                res_info = "Lỗi khi tra cứu: Không tìm thấy bản ghi trong CSDL"
                                res_data['Lỗi chi tiet'] = res_info
                            else:
                                res_info = "Lỗi khi tra cứu: " + str_faul
                                res_data['Lỗi chi tiet'] = res_info
                        else:
                            if code_faul:
                                res_info = "Lỗi khi tra cứu: lỗi " + str(code_faul)
                                res_data['Lỗi'] = code_faul
                                res_data['Lỗi chi tiet'] = res_info
                            elif str_faul:
                                res_info = "Lỗi khi tra cứu: lỗi " + str(str_faul)
                                res_data['Lỗi'] = 'Unexpected'
                                res_data['Lỗi chi tiet'] = res_info
                            else:
                                res_info = "Lỗi khi tra cứu: Unexpected"
                                res_data['Lỗi'] = 'Unexpected'
                                res_data['Lỗi chi tiet'] = res_info

        except Exception as err:
            print("Error %s when convert API Television get DT for log SON" % str(err))
            res_info = "Lỗi khi tra cứu: " + str(err)
            res_data['Lỗi'] = 'Unexpected'
            res_data['Lỗi chi tiet'] = res_info
            return res_data
    return res_data


def convert_api_vmsa_create_dt_log_for_son_xml_to_dct(xml_str):
    result = xmltodict.parse(xml_str)
    res_data = dict()
    if result:
        try:
            if 'S:Envelope' in result:
                res_env = result['S:Envelope']
                if 'S:Body' in res_env:
                    res_body = res_env['S:Body']
                    if 'ns2:createDtLogForSonResponse' in res_body:
                        res_data_json = res_body['ns2:createDtLogForSonResponse']
                        if 'return' in res_data_json:
                            res_retn = res_data_json['return']
                            if 'mopStatus' in res_retn:
                                res_data['mopStatus'] = res_retn['mopStatus']
                            if 'resultMessage' in res_retn:
                                res_data['resultMessage'] = res_retn['resultMessage']

                            if res_data:
                                return res_data
        except Exception as err:
            print("Error %s when convert VMSA get DT for log SON" % str(err))
            return res_data
    return res_data


def convert_api_vmsa_create_dt_log_for_bsc_xml_to_str(xml_str):
    result = xmltodict.parse(xml_str)
    res_data = dict()
    if result:
        try:
            if 'S:Envelope' in result:
                res_env = result['S:Envelope']
                if 'S:Body' in res_env:
                    res_body = res_env['S:Body']
                    if 'ns2:createDtLogForSonResponse' in res_body:
                        res_data_json = res_body['ns2:createDtLogForSonResponse']
                        if 'return' in res_data_json:
                            res_retn = res_data_json['return']
                            if 'mopStatus' in res_retn:
                                res_data['mopStatus'] = res_retn['mopStatus']
                            if 'resultMessage' in res_retn:
                                res_data['resultMessage'] = res_retn['resultMessage']
                            if 'logFileContent' in res_retn:
                                res_data['logFileContent'] = res_retn['logFileContent']

                            if res_data:
                                return res_data
        except Exception as err:
            print("Error %s when convert VMSA get DT for log SON" % str(err))
            return res_data
    return res_data


def check_legal_str(inp_str):
    check = True
    inp_str = inp_str.upper()
    if inp_str.strip() == '' or inp_str.find('%') or inp_str.find('UPDATE') >= 0 \
            or inp_str.find('DELETE') >= 0 or inp_str.find('INSERT') >= 0 or inp_str.find('SELECT') >= 0 \
            or inp_str.find('ALTER') >= 0:
        check = False
    return check


def check_contain_and_or_str(str_search, rule):
    # string check phan biet voi nhau thong qua dau | va &
    str_split_lst = ''
    chck = False
    str_search = str_search.upper()
    rule = rule.upper()
    if rule.find('|') > 0:
        str_split_lst = rule.split('|')
        for x in str_split_lst:
            if str_search.upper().find(x) >= 0:
                return True
    elif rule.find('&') > 0:
        str_split_lst = rule.split('&')
        for x in str_split_lst:
            if str_search.upper().find(x) < 0:
                return False
    else:
        # khong co truong and and or
        if str_search.upper().find(rule) >= 0:
            return True
        else:
            return False
    return False


def get_province_type(prvn):
    prvn = prvn.upper()
    if prvn == 'KV1' or prvn == 'KV2' or prvn == 'KV3':
        return 'area'
    else:
        return 'province'


def get_province_bras_name(prvn):
    prvn = prvn.upper()
    if prvn == 'HNI':
        return 'PDL'
    elif prvn == 'HCM':
        return 'HHT'
    elif prvn == 'DNG':
        return 'NTH'
    else:
        return prvn


def convert_web_name(web_name):
    if web_name.find('http://') >= 0:
        web_name_new = web_name[7:]
    elif web_name.find('https://') >= 0:
        web_name_new = web_name[8:]
    else:
        web_name_new = web_name

    # remove het tat ca phan / cuoi
    if web_name_new.find('/') >= 0:
        pos = web_name_new.find('/')
        web_name_new = web_name_new[:pos]
    return web_name_new


def split_mess_with_uctt(mess):
    # default mess with syntax uctt vnm ipms down => ok
    # nhung se co truong hop la uctt vnm (ipms kv1) (down dich vu) => chia tach boi dau (.
    # co truong hop khac la uctt vnm (ipms kv1) down dich vu
    pos_spc = mess.find("(")
    res_lst = []
    if pos_spc >= 0:
        mess_one = mess.split("(")
        if mess_one:
            leng = len(mess_one)
            if leng == 2:
                # phan down dich vu khong co ) o dang sau
                mess_2 = mess_one[1].split(")")
                mess_1 = mess_one[0].split(" ")
                for x in mess_1:
                    if x:
                        x = x.strip()
                        res_lst.append(x)
                for x in mess_2:
                    if x:
                        x = x.strip()
                        res_lst.append(x)
            else:
                # phan symptom cung co (
                mess_1 = mess_one[0].split(" ")
                for x in mess_1:
                    if x:
                        x = x.strip()
                        res_lst.append(x)
                for i in range(1, leng):
                    tmp = mess_one[i]
                    if tmp:
                        tmp = tmp.replace(")", "")
                        tmp = tmp.strip()
                        res_lst.append(tmp)
        return res_lst

    return mess.split(" ")


def get_mess_filter(mess, key_fltr_1, key_fltr_2=""):
    # muc dich lay phan ki tu dang sau key_fltr
    res = mess
    mess_up = str(mess).upper()
    key_fltr_1_up = str(key_fltr_1).upper()
    key_fltr_2_up = str(key_fltr_2).upper()

    if mess.find(key_fltr_1) >= 0:
        pos = mess_up.find(key_fltr_1_up)
        if pos >= 0:
            key_len = len(key_fltr_1_up)
            mess_fnd = mess[pos + key_len:]
            return str(mess_fnd).strip()
    elif key_fltr_2 != '':
        pos = mess_up.find(key_fltr_2_up)
        if pos >= 0:
            key_len = len(key_fltr_2_up)
            mess_fnd = mess[pos + key_len:]
            return str(mess_fnd).strip()
    return ''


def get_mess_filter_between_char(mess, key_fltr_1, key_contain):
    if mess:
        mess_up = str(mess).upper()
        key_contain_up = str(key_contain).upper()
        mess_up_lst = mess_up.split(key_fltr_1)
        mess_lst = mess.split(key_fltr_1)

        num = 0
        res = ''
        indx = 0
        for x in mess_up_lst:
            if num > 0:
                if x.find(key_contain_up) >= 0:
                    res += key_fltr_1 + mess_lst[indx]
            else:
                res = mess_lst[indx]
                num += 1
            indx += 1
    else:
        return 'Lỗi do không tìm thấy nội dung'
    return res


def get_total_filter_in_lst(mess_lst, key_fltr, key_fltr_detail='SO_'):
    from core.helpers.int_helpers import convert_string_to_int
    totl_res = 0
    totl_res_detl = 0
    key_fltr = str(key_fltr).upper()
    if mess_lst:
        try:
            for mess in mess_lst:
                try:
                    mess = mess.upper()
                    if mess.find(key_fltr) >= 0:
                        totl_res += 1
                        # De tim so tu chi tiet trong tung message ta can tim nhu sau: syntax : SO_TU: 10 Linh tinh: Lang tang
                        # B1: Tim ":" dau tien sau So_tu
                        # B2: Tim ":" thu hai sau ":" dau tien. Neu khong co : thu hai thi lay luon la cuoi cung vi co the ki tu SO_TU nam o cuoi chuoi
                        # B3: Tim ra so dau tien nam giua ":" giua B1 va B2
                        # B4: Tim ra dau " " nam giua so dau tien va ":" o B2
                        # Liet ke ra so neu co va tra ra 0 neu khong ton tai

                        if mess.find(key_fltr_detail) >= 0:
                            pos_key_detl = mess.find(key_fltr_detail)
                            # B1
                            pos_dbl_dot = mess.find(":", pos_key_detl)
                            if pos_dbl_dot:
                                pos_dbl_dot_2nd = mess.find(":", pos_dbl_dot + 1)
                                if pos_dbl_dot_2nd < 0:
                                    pos_dbl_dot_2nd = len(mess)
                                mess_sub = mess[pos_dbl_dot: pos_dbl_dot_2nd]
                                len_mess_sub = len(mess_sub)
                                for x in range(len_mess_sub):
                                    chr_x = mess_sub[x]
                                    chr_x_int = convert_string_to_int(chr_x)
                                    if chr_x_int > 0:
                                        # B4
                                        pos_spce = mess_sub.find(" ", x)
                                        if pos_spce > 0:
                                            num_fnd = mess_sub[x: pos_spce]
                                            num_fnd_int = convert_string_to_int(num_fnd)
                                            totl_res_detl += num_fnd_int
                                        else:
                                            if pos_dbl_dot_2nd == len(mess):
                                                # Truong hop khong con ki tu nao tim vi SO_TU nam o cuoi chuoi
                                                num_fnd = mess_sub[x:]
                                                num_fnd_int = convert_string_to_int(num_fnd)
                                                totl_res_detl += num_fnd_int
                                        break
                except Exception as err:
                    print("Error %s when get total filter sub from message" % str(err))
        except Exception as err:
            print("Error %s when get total filter from message" % str(err))

    return totl_res, totl_res_detl


def convert_string_to_dict(str_inp):
    res_dct = dict()
    str_inp = str(str_inp).upper().strip()
    chck_end = False
    # Muc dich la de convert 1 chuoi thanh 1 dict . VD Khong Cannguon: Co Mayno: Khong Co_linh_tinh
    # dau vao la Key: khong co dau cach. Value co the co dau cach.
    # B1: Tim dau ":" trong str_inp
    # B2: Tim : thu 2 trong str_inp sau ":" dau tien
    # B3: Neu co : thu 2: tim ra key thu 2 va lay val1 dau tien la tu ":" dau tien toi key thu 2
    # B4: Neu tu key dau tien den dau chuoi co ki tu => unexpected1 = ki tu dau tien
    # B5: Cat chuoi ra tu key thu 2 tro ve sau
    while not chck_end:
        try:
            pos_two_dot = str_inp.find(":")
            key1 = ''
            key_une = ''
            pos_bgn_key1 = 0
            if pos_two_dot >= 0:
                for x in range(pos_two_dot - 1, 0, -1):
                    tmp_chr = str_inp[x]
                    if tmp_chr != '' and tmp_chr != ' ':
                        # ki tu cuoi dau tien
                        pos_end_key1 = x
                        # tim dau cach nguoc tu key tro ve
                        chck_spce = False
                        for y in range(pos_end_key1, 0, -1):
                            if str_inp[y] == ' ':
                                pos_bgn_key1 = y
                                key1 = str(str_inp[y: x + 1]).strip()
                                chck_spce = True
                                break
                        if not chck_spce:
                            pos_bgn_key1 = 0
                            key1 = str(str_inp[:x + 1]).strip()
                        break

                if pos_bgn_key1 > 0:
                    key_une = str_inp[:pos_bgn_key1]
                    res_dct['unexpect1'] = str(key_une).strip()

            if pos_two_dot >= 0:
                pos_two_dot_2nd = str_inp.find(":", pos_two_dot + 1)
                if pos_two_dot_2nd >= 0:
                    # Truong hop search 2 key. Tim ra value thu 1
                    for x in range(pos_two_dot_2nd - 1, pos_two_dot, -1):
                        tmp_chr = str_inp[x]
                        if tmp_chr != '' and tmp_chr != ' ':
                            # ki tu cuoi dau tien
                            pos_end_key2 = x
                            # tim dau cach nguoc tu key tro ve
                            chck_spce = False
                            for y in range(pos_end_key2, pos_two_dot, -1):
                                if str_inp[y] == ' ':
                                    pos_bgn_key2 = y
                                    key2 = str_inp[y: x + 1]
                                    # tim ra value 1 luon
                                    val1 = str_inp[pos_two_dot + 1: y]
                                    res_dct[key1] = str(val1).strip()
                                    # cat chuoi luon
                                    str_inp = str(str_inp[y:]).strip()
                                    chck_spce = True
                                    break
                            if not chck_spce:
                                val1 = ''
                                res_dct[key1] = val1
                                str_inp = str_inp[pos_two_dot + 1:]
                            break
                else:
                    # truong hop search 1 key
                    # chu y co end luon truong hop nay
                    # truong hop val co thang cuoi cung unexpected. Cat bo luon thang cuoi.
                    # tim nguoc tu cuoi dau cach dau tien. Neu val tu : den dau " " dau tien # rong thi lay luon la val. Neu khong thi cho no val tu :
                    chck_spce = False
                    for x in range(len(str_inp) - 1, pos_two_dot, -1):
                        tmp_chr = str_inp[x]
                        if tmp_chr == ' ':
                            # ki tu cuoi dau tien
                            chck_spce = True
                            pos_end_val1 = x
                            # tim dau cach nguoc tu key tro ve
                            val1 = str_inp[pos_two_dot + 1:x]
                            key1 = str(str_inp[: pos_two_dot]).strip()
                            if str(val1).strip() == '':
                                val1 = str(str_inp[pos_two_dot + 1:]).strip()
                                res_dct[key1] = val1
                            else:
                                val1 = str(str_inp[pos_two_dot + 1: pos_end_val1]).strip()
                                res_dct[key1] = val1
                            chck_end = True
                            break
                    if not chck_spce:
                        # khong co space nao tinh luon tu dau
                        val1 = str(str_inp[pos_two_dot + 1:]).strip()
                        key1 = str(str_inp[: pos_two_dot]).strip()
                        res_dct[key1] = val1
                    if not chck_end:
                        # tim doc xuong cuoi khong thay dau cach nao?
                        val1 = str(str_inp[pos_two_dot + 1:]).strip()
                        key1 = str(str_inp[: pos_two_dot]).strip()
                        res_dct[key1] = val1
                        chck_end = True
            else:
                return None

        except Exception as err:
            print("Error %s when convert string to dict" % str(err))
    return res_dct

def clean_html_str(txt):
    n = len(txt)
    start_num = 0
    while start_num < n:
        try:
            pos_start_tag = txt.find("<", start_num)
            if pos_start_tag >= 0:
                chr_x = txt[pos_start_tag + 1]
                if chr_x == ' ':
                    chr_x = txt[pos_close_tag + 2]
                    if chr_x == ' ':
                        chr_x = txt[pos_close_tag + 3]
                        if chr_x == ' ':
                            chr_x = txt[pos_close_tag + 4]

                num = convert_string_to_int(str(chr_x))
                if num > 0:
                    txt = txt[:pos_start_tag] + " nho hon " + txt[pos_start_tag + 1:]
                start_num = pos_start_tag + 1
            else:
                start_num = n + 1
        except Exception as err:
            print(err)
            start_num = n +1
    start_num = 0
    while start_num < n:
        try:
            pos_close_tag = txt.find(">", start_num)
            if pos_close_tag >= 0:
                chr_x = txt[pos_close_tag + 1]
                if chr_x == ' ':
                    chr_x = txt[pos_close_tag + 2]
                    if chr_x == ' ':
                        chr_x = txt[pos_close_tag + 3]
                        if chr_x == ' ':
                            chr_x = txt[pos_close_tag + 4]
                num = convert_string_to_int(str(chr_x))
                if num > 0:
                    txt = txt[:pos_close_tag] + " lon hon " + txt[pos_close_tag + 1:]
                start_num = pos_close_tag + 1
            else:
                start_num = n + 1
        except Exception as err:
            print(err)
            start_num = n + 1
    return txt


if __name__ == '__main__':
    name_test = '<html>So do <10 va >70 <br> </br> </html>'
    mess_lst = ["QBH SO_TU: 200 Tinh yeu:  \n", " QNM SO_TU: 100 Linh tinh test: hehe", " QBH SO_TU: 300 test : hi hi 300"]
    name_new = clean_html_str(name_test)
    print(name_new)