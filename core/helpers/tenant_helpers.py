__author__ = 'vtn-dhml-dhip10'
from config import Config, Development
config = Development()
TENANT_ID_LST = config.__getattribute__('TENANT_ID_LST')
AREA_IP_MAIN_LST = config.__getattribute__('AREA_IP_MAIN_LST')
AREA_IP_SYSLOG_MAIN_LST = config.__getattribute__('AREA_IP_SYSLOG_MAIN_LST')
API_ACCESS_LST = config.__getattribute__('API_ACCESS_LST')


def get_tenant_id(ipms_ip):
    result = ""
    for tenant_dct in TENANT_ID_LST:
        for k, v in tenant_dct.items():
            if k == 'ip' and v == ipms_ip:
                return tenant_dct['id']
    return result


def get_tenant_syslog_id(area):
    result = ""
    area = area.upper()
    for tenant_dct in TENANT_ID_LST:
        for k, v in tenant_dct.items():
            if k == 'type' and v == 'syslog':
                if 'id' in tenant_dct:
                    tent_id = tenant_dct['id']
                    if tent_id.upper().find(area) >= 0:
                        return tent_id
    return result


def get_main_ipms(area):
    for area_dct in AREA_IP_MAIN_LST:
        for k, v in area_dct.items():
            if k == 'area' and v == area:
                return area_dct['ip']
    return ''


def get_main_ipms_syslog(area):
    for area_dct in AREA_IP_SYSLOG_MAIN_LST:
        for k, v in area_dct.items():
            if k == 'area' and v == area:
                return area_dct['ip']
    return ''


def get_tenant_id_from_area(area):
    ip = get_main_ipms(area)
    if ip:
        return get_tenant_id(ip)
    else:
        return ''


def get_tenant_id_syslog_from_area(area):
    ip = get_main_ipms_syslog(area)
    if ip:
        return get_tenant_id(ip)
    else:
        return ''


def get_ip_ipms_acc(area, type):
    area = area.upper()
    type = type.upper()
    ip_res = ''
    try:
        for x in API_ACCESS_LST:
            if x['area'].upper() == area and x['type'].upper() == type:
                return x['ip']

    except Exception as err:
        print("Error %s when find ip of ipms access" % err)
        return ip_res


def get_area_type(dev_type, area):
    dev_type = dev_type.upper()
    area = area.upper()
    if area == 'KV3':
        if dev_type == 'SRT':
            return 'KV3_SRT_ACC'
        elif dev_type == 'OLT':
            return 'KV3_OLT_ACC'
        return area
    else:
        if dev_type == 'SRT' or dev_type == 'OLT':
            if area == 'KV2':
                return 'KV2_ACC'
            elif area == 'KV1':
                return 'KV1_ACC'
        else:
            return area
    return ''