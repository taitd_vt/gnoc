__author__ = 'vtn-dhml-dhip10'
from datetime import datetime, timedelta


def get_date_now():
    now = datetime.now()
    return now


def convert_date_ipms_to_date_obj(date_ipms):
    try:
        date_time = date_ipms.strptime("%Y-%m-%d %H:%M:%S")
    except Exception as err:
        print("Error %s when convert date %s" % (err, str(date_ipms)))
        return None
    return date_time


def convert_date_soc_to_date_obj(date_soc):
    try:
        date_time = datetime.strptime(date_soc, "%Y/%m/%d %H:%M:%S")

    except Exception as err:
        print("Error %s when convert date %s" % (str(err), str(date_soc)))
        return None
    return date_time


def convert_date_to_epoch(date_time):
    try:
        date_time_epch = date_time.timestamp()
    except Exception as err:
            print("Error %s when convert date %s" % err, str(date_time))
            return None
    return date_time_epch


def date_diff_in_seconds(dt2, dt1):
    from datetime import datetime, time
    try:
        time_delta = dt2 - dt1
        days = time_delta.days
        seconds = time_delta.seconds
        total = days * 24 * 3600 + seconds
        return time_delta.days * 24 * 3600 + time_delta.seconds
    except Exception as err:
        print('Error %s when get diff in seconds between %s and %s' % str(err), str(dt2), str(dt1))
        return 666


def convert_date_str_to_date_obj_ipms(date_ipms):
    from datetime import datetime
    try:
        # bo qua phan GMT++ di
        pos_plus = date_ipms.find('+')
        if pos_plus > 0:
            date_ipms = date_ipms[:pos_plus]
        date_time_obj = datetime.strptime(date_ipms, '%Y-%m-%d %H:%M:%S')
        return date_time_obj
    except Exception as err:
       return None


def convert_date_str_to_date_obj_spring(date_str):
    from datetime import datetime
    try:
        # bo qua phan GMT++ di
        pos_plus = date_str.find('+')
        pos_dot = date_str.find('.')
        if pos_plus > 0:
            date_str = date_str[:pos_plus]
        if pos_dot > 0:
            date_str = date_str[:pos_dot]
        date_time_obj = datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S')

        return date_time_obj
    except Exception as err:
       print(err)
       return None


def convert_date_obj_to_date_str_ipms(date_obj):
    from datetime import datetime
    try:
        # bo qua phan GMT++ di
        date_str = datetime.strftime(date_obj, '%Y-%m-%d %H:%M:%S')
        return date_str
    except Exception as err:
        print(err)
        return None


def convert_date_obj_to_date_str_spring(date_obj):
    try:
            date_str = datetime.strftime(date_obj, "%Y-%m-%dT%H:%M:%S")
            return date_str
    except Exception as er:
            print(er)
            return None


def get_date_now_format_ipms():
    now = datetime.now()
    date_time = now.strftime("%Y-%m-%d %H:%M:%S")
    return date_time


def get_date_now_only_format_ipms():
    now = datetime.now()
    date_time = now.strftime("%Y-%m-%d")
    date_time += " 00:00:00"
    return date_time


def convert_date_to_ipms(date_now):
    date_time = date_now.strftime("%Y-%m-%d %H:%M:%S")
    return date_time


def get_only_date_now_format_ipms():
    now = datetime.now()
    date_time = now.strftime("%Y-%m-%d")
    return date_time


def get_date_now_format_elastic():
    import datetime
    now = datetime.datetime.now()
    date_time = now.isoformat()
    return date_time


def convert_date_to_elastic(date_now):
    date_time = date_now.isoformat()
    return date_time


def convert_date_to_elk_from_ipms(date_ipms):
    from datetime import datetime
    datetime_obj = datetime.strptime(date_ipms, '%Y-%m-%d %H:%M:%S')
    date_time_elk = datetime_obj.isoformat()
    return date_time_elk


def get_time_format_now():
    import datetime
    return datetime.datetime.now()


def plus_time_sec(tme, num):
    import datetime
    return tme + datetime.timedelta(seconds=num)


def convert_timedelta_to_second(time_delta):

    return time_delta.total_seconds()


def get_date_minus_format_ipms(time, minute_mins):
    date_time_minus = time - timedelta(minutes=minute_mins)
    date_time_minus_format = date_time_minus.strftime("%Y-%m-%d %H:%M:%S")
    return date_time_minus_format


def get_date_minus_format_elk(time, minute_mins):
    date_time_minus = time - timedelta(minutes=minute_mins)
    date_time_minus_format = date_time_minus.strftime("%Y-%m-%dT%H:%M:%S")
    return date_time_minus_format


def get_date_sec_format_elk(time, sec_mins):
    date_time_minus = time - timedelta(seconds=sec_mins)
    date_time_minus_format = date_time_minus.strftime("%Y-%m-%dT%H:%M:%S")
    return date_time_minus_format


def get_date_minus(time, minute_mins):
    date_time_minus = time - timedelta(minutes=minute_mins)
    return date_time_minus


def get_date_minus_sec(time, sec_mins):
    date_time_minus = time - timedelta(seconds=sec_mins)
    return date_time_minus


def get_date_yesterday_format_ipms():
    now = datetime.now()
    yesterday = now - timedelta(hours=24)
    yesterday_frmt = yesterday.strftime("%Y-%m-%d")
    return yesterday_frmt


def get_date_now_gmt(hour_diff_vn):
    now = datetime.now()
    date_time_gmt = now + timedelta(hours=hour_diff_vn)

    return date_time_gmt


def get_date_before_yesterday_format_ipms():
    now = datetime.now()
    yesterday = now - timedelta(hours=48)
    yesterday_frmt = yesterday.strftime("%Y-%m-%d")
    return yesterday_frmt


if __name__ == '__main__':
    _now = get_date_now()
    _now_soc = convert_date_soc_to_date_obj('2020/02/18 07:10:00')
    diff_sec = date_diff_in_seconds(_now, _now_soc)
    print(diff_sec)