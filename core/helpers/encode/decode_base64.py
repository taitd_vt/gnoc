import base64
import os
import shutil
from zipfile import ZipFile


class DecodeBase64:
    def __init__(self, base64_str, fold_name):
        self.base64_str = base64_str
        self.fold_name = fold_name

    def conv_to_zip(self):
        try:
            base64_img_bytes = self.base64_str.encode('utf-8')
            with open('test.zip', 'wb') as file_to_save:
                decoded_image_data = base64.decodebytes(base64_img_bytes)
                file_to_save.write(decoded_image_data)

            zip_file = ZipFile('test.zip')
            zip_file.extractall(self.fold_name)

        except Exception as err:
            print("Error %s when convert to zip" % str(err))
        finally:
            os.remove('test.zip')

    def read_file_get_val(self, *args):
        self.conv_to_zip()
        entries = os.listdir(self.fold_name)
        res_lst = []
        for x in entries:
            if x.find('txt') >= 0:
                with open(self.fold_name + "/" + x, 'r') as file_txt:
                    test = file_txt.readlines()
                    for lne in test:
                        for arg in args[0]:
                            if lne.find(arg) >= 0:
                                pos_dot_lst = lne.split(".")
                                res = str(pos_dot_lst[-1]).strip()
                                res_lst.append(dict(key=arg, val=res))
        try:
            shutil.rmtree(self.fold_name)
        except OSError:
            pass
        finally:
            return res_lst


if __name__ == '__main__':
    base64_img = 'UEsDBBQACAgIADlPiVAAAAAAAAAAAAAAAAA+AAAAU1FMX3Btc2FwQDEwLjYwLjYwLjc3XzMzMDZAc3RhdHVzX2t2MV8wMTEwLjYwLjYwLjc3XzA5NTc0OS50eHRdj1FrgzAQx9/7Ke7NF42XqK0KexqDlY2WzbHXkpp0dWh0MbaUse++RKkdg0B+d5f8ud99q5QsDZgWPsW+zJtL/1XnYUiRLMezWuVRhMsFvAxSXyCHXtbuw9BLrXgjfegNN0PvQ9VxIbQPe81tpXjftdpUwoeGl/aV1KeqlGXNezvt2roqL7uhm1G0ZzVmaWMqFzt0ghvpGA66baB4KIr1dlOQVtWVknA+Si3nNeAOvCNivPs4GHPcmeEkEg+4EuA9vVPPjd29gFdp91K9tCbXv9/+z+RgYZKw4CzsNWtYth7u6R8RW84mN3YqU+bkYvkms/i/5Ty1QFeUMBYTdyh1DSTJkrAoI5ShrR83a0xYvH1+QxpiGIcszaMkjFwKx7QkhzTJSCTHWNXqhtcuP2CIjQVxBYYMA4wClgLNcrrK43TuxgFmgGOXJb9QSwcIhhe6h0oBAAAgAgAAUEsBAhQAFAAICAgAOU+JUIYXuodKAQAAIAIAAD4AAAAAAAAAAAAAAAAAAAAAAFNRTF9wbXNhcEAxMC42MC42MC43N18zMzA2QHN0YXR1c19rdjFfMDExMC42MC42MC43N18wOTU3NDkudHh0UEsFBgAAAAABAAEAbAAAALYBAAAAAA=='
    _test = DecodeBase64(base64_img, 'milano')
    _test.conv_to_zip()
    key_lst = ['BUSY FULL RATE', 'BUSY HALF RATE', 'BUSY SDCCH', 'IDLE FULL RATE', 'IDLE HALF RATE',
               'IDLE SDCCH', 'BLOCKED RADIO TIME SLOTS', 'GPRS TIME SLOTS']
    rs_lst = _test.read_file_get_val(key_lst)
    print(rs_lst)



