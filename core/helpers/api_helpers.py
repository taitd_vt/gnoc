__author__ = 'VTN-DHML-DHIP10'
import time
import requests
from requests.auth import HTTPBasicAuth


def api_post_retry(url, headers, data):
    r = ''
    retry = 0
    while r == '' and retry < 3:
        try:
            r = requests.post(url, headers=headers, data=data, timeout=100)
            break

        except Exception as err:
            time.sleep(15)
            print('Sleep when post API ' + str(url))
            retry += 1
    return r


def api_post_basic_auth_retry(url, headers, data, user, pswd):
    r = ''
    retry = 0
    while r == '' and retry < 3:
        try:
            r = requests.post(url, headers=headers, data=data, auth=HTTPBasicAuth(username=user, password=pswd))
            break

        except Exception as err:
            time.sleep(15)
            print('Error %s and Sleep when post API %s' % (str(url), str(err)))
            retry += 1
    return r


def api_patch_retry(url, headers, data):
    r = ''
    retry = 0
    while r == '' and retry < 3:
        try:
            r = requests.patch(url, headers=headers, data=data)
            break

        except:
            time.sleep(15)
            print('Sleep when patch API ' + str(url))
            retry += 1
    return r


def api_get_retry(url, headers):
    r = ''
    retry = 0
    while r == '' and retry < 3:
        try:
            r = requests.get(url, headers=headers)
            retry += 1
            break

        except:
            time.sleep(15)
            print('Sleep when get API' + str(url))
            retry += 1
    return r


def api_delete_retry(url, headers):
    r = ''
    retry = 0
    while r == '' and retry < 3:
        try:
            r = requests.delete(url, headers=headers)
            break

        except:
            time.sleep(15)
            print('Sleep when delete API' + str(url))
            retry += 1
    return r


def api_put_retry(url, headers, data):
    r = ''
    retry = 0
    while r == '' and retry < 3:
        try:
            r = requests.put(url, headers=headers, data=data)
            break

        except:
            time.sleep(15)
            print('Sleep when put API' + str(url))
            retry += 1
    return r
