def get_alarm_type(alarm_num):
    res = ''
    try:
        if alarm_num == '1':
            return 'over threshold'
        elif alarm_num == '10':
            return 'packet error'
        elif alarm_num == '12':
            return 'Lower than yesterday and last week'
        elif alarm_num == '13':
            return 'Lower than avg last hour and yesterday'
        elif alarm_num == '11':
            return 'Lower than Avg last hour and last week'
        elif alarm_num == '2':
            return 'under threshold'
        elif alarm_num == '3':
            return 'Lower than average last hour'
        elif alarm_num == '4':
            return 'Lower than yesterday'
        elif alarm_num == '5':
            return 'Higher than peak last hour'
        elif alarm_num == '7':
            return 'Resource Jitter'
        elif alarm_num == '9':
            return 'Group usage is under threshold'
        else:
            return 'Not Defined'

    except Exception as err:
        print("Error %s when get alarm type of " % err, alarm_num)
        return res
