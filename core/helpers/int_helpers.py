__author__ = 'vtn-dhml-dhip10'


def convert_string_to_int(str_int):
    num = 0
    try:
        num = int(str_int)
        return num
    except Exception as err:
        print(err)
        return num


def convert_string_to_float(str_int):
    num = 0
    try:
        num = float(str_int)
        return num
    except Exception as err:
        print(err)
        return num


def check_legal_msisdn(str_msisdn):

    try:
        if 10 <= len(str_msisdn) < 14:
            num = int(str_msisdn)
            return True
        else:
            return False
    except Exception as err:
        return False