__author__ = 'VTN-DHML-DHIP10'
import inspect
from collections import namedtuple
from core.helpers.stringhelpers import convert_string_to_dict
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.database.impl.ipms.host_ipms_impl import HostIpmsImpl
from core.helpers.int_helpers import convert_string_to_int

class X(object):
    pass

def chunkIt(seq, num):
    avg = len(seq) / float(num)
    out = []
    last = 0.0

    while last < len(seq):
        out.append(seq[int(last): int(last + avg)])
        last += avg
    return out

def chunk_node_with_minus(node_str, node_plce):
    # dung de list truong hop co dau - VD GGHL01-10 = GGHL01,02,03,04,05 .. 10 hoac 01-10 va co node goc la GGHL thi la GGHL01, GGHL02..
    node_str = node_str.strip()
    res_lst = []
    if node_str.find("-") >= 0:
        # tim xem co ki tu trong node_str khong
        n = len(node_str)
        node_plce_new = ''
        for x in range(0, n):
            chr_x = node_str[x]
            if convert_string_to_int(chr_x) > 0:
                break
            elif chr_x == '0':
                break
            else:
                node_plce_new += chr_x

        pos_mins = node_str.find("-")
        if pos_mins >= 2:
            num_bgn_str = node_str[pos_mins - 2: pos_mins]
            num_end_str = node_str[pos_mins + 1:]
            if num_bgn_str and num_end_str:
                num_bgn = convert_string_to_int(num_bgn_str)
                num_end = convert_string_to_int(num_end_str)
                if num_bgn >= 0 and num_end > num_bgn:
                    if node_plce_new:
                        # tim so tu dau toi cuoi.
                        for x in range(num_bgn, num_end + 1):
                            if 10 > x >= 0:
                                tmp = node_plce_new + "0" + str(x)
                                if tmp not in res_lst:
                                    res_lst.append(tmp)
                            else:
                                tmp = node_plce_new + str(x)
                                if tmp not in res_lst:
                                    res_lst.append(tmp)

                    elif node_plce:
                        for x in range(num_bgn, num_end + 1                       ):
                            if 10 > x >= 0:
                                tmp = node_plce + "0" + str(x)
                                if tmp not in res_lst:
                                    res_lst.append(tmp)
                            else:
                                tmp = node_plce + str(x)
                                if tmp not in res_lst:
                                    res_lst.append(tmp)
                    else:
                        # return default khi khong co node_plce
                        for x in range(num_bgn, num_end + 1):
                            if 10 > x >= 0:
                                tmp = "0" + str(x)
                            else:
                                tmp = str(x)
                            if tmp not in res_lst:
                                res_lst.append(tmp)

    return res_lst, node_plce_new

def get_node_plce_mobile(node_str):
    n = len(node_str)
    node_plce = ''
    for x in range(0, n):
        chr_x = node_str[x]
        if convert_string_to_int(chr_x) > 0:
            break
        elif chr_x == '0':
            break
        else:
            node_plce += chr_x
    return node_plce

def chunk_node_str_to_lst(node_str):
    # co hai kieu splt: 1 la GGHL01-10 = list tu 01 den 10; 2 la GGHL01, 04, 05-10 la GGHL01, GGHL04, GGHL05-GGHL10
    # B1: cat chuoi bang ki tu ngan cach boi dau ","
    # B2: Lay ra tien to bang chu trong ki tu dau tien lam goc. VD GGHL => Tim ra cac ki tu chu va so
    # B3: Kiem tra xem co dau "-" khong?
    # B3.1: Neu co - thi cho so dau tien truoc dau "-" vao. Lay ra so cuoi cung o dau dau "-"
    # B3.2: Duyet lan luot tung so trong dai tu so dau tien toi so cuoi cung va cho vao list
    # B4. Duyet tiep tuc cho den het boi dau ,
    node_str = str(node_str).strip()
    comm_fnd = node_str.find(",")
    mins_fnd = node_str.find("-")
    res_lst = []
    # lay ra ki tu node goc
    n = len(node_str)
    node_plce = get_node_plce_mobile(node_str)

    if comm_fnd >= 0:
        node_str_comm_lst = node_str.split(",")
        if node_str_comm_lst:
            for node_str_comm in node_str_comm_lst:
                pos_mins_fnd = node_str_comm.find("-")
                if pos_mins_fnd >= 0:
                    res_mins_lst, node_plce_new = chunk_node_with_minus(node_str_comm, node_plce)
                    for x1 in res_mins_lst:
                        if x1 not in res_lst:
                            res_lst.append(x1)
                    if node_plce_new:
                        node_plce = node_plce_new
                else:
                    # truong hop neu co 2 so thi them node_plce goc vao. Neu co chu o trong tu thi them ca tu vao
                    if convert_string_to_int(node_str_comm) > 0:
                        if node_plce:
                            num = convert_string_to_int(node_str_comm)
                            if 10 > num >= 0:
                                tmp = node_plce + "0" + str(num)
                            else:
                                tmp = node_plce + str(num)
                            if tmp not in res_lst:
                                res_lst.append(tmp)
                    elif node_str_comm == '0':
                        if node_plce:
                            num = convert_string_to_int(node_str_comm)
                            if 10 > num >= 0:
                                tmp = node_plce + "0" + str(num)
                            else:
                                tmp = node_plce + str(num)
                            if tmp not in res_lst:
                                    res_lst.append(tmp)
                    else:
                        node_plce_new = get_node_plce_mobile(node_str_comm)
                        if node_plce_new:
                            node_plce = node_plce_new
                        res_lst.append(node_str_comm)

    else:
        if mins_fnd >= 0:
            res_lst, node_plce_new = chunk_node_with_minus(node_str, node_plce)
            if node_plce_new:
                node_plce = node_plce_new
        else:
            node_plce_new = get_node_plce_mobile(node_str)
            if node_plce_new:
                node_plce = node_plce_new
            res_lst.append(node_str)
    return res_lst


def get_pop_lst():
    host_lst = list()
    api_spring_host_grp_ipms = ApiSpringIpmsImpl('hostGroup', 'kv3068')
    srch_grp_name_dct = {'searchGroupName': 'FOIP_IPBN'}
    group_name_lst = api_spring_host_grp_ipms.get(srch_grp_name_dct)
    group_id_lst = list()
    if group_name_lst:
        for x in group_name_lst:
            if 'group_id' in x.keys():
                group_id_lst.append(x['group_id'])
    # Tim nhung host co ten POP
    api_spring_host_ipms = ApiSpringIpmsImpl('host', 'kv3068')
    srch_grp_name_dct = {'searchHostName': 'POP'}
    res_host_lst = api_spring_host_ipms.get(srch_grp_name_dct)
    host_lst = list()
    if res_host_lst:
        for x in res_host_lst:
            host_obj = namedtuple("HostIpms", x.keys())(*x.values())
            host_ipms_obj = HostIpmsImpl(host_obj.rid, host_obj.ip, host_obj.vgroup,
                                         host_obj.hostname, host_obj.sysDescr, host_obj.poller)
            host_ipms_obj = host_ipms_obj.get_vendor()
            if host_obj.vgroup in group_id_lst:
                host_lst.append(host_ipms_obj)
    return host_lst


def get_pop_manual_lst():
    hst_mnl_lst = ['HHT9602CRT24.IGW.ASR22.04', 'HCM_IW2_ASR9K_HHTN4', 'HKH9103CRT13.DGW.ASR12.01']
    res_lst = list()
    for host_nme in hst_mnl_lst:
        res_host_manual_lst = list()
        api_spring_host_ipms = ApiSpringIpmsImpl('host', 'kv3068')
        srch_hst_name_manual_dct = {'searchHostName': host_nme}
        res_host_manual_lst = api_spring_host_ipms.get(srch_hst_name_manual_dct)
        if res_host_manual_lst:
            for x in res_host_manual_lst:
                host_obj = namedtuple("HostIpms", x.keys())(*x.values())
                host_ipms_obj = HostIpmsImpl(host_obj.rid, host_obj.ip, host_obj.vgroup,
                                             host_obj.hostname, host_obj.sysDescr, host_obj.poller)
                host_ipms_obj = host_ipms_obj.get_vendor()
                res_lst.append(host_ipms_obj)
    return res_lst


def get_val_exst_in_lst(obj_dct_lst, key, val_fnd, key_rtrn):
    indx = -1
    for x in obj_dct_lst:
        indx += 1
        if key in x and key_rtrn in x:
            if x[key] == val_fnd:
                return x[key_rtrn], indx
    return [], 0


def get_filter_in_list(obj_lst, key_filter):
    # chuyen key_filter thanh list sau do test lai
    key_filter_dct = convert_string_to_dict(key_filter)
    res_lst = []
    if obj_lst:
        for obj in obj_lst:
            if isinstance(obj, dict):
                chck_fltr = True
                if key_filter_dct:
                    # so sanh het cac key, val trong key filter. neu ok het la ngon
                    for key_fltr, val_fltr in key_filter_dct.items():
                        chck_fltr_dct = False
                        for key, val in obj.items():
                            if str(key_fltr).upper() == str(key).upper():
                                if str(val).upper().find(str(val_fltr).strip()) >= 0:
                                    chck_fltr_dct = True

                            elif str(key).upper().find(str(key_fltr).upper()) >= 0:
                                if str(val).upper().find(str(val_fltr).strip()) >= 0:
                                    chck_fltr_dct = True

                        if not chck_fltr_dct:
                            chck_fltr = False

                    if chck_fltr:
                        res_lst.append(obj)
                else:
                    chck_fltr = False
                    for key, val in obj.items():
                        if str(key).upper().find(str(key_filter).upper()) >= 0 or str(val).upper().find(str(key_filter).upper()) >= 0:
                            chck_fltr = True
                            break
                    if chck_fltr:
                        res_lst.append(obj)
            elif isinstance(obj, list):
                # list cac string
                for x in obj:
                    if str(x).upper().find(str(key_filter).upper()) >= 0:
                        chck_fltr = True
                        res_lst.append(x)
            elif isinstance(obj, str):
                if obj.upper().find(str(key_filter).upper()) >= 0:
                    res_lst.append(obj)
            else:
                try:
                    chck_fltr = True
                    if key_filter_dct:
                        # so sanh het cac key, val trong key filter. neu ok het la ngon
                        for key_fltr, val_fltr in key_filter_dct.items():
                            chck_fltr_dct = False
                            for key, val in obj.__dict__.items():
                                if str(key_fltr).upper() == str(key).upper():
                                    if str(val).upper().find(str(val_fltr).strip()) >= 0:
                                        chck_fltr_dct = True
                                elif str(key).upper().find(str(key_filter_dct).upper()) >= 0:
                                    if str(val).upper().find(str(val_fltr).strip()) >= 0:
                                        chck_fltr_dct = True
                            if not chck_fltr_dct:
                                chck_fltr = False

                        if chck_fltr:
                            res_lst.append(obj)
                    else:
                        chck_fltr = False
                        for key, val in obj.__dict__.items():
                            if str(key).upper().find(str(key_filter).upper()) >= 0 and str(val).upper().find(
                                    str(key_filter).upper()) >= 0:
                                chck_fltr = True
                                break
                        if chck_fltr:
                            res_lst.append(obj)

                    #for attribute, value in obj.__dict__.items():
                    #    if str(attribute).upper().find(str(key_filter).upper()) >= 0 or str(value).upper().find(
                    #            str(key_filter).upper()) >= 0:
                    #        chck_fltr = True
                    #        break
                    #if chck_fltr:
                    #    res_lst.append(obj)
                except Exception as err:
                    print("Error %s when get filter in list" % str(err))

    return res_lst


def convert_lst_to_str(inp_lst, splt_chr, slash_chr):
    res = ''
    try:
        num_run = 0
        for x in inp_lst:
            if num_run == 0:
                res += slash_chr + str(x) + slash_chr
            else:
                res += splt_chr + slash_chr + str(x) + slash_chr
            num_run += 1

    except Exception as err:
        print("Error %s when convert list to string " % str(err))
    return res


if __name__ == '__main__':
    two_lst = "GGHL11,12, GGPD10, 11-13,15, GGHK12-16,23,25-35"
    res = chunk_node_str_to_lst(two_lst)
    print(res)
