__author__ = 'VTN-DHML-DHIP10'


def get_node_place(host_name, intf_desc):
    if host_name.find('HKH9103CRT13.DGW') >= 0:
        return 'JPN'
    elif host_name.find('HCM_IW2') >= 0:
        if intf_desc.find('SGP') >= 0:
            return 'SGP'
        else:
            return 'EU'
    elif host_name.find('HHT9602CRT24.IGW') >= 0:
        return 'HKG'
    else:
        node_place = host_name[0:3]
        return node_place


def get_alarm_level(alarm_number):
    if str(alarm_number) == '1':
        return 'CRITICAL'
    elif str(alarm_number) == '2':
        return 'MAJOR'
    elif str(alarm_number) == '3':
        return 'MINOR'
    elif str(alarm_number) == '4':
        return 'WARNING'
    return str(alarm_number)


def get_area(dev_name):
    dev_name = dev_name.upper()
    res = ''
    try:
        kv3_prv = ['BDG', 'CTO', 'AGG', 'HCM', 'KGG', 'DNI', 'TNH', 'TGG',
                   'VTU', 'LAN', 'VLG', 'DTP', 'CMU', 'BTE', 'STG', 'TVH', 'BPC', 'BLU', 'HUG',
                   'HHT']
        kv1_prv = ['BGG', 'BKN', 'BNH', 'CBG', 'DBN', 'HNI', 'HBH', 'HDG', 'HGG',
                   'HTH', 'HNM', 'HPG', 'HYN', 'LCI', 'LCU', 'LSN', 'NAN', 'NBH', 'NDH', 'PTO',
                   'QBH', 'QNH', 'SLA', 'TBH', 'THA', 'TNN', 'TQG', 'VPC', 'YBI', 'HTY', 'HAN',
                   'HLC', 'PVN', 'GVM', 'PDL']
        kv2_prv = ['BDH', 'BTN', 'DCN', 'DLK', 'DNG', 'GLI', 'KHA', 'KTM', 'LDG', 'NTN',
                   'PYN', 'QNI', 'QNM', 'QTI', 'TTH', 'NTH', 'HKH']

        chck_fnd = False
        pos_prev = 0
        while not chck_fnd:
            for x in kv1_prv:
                pos_prv = dev_name.find(x)
                if pos_prv >= 0:
                    return 'KV1'

            for x in kv2_prv:
                pos_prv = dev_name.find(x)
                if pos_prv >= 0:
                    return 'KV2'

            for x in kv3_prv:
                pos_prv = dev_name.find(x)
                if pos_prv >= 0:
                    return 'KV3'
            chck_fnd = True

    except Exception as err:
        print("Error %s when get Area of device name %s" % err, dev_name)
        return res
    return res


def get_area_from_acc(acc_name):
    acc_name_kv1_lst = ['h004', 'h031', 'l231', 's022', 't037', 'q052', 'p210', 'h321',
                        'n351', 'v211', 'n030', 'h320', 't036', 'n350', 'y029', 'n038',
                        'b241', 'q033', 't280', 'd230', 'h019', 'b240', 'l025', 'b281',
                        'h018', 'h039', 'c026', 't027', 'l020']
    acc_name_kv2_lst = ['q510', 'p057', 'd511', 'd500', 'q053', 'k058', 't054', 'b056',
                        'd501', 'q055', 'k060', 'l063', 'n068', 'b062', 'g059']
    acc_name_kv3_lst = ['t008', 'a076', 'b650', 'd061', 'v070', 'b651', 's079', 'v064',
                        'l072', 'h711', 'k077', 't074', 't073', 'c780', 'd067', 't066',
                        'c710', 'b075', 'b781']

    res = ''
    acc_name = acc_name.upper()
    try:
        for x in acc_name_kv1_lst:
            x = x.upper()
            if acc_name.find(x) >= 0:
                return 'KV1'

        for x in acc_name_kv2_lst:
            x = x.upper()
            if acc_name.find(x) >= 0:
                return 'KV2'

        for x in acc_name_kv3_lst:
            x = x.upper()
            if acc_name.find(x) >= 0:
                return 'KV3'
    except Exception as err:
        print("Error %s when convert account name from area" % err)
        return res

    return res


def filter_interface_name_ipms(intf_desc):
    res = False
    intf_desc = intf_desc.upper()
    if intf_desc.find('CLASS-DEFAULT_EGRESS') >= 0:
        return True
    elif intf_desc.find('TCP_SYN_EGRESS') >= 0:
        return True
    elif intf_desc.find('CLASS_UDP_EGRESS') >= 0:
        return True
    return res


def get_node_type_mobile(node_name):
    res = ''
    node_name = node_name.upper()
    try:
        if node_name[0:2] == 'MS':
            res ='MSC'
        elif node_name[0:2] == 'HL':
            res ='HLR'
        elif node_name[0:2] == 'HS':
            res ='HSS'
        elif node_name[0:2] == 'GG':
            res ='GGSN'
        elif node_name[0:2] == 'GS':
            res = 'GMSC'
        elif node_name[0:2] =='SG':
            res ='SGSN'
        elif node_name[0:2] =='SC':
            res ='SMSC'
        elif node_name[0:2] == 'ST':
            res = 'STP'
        elif node_name[0:2] == 'BC':
            res = 'BSC'
        elif node_name[0:2] =='RT':
            res ='CRBT'
        elif node_name[0:2] == 'RC':
            res = 'RNC'
        elif node_name[0:2] =='DR' or node_name[0:2] =='DE':
            res ='DSC'

    except Exception as err:
        print("Error %s when get node type of mobile %s" % (str(err), str(node_name)))
    return res


def find_group_name_overthreshold(grp_name):
    res = False
    grp_name = grp_name.upper()
    grp_name = grp_name.strip()
    grp_over_lst = ['GGC_FNA_AUTO',
                    'GGC_FBC_AUTO',
                    'IGW_POP_UPLINK_AUTO',
                    'IGW_POP_DOWNLINK_CKV_AUTO',
                    'PEDD_PEPS_GGC_FNA_AUTO',
                    'FOIP_KV3_DROP_POP',
                    'IGW_POP_AUTO',
                    'FOIP_KV3_IGW_POP_UPTREAM_TRANSIT',
                    'FOIP_KV3_IGW_POP_UPSTREAM_PEERING',
                    'PEKH_AUTO']
    for grp in grp_over_lst:
        if grp_name.find(grp) >= 0:
            return True
    return res


if __name__ == '__main__':
    #test = find_group_name_overthreshold('FOIP_KV3_IGW_POP_UPLINK_AUTO')
    #print(test)
    x = 30.99
    x_p = int(x) + 1
    print(x_p)