__author__ = 'vtn-dhml-dhip10'
import logging,logging.handlers, os
dirname_now = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = os.path.dirname((os.path.abspath(dirname_now)))
LOG_DIR = os.path.join(ROOT_DIR, 'log')


class Logging:
    def __init__(self, file_name, log_name):
        self.file_name = file_name
        self.log_name = log_name

    def create_log(self, level, message):
        logger = logging.getLogger(self.log_name)
        if logger.hasHandlers():
            logger.handlers.clear()

        logger.setLevel(logging.DEBUG)
        log_path = os.path.join(ROOT_DIR, 'logging')
        log_path = os.path.join(log_path, self.file_name)
        handler = logging.handlers.RotatingFileHandler(log_path, backupCount=20)
        formater = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s ')
        handler.setFormatter(formater)
        logger.addHandler(handler)
        if level.upper() == 'INFO':
            logger.info(message)
        elif level.upper() == 'CRITICAL':
            logger.critical(message)
        else:
            logger.exception(message)


if __name__ == '__main__':
    log1 = Logging('log_add_new_coin.txt', 'Add coin')
    log1.create_log('info', 'Fix some stuffs')