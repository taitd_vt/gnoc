__author__ = 'PTH'
from selenium import webdriver
from PIL import Image
from io import BytesIO
import time

fox = webdriver.Chrome('d:\\chromedriver.exe')
fox.get('http://192.168.251.15:9119/app/canvas#/workpad/workpad-447717c1-fa49-42cb-a2c0-b0432966d4db/page/1')
time.sleep(15)
fox.maximize_window()

# now that we have the preliminary stuff out of the way time to get that image :D
element = fox.find_element_by_xpath("//div[@class='canvasCheckered']") # find part of the page you want image of
location = element.location
size = element.size
png = fox.get_screenshot_as_png() # saves screenshot of entire page
fox.quit()

im = Image.open(BytesIO(png)) # uses PIL library to open image in memory

left = location['x']
top = location['y']
right = location['x'] + size['width']
bottom = location['y'] + size['height']


im = im.crop((left, top, right, bottom)) # defines crop points
im.save('screenshot.png') # saves new cropped image