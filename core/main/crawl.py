__author__ = 'PTH'
from selenium import webdriver
import time

#driver = webdriver.PhantomJS() # or add to your PATH
#driver.set_window_size(1024, 768) # optional
#
#driver.get('http://192.168.251.15:9119/app/canvas#/workpad/workpad-447717c1-fa49-42cb-a2c0-b0432966d4db/page/1')
#driver.save_screenshot('screen.png') # save a screenshot to disk
#sbtn = driver.find_element_by_css_selector('button.gbqfba')

def download(driver, target_path):
    """Download the currently displayed page to target_path."""
    def execute(script, args):
        driver.execute('executePhantomScript',
                       {'script': script, 'args': args})

    # hack while the python interface lags
    driver.command_executor._commands['executePhantomScript'] = ('POST', '/session/$sessionId/phantom/execute')
    # set page format
    # inside the execution script, webpage is "this"
    page_format = 'this.paperSize = {format: "A4", orientation: "landscape" };'
    execute(page_format, [])

    # render current page
    render = '''this.render("{}")'''.format(target_path)
    execute(render, [])

def download_lst(driver, target_path, urls, name):
   count = 0
   while count < len(urls):
        def execute(script, args):
            driver.execute('executePhantomScript', {'script': script, 'args' : args })

        driver = webdriver.PhantomJS('phantomjs')

        # hack while the python interface lags
        driver.command_executor._commands['executePhantomScript'] = ('POST',     '/session/$sessionId/phantom/execute')

        driver.get(urls[count])

        time.sleep(15)

        # set page format
        # inside the execution script, webpage is "this"
        pageFormat = '''this.paperSize = {format: "A4", orientation: "landscape" };'''
        execute(pageFormat, [])

        # render current page
        render = 'this.render("{file_name}.pdf")'.format(file_name=name[count])
        execute(render, [])

        count+=1


if __name__ == '__main__':
    driver = webdriver.PhantomJS('phantomjs')
    urls_lst = list()
    urls_lst.append('http://192.168.251.15:9119/app/canvas#/workpad/workpad-447717c1-fa49-42cb-a2c0-b0432966d4db/page/1')
    urls_lst.append('http://192.168.251.15:9119/app/canvas#/workpad/workpad-447717c1-fa49-42cb-a2c0-b0432966d4db/page/2')
    name_lst = list()
    name_lst.append('save_me_1')
    name_lst.append('save_me_2')
    #driver.get()

    download_lst(driver, "save_me.pdf", urls_lst, name_lst)

