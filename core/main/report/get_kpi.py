__author__ = 'VTN-DHML-DHIP10'
from core.database.impl.ipms.intf_grp_name_imps_impl import InterfaceGroupNameIpmsImpl
from core.database.impl.ipms.intf_ipms_impl import InterfaceImpsImpl
from core.database.impl.ipms.intf_grp_ipms_impl import InterfaceGroupIpmsImpl
from core.database.impl.ipms.poller_ipms_impl import PollerIpmsImpl
from core.helpers.date_helpers import get_date_yesterday_format_ipms
from core.database.impl.ipms.host_ipms_impl import HostIpmsImpl
from core.helpers.list_helpers import get_val_exst_in_lst
from core.helpers.tenant_helpers import get_tenant_id, get_main_ipms
from core.database.impl.server_minhnd.tbl_kpi_wait_server_impl import TblKpiWaitServerImpl
from core.database.impl.server_minhnd.tbl_kpi_server_impl import TblKpiServerImpl


class GetKpi:
    def __init__(self, time_begin, area):
        self.time_begin = time_begin
        self.area = area

    def get_kpi_wait(self):
        # tim phan nhom cua 1 area co ten FOIP
        _intf_obj = InterfaceImpsImpl(0, '', 0, 0, '', '')
        poll_impl = PollerIpmsImpl(0, '', '')
        _intf_grp_name_impl = InterfaceGroupNameIpmsImpl(0, '')
        ip_main_ipms = get_main_ipms(self.area)
        tent_area = get_tenant_id(ip_main_ipms)

        intf_lst, intf_grp_id_lst = _intf_obj.find_intf_from_grp_name(tent_area, 'FOIP')
        poll_lst = poll_impl.lst(tent_area)
        intf_grp_name_lst = _intf_grp_name_impl.get_grp_lst(tent_area, 'FOIP')

        if intf_lst:
            # add host
            indx = 0
            intf_obj_rid_poll_dct_lst = []

            for x in intf_lst:
                if x:

                    try:
                        _tbl_hst_ipms = HostIpmsImpl(0, '', '', '', '', '')
                        # tim ra group id trong list co san
                        intf_grp_id = _intf_obj.find_grp_id_in_intf_id_lst(x.id, intf_grp_id_lst)

                        if hasattr(x, 'rid') and hasattr(x, 'id'):
                            hst_rid = x.rid
                            # check exist
                            intf_id_obj_exst_lst, indx_rid = get_val_exst_in_lst(intf_obj_rid_poll_dct_lst, 'rid',
                                                                                 hst_rid,
                                                                                 'interface_lst')
                            hst_name = ''
                            poll_ip = ''
                            poll_name = ''

                            if intf_id_obj_exst_lst:
                                # ban chat interface list da loc duplicate roi
                                intf_id_obj_exst_lst.append(x.id)
                                intf_obj_rid_poll_dct_lst[indx_rid]['interface_list'] = intf_id_obj_exst_lst
                                hst_name = intf_obj_rid_poll_dct_lst[indx_rid]['hostname']
                                poll_ip = intf_obj_rid_poll_dct_lst[indx_rid]['poller_ip']
                                poll_name = intf_obj_rid_poll_dct_lst[indx_rid]['poller_id']

                            else:
                                # tao mot list moi
                                hst_rid_lst = _tbl_hst_ipms.find_host(tent_area, **dict(searchRid=hst_rid))
                                if hst_rid_lst and len(hst_rid_lst) == 1:
                                    # Tim ra RID
                                    # Cau truc list dict se la interface_obj = [dict(interface_id=,
                                    # interface_speed=..], rid= , poller =

                                    hst_obj = hst_rid_lst[0]
                                    hst_poll_id = hst_obj.poller
                                    # Tim ra poller
                                    poll_obj = poll_impl.find(hst_poll_id, poll_lst)

                                    if poll_obj:
                                        intf_obj_rid_poll_dct_lst.append(dict(rid=hst_rid, interface_list=[x.id],
                                                                              poller_id=hst_poll_id,
                                                                              poller_ip=poll_obj.poller_ip,
                                                                              hostname=hst_obj.hostname))
                                        hst_name = hst_obj.hostname
                                        poll_ip = poll_obj.poller_ip
                                        poll_name = hst_poll_id

                            # check host va poller

                            grp_fnd_obj = _intf_grp_name_impl.find_grp_obj_in_lst(intf_grp_name_lst, intf_grp_id)
                            if grp_fnd_obj:
                                intf_grp_name = grp_fnd_obj.group_name
                            else:
                                intf_grp_name = ''
                            # Insert vao bang KPI WAIT
                            if poll_name and poll_ip:
                                _tbl_kpi_wait = TblKpiWaitServerImpl(self.time_begin, self.area, poll_name, x.id,
                                                                     'WAIT', x.speed, hst_name, x.name,
                                                                     x.description, poll_ip,
                                                                     intf_grp_name, hst_rid, x.status, self.area + ' '
                                                                     + str(x.id))
                                _res_put = _tbl_kpi_wait.save()
                            else:
                                _tbl_kpi_wait = TblKpiWaitServerImpl(self.time_begin, self.area, poll_name, x.id,
                                                                     'FINISH BUT CAN NOT GET POLLNAME', x.speed,
                                                                     hst_name, x.name,
                                                                     x.description, poll_ip,
                                                                     intf_grp_name, hst_rid, x.status, self.area + ' '
                                                                     + str(x.id))
                                _res_put = _tbl_kpi_wait.save()

                    except Exception as err:
                        print('Error when get KPI wait %s %s' % (str(err), str(x)))

        return True

    def get_kpi(self):
        # search thanh phan status la WAIT va chuyen thanh FINISH
        _tbl_kpi_wait_impl = TblKpiWaitServerImpl('', '', '', 0, '', 0, '', '', '', '', '', 0, '', '')
        _poll_ipms_impl = PollerIpmsImpl(0, '', '')
        _tbl_kpi_wait_lst = _tbl_kpi_wait_impl.lst_area_wait(self.area)
        ip_main_ipms = get_main_ipms(self.area)
        tent_area = get_tenant_id(ip_main_ipms)
        tme_bgn_sec = self.time_begin + ' 00:00:00'
        tme_end_sec = self.time_begin + ' 23:59:59'
        _poll_lst = _poll_ipms_impl.lst(tent_area)
        if _tbl_kpi_wait_lst:

            for _tbl_kpi_wait_obj in _tbl_kpi_wait_lst:
                try:
                    tent_poll = ''
                    intf_ipms_impl = InterfaceImpsImpl(_tbl_kpi_wait_obj.idInterface, _tbl_kpi_wait_obj.tenInterface,
                                                       _tbl_kpi_wait_obj.rid, _tbl_kpi_wait_obj.speed,
                                                       _tbl_kpi_wait_obj.description,
                                                       _tbl_kpi_wait_obj.status_interface)

                    max_peak = 0.0
                    max_in = 0.0
                    max_out = 0.0
                    if _tbl_kpi_wait_obj.poller and _tbl_kpi_wait_obj.poller_ip:
                        if _poll_lst:
                            for x in _poll_lst:
                                if hasattr(x, 'poller_id') and hasattr(x, 'poller_ip'):
                                    if x.poller_id == _tbl_kpi_wait_obj.poller:
                                        tent_poll = get_tenant_id(x.poller_ip)
                                        break
                        if tent_poll:

                            if _tbl_kpi_wait_obj.status_interface == 'active':
                                max_in = intf_ipms_impl.get_max_in(tme_bgn_sec, tme_end_sec, tent_poll,
                                                                   _tbl_kpi_wait_obj.poller,
                                                                   _tbl_kpi_wait_obj.rid)

                                max_out = intf_ipms_impl.get_max_out(tme_bgn_sec, tme_end_sec, tent_poll,
                                                                     _tbl_kpi_wait_obj.poller,
                                                                     _tbl_kpi_wait_obj.rid)

                                if max_in > max_out:
                                    max_peak = max_in
                                else:
                                    max_peak = max_out
                            elif _tbl_kpi_wait_obj.status_interface == 'custom':
                                max_peak = max_in = intf_ipms_impl.get_max_custom(tme_bgn_sec, tme_end_sec, tent_poll,
                                                                                  _tbl_kpi_wait_obj.poller,
                                                                                  _tbl_kpi_wait_obj.rid)

                                max_out = 0

                            else:
                                pass

                            if max_peak > 0:
                                _tbl_kpi_wait_obj.status = 'FINISH'

                                if _tbl_kpi_wait_obj.speed > 0:
                                    perf = max_peak * 100 / _tbl_kpi_wait_obj.speed
                                    # Ghi thang moi vao
                                    _tbl_kpi_impl = TblKpiServerImpl(_tbl_kpi_wait_obj.idInterface,
                                                                     _tbl_kpi_wait_obj.group_name,
                                                                     _tbl_kpi_wait_obj.speed,
                                                                     _tbl_kpi_wait_obj.tenNode,
                                                                     _tbl_kpi_wait_obj.tenInterface,
                                                                     _tbl_kpi_wait_obj.description,
                                                                     _tbl_kpi_wait_obj.time_begin,
                                                                     max_peak,
                                                                     perf,
                                                                     max_in,
                                                                     max_out,
                                                                     _tbl_kpi_wait_obj.area
                                                                     )
                                    # check kpi
                                    check_over = _tbl_kpi_impl.check_over_kpi()
                                    if check_over:
                                        res_save = _tbl_kpi_impl.save()
                                        # print(res_save)


                                    # chuyen link wait sang finish
                                    res_save_wait = _tbl_kpi_wait_obj.save()
                                    # print(res_save_wait)
                                    # insert KPI
                            else:
                                _tbl_kpi_wait_obj.status = 'FINISH BUT SOME PROBLEM'
                                res_save_wait = _tbl_kpi_wait_obj.save()
                                # print(res_save_wait)
                except Exception as err:
                    print('Error when get KPI %s %s ' % str(err), str(_tbl_kpi_wait_obj))

        return True


if __name__ == '__main__':
    _ystd = get_date_yesterday_format_ipms()
    _obj = GetKpi(_ystd, 'KV1')
    res = _obj.get_kpi()
