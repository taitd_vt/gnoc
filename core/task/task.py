import time
import json
import uuid

import celery
from flask_mail import Message
from flask import render_template

from manage import app, mail
from core.helpers.list_helpers import chunkIt, get_pop_lst, get_pop_manual_lst
from core.threading.device_thread.dev_scan_intf_lst import DeviceScanInterfaceListThread
from core.threading.device_thread.dev_scan_backhaul_upstream_lst import DeviceScanBackhaulUpstreamListThread
from core.database.impl.tbl_monitor_impl import TblMonitorImpl
from core.helpers.date_helpers import get_date_yesterday_format_ipms, \
    get_date_before_yesterday_format_ipms
from core.main.report.get_kpi import GetKpi
from core.database.impl.nocpro.tbl_ip_syslog_core import TblIpSyslogCoreElk
from core.database.impl.nocpro.tbl_free_task import TblFreeTask
from app.api.vipa.ring.api_ring import ApiVipaRing
from core.database.impl.tbl_ring_info_impl import TblRingInfoImpl
from core.database.impl.tbl_bras_hsi_info_impl import TblBrasHsiInfoImpl
from core.helpers.date_helpers import get_only_date_now_format_ipms
from core.helpers.date_helpers import get_date_minus_format_ipms, convert_date_to_ipms, \
    get_time_format_now, get_date_now_gmt
from core.helpers.tenant_helpers import get_tenant_id
from core.parsing.scan_dev_wanphy import ScanDeviceWanphy
from core.database.impl.ipms.poller_ipms_impl import PollerIpmsImpl
from core.database.impl.ipms.intf_ipms_impl import InterfaceImpsImpl
from core.helpers.stringhelpers import get_content
from core.helpers.date_helpers import convert_date_to_elastic, get_date_now, get_date_minus, get_date_minus_sec
from core.database.impl.elasticsearch.tbl_intf_stat_impl import TblIntfStatImpl
from core.database.impl.server_minhnd.tbl_interface_not_add_group_impl import TblInterfaceNotAddGroupImpl
from core.database.__init__ import es
from core.threading.scan_ring_info_vipa import ScanRingInfoVipaThread


@celery.task
def mail_pckt_err():
    logger = mail_pckt_err.get_logger()
    logger.info('Mail Link Packet error')
    ystd = get_date_yesterday_format_ipms()
    msg = Message('List Packet Error Links ' + str(ystd), recipients=['thongplh@viettel.com.vn',
                                                                      'minhnd6@viettel.com.vn',
                                                                      'noc_ip@viettel.com.vn'],

                  sender='foip_vhkttc@viettel.com.vn')
    msg.body = 'Hi! This is a test mail!'

    _tbl_monitor = TblMonitorImpl()
    with app.app_context():
        res_lst = _tbl_monitor.find_pckt_err(6, ystd)
        msg_html = render_template('report/reminder.html', items=res_lst)
        msg.html = msg_html
        mail.send(msg)

    return 'Hello'


@celery.task
def mail_top_alm_vn():
    logger = mail_top_alm_vn.get_logger()
    logger.info('Mail Top Alarm VN')
    ystd = get_date_yesterday_format_ipms()
    tody = get_only_date_now_format_ipms()
    msg = Message('List Top Alarm IP Syslog ' + str(ystd), recipients=['thongplh@viettel.com.vn',
                                                                       'minhnd6@viettel.com.vn',
                                                                       'noc_ip@viettel.com.vn',
                                                                       'bo_ip@viettel.com.vn'],

                  sender='foip_vhkttc@viettel.com.vn')
    msg.body = 'Hi! This is Top 10 IP Syslog Alarm NOCPRO yesterday!'

    _tbl_free = TblFreeTask(ystd, tody)
    with app.app_context():
        res_lst = _tbl_free.get_top_ip_alarm()
        msg_html = render_template('report/top_nocpro.html', items=res_lst)
        msg.html = msg_html
        mail.send(msg)

    return 'OK'


@celery.task
def mail_top_syslog_dev_vn():
    logger = mail_top_syslog_dev_vn.get_logger()
    logger.info('Mail Top Syslog Alarm each Table Syslog VN')
    ystd = get_date_yesterday_format_ipms()
    tody = get_only_date_now_format_ipms()
    msg = Message('List Top Syslog Alarm Each Table Syslog VN ' + str(ystd), recipients=['thongplh@viettel.com.vn',
                                                                                         'minhnd6@viettel.com.vn',
                                                                                         'noc_ip@viettel.com.vn',
                                                                                         'bo_ip@viettel.com.vn',
                                                                                         'quynhnt30@viettel.com.vn',
                                                                                         'toannk86@viettel.com.vn'],

                  sender='foip_vhkttc@viettel.com.vn')
    msg.body = 'Hi! This is Top Syslog Alarm Each Table Syslog VN yesterday!'

    _tbl_free = TblFreeTask(ystd, tody)
    with app.app_context():
        num_tot_sec = _tbl_free.get_total_secondary_ip()
        num_tot_pri = _tbl_free.get_total_first_ip()
        num_tot_cbs = _tbl_free.get_total_alm_cbs_ip()

        num_tot_dcn = _tbl_free.get_total_alm_tbl_syslog_ip('log_dcn')
        tot_dcn_lst = _tbl_free.get_top_node_alarm_in_tbl_syslog('log_dcn')

        num_tot_ipbn = _tbl_free.get_total_alm_tbl_syslog_ip('log_ipbn')
        tot_ipbn_lst = _tbl_free.get_top_node_alarm_in_tbl_syslog('log_ipbn')

        num_tot_mpbn = _tbl_free.get_total_alm_tbl_syslog_ip('log_mpbn')
        tot_mpbn_lst = _tbl_free.get_top_node_alarm_in_tbl_syslog('log_mpbn')

        num_tot_ps = _tbl_free.get_total_alm_tbl_syslog_ip('log_ps')
        tot_ps_lst = _tbl_free.get_top_node_alarm_in_tbl_syslog('log_ps')

        res_cbs_lst = _tbl_free.get_top_cbs_ip()
        res_cbs_intf_lst = _tbl_free.get_top_cbs_intf_ip()

        msg_html = render_template('report/top_syslog_tbl.html', items_dcn=tot_dcn_lst, items_ipbn=tot_ipbn_lst
                                   , items_mpbn=tot_mpbn_lst, items_ps=tot_ps_lst, num_sec=num_tot_sec,
                                   num_pri=num_tot_pri, num_dcn=num_tot_dcn, num_ipbn=num_tot_ipbn,
                                   num_mpbn=num_tot_mpbn, num_ps=num_tot_ps, num_cbs=num_tot_cbs, items_cbs=res_cbs_lst,
                                   items_cbs_intf=res_cbs_intf_lst)

        msg.html = msg_html
        mail.send(msg)

    return 'OK'


@celery.task
def scan_ring_info_vipa():
    logger = scan_ring_info_vipa.get_logger()
    try:
        _scan_ring_vipa = ScanRingInfoVipaThread(False, 60 * 60 * 24)
        _scan_ring_vipa.start()

    except Exception as err:
        logger.critical(err)


@celery.task
def scan_dev_intf_lst():
    logger = scan_dev_intf_lst.get_logger()
    logger.info('Start getting data of interface of device')
    time_now = get_time_format_now()
    try:

        pop_mnl_lst = get_pop_manual_lst()
        if pop_mnl_lst:
            dev_mnl_scan = DeviceScanInterfaceListThread(False, time_now, pop_mnl_lst, "thunv5", "Viettel#321#")
            dev_mnl_scan.start()
        pop_auto_lst = get_pop_lst()
        # divide to 3
        dev_scan = DeviceScanInterfaceListThread(False, time_now, pop_auto_lst, "dungnd18", "dungnd18@123")
        dev_scan.start()
    except Exception as err:
        print(err)
        logger.critical(err)


@celery.task
def scan_back_upstr():
    logger = scan_back_upstr.get_logger()
    logger.info('Start getting data Backhaul and Upstream')

    try:
        time_now = get_time_format_now()

        hst_mnl_lst = ['HHT9602CRT24.IGW.ASR22.04', 'HCM_IW2_ASR9K_HHTN4', 'HKH9103CRT13.DGW.ASR12.01']
        dev_mnl = DeviceScanBackhaulUpstreamListThread(False, time_now, 'manual', hst_mnl_lst, 'cisco')
        dev_mnl.start()
        # scan auto
        pop_lst = get_pop_lst()
        if pop_lst:
            pop_lst_div = chunkIt(pop_lst, 3)
            for y in pop_lst_div:
                dev_auto = DeviceScanBackhaulUpstreamListThread(False, time_now, 'auto', y, 'cisco')
                dev_auto.start()

    except Exception as err:
        print(err)
        logger.critical(err)


@celery.task
def get_nocpro_syslog_ip_core():
    logger = get_nocpro_syslog_ip_core.get_logger()
    logger.info('Get LOG NOCPRO SYSLOG IP CORE')

    try:
        yes_date = get_date_yesterday_format_ipms()
        bef_yes_date = get_date_before_yesterday_format_ipms()

        _tbl = TblIpSyslogCoreElk(bef_yes_date, yes_date)
        _tbl_new_lst = _tbl.get_new_lst()
        _tbl_clr_lst = _tbl.get_clr_lst()
        _tbl_his_lst = _tbl.get_his_lst()

        if _tbl_new_lst:
            _tbl.inst_els('NEW', _tbl_new_lst)
        if _tbl_clr_lst:
            _tbl.inst_els('CLEAR', _tbl_clr_lst)
        if _tbl_his_lst:
            _tbl.inst_els('HISTORY', _tbl_his_lst)

    except Exception as err:
        print(err)
        logger.critical(err)


@celery.task
def get_kpi_wait_and_scan_kv1():
    logger = get_kpi_wait_and_scan_kv1.get_logger()
    logger.info('Get KPI KV1 WAIT')
    area = 'KV1'
    yes_date = get_date_yesterday_format_ipms()
    _obj = GetKpi(yes_date, area)

    try:

        res = _obj.get_kpi_wait()
        logger.info('GET KPI WAIT OF ' + str(area) + ' of ' + str(yes_date) + ' is' + str(res))

    except Exception as err:
        print(err)
        logger.critical(err)

    try:
        res = _obj.get_kpi()
        logger.info('SCAN KPI OF ' + str(area) + ' of ' + str(yes_date) + ' is' + str(res))
    except Exception as err:
        print(err)
        logger.critical(err)


@celery.task
def get_kpi_wait_and_scan_kv2():
    logger = get_kpi_wait_and_scan_kv2.get_logger()
    logger.info('Get KPI KV2 WAIT')
    area = 'KV2'
    yes_date = get_date_yesterday_format_ipms()
    _obj = GetKpi(yes_date, area)
    try:
        res = _obj.get_kpi_wait()
        logger.info('GET KPI WAIT OF ' + str(area) + ' of ' + str(yes_date) + ' is' + str(res))

    except Exception as err:
        print(err)
        logger.critical(err)

    try:
        res = _obj.get_kpi()
        logger.info('SCAN KPI OF ' + str(area) + ' of ' + str(yes_date) + ' is' + str(res))
    except Exception as err:
        print(err)
        logger.critical(err)


@celery.task
def get_kpi_wait_and_scan_kv3():
    logger = get_kpi_wait_and_scan_kv3.get_logger()
    logger.info('Get KPI KV3 WAIT')
    area = 'KV3'
    yes_date = get_date_yesterday_format_ipms()
    _obj = GetKpi(yes_date, area)

    try:

        res = _obj.get_kpi_wait()
        logger.info('GET KPI WAIT OF ' + str(area) + ' of ' + str(yes_date) + ' is' + str(res))

    except Exception as err:
        print(err)
        logger.critical(err)

    try:
        res = _obj.get_kpi()
        logger.info('SCAN KPI OF ' + str(area) + ' of ' + str(yes_date) + ' is' + str(res))
    except Exception as err:
        print(err)
        logger.critical(err)


@celery.task
def get_kpi_wait_and_scan_cam():
    logger = get_kpi_wait_and_scan_cam.get_logger()
    logger.info('Get KPI CAM WAIT')
    area = 'CAM'
    yes_date = get_date_yesterday_format_ipms()
    _obj = GetKpi(yes_date, area)

    try:

        res = _obj.get_kpi_wait()
        logger.info('GET KPI WAIT OF ' + str(area) + ' of ' + str(yes_date) + ' is' + str(res))

    except Exception as err:
        print(err)
        logger.critical(err)

    try:
        res = _obj.get_kpi()
        logger.info('SCAN KPI OF ' + str(area) + ' of ' + str(yes_date) + ' is' + str(res))
    except Exception as err:
        print(err)
        logger.critical(err)


@celery.task
def get_kpi_wait_and_scan_tim():
    logger = get_kpi_wait_and_scan_tim.get_logger()
    logger.info('Get KPI TIMOR WAIT')
    area = 'TIM'
    yes_date = get_date_yesterday_format_ipms()
    _obj = GetKpi(yes_date, area)

    try:

        res = _obj.get_kpi_wait()
        logger.info('GET KPI WAIT OF ' + str(area) + ' of ' + str(yes_date) + ' is' + str(res))

    except Exception as err:
        print(err)
        logger.critical(err)

    try:
        res = _obj.get_kpi()
        logger.info('SCAN KPI OF ' + str(area) + ' of ' + str(yes_date) + ' is' + str(res))
    except Exception as err:
        print(err)
        logger.critical(err)


@celery.task
def get_kpi_wait_and_scan_moz():
    logger = get_kpi_wait_and_scan_moz.get_logger()
    logger.info('Get KPI MOZ WAIT')
    area = 'MOZ'
    yes_date = get_date_yesterday_format_ipms()
    _obj = GetKpi(yes_date, area)

    try:

        res = _obj.get_kpi_wait()
        logger.info('GET KPI WAIT OF ' + str(area) + ' of ' + str(yes_date) + ' is' + str(res))

    except Exception as err:
        print(err)
        logger.critical(err)

    try:
        res = _obj.get_kpi()
        logger.info('SCAN KPI OF ' + str(area) + ' of ' + str(yes_date) + ' is' + str(res))
    except Exception as err:
        print(err)
        logger.critical(err)


@celery.task
def get_kpi_wait_and_scan_hai():
    logger = get_kpi_wait_and_scan_hai.get_logger()
    logger.info('Get KPI HAI WAIT')
    area = 'HAI'
    yes_date = get_date_yesterday_format_ipms()
    _obj = GetKpi(yes_date, area)

    try:

        res = _obj.get_kpi_wait()
        logger.info('GET KPI WAIT OF ' + str(area) + ' of ' + str(yes_date) + ' is' + str(res))

    except Exception as err:
        print(err)
        logger.critical(err)

    try:
        res = _obj.get_kpi()
        logger.info('SCAN KPI OF ' + str(area) + ' of ' + str(yes_date) + ' is' + str(res))
    except Exception as err:
        print(err)
        logger.critical(err)


@celery.task
def get_kpi_wait_and_scan_per():
    logger = get_kpi_wait_and_scan_per.get_logger()
    logger.info('Get KPI PER WAIT')
    area = 'PER'
    yes_date = get_date_yesterday_format_ipms()
    _obj = GetKpi(yes_date, area)

    try:

        res = _obj.get_kpi_wait()
        logger.info('GET KPI WAIT OF ' + str(area) + ' of ' + str(yes_date) + ' is' + str(res))

    except Exception as err:
        print(err)
        logger.critical(err)

    try:
        res = _obj.get_kpi()
        logger.info('SCAN KPI OF ' + str(area) + ' of ' + str(yes_date) + ' is' + str(res))
    except Exception as err:
        print(err)
        logger.critical(err)


@celery.task
def get_traffic_uplink_international():
    logger = get_traffic_uplink_international.get_logger()
    logger.info('Get Traffic Uplink International')

    try:
        _intf_obj = InterfaceImpsImpl(0, '', 0, 0, '', '')
        poll_impl = PollerIpmsImpl(0, '', '')

        intf_grp_uplk_lst = list()
        intf_grp_tim_dct = dict(tent_id='tim038', gmt=2, group_name='GLOBAL_TIMOR_IPBN_IGW_UPLINK', area="TIMOR")
        intf_grp_cam_dct = dict(tent_id='cam134', gmt=0, group_name='GLOBAL_CAM_IPBN_IGW_UPLINK', area="CAMBODIA")
        intf_grp_moz_dct = dict(tent_id='moz022', gmt=-5, group_name='FOIP_IPBN_MOV_IGW_UPLINK', area="MOZAMBIQUE")
        intf_grp_hai_dct = dict(tent_id='hai018', gmt=-12, group_name='GLOBAL_HAITI_IPBN_IGW_UPLINK', area="HAITI")
        intf_grp_per_dct = dict(tent_id='per070', gmt=-12, group_name='GLOBAL_PERU_IPBN_IGW_UPLINK', area="PERU")
        intf_grp_bur_dct = dict(tent_id='bur040', gmt=-5, group_name='GLOBAL_BURUNDI_IPBN_IGW_UPLINK', area="BURUNDI")
        intf_grp_uplk_lst.append(intf_grp_tim_dct)
        intf_grp_uplk_lst.append(intf_grp_cam_dct)
        intf_grp_uplk_lst.append(intf_grp_moz_dct)
        intf_grp_uplk_lst.append(intf_grp_hai_dct)
        intf_grp_uplk_lst.append(intf_grp_per_dct)
        intf_grp_uplk_lst.append(intf_grp_bur_dct)
        tme_now = get_date_now()
        date_time_elastic_now = convert_date_to_elastic(tme_now)

        for area_obj in intf_grp_uplk_lst:
            tent_area = area_obj['tent_id']
            area = area_obj['area']
            date_time_now = get_date_now_gmt(area_obj['gmt'])
            date_time_ipms_now = convert_date_to_ipms(date_time_now)
            date_time_ipms_last_15_mins = get_date_minus_format_ipms(date_time_now, 15)

            intf_lst = _intf_obj.find_intf_from_grp_name(area_obj['tent_id'], area_obj['group_name'])
            poll_lst = poll_impl.lst(tent_area)
            if intf_lst:
                for x in intf_lst[0]:
                    x_host = x.get_host_obj(tent_area)
                    if x_host:
                        poll_name = x_host.poller
                        poll_obj = poll_impl.find(poll_name, poll_lst)
                        if poll_obj:
                            hst_tent = get_tenant_id(poll_obj.poller_ip)
                            max_in = x.get_max_in(date_time_ipms_last_15_mins, date_time_ipms_now, hst_tent, poll_name,
                                                  x.rid)
                            max_out = x.get_max_out(date_time_ipms_last_15_mins, date_time_ipms_now, hst_tent,
                                                    poll_name, x.rid)
                            node_begin = x_host.hostname
                            intf_desc = x.description
                            net_type = 'IPBN_UPSTREAM'
                            content_name = get_content(net_type, intf_desc)
                            max_intf = max_in
                            err = 'None'
                            tbl_intf_stat = TblIntfStatImpl(node_begin=x_host.hostname,
                                                            interface_name=x.name,
                                                            ip=x_host.ip,
                                                            area=area,
                                                            description=intf_desc,
                                                            node_type='IGW',
                                                            interface_status="UP",
                                                            interface_type="TRAFFFIC",
                                                            cable_name='',
                                                            speed=x.speed,
                                                            interface_id=x.id,
                                                            network_type='IPBN_UPSTREAM',
                                                            timestamp=date_time_elastic_now,
                                                            max=max_intf,
                                                            node_place=x_host.hostname[:2],
                                                            max_in=max_in,
                                                            max_out=max_out,
                                                            error=err,
                                                            class_map='',
                                                            content=content_name)
                            tbl_intf_stat_json = json.dumps(tbl_intf_stat, default=lambda o: o.__dict__)
                            id1 = uuid.uuid3(uuid.NAMESPACE_DNS, str(x.id) + node_begin +
                                             x.name + content_name + net_type + "_" + area + "_" +
                                             date_time_elastic_now)
                            srch_dct = dict(interface_id=x.id, timestamp=date_time_elastic_now,
                                            area=area, network_type=net_type)
                            hits = es.query_search_must(srch_dct, "tbl_interface_international_status")
                            if not hits:
                                # check xem truoc do khoang 13p co bi trung gi khong ?
                                if tbl_intf_stat.interv > 2:
                                    tme_last = get_date_minus(tme_now, tbl_intf_stat.interv - 2)
                                elif tbl_intf_stat.interv == 2:
                                    tme_last = get_date_minus(tme_now, tbl_intf_stat.interv - 1)
                                else:
                                    tme_last = get_date_minus_sec(tme_now, 30)

                                date_time_elastic_lst = convert_date_to_elastic(tme_last)
                                srch_tme_range_dct = dict(interface_id=x.id,
                                                          area=area, network_type=net_type)
                                hits_range = es.query_search_must_time_range(srch_tme_range_dct, date_time_elastic_lst,
                                                                             date_time_elastic_now,
                                                                             "tbl_interface_international_status")
                                if not hits_range:
                                    # insert new
                                    res = es.create_document("tbl_interface_international_status", id1.urn, "_doc",
                                                             tbl_intf_stat_json)
                                    print(res)
                            else:
                                print(json.dumps(hits, indent=4))
                                id1.urn = ''

    except Exception as err:
        err = str(err) + 'Error when get traffic of international link'
        logger.critical(err)


@celery.task
def get_interface_wanphy_wrong():
    logger = get_interface_wanphy_wrong.get_logger()
    logger.info('Get Interface Wanphy description wrong')

    dev_scan_lst = []
    _intf_wanphy_wrong_lst = []

    dev_scan_lst.append(ScanDeviceWanphy('PDL9104CKV91_OLD', "10.56.14.66", "Cisco", "diepnk", "Khongbiet@789"))
    dev_scan_lst.append(ScanDeviceWanphy('HLC9102CKV91', "10.60.78.2", "Cisco", "diepnk", "Khongbiet@789"))
    dev_scan_lst.append(ScanDeviceWanphy('HLC9105CKV92', "10.60.78.17", "Cisco", "diepnk", "Khongbiet@789"))
    dev_scan_lst.append(ScanDeviceWanphy('PDL9102CKV92', "10.56.14.57", "Cisco", "diepnk", "Khongbiet@789"))
    dev_scan_lst.append(ScanDeviceWanphy('NTH9205CKV91', "10.41.237.234", "Cisco", "diepnk", "Khongbiet@789"))
    dev_scan_lst.append(ScanDeviceWanphy('NTH9202CKV92', "10.41.237.82", "Cisco", "diepnk", "Khongbiet@789"))
    dev_scan_lst.append(ScanDeviceWanphy('HKH9103CKV91', "10.42.6.2", "Cisco", "diepnk", "Khongbiet@789"))
    dev_scan_lst.append(ScanDeviceWanphy('HKH9103CKV92', "10.42.6.10", "Cisco", "diepnk", "Khongbiet@789"))
    dev_scan_lst.append(ScanDeviceWanphy('HHT9402CKV91', "10.74.237.90", "Cisco", "diepnk", "Khongbiet@789"))
    dev_scan_lst.append(ScanDeviceWanphy('HHT9403CKV92', "10.74.237.132", "Cisco", "diepnk", "Khongbiet@789"))
    dev_scan_lst.append(ScanDeviceWanphy('HHT9602CKV93', "10.73.229.116", "Cisco", "diepnk", "Khongbiet@789"))
    dev_scan_lst.append(ScanDeviceWanphy('HHT9602CKV94', "10.73.229.132", "Cisco", "diepnk", "Khongbiet@789"))

    try:
        for x in dev_scan_lst:
            _intf_wanphy_wrong_lst += x.get_intf_lst()

        ystd = get_date_yesterday_format_ipms()
        msg = Message('List CKV with interface wanphy description wrong ' + str(ystd),
                      recipients=['thongplh@viettel.com.vn',
                                  'minhnd6@viettel.com.vn',
                                  'noc_ip@viettel.com.vn',
                                  'bo_ip@viettel.com.vn'],

                      sender='foip_vhkttc@viettel.com.vn')
        msg.body = 'Hi! This is List CKV wanphy interface description wrong:!'

        with app.app_context():

            msg_html = render_template('report/wanphy_interface_wrong.html', items=_intf_wanphy_wrong_lst)
            msg.html = msg_html
            mail.send(msg)

    except Exception as err:
        print(err)
        logger.critical(err)
    return 'OK'


@celery.task
def insert_interface_not_in_group_kv1():
    logger = get_interface_wanphy_wrong.get_logger()
    logger.info('Insert Interface Not In group FOIP_NEW KV1')
    try:
        time_now = get_time_format_now()
        intf_impl = TblInterfaceNotAddGroupImpl('KV1', '', '', '', 0, 'FOIP_IPBN', time_now, 0, 'active')
        intf_impl.check_and_save_intf()
    except Exception as err:
        print(err)
        logger.critical(err)


@celery.task
def insert_interface_not_in_group_kv2():
    logger = get_interface_wanphy_wrong.get_logger()
    logger.info('Insert Interface Not In group FOIP_NEW KV2')
    try:
        time_now = get_time_format_now()
        intf_impl = TblInterfaceNotAddGroupImpl('KV2', '', '', '', 0, 'FOIP_IPBN', time_now, 0, 'active')
        intf_impl.check_and_save_intf()
    except Exception as err:
        print(err)
        logger.critical(err)


@celery.task
def insert_interface_not_in_group_kv3():
    logger = get_interface_wanphy_wrong.get_logger()
    logger.info('Insert Interface Not In group FOIP_NEW KV1')
    try:
        time_now = get_time_format_now()
        intf_impl = TblInterfaceNotAddGroupImpl('KV3', '', '', '', 0, 'FOIP_IPBN', time_now, 0, 'active')
        intf_impl.check_and_save_intf()
    except Exception as err:
        print(err)
        logger.critical(err)

