__author__ = 'vtn-dhml-dhip10'
import os


class GetPathForTextFsm:
    def __init__(self, vendor):
        self.vendor = vendor

    def get_path(self, pth_name):
        pth = os.path.dirname(__file__)
        path_join = os.path.join(pth, "template")
        path_join = os.path.join(path_join, self.vendor)
        path_want = os.path.join(path_join, pth_name + ".textfsm")
        return path_want