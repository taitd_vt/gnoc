__author__ = 'vtn-dhml-dhip10'
import json
import uuid

import textfsm

from core.parsing.text_fsm import TxtFSM
from core.helpers.int_helpers import convert_string_to_int
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.parsing.get_path_for_textfsm import GetPathForTextFsm
from core.helpers.stringhelpers import convert_intf_short, get_name_service_clsm, \
    is_node_manual, get_content, get_cable_name
from app.api.vipa.parsing.api_parsing import ApiParsing
from core.helpers.fix_helper import get_node_place
from core.helpers.stringhelpers import convert_api_vipa_xml_to_dct
from core.database.impl.ipms.intf_ipms_impl import InterfaceImpsImpl
from core.database.impl.elasticsearch.tbl_intf_stat_impl import TblIntfStatImpl


class DeviceVipa:
    def __init__(self, name, vendor, version, username, password):
        self.name = name
        self.vendor = vendor
        self.version = version
        self.username = username
        self.password = password

    def show_command(self, cmd, cmd_name, para_lst, path_file_chk):
        fsm_results_lst = []
        _api_vipa = ApiParsing(cmd_name, cmd, para_lst, self.vendor, self.version,
                               self.name, self.username, self.password)
        _api_req = _api_vipa.request()
        if _api_req:
            _cmd_res = convert_api_vipa_xml_to_dct(_api_req)
            if _cmd_res and path_file_chk:
                re_table = textfsm.TextFSM(open(path_file_chk))
                fsm_results_lst = re_table.ParseText(_cmd_res)

        return fsm_results_lst

    def get_intf_desc_upstream(self, pth_upstream):
        cmd = "show interface description  | inc Upstream | exclude down"
        cmd_name = "<minhnd6><" + self.vendor + "><" + self.version + "><check interface upstream up>"

        rst_show = self.show_command(cmd, cmd_name, [], pth_upstream)
        inte_dct = dict(interface=0, description=1)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        intf_lst = fsm_obj.divide_lst(frm_obj_lst, "interface")
        des_lst = fsm_obj.divide_lst(frm_obj_lst, "description")
        # loai bo ki tu trong
        intf_lst = [x.strip() for x in intf_lst]
        des_lst = [x.strip() for x in des_lst]

        return intf_lst, des_lst

    def get_intf_desc_backhaul(self, pth_upstream):
        cmd = "show interface description  | inc Cap_ | exclude down"
        cmd_name = "<minhnd6><" + self.vendor + "><" + self.version + "><check interface backhaul up>"

        rst_show = self.show_command(cmd, cmd_name, [], pth_upstream)
        inte_dct = dict(interface=0, description=1)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        intf_lst = fsm_obj.divide_lst(frm_obj_lst, "interface")
        des_lst = fsm_obj.divide_lst(frm_obj_lst, "description")

        intf_lst = [x.strip() for x in intf_lst]
        des_lst = [x.strip() for x in des_lst]

        return intf_lst, des_lst

    def get_bandwidth_link(self, x_intf, pth_bw):
        cmd = "show interface @{interfaceID} | inc BW"
        cmd_name = "<minhnd6><" + self.vendor + "><" + self.version + "><check interface bandwidth>"

        para_dct = dict(para_key="interfaceID", para_val=x_intf)
        para_lst = [para_dct]

        rst_show = self.show_command(cmd, cmd_name, para_lst, pth_bw)
        inte_dct = dict(bw=0)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        bw_lst = fsm_obj.divide_lst(frm_obj_lst, "bw")
        intf_bw = 0
        if bw_lst:
            intf_bw = convert_string_to_int(bw_lst[0])
        # convert to 1000 vi no la kbit

        return intf_bw * 1000

    def get_intf_bw_lst(self, pth_bw):
        cmd = "show interface br | exclude tt | exclude BE | exclude CPU | exclude Nu | exclude Lo | exclude ti"
        cmd_name = "<minhnd6><" + self.vendor + "><" + self.version + "><check interface br list>"

        rst_show = self.show_command(cmd, cmd_name, [], pth_bw)
        inte_dct = dict(interface=0, phy_status=1, interface_bw=2)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        # convert them * 1000
        for x_obj in frm_obj_lst:
            if 'interface_bw' in x_obj:
                intf_bw_str = x_obj['interface_bw']
                intf_bw_int = convert_string_to_int(intf_bw_str)
                x_obj['interface_bw'] = intf_bw_int * 1000
            if 'interface' in x_obj:
                x_obj['interface'] = x_obj['interface'].strip()
        return frm_obj_lst

    def get_intf_des_lst(self, pth_bw):
        cmd = "show interface description | exclude tt | exclude BE | exclude CPU | exclude Nu | exclude Lo"
        cmd_name = "<minhnd6><" + self.vendor + "><" + self.version + "><check interface des list>"

        rst_show = self.show_command(cmd, cmd_name, [], pth_bw)

        inte_dct = dict(interface=0, description=3)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        for x in frm_obj_lst:
            if 'interface' in x:
                x['interface'] = x['interface'].strip()
            if 'description' in x:
                x['description'] = x['description'].strip()

        return frm_obj_lst

    def get_description_link(self, x_intf, pth_desc):
        cmd = "show interface @{interfaceID} | inc Des"
        cmd_name = "<minhnd6><" + self.vendor + "><" + self.version + "><check interface link description>"

        para_dct = dict(para_key="interfaceID", para_val=x_intf)
        para_lst = [para_dct]

        rst_show = self.show_command(cmd, cmd_name, para_lst, pth_desc)
        inte_dct = dict(description=0)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        des_lst = fsm_obj.divide_lst(frm_obj_lst, "description")
        intf_des = ''
        if des_lst:
            intf_des = des_lst[0].strip()
        return intf_des

    def get_traffic_link(self, x_intf, pth_bw_inp_outp):
        cmd = "show interface @{interfaceID} | inc  rate"
        cmd_name = "<minhnd6><" + self.vendor + "><" + self.version + "><check interface link rate>"

        para_dct = dict(para_key="interfaceID", para_val=x_intf)
        para_lst = [para_dct]

        rst_show = self.show_command(cmd, cmd_name, para_lst, pth_bw_inp_outp)
        inte_dct = dict(bw_input=0, bw_output=1)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        bw_inp = fsm_obj.divide_lst(frm_obj_lst, "bw_input")
        bw_outp = fsm_obj.divide_lst(frm_obj_lst, "bw_output")
        # convert list to int
        intf_bw_in = intf_bw_out = 0
        if bw_inp:
            intf_bw_in = convert_string_to_int(bw_inp[0])
        if bw_outp:
            intf_bw_out = convert_string_to_int(bw_outp[0])
        return intf_bw_in, intf_bw_out

    def get_class_map_traff_link(self, x_intf, pth_class_map):
        cmd = "show policy-map interface @{interfaceID} input"
        cmd_name = "<minhnd6><" + self.vendor + "><" + self.version + "><check interface link policy-map input>"

        para_dct = dict(para_key="interfaceID", para_val=x_intf)
        para_lst = [para_dct]

        rst_show = self.show_command(cmd, cmd_name, para_lst, pth_class_map)

        inte_dct = dict(class_map=0, rate=1)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        clsm_name_lst = fsm_obj.divide_lst(frm_obj_lst, "class_map")
        rate_lst = fsm_obj.divide_lst(frm_obj_lst, "rate")
        # filter lai lít loc bo zero
        index_lst = - 1
        clsm_name_flt_lst = list()
        rate_flt_lst = list()
        for x in rate_lst:
            index_lst += 1
            try:

                if float(x) > 0:
                    # do du lieu la kbps convert ve bps
                    y = float(x) * 1000
                    clsm_name_flt_lst.append(clsm_name_lst[index_lst])
                    rate_flt_lst.append(y)
            except Exception as err:
                print(err)
                return clsm_name_flt_lst, rate_flt_lst

        return clsm_name_flt_lst, rate_flt_lst

    def get_link_class_map_lst(self, pth_class_map_link_lst):
        cmd = "show policy-map interface all | inc input | exclude CKV | exclude KDDI | exclude MARKING_CLASS | " \
              "exclude MARKING_EF | exclude VIP | exclude GBPS | exclude PCCW_PM"
        cmd_name = "<minhnd6><" + self.vendor + "><" + self.version + "><check policy map interface list>"

        rst_show = self.show_command(cmd, cmd_name, [], pth_class_map_link_lst)

        inte_dct = dict(interface=0)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        class_map_lst = fsm_obj.divide_lst(frm_obj_lst, "interface")
        # drop bot class co chua may chu link tinh
        class_map_filter_lst = [x for x in class_map_lst if str(x).upper().find("SHOW") < 0]

        return class_map_filter_lst

    def get_intf_desc_tengig(self, pth):
        cmd = "show interface description | include Te"
        cmd_name = "<minhnd6><" + self.vendor + "><" + self.version + "><check interface Ten exclude down>"

        rst_show = self.show_command(cmd, cmd_name, [], pth)

        inte_dct = dict(interface=0, status=1, description=3)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        for x in frm_obj_lst:
            if 'interface' in x:
                x['interface'] = x['interface'].strip()
            if 'description' in x:
                x['description'] = x['description'].strip()

        return frm_obj_lst

    def get_intf_wanphy(self, pth):
        cmd = "show controller wanphy ?"
        cmd_name = "<minhnd6><" + self.vendor + "><" + self.version + "><show controller wanphy>"

        rst_show = self.show_command(cmd, cmd_name, [], pth)

        inte_dct = dict(interface=0)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        for x in frm_obj_lst:
            if 'interface' in x:
                x['interface'] = x['interface'].strip()

        return frm_obj_lst

    def check_intf_wanphy(self, x_intf, pth):
        cmd = "show controllers wanphy  @{interface} all | inc WAN"
        cmd_name = "<MinhND6><" + self.vendor + "><" + self.version + "><show interface mode WAN>"

        para_dct = dict(para_key="interface", para_val=x_intf)
        para_lst = [para_dct]

        rst_show = self.show_command(cmd, cmd_name, para_lst, pth)

        inte_dct = dict(wanphy=0)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        check = False
        try:
            if frm_obj_lst:
                if isinstance(frm_obj_lst, list):
                    intf_dct = frm_obj_lst[0]
                    if 'wanphy' in intf_dct:
                        intf_mode = intf_dct['wanphy']
                        if intf_mode.upper() == 'WAN':
                            check = True
        except Exception as err:
            print('Error %s when check interface wanphy %s' % err, x_intf)
            return check
        return check

    def get_drop_of_interface(self, x_intf, pth):
        if self.vendor.upper() == 'CISCO':
            cmd = "show policy-map interface @{interface} output | i Total Dropped"
        elif self.vendor.upper() == 'JUNIPER':
            cmd = 'show interfaces descriptions | except irb | except lsi | except ge | except ae | except fxp  | except down'
        else:
            cmd = ''
        cmd_name = "<minhnd6><" + self.vendor + "><" + self.version + "><show policy map interface drop>"

        para_dct = dict(para_key="interface", para_val=x_intf)
        para_lst = [para_dct]
        rst_show = self.show_command(cmd, cmd_name, para_lst, pth)

        inte_dct = dict(class_x=0)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        indx = 0
        res_lst = []
        for x in frm_obj_lst:
            if indx == 0:
                res_lst.append(dict(class_05=x['class_x']))
            if indx == 1:
                res_lst.append(dict(class_04=x['class_x']))
            if indx == 2:
                res_lst.append(dict(class_03=x['class_x']))
            if indx == 3:
                res_lst.append(dict(class_01=x['class_x']))
            if indx == 4:
                res_lst.append(dict(class_default=x['class_x']))
            indx += 1

        return res_lst

    def get_crc_of_interface(self, x_intf, pth):
        if self.vendor.upper() == 'CISCO':
            cmd = "show interface @{interface} | i CRC"
        elif self.vendor.upper() == 'JUNIPER':
            cmd = 'show interfaces @{interface}  extensive | match CRC'
            x_intf = x_intf.lower()
        elif self.vendor.upper() == 'HP':
            cmd = 'display interface @{interface} | inc CRC'
        else:
            cmd = ''
        cmd_name = "<minhnd6><" + self.vendor + "><" + self.version + "><show interface crc>"

        para_dct = dict(para_key="interface", para_val=x_intf)
        para_lst = [para_dct]
        rst_show = self.show_command(cmd, cmd_name, para_lst, pth)

        inte_dct = dict(crc=0)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        for x in frm_obj_lst:
            if 'crc' in x:
                x['crc'] = x['crc'].strip()

        return frm_obj_lst


if __name__ == '__main__':
    _dev = DeviceVipa("HLC9206SPI01", "HP", "H_P5940", "minhnd6", "Qazwsx@123")
    from core.parsing.scan_dev_wanphy import ScanDeviceWanphy
    from core.threading.device_thread.dev_show_interface_crc_thread import DeviceShowInterfaceCrcThread
    #_max = ScanDeviceWanphy("HHT9602CKV94", "10.73.229.132", "Cisco", "diepnk", "Khongbiet@789")
    _crc = DeviceShowInterfaceCrcThread(False, 0, '10.240.180.202', '', '', '', '', 0, 0)
    pth_crc = _crc.get_path()
    #pth_wanphy, pth_intf_des, pth_intf_wanphy = _max.get_path()

    test = _dev.get_crc_of_interface('FortyGigE1/1/1', pth_crc)
    #print(test)
