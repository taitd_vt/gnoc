__author__ = 'VTN-DHML-DHIP10'

import os
import uuid

from config import Development
from app.api.vipa.device.api_device import ApiDevice
from core.parsing.vipa_parsing.device_vipa import DeviceVipa
from core.helpers.date_helpers import get_date_now_format_ipms
from core.helpers.stringhelpers import is_link_phy

config = Development()
SERVER_PARSING = config.__getattribute__('SERVER_PARSING')
SERVER_PARSING_USER = config.__getattribute__('SERVER_PARSING_USER')
SERVER_PARSING_PASS = config.__getattribute__('SERVER_PARSING_PASS')


class ScanDeviceWanphy:
    def __init__(self, hst_nme, ip, vendor, username, passwd):
        self.host_name = hst_nme
        self.ip = ip
        self.vendor = vendor
        self.username = username
        self.password = passwd

    def get_path(self):
        pth = os.path.dirname(__file__)
        path_join = os.path.join(pth, "template")
        path_join = os.path.join(path_join, self.vendor.lower())
        path_intf_wanphy_lst = os.path.join(path_join, "show_controller_wanphy.textfsm")
        path_des_lst = os.path.join(path_join, "show_interface_description.textfsm")
        path_intf_wanphy_detail_lst = os.path.join(path_join, "show_controller_interface_wanphy_detail.textfsm")
        return path_intf_wanphy_lst, path_des_lst, path_intf_wanphy_detail_lst

    def get_intf_lst(self):
        intf_mer_lst = list()
        res_lst = list()
        # Connect to the client
        try:

            pth_wanphy, pth_intf_des, pth_intf_wanphy = self.get_path()

            _api_node = ApiDevice(self.ip, 'VNM')
            node_code_vipa, node_vend, node_vers = _api_node.get_name_ven_ver()
            if node_code_vipa and node_vend and node_vers:
                dev = DeviceVipa(node_code_vipa, node_vend, node_vers, self.username, self.password)

                intf_wanphy_lst = dev.get_intf_wanphy(pth_wanphy)
                intf_des_lst = dev.get_intf_desc_tengig(pth_intf_des)
                for intf in intf_wanphy_lst:
                    if 'interface' in intf:
                        intf_name = intf['interface']
                        intf_name = intf_name.strip()
                        # check chi tiet xem co phai la wanphy khong dua theo lenh show controller wanphy X all
                        check_wanphy = dev.check_intf_wanphy(intf_name, pth_intf_wanphy)

                        # tim ra dict can thiet
                        intf_dct = self.get_intf_wanphy_tengig_frm_int_lst(intf_name, intf_des_lst)
                        if intf_dct:
                            # Port nay la port wanphy
                            if 'status' in intf_dct:
                                intf_stat = intf_dct['status']
                                if intf_stat == 'up':
                                    if 'description' in intf_dct and 'interface' in intf_dct:
                                        intf_des = intf_dct['description'].upper()
                                        if intf_des.find('TO_') == 0:
                                            intf_des = intf_des.replace('TO_', '')
                                        intf_des_adv = ''
                                        if check_wanphy:
                                            if intf_des.find('PEDD') >= 0 or intf_des.find('PEPS') >= 0:
                                                if intf_des.find('WANPHY_8.5_GBPS') < 0:
                                                    intf_des = intf_des.replace('WANPHY_', '')
                                                    intf_des = intf_des.replace('WANPHY', '')
                                                    intf_des_adv = 'WANPHY_8.5_GBPS_' + intf_des
                                            else:
                                                if intf_des.find('WANPHY_9.1_GBPS') < 0:
                                                    intf_des = intf_des.replace('WANPHY_', '')
                                                    intf_des = intf_des.replace('WANPHY', '')
                                                    intf_des_adv = 'WANPHY_9.1_GBPS_' + intf_des
                                        else:
                                            if intf_des.find('PEDD') >= 0 or intf_des.find('PEPS') >= 0:
                                                if intf_des.find('LANPHY_9.5_GBPS') < 0:
                                                    intf_des = intf_des.replace('LANPHY_', '')
                                                    intf_des = intf_des.replace('LANPHY', '')
                                                    intf_des_adv = 'LANPHY_9.5_GBPS_' + intf_des
                                            else:
                                                if intf_des.find('LANPHY') < 0:
                                                    intf_des_adv = 'LANPHY_9.5_GBPS_' + intf_des
                                        if check_wanphy:
                                            if intf_des.find('WANPHY') < 0:
                                                res_lst.append(dict(interface=intf_dct['interface'],
                                                                    node_name=self.host_name,
                                                                    node_ip=self.ip,
                                                                    description=intf_des,
                                                                    result='Description not have WANPHY',
                                                                    advise=intf_des_adv))
                                            elif intf_des.find('LANPHY') >= 0:
                                                res_lst.append(dict(interface=intf_dct['interface'],
                                                                    node_name=self.host_name,
                                                                    node_ip=self.ip,
                                                                    description=intf_des,
                                                                    result='Description LANPHY wrong',
                                                                    advise=intf_des_adv))
                                            elif intf_des_adv:
                                                res_lst.append(dict(interface=intf_dct['interface'],
                                                                    node_name=self.host_name,
                                                                    node_ip=self.ip,
                                                                    description=intf_des,
                                                                    result='Description should edit some',
                                                                    advise=intf_des_adv))
                                        else:
                                            if intf_des.find('WANPHY') >= 0:
                                                res_lst.append(dict(interface=intf_dct['interface'],
                                                                    node_name=self.host_name,
                                                                    node_ip=self.ip,
                                                                    description=intf_des,
                                                                    result='Description WANPHY wrong',
                                                                    advise=intf_des_adv))
                                            elif intf_des.find('LANPHY') < 0:
                                                res_lst.append(dict(interface=intf_dct['interface'],
                                                                    node_name=self.host_name,
                                                                    node_ip=self.ip,
                                                                    description=intf_des,
                                                                    result='Interface Lanphy but description '
                                                                           'not have LANPHY',
                                                                    advise=intf_des_adv))
                                            elif intf_des.find('LANPHY') >= 0 and intf_des_adv:
                                                res_lst.append(dict(interface=intf_dct['interface'],
                                                                    node_name=self.host_name,
                                                                    node_ip=self.ip,
                                                                    description=intf_des,
                                                                    result='Description LANPHY OK',
                                                                    advise=intf_des_adv))
                                            elif intf_des_adv:
                                                res_lst.append(dict(interface=intf_dct['interface'],
                                                                    node_name=self.host_name,
                                                                    node_ip=self.ip,
                                                                    description=intf_des,
                                                                    result='Description should edit some',
                                                                    advise=intf_des_adv))
                                                # loai bo interface sub va linh tinh

        except Exception as err:
            print('Error when scan port interface list of POP ' + str(err))
            return res_lst

        return res_lst

    @staticmethod
    def get_des_frm_intf(intf_name, des_lst):
        result = ''
        for x in des_lst:
            if 'interface' in x:
                intf = x['interface']
                if 'description' in x:
                    intf_des = x['description']
                    if intf.strip() == intf_name.strip():
                        return intf_des
        return result

    @staticmethod
    def get_intf_wanphy_tengig_frm_int_lst(intf_wan_name, intf_stat_des_lst):
        # dinh dang cua intf_stat_des_lst la dict(interface,status,description)
        # interface intf_name can them Te dang truoc de giong interface binh thuong
        intf_wan_name = 'Te' + intf_wan_name
        for x in intf_stat_des_lst:
            if 'interface' in x:
                intf_in_lst = x['interface'].upper()
                if intf_wan_name.upper() == intf_in_lst:
                    return x
        return dict()


if __name__ == '__main__':
    dev_scan_lst = []
    _intf_wanphy_wrong_lst = []

    # dev_scan_lst.append(ScanDeviceWanphy('PDL9104CKV91_OLD', "10.56.14.66", "Cisco", "diepnk", "Khongbiet@789"))
    # dev_scan_lst.append(ScanDeviceWanphy('HLC9102CKV91', "10.60.78.2", "Cisco", "diepnk", "Khongbiet@789"))
    # dev_scan_lst.append(ScanDeviceWanphy('HLC9105CKV92', "10.60.78.17", "Cisco", "diepnk", "Khongbiet@789"))
    # dev_scan_lst.append(ScanDeviceWanphy('PDL9102CKV92', "10.56.14.57", "Cisco", "diepnk", "Khongbiet@789"))
    # dev_scan_lst.append(ScanDeviceWanphy('NTH9205CKV91', "10.41.237.234", "Cisco", "diepnk", "Khongbiet@789"))
    # dev_scan_lst.append(ScanDeviceWanphy('NTH9202CKV92', "10.41.237.82", "Cisco", "diepnk", "Khongbiet@789"))
    # dev_scan_lst.append(ScanDeviceWanphy('HKH9103CKV91', "10.42.6.2", "Cisco", "diepnk", "Khongbiet@789"))
    # dev_scan_lst.append(ScanDeviceWanphy('HKH9103CKV92', "10.42.6.10", "Cisco", "diepnk", "Khongbiet@789"))
    # dev_scan_lst.append(ScanDeviceWanphy('HHT9402CKV91', "10.74.237.90", "Cisco", "diepnk", "Khongbiet@789"))
    dev_scan_lst.append(ScanDeviceWanphy('HHT9403CKV92', "10.74.237.132", "Cisco", "diepnk", "Khongbiet@789"))
    dev_scan_lst.append(ScanDeviceWanphy('HHT9602CKV93', "10.73.229.116", "Cisco", "diepnk", "Khongbiet@789"))
    dev_scan_lst.append(ScanDeviceWanphy('HHT9602CKV94', "10.73.229.132", "Cisco", "diepnk", "Khongbiet@789"))
    for x in dev_scan_lst:
        _intf_wanphy_wrong_lst += x.get_intf_lst()


