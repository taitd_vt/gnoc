__author__ = 'vtn-dhml-dhip10'
import socket
import time
import re
import json
import uuid

import textfsm

from core.parsing.text_fsm import TxtFSM
from core.helpers.int_helpers import convert_string_to_int
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.parsing.get_path_for_textfsm import GetPathForTextFsm
from core.helpers.stringhelpers import convert_intf_short, get_name_service_clsm, \
                                        is_node_manual, get_content, get_cable_name
from core.helpers.fix_helper import get_node_place
from core.database.impl.ipms.intf_ipms_impl import InterfaceImpsImpl
from core.database.impl.elasticsearch.tbl_intf_stat_impl import TblIntfStatImpl
from core.database.__init__ import es


class Device:

    def __init__(self, name, ip_add, vendor, version, username, password):
        self.name = name
        self.ip_add = ip_add
        self.vendor = vendor.upper()
        self.version = version
        self.username = username
        self.password = password

    def wait_stop(self):
        stop_chr = ""
        if self.vendor == 'CISCO':
            stop_chr = "#"

        if self.vendor == 'HUAWEI':
            stop_chr = ">"

        if self.vendor == 'ZTE':
            stop_chr = "#"

        if self.vendor == 'JUNIPER':
            stop_chr = ">"

        if self.vendor == 'LINUX':
            stop_chr = "$"

        return stop_chr

    def wait_stop_config(self):
        stop_chr = ""
        if self.vendor == 'CISCO':
            stop_chr = "(config)#"

        if self.vendor == 'HUAWEI':
            stop_chr = "#"

        if self.vendor == 'ZTE':
            stop_chr = "#"

        if self.vendor == 'JUNIPER':
            stop_chr = ">"

        if self.vendor == 'LINUX':
            stop_chr = "$"

        return stop_chr

    def command_config_terminal(self):
        cmd = ""
        if self.vendor == 'CISCO':
            cmd = "configure terminal \n"

        if self.vendor == 'HUAWEI':
            cmd = "sys \n"

        if self.vendor == 'ZTE':
            cmd = "configure terminal \n"

        if self.vendor == 'JUNIPER':
            cmd = ">"

        return cmd

    def command_exit(self):
        cmd = ""
        if self.vendor == 'CISCO':
            cmd = "exit \n"

        if self.vendor == 'HUAWEI':
            cmd = "quit \n"

        if self.vendor == 'ZTE':
            cmd = "exit \n"

        if self.vendor == 'JUNIPER':
            cmd = ">"
        if self.vendor == 'LINUX':
            cmd = "$"

        return cmd

    def wait_str_login(self):
        stop_chr = ""
        if self.vendor == 'CISCO':
            stop_chr = ":"

        if self.vendor == 'HUAWEI':
            stop_chr = ":"

        if self.vendor == 'ZTE':
            stop_chr = ":"

        if self.vendor == 'JUNIPER':
            stop_chr = ":"
        return stop_chr

    def telnet(self, shell):
        result = 'True'
        character_stop = self.wait_stop()
        character_username = self.wait_str_login()
        character_stop_config = self.wait_stop_config()
        ip = self.ip_add
        username = self.username
        password = self.password

        if username != "":
            check_telnet_node = send_string_wait_string('telnet ' + ip + '\n', character_username, 15, shell)
            if check_telnet_node:
                password_result = send_string_wait_string(username + '\n', 'assword:', 15, shell)
                if password_result:
                    telnet_result = send_string_wait_string(password + '\n', character_stop, 15, shell)
                    if telnet_result:
                        result = 'True'
                    else:
                        result = 'Sai Password'
                else:
                    #maybe no userlogin just only password
                    #send enable and wait #
                    send_string_wait_string('enable \n', 'assword:', 15, shell)
                    prompt_result = send_string_wait_string(password + '\n', character_stop_config, 15, shell)
                    if prompt_result:
                        result = 'True'
                    else:
                        result = 'Sai password enable'

            else:
                result = 'Telnet No OK'

        else:
            #truong hop khong co username:
            check_telnet_node = send_string_wait_string('telnet ' + ip + '\n', character_username, 15, shell)
            if check_telnet_node:
                    telnet_result = send_string_wait_string(password + '\n', character_stop, 15, shell)
                    if telnet_result:
                        result = 'True'
                    else:
                        result = 'Sai Password'
        return result

    def telnet_via(self, shell, device_via):
        result = device_via.telnet(shell)
        if result == 'True':
            result_teln_nxt = self.telnet(shell)
            return result_teln_nxt
        else:
            result == 'False'
        return result

    def show_command(self, command, path_file_chk, shell):
        chr_stop = self.wait_stop()
        result_send_cmd = command_cli(command, chr_stop, shell)
        fsm_results = []
        if path_file_chk != "":
            re_table = textfsm.TextFSM(open(path_file_chk))
            fsm_results = re_table.ParseText(result_send_cmd)
        else:
            fsm_results.append(result_send_cmd)
        return fsm_results

    def config(self, shell):
        check = 'True'
        cmd_config = self.command_config_terminal()
        chr_stop = self.wait_stop_config()
        result_send_cmd = command_cli(cmd_config, chr_stop, shell)
        if 'Error' in result_send_cmd:
            check = 'Error for some thing'
        else:
            check = 'True'
        return check

    def exit_config(self, shell):
        check = 'False'
        cmd_exit_config = self.command_exit()
        chr_stop = self.wait_stop()
        result_send_cmd = command_cli(cmd_exit_config, chr_stop, shell)
        while check == 'False':
            if '(config)' in result_send_cmd:
                result_send_cmd = command_cli(cmd_exit_config, chr_stop, shell)
            elif 'Error' in result_send_cmd:
                result_send_cmd = command_cli('no', chr_stop, shell)
            else:
                check = 'True'

        return check

    def config_device(self, cmd, shell):
        check = 'True'
        chr_stop = self.wait_stop()
        result_send_cmd = command_cli(cmd, chr_stop, shell)
        if 'Error' in result_send_cmd:
            check = 'Error for some thing'
        else:
            check = 'True'
        return check

    def exit_device(self, shell):
        check = 'False'
        cmd_exit = self.command_exit()
        chr_stop = self.wait_stop()
        number_type = 0
        while check == 'False':
            result_send_cmd = command_cli(cmd_exit, chr_stop, shell)
            number_type += 1
            if '$' in result_send_cmd:
                check = 'True'
            else:
                check = '$'
            if number_type > 10:
                check = 'True'
        return check

    def get_vendor(self):
        return self.vendor

    def get_version(self):
        return self.version

    def get_name(self):
        return self.name

    def get_ip(self):
        return self.ip_add

    def get_intf_desc_upstream(self, pth_upstream, shell):
        cmd_show = "show interface description  | inc Upstream | exclude down \n"
        rst_show = self.show_command(cmd_show, pth_upstream, shell)
        inte_dct = dict(interface=0, description=1)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        intf_lst = fsm_obj.divide_lst(frm_obj_lst, "interface")
        des_lst = fsm_obj.divide_lst(frm_obj_lst, "description")
        return intf_lst, des_lst

    def get_intf_desc_backhaul(self, pth_upstream, shell):
        cmd_show = "show interface description  | inc Cap_ | exclude down \n"
        rst_show = self.show_command(cmd_show, pth_upstream, shell)
        inte_dct = dict(interface=0, description=1)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        intf_lst = fsm_obj.divide_lst(frm_obj_lst, "interface")
        des_lst = fsm_obj.divide_lst(frm_obj_lst, "description")
        return intf_lst, des_lst

    def get_bandwidth_link(self, x_intf, pth_bw, shell):
        cmd_show = "show interface " + x_intf + " | inc BW \n"
        rst_show = self.show_command(cmd_show, pth_bw, shell)
        inte_dct = dict(bw=0)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        bw_lst = fsm_obj.divide_lst(frm_obj_lst, "bw")
        intf_bw = 0
        if bw_lst:
            intf_bw = convert_string_to_int(bw_lst[0])
        # convert to 1000 vi no la kbit

        return intf_bw * 1000

    def get_intf_bw_lst(self, pth_bw, shell):
        cmd_show = "show interface br | exclude tt | exclude BE | exclude CPU | exclude Nu | exclude Lo | exclude ti \n"
        rst_show = self.show_command(cmd_show, pth_bw, shell)
        inte_dct = dict(interface=0, phy_status=1, interface_bw=2)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        # convert them * 1000
        for x_obj in frm_obj_lst:
            if 'interface_bw' in x_obj:
                intf_bw_str = x_obj['interface_bw']
                intf_bw_int = convert_string_to_int(intf_bw_str)
                x_obj['interface_bw'] = intf_bw_int * 1000
        return frm_obj_lst

    def get_intf_des_lst(self, pth_bw, shell):
        cmd_show = "show interface description | exclude tt | exclude BE | exclude CPU | exclude Nu | exclude Lo  \n"
        rst_show = self.show_command(cmd_show, pth_bw, shell)
        inte_dct = dict(interface=0, description=3)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        # convert them * 1000
        return frm_obj_lst

    def get_description_link(self, x_intf, pth_desc, shell):
        cmd_show = "show interface " + x_intf + " | inc Des \n"
        rst_show = self.show_command(cmd_show, pth_desc, shell)
        inte_dct = dict(description=0)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        des_lst = fsm_obj.divide_lst(frm_obj_lst, "description")
        intf_des = ''
        if des_lst:
            intf_des = des_lst[0]
        return intf_des

    def get_traffic_link(self, x_intf, pth_bw_inp_outp, shell):
        cmd_show = "show interface " + x_intf + " | inc rate \n"
        rst_show = self.show_command(cmd_show, pth_bw_inp_outp, shell)
        inte_dct = dict(bw_input=0, bw_output=1)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        bw_inp = fsm_obj.divide_lst(frm_obj_lst, "bw_input")
        bw_outp = fsm_obj.divide_lst(frm_obj_lst, "bw_output")
        # convert list to int
        intf_bw_in = intf_bw_out = 0
        if bw_inp:
            intf_bw_in = convert_string_to_int(bw_inp[0])
        if bw_outp:
            intf_bw_out = convert_string_to_int(bw_outp[0])
        return intf_bw_in, intf_bw_out

    def get_class_map_traff_link(self, x_intf, pth_class_map, shell):
        cmd_show = "show policy-map interface " + x_intf + " input \n"
        rst_show = self.show_command(cmd_show, pth_class_map, shell)
        inte_dct = dict(class_map=0, rate=1)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        clsm_name_lst = fsm_obj.divide_lst(frm_obj_lst, "class_map")
        rate_lst = fsm_obj.divide_lst(frm_obj_lst, "rate")
        # filter lai lít loc bo zero
        index_lst = - 1
        clsm_name_flt_lst = list()
        rate_flt_lst = list()
        for x in rate_lst:
            index_lst += 1
            try:

                if float(x) > 0:
                    # do du lieu la kbps convert ve bps
                    y = float(x) * 1000
                    clsm_name_flt_lst.append(clsm_name_lst[index_lst])
                    rate_flt_lst.append(y)
            except Exception as err:
                print(err)
                return clsm_name_flt_lst, rate_flt_lst

        return clsm_name_flt_lst, rate_flt_lst

    def get_link_class_map_lst(self, pth_class_map_link_lst, shell):
        cmd_show = "show policy-map interface all | inc input | exclude CKV | exclude KDDI | exclude MARKING_CLASS " \
                   "| exclude MARKING_EF | exclude VIP | exclude GBPS | exclude PCCW_PM \n"
        rst_show = self.show_command(cmd_show, pth_class_map_link_lst, shell)
        inte_dct = dict(interface=0)
        fsm_obj = TxtFSM(rst_show, inte_dct)
        frm_obj_lst = fsm_obj.get_want_lst()
        class_map_lst = fsm_obj.divide_lst(frm_obj_lst, "interface")
        # drop bot class co chua may chu link tinh
        class_map_filter_lst = [x for x in class_map_lst if str(x).upper().find("SHOW") < 0]

        return class_map_filter_lst

    def insert_elastic_upstream_backhaul(self, x_intf, intf_desc, host_ipms_obj, date_time_ipms_last_15_mins,
                                         date_time_ipms_now, tenant_id, poller_name, date_time_elastic_now, shell,
                                         net_type, id_intf_manual, host_type):
        id1 = uuid.uuid3(uuid.NAMESPACE_DNS, '')
        tbl_intf_stat_json = None
        try:
            device_test = Device(host_ipms_obj.hostname, host_ipms_obj.ip, host_ipms_obj.vendor, host_ipms_obj.type,
                                 'dungnd18', 'dungnd18@123')

            get_path_obj = GetPathForTextFsm('cisco')
            pth_bw = get_path_obj.get_path('show_interface_inc_bandwidth')
            pth_bw_inp_outp = get_path_obj.get_path('show_interface_input_output_bandwidth')

            # check interface name in IPMS
            rid = host_ipms_obj.rid
            api_spring_intf_ipms = ApiSpringIpmsImpl('interface', 'kv3068')

            node_place = get_node_place(self.get_name(), intf_desc)
            intf_name = convert_intf_short(host_ipms_obj.vendor, x_intf)
            # So Spring boot phan biet / tren URL nen chuyen / thanh _
            intf_name_under_line = intf_name.replace("/", "_", 100)
            srch_intf_name_dct = {'searchRid': rid,
                                  'searchInterfaceName': intf_name_under_line}
            # Tim interface id va interface name tren ipms
            # Tinh ra cable name va service name luon

            cable_name = get_cable_name(net_type, intf_desc)
            content_name = get_content(net_type, intf_desc)
            node_begin = device_test.get_name()
            is_manual = is_node_manual(node_begin)

            res_intf_lst = api_spring_intf_ipms.get(srch_intf_name_dct)
            check_intf_ipms = False
            if res_intf_lst:
                for ipms_intf in res_intf_lst:
                    if 'speed' in ipms_intf:
                        intf_speed = convert_string_to_int(ipms_intf['speed'])
                        if intf_speed > 0:
                            if 'id' in ipms_intf and 'name' in ipms_intf and 'description' in ipms_intf and 'status' in ipms_intf:
                                intf_ipms_obj = InterfaceImpsImpl(ipms_intf['id'], ipms_intf['name'], rid,
                                                                  ipms_intf['speed'], ipms_intf['description'],
                                                                  ipms_intf['status'])
                                if intf_ipms_obj.check_link_physical():

                                    # tinh max In cua Upstream
                                    if tenant_id:
                                        # Neu la Upstream tính chiều in mà nếu là backhaul tính chiều out
                                        max_intp = intf_ipms_obj.get_max_in(date_time_ipms_last_15_mins,
                                                                               date_time_ipms_now,
                                                                               tenant_id, poller_name, rid)
                                        max_outp = intf_ipms_obj.get_max_out(date_time_ipms_last_15_mins,
                                                                              date_time_ipms_now,
                                                                               tenant_id, poller_name, rid)

                                        if net_type.find('IPBN_UPSTREAM') >= 0:
                                            max_intf = max_intp
                                        else:
                                            if is_manual:
                                                max_intf = max_intp
                                            else:
                                                max_intf = max_outp
                                            # fix speed cho backhaul wanphy
                                            if intf_ipms_obj.speed == 9290000000:
                                                intf_ipms_obj.speed = 10000000000

                                        check_intf_ipms = True
                                        err = 'None'
                                        tbl_intf_stat = TblIntfStatImpl(node_begin=node_begin,
                                                                        interface_name=ipms_intf['name'],
                                                                        ip=device_test.get_ip(),
                                                                        area="KV3",
                                                                        description=intf_desc,
                                                                        node_type=host_type,
                                                                        interface_status="UP",
                                                                        interface_type="TRAFFFIC",
                                                                        cable_name=cable_name,
                                                                        speed=intf_ipms_obj.speed,
                                                                        interface_id=intf_ipms_obj.id,
                                                                        network_type=net_type,
                                                                        timestamp=date_time_elastic_now,
                                                                        max=max_intf,
                                                                        node_place=node_place,
                                                                        max_in=max_intp,
                                                                        max_out=max_outp,
                                                                        error=err,
                                                                        class_map='',
                                                                        content=content_name)

                                        tbl_intf_stat_json = json.dumps(tbl_intf_stat, default=lambda o: o.__dict__)
                                        id1 = uuid.uuid3(uuid.NAMESPACE_DNS, str(intf_ipms_obj.id) + node_begin +
                                                         x_intf + content_name + net_type + "_KV3_" +
                                                         date_time_elastic_now)
                                        srch_dct = dict(interface_id=intf_ipms_obj.id, timestamp=date_time_elastic_now,
                                                        area="KV3", network_type=net_type)
                                        hits = es.query_search_must(srch_dct, "tbl_interface_status")
                                        if not hits:
                                            # insert new
                                            # res = es.create_document("tbl_interface_status", id1.urn, "_doc",
                                            #                         tbl_intf_stat_json)
                                            # print(res)
                                            pass


                                        else:
                                            print(json.dumps(hits, indent=4))
                                            id1.urn = ''
                                    break

            if not check_intf_ipms:
                # get bandwidth cua link
                intf_bw = device_test.get_bandwidth_link(x_intf, pth_bw, shell)
                # get traffic cua link
                bw_inp, bw_outp = device_test.get_traffic_link(x_intf, pth_bw_inp_outp, shell)
                # convert list to int

                if net_type.find('IPBN_UPSTREAM') >= 0:
                    max_intf = bw_inp

                else:
                    if is_manual:
                        max_intf = bw_inp
                    else:
                        max_intf = bw_outp
                    if intf_bw == 9290000000:
                        intf_bw = 10000000000

                intf_id = id_intf_manual
                err = 'Not in IPMS'
                # insert thu cong
                tbl_intf_stat = TblIntfStatImpl(node_begin=node_begin,
                                                interface_name=x_intf,
                                                ip=device_test.get_ip(),
                                                area="KV3",
                                                description=intf_desc,
                                                node_type=host_type,
                                                interface_status="UP",
                                                interface_type="TRAFFFIC",
                                                cable_name=cable_name,
                                                speed=intf_bw,
                                                interface_id=intf_id,
                                                network_type=net_type,
                                                timestamp=date_time_elastic_now,
                                                max=max_intf,
                                                node_place=node_place,
                                                max_in=bw_inp,
                                                max_out=bw_outp,
                                                error=err,
                                                class_map='',
                                                content=content_name
                                                )

                tbl_intf_stat_json = json.dumps(tbl_intf_stat, default=lambda o: o.__dict__)
                id1 = uuid.uuid3(uuid.NAMESPACE_DNS, str(intf_id) + node_begin + x_intf + content_name +
                                 net_type + "_KV3_" + date_time_elastic_now)
                srch_dct = dict(node_begin=device_test.get_name(),
                                interface_name=x_intf, timestamp=date_time_elastic_now, area="KV3",
                                network_type=net_type)
                hits = es.query_search_must(srch_dct, "tbl_interface_status")
                id_intf_manual += 1
                if not hits:
                    # insert new
                    # res = es.create_document("tbl_interface_status", id1.urn, "_doc", tbl_intf_stat_json)
                    # print(res)
                    pass
                else:
                    print('duplicate ' + device_test.get_name() + " interface:" + x_intf)
                    print(json.dumps(hits, indent=4))
                    id1.urn = ''
        except Exception as err:
            print('Error while inserting elasticsearch upstream ' + str(err))
            return id_intf_manual, shell, '', tbl_intf_stat_json
        return id_intf_manual, shell, id1.urn, tbl_intf_stat_json

    def insert_elastic_upstream_clsm(self, x_intf, intf_desc, host_ipms_obj, date_time_ipms_last_15_mins,
                                         date_time_ipms_now, tenant_id, poller_name, date_time_elastic_now, shell,
                                         net_type, id_intf_manual, host_type, rate, clsm, intf_bw_host):

        id1 = uuid.uuid3(uuid.NAMESPACE_DNS, '')
        tbl_intf_stat_json = None

        try:
            device_test = Device(host_ipms_obj.hostname, host_ipms_obj.ip, host_ipms_obj.vendor, host_ipms_obj.type,
                                 'dungnd18', 'dungnd18@123')
            service_name = get_name_service_clsm(clsm)
            node_place = get_node_place(self.get_name(), intf_desc)

            # check interface name in IPMS
            rid = host_ipms_obj.rid
            api_spring_intf_ipms = ApiSpringIpmsImpl('interface', 'kv3068')

            # So Spring boot phan biet / tren URL nen chuyen / thanh _
            intf_name_under_line = x_intf.replace("/", "_", 100)
            intf_name_under_line += clsm + '_ingress'
            srch_intf_name_dct = {'searchRid': rid,
                                  'searchInterfaceName': intf_name_under_line}
            # Tim interface id va interface name tren ipms

            # Tim luon cable name va service name
            cable_name = get_cable_name(net_type, intf_desc)
            content_name = get_content(net_type, intf_desc)
            node_begin = device_test.get_name()


            res_intf_lst = api_spring_intf_ipms.get(srch_intf_name_dct)
            check_intf_ipms = False
            if res_intf_lst:
                for ipms_intf in res_intf_lst:
                    if 'speed' in ipms_intf:
                        intf_speed = convert_string_to_int(ipms_intf['speed'])
                        if intf_speed > 0:
                            if 'id' in ipms_intf and 'name' in ipms_intf and 'description' in ipms_intf and 'status' in ipms_intf:
                                intf_ipms_obj = InterfaceImpsImpl(ipms_intf['id'], ipms_intf['name'], rid,
                                                                  ipms_intf['speed'], ipms_intf['description'],
                                                                  ipms_intf['status'])
                                if intf_ipms_obj.check_link_physical():

                                    # tinh max In cua Upstream
                                    if tenant_id:
                                        # Do la Class map nen tinh rate nhu nhau

                                        check_intf_ipms = True
                                        err = 'None'
                                        tbl_intf_stat = TblIntfStatImpl(node_begin=node_begin,
                                                                        interface_name=ipms_intf['name'],
                                                                        ip=device_test.get_ip(),
                                                                        area="KV3",
                                                                        description=intf_desc,
                                                                        node_type=host_type,
                                                                        interface_status="UP",
                                                                        interface_type="TRAFFIC_QOS",
                                                                        cable_name=cable_name,
                                                                        speed=intf_ipms_obj.speed,
                                                                        interface_id=intf_ipms_obj.id,
                                                                        network_type=net_type,
                                                                        timestamp=date_time_elastic_now,
                                                                        max=rate,
                                                                        node_place=node_place,
                                                                        max_in=rate,
                                                                        max_out=rate,
                                                                        error=err,
                                                                        class_map=service_name,
                                                                        content=content_name)

                                        tbl_intf_stat_json = json.dumps(tbl_intf_stat, default=lambda o: o.__dict__)
                                        id1 = uuid.uuid3(uuid.NAMESPACE_DNS, str(intf_ipms_obj.id) + node_begin +
                                                         x_intf + service_name + content_name + "_KV3_" +
                                                         date_time_elastic_now)
                                        srch_dct = dict(interface_id=intf_ipms_obj.id, timestamp=date_time_elastic_now,
                                                        area="KV3", network_type=net_type)
                                        hits = es.query_search_must(srch_dct, "tbl_interface_status")
                                        if not hits:
                                            # insert new
                                            # res = es.create_document("tbl_interface_status", id1.urn, "_doc",
                                            #                         tbl_intf_stat_json)
                                            # print(res)
                                            pass

                                        else:
                                            # print(json.dumps(hits, indent=4))
                                            id1.urn = ''
                                    break

            if not check_intf_ipms:
                # get bandwidth cua link
                # convert list to int

                intf_id = id_intf_manual
                err = ''
                # insert thu cong
                tbl_intf_stat = TblIntfStatImpl(node_begin=node_begin,
                                                interface_name=x_intf,
                                                ip=device_test.get_ip(),
                                                area="KV3",
                                                description=intf_desc,
                                                node_type=host_type,
                                                interface_status="UP",
                                                interface_type="TRAFFIC_QOS",
                                                cable_name=cable_name,
                                                speed=intf_bw_host,
                                                interface_id=intf_id,
                                                network_type=net_type,
                                                timestamp=date_time_elastic_now,
                                                max=rate,
                                                node_place=node_place,
                                                max_in=rate,
                                                max_out=rate,
                                                error=err,
                                                class_map=service_name,
                                                content=content_name)

                tbl_intf_stat_json = json.dumps(tbl_intf_stat, default=lambda o: o.__dict__)
                id1 = uuid.uuid3(uuid.NAMESPACE_DNS, str(intf_id) + node_begin + x_intf +
                                 service_name + content_name + "_KV3_" + date_time_elastic_now)
                srch_dct = dict(node_begin=device_test.get_name(), network_type=net_type,
                                interface_id=intf_id, timestamp=date_time_elastic_now, area="KV3")
                hits = es.query_search_must(srch_dct, "tbl_interface_status")
                id_intf_manual += 1
                if not hits:
                    # insert new
                    # res = es.create_document("tbl_interface_status", id1.urn, "_doc", tbl_intf_stat_json)
                    # print(res)
                    pass
                else:
                    # print(json.dumps(hits, indent=4))
                    id1.urn = ''
        except Exception as err:
            print('Error while inserting elasticsearch upstream class map ' + str(err))
            return id_intf_manual, shell, '', tbl_intf_stat_json

        return id_intf_manual, shell, id1.urn, tbl_intf_stat_json


def send_string_wait_string(command, wait_string, wait_time, shell):
    temp_wait_time = wait_time / 3
    shell.settimeout(temp_wait_time)
    shell.send(command)
    buffer = ''
    timeout = time.time() + wait_time * 3
    result = True
    resp = ""
    while not wait_string in buffer:
        try:
            time.sleep(temp_wait_time)
            resp = shell.recv(1024).decode("utf-8")
            if 'Last login:' in resp:
                pos_telnet = resp.find('telnet')
                resp = resp[pos_telnet:]
            if 'Connection refused' in resp or 'No route to' in resp:
                result = False
                break
            buffer += resp
            print('broke')
            if time.time() > timeout:
                print('BROKE because time out')
                result = False
                shell.send('\x03')
                break
            else:
                continue
        except socket.timeout:
            print('Connection failed')
            result = False
            break
    print(resp)
    return result


def command_cli(command, character_stop, shell):
    shell.send(command)
    receive_buffer = ''
    check_cli = False
    if character_stop == '$':
        socket.setdefaulttimeout(10000)
    while not check_cli:
        # Wait a bit, if necessary
        # Flush the receive buffer
        try:
            time.sleep(2)
            receive_buffer_byte = shell.recv(4068)
            receive_buffer_str = receive_buffer_byte.decode("utf-8")
            receive_buffer += receive_buffer_str
            if "--More--" in receive_buffer:
                receive_buffer = receive_buffer.replace('--More--', '')
                shell.send(' ')
            else:
                if character_stop in receive_buffer:
                    check_cli = True
            if character_stop == '$':
                shell.send(' ')
        except socket.timeout:
            #trong truong hop hoi commit
            shell.send('no \n')
            shell.send('\x03')
            return "Error when send command ..."

    number_line = len(receive_buffer.splitlines())
    string_result = ''

    for i in range(0, number_line):
        temp_string = receive_buffer.splitlines()[i].strip()
        string_filter_temp = temp_string.replace('--More--', '')
        string_filter_temp = string_filter_temp.replace('[K', '')
        string_filter_temp = re.sub(r'[^a-zA-Z0-9-+*/:._=!#|() ]', r'', string_filter_temp)
        string_filter_temp = string_filter_temp.strip()
        string_result += string_filter_temp + '\n'

    print(string_result)
    # Print the receive buffer, if necessary
    return string_result