S = '125.235.255.249'
__author__ = 'VTN-DHML-DHIP10'
import os
import time
import uuid
from collections import namedtuple

import paramiko

from config import Development
from core.parsing.device import Device
from core.database.impl.ipms.host_ipms_impl import HostIpmsImpl
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
from core.helpers.tenant_helpers import get_tenant_id
from core.helpers.date_helpers import get_date_minus_format_ipms, get_date_now_format_ipms, \
    get_date_now_format_elastic, get_date_now
from core.database.__init__ import es

config = Development()
SERVER_PARSING = config.__getattribute__('SERVER_PARSING')
SERVER_PARSING_USER = config.__getattribute__('SERVER_PARSING_USER')
SERVER_PARSING_PASS = config.__getattribute__('SERVER_PARSING_PASS')


class MaxUpstreamBackhaul:
    def __init__(self, vendor):
        self.vendor = vendor
        pass

    def get_path(self):
        pth = os.path.dirname(__file__)
        path_join = os.path.join(pth, "template")
        path_join = os.path.join(path_join, self.vendor)
        path_upstream = os.path.join(path_join, "show_max_upstream.textfsm")
        path_backhaul = os.path.join(path_join, "show_max_backhaul.textfsm")
        path_bw = os.path.join(path_join, "show_interface_inc_bandwidth.textfsm")
        path_bw_inp_outp = os.path.join(path_join, "show_interface_input_output_bandwidth.textfsm")
        path_bw_class_map = os.path.join(path_join, "show_interface_traffic_class_map.textfsm")
        path_intf_lst_class_map = os.path.join(path_join, "show_interface_class_map.textfsm")
        path_intf_one_description = os.path.join(path_join, "show_interface_description_one_link.textfsm")
        return path_upstream, path_backhaul, path_bw, path_bw_inp_outp, \
               path_bw_class_map, path_intf_lst_class_map, path_intf_one_description

    def get_manl_lst(self, host_nme_lst):
        res_lst = list()
        for host_nme in host_nme_lst:
            res_host_manual_lst = list()
            api_spring_host_ipms = ApiSpringIpmsImpl('host', 'kv3068')
            srch_hst_name_manual_dct = {'searchHostName': host_nme}
            res_host_manual_lst = api_spring_host_ipms.get(srch_hst_name_manual_dct)

            if res_host_manual_lst:
                for x in res_host_manual_lst:
                    host_obj = namedtuple("HostIpms", x.keys())(*x.values())
                    host_ipms_obj = HostIpmsImpl(host_obj.rid, host_obj.ip, host_obj.vgroup,
                                                 host_obj.hostname, host_obj.sysDescr, host_obj.poller)

                    host_ipms_obj = host_ipms_obj.get_vendor()
                    res_lst.append(host_ipms_obj)
        return res_lst

    def run(self):
        date_time_now = get_date_now()
        date_time_ipms_now = get_date_now_format_ipms()
        date_time_ipms_last_15_mins = get_date_minus_format_ipms(date_time_now, 15)
        date_time_elastic_now = get_date_now_format_elastic()

        # Create an SSH client
        client = paramiko.SSHClient()
        # Make sure that we add the remote server's SSH key automatically
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        # Connect to the client
        try:
            client.connect(SERVER_PARSING, username=SERVER_PARSING_USER, password=SERVER_PARSING_PASS)
            pth_upstrm, pth_bckhaul, pth_bw, pth_bw_inp_outp, pth_class_map, \
            pth_link_clsm, pth_link_one_des = self.get_path()

            # ID URN default
            id_urn_def = uuid.uuid3(uuid.NAMESPACE_DNS, '')
            # Xac dinh thuoc Host Group FOIP IPBN truoc

            api_spring_host_grp_ipms = ApiSpringIpmsImpl('hostGroup', 'kv3068')
            srch_grp_name_dct = {'searchGroupName': 'FOIP_IPBN'}
            group_name_lst = api_spring_host_grp_ipms.get(srch_grp_name_dct)
            group_id_lst = list()
            if group_name_lst:
                for x in group_name_lst:
                    if 'group_id' in x.keys():
                        group_id_lst.append(x['group_id'])

            # Tim nhung host co ten POP

            api_spring_host_ipms = ApiSpringIpmsImpl('host', 'kv3068')

            srch_grp_name_dct = {'searchHostName': 'POP'}

            # Check manual list
            hst_mnl_lst = ['HHT9602CRT24.IGW.ASR22.04', 'HCM_IW2_ASR9K_HHTN4', 'HKH9103CRT13.DGW.ASR12.01']

            host_manual_lst = self.get_manl_lst(hst_mnl_lst)
            id_intf_manual = 1
            intf_l3ll = 'Bundle-Ether99'
            elk_imp_lst = list()

            if host_manual_lst:
                for x in host_manual_lst:
                    # find poller id and tenant id
                    poller_name = x.poller
                    rid = x.rid
                    check_poller = True
                    tenant_id = ''
                    host_name = x.hostname.upper()
                    host_type = 'POP_TRANSIT'

                    if poller_name:
                        # tim ip cua poller name
                        # tim poller id
                        api_spring_poll_ipms = ApiSpringIpmsImpl('poller', 'kv3068')
                        srch_poll_dct = {'searchPoller': poller_name}
                        res_poll_lst = api_spring_poll_ipms.get(srch_poll_dct)
                        if len(res_poll_lst) == 1:
                            if 'poller_ip' in res_poll_lst[0]:
                                poller_ip = res_poll_lst[0]['poller_ip']
                                tenant_id = get_tenant_id(poller_ip)

                        else:
                            print('Error There are two many or do not find poller id')
                            check_poller = False

                    # Create a raw shell
                    shell = client.invoke_shell()
                    device_test = Device(x.hostname, x.ip, x.vendor, x.type, 'thunv5', 'Viettel#321#')

                    check_telnet = device_test.telnet(shell)

                    if check_telnet == 'True' and check_poller:
                        print("Connect OK")
                        print("Continue to config...")

                        # check link qos
                        intf_class_map_lst = device_test.get_link_class_map_lst(pth_link_clsm, shell)
                        if intf_class_map_lst:
                            for intf_clsm in intf_class_map_lst:
                                name_clsm_lst, rate_clsm = device_test.get_class_map_traff_link(intf_clsm, pth_class_map,
                                                                                                shell)
                                # insert vao elasticsearch
                                if rate_clsm:
                                    intf_one_desc = device_test.get_description_link(intf_clsm, pth_link_one_des, shell)
                                    intf_bw = device_test.get_bandwidth_link(intf_clsm, pth_bw, shell)

                                    index_clsm = -1
                                    for name_clsm in name_clsm_lst:
                                        index_clsm += 1
                                        id_intf_manual, shell, id_els, tbl_json = \
                                            device_test.insert_elastic_upstream_clsm(
                                                intf_clsm,
                                                intf_one_desc,
                                                x,
                                                date_time_ipms_last_15_mins,
                                                date_time_ipms_now,
                                                tenant_id,
                                                poller_name,
                                                date_time_elastic_now,
                                                shell,
                                                'IPBN_UPSTREAM_QOS',
                                                id_intf_manual,
                                                host_type,
                                                rate_clsm[index_clsm],
                                                name_clsm, intf_bw)
                                        if id_els != '' and id_els != id_urn_def:
                                            elk_imp_lst.append(dict(id=id_els, data=tbl_json))
                                        else:
                                            print('Duplicate id')

                        # insert them interface bundle-ether 99 phuc vu luu luong L3LL
                        intf_l3ll_desc = device_test.get_description_link(intf_l3ll, pth_link_one_des, shell)
                        if intf_l3ll_desc:
                            id_int_manual, shell, id_els, tbl_json = \
                                device_test.insert_elastic_upstream_backhaul(intf_l3ll,
                                                                             intf_l3ll_desc,
                                                                             x,
                                                                             date_time_ipms_last_15_mins,
                                                                             date_time_ipms_now,
                                                                             tenant_id,
                                                                             poller_name,
                                                                             date_time_elastic_now,
                                                                             shell,
                                                                             'IPBN_BACKHAUL_L3LL',
                                                                             id_intf_manual,
                                                                             host_type)
                            if id_els != '' and id_els != id_urn_def:
                                elk_imp_lst.append(dict(id=id_els, data=tbl_json))
                            else:
                                print('Duplicate id')

                        intf_bckhl_lst, des_bchk_lst = device_test.get_intf_desc_backhaul(pth_upstrm, shell)

                        index_intf = 0
                        for x_intf in intf_bckhl_lst:
                            intf_desc = des_bchk_lst[index_intf]
                            id_int_manual, shell, id_els, tbl_json = \
                                device_test.insert_elastic_upstream_backhaul(x_intf,
                                                                             intf_desc,
                                                                             x,
                                                                             date_time_ipms_last_15_mins,
                                                                             date_time_ipms_now,
                                                                             tenant_id,
                                                                             poller_name,
                                                                             date_time_elastic_now,
                                                                             shell,
                                                                             'IPBN_BACKHAUL',
                                                                             id_intf_manual,
                                                                             host_type)
                            if id_els != '' and id_els != id_urn_def:
                                elk_imp_lst.append(dict(id=id_els, data=tbl_json))
                            else:
                                print('Duplicate id')

                            id_int_manual, shell, id_els, tbl_json = \
                                device_test.insert_elastic_upstream_backhaul(x_intf,
                                                                             intf_desc,
                                                                             x,
                                                                             date_time_ipms_last_15_mins,
                                                                             date_time_ipms_now,
                                                                             tenant_id,
                                                                             poller_name,
                                                                             date_time_elastic_now,
                                                                             shell,
                                                                             'IPBN_UPSTREAM',
                                                                             id_intf_manual,
                                                                             host_type)
                            if id_els != '' and id_els != id_urn_def:
                                elk_imp_lst.append(dict(id=id_els, data=tbl_json))
                            else:
                                print('Duplicate id')
                            index_intf += 1

            res_host_lst = api_spring_host_ipms.get(srch_grp_name_dct)
            host_lst = list()
            if res_host_lst:
                for x in res_host_lst:
                    host_obj = namedtuple("HostIpms", x.keys())(*x.values())
                    host_ipms_obj = HostIpmsImpl(host_obj.rid, host_obj.ip, host_obj.vgroup,
                                                 host_obj.hostname, host_obj.sysDescr, host_obj.poller)
                    # if host_ipms_obj.ip == '125.235.255.246':
                    #    host_ipms_obj.ip = '10.74.237.74'
                    # elif host_ipms_obj.ip == '125.235.255.249':
                    #    host_ipms_obj.ip = '10.74.237.66'

                    host_ipms_obj = host_ipms_obj.get_vendor()
                    if host_obj.vgroup in group_id_lst:
                        host_lst.append(host_ipms_obj)
            print("Connect successful")
            time.sleep(2)

            for x_node in host_lst:
                # find poller id and tenant id
                poller_name = x_node.poller
                rid = x_node.rid
                check_poller = True
                tenant_id = ''
                host_name = x_node.hostname.upper()
                if host_name.find('PEERING') > 0:
                    host_type = 'POP_PEERING'
                else:
                    host_type = 'POP_TRANSIT'

                if poller_name:
                    # tim ip cua poller name
                    # tim poller id
                    api_spring_poll_ipms = ApiSpringIpmsImpl('poller', 'kv3068')
                    srch_poll_dct = {'searchPoller': poller_name}
                    res_poll_lst = api_spring_poll_ipms.get(srch_poll_dct)
                    if len(res_poll_lst) == 1:
                        if 'poller_ip' in res_poll_lst[0]:
                            poller_ip = res_poll_lst[0]['poller_ip']
                            tenant_id = get_tenant_id(poller_ip)

                    else:
                        print('Error There are two many or do not find poller id')
                        check_poller = False

                # Create a raw shell
                shell = client.invoke_shell()
                device_test = Device(x_node.hostname, x_node.ip, x_node.vendor, x_node.type, 'dungnd18', 'dungnd18@123')
                if x_node.ip == '125.235.255.252':
                    device_via = Device('USA_POP1_TRANSIT_ASR', '125.235.255.251', x_node.vendor, x_node.type, 'dungnd18',
                                        'dungnd18@123')
                    check_telnet = device_test.telnet_via(shell, device_via)
                else:
                    device_via = Device(x_node.hostname, x_node.ip, x_node.vendor, x_node.type, 'dungnd18', 'dungnd18@123')
                    check_telnet = device_test.telnet(shell)

                if check_telnet == 'True' and check_poller:
                    print("Connect OK")
                    print("Continue to config...")
                    # check link qos
                    intf_class_map_lst = device_test.get_link_class_map_lst(pth_link_clsm, shell)
                    for intf_clsm in intf_class_map_lst:
                        name_clsm_lst, rate_clsm = device_test.get_class_map_traff_link(intf_clsm, pth_class_map, shell)
                        # insert vao elasticsearch
                        if rate_clsm:
                            intf_one_desc = device_test.get_description_link(intf_clsm, pth_link_one_des, shell)
                            intf_bw = device_test.get_bandwidth_link(intf_clsm, pth_bw, shell)
                            index_clsm = -1
                            for name_clsm in name_clsm_lst:
                                index_clsm += 1
                                id_intf_manual, shell, id_els, tbl_json = \
                                    device_test.insert_elastic_upstream_clsm(
                                        intf_clsm,
                                        intf_one_desc,
                                        x_node,
                                        date_time_ipms_last_15_mins,
                                        date_time_ipms_now,
                                        tenant_id, poller_name,
                                        date_time_elastic_now,
                                        shell,
                                        'IPBN_UPSTREAM_QOS',
                                        id_intf_manual,
                                        host_type,
                                        rate_clsm[index_clsm],
                                        name_clsm,
                                        intf_bw)
                                if id_els != '' and id_els != id_urn_def:
                                    elk_imp_lst.append(dict(id=id_els, data=tbl_json))
                                else:
                                    print('Duplicate id')

                    # insert them interface bundle-ether 99 phuc vu luu luong L3LL
                    intf_l3ll_desc = device_test.get_description_link(intf_l3ll, pth_link_one_des, shell)
                    if intf_l3ll_desc:
                        id_int_manual, shell, id_els, tbl_json = \
                            device_test.insert_elastic_upstream_backhaul(intf_l3ll,
                                                                         intf_l3ll_desc,
                                                                         x_node,
                                                                         date_time_ipms_last_15_mins,
                                                                         date_time_ipms_now,
                                                                         tenant_id,
                                                                         poller_name,
                                                                         date_time_elastic_now,
                                                                         shell,
                                                                         'IPBN_BACKHAUL_L3LL',
                                                                         id_intf_manual,
                                                                         host_type)
                        if id_els != '' and id_els != id_urn_def:
                            elk_imp_lst.append(dict(id=id_els, data=tbl_json))
                        else:
                            print('Duplicate id')

                    intf_lst, des_lst = device_test.get_intf_desc_upstream(pth_upstrm, shell)
                    intf_bckhl_lst, des_bchk_lst = device_test.get_intf_desc_backhaul(pth_upstrm, shell)

                    index_intf = 0
                    # co danh sach cong roi sau do show interface de tim list
                    for x_intf in intf_lst:
                        intf_desc = des_lst[index_intf]
                        id_int_manual, shell, id_els, tbl_json = \
                            device_test.insert_elastic_upstream_backhaul(x_intf,
                                                                         intf_desc,
                                                                         x_node,
                                                                         date_time_ipms_last_15_mins,
                                                                         date_time_ipms_now,
                                                                         tenant_id,
                                                                         poller_name,
                                                                         date_time_elastic_now,
                                                                         shell,
                                                                         'IPBN_UPSTREAM',
                                                                         id_intf_manual,
                                                                         host_type)
                        if id_els != '' and id_els != id_urn_def:
                            elk_imp_lst.append(dict(id=id_els, data=tbl_json))
                        else:
                            print('Duplicate id')
                        index_intf += 1

                    index_intf = 0
                    for x_intf in intf_bckhl_lst:
                        intf_desc = des_bchk_lst[index_intf]
                        id_int_manual, shell, id_els, tbl_json = \
                            device_test.insert_elastic_upstream_backhaul(x_intf,
                                                                         intf_desc,
                                                                         x_node,
                                                                         date_time_ipms_last_15_mins,
                                                                         date_time_ipms_now,
                                                                         tenant_id,
                                                                         poller_name,
                                                                         date_time_elastic_now,
                                                                         shell,
                                                                         'IPBN_BACKHAUL',
                                                                         id_intf_manual,
                                                                         host_type)
                        if id_els != '' and id_els != id_urn_def:
                            elk_imp_lst.append(dict(id=id_els, data=tbl_json))
                        else:
                            print('Duplicate id')
                        index_intf += 1
        finally:
            client.close()

        if elk_imp_lst:

            for x in elk_imp_lst:
                if 'id' in x and 'data' in x:
                    id_els = x['id']
                    tbl_json = x['data']
                    res = es.create_document("tbl_interface_status", id_els, "_doc", tbl_json)
                    print(res)
