__author__ = 'VTN-DHML-DHIP10'

import os
import uuid

from config import Development
from app.api.vipa.device.api_device import ApiDevice
from core.parsing.vipa_parsing.device_vipa import DeviceVipa
from core.helpers.date_helpers import get_date_now_format_ipms
from core.helpers.stringhelpers import is_link_phy

config = Development()
SERVER_PARSING = config.__getattribute__('SERVER_PARSING')
SERVER_PARSING_USER = config.__getattribute__('SERVER_PARSING_USER')
SERVER_PARSING_PASS = config.__getattribute__('SERVER_PARSING_PASS')


class GetInterfaceList:
    def __init__(self, hst_nme, ip, vendor, username, passwd):
        self.host_name = hst_nme
        self.ip = ip
        self.vendor = vendor
        self.username = username
        self.password = passwd

    def get_path(self):
        pth = os.path.dirname(__file__)
        path_join = os.path.join(pth, "template")
        path_join = os.path.join(path_join, self.vendor)
        path_intf_bw_lst = os.path.join(path_join, "show_interface_bw_list.textfsm")
        path_des_lst = os.path.join(path_join, "show_interface_description.textfsm")
        return path_intf_bw_lst, path_des_lst

    def get_intf_lst(self):
        intf_mer_lst = list()
        #client = paramiko.SSHClient()
        # Make sure that we add the remote server's SSH key automatically
        #client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        # Connect to the client
        try:
            #client.connect(SERVER_PARSING, username=SERVER_PARSING_USER, password=SERVER_PARSING_PASS)
            pth_intf_bw, pth_intf_des = self.get_path()
            #shell = client.invoke_shell()
            _api_node = ApiDevice(self.ip, 'VNM')
            node_code_vipa, node_vend, node_vers = _api_node.get_name_ven_ver()
            if node_code_vipa and node_vend and node_vers:
                dev = DeviceVipa(node_code_vipa, node_vend, node_vers, self.username, self.password)
                time_now = get_date_now_format_ipms()

                intf_lst = dev.get_intf_bw_lst(pth_intf_bw)
                intf_des_lst = dev.get_intf_des_lst(pth_intf_des)
                for intf in intf_lst:
                    if 'interface' in intf:
                        intf_name = intf['interface']
                        intf_name = intf_name.strip()
                        # loai bo interface sub va linh tinh
                        is_lnk_phy = is_link_phy(intf_name)
                        if is_lnk_phy:
                            intf_des = self.get_des_frm_intf(intf_name, intf_des_lst)
                            intf_mode = ''
                            if 'phy_status' in intf:
                                intf_stat = intf['phy_status']
                            else:
                                intf_stat = ''
                            if 'interface_bw' in intf:
                                intf_bw = intf['interface_bw']
                                if intf_bw == 9953280000:
                                    intf_mode = 'WANPHY'
                                else:
                                    intf_mode = 'LANPHY'
                            else:
                                intf_bw = 0
                            intf_id = uuid.uuid3(uuid.NAMESPACE_DNS, self.host_name + intf_name + str(time_now))
                            intf_dct = dict(interface_name=intf_name, node_begin=self.host_name, ip=self.ip, description=intf_des,
                                            interface_status=intf_stat, interface_id=intf_id.urn, interface_mode=intf_mode,
                                            speed=intf_bw)
                            intf_mer_lst.append(intf_dct)

        except Exception as err:
            print('Error when scan port interface list of POP ' + str(err))
            return intf_mer_lst

        return intf_mer_lst

    @staticmethod
    def get_des_frm_intf(intf_name, des_lst):
        result = ''
        for x in des_lst:
            if 'interface' in x:
                intf = x['interface']
                if 'description' in x:
                    intf_des = x['description']
                    if intf.strip() == intf_name.strip():
                        return intf_des
        return result

if __name__ == '__main__':
    _test = GetInterfaceList('HKG_POP4_TRANSIT_ASR', "125.235.255.243", "Cisco", "dungnd18", "dungnd18@123")
    _intf_lst = _test.get_intf_lst()
