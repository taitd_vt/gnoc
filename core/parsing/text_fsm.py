class TxtFSM:
    def __init__(self, data_lst, choice_dct):
        self.data_lst = data_lst
        self.choice_dct = choice_dct

    def get_want_lst(self):
        lst_data = self.data_lst
        tmp_choice_dct = self.choice_dct
        lst_want = []

        if lst_data:
            number_com_big = len(lst_data[0])
            number_com_small = len(lst_data[0][0])
            if number_com_big == 1 and isinstance(lst_data[0][0], str):
                tmp_dict = dict()
                for key,value in tmp_choice_dct.items():
                    tmp_dict[key] = lst_data[0][0]
                lst_want.append(tmp_dict)
                return lst_want
            else:
                index_dct = 0
                if isinstance(lst_data[0][0], str):
                    # tat ca deu la string
                    tmp_dct = dict()
                    for k, v in tmp_choice_dct.items():
                        tmp_dct[k] = lst_data[0][index_dct]
                        index_dct += 1
                    lst_want.append(tmp_dct)
                else:
                    for i in range(0, number_com_small): #check phan tu cua mang
                        tmp_dict = dict()
                        for j in range(0, number_com_big):
                            tmp_value = lst_data[0][j][i]
                            for key,value in tmp_choice_dct.items():
                                if j == value:
                                    tmp_dict[key] = tmp_value
                        lst_want.append(tmp_dict)

        return lst_want

    def divide_lst(self, dct_lst, key_dct):
        res_lst = list()
        for x in dct_lst:
            for key, value in x.items():
                if key == key_dct:
                    res_lst.append(value)
        return res_lst
