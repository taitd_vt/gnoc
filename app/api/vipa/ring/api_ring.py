__author__ = 'VTN-DHML-DHIP10'
import json
import pymysql
import requests
from core.helpers.api_helpers import api_post_retry

from config import Development

config = Development()
API_VIPA_RING = config.__getattribute__('API_VIPA_RING')
API_VIPA_RING_USERNAME = config.__getattribute__('API_VIPA_RING_USERNAME')
API_VIPA_RING_PASSWORD = config.__getattribute__('API_VIPA_RING_PASSWORD')
SERVER_HOST = config.__getattribute__('SERVER_HOST')
SERVER_HOST_USER = config.__getattribute__('SERVER_HOST_USER')
SERVER_HOST_PASS = config.__getattribute__('SERVER_HOST_PASS')


class ApiVipaRing:
    def __init__(self):
        self.api_url = API_VIPA_RING
        self.username = API_VIPA_RING_USERNAME
        self.password = API_VIPA_RING_PASSWORD

    def get_ring_info(self):
        try:
            def_dct = {"code": "getRingInfo", "params": [], "query": "", "compressData": 0}
            url_api = self.api_url
            headers_json = {"Content-Type": "application/x-www-form-urlencoded"}
            body_json = {"userService": self.username, "passService": self.password, "input": str(def_dct)}

            # r = requests.post(url_api, headers=headers_json, data=body_json)
            r = api_post_retry(url_api, headers_json, body_json)
            if r:
                if r.ok:
                    result_json = json.loads(r.text)
                    if isinstance(result_json, dict) and 'data' in result_json:
                        result_lst = result_json['data']
                        return result_lst
            else:
                return list()
        except Exception as err:
            print("Error when get Ring info of vipa" + str(err))
            return list()

    def get_bras_hsi_info(self):
        try:
            def_dct = {"code": "getBrasHsiInfoExt", "params": [], "query": "", "compressData": 0}
            url_api = self.api_url
            headers_json = {"Content-Type": "application/x-www-form-urlencoded"}
            body_json = {"userService": self.username, "passService": self.password, "input": str(def_dct)}
            # r = requests.post(url_api, headers=headers_json, data=body_json)
            r = api_post_retry(url_api, headers_json, body_json)
            if r:
                if r.ok:
                    result_json = json.loads(r.text)
                    if isinstance(result_json, dict) and 'data' in result_json:
                        result_lst = result_json['data']
                        return result_lst

        except Exception as err:
            print("Error when get Ring info of vipa" + str(err))
            return list()
        return list()

    def get_relation_node(self, node_name):
        try:
            def_dct = {"code": "getRelationNode", "params": [{"name": "node", "value": node_name}], "query": "",
                       "compressData": 0}
            url_api = self.api_url
            headers_json = {"Content-Type": "application/x-www-form-urlencoded"}
            body_json = {"userService": self.username, "passService": self.password, "input": str(def_dct)}
            # r = requests.post(url_api, headers=headers_json, data=body_json)
            r = api_post_retry(url_api, headers_json, body_json)
            if r:
                if r.ok:
                    result_json = json.loads(r.text)
                    if isinstance(result_json, dict) and 'data' in result_json:
                        result_lst = result_json['data']
                        return result_lst

        except Exception as err:
            print("Error when get Ring info of vipa" + str(err))
            return list()
        return list()

    def get_conn(self):
        ip_server = ''

        con = pymysql.connect(host=SERVER_HOST, port=3306, user=SERVER_HOST_USER, password=SERVER_HOST_PASS,
                                  db='luuluong', cursorclass=pymysql.cursors.DictCursor)
        return con

    def delete_ring_all(self):
        conn = self.get_conn()
        # print(div_lst)
        # request
        result_acc_lst = list()
        if conn:
            try:
                with conn.cursor() as cursor:
                    # check voi vendor la Juniper
                    sql = "DELETE from tbl_ring_info"
                    cursor.execute(sql)
                    conn.commit()
            except Exception as err:
                 print(err)
                 return result_acc_lst
            finally:
                conn.close()

    def delete_bras_all(self):
        conn = self.get_conn()
        # print(div_lst)
        # request
        result_acc_lst = list()
        if conn:
            try:
                with conn.cursor() as cursor:
                    # check voi vendor la Juniper
                    sql = "DELETE from tbl_bras_hsi_info"
                    cursor.execute(sql)
                    conn.commit()
            except Exception as err:
                 print(err)
                 return result_acc_lst
            finally:
                conn.close()

if __name__ == '__main__':
    _test = ApiVipaRing()
    _ring_info = _test.delete_all()

