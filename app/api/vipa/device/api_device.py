__author__ = 'VTN-DHML-DHIP10'
from config import Development
import requests, json
from core.helpers.api_helpers import api_post_retry
config = Development()
API_VIPA_RING = config.__getattribute__('API_VIPA_RING')
API_VIPA_RING_USERNAME = config.__getattribute__('API_VIPA_RING_USERNAME')
API_VIPA_RING_PASSWORD = config.__getattribute__('API_VIPA_RING_PASSWORD')


class ApiDevice:
    def __init__(self, ip, area):
        self.ip = ip
        self.area = area

    def get_name_ven_ver(self):
        node_name = ''
        node_ver = ''
        node_ven = ''
        # do dac thu dung VIPA de parsing nen phai vao NIMS de thuc hien sua dong bo giua IPMS va NIMS de VIPA co the dong bo tot
        try:

            def_dct = {"code": "getInfoNode2", "params": [{"name": "nodeIp", "value": self.ip},
                                                          {"name": "countryCode", "value": self.area}],
                       "query": "", "compressData": 0}

            url_api = API_VIPA_RING
            headers_json = {"Content-Type": "application/x-www-form-urlencoded"}
            body_json = {"userService": API_VIPA_RING_USERNAME, "passService": API_VIPA_RING_PASSWORD,
                         "input": str(def_dct)}

            # r = requests.post(url_api, headers=headers_json, data=body_json)
            r = api_post_retry(url_api, headers_json, body_json)
            if r:
                if r.ok:
                    result_json = json.loads(r.text)
                    if isinstance(result_json, dict) and 'data' in result_json:
                        result_lst = result_json['data']
                        if result_lst:
                            result_dct = result_lst[0]
                            if 'node_code' in result_dct:
                                node_name = result_dct['node_code']
                            if 'vendor_name' in result_dct:
                                node_ven = result_dct['vendor_name']
                            if 'version_name' in result_dct:
                                node_ver = result_dct['version_name']
                            return node_name, node_ven, node_ver

        except Exception as err:
            print("Error when get Ring info of vipa" + str(err))
            return node_name, node_ven, node_ver
        return node_name, node_ven, node_ver

if __name__ == '__main__':
    _api = ApiDevice("125.235.255.241", "VNM")
    name, ven, ver = _api.get_name_ven_ver()
    print(name)