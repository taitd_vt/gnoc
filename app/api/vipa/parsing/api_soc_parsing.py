__author__ = 'VTN-DHML-DHIP10'
from config import Development
from core.helpers.stringhelpers import convert_api_vipa_xml_to_dct
import requests, json
from core.helpers.api_helpers import api_post_retry

config = Development()
API_SOC_CUSTOM_VIP_1 = config.__getattribute__('API_SOC_CUSTOM_VIP_1')
API_SOC_CUSTOM_VIP_2 = config.__getattribute__('API_SOC_CUSTOM_VIP_2')
API_SOC_CUSTOM_VIP_3 = config.__getattribute__('API_SOC_CUSTOM_VIP_3')
API_SOC_CUSTOM_VIP_4 = config.__getattribute__('API_SOC_CUSTOM_VIP_4')
API_SOC_CUSTOM_VIP_5 = config.__getattribute__('API_SOC_CUSTOM_VIP_5')
API_SOC_CUSTOM_VIP_6 = config.__getattribute__('API_SOC_CUSTOM_VIP_6')
API_SOC_CUSTOM_VIP_7 = config.__getattribute__('API_SOC_CUSTOM_VIP_7')
API_SOC_CUSTOM_VIP_8 = config.__getattribute__('API_SOC_CUSTOM_VIP_8')
API_SOC_CUSTOM_VIP_9 = config.__getattribute__('API_SOC_CUSTOM_VIP_9')


class ApiSocParsing:
    def __init__(self):
        self.api_url_1 = API_SOC_CUSTOM_VIP_1
        self.api_url_2 = API_SOC_CUSTOM_VIP_1
        self.api_url_3 = API_SOC_CUSTOM_VIP_1
        self.api_url_4 = API_SOC_CUSTOM_VIP_1
        self.api_url_5 = API_SOC_CUSTOM_VIP_1
        self.api_url_6 = API_SOC_CUSTOM_VIP_1
        self.api_url_7 = API_SOC_CUSTOM_VIP_1
        self.api_url_8 = API_SOC_CUSTOM_VIP_1
        self.api_url_9 = API_SOC_CUSTOM_VIP_1

    def request(self):
        result = ''
        try:

            headers = {'content-type': 'text/xml'}
            body_begn = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:fun="http://function.webservice.viettel.com/">
                            <soapenv:Header/>
                                 <soapenv:Body>
                                    <fun:getDataJson>
                                    <authorityBO>
                                        <password>bed0c2ddbfff2e0e3077e2dc2885b38a</password>
                                        <requestId>1111</requestId>
                                        <userName>6f2efd0770ec8d37c22a0e885a32707f</userName>
                                    </authorityBO>
                                    <requestInputBO>
                                        <code>WS_GET_QUALITY_MONITOR_CUR_VIP</code>
                                        <compressData></compressData>
                                        <query></query>
                                    </requestInputBO>
                                    </fun:getDataJson>
                                </soapenv:Body>
                            </soapenv:Envelope>"""

            body = body_begn
            if body != '':
                # response = requests.post(self.api_url, data=body, headers=headers)
                # Do doi sang dung LB nen khong can tro nhieu API nua
                chck_res = False
                chck_api_url = 1
                while not chck_res:
                    if chck_api_url == 1:
                        response = api_post_retry(self.api_url_1, headers, body)
                    elif chck_api_url == 2:
                        response = api_post_retry(self.api_url_2, headers, body)
                    elif chck_api_url == 3:
                        response = api_post_retry(self.api_url_3, headers, body)
                    elif chck_api_url == 4:
                        response = api_post_retry(self.api_url_4, headers, body)
                    elif chck_api_url == 5:
                        response = api_post_retry(self.api_url_5, headers, body)
                    elif chck_api_url == 6:
                        response = api_post_retry(self.api_url_6, headers, body)
                    elif chck_api_url == 7:
                        response = api_post_retry(self.api_url_7, headers, body)
                    elif chck_api_url == 8:
                        response = api_post_retry(self.api_url_8, headers, body)
                    else:
                        response = api_post_retry(self.api_url_9, headers, body)
                    if response:
                        if response.ok:
                            result = response.text
                            chck_res = True
                            return result
                    else:
                        chck_api_url += 1
                    if chck_api_url > 10:
                        break
        except Exception as err:
            print("Error when get info of soc api parsing" + str(err))
            return result
        return result


if __name__ == '__main__':
    _test = ApiSocParsing()
    _ring_info = _test.request()
    _ring_dct = convert_api_vipa_xml_to_dct(_ring_info)
    import json
    _ring_conv_dct = json.loads(_ring_dct)
    data_flt_lst = []
    BOT_ID = config.__getattribute__('BOT_ID')
    BOT_GROUP_ID = -218379033
    BOT_GROUP_WHITELIST = config.__getattribute__('BOT_GROUP_WHITELIST')
    BOT_GROUP_VIP_CDBR_ID = config.__getattribute__('BOT_GROUP_VIP_CDBR_ID')
    TELEBOT_BOT_TOKEN = BOT_ID
    GROUP_CHAT_ID = int(BOT_GROUP_ID)
    import telebot
    bot = telebot.AsyncTeleBot(BOT_ID)
    from core.database.impl.bot.bot_impl import BotImpl
    _bot = BotImpl(bot)
    if 'data' in _ring_conv_dct:
        data_cus_lst = _ring_conv_dct['data']
        try:
            for x in data_cus_lst:
                data_flt_lst.append(dict(service_acc=x['service_acc'],
                                         user_type=x['user_type'],
                                         symptom=x['symptom'],
                                         problem_start_time=x['problem_start_time'],
                                         user_full_name=x['user_full_name'],
                                         user_address=x['user_address'],
                                         phone_number=x['phone_number']))
                mess = "Error Customer: " + x['service_acc'] + "\n" + "User Type:" + x['user_type'] + "\n" + \
                       "Symptom: " + x['symptom'] + "\n" + \
                       "Time: " + x['problem_start_time'] + "\n" + \
                       "User Full name: " + x['user_full_name'] + "\n" + \
                       "Address: " + x['user_address'] + "\n" + \
                       "Phone Number: " + x['phone_number']

                _bot.send_mess(mess, BOT_GROUP_VIP_CDBR_ID)

        except Exception as err:
            print("Error %s when get customer errror " % err)
    print(_ring_dct)


