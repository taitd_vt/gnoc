__author__ = 'VTN-DHML-DHIP10'
import random
from config import Development
from core.helpers.stringhelpers import convert_api_vipa_xml_to_dct
import requests, json
from core.helpers.api_helpers import api_post_retry

config = Development()
API_VIPA_PARSING = config.__getattribute__('API_VIPA_PARSING')
API_VIPA_PARSING_2 = config.__getattribute__('API_VIPA_PARSING_2')
API_VIPA_RING_USERNAME = config.__getattribute__('API_VIPA_RING_USERNAME')
API_VIPA_RING_PASSWORD = config.__getattribute__('API_VIPA_RING_PASSWORD')


class ApiParsing:
    def __init__(self, cmd_name, cmd, para_lst, vend, ver, node_code, node_user, node_pass):
        self.api_url = API_VIPA_PARSING
        self.username = API_VIPA_RING_USERNAME
        self.password = API_VIPA_RING_PASSWORD
        self.cmd_name = cmd_name
        self.cmd = cmd
        self.vend = vend
        self.ver = ver
        self.node_code = node_code
        self.node_user = node_user
        self.node_pass = node_pass
        self.para_lst = para_lst

    def request(self):
        result = ''
        try:
#            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.viettel.com/">
#   <soapenv:Header/>
#   <soapenv:Body>
#      <web:getDataJson>
#         <authorityBO>
#            <!--Optional:-->
#            <password>Vipa@123</password>
#            <!--Optional:-->
#            <requestId>?</requestId>
#            <!--Optional:-->
#            <userName>vipa</userName>
#         </authorityBO>
#         <requestInputBO>
#            <!--Optional:-->
#            <code>GET_DATA_FROM_NETWORK</code>
#            <compressData>0</compressData>
#            <!--Zero or more repetitions:-->
#            <params>
#               <name>_commandName</name>
#               <value><![CDATA[<vtp><huanvt><asr9000><show><interface>]]></value>
#            </params>
#            <params>
#               <name>_cmd</name>
#               <value>show interfaces description</value>
#            </params>
#            <params>
#               <name>_vendor</name>
#               <value>Cisco</value>
#            </params>
#            <params>
#               <name>_version</name>
#               <value>ASR9000</value>
#            </params>
#
#            <params>
#               <name>_nodeCode</name>
#               <value>SGP9102POP03</value>
#            </params>
#            <params>
#               <name>_account</name>
#               <value>thongplh</value>
#            </params>
#            <params>
#               <name>_password</name>
#               <value>alehap@123</value>
#            </params>
#            <!--Optional:-->
#            <query>?</query>
#         </requestInputBO>
#      </web:getDataJson>
#   </soapenv:Body>
#</soapenv:Envelope>
            headers = {'content-type': 'text/xml'}
            body_begn = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.viettel.com/">
                        <soapenv:Header/>
                        <soapenv:Body>
                           <web:getDataJson>
                              <authorityBO>
                                 <!--Optional:-->
                                 <password>""" + API_VIPA_RING_PASSWORD + """</password>
                                 <!--Optional:-->
                                 <requestId>?</requestId>
                                 <!--Optional:-->
                                 <userName>""" + API_VIPA_RING_USERNAME + """</userName>
                              </authorityBO>
                              <requestInputBO>
                                 <!--Optional:-->
                                 <code>GET_DATA_FROM_NETWORK</code>
                                 <compressData>0</compressData>
                                 <!--Zero or more repetitions:-->
                                 <params>
                                    <name>_commandName</name>
                                    <value><![CDATA[""" + self.cmd_name + """]]></value>
                                 </params>
                                 <params>
                                    <name>_cmd</name>
                                    <value>""" + self.cmd + """</value>
                                 </params>
                                 <params>
                                    <name>_vendor</name>
                                    <value>""" + self.vend + """</value>
                                 </params>
                                 <params>
                                    <name>_version</name>
                                    <value>""" + self.ver + """</value>
                                 </params>

                                 <params>
                                    <name>_nodeCode</name>
                                    <value>""" + self.node_code + """</value>
                                 </params>
                                 <params>
                                    <name>_account</name>
                                    <value>""" + self.node_user + """</value>
                                 </params>
                                 <params>
                                    <name>_password</name>
                                    <value>""" + self.node_pass + """</value>
                                 </params>"""
            body_end = """<!--Optional:-->
                                 <query>?</query>
                              </requestInputBO>
                           </web:getDataJson>
                        </soapenv:Body>
                    </soapenv:Envelope>"""
            body = ""
            if not self.para_lst:
                body = body_begn + body_end
            else:
                body_para = ""
                for x in self.para_lst:
                    if 'para_key' in x:
                        para_key = x['para_key']
                        if 'para_val' in x:
                            para_val = x['para_val']
                            body_para = body_para + """<params>
                                                        <name>""" + para_key + """</name>
                                                        <value>""" + para_val + """</value>
                                                    </params>"""
                body = body_begn + body_para + body_end
            if body != '':
                # response = requests.post(self.api_url, data=body, headers=headers)
                num1 = random.randint(0, 1)
                if num1 == 0:
                    api_url = API_VIPA_PARSING
                else:
                    api_url = API_VIPA_PARSING_2
                response = api_post_retry(api_url, headers, body)
                if response:
                    if response.ok:
                        result = response.text
                        return result
                else:
                    # api vipa bi loi change thang khac
                    if num1 == 0:
                        api_url = API_VIPA_PARSING_2
                    else:
                        api_url = API_VIPA_PARSING
                    response = api_post_retry(api_url, headers, body)
                    if response:
                        if response.ok:
                            result = response.text

        except Exception as err:
            print("Error when get info of vipa parsing" + str(err))
            return result
        return result


if __name__ == '__main__':
    _test = ApiParsing("<vtp><huanvt><asr9000><show><interface>", "show interfaces description", "Cisco",
                       "ASR9000", "SGP9102POP03", "thongplh", "alehap@123")
    _ring_info = _test.request()
    _ring_dct = convert_api_vipa_xml_to_dct(_ring_info)


