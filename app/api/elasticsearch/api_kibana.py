import json
from config import Config, Development
from core.helpers.date_helpers import get_date_now, convert_date_to_epoch, get_date_minus
from core.helpers.api_helpers import api_post_retry, api_get_retry
config = Development()
ELASTIC_SERVER = config.__getattribute__('ELASTIC_SERVER')
ELASTIC_PORT = config.__getattribute__('ELASTIC_PORT')
ELASTIC_USERNAME = config.__getattribute__('ELASTIC_USERNAME')
ELASTIC_PASSWORD = config.__getattribute__('ELASTIC_PASSWORD')


class ApiKibana:
    def __init__(self, time_epch_bgn, time_epch_end, tbl_srch):
        self.time_epch_bgn = time_epch_bgn
        self.time_epch_end = time_epch_end
        self.api_url = "http://" + ELASTIC_USERNAME + ':' + ELASTIC_PASSWORD + '@' + ELASTIC_SERVER + ":" + str(ELASTIC_PORT) + "/" + tbl_srch + "/_search"

    def get_cpct_util(self):

        _body_dct = {
            "aggs": {
                "2": {
                    "date_histogram": {
                        "field": "timestamp",
                        "interval": "15m",
                        "time_zone": "Asia/Ho_Chi_Minh",
                        "min_doc_count": 1
                    },
                    "aggs": {
                        "1": {
                            "sum": {
                                "script": "doc['network_type.keyword'].value == 'IPBN_BACKHAUL' ?doc['max'].value > 1000000 ?Math.round(doc['speed'].value*1000)/1000000000000.0 : doc['max_in'].value > 1000000 ?Math.round(doc['speed'].value*1000)/1000000000000.0 :0:0"
                            }
                        },
                        "3": {
                            "sum": {
                                "script": "doc['network_type.keyword'].value == 'IPBN_UPSTREAM' ?Math.round(doc['max'].value*1000)/1000000000000.0 : 0"
                            }
                        }
                    }
                }
            },
            "size": 0,
            "_source": {
                "excludes": []
            },
            "stored_fields": [
                "*"
            ],
            "script_fields": {
                "max_gbps": {
                    "script": {
                        "source": "Math.round(doc['max'].value*1000)/1000000000000.0",
                        "lang": "painless"
                    }
                },
                "speed_gbps": {
                    "script": {
                        "source": "Math.round(doc['speed'].value*1000)/1000000000000.0",
                        "lang": "painless"
                    }
                },
                "performance": {
                    "script": {
                        "source": "if (doc['max_in'].value >doc['max_out'].value){\nreturn doc['max_in'].value *100  /doc['speed'].value\n}else {\nreturn doc['max_out'].value *100 /doc['speed'].value\n}\n",
                        "lang": "painless"
                    }
                },
                "max_avg": {
                    "script": {
                        "source": "Math.round(doc['max'].value*900)/1000000000000.0",
                        "lang": "painless"
                    }
                }
            },
            "docvalue_fields": [
                {
                    "field": "timestamp",
                    "format": "date_time"
                }
            ],
            "query": {
                "bool": {
                    "must": [
                        {
                            "match_all": {}
                        },
                        {
                            "match_all": {}
                        },
                        {
                            "range": {
                                "timestamp": {
                                    "gte": self.time_epch_bgn,
                                    "lte": self.time_epch_end,
                                    "format": "epoch_millis"
                                }
                            }
                        }
                    ],
                    "filter": [],
                    "should": [],
                    "must_not": []
                }
            }

        }

        _body_json = json.dumps(_body_dct)

        url_api = self.api_url
        headers_json = {"Content-Type": "application/json"}
        bckt_lst = []
        # r = requests.post(url_api, headers=headers_json, data=body_json)
        r = api_post_retry(url_api, headers_json, _body_json)
        if r:
            if r.ok:
                result_json = json.loads(r.text)
                if 'aggregations' in result_json:
                    _agg = result_json['aggregations']
                    if '2' in _agg:
                        _agg_2 = _agg['2']
                        if 'buckets' in _agg_2:
                            bckt_lst = _agg_2['buckets']
                            return bckt_lst
        return bckt_lst

    def get_cable(self):
        _body_dct = {

            "aggs": {
                "2": {
                    "date_histogram": {
                        "field": "timestamp",
                        "interval": "15m",
                        "time_zone": "Asia/Ho_Chi_Minh",
                        "min_doc_count": 1
                    },
                    "aggs": {
                        "3": {
                            "terms": {
                                "field": "cable_name.keyword",
                                "size": 5,
                                "order": {
                                    "1": "desc"
                                }
                            },
                            "aggs": {
                                "1": {
                                    "sum": {
                                        "script": "doc['network_type.keyword'].value == 'IPBN_BACKHAUL' ?doc['max'].value > 1000000 ?Math.round(doc['speed'].value*1000)/1000000000000.0 : doc['max_in'].value > 1000000 ?Math.round(doc['speed'].value*1000)/1000000000000.0 :0:0"
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "size": 0,
            "_source": {
                "excludes": []
            },
            "stored_fields": [
                "*"
            ],
            "script_fields": {
                "max_gbps": {
                    "script": {
                        "source": "Math.round(doc['max'].value*1000)/1000000000000.0",
                        "lang": "painless"
                    }
                },
                "speed_gbps": {
                    "script": {
                        "source": "Math.round(doc['speed'].value*1000)/1000000000000.0",
                        "lang": "painless"
                    }
                },
                "performance": {
                    "script": {
                        "source": "if (doc['max_in'].value >doc['max_out'].value){\nreturn doc['max_in'].value *100  /doc['speed'].value\n}else {\nreturn doc['max_out'].value *100 /doc['speed'].value\n}\n",
                        "lang": "painless"
                    }
                },
                "max_avg": {
                    "script": {
                        "source": "Math.round(doc['max'].value*900)/1000000000000.0",
                        "lang": "painless"
                    }
                }
            },
            "docvalue_fields": [
                {
                    "field": "timestamp",
                    "format": "date_time"
                }
            ],
            "query": {
                "bool": {
                    "must": [
                        {
                            "match_all": {}
                        },
                        {
                            "match_all": {}
                        },
                        {
                            "match_phrase": {
                                "network_type.keyword": {
                                    "query": "IPBN_BACKHAUL"
                                }
                            }
                        },
                        {
                            "range": {
                                "timestamp": {
                                    "gte": self.time_epch_bgn,
                                    "lte": self.time_epch_end,
                                    "format": "epoch_millis"
                                }
                            }
                        },
                        {
                            "match_phrase": {
                                "network_type.keyword": {
                                    "query": "IPBN_BACKHAUL"
                                }
                            }
                        }
                    ],
                    "filter": [],
                    "should": [],
                    "must_not": []
                }
            }
        }
        _body_json = json.dumps(_body_dct)

        url_api = self.api_url
        headers_json = {"Content-Type": "application/json"}
        bckt_lst = []
        # r = requests.post(url_api, headers=headers_json, data=body_json)
        r = api_post_retry(url_api, headers_json, _body_json)
        if r:
            if r.ok:
                result_json = json.loads(r.text)
                if 'aggregations' in result_json:
                    _agg = result_json['aggregations']
                    if '2' in _agg:
                        _agg_2 = _agg['2']
                        if 'buckets' in _agg_2:
                            bckt_lst = _agg_2['buckets']
                            return bckt_lst
        return bckt_lst

    def get_total_backhaul_link(self):
        _body_dct = {
                      "aggs": {
                                "2": {
                                        "terms": {
                                                    "field": "network_type.keyword",
                                                    "size": 5,
                                                    "order": {
                                                                "_count": "desc"
                                                            }
                                        }
                                }
                      },
            "size": 0,
            "_source": {
                "excludes": []
            },
            "stored_fields": [
                "*"
            ],
            "script_fields": {
                "max_gbps": {
                    "script": {
                        "source": "Math.round(doc['max'].value*1000)/1000000000000.0",
                        "lang": "painless"
                    }
                },
                "speed_gbps": {
                    "script": {
                        "source": "Math.round(doc['speed'].value*1000)/1000000000000.0",
                        "lang": "painless"
                    }
                },
                "performance": {
                    "script": {
                        "source": "if (doc['max_in'].value >doc['max_out'].value){\nreturn doc['max_in'].value *100  /doc['speed'].value\n}else {\nreturn doc['max_out'].value *100 /doc['speed'].value\n}\n",
                        "lang": "painless"
                    }
                },
                "max_avg": {
                    "script": {
                        "source": "Math.round(doc['max'].value*900)/1000000000000.0",
                        "lang": "painless"
                    }
                }
            },
            "docvalue_fields": [
                {
                    "field": "query.range.@timestamp.gt",
                    "format": "date_time"
                },
                {
                    "field": "query.range.timestamp.gt",
                    "format": "date_time"
                },
                {
                    "field": "timestamp",
                    "format": "date_time"
                }
            ],
            "query": {
                "bool": {
                    "must": [
                        {
                            "match_all": {}
                        },
                        {
                            "match_all": {}
                        },
                        {
                            "range": {
                                "timestamp": {
                                    "gte": self.time_epch_bgn,
                                    "lte": self.time_epch_end,
                                    "format": "epoch_millis"
                                }
                            }
                        }
                    ],
                    "filter": [],
                    "should": [],
                    "must_not": []
                }
            }

        }
        _body_json = json.dumps(_body_dct)

        url_api = self.api_url
        headers_json = {"Content-Type": "application/json"}
        bckt_lst = []
        # r = requests.post(url_api, headers=headers_json, data=body_json)
        r = api_post_retry(url_api, headers_json, _body_json)
        if r:
            if r.ok:
                result_json = json.loads(r.text)
                if 'aggregations' in result_json:
                    _agg = result_json['aggregations']
                    if '2' in _agg:
                        _agg_2 = _agg['2']
                        if 'buckets' in _agg_2:
                            bckt_lst = _agg_2['buckets']
                            return bckt_lst
        return bckt_lst

    def get_total_upstream_transit_link(self):
        _body_dct = {
            "aggs": {
                "2": {
                    "terms": {
                        "field": "network_type.keyword",
                        "size": 5,
                        "order": {
                            "_count": "desc"
                        }
                    }
                }
            },
            "size": 0,
            "_source": {
                "excludes": []
            },
            "stored_fields": [
                "*"
            ],
            "script_fields": {
                "max_gbps": {
                    "script": {
                        "source": "Math.round(doc['max'].value*1000)/1000000000000.0",
                        "lang": "painless"
                    }
                },
                "speed_gbps": {
                    "script": {
                        "source": "Math.round(doc['speed'].value*1000)/1000000000000.0",
                        "lang": "painless"
                    }
                },
                "performance": {
                    "script": {
                        "source": "if (doc['max_in'].value >doc['max_out'].value){\nreturn doc['max_in'].value *100  /doc['speed'].value\n}else {\nreturn doc['max_out'].value *100 /doc['speed'].value\n}\n",
                        "lang": "painless"
                    }
                },
                "max_avg": {
                    "script": {
                        "source": "Math.round(doc['max'].value*900)/1000000000000.0",
                        "lang": "painless"
                    }
                }
            },
            "docvalue_fields": [
                {
                    "field": "query.range.@timestamp.gt",
                    "format": "date_time"
                },
                {
                    "field": "query.range.timestamp.gt",
                    "format": "date_time"
                },
                {
                    "field": "timestamp",
                    "format": "date_time"
                }
            ],
            "query": {
                "bool": {
                    "must": [
                        {
                            "match_all": {}
                        },
                        {
                            "match_all": {}
                        },
                        {
                            "match_phrase": {
                                "network_type.keyword": {
                                    "query": "IPBN_UPSTREAM"
                                }
                            }
                        },
                        {
                            "bool": {
                                "should": [
                                    {
                                        "match_phrase": {
                                            "transit_type.keyword": "TRANSIT-NORMAL"
                                        }
                                    },
                                    {
                                        "match_phrase": {
                                            "transit_type.keyword": "TRANSIT-VIP"
                                        }
                                    }
                                ],
                                "minimum_should_match": 1
                            }
                        },
                        {
                            "range": {
                                "timestamp": {
                                    "gte": self.time_epch_bgn,
                                    "lte": self.time_epch_end,
                                    "format": "epoch_millis"
                                }
                            }
                        },
                        {
                            "match_phrase": {
                                "network_type.keyword": {
                                    "query": "IPBN_UPSTREAM"
                                }
                            }
                        },
                        {
                            "bool": {
                                "should": [
                                    {
                                        "match_phrase": {
                                            "transit_type.keyword": "TRANSIT-NORMAL"
                                        }
                                    },
                                    {
                                        "match_phrase": {
                                            "transit_type.keyword": "TRANSIT-VIP"
                                        }
                                    }
                                ],
                                "minimum_should_match": 1
                            }
                        }
                    ],
                    "filter": [],
                    "should": [],
                    "must_not": []
                }
            }
        }
        _body_json = json.dumps(_body_dct)

        url_api = self.api_url
        headers_json = {"Content-Type": "application/json"}
        bckt_lst = []
        # r = requests.post(url_api, headers=headers_json, data=body_json)
        r = api_post_retry(url_api, headers_json, _body_json)
        if r:
            if r.ok:
                result_json = json.loads(r.text)
                if 'aggregations' in result_json:
                    _agg = result_json['aggregations']
                    if '2' in _agg:
                        _agg_2 = _agg['2']
                        if 'buckets' in _agg_2:
                            bckt_lst = _agg_2['buckets']
                            return bckt_lst
        return bckt_lst

    def get_total_congestion_backhaul_upstream(self):
        _body_dct = {
            "aggs": {
                "2": {
                    "terms": {
                        "field": "network_type.keyword",
                        "size": 30,
                        "order": {
                            "_key": "desc"
                        }
                    },
                    "aggs": {
                        "1": {
                            "max_bucket": {
                                "buckets_path": "1-bucket>1-metric"
                            }
                        },
                        "3": {
                            "max_bucket": {
                                "buckets_path": "3-bucket>3-metric"
                            }
                        },
                        "4": {
                            "max_bucket": {
                                "buckets_path": "4-bucket>4-metric"
                            }
                        },
                        "1-bucket": {
                            "date_histogram": {
                                "field": "timestamp",
                                "interval": "15m",
                                "time_zone": "Asia/Ho_Chi_Minh",
                                "min_doc_count": 1
                            },
                            "aggs": {
                                "1-metric": {
                                    "sum": {
                                        "script": "doc['max'].value * 100/doc['speed'].value  >98 ?1 :0"
                                    }
                                }
                            }
                        },
                        "3-bucket": {
                            "date_histogram": {
                                "field": "timestamp",
                                "interval": "15m",
                                "time_zone": "Asia/Ho_Chi_Minh",
                                "min_doc_count": 1
                            },
                            "aggs": {
                                "3-metric": {
                                    "sum": {
                                        "script": "doc['max'].value * 100/doc['speed'].value  >=95? doc['max'].value * 100/doc['speed'].value <98 ?1:0:0"
                                    }
                                }
                            }
                        },
                        "4-bucket": {
                            "date_histogram": {
                                "field": "timestamp",
                                "interval": "15m",
                                "time_zone": "Asia/Ho_Chi_Minh",
                                "min_doc_count": 1
                            },
                            "aggs": {
                                "4-metric": {
                                    "sum": {
                                        "script": "doc['max'].value * 100/doc['speed'].value  >=93? doc['max'].value * 100/doc['speed'].value <95 ?1:0:0"
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "size": 0,
            "_source": {
                "excludes": []
            },
            "stored_fields": [
                "*"
            ],
            "script_fields": {
                "max_gbps": {
                    "script": {
                        "source": "Math.round(doc['max'].value*1000)/1000000000000.0",
                        "lang": "painless"
                    }
                },
                "speed_gbps": {
                    "script": {
                        "source": "Math.round(doc['speed'].value*1000)/1000000000000.0",
                        "lang": "painless"
                    }
                },
                "performance": {
                    "script": {
                        "source": "if (doc['max_in'].value >doc['max_out'].value){\nreturn doc['max_in'].value *100  /doc['speed'].value\n}else {\nreturn doc['max_out'].value *100 /doc['speed'].value\n}\n",
                        "lang": "painless"
                    }
                },
                "max_avg": {
                    "script": {
                        "source": "Math.round(doc['max'].value*900)/1000000000000.0",
                        "lang": "painless"
                    }
                }
            },
            "docvalue_fields": [
                {
                    "field": "query.range.@timestamp.gt",
                    "format": "date_time"
                },
                {
                    "field": "query.range.timestamp.gt",
                    "format": "date_time"
                },
                {
                    "field": "timestamp",
                    "format": "date_time"
                }
            ],
            "query": {
                "bool": {
                    "must": [
                        {
                            "match_all": {}
                        },
                        {
                            "match_all": {}
                        },
                        {
                            "bool": {
                                "should": [
                                    {
                                        "match_phrase": {
                                            "network_type.keyword": "IPBN_UPSTREAM"
                                        }
                                    },
                                    {
                                        "match_phrase": {
                                            "network_type.keyword": "IPBN_BACKHAUL"
                                        }
                                    }
                                ],
                                "minimum_should_match": 1
                            }
                        },
                        {
                            "range": {
                                "timestamp": {
                                    "gte": self.time_epch_bgn,
                                    "lte": self.time_epch_end,
                                    "format": "epoch_millis"
                                }
                            }
                        },
                        {
                            "bool": {
                                "should": [
                                    {
                                        "match_phrase": {
                                            "network_type.keyword": "IPBN_UPSTREAM"
                                        }
                                    },
                                    {
                                        "match_phrase": {
                                            "network_type.keyword": "IPBN_BACKHAUL"
                                        }
                                    }
                                ],
                                "minimum_should_match": 1
                            }
                        }
                    ],
                    "filter": [],
                    "should": [],
                    "must_not": []
                }
            }
        }
        _body_json = json.dumps(_body_dct)

        url_api = self.api_url
        headers_json = {"Content-Type": "application/json"}
        bckt_lst = []
        # r = requests.post(url_api, headers=headers_json, data=body_json)
        r = api_post_retry(url_api, headers_json, _body_json)
        if r:
            if r.ok:
                result_json = json.loads(r.text)
                if 'aggregations' in result_json:
                    _agg = result_json['aggregations']
                    if '2' in _agg:
                        _agg_2 = _agg['2']
                        if 'buckets' in _agg_2:
                            bckt_tmp_lst = _agg_2['buckets']
                            for x in bckt_tmp_lst:
                                netw_type = ''
                                val_dct = dict()
                                if 'key' in x:
                                    val_dct['network_type'] = x['key']
                                else:
                                    val_dct['network_type'] = netw_type
                                if '1' in x:
                                    if 'value' in x['1']:
                                        if x['1']['value']:
                                            val_dct['performance_98'] = x['1']['value']
                                        else:
                                            val_dct['performance_98'] = 0

                                if '3' in x:
                                    if 'value' in x['3']:
                                        if x['3']['value']:
                                            val_dct['performance_95_98'] = x['3']['value']
                                        else:
                                            val_dct['performance_95_98'] = 0

                                if '4' in x:
                                    if 'value' in x['4']:
                                        if x['4']['value']:
                                            val_dct['performance_93'] = x['4']['value']
                                        else:
                                            val_dct['performance_93'] = 0
                                bckt_lst.append(val_dct)
                            return bckt_lst
        return bckt_lst

    def get_total_congestion_upstream_transit(self):
        _body_dct = {
            "aggs": {
                "2": {
                    "terms": {
                        "field": "network_type.keyword",
                        "size": 30,
                        "order": {
                            "_key": "desc"
                        }
                    },
                    "aggs": {
                        "1": {
                            "max_bucket": {
                                "buckets_path": "1-bucket>1-metric"
                            }
                        },
                        "3": {
                            "max_bucket": {
                                "buckets_path": "3-bucket>3-metric"
                            }
                        },
                        "4": {
                            "max_bucket": {
                                "buckets_path": "4-bucket>4-metric"
                            }
                        },
                        "1-bucket": {
                            "range": {
                                "script": {
                                    "source": "if (doc['max_in'].value >doc['max_out'].value){\nreturn doc['max_in'].value *100  /doc['speed'].value\n}else {\nreturn doc['max_out'].value *100 /doc['speed'].value\n}\n",
                                    "lang": "painless"
                                },
                                "ranges": [
                                    {
                                        "from": 98,
                                        "to": 1000
                                    }
                                ],
                                "keyed": "true"
                            },
                            "aggs": {
                                "1-metric": {
                                    "cardinality": {
                                        "field": "interface_name.keyword"
                                    }
                                }
                            }
                        },
                        "3-bucket": {
                            "range": {
                                "script": {
                                    "source": "if (doc['max_in'].value >doc['max_out'].value){\nreturn doc['max_in'].value *100  /doc['speed'].value\n}else {\nreturn doc['max_out'].value *100 /doc['speed'].value\n}\n",
                                    "lang": "painless"
                                },
                                "ranges": [
                                    {
                                        "from": 95,
                                        "to": 97.999
                                    }
                                ],
                                "keyed": "true"
                            },
                            "aggs": {
                                "3-metric": {
                                    "cardinality": {
                                        "field": "interface_name.keyword"
                                    }
                                }
                            }
                        },
                        "4-bucket": {
                            "range": {
                                "script": {
                                    "source": "if (doc['max_in'].value >doc['max_out'].value){\nreturn doc['max_in'].value *100  /doc['speed'].value\n}else {\nreturn doc['max_out'].value *100 /doc['speed'].value\n}\n",
                                    "lang": "painless"
                                },
                                "ranges": [
                                    {
                                        "from": 93,
                                        "to": 94.99999
                                    }
                                ],
                                "keyed": "true"
                            },
                            "aggs": {
                                "4-metric": {
                                    "cardinality": {
                                        "field": "interface_name.keyword"
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "size": 0,
            "_source": {
                "excludes": []
            },
            "stored_fields": [
                "*"
            ],
            "script_fields": {
                "max_gbps": {
                    "script": {
                        "source": "Math.round(doc['max'].value*1000)/1000000000000.0",
                        "lang": "painless"
                    }
                },
                "speed_gbps": {
                    "script": {
                        "source": "Math.round(doc['speed'].value*1000)/1000000000000.0",
                        "lang": "painless"
                    }
                },
                "performance": {
                    "script": {
                        "source": "if (doc['max_in'].value >doc['max_out'].value){\nreturn doc['max_in'].value *100  /doc['speed'].value\n}else {\nreturn doc['max_out'].value *100 /doc['speed'].value\n}\n",
                        "lang": "painless"
                    }
                },
                "max_avg": {
                    "script": {
                        "source": "Math.round(doc['max'].value*900)/1000000000000.0",
                        "lang": "painless"
                    }
                }
            },
            "docvalue_fields": [
                {
                    "field": "query.range.@timestamp.gt",
                    "format": "date_time"
                },
                {
                    "field": "query.range.timestamp.gt",
                    "format": "date_time"
                },
                {
                    "field": "timestamp",
                    "format": "date_time"
                }
            ],
            "query": {
                "bool": {
                    "must": [
                        {
                            "match_all": {}
                        },
                        {
                            "match_all": {}
                        },
                        {
                            "match_phrase": {
                                "network_type.keyword": {
                                    "query": "IPBN_UPSTREAM"
                                }
                            }
                        },
                        {
                            "match_phrase": {
                                "node_type.keyword": {
                                    "query": "POP_TRANSIT"
                                }
                            }
                        },
                        {
                            "bool": {
                                "should": [
                                    {
                                        "match_phrase": {
                                            "transit_type.keyword": "TRANSIT-NORMAL"
                                        }
                                    },
                                    {
                                        "match_phrase": {
                                            "transit_type.keyword": "TRANSIT-VIP"
                                        }
                                    }
                                ],
                                "minimum_should_match": 1
                            }
                        },
                        {
                            "range": {
                                "timestamp": {
                                    "gte": self.time_epch_bgn,
                                    "lte": self.time_epch_end,
                                    "format": "epoch_millis"
                                }
                            }
                        },
                        {
                            "match_phrase": {
                                "network_type.keyword": {
                                    "query": "IPBN_UPSTREAM"
                                }
                            }
                        },
                        {
                            "match_phrase": {
                                "node_type.keyword": {
                                    "query": "POP_TRANSIT"
                                }
                            }
                        },
                        {
                            "bool": {
                                "should": [
                                    {
                                        "match_phrase": {
                                            "transit_type.keyword": "TRANSIT-NORMAL"
                                        }
                                    },
                                    {
                                        "match_phrase": {
                                            "transit_type.keyword": "TRANSIT-VIP"
                                        }
                                    }
                                ],
                                "minimum_should_match": 1
                            }
                        }
                    ],
                    "filter": [],
                    "should": [],
                    "must_not": []
                }
            }
        }
        _body_json = json.dumps(_body_dct)

        url_api = self.api_url
        headers_json = {"Content-Type": "application/json"}
        bckt_lst = []
        # r = requests.post(url_api, headers=headers_json, data=body_json)
        r = api_post_retry(url_api, headers_json, _body_json)
        if r:
            if r.ok:
                result_json = json.loads(r.text)
                if 'aggregations' in result_json:
                    _agg = result_json['aggregations']
                    if '2' in _agg:
                        _agg_2 = _agg['2']
                        if 'buckets' in _agg_2:
                            bckt_tmp_lst = _agg_2['buckets']
                            for x in bckt_tmp_lst:
                                netw_type = ''
                                val_dct = dict()
                                if 'key' in x:
                                    val_dct['network_type'] = x['key']
                                else:
                                    val_dct['network_type'] = netw_type
                                if '1' in x:
                                    if 'value' in x['1']:
                                        if x['1']['value']:
                                            val_dct['performance_98'] = x['1']['value']
                                        else:
                                            val_dct['performance_98'] = 0
                                if '3' in x:
                                    if 'value' in x['3']:
                                        if x['3']['value']:
                                            val_dct['performance_95_98'] = x['3']['value']
                                        else:
                                            val_dct['performance_95_98'] = 0

                                if '4' in x:
                                    if 'value' in x['4']:
                                        if x['4']['value']:
                                            val_dct['performance_93'] = x['4']['value']
                                        else:
                                            val_dct['performance_93'] = 0
                                bckt_lst.append(val_dct)
                            return bckt_lst
        return bckt_lst

    def get_max_cpct_util(self):
        _bckt_lst = self.get_cpct_util()
        max_cpct = 0.0
        max_util = 0.0
        for x in _bckt_lst:
            _cpct = 0.0
            _util = 0.0
            if '1' in x:
                if 'value' in x['1']:
                    _cpct = x['1']['value']
            if '3' in x:
                if 'value' in x['3']:
                    _util = x['3']['value']
            if _cpct > max_cpct:
                max_cpct = _cpct
            if _util > max_util:
                max_util = _util
        return max_cpct, max_util

    def get_cable_cpct(self):
        _bckt_lst = self.get_cable()
        max_cpct = 0.0
        cbl_cpct_lst = []
        res_lst = []
        if _bckt_lst:
            for _bckt in _bckt_lst:
                tmp_max = 0.0
                if '3' in _bckt:
                    _bckt = _bckt['3']
                    if _bckt:
                        if 'buckets' in _bckt:
                            _cbl_lst = _bckt['buckets']
                            tmp_res_lst = []
                            for x in _cbl_lst:
                                tmp_max += x['1']['value']

                            tmp_res_lst = list(dict(cable_name=x['key'], capacity=x['1']['value']) for x in _cbl_lst)

                            if tmp_max > max_cpct:
                                max_cpct = tmp_max
                                res_lst = tmp_res_lst

        return max_cpct, res_lst


if __name__ == '__main__':
    _now = get_date_now()
    _now_end = get_date_minus(_now, 0)
    _now_bgn = get_date_minus(_now, 10)

    _now_end_epch = int(convert_date_to_epoch(_now_end) * 1000)
    _now_bgn_epch = int(convert_date_to_epoch(_now_bgn) * 1000)

    _api = ApiKibana(_now_bgn_epch, _now_end_epch, "tbl_interface_status")
    test = _api.get_total_backhaul_link()
    print(test)
