__author__ = 'vtn-dhml-dhip10'
import json
from config import Development
from core.helpers.tenant_helpers import get_ip_ipms_acc
from core.helpers.int_helpers import convert_string_to_float
from core.helpers.api_helpers import api_delete_retry, api_get_retry, api_post_retry, api_put_retry
config = Development()
API_ACCESS_LST = config.__getattribute__('API_ACCESS_LST')


class ApiIpms:
    def __init__(self, area, type):
        self.area = area
        self.type = type

    def get_intf_peak_from_group_lst(self, group_id, peak_rate):
        ip_ipms = get_ip_ipms_acc(self.area, self.type)
        res_lst = []
        if ip_ipms:
            url_api = "http://" + ip_ipms + "/api/?api=get_int_group_peak&gr=" + str(group_id)
            headers_json = {"Content-Type": "application/json"}
            try:
                r = api_get_retry(url_api, headers_json)
                # r = requests.get(url_api, headers=headers_json)

                if r.ok:
                        result_json = json.loads(r.text)
                        if isinstance(result_json, list):
                            for x in result_json:
                                try:
                                    max_inp_str = x['maxi_60']
                                    max_oup_str = x['maxo_60']
                                    speed_str = x['speed']
                                    intf_name = x['name'].upper()
                                    if max_inp_str and max_oup_str and speed_str:
                                        max_inp = convert_string_to_float(max_inp_str)
                                        max_oup = convert_string_to_float(max_oup_str)
                                        speed = convert_string_to_float(speed_str)
                                        if speed > 0:
                                            if intf_name.find('CPU') < 0 or intf_name.find('MEM') < 0 or \
                                               intf_name.find('USER') < 0 or intf_name.find('FAN') < 0 \
                                               or intf_name.find('POWER') < 0:

                                                max_inp *= 8
                                                max_oup *= 8

                                                # API chua thay doi phan Maxinput va maxout khi chua nhan vs 8
                                                x['maxi_60'] = max_inp
                                                x['maxo_60'] = max_oup

                                            perf = max(max_inp, max_oup) * 100 / speed
                                            if perf >= peak_rate:
                                                res_lst.append(x)

                                except Exception as err:
                                    print("Error when get ipms access")

                        return res_lst

                else:
                    return res_lst
            except Exception as err:
                print("Error %s when get result list interface peak" % str(err))
                return res_lst

    def get_cust_peak_from_group_lst(self, group_id, peak_rate):
        ip_ipms = get_ip_ipms_acc(self.area, self.type)
        res_lst = []
        if ip_ipms:
            url_api = "http://" + ip_ipms + "/api/?api=get_cus_group_peak&gr=" + str(group_id)
            headers_json = {"Content-Type": "application/json"}
            try:
                r = api_get_retry(url_api, headers_json)
                # r = requests.get(url_api, headers=headers_json)

                if r.ok:
                        result_json = json.loads(r.text)
                        if isinstance(result_json, list):
                            for x in result_json:
                                try:
                                    max_inp_str = x['maxi_60']
                                    max_oup_str = x['maxo_60']
                                    speed_str = x['speed']
                                    intf_name = x['name'].upper()
                                    if max_inp_str and max_oup_str and speed_str:
                                        max_inp = convert_string_to_float(max_inp_str)
                                        max_oup = convert_string_to_float(max_oup_str)
                                        x['maxi_60'] = max_inp
                                        x['maxo_60'] = max_oup
                                        speed = convert_string_to_float(speed_str)
                                        if 'name' in x:
                                            intf_name = x['name']
                                            pos_traf_tn = intf_name.upper().find('TRAFFIC_TN')
                                            pos_traf_qt = intf_name.upper().find('TRAFFIC_QT')
                                            if pos_traf_qt > 0 or pos_traf_tn > 0:
                                                max_inp = max_inp * 8
                                                max_oup = max_oup * 8 / 300
                                                x['maxi_60'] = max_inp
                                                x['maxo_60'] = max_oup

                                        if speed > 0:
                                            perf = max(max_inp, max_oup) * 100 / speed
                                            if perf >= peak_rate:
                                                res_lst.append(x)

                                except Exception as err:
                                    print("Error when get ipms access")

                        return res_lst

                else:
                    return res_lst
            except Exception as err:
                print("Error %s when get result API" % str(err))
                return res_lst


if __name__ == '__main__':
    _api = ApiIpms('KV3', 'kv3_acc_srt')
    res_lst = _api.get_intf_peak_from_group_lst(2541, 85)
    print(res_lst)


