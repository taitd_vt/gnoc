__author__ = 'VTN-DHML-DHIP10'

from config import Development
from core.helpers.stringhelpers import convert_api_vmsa_get_template_info_param_xml_to_dct
from core.helpers.api_helpers import api_post_retry

config = Development()
API_VMSA_PARSING = config.__getattribute__('API_VMSA_PARSING')
API_VMSA_USERNAME = config.__getattribute__('API_VMSA_USERNAME')
API_VMSA_PASSWORD = config.__getattribute__('API_VMSA_PASSWORD')


class VmsaGetTemplateInfoParam:
    def __init__(self, temp_id):
        self.api_url = API_VMSA_PARSING
        self.username = API_VMSA_USERNAME
        self.password = API_VMSA_PASSWORD
        self.temp_id = temp_id

    def request(self):
        result = ''
        try:

            headers = {'content-type': 'text/xml'}
            body_begn = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.viettel.com/">
                        <soapenv:Header/>
                        <soapenv:Body>
                           <web:getTemplateInfoParams>
                                 <passService>""" + API_VMSA_PASSWORD + """</passService>
                                 <userService>""" + API_VMSA_USERNAME + """</userService>
                                 <flowTemplateId>""" + str(self.temp_id) + """</flowTemplateId>
                           </web:getTemplateInfoParams>
                              """
            body_end = """<!--Optional:-->

                        </soapenv:Body>
                    </soapenv:Envelope>"""

            body = body_begn + body_end

            if body != '':
                # response = requests.post(self.api_url, data=body, headers=headers)
                response = api_post_retry(self.api_url, headers, body)
                if response:
                    if response.ok:
                        result = response.text
                        return result

        except Exception as err:
            print("Error when get info of vipa parsing" + str(err))
            return result
        return result

    def check_dct_xml(self, xml_res):
        _res_dct = convert_api_vmsa_get_template_info_param_xml_to_dct(xml_res)
        check = True

        try:
            if isinstance(_res_dct, dict):
                if _res_dct:
                    if 'resultCode' in _res_dct:
                        if _res_dct['resultCode'] != '1':
                            return False
                    if 'resultMessage' in _res_dct:
                        if _res_dct['resultMessage'] != 'Get template info param success list templateInfoParams 1':
                            return False
                    if 'templateId' in _res_dct:
                        if _res_dct['templateId'] != str(self.temp_id):
                            return False
        except Exception as err:
            print('Error %s when get VMSA get template info params ' % str(err))
            check = False
        return check


if __name__ == '__main__':
    _test = VmsaGetTemplateInfoParam(26333)
    _ring_info = _test.request()

    _check = _test.check_dct_xml(_ring_info)
    print(_check)


