__author__ = 'VTN-DHML-DHIP10'
from config import Development
from core.helpers.stringhelpers import convert_api_vipa_xml_to_dct, convert_api_vmsa_create_dt_log_for_son_xml_to_dct, convert_api_vmsa_create_dt_log_for_bsc_xml_to_str
import requests, json
from core.helpers.api_helpers import api_post_retry

config = Development()
API_VMSA_PARSING = config.__getattribute__('API_VMSA_PARSING')
API_VMSA_USERNAME = config.__getattribute__('API_VMSA_USERNAME')
API_VMSA_PASSWORD = config.__getattribute__('API_VMSA_PASSWORD')


class VmsaParsing:
    def __init__(self, temp_id, flow_run_name, para_lst, node_code, node_type_code, type_run_mop):
        self.api_url = API_VMSA_PARSING
        self.username = API_VMSA_USERNAME
        self.password = API_VMSA_PASSWORD
        self.temp_id = temp_id
        self.flow_run_name = flow_run_name
        self.node_type_code = node_type_code
        self.type_run_mop = type_run_mop
        self.node_code = node_code
        self.para_lst = para_lst

    def request(self):
        result = ''
        try:
            headers = {'content-type': 'text/xml'}
            body_begn = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.viettel.com/">
                        <soapenv:Header/>
                        <soapenv:Body>
                           <web:createDtLogForSon>
                                 <userService>""" + self.username + """</userService>
                                 <passService>""" + self.password + """</passService>
                                 <flowTemplateId>""" + str(self.temp_id) + """</flowTemplateId>
                                 <flowRunName>""" + self.flow_run_name + """</flowRunName>

                              <inputTemplateInfoParams>
                                 <templateInfoParams>
                                    <nodeCode>""" + self.node_code + """</nodeCode>
                                    <nodeTypeCode>""" + self.node_type_code + """</nodeTypeCode>

                               """
            body_end = """
                                 </templateInfoParams>
                              </inputTemplateInfoParams>
                              <typeRunMop>""" + str(self.type_run_mop) + """</typeRunMop>
                           </web:createDtLogForSon>
                        </soapenv:Body>
                    </soapenv:Envelope>"""
            body = ""
            if not self.para_lst:
                body = body_begn + body_end
            else:
                body_para = ""
                for x in self.para_lst:
                    if 'para_key' in x:
                        para_key = x['para_key']
                        if 'para_val' in x:
                            para_val = x['para_val']
                            body_para = body_para + """<params>
                                                        <paramCode>""" + para_key + """</paramCode>
                                                        <paramValue>""" + para_val + """</paramValue>
                                                        </params>
                                                   """
                body = body_begn + body_para + body_end
            if body != '':
                # response = requests.post(self.api_url, data=body, headers=headers)
                response = api_post_retry(self.api_url, headers, body)
                if response:
                    if response.ok:
                        result = response.text
                        return result

        except Exception as err:
            print("Error when get info of vipa parsing" + str(err))
            return result
        return result

    def check_dct_xml(self, _res_dct):

        check = True

        try:
            if isinstance(_res_dct, dict):
                if _res_dct:
                    if 'mopStatus' in _res_dct:
                        if str(self.type_run_mop) == '1':
                            if _res_dct['mopStatus'] != 'Run DT successful':
                                return False
                        elif str(self.type_run_mop) == '0':
                            if _res_dct['mopStatus'] != 'Create DT successful':
                                return False

        except Exception as err:
            print('Error %s when get VMSA create DT Log for SON ' % str(err))
            check = False
        return check


if __name__ == '__main__':
    para_lst = list()
    #para_dct = dict(para_key="MSISDN", para_val="84982491985")
    para_dct = dict(para_key="OP_STATE", para_val="BL")
    para_lst.append(para_dct)
    #_test = VmsaParsing(26333,"xxxxx-minhnd6-test", para_lst, "SGHL06", "SGSN", 1)
    _test = VmsaParsing(29020, "xxx-check-rnc-and-bsc", para_lst, "BCPD56", "BSC", 1)
    _ring_info = _test.request()
    #_res_dct = convert_api_vmsa_create_dt_log_for_son_xml_to_dct(_ring_info)
    _res_dct = convert_api_vmsa_create_dt_log_for_bsc_xml_to_str(_ring_info)
    #_ring_dct = _test.check_dct_xml(_ring_info)
    from core.helpers.encode.decode_base64 import DecodeBase64
    _encode = DecodeBase64(_res_dct['logFileContent'], 'milanox')
    key_lst = ['BUSY FULL RATE', 'BUSY HALF RATE', 'BUSY SDCCH', 'IDLE FULL RATE', 'IDLE HALF RATE',
               'IDLE SDCCH', 'BLOCKED RADIO TIME SLOTS', 'GPRS TIME SLOTS']
    _res_bsc_lst = _encode.read_file_get_val(key_lst)
    print(_res_bsc_lst)




