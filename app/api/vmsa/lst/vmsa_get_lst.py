__author__ = 'VTN-DHML-DHIP10'
import json
from config import Development
from core.helpers.stringhelpers import convert_api_vmsa_get_template_info_param_xml_to_dct
from core.helpers.api_helpers import api_post_retry

config = Development()
API_VMSA_LIST = config.__getattribute__('API_VMSA_LIST')
API_VMSA_LIST_USERNAME = config.__getattribute__('API_VMSA_LIST_USERNAME')
API_VMSA_LIST_PASSWORD = config.__getattribute__('API_VMSA_LIST_PASSWORD')


class VmsaGetLst:
    def __init__(self):
        self.api_url = API_VMSA_LIST
        self.username = API_VMSA_LIST_USERNAME
        self.password = API_VMSA_LIST_PASSWORD

    def request(self):
        result = []
        try:

            headers = {'content-type': 'application/x-www-form-urlencoded'}
            body = "userService=vipa&passService=Vipa%40123&input=%7B%22code%22%3A%22getListTemplateByGroup%22%2C+%22params%22%3A%5B%5D%7D"

            # response = requests.post(self.api_url, data=body, headers=headers)
            response = api_post_retry(self.api_url, headers, body)
            if response:
                if response.ok:
                    res_str = response.text
                    # convert to json
                    res_json = json.loads(res_str)
                    if isinstance(res_json, dict):
                        if 'data' in res_json:
                            return res_json['data']
                    return res_json

        except Exception as err:
            print("Error when get info of vipa parsing" + str(err))
            return result
        return result

    def find_template(self, template_name):
        # tim chinh xac
        temp_lst = self.request()
        if isinstance(temp_lst, list):
            for x in temp_lst:
                if 'flow_template_name' in x:
                    if template_name.upper() == x['flow_template_name'].upper():
                        return x
        return dict()


if __name__ == '__main__':
    _test = VmsaGetLst()
    _ring_info = _test.request()

    _test_name = _test.find_template("KHM.NSN.BSC3i.GSM17.change.HSN")
    #print(_test_name)
    #print(_ring_info)


