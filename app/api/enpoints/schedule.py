from flask import request
from flask_restplus import Resource
from core.database.impl.schedule_impl import ScheduleImpl
from core.database.model.schedule import Schedule

from ..parsers import schedule_arguments
from ..serializers import list_schedule, schedule
from flask import jsonify
import json


from ..restful import api

ns = api.namespace('v1/schedules', description='Operations related to ironman schedules')

@ns.route('/')
class ScheduleCollection(Resource):
    @api.expect(schedule_arguments)
    @api.marshal_with(schedule)
    def get(self):
        """
        Returns list of schedules.
        """

        schedule_impl = ScheduleImpl()

        args = schedule_arguments.parse_args(request)
        #approve = args.get('approved', None)
        #active = args.get('active', None)

        _data_filter = {
            'approved': args.get('approved', None),
            'active': args.get('active', None),
            'mechanism': args.get('mechanism', None),
            'weekly': args.get('weekly', None),
        }
        #posts_query = Post.query
        #posts_page = posts_query.paginate(page, per_page, error_out=False)
        schedules = schedule_impl.filter(**_data_filter)
        data = [{'schedule_id':s.schedule_id, 'name': s.name,
                 'devices':s.devices, 'time': s.time, 'active':s.active,
                 'weekly':s.weekly, 'mechanism':s.mechanism,
                 'templates': s.templates, 'interval':s.interval,
                 'template_name': s.template_name} for s in schedules]
        return data

    '''@api.expect(blog_post)
    def post(self):
        """
        Creates a new blog post.
        """
        create_blog_post(request.json)
        return None, 201'''
