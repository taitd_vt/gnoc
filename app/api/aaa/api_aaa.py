__author__ = 'VTN-DHML-DHIP10'
from config import Development
from core.database.impl.api_spring_ipms_impl import ApiSpringIpmsImpl
config = Development()
API_SPRING_LIST = config.__getattribute__('TENANT_ID_LST')


class ApiAaa:
    def __init__(self, area):
        self.area = area.upper()
        self.id = self.get_id()

    def get_id(self):
        api_lst = API_SPRING_LIST
        result = ''
        for x in api_lst:
            if 'id' in x:
                tenant_id = x['id'].upper()
                if tenant_id.find('AAA') >= 0 and tenant_id.find(self.area) >=0:
                    return tenant_id
        return result

    def get_online_lst(self, acc_lst):
        # vi yeu cau cua URL Length nen tach ra chia lam 80 user 1 lan
        div_lst = [acc_lst[x:x+70] for x in range(0, len(acc_lst), 70)]
        # print(div_lst)
        # request
        api_aaa = ApiSpringIpmsImpl('online_aaa', self.id)
        bras_lst = self.get_bras_lst()

        online_lst = list()
        for acc_sml_lst in div_lst:
            acc_sml_str = ''
            for acc_sml in acc_sml_lst:
                if acc_sml_str != '':
                    acc_sml_str += ',' + acc_sml
                else:
                    acc_sml_str = acc_sml

            search_dct = dict(searchAcc=acc_sml_str)
            res_lst = api_aaa.get(search_dct)

            if res_lst:
                for x in res_lst:
                    if 'bras' in x:
                        bras_ip_x = x['bras']
                        bras_name = self.search_bras_name(bras_ip_x, bras_lst)
                        if bras_name:
                            x['bras_name'] = bras_name
                online_lst = online_lst + res_lst
            # print(online_lst)

        return online_lst

    def get_bras_lst(self):
        api_bras = ApiSpringIpmsImpl('bras_aaa', self.id)
        search_dct = dict()
        bras_lst = api_bras.get(search_dct)
        # print(bras_lst)
        return bras_lst

    def search_bras_name(self, bras_ip, bras_lst):
        result = ''
        for x in bras_lst:
            if 'bras' in x:
                bras_ip_x = x['bras']
                if 'brasname' in x:
                    bras_name = x['brasname']
                    if bras_ip == bras_ip_x:
                        return bras_name
        return result


if __name__ == '__main__':
    api_aaa = ApiAaa('kv1')
    api_aaa.get_online_lst()