__author__ = 'VTN-DHML-DHIP10'
from config import Development
from core.helpers.stringhelpers import convert_api_vipa_xml_to_dct, convert_api_television_check_user_to_dct
from core.helpers.api_helpers import api_post_retry, api_post_basic_auth_retry

config = Development()
API_TELEVISION = config.__getattribute__('API_TELEVISION')
API_TELEVISION_USERNAME = config.__getattribute__('API_TELEVISION_USERNAME')
API_TELEVISION_PASSWORD = config.__getattribute__('API_TELEVISION_PASSWORD')


class QosApiServer:
    def __init__(self, account):
        self.api_url = API_TELEVISION
        self.account = account
        self.user = API_TELEVISION_USERNAME
        self.pswd = API_TELEVISION_PASSWORD

    def request(self):
        result = ''
        try:
            headers = {'content-type': 'text/xml'}
            body_begn = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:apis="http://10.60.66.228:8080/APIService">\r
                               <soapenv:Header/>\r
                               <soapenv:Body>\r
                                  <apis:GetSTBStatusReq>\r
                                     <apis:FilterType>AccountID</apis:FilterType>\r
                                     <apis:FilterData>""" + str(self.account) + """</apis:FilterData>\r
                                  </apis:GetSTBStatusReq>\r
                               </soapenv:Body>\r
                            </soapenv:Envelope>

                               """

            body = body_begn
            if body != '':
                # response = requests.post(self.api_url, data=body, headers=headers)
                response = api_post_basic_auth_retry(self.api_url, headers, body, self.user, self.pswd)
                if response:
                    if response.ok:
                        result = response.text
                        return result

        except Exception as err:
            print("Error when get info of vipa parsing" + str(err))
            return result
        return result

    def check_dct_xml(self, _res_dct):

        check = True

        try:
            if isinstance(_res_dct, dict):
                if _res_dct:
                    if 'mopStatus' in _res_dct:
                        if str(self.type_run_mop) == '1':
                            if _res_dct['mopStatus'] != 'Run DT successful':
                                return False
                        elif str(self.type_run_mop) == '0':
                            if _res_dct['mopStatus'] != 'Create DT successful':
                                return False

        except Exception as err:
            print('Error %s when get VMSA create DT Log for SON ' % str(err))
            check = False
        return check


if __name__ == '__main__':
    para_lst = list()
    para_dct = dict(para_key="MSISDN", para_val="84982491985")
    para_lst.append(para_dct)
    _test = QosApiServer("q055_gmts_nguyennt1")
    _ring_info = _test.request()
    _res_dct = convert_api_television_check_user_to_dct(_ring_info)
    for k, v in _res_dct.items():
        print("Key %s value " % (str(k)))
        pass
    #_ring_dct = _test.check_dct_xml(_ring_info)
    #print(_ring_dct)



