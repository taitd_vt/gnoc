__author__ = 'VTN-DHML-DHIP10'
from config import Development
from core.helpers.stringhelpers import convert_api_vipa_xml_to_dct, convert_api_television_check_user_to_dct, \
    convert_api_vipa_television_xml_to_dct
from core.helpers.api_helpers import api_post_retry, api_post_basic_auth_retry

config = Development()
API_SOC_LST = config.__getattribute__('API_SOC_LST')


class ApiSocTvServer:
    # muc dich xay dung ra request o day
    def __init__(self):
        self.api_url = API_SOC_LST

    def request(self):
        result = ''
        try:
            headers = {'content-type': 'text/xml'}
            body_begn = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:fun="http://function.webservice.viettel.com/">
                                <soapenv:Header/>
                                    <soapenv:Body>
                                        <fun:getDataJson>
                                            <authorityBO>
                                                <!--Optional:-->
                                                <password>bed0c2ddbfff2e0e3077e2dc2885b38a</password>
                                                <!--Optional:-->
                                                <requestId>1111</requestId>
                                                <!--Optional:-->
                                                <userName>6f2efd0770ec8d37c22a0e885a32707f</userName>
                                            </authorityBO>
                                        <requestInputBO>
                                        <!--Optional:-->
                                    <code>WS_GET_DATA_KH_VIP_TRUYEN_HINH</code>
                                    <compressData></compressData>
                                    <!--Zero or more repetitions:-->                          
                                    <!--Optional:-->
                                <query></query>
                            </requestInputBO>
                        </fun:getDataJson>
                    </soapenv:Body>
                    </soapenv:Envelope>
                               """

            body = body_begn
            if body != '':
                # response = requests.post(self.api_url, data=body, headers=headers)
                check = False
                for api_url in self.api_url:
                    try:
                        response = api_post_retry(api_url, headers, body)
                        if response:
                            check = True
                            if response.ok:
                                result = response.text
                                return result
                    except Exception as err:
                        print("Error when get info of soc parsing " + str(err))
                        return result

        except Exception as err:
            print("Error when get info of soc parsing" + str(err))
            return result
        return result

    def request_custom_normal(self):
            result = ''
            try:
                headers = {'content-type': 'text/xml'}
                body_begn = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:fun="http://function.webservice.viettel.com/">
                                    <soapenv:Header/>
                                        <soapenv:Body>
                                            <fun:getDataJson>
                                                <authorityBO>
                                                    <!--Optional:-->
                                                    <password>bed0c2ddbfff2e0e3077e2dc2885b38a</password>
                                                    <!--Optional:-->
                                                    <requestId>1111</requestId>
                                                    <!--Optional:-->
                                                    <userName>6f2efd0770ec8d37c22a0e885a32707f</userName>
                                                </authorityBO>
                                            <requestInputBO>
                                                <!--Optional:-->
                                                <code>WS_GET_DATA_TRUYEN_HINH_GIAT_VO_HINH</code>
                                                <compressData></compressData>
                                                <!--Zero or more repetitions:-->                          
                                                <!--Optional:-->
                                                <query></query>
                                            </requestInputBO>
                                        </fun:getDataJson>
                                    </soapenv:Body>
                                </soapenv:Envelope>
                                   """

                body = body_begn
                if body != '':
                    # response = requests.post(self.api_url, data=body, headers=headers)
                    for api_url in self.api_url:
                        response = api_post_retry(api_url, headers, body)
                        if response:
                            if response.ok:
                                result = response.text
                                return result

            except Exception as err:
                print("Error when get info of soc parsing" + str(err))
                return result
            return result


if __name__ == '__main__':
    para_lst = list()
    para_dct = dict(para_key="MSISDN", para_val="84982491985")
    para_lst.append(para_dct)
    _test = ApiSocTvServer()
    _ring_info = _test.request()
    _res_dct = convert_api_vipa_television_xml_to_dct(_ring_info)
    for k, v in _res_dct.items():
        print("Key %s value " % (str(k)))
        pass
    #_ring_dct = _test.check_dct_xml(_ring_info)
    #print(_ring_dct)



