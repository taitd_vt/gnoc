from flask import Flask
from flask_bootstrap import Bootstrap
from mongoengine import connect
from flask import Flask, Response, redirect, url_for, request, session, abort
from flask_login import LoginManager, UserMixin, \
login_required, login_user, logout_user
from core.database.impl.account_impl import User
# from flask.ext.login import LoginManager, current_user
from config import config
from core.database.impl.account_impl import User, UserRepository
from datetime import timedelta
bootstrap = Bootstrap()
login_manager = LoginManager()


def create_app(config_name):
    app = Flask(__name__, template_folder='templates')
    app.config.from_object('appconfig')
    app.config.from_object('mailconfig')
    app.config.from_object(config[config_name])
    app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(minutes=5)

    app.config.update(
        DEBUG=True,
        SECRET_KEY='secret_xxx'
    )

    # flask-login
    login_manager.session_protection = 'strong'
    login_manager.login_view = "auth.login"
    login_manager.init_app(app)

    connect(config[config_name].MONGO_DATABASE_NAME, host=config[config_name].MONGO_DATABASE_SERVER,
            port=config[config_name].MONGO_DATABASE_PORT)

    config[config_name].init_app(app)

    bootstrap.init_app(app)
    #api = create_api()
    #login_manager.session_protection = 'strong'
    #login_manager.login_view = 'auth.login'
    #login_manager.init_app(app)


    #app.session_interface = MongoEngineSessionInterface(db)

    # Specify the debug panels you want
    app.config['DEBUG_TB_PANELS'] = [
        'flask_debugtoolbar.panels.versions.VersionDebugPanel',
        'flask_debugtoolbar.panels.timer.TimerDebugPanel',
        'flask_debugtoolbar.panels.headers.HeaderDebugPanel',
        'flask_debugtoolbar.panels.request_vars.RequestVarsDebugPanel',
        'flask_debugtoolbar.panels.template.TemplateDebugPanel',
        'flask_debugtoolbar.panels.sqlalchemy.SQLAlchemyDebugPanel',
        'flask_debugtoolbar.panels.logger.LoggingPanel',
        #'flask_debugtoolbar.panels.profiler.ProfilerDebugPanel',
        # Add the MongoDB panel
        #'flask.ext.mongoengine.panels.MongoDebugPanel',
    ]
    app.config['DEBUG_TB_INTERCEPT_REDIRECTS'] = False

    #toolbar = DebugToolbarExtension(app)

    ############### begin blueprint ############################
    from app.controllers.auth import auth as auth_blueprint
    from app.controllers.nocpro_afe import nocpro_afe as nocpro_afe_blueprint
    from app.controllers.check import check as check_blueprint
    from app.controllers.chart import chart as chart_blueprint
    from app.controllers.home import home as home_blueprint
    from app.controllers.deny_web import deny_web as deny_web_blueprint
    from app.controllers.kick_user import kick as kick_user_blueprint
    from app.controllers.thread_control import thread_control as thread_control_blueprint
    from app.controllers.nocpro_tbl_cabinet_bkk import nocpro_cabinet_bkk as nocpro_cabinet_bkk_blueprint
    from app.controllers.nocpro_tbl_cell_bkk import nocpro_cell_bkk as nocpro_cell_bkk_blueprint
    from app.controllers.tbl_insert_interface_group_server import insert_intf_group as insert_intf_group_blueprint
    from app.controllers.tbl_interface_not_add_group_server import intf_not_add_group as intf_not_add_group_blueprint
    from app.controllers.tbl_log_insert_interface_group_server import log_insert_intf_group as log_insert_intf_group_blueprint

    app.register_blueprint(check_blueprint, url_prefix='/check')
    app.register_blueprint(nocpro_afe_blueprint, url_prefix='/nocpro_afe')
    app.register_blueprint(auth_blueprint, url_prefix='/auth')
    app.register_blueprint(chart_blueprint, url_prefix='/chart')
    app.register_blueprint(thread_control_blueprint, url_prefix='/thread')
    app.register_blueprint(home_blueprint, url_prefix='/home')
    app.register_blueprint(deny_web_blueprint, url_prefix='/deny_web')
    app.register_blueprint(kick_user_blueprint, url_prefix='/kick')
    app.register_blueprint(home_blueprint, url_prefix='/')
    app.register_blueprint(nocpro_cabinet_bkk_blueprint, url_prefix='/nocpro_cabinet_bkk')
    app.register_blueprint(nocpro_cell_bkk_blueprint, url_prefix='/nocpro_cell_bkk')
    app.register_blueprint(insert_intf_group_blueprint, url_prefix='/insert_intf_group')
    app.register_blueprint(intf_not_add_group_blueprint, url_prefix='/intf_not_add_group')
    app.register_blueprint(log_insert_intf_group_blueprint, url_prefix='/log_insert_intf_group')





    ############### end blueprint   ############################

    ############### api blueprint ##############################

    ################## Celery ##########################################

    return app



