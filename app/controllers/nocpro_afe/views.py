# -*- coding:utf-8 -*-
from flask import render_template, flash, request, redirect, url_for
from app.controllers.nocpro_afe import nocpro_afe
from flask_login import login_required
from flask import send_file
from flask_paginate import Pagination, get_page_args
import os, json
import csv
from app.controllers.deny_web.forms import DenyWebForm
import uuid
from core.helpers.stringhelpers import check_legal_str
from flask_login import current_user
from core.database.impl.account_impl import User
from core.helpers.date_helpers import get_date_minus_format_ipms, get_date_now_format_ipms, \
    get_date_now_format_elastic, get_date_now
from core.database.impl.nocpro.tbl_afe_cat_data_free_load import TblAfeCatDataFreeLoad
from .forms import SearchForm

app_root = os.path.dirname(os.path.abspath(__file__))


def get_users(tbl_lst, offset=0, per_page=50):
    return tbl_lst[offset: offset + per_page]


@nocpro_afe.route('/', methods=['GET'])
@login_required
def index():
    tbl_bkk = TblAfeCatDataFreeLoad()
    chek_form = SearchForm(request.form)
    tbl_lst = []
    try:
        user_type = current_user.find_type_by_id(current_user.id)
        if user_type == 'editor' or user_type == 'admin_web':
            tbl_lst = tbl_bkk.get_lst()
        else:
            flash("You don't have permission here!")
            redirect(url_for('nocpro_afe.index'))

        page, per_page, offset = get_page_args(page_parameter='page',
                                               per_page_parameter='per_page')
        total = len(tbl_lst)
        pagination_tbl = get_users(tbl_lst, offset=offset, per_page=10)
        pagination = Pagination(page=page, per_page=per_page, total=total, css_framework='bootstrap4')
        return render_template("nocpro_afe/index.html", err="", tbl_lst=pagination_tbl,
                               page=page,
                               per_page=per_page,
                               pagination=pagination,
                               form=chek_form,
                               key_name='')
    except Exception as err:

        tbl_bkk = TblAfeCatDataFreeLoad()
        tbl_lst = tbl_bkk.get_lst()
        flash(err)
        return render_template("nocpro_afe/index.html", tbl_lst=tbl_lst, form=chek_form,
                               key_name='')


@nocpro_afe.route('/search/', methods=['POST'])
@login_required
def index_srch():
    chek_form = SearchForm(request.form)

    try:
        user_type = current_user.find_type_by_id(current_user.id)
        if user_type == 'editor' or user_type == 'admin_web':
            if request.method == 'POST':
                key_name_new = chek_form.key_name.data
                if key_name_new:
                    return redirect(url_for('nocpro_afe.index_srch_pretty', key_name=key_name_new))
                else:
                    return redirect(url_for('nocpro_afe.index'))

        else:
            flash("You don't have permission here!")
            redirect(url_for('nocpro_afe.index'))

    except Exception as err:
        flash(err)
        return redirect(url_for('nocpro_afe.index'))


@nocpro_afe.route('/search/<key_name>')
@login_required
def index_srch_pretty(key_name):

    tbl_bkk = TblAfeCatDataFreeLoad()
    chek_form = SearchForm(request.form)
    tbl_lst = []
    try:
        user_type = current_user.find_type_by_id(current_user.id)
        if user_type == 'editor' or user_type == 'admin_web':
            chek_form.key_name.default = key_name
            chek_form.process()
            tbl_lst = tbl_bkk.find_like(key_name)
        else:
            flash("You don't have permission here!")
            redirect(url_for('nocpro_afe.index', key_name=key_name))

        page, per_page, offset = get_page_args(page_parameter='page',
                                               per_page_parameter='per_page')
        total = len(tbl_lst)
        pagination_tbl = get_users(tbl_lst, offset=offset, per_page=10)
        pagination = Pagination(page=page, per_page=per_page, total=total, css_framework='bootstrap4')
        return render_template("nocpro_afe/index.html", err="", tbl_lst=pagination_tbl,
                               page=page,
                               per_page=per_page,
                               pagination=pagination,
                               form=chek_form,
                               key_name=key_name)
    except Exception as err:

        tbl_bkk = TblAfeCatDataFreeLoad()
        tbl_lst = tbl_bkk.get_lst()
        flash(err)
        return render_template("nocpro_afe/index.html", tbl_lst=tbl_lst, form=chek_form,
                               key_name=key_name)

