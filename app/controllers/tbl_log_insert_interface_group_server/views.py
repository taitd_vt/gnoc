# -*- coding:utf-8 -*-
from flask import render_template, flash, request, redirect, url_for
from app.controllers.tbl_log_insert_interface_group_server import log_insert_intf_group
from flask_login import login_required, current_user
from flask import send_file
from flask_paginate import Pagination, get_page_args
from core.database.impl.server_minhnd.tbl_log_interface_add_group_impl import TblLogInterfaceAddGroupImpl
import os, json
import csv

import uuid
from core.helpers.stringhelpers import check_legal_str
from core.database.impl.account_impl import User
from core.helpers.date_helpers import get_date_minus_format_ipms, get_date_now_format_ipms, \
    get_date_now_format_elastic, get_date_now, convert_date_str_to_date_obj_ipms
from core.database.impl.nocpro.tbl_cabinet_bkk import TblCabinetBkk
from .forms import CabinetBKKForm, SearchForm

app_root = os.path.dirname(os.path.abspath(__file__))


def get_users(tbl_lst, offset=0, per_page=50):
    return tbl_lst


@log_insert_intf_group.route('/', methods=['GET'])
@login_required
def index():
    time_now = get_date_now()
    _tbl_obj = TblLogInterfaceAddGroupImpl(timestamp=time_now,
                                           host_name='',
                                           description='',
                                           intf='',
                                           area='KV1',
                                           speed='',
                                           group_name='',
                                           interface_id='',
                                           user='',
                                           host_group_name=''
                                           )
    chek_form = SearchForm(request.form)
    tbl_lst = []
    try:
        user_type = current_user.find_type_by_id(current_user.id)
        page, per_page, offset = get_page_args(page_parameter='page',
                                               per_page_parameter='per_page')
        per_page = 50

        if user_type == 'editor' or user_type == 'admin_web':
            tbl_lst = _tbl_obj.list_page(page, 50)
        else:
            flash("You don't have permission here!")
            redirect(url_for('log_insert_intf_group.index'))

        total = _tbl_obj.total()
        if total:
            total = int(total)
        pagination_tbl = get_users(tbl_lst, offset=offset, per_page=50)
        pagination = Pagination(page=page, per_page=50, total=total, css_framework='bootstrap4')
        return render_template("log_insert_intf_group/index.html", err="", tbl_lst=pagination_tbl,
                               page=page,
                               per_page=per_page,
                               pagination=pagination,
                               form=chek_form,
                               key_name='',
                               total=total)
    except Exception as err:
        tbl_lst = _tbl_obj.list_page(1, 50)
        total = _tbl_obj.total()
        flash(err)
        return render_template("log_insert_intf_group/index.html", tbl_lst=tbl_lst, form=chek_form,
                               key_name='', total=total)


@log_insert_intf_group.route('/search/', methods=['POST'])
@login_required
def index_srch():
    chek_form = SearchForm(request.form)

    try:
        user_type = current_user.find_type_by_id(current_user.id)
        if user_type == 'editor' or user_type == 'admin_web':
            if request.method == 'POST':
                key_name_new = chek_form.key_name.data
                if key_name_new:
                    return redirect(url_for('log_insert_intf_group.index_srch_pretty', key_name=key_name_new))
                else:
                    return redirect(url_for('log_insert_intf_group.index'))

        else:
            flash("You don't have permission here!")
            redirect(url_for('log_insert_intf_group.index'))

    except Exception as err:
        flash(err)
        return redirect(url_for('log_insert_intf_group.index'))


@log_insert_intf_group.route('/search/<key_name>')
@login_required
def index_srch_pretty(key_name):
    time_now = get_date_now()
    _tbl_obj = TblLogInterfaceAddGroupImpl(timestamp=time_now,
                                           host_name='',
                                           description='',
                                           intf='',
                                           area='KV1',
                                           speed='',
                                           group_name='',
                                           interface_id='',
                                           user='',
                                           host_group_name=''
                                           )
    chek_form = SearchForm(request.form)
    tbl_lst = []
    try:
        user_type = current_user.find_type_by_id(current_user.id)

        page, per_page, offset = get_page_args(page_parameter='page',
                                               per_page_parameter='per_page')
        per_page = 50
        if user_type == 'editor' or user_type == 'admin_web':
            chek_form.key_name.default = key_name
            chek_form.process()
            srch_dct = dict(search=key_name, page=page, pageSize=50)
            tbl_lst = _tbl_obj.find_like(**srch_dct)
        else:
            flash("You don't have permission here!")
            redirect(url_for('log_insert_intf_group.index'))

        total = _tbl_obj.total_key_name(key_name)
        if total:
            total = int(total)
        pagination_tbl = get_users(tbl_lst, offset=offset, per_page=50)

        pagination = Pagination(page=page, per_page=per_page, total=total, css_framework='bootstrap4')
        return render_template("log_insert_intf_group/index.html", err="", tbl_lst=pagination_tbl,
                               page=page,
                               per_page=per_page,
                               pagination=pagination,
                               form=chek_form,
                               key_name=key_name,
                               total=total)
    except Exception as err:
        flash(err)
        return redirect(url_for('log_insert_intf_group.index'))
