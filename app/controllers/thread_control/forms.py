from wtforms import StringField, PasswordField, BooleanField, Form, IntegerField, SelectField, validators


class ThreadForm(Form):
    name = StringField(u'Thread Name', [validators.DataRequired()])
    status = SelectField(u'Status', choices=[('START_PENDING', 'START_PENDING'), ('STOP_PENDING', 'STOP_PENDING')])
    last_update = StringField(u'Last Update')


