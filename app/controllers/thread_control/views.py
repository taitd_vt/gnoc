from flask import render_template, flash, request, redirect, url_for
from . import thread_control
from core.database.impl.thread_control_impl import ThreadControlImpl
from app.controllers.thread_control.forms import ThreadForm


#@thread_control.route('/')
#def index():
#    try:
#        thread_impl = ThreadControlImpl("")
#        thread_lst = thread_impl.get_all()
#        if request.method == 'POST':
#            pass
#        return render_template("thread_control/index.html", thread_lst=thread_lst)
#    except Exception as e:
#        print('Error %s when checking chart of token %s' % (e))
#        flash('Error %s' % str(e))
#        return render_template("thread_control/index.html", json_data=list())
#
#
#@thread_control.route('/edit/<thread_name>', methods=['GET', 'POST'])
#def edit(thread_name):
#    try:
#        thread_impl = ThreadControlImpl(thread_name)
#        thread_obj = thread_impl.get()
#        thread_form = ThreadForm(request.form)
#
#        status = ""
#        if thread_obj:
#            status = thread_obj["status"]
#            if status == "START":
#                choices = [("START", "START"), ("STOPPENDING", "STOP_PENDING")]
#            elif status == "STOP":
#                choices = [("STOP", "STOP"), ("STARTPENDING", "START_PENDING")]
#            else:
#                choices = [("STOPPENDING", "STOP_PENDING"), ("STARTPENDING", "START_PENDING")]
#            thread_form.status.choices = choices
#
#        if request.method == 'POST':
#            status_new = thread_form.status.data
#            status_new = status_new.upper()
#            if status_new != status:
#                res, err = thread_impl.post_api(**dict(status=status_new))
#                flash("Result : %s" % res)
#                return redirect(url_for('thread_control.edit', thread_name=thread_obj['name']))
#
#        return render_template("thread_control/edit.html", thread_obj=thread_obj, form=thread_form)
#    except Exception as e:
#        print('Error %s when checking thread control of token %s' % e)
#        flash('Error %s' % str(e))
#        return url_for("thread_control.index")
#
#
#@thread_control.route('/delete/<thread_name>')
#def delete(thread_name):
#    try:
#        thread_impl = ThreadControlImpl("")
#        thread_lst = thread_impl.get_all()
#        if request.method == 'POST':
#            pass
#        return render_template("thread_control/delete.html", thread_lst=thread_lst)
#    except Exception as e:
#        print('Error %s when checking chart of token %s' % (e))
#        flash('Error %s' % str(e))
#        return render_template("thread_control/delete.html", json_data=list())
#