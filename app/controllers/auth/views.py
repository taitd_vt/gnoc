 #-*- coding:utf-8 -*-
from flask import render_template, redirect, url_for, request, flash, g
from flask import Flask, Response, redirect, url_for, request, session, abort
from . import auth
from flask_login import login_required, current_user
from .forms import LoginForm
from flask_login import login_user, current_user, logout_user
from core.database.impl.account_impl import User, UserRepository
from app import login_manager
from core.database.impl.account_impl import get_by_username


@login_manager.user_loader
def load_user(username):
    return get_by_username(username)


@auth.before_request
def get_current_user():
    if current_user.is_authenticated:
        g.user = current_user


@auth.route('/login', methods=['GET', 'POST'])
def login():
    info_mess = ''
    if request.method == 'POST':
        form = LoginForm(request.form)

        if form.validate():
            try:
                session.permanent = True
                username = form.username.data
                password = form.password.data
                session['username'] = username
                id_usr, user, passwd = User(0, username, password).find_by_username(username)
                if user and password == passwd:
                    # correct user
                    userCor = User(id_usr, username, password)
                    login_user(userCor)

                    return redirect(url_for('home.index'))
                else:
                    # return abort(401)
                    flash("Error login failed")
                    # info_mess = ' Login: Username and password is not correct'
                    redirect(url_for('auth.login'))

            except Exception as ex:
                flash('#ERROR LOGIN:' + str(ex))
                # info_mess = ' Login: Username and password is not correct'
                redirect(url_for('auth.login'))
                #return abort(401)
    else:
        form = LoginForm()
    return render_template('auth/login.html', form=form, info_mess=info_mess)


@auth.route('/hello')
def hello():
    print('Hello ')
    print(current_user)
    cur_usr = current_user
    print(cur_usr)
    return 'Hello ' + cur_usr.username


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return render_template('auth/logout.html')


@auth.errorhandler(401)
def page_not_found(e):
    return render_template('auth/login_fail.html')