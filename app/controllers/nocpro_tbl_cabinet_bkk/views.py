# -*- coding:utf-8 -*-
from flask import render_template, flash, request, redirect, url_for
from app.controllers.nocpro_tbl_cabinet_bkk import nocpro_cabinet_bkk
from flask_login import login_required, current_user
from flask import send_file
from flask_paginate import Pagination, get_page_args

import os, json
import csv

import uuid
from core.helpers.stringhelpers import check_legal_str
from core.database.impl.account_impl import User
from core.helpers.date_helpers import get_date_minus_format_ipms, get_date_now_format_ipms, \
    get_date_now_format_elastic, get_date_now, convert_date_str_to_date_obj_ipms
from core.database.impl.nocpro.tbl_cabinet_bkk import TblCabinetBkk
from .forms import CabinetBKKForm, SearchForm

app_root = os.path.dirname(os.path.abspath(__file__))


def get_users(tbl_lst, offset=0, per_page=50):
    return tbl_lst[offset: offset + per_page]


@nocpro_cabinet_bkk.route('/', methods=['GET'])
@login_required
def index():
    time_now = get_date_now()
    tbl_bkk = TblCabinetBkk('', '', '', 1, time_now)
    chek_form = SearchForm(request.form)
    tbl_lst = []
    try:
        user_type = current_user.find_type_by_id(current_user.id)
        if user_type == 'editor' or user_type == 'admin_web':
            tbl_lst = tbl_bkk.get_lst()
        else:
            flash("You don't have permission here!")
            redirect(url_for('nocpro_cabinet_bkk.index'))

        page, per_page, offset = get_page_args(page_parameter='page',
                                               per_page_parameter='per_page')
        total = len(tbl_lst)
        pagination_tbl = get_users(tbl_lst, offset=offset, per_page=10)
        pagination = Pagination(page=page, per_page=per_page, total=total, css_framework='bootstrap4')
        return render_template("nocpro_cabinet_bkk/index.html", err="", tbl_lst=pagination_tbl,
                               page=page,
                               per_page=per_page,
                               pagination=pagination,
                               form=chek_form,
                               key_name='')
    except Exception as err:
        time_now = get_date_now()
        tbl_bkk = TblCabinetBkk('', '', '', 1, time_now)
        tbl_lst = tbl_bkk.get_lst()
        flash(err)
        return render_template("nocpro_cabinet_bkk/index.html", tbl_lst=tbl_lst, form=chek_form,
                               key_name='')


@nocpro_cabinet_bkk.route('/search/', methods=['POST'])
@login_required
def index_srch():
    chek_form = SearchForm(request.form)

    try:
        user_type = current_user.find_type_by_id(current_user.id)
        if user_type == 'editor' or user_type == 'admin_web':
            if request.method == 'POST':
                key_name_new = chek_form.key_name.data
                if key_name_new:
                    return redirect(url_for('nocpro_cabinet_bkk.index_srch_pretty', key_name=key_name_new))
                else:
                    return redirect(url_for('nocpro_cabinet_bkk.index'))

        else:
            flash("You don't have permission here!")
            redirect(url_for('nocpro_cabinet_bkk.index'))

    except Exception as err:
        flash(err)
        return redirect(url_for('nocpro_cabinet_bkk.index'))


@nocpro_cabinet_bkk.route('/search/<key_name>')
@login_required
def index_srch_pretty(key_name):
    time_now = get_date_now()
    tbl_bkk = TblCabinetBkk('', '', '', 1, time_now)
    chek_form = SearchForm(request.form)
    tbl_lst = []
    try:
        user_type = current_user.find_type_by_id(current_user.id)
        if user_type == 'editor' or user_type == 'admin_web':
            chek_form.key_name.default = key_name
            chek_form.process()
            tbl_lst = tbl_bkk.find_like(key_name)
        else:
            flash("You don't have permission here!")
            redirect(url_for('nocpro_cabinet_bkk.index', key_name=key_name))

        page, per_page, offset = get_page_args(page_parameter='page',
                                               per_page_parameter='per_page')
        total = len(tbl_lst)
        pagination_tbl = get_users(tbl_lst, offset=offset, per_page=10)
        pagination = Pagination(page=page, per_page=per_page, total=total, css_framework='bootstrap4')
        return render_template("nocpro_cabinet_bkk/index.html", err="", tbl_lst=pagination_tbl,
                               page=page,
                               per_page=per_page,
                               pagination=pagination,
                               form=chek_form,
                               key_name=key_name)
    except Exception as err:
        time_now = get_date_now()
        tbl_bkk = TblCabinetBkk('', '', '', 1, time_now)
        tbl_lst = tbl_bkk.get_lst()
        return render_template("nocpro_cabinet_bkk/index.html", tbl_lst=tbl_lst, form=chek_form,
                               key_name=key_name)


@nocpro_cabinet_bkk.route('/add/', methods=['GET', 'POST'])
@login_required
def add():
    user_type = current_user.find_type_by_id(current_user.id)
    if user_type == 'editor' or user_type == 'admin_web':

        chek_form = CabinetBKKForm(request.form)
        info_mess = "Input cabinet BKK"
        time_now = get_date_now()
        tbl_bkk = TblCabinetBkk('', '', '', 1, time_now)
        try:
            if request.method == 'POST':
                user_login = current_user.username
                user_type = current_user.find_type_by_id(current_user.id)
                if user_type == 'editor' or user_type == 'admin_web':

                    cabinet_name_new = chek_form.cabinet_name.data
                    document_new = chek_form.document.data
                    expire_date_new = chek_form.expire_date.data
                    reason_new = chek_form.reason.data
                    status_new = 1

                    tbl_bkk.cbn_name = cabinet_name_new
                    tbl_bkk.status = status_new
                    tbl_bkk.document = document_new
                    tbl_bkk.expire_date = convert_date_str_to_date_obj_ipms(expire_date_new)
                    tbl_bkk.reason = reason_new

                    # check duplicate
                    res_chk_dupl = tbl_bkk.find(cabinet_name_new)
                    if res_chk_dupl:
                        info_mess = "This cabinet is exist in Database"
                        flash(info_mess)
                    else:
                        res = tbl_bkk.insert()
                        if res:
                            info_mess = "Add new Cabinet " + cabinet_name_new + " successful!"
                            flash(info_mess)
                        else:
                            info_mess = "Add new Cabinet " + cabinet_name_new + " failed!"
                            flash(info_mess)

                        return redirect(url_for('nocpro_cabinet_bkk.index'))

                else:
                    info_mess = "You don't have permission here!"
                    flash(info_mess)
                    return redirect(url_for('nocpro_cabinet_bkk.index'))
        except Exception as err:
            time_now = get_date_now()
            tbl_bkk = TblCabinetBkk('', '', '', 1, time_now)
            tbl_lst = tbl_bkk.get_lst()
            return render_template("nocpro_cabinet_bkk/index.html", tbl_lst=tbl_lst)

    else:
        err = "You don't have permission here"
        flash(err)
        return render_template('nocpro_cabinet_bkk/error.html')

    return render_template('nocpro_cabinet_bkk/add.html', form=chek_form)


@nocpro_cabinet_bkk.route('/edit/<name>', methods=['GET', 'POST'])
@login_required
def edit(name):
    time_now = get_date_now()

    user_type = current_user.find_type_by_id(current_user.id)
    if user_type == 'editor' or user_type == 'admin_web':
        try:
            tbl_bkk = TblCabinetBkk('', '', '', 0, time_now)
            cbn_obj = tbl_bkk.find(name)
            if cbn_obj:
                check_form = CabinetBKKForm(request.form, obj=cbn_obj)

                if request.method == 'POST':
                    # Chi user admin_web moi co the Accept chan web. User thuong thi chi edit duoc ten web thoi
                    cabinet_name_new = check_form.cabinet_name.data
                    document_new = check_form.document.data
                    expire_date_new = check_form.expire_date.data
                    reason_new = check_form.reason.data

                    if cabinet_name_new:
                        if cabinet_name_new != cbn_obj.cbn_name:
                            res_chk_dupl = tbl_bkk.find(cabinet_name_new)
                            if res_chk_dupl:
                                info_mess = "This cabinet is exist in Database"
                                flash(info_mess)
                                return render_template('nocpro_cabinet_bkk/edit.html', name=name,
                                                       tbl_obj=cbn_obj,
                                                       form=check_form)

                        res = cbn_obj.update(cabinet_name_new, reason_new, document_new, cbn_obj.status,
                                             expire_date_new)
                        if res:
                            flash("Edit Successful!")
                        else:
                            flash("Edit Fail!")
                            return render_template('nocpro_cabinet_bkk/edit.html', name=name,
                                                   tbl_obj=cbn_obj,
                                                   form=check_form)
                    else:
                        flash('CABINET NAME must be character')
                else:
                    return render_template('nocpro_cabinet_bkk/edit.html', name=name,
                                           tbl_obj=cbn_obj,
                                           form=check_form)

        except Exception as err:
            time_now = get_date_now()
            tbl_bkk = TblCabinetBkk('', '', '', 1, time_now)
            tbl_lst = tbl_bkk.get_lst()
            flash(err)
            return render_template("nocpro_cabinet_bkk/index.html", tbl_lst=tbl_lst)

    else:
        err = "You don't have permission here"
        flash(err)
        return render_template('nocpro_cabinet_bkk/error.html')
    # flash(info_mess)
    # return render_template("nocpro_cabinet_bkk/error.html", info_mess=info_mess)
    return redirect(url_for('nocpro_cabinet_bkk.index'))


@nocpro_cabinet_bkk.route('/delete/<name>', methods=['GET', 'POST'])
@login_required
def delete(name):
    time_now = get_date_now()
    tbl_bkk = TblCabinetBkk('', '', '', 0, time_now)
    user_type = current_user.find_type_by_id(current_user.id)
    if user_type == 'editor' or user_type == 'admin_web':
        try:
            tbl_obj = tbl_bkk.find(name)

            if tbl_obj:
                check_form = CabinetBKKForm(request.form, obj=tbl_obj)

                if request.method == 'POST':
                    user_login = current_user.username
                    user_type = current_user.find_type_by_id(current_user.id)
                    # Chi user admin_web moi co the Accept chan web. User thuong thi chi edit duoc ten web thoi
                    if user_type == 'editor' or user_type == 'admin_web':
                        # DELETE
                        res = tbl_obj.delete()
                        if res:
                            info_mess = "Delete Successful 1 cabinet bkk"
                            flash(info_mess)
                        else:
                            info_mess = "Delete Fail!!!"
                            flash(info_mess)
                        return redirect(url_for('nocpro_cabinet_bkk.index'))
                    else:
                        info_mess = 'You do not have permission here'
                        flash(info_mess)
                        return render_template('nocpro_cabinet_bkk/error.html')

                return render_template('nocpro_cabinet_bkk/delete.html', name=name,
                                       tbl_obj=tbl_obj, form=check_form)
            else:
                info_mess = "Cabinet BKK name not exist"
                flash(info_mess)
                return render_template('nocpro_cabinet_bkk/error.html')

        except Exception as err:
            flash(err)
            return render_template('nocpro_cabinet_bkk/error.html')
    else:
        err = "You don't have permission here"
        flash(err)
        return render_template('nocpro_cabinet_bkk/error.html')
