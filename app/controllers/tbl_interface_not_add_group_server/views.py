# -*- coding:utf-8 -*-
from flask import render_template, flash, request, redirect, url_for
from app.controllers.tbl_interface_not_add_group_server import intf_not_add_group
from flask_login import login_required, current_user
from flask import send_file
from flask_paginate import Pagination, get_page_args

import os, json
import csv

import uuid
from core.helpers.stringhelpers import check_legal_str
from core.database.impl.account_impl import User
from core.helpers.date_helpers import get_date_minus_format_ipms, get_date_now_format_ipms, \
    get_date_now_format_elastic, get_date_now, convert_date_str_to_date_obj_ipms
from core.database.impl.server_minhnd.tbl_interface_not_add_group_impl import TblInterfaceNotAddGroupImpl
from .forms import SearchForm

app_root = os.path.dirname(os.path.abspath(__file__))


def get_users(tbl_lst, offset=0, per_page=50):
    return tbl_lst


@intf_not_add_group.route('/', methods=['GET'])
@login_required
def index():
    time_now = get_date_now()
    tbl_impl = TblInterfaceNotAddGroupImpl('', '', '', '', 0, '', time_now, 0, 'active')
    chek_form = SearchForm(request.form)
    tbl_lst = []
    try:
        page, per_page, offset = get_page_args(page_parameter='page',
                                               per_page_parameter='per_page')
        per_page = 50

        user_type = current_user.find_type_by_id(current_user.id)
        if user_type == 'editor' or user_type == 'admin_web':
            tbl_lst = tbl_impl.list_page(page, per_page)
        else:
            flash("You don't have permission here!")
            redirect(url_for('intf_not_add_group.index'))


        total = tbl_impl.total()
        if total:
            total = int(total)

        pagination_tbl = get_users(tbl_lst, offset=offset, per_page=50)
        pagination = Pagination(page=page, per_page=per_page, total=total, css_framework='bootstrap4')

        return render_template("server_interface_not_add_group/index.html", err="", tbl_lst=pagination_tbl,
                               page=page,
                               per_page=per_page,
                               pagination=pagination,
                               form=chek_form,
                               key_name='',
                               total=total)
    except Exception as err:
        tbl_lst = tbl_impl.list_page(1, 50)
        total = int(tbl_impl.total())
        flash(err)
        return render_template("server_interface_not_add_group/index.html", tbl_lst=tbl_lst, form=chek_form,
                               key_name='', total=total)


@intf_not_add_group.route('/search/', methods=['POST'])
@login_required
def index_srch():
    chek_form = SearchForm(request.form)

    try:
        user_type = current_user.find_type_by_id(current_user.id)
        if user_type == 'editor' or user_type == 'admin_web':
            if request.method == 'POST':
                key_name_new = chek_form.key_name.data
                if key_name_new:
                    return redirect(url_for('intf_not_add_group.index_srch_pretty', key_name=key_name_new))
                else:
                    return redirect(url_for('intf_not_add_group.index'))

        else:
            flash("You don't have permission here!")
            redirect(url_for('intf_not_add_group.index'))

    except Exception as err:
        flash(err)
        return redirect(url_for('intf_not_add_group.index'))


@intf_not_add_group.route('/search/<key_name>')
@login_required
def index_srch_pretty(key_name):
    time_now = get_date_now()
    tbl_impl = TblInterfaceNotAddGroupImpl('', '', '', '', 0, '', time_now, 0, 'active')
    chek_form = SearchForm(request.form)
    tbl_lst = []
    try:
        page, per_page, offset = get_page_args(page_parameter='page',
                                               per_page_parameter='per_page')
        per_page = 50

        user_type = current_user.find_type_by_id(current_user.id)
        if user_type == 'editor' or user_type == 'admin_web':
            chek_form.key_name.default = key_name
            chek_form.process()
            srch_dct = dict(search=key_name, page=page, pageSize=50)
            tbl_lst = tbl_impl.find_like(**srch_dct)

        else:
            flash("You don't have permission here!")
            redirect(url_for('intf_not_add_group.index', key_name=key_name))

        total = tbl_impl.total_key_name(key_name)
        if total:
            total = int(total)
        pagination_tbl = get_users(tbl_lst, offset=offset, per_page=10)
        pagination = Pagination(page=page, per_page=per_page, total=total, css_framework='bootstrap4')
        return render_template("server_interface_not_add_group/index.html", err="", tbl_lst=pagination_tbl,
                               page=page,
                               per_page=per_page,
                               pagination=pagination,
                               form=chek_form,
                               key_name=key_name,
                               total=total)
    except Exception as err:
        flash(err)
        tbl_lst = tbl_impl.list_page(1, 50)

        return render_template("server_interface_not_add_group/index.html", tbl_lst=tbl_lst, form=chek_form,
                               key_name=key_name, total=0)


