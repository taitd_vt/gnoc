from wtforms import StringField, DateField, BooleanField, Form, IntegerField, SelectField, validators


class CabinetBKKForm(Form):
    cabinet_name = StringField(u'Cabinet Name')
    document = StringField(u'Document')
    reason = StringField(u'Reason')
    expire_date = StringField(u'Expire Date')


class SearchForm(Form):
    key_name = StringField(u'Search',)


