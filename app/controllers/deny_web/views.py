# -*- coding:utf-8 -*-
from flask import render_template, flash, request, redirect, url_for
from app.controllers.deny_web import deny_web
from flask_login import login_required
from flask import send_file
import os, json
import csv
from app.controllers.deny_web.forms import DenyWebForm
import uuid
from core.helpers.stringhelpers import check_legal_str
from flask_login import current_user
from core.database.impl.account_impl import User
from core.helpers.date_helpers import get_date_minus_format_ipms, get_date_now_format_ipms, \
    get_date_now_format_elastic, get_date_now
from core.database.impl.tbl_deny_web_impl import TblDenyWebImpl

app_root = os.path.dirname(os.path.abspath(__file__))


@deny_web.route('/', methods=['GET'])
@login_required
def index():

    user_type = current_user.find_type_by_id(current_user.id)
    if user_type == 'editor' or user_type == 'admin_web':
        try:
            tbl_deny_web = TblDenyWebImpl()
            web_lst = tbl_deny_web.list()
        except Exception as err:
            return render_template('deny_web/error.html', info_mess=err)
    else:
        err = 'You donot have permission here'
        return render_template('deny_web/error.html', info_mess=err)
    return render_template("deny_web/index.html", err="", web_lst=web_lst)


@deny_web.route('/downloads/<filename>')
@login_required
def download(filename):
    target = os.path.join(app_root, 'static/file/')
    if not os.path.isdir(target):
        os.makedirs(target)
    path = os.path.join(target, filename)

    return send_file(path, as_attachment=True)


@deny_web.route('/add_auto', methods=['GET', 'POST'])
@login_required
def add_auto():
    row = 0
    user_type = current_user.find_type_by_id(current_user.id)
    if user_type == 'editor' or user_type == 'admin_web':

        try:
            # with open('', 'r') as outfile:
            #    reader = csv.DictReader(outfile)
            #    helpers.bulk(es, reader, index="tbl_deny_web", doc_type="_doc")
            target = os.path.join(app_root, 'static/file/')
            if not os.path.isdir(target):
                os.makedirs(target)
            tbl_deny_web = TblDenyWebImpl()
            if request.method == 'POST':

                f = request.files['fileInput']
                file_name = f.filename or ''
                fstring_byte = f.read()
                fstring = ''

                if isinstance(fstring_byte, bytes):
                    fstring = fstring_byte.decode("ISO-8859-1")
                elif isinstance(fstring_byte, str):
                    fstring = fstring_byte

                csv_lst = [{k: v for k, v in row.items()} for row in
                           csv.DictReader(fstring.splitlines(), skipinitialspace=True)]
                if csv_lst:
                    # bat dau insert
                    # thay doi kieu. Insert vao MYSQL khong vao ES nua
                    user_login = current_user.username
                    user_type = current_user.find_type_by_id(current_user.id)
                    stat_new = 0
                    timestamp = get_date_now_format_elastic()

                    for x in csv_lst:
                        if 'URL' in x:
                            web_name = x['URL']
                            document = x['document']
                            print(web_name)
                            check_web = check_legal_str(web_name)
                            if user_type == 'editor' or user_type == 'admin_web':
                                if check_web:
                                    post_dct = dict(web=web_name, usr='', cv=document, usr_order=user_login, timestamp=timestamp,
                                                    status=stat_new)
                                    res_ins = tbl_deny_web.post(**post_dct)
                                    if res_ins:
                                        row += 1
                                    print(res_ins)
                        else:
                            print("No URL in x:" + str(x))

        except Exception as error:
            flash(error, 'info')
            return render_template('deny_web/error.html', info_mess=error)

        mes_str = "Input successfull " + str(row) + " row."
        flash(mes_str, 'info')
    else:
        err = "You don't have permission here"
        return render_template('deny_web/error.html', info_mess=err)
    return render_template('deny_web/add_auto.html', err=mes_str)


@deny_web.route('/add_manual/', methods=['GET', 'POST'])
@login_required
def add_manual():
    user_type = current_user.find_type_by_id(current_user.id)
    if user_type == 'editor' or user_type == 'admin_web':

        chek_form = DenyWebForm(request.form)
        info_mess = "Input Web to be denied"
        tbl_deny_web = TblDenyWebImpl()
        try:
            if request.method == 'POST':
                user_login = current_user.username
                user_type = current_user.find_type_by_id(current_user.id)
                stat_new = 0
                timestamp = get_date_now_format_elastic()
                web = chek_form.web.data
                document = chek_form.document.data
                check_web = check_legal_str(web)
                if user_type == 'editor' or user_type == 'admin_web':
                    if check_web:
                        post_dct = dict(web=web, usr='', cv=document, usr_order=user_login, timestamp=timestamp, status=stat_new)
                        res = tbl_deny_web.post(**post_dct)
                        if res:
                            info_mess = "Input Successful 1 web"
                        else:
                            info_mess = "Input Fail!!!"
                    else:
                        info_mess = "Web link not correct"
                else:
                    info_mess = "You don't have permission here!"
        except Exception as err:
            return render_template('deny_web/error.html', info_mess=err)
    else:
        err = "You don't have permission here"
        return render_template('deny_web/error.html', info_mess=err)

    return render_template('deny_web/add.html', info_mess=info_mess, form=chek_form)


@deny_web.route('/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit(id):
    info_mess = "Error!!!"
    user_type = current_user.find_type_by_id(current_user.id)
    if user_type == 'editor' or user_type == 'admin_web':

        try:
            tbl_deny_web = TblDenyWebImpl()
            tbl_obj = tbl_deny_web.get(id)
            if tbl_obj:
                check_form = DenyWebForm(request.form, obj=tbl_obj[0])
                stat_old = tbl_obj[0].get('status')
                post_dct = dict()
                if request.method == 'POST':
                    user_login = current_user.username
                    user_type = current_user.find_type_by_id(current_user.id)
                    # Chi user admin_web moi co the Accept chan web. User thuong thi chi edit duoc ten web thoi
                    web = check_form.web.data
                    document = check_form.document.data
                    stat_new = int(check_form.status.data)
                    timestamp = get_date_now_format_elastic()
                    chck_pri = False
                    if user_type == 'editor':
                        if stat_old != stat_new:
                            info_mess = "You don't have permission to change the status"
                        else:
                            post_dct = dict(Id=id, web=web, cv=document, usr_order=user_login, timestamp=timestamp,
                                            status=stat_old)

                            if 'usr_acc' in tbl_obj[0]:
                                user_acc = tbl_obj[0].get('usr_acc')
                                post_dct['usr_acc'] = user_acc
                            if 'usr_order' in tbl_obj[0]:
                                user_ord = tbl_obj[0].get('usr_order')
                                post_dct['usr_order'] = user_ord

                            if 'time_acc' in tbl_obj[0]:
                                time_acc = tbl_obj[0].get('time_acc')
                                post_dct['time_acc'] = time_acc
                            if 'usr' in tbl_obj[0]:
                                usr = tbl_obj[0].get('usr')
                                post_dct['usr'] = usr

                            chck_pri = True
                    elif user_type == 'admin_web':
                        if stat_new != stat_old:
                            post_dct = dict(Id=id, web=web, cv=document, usr_acc=user_login, time_acc=timestamp, status=stat_new)
                        else:
                            post_dct = dict(Id=id, web=web, cv=document, status=stat_new, time_acc=timestamp)
                        if 'usr_acc' in tbl_obj[0]:
                            user_acc = tbl_obj[0].get('usr_acc')
                            post_dct['usr_acc'] = user_acc

                        if 'usr' in tbl_obj[0]:
                            usr = tbl_obj[0].get('usr')
                            post_dct['usr'] = usr
                        if 'usr_order' in tbl_obj[0]:
                            user_ord = tbl_obj[0].get('usr_order')
                            post_dct['usr_order'] = user_ord
                        if 'timestamp' in tbl_obj[0]:
                            time_ord = tbl_obj[0].get('timestamp')
                            post_dct['timestamp'] = time_ord

                        chck_pri = True
                    else:
                        info_mess = "You don't have privellege to change the status"

                    if post_dct and chck_pri:
                        res = tbl_deny_web.put(id, **post_dct)
                        if res:
                            info_mess = "Edit Successful 1 web"
                        else:
                            info_mess = "Edit Fail!!!"
                    return render_template('deny_web/error.html', info_mess=info_mess)
                else:

                    check_form.status.default = tbl_obj[0].get('status')
                    check_form.document.data = tbl_obj[0].get('cv')
                    check_form.process()

                return render_template('deny_web/edit.html', info_mess=info_mess, id_web=id, tbl_obj=tbl_obj[0],
                                   form=check_form)

        except Exception as err:
            return render_template('deny_web/error.html', info_mess=err)
    else:
        err = "You don't have permission here"
        return render_template('deny_web/error.html', info_mess=err)
    return render_template('deny_web/error.html', info_mess=info_mess)


@deny_web.route('/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete(id):
    user_type = current_user.find_type_by_id(current_user.id)
    if user_type == 'editor' or user_type == 'admin_web':
        tbl_deny_web = TblDenyWebImpl()
        try:
            tbl_obj = tbl_deny_web.get(id)
            info_mess = "Input Web to be deleted"
            if tbl_obj:
                check_form = DenyWebForm(request.form, obj=tbl_obj[0])

                if request.method == 'POST':
                    user_login = current_user.username
                    user_type = current_user.find_type_by_id(current_user.id)
                    # Chi user admin_web moi co the Accept chan web. User thuong thi chi edit duoc ten web thoi
                    if user_type == 'editor' or user_type == 'admin_web':
                        # DELETE
                        res = tbl_deny_web.delete(id)
                        if res:
                            info_mess = "Delete Successful 1 web"
                        else:
                            info_mess = "Delete Fail!!!"

                    return render_template('deny_web/error.html', info_mess=info_mess)
                return render_template('deny_web/delete.html', info_mess=info_mess, id_web=id, tbl_obj=tbl_obj[0],
                                       form=check_form)
            else:
                info_mess = "ID web not exist"
                return render_template('deny_web/error.html', info_mess=info_mess)
        except Exception as err:
            return render_template('deny_web/error.html', info_mess=err)
    else:
        err = "You don't have permission here"
        return render_template('deny_web/error.html', info_mess=err)