from wtforms import StringField, DateField, BooleanField, Form, IntegerField, SelectField, validators


class DenyWebForm(Form):
    web = StringField(u'web')
    document = StringField(u'document')
    status = SelectField(u'status', choices=[(0, 'DENY'), (1, 'ACCEPT'), (2, 'WAITING')])





