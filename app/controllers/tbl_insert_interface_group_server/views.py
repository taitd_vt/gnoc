# -*- coding:utf-8 -*-
from flask import render_template, flash, request, redirect, url_for
from app.controllers.tbl_insert_interface_group_server import insert_intf_group
from flask_login import login_required, current_user
from flask import send_file
from flask_paginate import Pagination, get_page_args
import os, json
import csv
import uuid
from core.helpers.stringhelpers import check_legal_str
from core.database.impl.account_impl import User
from core.helpers.date_helpers import get_date_minus_format_ipms, get_date_now_format_ipms, \
    get_date_now_format_elastic, get_date_now, convert_date_str_to_date_obj_ipms
from core.database.impl.server_minhnd.tbl_insert_interface_group_impl import TblInsertInterfaceGroupImpl
from .forms import TblInsertInterfaceGroupForm, SearchForm

app_root = os.path.dirname(os.path.abspath(__file__))


def get_users(tbl_lst, offset=0, per_page=50):
    return tbl_lst[offset: offset + per_page]


@insert_intf_group.route('/', methods=['GET'])
@login_required
def index():

    tbl_obj = TblInsertInterfaceGroupImpl(group_name='',
                                          rule_description='',
                                          area='',
                                          interface_not_contain='',
                                          host_contain='',
                                          interface_contain='',
                                          group_host_name='',
                                          interface_description='',
                                          interface_des_not_contain='',
                                          speed_greater_mbps=0,
                                          speed_less_mbps=0)
    chek_form = SearchForm(request.form)
    tbl_lst = []
    try:
        page, per_page, offset = get_page_args(page_parameter='page',
                                               per_page_parameter='per_page')

        per_page = 50

        user_type = current_user.find_type_by_id(current_user.id)
        if user_type == 'editor' or user_type == 'admin_web':
            tbl_lst = tbl_obj.list_page(page, per_page)
        else:
            flash("You don't have permission here!")
            redirect(url_for('insert_intf_group.index'))

        total = tbl_obj.total()
        if total:
            total = int(total)
        pagination_tbl = get_users(tbl_lst, offset=offset, per_page=per_page)
        pagination = Pagination(page=page, per_page=per_page, total=total, css_framework='bootstrap4')
        return render_template("server_insert_interface_group/index.html", err="", tbl_lst=pagination_tbl,
                               page=page,
                               per_page=per_page,
                               pagination=pagination,
                               form=chek_form,
                               key_name='')
    except Exception as err:
        tbl_lst = tbl_obj.list_page(1, 50)
        flash(err)
        return render_template("server_insert_interface_group/index.html", tbl_lst=tbl_lst, form=chek_form,
                               key_name='', total=0)


@insert_intf_group.route('/search/', methods=['POST'])
@login_required
def index_srch():
    chek_form = SearchForm(request.form)

    try:
        user_type = current_user.find_type_by_id(current_user.id)
        if user_type == 'editor' or user_type == 'admin_web':
            if request.method == 'POST':
                key_name_new = chek_form.key_name.data
                if key_name_new:
                    return redirect(url_for('insert_intf_group.index_srch_pretty', key_name=key_name_new))
                else:
                    return redirect(url_for('insert_intf_group.index'))

        else:
            flash("You don't have permission here!")
            redirect(url_for('insert_intf_group.index'))

    except Exception as err:
        flash(err)
        return redirect(url_for('insert_intf_group.index'))


@insert_intf_group.route('/search/<key_name>')
@login_required
def index_srch_pretty(key_name):
    tbl_obj = TblInsertInterfaceGroupImpl(group_name='',
                                          rule_description='',
                                          area='',
                                          interface_not_contain='',
                                          host_contain='',
                                          interface_contain='',
                                          group_host_name='',
                                          interface_description='',
                                          interface_des_not_contain='',
                                          speed_greater_mbps=0,
                                          speed_less_mbps=0)
    chek_form = SearchForm(request.form)
    tbl_lst = []
    try:
        page, per_page, offset = get_page_args(page_parameter='page',
                                               per_page_parameter='per_page')

        user_type = current_user.find_type_by_id(current_user.id)
        if user_type == 'editor' or user_type == 'admin_web':
            chek_form.key_name.default = key_name
            chek_form.process()
            srch_dct = dict(search=key_name, page=page, pageSize=50)
            tbl_lst = tbl_obj.find_like(**srch_dct)
        else:
            flash("You don't have permission here!")
            redirect(url_for('insert_intf_group.index', key_name=key_name))

        total = tbl_obj.total_key_name(key_name)
        if total:
            total = int(total)
        pagination_tbl = get_users(tbl_lst, offset=offset, per_page=per_page)
        pagination = Pagination(page=page, per_page=per_page, total=total, css_framework='bootstrap4')
        return render_template("server_insert_interface_group/index.html", err="", tbl_lst=pagination_tbl,
                               page=page,
                               per_page=per_page,
                               pagination=pagination,
                               form=chek_form,
                               key_name=key_name,
                               total=total)
    except Exception as err:
        tbl_lst = tbl_obj.list()
        flash(err)
        return render_template("server_insert_interface_group/index.html", tbl_lst=tbl_lst, form=chek_form,
                               key_name=key_name)


@insert_intf_group.route('/add/', methods=['GET', 'POST'])
@login_required
def add():
    user_type = current_user.find_type_by_id(current_user.id)
    if user_type == 'editor' or user_type == 'admin_web':

        chek_form = TblInsertInterfaceGroupForm(request.form)

        tbl_obj = TblInsertInterfaceGroupImpl(group_name='',
                                              rule_description='',
                                              area='',
                                              interface_not_contain='',
                                              host_contain='',
                                              interface_contain='',
                                              group_host_name='',
                                              interface_description='',
                                              interface_des_not_contain='',
                                              speed_greater_mbps=0,
                                              speed_less_mbps=0)
        try:
            if request.method == 'POST':

                user_type = current_user.find_type_by_id(current_user.id)
                if user_type == 'editor' or user_type == 'admin_web':

                    group_name_new = chek_form.group_name.data
                    rule_description_new = chek_form.rule_description.data
                    area_new = chek_form.area.data
                    interface_not_contain_new = chek_form.interface_not_contain.data
                    host_contain_new = chek_form.host_contain.data
                    interface_contain_new = chek_form.interface_contain.data
                    group_host_name_new = chek_form.group_host_name.data
                    interface_description_new = chek_form.interface_description.data
                    interface_des_not_contain_new = chek_form.interface_des_not_contain.data
                    speed_greater_mbps_new = chek_form.speed_greater_mbps.data
                    speed_less_mbps_new = chek_form.speed_less_mbps.data

                    tbl_obj.group_name = group_name_new
                    tbl_obj.rule_description = rule_description_new
                    tbl_obj.area = area_new
                    tbl_obj.host_contain = host_contain_new
                    tbl_obj.interface_contain = interface_contain_new
                    tbl_obj.interface_not_contain = interface_not_contain_new
                    tbl_obj.group_host_name = group_host_name_new
                    tbl_obj.interface_description = interface_description_new
                    tbl_obj.interface_des_not_contain = interface_des_not_contain_new
                    tbl_obj.speed_greater_mbps = speed_greater_mbps_new
                    tbl_obj.speed_less_mbps = speed_less_mbps_new

                    # check duplicate
                    res_chk_dupl = tbl_obj.check_exist()
                    if res_chk_dupl:
                        info_mess = "This Rule is exist in Database"
                        flash(info_mess)
                    else:
                        res = tbl_obj.add()
                        if res:
                            info_mess = "Add new Rule " + rule_description_new + " successful!"
                            flash(info_mess)
                        else:
                            info_mess = "Add new Rule " + rule_description_new + " failed!"
                            flash(info_mess)

                        return redirect(url_for('insert_intf_group.index'))

                else:
                    info_mess = "You don't have permission here!"
                    flash(info_mess)
                    return redirect(url_for('insert_intf_group.index'))
        except Exception as err:
            flash(err)
            tbl_lst = tbl_obj.list()
            return render_template("server_insert_interface_group/index.html", tbl_lst=tbl_lst)

    else:
        err = "You don't have permission here"
        flash(err)
        return render_template('server_insert_interface_group/error.html')

    return render_template('server_insert_interface_group/add.html', form=chek_form, tbl_obj=tbl_obj)


@insert_intf_group.route('/edit/<id>', methods=['GET', 'POST'])
@login_required
def edit(id):

    user_type = current_user.find_type_by_id(current_user.id)
    tbl_obj = TblInsertInterfaceGroupImpl(group_name='',
                                          rule_description='',
                                          area='',
                                          interface_not_contain='',
                                          host_contain='',
                                          interface_contain='',
                                          group_host_name='',
                                          interface_description='',
                                          interface_des_not_contain='',
                                          speed_greater_mbps=0,
                                          speed_less_mbps=0)
    if user_type == 'editor' or user_type == 'admin_web':
        try:

            cbn_obj = tbl_obj.get(id)
            if cbn_obj:

                chek_form = TblInsertInterfaceGroupForm(request.form, obj=cbn_obj)

                if request.method == 'POST':
                    # Chi user admin_web moi co the Accept chan web. User thuong thi chi edit duoc ten web thoi
                    group_name_new = chek_form.group_name.data
                    rule_description_new = chek_form.rule_description.data
                    area_new = chek_form.area.data
                    interface_not_contain_new = chek_form.interface_not_contain.data
                    host_contain_new = chek_form.host_contain.data
                    interface_contain_new = chek_form.interface_contain.data
                    group_host_name_new = chek_form.group_host_name.data
                    interface_description_new = chek_form.interface_description.data
                    interface_des_not_contain_new = chek_form.interface_des_not_contain.data
                    speed_greater_mbps_new = chek_form.speed_greater_mbps.data
                    speed_less_mbps_new = chek_form.speed_less_mbps.data

                    cbn_obj.group_name = group_name_new
                    cbn_obj.rule_description = rule_description_new
                    cbn_obj.area = area_new
                    cbn_obj.host_contain = host_contain_new
                    cbn_obj.interface_contain = interface_contain_new
                    cbn_obj.interface_not_contain = interface_not_contain_new
                    cbn_obj.group_host_name = group_host_name_new
                    cbn_obj.interface_description = interface_description_new
                    cbn_obj.interface_des_not_contain = interface_des_not_contain_new
                    cbn_obj.speed_greater_mbps = speed_greater_mbps_new
                    cbn_obj.speed_less_mbps = speed_less_mbps_new
                    cbn_obj.id_insert = id

                    if group_name_new:

                        res = cbn_obj.edit()
                        if res:
                            flash("Edit Successful!")
                        else:
                            flash("Edit Fail!")
                            return render_template('server_insert_interface_group/edit.html', id=id,
                                                   tbl_obj=cbn_obj,
                                                   form=chek_form)
                    else:
                        flash('CABINET NAME must be character')
                else:
                    return render_template('server_insert_interface_group/edit.html', id=id,
                                           tbl_obj=cbn_obj,
                                           form=chek_form)

        except Exception as err:

            flash(err)
            return redirect(url_for('insert_intf_group.index'))

    else:
        err = "You don't have permission here"
        flash(err)
        return render_template('server_insert_interface_group/error.html')
    # flash(info_mess)
    # return render_template("server_insert_interface_group/error.html", info_mess=info_mess)
    return redirect(url_for('insert_intf_group.index'))


@insert_intf_group.route('/delete/<id>', methods=['GET', 'POST'])
@login_required
def delete(id):
    tbl_obj = TblInsertInterfaceGroupImpl(group_name='',
                                          rule_description='',
                                          area='',
                                          interface_not_contain='',
                                          host_contain='',
                                          interface_contain='',
                                          group_host_name='',
                                          interface_description='',
                                          interface_des_not_contain='',
                                          speed_greater_mbps=0,
                                          speed_less_mbps=0)
    user_type = current_user.find_type_by_id(current_user.id)
    if user_type == 'editor' or user_type == 'admin_web':
        try:
            tbl_del_obj = tbl_obj.get(id)

            if tbl_del_obj:

                check_form = TblInsertInterfaceGroupForm(request.form, obj=tbl_del_obj)

                if request.method == 'POST':
                    user_type = current_user.find_type_by_id(current_user.id)
                    # Chi user admin_web moi co the Accept chan web. User thuong thi chi edit duoc ten web thoi
                    if user_type == 'editor' or user_type == 'admin_web':
                        # DELETE
                        res = tbl_del_obj.delete()
                        if res:
                            info_mess = "Delete Successful 1 cabinet bkk"
                            flash(info_mess)
                        else:
                            info_mess = "Delete Fail!!!"
                            flash(info_mess)
                        return redirect(url_for('insert_intf_group.index'))
                    else:
                        info_mess = 'You do not have permission here'
                        flash(info_mess)
                        return render_template('server_insert_interface_group/error.html')

                return render_template('server_insert_interface_group/delete.html', id=id,
                                       tbl_obj=tbl_del_obj, form=check_form)
            else:
                info_mess = "Cabinet BKK name not exist"
                flash(info_mess)
                return render_template('server_insert_interface_group/error.html')

        except Exception as err:
            flash(err)
            return render_template('server_insert_interface_group/error.html')
    else:
        err = "You don't have permission here"
        flash(err)
        return render_template('server_insert_interface_group/error.html')
