from wtforms import StringField, DateField, BooleanField, Form, IntegerField, SelectField, validators


class TblInsertInterfaceGroupForm(Form):
    group_name = StringField(u'Group Name')
    rule_description = StringField(u'Rule Description')
    area = StringField(u'area')
    interface_not_contain = StringField(u'Interface Not Contain')
    host_contain = StringField(u'Host Contain')
    interface_contain = StringField(u'Interface Contain')
    group_host_name = StringField(u'Group Host Name')
    interface_description = StringField(u'Interface Description')
    interface_des_not_contain = StringField(u'Interface Description Not Contain')
    speed_greater_mbps = IntegerField(u'Speed Greater Mbps')
    speed_less_mbps = IntegerField(u'Speed Less Mbps')


class SearchForm(Form):
    key_name = StringField(u'Search',)


