from flask import render_template, flash, request, redirect, url_for
from . import chart
from datetime import timezone
from datetime import datetime


@chart.route('/<token_name>')
def index(token_name):
    try:
        token_name = token_name.upper()



        return render_template("trade_history/line.html", )
    except Exception as e:
        print('Error %s when checking chart of token %s' % (e, token_name))
        flash('Error %s' % str(e))
        return render_template("trade_history/line.html", json_data=list())



def convert_to_thousand(number):
    i = 0
    try:
        if number > 99999:
            i = 1
            number = number / 10
            while number > 99999:
                i += 1
                number = number / 10
                if i > 100000:
                    return i
        else:
            return i

    except Exception as e:
        print('Error %s when checking convert to thousand' % e)
        return number

    return i


def get_num_convert(num_wallet, num_price):
    num_conv = 1
    if num_price > 0:
        if num_wallet > num_price:
            while num_wallet > num_price:
                num_conv *= 10
                num_price *= 10
            if num_conv > 2:
                tmp_wallet_minus_price_up = abs(num_wallet - num_price)
                tmp_wallet_minus_price_down = abs(num_wallet - num_price * 0.1)
                if tmp_wallet_minus_price_up < tmp_wallet_minus_price_down:
                    return num_conv
                else:
                    return num_conv * 0.1
            return num_conv
        else:
            while num_wallet < num_price:
                num_conv *= 0.1
                num_price /= 10
            if num_conv > 2:
                tmp_wallet_minus_price_up = abs(num_price - num_wallet)
                tmp_wallet_minus_price_down = abs(num_price * 10 - num_wallet)
                if tmp_wallet_minus_price_up > tmp_wallet_minus_price_down:
                    return num_conv
                else:
                    return num_conv * 10

            return num_conv

    return num_conv