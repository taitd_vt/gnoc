from flask_login import login_required
from flask import render_template, flash, request, redirect, url_for
from . import check
import csv
import os
from collections import Counter
from core.helpers.stringhelpers import check_regex_acc
from core.database.impl.tbl_online_aaa_impl import TblOnlineAaaImpl
from core.database.impl.tbl_ring_info_impl import TblRingInfoImpl
from core.database.impl.tbl_bras_hsi_info_impl import TblBrasHsiInfoImpl
from app.controllers.check.forms import CheckForm
from core.helpers.int_helpers import convert_string_to_int
from flask import send_file
from flask_login import current_user

app_root = os.path.dirname(os.path.abspath(__file__))


@check.route('/fttx', methods=['GET', 'POST'])
@login_required
def index():
    user_type = current_user.find_type_by_id(current_user.id)
    if user_type != 'cc':
        chek_form = CheckForm(request.form)

        try:
            target = os.path.join(app_root, 'static/file/')
            if not os.path.isdir(target):
                os.makedirs(target)
            # Them phan so top

            if request.method == 'POST':
                num_find_str = chek_form.num_find.data
                num_find_int = convert_string_to_int(num_find_str)
                f = request.files['fileInput']
                file_name = f.filename or ''
                fstring_byte = f.read()
                fstring = ''

                if isinstance(fstring_byte, bytes):
                    fstring = fstring_byte.decode("utf-8")
                elif isinstance(fstring_byte, str):
                    fstring = fstring_byte

                csv_lst = [{k: v for k, v in row.items()} for row in
                           csv.DictReader(fstring.splitlines(), skipinitialspace=True)]
                # divide list username into area
                # print(csv_dict)

                user_kv1_lst = list()
                user_kv2_lst = list()
                user_kv3_lst = list()

                res_kv_lst = list()
                num_acc_inp_fail = 0
                num_acc_stop = 0
                num_acc_lgn_fail = 0

                for x in csv_lst:
                    if 'user' in x:
                        user_nme = x['user'].strip()
                        chk_acc = check_regex_acc(user_nme)
                        if chk_acc:
                            if 'area' in x:
                                area_acc = x['area']
                                if area_acc.upper() == 'KV1':
                                    user_kv1_lst.append(user_nme)
                                elif area_acc.upper() == 'KV2':
                                    user_kv2_lst.append(user_nme)
                                elif area_acc.upper() == 'KV3':
                                    user_kv3_lst.append(user_nme)
                                else:
                                    num_acc_inp_fail += 1
                            else:
                                num_acc_inp_fail += 1
                        else:
                            num_acc_inp_fail += 1

                mysql_aaa_kv1 = TblOnlineAaaImpl('kv1')
                mysql_aaa_kv2 = TblOnlineAaaImpl('kv2')
                mysql_aaa_kv3 = TblOnlineAaaImpl('kv3')
                if user_kv1_lst:
                    res_kv_lst = mysql_aaa_kv1.find_acc(user_kv1_lst)
                    for x in res_kv_lst:
                        x['area'] = 'kv1'

                if user_kv2_lst:
                    res_kv2_lst = mysql_aaa_kv2.find_acc(user_kv2_lst)
                    for x in res_kv2_lst:
                        x['area'] = 'kv2'

                    res_kv_lst = res_kv_lst + res_kv2_lst

                if user_kv3_lst:
                    res_kv3_lst = mysql_aaa_kv3.find_acc(user_kv3_lst)
                    for x in res_kv3_lst:
                        x['area'] = 'kv3'

                    res_kv_lst = res_kv_lst + res_kv3_lst

                _tbl_ring = TblRingInfoImpl('')
                _tbl_bras_hsi_inf = TblBrasHsiInfoImpl('')
                _nas_port_lst = list()
                _ring_srt_lst = list()
                _ring_agg_lst = list()
                _bras_lst = list()
                _bras_port_lst = list()
                _bras_port_vlan_lst = list()

                for x in res_kv_lst:
                    # bat dau duyet het khu vuc
                    check_ring_srt = True
                    if 'status' in x:
                        stat_x = x['status']
                        if stat_x != 'update':
                            num_acc_stop += 1

                    if 'serviceclass' in x:
                        ser_cls = x['serviceclass']
                        if ser_cls != 'normal':
                            num_acc_lgn_fail += 1

                    if 'nasportid' in x:
                        nas_port_id = x['nasportid']
                        pos_spc = nas_port_id.find(' ')
                        if pos_spc < 0:
                            pos_spc = nas_port_id.find('_')
                            if pos_spc < 0:
                                pos_spc = nas_port_id.find('/')

                        if pos_spc > 0:
                            x['nas_port'] = nas_port_id[:pos_spc]
                            _nas_port_lst.append(x['nas_port'])
                            # ket noi vao API de check
                            src_dct = dict(searchRingSrt=x['nas_port'])
                            ring_srt_lst = _tbl_ring.search_single(**src_dct)
                            if len(ring_srt_lst) <= 0:
                                src_dct = dict(searchRingSrt=x['nas_port'][:-2])
                                ring_srt_lst = _tbl_ring.search_single(**src_dct)
                                if len(ring_srt_lst) <= 0:
                                    src_dct = dict(searchRingSrt=x['nas_port'][:7])
                                    ring_srt_lst = _tbl_ring.search_single(**src_dct)

                            if ring_srt_lst:
                                if 'ring_srt' in ring_srt_lst[0]:
                                    x['ring_srt'] = ring_srt_lst[0]['ring_srt']
                                    if 'ring_agg' in ring_srt_lst[0]:
                                        x['ring_agg'] = ring_srt_lst[0]['ring_agg']
                                        _ring_srt_lst.append(x['ring_srt'])
                                        _ring_agg_lst.append(x['ring_agg'])
                                    else:
                                        check_ring_srt = False
                                        x['ring_agg'] = None
                                else:
                                    check_ring_srt = False
                                    x['ring_agg'] = None
                            else:
                                # Trong truong hop khong tim duoc DSL hoac OLT thi tim ring agg dua theo BRAS INFO
                                check_ring_srt = False
                                x['ring_srt'] = None

                    if 'sub_interface' in x:
                        sub_intf = x['sub_interface']
                        pos_dot = sub_intf.find('.')
                        if pos_dot > 0:
                            x['bras_port'] = sub_intf[:pos_dot]
                            x['vlan'] = sub_intf[pos_dot + 1:]
                            if 'brasname' in x:

                                x['bras_port_vlan'] = x['brasname'] + '_' + x['bras_port'] + '.' + x['vlan']
                                x['bras_and_port'] = x['brasname'] + '_' + x['bras_port']
                                _bras_lst.append(x['brasname'])
                                _bras_port_lst.append(x['bras_and_port'])
                                _bras_port_vlan_lst.append(x['bras_port_vlan'])
                                # search tim ket qua RING AGG tap trung tu BRAS
                                if not check_ring_srt:
                                    srch_dct = dict(bras_master_code=x['brasname'],
                                                    port_bras_master=x['bras_port'],
                                                    vlan_bras_master=x['vlan'])

                                    ring_agg_lst = _tbl_bras_hsi_inf.search(**srch_dct)
                                    if ring_agg_lst:
                                        if 'agg1_code' in ring_agg_lst[0]:
                                            _agg1_code = ring_agg_lst[0]['agg1_code']
                                            srch_agg_dct = dict(searchAgg=_agg1_code)
                                            _ring_agg_srch_lst = _tbl_ring.search_single(**srch_agg_dct)
                                            if _ring_agg_srch_lst:
                                                if 'ring_agg' in _ring_agg_srch_lst[0]:
                                                    _ring_agg_lst.append(_ring_agg_srch_lst[0]['ring_agg'])
                                    else:
                                        # search bras backup
                                        srch_dct = dict(bras_backup_code=x['brasname'],
                                                        port_bras_backup=x['bras_port'],
                                                        vlan_bras_backup=x['vlan'])

                                        ring_agg_lst = _tbl_bras_hsi_inf.search(**srch_dct)
                                        if ring_agg_lst:
                                            if 'agg1_code' in ring_agg_lst[0]:
                                                _agg1_code = ring_agg_lst[0]['agg1_code']
                                                srch_agg_dct = dict(searchAgg=_agg1_code)
                                                _ring_agg_srch_lst = _tbl_ring.search_single(**srch_agg_dct)
                                                if _ring_agg_srch_lst:
                                                    if 'ring_agg' in _ring_agg_srch_lst[0]:
                                                        _ring_agg_lst.append(_ring_agg_srch_lst[0]['ring_agg'])



                                                        # check ket noi BRAS port VLAN
                # Tinh ra kha nang xuat hien nhieu nhat
                num_input = len(csv_lst)
                num_acc = len(res_kv_lst)
                num_ring_srt = len(_ring_srt_lst)
                num_ring_agg = len(_ring_agg_lst)
                _ring_srt_cnt = Most_common(_ring_srt_lst, num_find_int)
                _ring_agg_cnt = Most_common(_ring_agg_lst, num_find_int)
                _bras_cnt = Most_common(_bras_lst, num_find_int)
                _bras_prt = Most_common(_bras_port_lst, num_find_int)
                _bras_prt_vlan = Most_common(_bras_port_vlan_lst, num_find_int)
                _nas_prt = Most_common(_nas_port_lst, num_find_int)

                return render_template('check/result.html', acc_lst=res_kv_lst, num_acc=num_acc, num_input=num_input,
                                       num_acc_inp_fail=num_acc_inp_fail,
                                       num_ring_srt=num_ring_srt,
                                       num_ring_agg=num_ring_agg,
                                       num_acc_stop=num_acc_stop,
                                       num_acc_lgn_fail=num_acc_lgn_fail,
                                       _ring_srt_cnt=_ring_srt_cnt,
                                       _ring_agg_cnt=_ring_agg_cnt,
                                       _bras_cnt=_bras_cnt,
                                       _bras_prt=_bras_prt,
                                       _bras_prt_vlan=_bras_prt_vlan,
                                       _nas_prt=_nas_prt,
                                       num_find=num_find_int)

            return render_template("check/index.html", err="", form=chek_form)
        except Exception as e:
            print('Error %s when checking account ADSL and FTTX %s' % (str(e), ""))
            flash('Error %s' % str(e))
            return render_template("check/index.html", err=str(e), form=chek_form)
    else:
        err = 'You donot have permission here'
        return render_template('check/error.html', info_mess=err)


@check.route('/downloads/<filename>')
@login_required
def download(filename):
    target = os.path.join(app_root, 'static/file/')
    if not os.path.isdir(target):
        os.makedirs(target)
    path = os.path.join(target, filename)

    return send_file(path, as_attachment=True)


def Most_common(lst, num_int):
    data = Counter(lst)
    fix_size = 100
    fix_lst = list()

    if num_int > 0:
        most_lst = data.most_common(num_int)

        for x in most_lst:
            if isinstance(x[0], str):
                if len(x[0]) >= fix_size:
                    y = (x[0][:fix_size] + "...", x[1])
                    fix_lst.append(y)
                else:
                    y = (x[0], x[1])
                    fix_lst.append(y)

    return fix_lst
