from wtforms import StringField, DateField, BooleanField, Form, IntegerField, SelectField, validators


class CellBKKForm(Form):
    cell_name = StringField(u'Cell Name')
    document = StringField(u'Document')
    reason = StringField(u'Reason')
    expire_date = StringField(u'Expire Date')


class SearchForm(Form):
    key_name = StringField(u'Search',)


