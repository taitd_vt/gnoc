from wtforms import StringField, DateField, BooleanField, Form, IntegerField, SelectField, validators


class KickUserForm(Form):
    sgsn = SelectField(u'Node SGSN', choices=[('SGHL04', 'SGHL04_84980200554'), ('SGHL05', 'SGHL05_84980200555'),
                                              ('SGHL06', 'SGHL06_84980200556'), ('SGHL07', 'SGHL07_84980200557'),
                                              ('SGHL08', 'SGHL08_84980200558'), ('SGHL09', 'SGHL09_84980200559'),
                                              ('SGHT07', 'SGHT07_84980200957'), ('SGHT08', 'SGHT08_84980200958'),
                                              ('SGHT09', 'SGHT09_84980200959'), ('SGHT10', 'SGHT10_84980200960'),
                                              ('SGHT11', 'SGHT11_84980200961'), ('SGHK01', 'SGHK01_84980200721'),
                                              ('SGHK02', 'SGHK02_84980200722'), ('SGHK03', 'SGHK03_84980200723'),
                                              ])
    msisdn = StringField(u'msisdn')






