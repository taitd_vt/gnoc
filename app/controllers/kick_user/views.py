# -*- coding:utf-8 -*-
from flask import render_template, flash, request, redirect, url_for
from app.controllers.kick_user import kick
from flask_login import login_required
import os, json
from app.api.vmsa.parsing.vmsa_get_temp_info_param import VmsaGetTemplateInfoParam
from app.api.vmsa.parsing.vmsa_parsing import VmsaParsing

from app.controllers.kick_user.forms import KickUserForm
from core.helpers.stringhelpers import convert_api_vipa_xml_to_dct, convert_api_vmsa_create_dt_log_for_son_xml_to_dct
from core.helpers.stringhelpers import check_legal_str
from core.helpers.int_helpers import check_legal_msisdn
from flask_login import current_user
from core.database.impl.account_impl import User
from core.helpers.date_helpers import get_date_minus_format_ipms, get_date_now_format_ipms, \
    get_date_now_format_elastic, get_date_now
from core.database.impl.server_minhnd.tbl_kick_user_impl import TblKickUserImpl

app_root = os.path.dirname(os.path.abspath(__file__))


@kick.route('/', methods=['GET'])
@login_required
def index():
    try:
        tbl_kick_user_impl = TblKickUserImpl('','', '2019-06-19T00:00:00', '', '')
        web_lst = tbl_kick_user_impl.list()
    except Exception as err:
        return render_template('kick_user/error.html', info_mess=err)
    return render_template("kick_user/index.html", err="", web_lst=web_lst)


@kick.route('/user', methods=['GET', 'POST'])
@login_required
def kick_user():
    chek_form = KickUserForm(request.form)
    info_mess = "Input MSISDN and SGSN to kick"
    check_err = False
    try:
        if request.method == 'POST':
            user_login = current_user.username
            user_type = current_user.find_type_by_id(current_user.id)
            stat_new = 0
            timestamp = get_date_now_format_elastic()

            msisdn = chek_form.msisdn.data
            if msisdn[0] == '0':
                msisdn = '84' + msisdn[1:]

            sgsn = chek_form.sgsn.data
            check_msisdn = check_legal_msisdn(msisdn)
            if user_type == 'cc' or user_type == 'admin_web':
                if check_msisdn:
                    # check wait time:
                    tbl_kick_user_impl = TblKickUserImpl(msisdn, user_login, timestamp, sgsn, '')
                    check_wait = tbl_kick_user_impl.check_wait_time()
                    if check_wait:
                        _chk_tmpl = VmsaGetTemplateInfoParam(26333)
                        _chk_res = _chk_tmpl.request()

                        _check_res_tmpl = _chk_tmpl.check_dct_xml(_chk_res)
                        if _check_res_tmpl:
                            para_lst = list()
                            para_dct = dict(para_key="MSISDN", para_val=msisdn)
                            para_lst.append(para_dct)
                            _vmsa_parsing = VmsaParsing(26333, "cc-" + user_login + "-kick", para_lst, sgsn, "SGSN", 1)
                            _res = _vmsa_parsing.request()
                            _res_dct = convert_api_vmsa_create_dt_log_for_son_xml_to_dct(_res)
                            _res_chk_xml = _vmsa_parsing.check_dct_xml(_res_dct)
                            # print(_res_chk_xml)
                            if isinstance(_res_dct, dict):
                                if 'resultMessage' in _res_dct:
                                    tbl_kick_user_impl.result = _res_dct['resultMessage']
                            else:
                                tbl_kick_user_impl.result = str(_res_chk_xml)

                            tbl_kick_user_impl.save()
                            return redirect(url_for('kick.index'))
                    else:
                        info_mess = "Not enough time 60s from the last kick"
                        return render_template('kick_user/error.html', info_mess=info_mess)
                else:
                    info_mess = "Number you input is invalid"
                    return render_template('kick_user/error.html', info_mess=info_mess)

            else:
                info_mess = "You don't have permission here!"
                return render_template('deny_web/error.html', info_mess=info_mess)
    except Exception as err:
        return render_template('kick_user/error.html', info_mess=err)

    return render_template('kick_user/add.html', info_mess=info_mess, form=chek_form)

