__author__ = 'VTN-DHML-DHIP10'
from os import environ


REDIS_HOST = "192.168.251.15"
REDIS_PORT = 9292
BROKER_URL = environ.get('REDIS_URL', "redis://{host}:{port}/0".format(
    host=REDIS_HOST, port=str(REDIS_PORT)))
CELERY_RESULT_BACKEND = BROKER_URL
