from app import create_app
from flask_script import Manager, Server
import os
from celery import Celery
from flask_mail import Mail
# disable log info
import logging
log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)


app = create_app(os.getenv('IRONMAN_EVR') or 'default')

manager = Manager(app)


def make_celery(app):
    # create context tasks in celery
    celery = Celery(app.import_name, broker=app.config['BROKER_URL'])
    celery.config_from_object('celeryconfig')
    # celery.conf.update(app.config)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask

    return celery

celery = make_celery(app)
mail = Mail(app)


@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response


manager.add_command('runserver', Server(host='0.0.0.0', port='9110',
                                        use_debugger='True', use_reloader='True'))

#

if __name__ == '__main__':

    manager.run()

